# -*- coding: utf-8 -*-

"""
SFTPに置かれるファイルを取得/転送する処理の起動モジュール
"""

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'production'

import argparse
import datetime
import glob
import os
import re
import shutil
import traceback
from   conf.config          import BatchConfig
import lib.const            as     const
from   lib.lockfile         import LockFile, AlreadyLocked
import lib.sftputil         as     sftputil
from   lib.s3util           import S3Util
import lib.workflow.starter as     wfstarter
import lib.util             as     util


def process_local_receive_files(agency_id, env):

    # Load config
    config, fmon_config, file_configs = _load_configs(agency_id, env)
    logger = config.get_prefixed_transfer_logger(
                 'filemonitor', '[FileMonitor process local {}] '.format(agency_id)
             )
    try:
        logger.info('<Start filemonitor process local>')

        with LockFile('fmon_process_local_' + agency_id, env):

            # Receive file pattern
            # e.g. path/{sid} -> path/*/*
            rcv_dir = fmon_config['receive_dir']
            rcv_file_pattern = os.path.join(re.sub('{.+}', '*', rcv_dir), '*')

            # List directory
            s3util = S3Util()
            for rcv_file in _list_local_files(rcv_file_pattern):

                # Find config
                file_config, file_date, sid = _parse_local_filename(file_configs, rcv_file)
                if not file_config:
                    logger.warn('Unknown file: local_file=%s', rcv_file)
                    continue

                data_type = file_config['type']
                logger.info('File found: sid=%s, data_type=%s, local_file=%s',
                            sid, data_type, rcv_file)

                # Upload to s3
                s3bucket = config.s3bucket_transfer_data
                s3dir    = file_config['s3dir'].format(sid=sid)
                s3util.upload_single_file(rcv_file, s3bucket, s3dir)

                # Start workflow
                try:
                    s3file = os.path.join(s3dir, os.path.basename(rcv_file))
                    run_id = wfstarter.start_transfer(
                                 sid, env, data_type, s3bucket, s3file
                             )
                except:
                    logger.exception('Failed to start workflow: '
                                     'sid=%s, data_type=%s, s3bucket=%s, s3file=%s',
                                      sid, data_type, s3bucket, s3file)
                else:
                    logger.info('Workflow started: '
                                'sid=%s, data_type=%s, s3bucket=%s, s3file=%s, run_id=%s',
                                sid, data_type, s3bucket, s3file, run_id)

                # Save local file
                if fmon_config['save_file_days'] < 0:
                    os.remove(rcv_file)
                else:
                    save_dir  = fmon_config['save_dir'].format(sid=sid)
                    save_file = os.path.join(save_dir, os.path.basename(rcv_file))
                    if not os.path.exists(save_dir):
                        os.makedirs(save_dir)
                    if os.path.exists(save_file):
                        save_file = save_file + '.' + datetime.datetime.now().strftime('%Y%m%d%H%M%S')
                        logger.error('Duplicate receive: rcv_file=%s, save_file=%s',
                                     rcv_file, save_file)
                    shutil.move(rcv_file, save_file)

    except AlreadyLocked:
        logger.warn('Still running')
    except Exception:
        logger.exception('Failed to process local: laststacktrace="%s"',
                         traceback.format_exc().splitlines()[-1])
        raise
    finally:
        logger.info('<End filemonitor process local>')


def remove_local_save_files(agency_id, env):

    # Load config
    config, fmon_config, file_configs = _load_configs(agency_id, env)
    logger = config.get_prefixed_transfer_logger(
                 'filemonitor', '[FileMonitor remove local {}] '.format(agency_id)
             )
    try:
        logger.info('<Start filemonitor remove local>')

        with LockFile('fmon_remove_local_' + agency_id, env):

            # Save file pattern
            # e.g. path/{sid} -> path/*/*
            save_dir = fmon_config['save_dir']
            save_file_pattern = os.path.join(re.sub('{.+}', '*', save_dir), '*')

            # Remove target date
            target_date = datetime.date.today() - datetime.timedelta(days=fmon_config['save_file_days'])
            def _over_save_days(f):
                file_config, file_date, sid = _parse_local_filename(file_configs, f)
                if not file_config:
                    return False
                return file_date < target_date

            # List directory
            for save_file in [f for f in _list_local_files(save_file_pattern) if _over_save_days(f)]:
                os.remove(save_file)
                logger.info('File removed: local_file=%s', save_file)

    except AlreadyLocked:
        logger.warn('Still running')
    except Exception, e:
        logger.exception('Failed to remove local: laststacktrace="%s"',
                         traceback.format_exc().splitlines()[-1])
        raise e
    finally:
        logger.info('<End filemonitor remove local>')

#---------------------------------------------------------------------------
#   Monitor ScaleAds rawdata file
#---------------------------------------------------------------------------
def process_scaleads_rawdata_file(agency_id, env, target_datetime_str=None):
    """
    ScaleoutのSFTPサーバからrawdataを取得し、S3に転送する関数

    実行日付のn日前のrawdataを取得し転送する(Scaleoutのrawdata生成が一定日時遅れるため)

    agency_id : ダミーの引数(本モジュールのインターフェース上必要なため)
    env       : 実行環境
    """

    # Load config
    AGENCY_ID    = 'scaleads'
    DELAYED_DAYS = 5
    DUMMY_STR    = agency_id
    DATA_TYPE    = 'rawdata'

    if not target_datetime_str:
        target_datetime_str = util.create_n_delta_days_datetime_str(delta_days=DELAYED_DAYS)

    config, fmon_config, file_configs = _load_configs(AGENCY_ID, env)
    logger = config.get_prefixed_transfer_logger(
                 'filemonitor', '[FileMonitor process remote {}] '.format(AGENCY_ID)
             )
    try:
        logger.info('<Start filemonitor process remote>')

        with LockFile('fmon_process_remote_' + AGENCY_ID, env):
            s3util = S3Util()

            for file_config in file_configs:
                s3bucket  = config.s3bucket_transfer_data
                s3dir     = file_config['s3dir']

                # Initialize local directory
                local_dir = config.tmp_transfer_dir.format(sid=DUMMY_STR, pid=AGENCY_ID,
                                                           data_type=DATA_TYPE)
                shutil.rmtree(local_dir, ignore_errors=True)
                os.makedirs(local_dir)

                with sftputil.open(host=file_config['host'],
                                   port=file_config['port'],
                                   username=file_config['username'],
                                   password=file_config.get('password'),
                                   pkey_file=file_config.get('pkey_file')) as sftp:

                    # Process remote files
                    target_filename     = file_config['filename_pattern'].format(dt=target_datetime_str)
                    remote_dir          = file_config['remote_dir']
                    remote_file         = os.path.join(remote_dir, target_filename)

                    if not sftp.exists(remote_file):
                        logger.exception('target rawdata file (%s) is not exist: remote_dir=%s', target_filename, remote_dir)
                        raise Exception, 'target rawdata file is not exist'

                    logger.info('File found: remote_file=%s', remote_file)

                    # Download remote file
                    sftp.get(remote_file, local_dir)

                    # Upload to s3
                    local_file = os.path.join(local_dir, target_filename)

                    s3util.upload_single_file(local_file, s3bucket, s3dir)

                    # Start workflow
                    try:
                        s3file = os.path.join(s3dir, os.path.basename(local_file))
                        run_id = wfstarter.start_rawdata_process(
                            env, DATA_TYPE, s3bucket, s3file
                            )
                    except:
                        logger.exception('Failed to start workflow: '
                                         'data_type=%s, s3bucket=%s, s3file=%s',
                                         DATA_TYPE, s3bucket, s3file)
                    else:
                        logger.info('Workflow started: '
                                    'data_type=%s, s3bucket=%s, s3file=%s, run_id=%s',
                                    DATA_TYPE, s3bucket, s3file, run_id)


    except AlreadyLocked:
        logger.warn('Still running')
    except Exception, e:
        logger.exception('Failed to process remote: laststacktrace="%s"',
                         traceback.format_exc().splitlines()[-1])
        raise e
    finally:
        logger.info('<End filemonitor process remote>')


#---------------------------------------------------------------------------
#   Private functions
#---------------------------------------------------------------------------
def _load_configs(agency_id, env):
    config = BatchConfig(env)
    fmon_config  = config.filemonitor[agency_id]
    file_configs = fmon_config['file_configs']
    return config, fmon_config, file_configs

def _list_local_files(file_pattern):
    _is_file    = lambda f: os.path.isfile(f)
    _is_dotfile = lambda f: os.path.basename(f).startswith('.')
    return sorted([f for f in glob.glob(file_pattern) if _is_file(f) and not _is_dotfile(f)])

def _parse_local_filename(file_configs, rcv_file):
    for file_config in file_configs:
        rcv_filename = os.path.basename(rcv_file)
        r = re.compile(file_config['filename_pattern'])
        m = r.match(rcv_filename)
        if not m:
            continue
        return (
            file_config,
            datetime.datetime.strptime(m.group('date'), '%Y%m%d').date(),
            file_config['sid'] if 'sid' in file_config else m.group('sid')
        )
    else:
        return None, None, None

#---------------------------------------------------------------------------
#   Commandline function
#---------------------------------------------------------------------------
def _get_function_args_from_parse_args(parse_args):
    my_args = vars(parse_args).copy()
    del my_args['func']
    return my_args

if __name__ == '__main__':

    # Create command
    parser = argparse.ArgumentParser()

    # Create subcommands
    subparsers = parser.add_subparsers(help='subcommands')

    # Create "process_local_receive_files" command
    parser_process = subparsers.add_parser('process', help='process local receive files')
    parser_process.add_argument('agency_id', type=str, help='agency_id')
    parser_process.add_argument('env', type=str, choices=const.ENVS, help='environment')
    parser_process.set_defaults(func=process_local_receive_files)

    # Create "remove_local_save_files" command
    parser_remove = subparsers.add_parser('remove', help='remove save files')
    parser_remove.add_argument('agency_id', type=str, help='agency_id')
    parser_remove.add_argument('env', type=str, choices=const.ENVS, help='environment')
    parser_remove.set_defaults(func=remove_local_save_files)

    # Create "process_scaleads_receive_rawdata_file" command
    parser_scaleads = subparsers.add_parser('scaleads_rawdata', help='process scaleads receive rawdata file')
    parser_scaleads.add_argument('agency_id', default='dummy', help='agency_id')
    parser_scaleads.add_argument('env', type=str, choices=const.ENVS, help='environment')
    parser_scaleads.add_argument('-t', '--target_datetime_str', type=str, help='target datetime(format: yyyymmdd)')
    parser_scaleads.set_defaults(func=process_scaleads_rawdata_file)

    # Parse argument
    args = parser.parse_args()

    # Execute command
    my_args = _get_function_args_from_parse_args(args)
    args.func(**my_args)
