# -*- coding: utf-8 -*-

import json
import logging
import logging.config
import os
from lib.crypto import AesCrypto

class BaseConfig:
    ENVIRONMENTS = ['prd', 'stg', 'dev']
    DEFAULT_ENVIRONMENT = 'dev'

    def __init__(self, environment):
        if environment in self.ENVIRONMENTS:
            self.environment = environment
        else:
            self.environment = self.DEFAULT_ENVIRONMENT

        self._set_local_config(self.environment)
        self._set_s3_config(self.environment)
        self._set_common_config(self.environment)

    def _set_from_json(self, json_file):
        with open(json_file,) as f:
            return json.load(f)

    def _set_from_txt(self, text_file):
        with open(text_file) as f:
            return f.read().strip()

    def _set_local_config(self, environment):
        self.home_dir                  = '/usr/local/tamar'
        self.tmp_dir                   = os.path.join(self.home_dir, 'tmp')
        self.tmp_mapping_dir           = os.path.join(self.tmp_dir,  'mapping/{sid}/{pid}')
        self.tmp_extsegdata_dir        = os.path.join(self.tmp_dir,  'extsegdata/{sid}/{pid}')
        self.tmp_optout_dir            = os.path.join(self.tmp_dir,  'optout/{target}')
        self.tmp_transfer_dir          = os.path.join(self.tmp_dir,  'transfer/{sid}/{pid}/{data_type}')
        self.tmp_tools_dir             = os.path.join(self.tmp_dir,  'tools/{tool_name}')
        self.tmp_rawdata_dir           = os.path.join(self.tmp_dir,  'rawdata')
        self.tmp_converted_rawdata_dir = os.path.join(self.tmp_dir,  'converted_rawdata')
        self.log_dir                   = os.path.join(self.home_dir, 'log')
        self.lock_dir                  = os.path.join(self.home_dir, 'lock')
        self.repos_dir                 = os.path.join(self.home_dir, 'repos')

    def _set_s3_config(self, environment):
        self.s3bucket_partner_log   = '{env}-partner-log'.format(env=self.environment)
        self.s3bucket_site_log      = '{env}-site-log'.format(env=self.environment)
        self.s3bucket_transfer_data = '{env}-transfer-data'.format(env=self.environment)
        self.s3bucket_api_log       = '{env}-api-log'.format(env=self.environment)

    def _set_common_config(self, environment):
        if environment == 'prd':
            SITE_MST_FILE    = 'conf/common/prd/site_mst.json'
            PARTNER_MST_FILE = 'conf/common/prd/partner_mst.json'
            MAPPING_FILE     = 'conf/common/prd/mapping.json'
            DATABASE_FILE    = 'conf/common/prd/database.json'
            CRYPTOKEY_FILE   = 'conf/common/prd/cryptokey.txt'
        elif environment == 'stg':
            SITE_MST_FILE    = 'conf/common/stg/site_mst.json'
            PARTNER_MST_FILE = 'conf/common/stg/partner_mst.json'
            MAPPING_FILE     = 'conf/common/stg/mapping.json'
            DATABASE_FILE    = 'conf/common/stg/database.json'
            CRYPTOKEY_FILE   = 'conf/common/stg/cryptokey.txt'
        else:
            SITE_MST_FILE    = 'conf/common/dev/site_mst.json'
            PARTNER_MST_FILE = 'conf/common/dev/partner_mst.json'
            MAPPING_FILE     = 'conf/common/dev/mapping.json'
            DATABASE_FILE    = 'conf/common/dev/database.json'
            CRYPTOKEY_FILE   = 'conf/common/dev/cryptokey.txt'

        self.site_mst    = self._set_from_json(SITE_MST_FILE)
        self.partner_mst = self._set_from_json(PARTNER_MST_FILE)
        self.mapping     = self._set_from_json(MAPPING_FILE)
        self.database    = self._set_from_json(DATABASE_FILE)
        self.crypto      = AesCrypto(self._set_from_txt(CRYPTOKEY_FILE))


class ApplicationConfig(BaseConfig):
    def __init__(self, environment):
        BaseConfig.__init__(self, environment)

class BatchConfig(BaseConfig):
    def __init__(self, environment):
        BaseConfig.__init__(self, environment)

        self._set_batch_config(self.environment)

    def _set_batch_config(self, environment):
        if environment == 'prd':
            FILEMONITOR_FILE = 'conf/batch/prd/filemonitor.json'
            WORKFLOW_FILE    = 'conf/batch/prd/workflow.json'
            LOGGER_CONF_FILE = 'conf/batch/prd/logger.conf'
        elif environment == 'stg':
            FILEMONITOR_FILE = 'conf/batch/stg/filemonitor.json'
            WORKFLOW_FILE    = 'conf/batch/stg/workflow.json'
            LOGGER_CONF_FILE = 'conf/batch/stg/logger.conf'
        else:
            FILEMONITOR_FILE = 'conf/batch/dev/filemonitor.json'
            WORKFLOW_FILE    = 'conf/batch/dev/workflow.json'
            LOGGER_CONF_FILE = 'conf/batch/dev/logger.conf'

        self.filemonitor = self._set_from_json(FILEMONITOR_FILE)
        self.workflow    = self._set_from_json(WORKFLOW_FILE)

        logging.config.fileConfig(LOGGER_CONF_FILE)
        self.logger_mapping           = logging.getLogger('mapping')
        self.logger_extsegdata        = logging.getLogger('extsegdata')
        self.logger_transfer          = logging.getLogger('transfer')
        self.logger_workflow_decision = logging.getLogger('workflow_decision')
        self.logger_workflow_activity = logging.getLogger('workflow_activity')
        self.logger_rawdata           = logging.getLogger('rawdata')

    class AddPrefixFilter(object):
        def __init__(self, log_prefix):
            self.log_prefix = log_prefix

        def filter(self, record):
            record.msg = self.log_prefix + record.msg
            return True

    def _get_log_funcs(self, logger, log_prefix):
        if not logger.filters:
            logger.addFilter(BatchConfig.AddPrefixFilter(log_prefix))
        return (
                    logger.info,
                    logger.warning,
                    logger.error,
                    logger.exception,
               )

    def get_mapping_log_funcs(self, logger_suffix, log_prefix):
        return self._get_log_funcs(
                   self.logger_mapping.getChild(logger_suffix), log_prefix
               )

    def get_extsegdata_log_funcs(self, logger_suffix, log_prefix):
        return self._get_log_funcs(
                   self.logger_extsegdata.getChild(logger_suffix), log_prefix
               )

    def get_transfer_log_funcs(self, logger_suffix, log_prefix):
        return self._get_log_funcs(
                   self.logger_transfer.getChild(logger_suffix), log_prefix
               )

    def get_workflow_decision_log_funcs(self, logger_suffix, log_prefix):
        return self._get_log_funcs(
                   self.logger_workflow_decision.getChild(logger_suffix), log_prefix
               )

    def get_workflow_activity_log_funcs(self, logger_suffix, log_prefix):
        return self._get_log_funcs(
                   self.logger_workflow_activity.getChild(logger_suffix), log_prefix
               )

    def get_rawdata_log_funcs(self, logger_suffix, log_prefix):
        return self._get_log_funcs(
                   self.logger_rawdata.getChild(logger_suffix), log_prefix
               )

    def _get_prefixed_logger(self, logger, logger_suffix, log_prefix):
        child_logger = logger.getChild(logger_suffix)
        if not child_logger.filters:
            child_logger.addFilter(BatchConfig.AddPrefixFilter(log_prefix))
        return child_logger

    def get_prefixed_mapping_logger(self, logger_suffix, log_prefix):
        return self._get_prefixed_logger(self.logger_mapping,
                                         logger_suffix,
                                         log_prefix)

    def get_prefixed_extsegdata_logger(self, logger_suffix, log_prefix):
        return self._get_prefixed_logger(self.logger_extsegdata,
                                         logger_suffix,
                                         log_prefix)

    def get_prefixed_transfer_logger(self, logger_suffix, log_prefix):
        return self._get_prefixed_logger(self.logger_transfer,
                                         logger_suffix,
                                         log_prefix)

    def get_prefixed_workflow_decision_logger(self, logger_suffix, log_prefix):
        return self._get_prefixed_logger(self.logger_workflow_decision,
                                         logger_suffix,
                                         log_prefix)

    def get_prefixed_workflow_activity_logger(self, logger_suffix, log_prefix):
        return self._get_prefixed_logger(self.logger_workflow_activity,
                                         logger_suffix,
                                         log_prefix)
    
    def get_prefixed_rawdata_logger(self, logger_suffix, log_prefix):
        return self._get_prefixed_logger(self.logger_rawdata,
                                         logger_suffix,
                                         log_prefix)
