# -*- coding: utf-8 -*-

"""
外部からのセグメントマスタに対する操作を処理するAPI
"""

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'production'

from   flask                  import Flask, jsonify, request, Response
import flask_restful
from   flask.ext.restful      import reqparse
import json
from   lib.const              import DATASOURCE
import lib.segmentmsttable    as     segmentmsttable
from   lib.extapi.base        import ExtAPIBase
from   lib.extapi.custom_rest import CustomRestAPI
import lib.extapi.errors      as     errors
from   lib.extapi.healthcheck import HealthCheckAPI
import lib.extapi.parameters  as     params
import logging
import werkzeug

app = Flask(__name__)

class SegmentMasterAPI(ExtAPIBase):

    @staticmethod
    def _retrieve_all_registered_segments(sid, pid):
        with segmentmsttable.open(pid, 'rt'+sid, SegmentMasterAPI.config.database[sid]) as segmst:
            registered_segment_rowproxies = segmst.select_all_rows()
        return [dict(rsr.items()) for rsr in registered_segment_rowproxies]

    @staticmethod
    def _retrieve_registered_segment(sid, pid, seg_id):
        with segmentmsttable.open(pid, 'rt'+sid, SegmentMasterAPI.config.database[sid]) as segmst:
            registered_segment = segmst.select_row_by_segment_id(seg_id)
            if registered_segment:
                return dict(registered_segment.items())
            else:
                raise errors.SegmentNotFound()

    @ExtAPIBase.auth.login_required
    def get(self):
        """
        CrossOven のもつセグメント一覧を取得し、返却する
        """
        try:
            try:
                parser = reqparse.RequestParser()
                parser.add_argument('pid', type=params.PID, required=True, location='args')
                parser.add_argument('sid', type=params.SID, required=True, location='args')
                args = parser.parse_args()
                pid = args['pid'].value
                sid = args['sid'].value
            except (errors.IllegalParameter, werkzeug.exceptions.ClientDisconnected) as e:
                app.logger.warn(e.description)
                raise errors.IllegalParameter()
            except Exception, e:
                raise errors.IllegalParameter()

            registered_segments = SegmentMasterAPI._retrieve_all_registered_segments(sid, pid)
            segments = [ { 'id':s['segment_id_from'], 'name':s['name'], 'description':s['description'] } for s in registered_segments ]
            return Response(json.dumps({'segments':segments}, ensure_ascii=False), content_type='application/json; charset=UTF-8')

        except Exception, e:
            app.logger.exception('[ERROR]')
            raise e

    @ExtAPIBase.auth.login_required
    def post(self):
        """
        新しくセグメントを追加する
        """
        try:
            try:
                parser = reqparse.RequestParser()
                parser.add_argument('pid',         type=params.PID,   required=True, location='json')
                parser.add_argument('sid',         type=params.SID,   required=True, location='json')
                parser.add_argument('seg_id',      type=params.SegID, required=True, location='json')
                parser.add_argument('name',        type=params.Name,  required=True, location='json')
                parser.add_argument('description', type=params.Description,          location='json')
                args = parser.parse_args()
                pid         = args['pid'].value
                ds          = DATASOURCE[pid]
                sid         = args['sid'].value
                seg_id      = args['seg_id'].value
                name        = args['name'].value
                description = None if args['description'] is None else args['description'].value
                rta         = SegmentMasterAPI.config.mapping[sid]['rt']['rta']
            except (errors.IllegalParameter, werkzeug.exceptions.ClientDisconnected) as e:
                app.logger.warn(e.description)
                raise errors.IllegalParameter()
            except Exception, e:
                raise errors.IllegalParameter()

            registered_segments = SegmentMasterAPI._retrieve_all_registered_segments(sid, pid)
            if seg_id in [ s['segment_id_from'] for s in registered_segments ]:
                raise errors.SegmentDuplicated('Given segment ID is already registered.')
            if name in [ s['name'] for s in registered_segments ]:
                raise errors.SegmentDuplicated('Given segment name is already registered.')

            if SegmentMasterAPI.rtoaster_api.is_over_limit(pid, rta, ds):
                raise errors.OverLimit()

            payload  = { "name": name, "memo": description }
            response = SegmentMasterAPI.rtoaster_api.post(pid, rta, ds, payload)
            if response.status_code == 200:
                rt_segid = str(json.loads(response.text)['id'])
                try:
                    with segmentmsttable.open(pid, 'rt'+sid, SegmentMasterAPI.config.database[sid]) as segmst:
                        segmst.insert_row(seg_id, rt_segid, name, description)
                except Exception, e:
                    app.logger.error(
                        '[DB Failed:insert] pid:{pid}, sid:{sid}, seg_id:{seg_id}, name:{name}, description:{description}, message:{message}'\
                        .format(pid=pid, sid=sid, seg_id=seg_id, name=name, description=description, message=str(e))
                    )
                    raise errors.StatusConflicted()
                return jsonify({'status': 'ok'})
            else:
                app.logger.error('[RtoasterAPI Failed] status:{status}, response:{response}'\
                                 .format(status=response.status_code, response=response.text))
                raise errors.PostFailed()

        except Exception, e:
            app.logger.exception('[ERROR]')
            raise e


    @ExtAPIBase.auth.login_required
    def put(self):
        """
        セグメント情報を編集する
        """
        try:
            try:
                parser = reqparse.RequestParser()
                parser.add_argument('pid',         type=params.PID,   required=True, location='json')
                parser.add_argument('sid',         type=params.SID,   required=True, location='json')
                parser.add_argument('seg_id',      type=params.SegID, required=True, location='json')
                parser.add_argument('name',        type=params.Name,                 location='json')
                parser.add_argument('description', type=params.Description,          location='json')
                args = parser.parse_args()
                pid         = args['pid'].value
                ds          = DATASOURCE[pid]
                sid         = args['sid'].value
                seg_id      = args['seg_id'].value
                name        = None if args['name']        is None else args['name'].value
                description = None if args['description'] is None else args['description'].value
                rta         = SegmentMasterAPI.config.mapping[sid]['rt']['rta']
            except (errors.IllegalParameter, werkzeug.exceptions.ClientDisconnected) as e:
                app.logger.warn(e.description)
                raise errors.IllegalParameter()
            except Exception, e:
                raise errors.IllegalParameter()

            registered_segment = SegmentMasterAPI._retrieve_registered_segment(sid, pid, seg_id)
            rt_segid = registered_segment['segment_id_to']

            payload  = { "id": rt_segid, "name": name, "memo": description }
            response = SegmentMasterAPI.rtoaster_api.put(pid, rta, ds, payload)
            if response.status_code == 200:
                try:
                    with segmentmsttable.open(pid, 'rt'+sid, SegmentMasterAPI.config.database[sid]) as segmst:
                        segmst.update_row_by_segment_id(seg_id, { "name":name, "description":description })
                except Exception, e:
                    app.logger.error(
                        '[DB Failed:update] pid:{pid}, sid:{sid}, seg_id:{seg_id}, name:{name}, description:{description}, message:{message}'\
                        .format(pid=pid, sid=sid, seg_id=seg_id, name=name, description=description, message=str(e))
                    )
                    raise errors.StatusConflicted()
                return jsonify({'status': 'ok'})
            else:
                app.logger.error('[RtoasterAPI Failed] status:{status}, response:{response}'\
                                 .format(status=response.status_code, response=response.text))
                raise errors.UpdateFailed()

        except Exception, e:
            app.logger.exception('[ERROR]')
            raise e

    @ExtAPIBase.auth.login_required
    def delete(self):
        """
        セグメントを連携停止状態とし、CrossOven から削除する
        """
        try:
            try:
                parser = reqparse.RequestParser()
                parser.add_argument('pid',    type=params.PID,   required=True, location='json')
                parser.add_argument('sid',    type=params.SID,   required=True, location='json')
                parser.add_argument('seg_id', type=params.SegID, required=True, location='json')
                args   = parser.parse_args()
                pid    = args['pid'].value
                ds     = DATASOURCE[pid]
                sid    = args['sid'].value
                seg_id = args['seg_id'].value
                rta    = SegmentMasterAPI.config.mapping[sid]['rt']['rta']
            except (errors.IllegalParameter, werkzeug.exceptions.ClientDisconnected) as e:
                app.logger.warn(e.description)
                raise errors.IllegalParameter()
            except Exception, e:
                raise errors.IllegalParameter()

            registered_segment = SegmentMasterAPI._retrieve_registered_segment(sid, pid, seg_id)
            rt_segid = registered_segment['segment_id_to']

            payload  = { "id": rt_segid, "action": "STOP" }
            response = SegmentMasterAPI.rtoaster_api.put(pid, rta, ds, payload)
            if response.status_code == 200:
                try:
                    with segmentmsttable.open(pid, 'rt'+sid, SegmentMasterAPI.config.database[sid]) as segmst:
                        segmst.delete_rows_by_segment_id(seg_id)
                except Exception, e:
                    app.logger.error(
                        '[DB Failed:delete] pid:{pid}, sid:{sid}, seg_id:{seg_id}, message:{message}'\
                        .format(pid=pid, sid=sid, seg_id=seg_id, message=str(e))
                    )
                    raise errors.StatusConflicted()
                return jsonify({'status': 'ok'})
            else:
                app.logger.error('[RtoasterAPI Failed] status:{status}, response:{response}'\
                                 .format(status=response.status_code, response=response.text))
                raise errors.DeleteFailed()

        except Exception, e:
            app.logger.exception('[ERROR]')
            raise e

api = CustomRestAPI(app)
api.add_resource(SegmentMasterAPI, '/v1/segmst')
api.add_resource(HealthCheckAPI,   '/healthcheck')
app.logger.setLevel(logging.WARN)
# FIXME debug モードにしないと uwsgi ログに出力されない
app.debug = True


if __name__ == '__main__':
    api.app.run(debug=True)
