# -*- coding: utf-8 -*-

"""
外部セグメントのデータをロードするクラス
"""

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'production'

import argparse
import datetime
import os
import shutil
import traceback
from   conf.config    import BatchConfig
from   lib.lockfile   import LockFile, AlreadyLocked
from   lib.extsegdata import logparse
from   lib.extsegdata import csvload
from   lib.s3util     import S3Util
import lib.util       as     util

class NoLogFiles(Exception):
    pass

class ExtSegDataLoader(object):
    S3_APIDATALOG_DIR = 'api-data-log/{sid}/{pid}/{year}/{month}/{day}/{hour}/'
    S3_EXTSEGDATA_DIR = 'extseg-data/{sid}/{pid}/{year}/{month}/{day}/{hour}/'

    def __init__(self, sid, pid, env, target_hour=None):
        self.sid = sid
        self.pid = pid
        self.target_hour = target_hour

        self.config = BatchConfig(env)
        self.env    = self.config.environment

        if target_hour:
            target_datetime = datetime.datetime.strptime(self.target_hour, '%Y%m%d%H')
        else:
            target_datetime = datetime.datetime.now() - datetime.timedelta(hours=1)
        extsegdata_info = {
            'env'  : self.env,
            'sid'  : self.sid,
            'pid'  : self.pid,
            'year' : target_datetime.strftime('%Y'),
            'month': target_datetime.strftime('%m'),
            'day'  : target_datetime.strftime('%d'),
            'hour' : target_datetime.strftime('%H')
        }

        self.s3util            = S3Util()
        self.s3_log_bucket     = self.config.s3bucket_api_log
        self.s3_apidatalog_dir = self.S3_APIDATALOG_DIR.format(**extsegdata_info)
        self.s3_extsegdata_dir = self.S3_EXTSEGDATA_DIR.format(**extsegdata_info)

        self.local_base_dir = self.config.tmp_extsegdata_dir.format(sid=sid, pid=pid)
        self.local_log_dir  = os.path.join(self.local_base_dir, 'log')
        self.local_csv_dir  = os.path.join(self.local_base_dir, 'csv')

        self.logger    = self.config.logger_extsegdata
        self.logprefix = '[ExtSegDataLoader {sid} {pid} {hour}] '.format(sid=sid,
                                                                         pid=pid,
                                                                         hour=target_datetime.strftime('%Y%m%d%H'))

    def execute(self):
        self.logger.info(self.logprefix + '<Start importing extsegdata>')

        try:
            with(self._create_lock_file()):
                self.logger.info(self.logprefix + 'Initializing work directory')
                self._initialize_work_directory()

                self.logger.info(self.logprefix + 'Downloading log files')
                self._download_log_files()

                self.logger.info(self.logprefix + 'Parsing log files')
                self._parse_log_files()

                self.logger.info(self.logprefix + 'Loading csv files')
                self._load_csv_files()

                self.logger.info(self.logprefix + 'Saving csv files')
                self._save_csv_files()

        except AlreadyLocked:
            self.logger.warn(self.logprefix + 'Still running')

        except NoLogFiles:
            self.logger.info(self.logprefix + 'No log files')

        except Exception, e:
            self.logger.exception(self.logprefix + 'Failed to import extsegdata [%s]',
                                  traceback.format_exc().splitlines()[-1])
            raise e
        finally:
            self.logger.info(self.logprefix + '<End importing extsegdata>')

    def _create_lock_file(self):
        lock_key = 'extsegdata_{sid}_{pid}'.format(sid=self.sid, pid=self.pid)
        return LockFile(lock_key, self.env)

    def _initialize_work_directory(self):
        shutil.rmtree(self.local_log_dir, ignore_errors=True)
        shutil.rmtree(self.local_csv_dir, ignore_errors=True)
        os.makedirs(self.local_log_dir)
        os.makedirs(self.local_csv_dir)

    def _download_log_files(self):
        s3objs = self.s3util.download_files(self.s3_log_bucket,
                                            self.s3_apidatalog_dir,
                                            self.local_log_dir)
        if not s3objs:
            raise NoLogFiles()

    def _parse_log_files(self):
        logparse.parse(self.sid, self.pid, self.env,
                       self.local_log_dir, self.local_csv_dir)

    def _load_csv_files(self):
        csvload.load(self.sid, self.pid, self.env, self.local_csv_dir)

    def _save_csv_files(self):
        for f in os.listdir(self.local_csv_dir):
            csv_file = os.path.join(self.local_csv_dir, f)
            util.gzip_file(csv_file)
            os.remove(csv_file)

        self.s3util.upload_files(self.local_csv_dir,
                                 self.s3_log_bucket,
                                 self.s3_extsegdata_dir)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Load extsegdata table data')
    parser.add_argument('sid', type=str, help='site_id')
    parser.add_argument('pid', type=str, help='patner_id')
    parser.add_argument('env', type=str, choices=('prd', 'stg', 'dev'), help='environment')
    parser.add_argument('-t', '--target_hour', type=str, help='target hour(format: yyyymmddhh)')
    args = parser.parse_args()

    extsegdata_loader = ExtSegDataLoader(sid=args.sid,
                                         pid=args.pid,
                                         env=args.env,
                                         target_hour=args.target_hour)
    extsegdata_loader.execute()
