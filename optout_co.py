# -*- coding: utf-8 -*-

"""
CrossOven サービスのオプトアウトを行うバッチ
CrossOven cookie が紐づく Rtoaster メンバ値のマッピングをすべて削除する。
"""

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'production'

import argparse
import datetime
import os
import shutil
import csv
import json
import lib.util as util
import lib.mapping.logparse as logparse
import lib.mappingtable as mappingtable
from conf.config      import BatchConfig
from lib.lockfile     import LockFile
from lib.s3util       import S3Util
from sets             import Set

class CoOptout:

    S3_CO_OPTOUT_DIR = 'optout/co/{year}/{month}/{day}/'

    def __init__(self, env, target_day=None):
        self.target_day = target_day

        self.config = BatchConfig(env)
        self.env    = self.config.environment

        if target_day:
            target_datetime = datetime.datetime.strptime(self.target_day, '%Y%m%d')
        else:
            target_datetime = datetime.datetime.now() - datetime.timedelta(days=1)

        optout_info = {
            'year' : target_datetime.strftime('%Y'),
            'month': target_datetime.strftime('%m'),
            'day'  : target_datetime.strftime('%d')
        }
        self.s3_log_bucket    = self.config.s3bucket_partner_log
        self.s3_co_optout_dir = self.S3_CO_OPTOUT_DIR.format(**optout_info)
        self.local_base_dir   = self.config.tmp_optout_dir.format(target='co')
        self.local_log_dir    = os.path.join(self.local_base_dir, 'log')

        self.logger    = self.config.logger_mapping
        self.logprefix = '[CoOptout {day}] '.format(day=target_datetime.strftime('%Y%m%d'))

    def _gather_co_uids(self, log_files):
        co_uids = Set([])
        for log_file in log_files:
            for values in logparse.yield_from_fluentd(log_file, 'uid'):
                co_uids.add(values[0])
        return co_uids

    def _convert_id_from_co_to_rt(self, sid, co_uids):
        rt_uids = Set([])
        with mappingtable.open(sid, 'co', self.config.database[sid]) as mapping_co:
            for co_uid in co_uids:
                for uid in mapping_co.select_user_ids_by_partner_user_id(co_uid):
                    rt_uids.add(uid)
        return rt_uids

    def _delete_records_by_co_uid(self, sid, co_uids):
        with mappingtable.open(sid, 'co', self.config.database[sid]) as mapping_co:
            return mapping_co.delete_rows_by_partner_user_ids(co_uids)

    def _delete_records_by_uid(self, sid, uids):
        count = 0
        for pid in self.config.mapping[sid].keys():
            _pids = []

            if pid == 'rt':
                _bridges = self.config.mapping[sid][pid].get('bridges')
                if _bridges:
                    _pids.extend(_bridges)
            elif pid == 'co' or self.config.partner_mst[pid]['option'].get('no_mapping'):
                continue
            else:
                _pids.append(pid)

            for _pid in _pids:
                with mappingtable.open(sid, _pid, self.config.database[sid]) as mapping:
                    count += mapping.delete_rows_by_user_ids(uids)

        return count

    def execute(self):
        self.logger.info(self.logprefix + '<Start optout>')

        lock_key = 'co_optout'
        lock_file = LockFile(lock_key, self.env)
        if not lock_file.lock():
            self.logger.warn(self.logprefix + 'Still running')
            self.logger.info(self.logprefix + '<End optout>')
            return

        try:
            self.logger.info(self.logprefix + 'Initializing work directory')
            shutil.rmtree(self.local_log_dir, ignore_errors=True)
            os.makedirs(self.local_log_dir)

            self.logger.info(self.logprefix + 'Downloading log files')
            s3 = S3Util()
            keys = s3.download_files(self.s3_log_bucket, self.s3_co_optout_dir, self.local_log_dir)
            if not keys:
                self.logger.info(self.logprefix + 'No log files')
                return

            # delete
            count = 0
            log_files = [os.path.join(self.local_log_dir, f) for f in os.listdir(self.local_log_dir)]
            log_files = [f for f in log_files if os.path.isfile(f)]
            if log_files:
                # gather co uids from logs
                co_uids = self._gather_co_uids(log_files)

                # delete records
                for sid in self.config.mapping.keys():
                    if 'co' not in self.config.mapping[sid].keys():
                        continue

                    # retrieve rtuids
                    rt_uids = self._convert_id_from_co_to_rt(sid, co_uids)

                    # delete records
                    count += self._delete_records_by_uid(sid, rt_uids)
                    count += self._delete_records_by_co_uid(sid, co_uids)
    
            self.logger.info(self.logprefix + '{count} user mappings have been deleted.'.format(count=count))

        except Exception, e:
            self.logger.exception(self.logprefix + 'Failed to optout')
            raise e

        finally:
            lock_file.unlock()
            self.logger.info(self.logprefix + '<End optout>')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Execute co_optout')
    parser.add_argument('env', type=str, choices=('prd', 'stg', 'dev'), help='environment')
    parser.add_argument('-t', '--target_day', type=str, help='target day(format: yyyymmdd)')
    args = parser.parse_args()

    optout = CoOptout(env=args.env, target_day=args.target_day)
    optout.execute()
