# -*- coding: utf-8 -*-

"""
CrossOven の 3rd パーティクッキーをキーとし、Rtoaster の 1st パーティクッキーの紐付けを行うバッチ
* 通常のマッピング処理が定期的に行われることを想定し、2時間前から1時間前の、1時間分のデータを対象とする
* 送信元(sid) の CrossOven クッキーを保存したテーブルから1時間分のデータを取得し、送信先(pid)のテーブルと突き合わせを行う

コマンド例:
    $ python bridge.py 9999 rt9998 dev
    $ python bridge.py 9999 rt9998 dev --target_hour=2014010101
"""

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'production'

import argparse
import datetime
import traceback
from   conf.config      import BatchConfig
from   lib.lockfile     import LockFile, AlreadyLocked
import lib.mappingtable as     mappingtable

class Bridge(object):

    QUERY_JOIN = """
        INSERT INTO DB_TAMAR_{sid}.t_user_mapping_{sid}_rt{pid} (user_id, partner_user_id, tamar_user_type, tamar_user_id, inserted_at)
                SELECT mapping_from.user_id         AS user_id,
                       mapping_to.user_id           AS partner_user_id,
                       2                            AS tamar_user_type,
                       mapping_from.partner_user_id AS tamar_user_id,
                       NOW()                        AS inserted_at
                  FROM DB_TAMAR_{sid}.t_user_mapping_{sid}_co mapping_from
            INNER JOIN DB_TAMAR_{pid}.t_user_mapping_{pid}_co mapping_to
                    ON mapping_from.partner_user_id = mapping_to.partner_user_id
                 WHERE mapping_from.updated_at BETWEEN '{date_from}' AND '{date_to}'
        ON DUPLICATE KEY UPDATE updated_at = NOW()
    """

    def __init__(self, sid, pid, env, target_hour=None):
        self.sid = sid
        self.pid = pid
        self.env = env
        self.target_hour = target_hour
        self.config = BatchConfig(env)

        if target_hour:
            date_from = datetime.datetime.strptime(self.target_hour, '%Y%m%d%H')
        else:
            date_from = datetime.datetime.now() - datetime.timedelta(hours=2)
            date_from = date_from.replace(minute=0, second=0)

        self.date_from = date_from
        self.date_to   = date_from + datetime.timedelta(hours=1)

        self.logger    = self.config.logger_mapping
        self.logprefix = '[Bridge {sid} {pid} {hour}] '.format(sid=sid,
                                                               pid=pid,
                                                               hour=date_from.strftime('%Y%m%d%H'))

    def execute(self):
        self.logger.info(self.logprefix + '<Start bridging>')

        try:
            with(self._create_lock_file()):
                self.logger.info(self.logprefix + 'Execute bridging')
                result = self._execute_bridging()
                self.logger.info(self.logprefix + 'sqlalchemy says {num} rows are affected.'.format(num=result.rowcount))

        except AlreadyLocked:
            self.logger.warn(self.logprefix + 'Still running')

        except Exception, e:
            self.logger.exception(self.logprefix + 'Failed to bridge [%s]', traceback.format_exc().splitlines()[-1])
            raise e
        finally:
            self.logger.info(self.logprefix + '<End bridging>')

    def _execute_bridging(self):
        with self._open_mappingtable() as con:
            result = con.conn.execute(self.QUERY_JOIN.format(sid=self.sid,
                                                             pid=self.pid[2:],
                                                             date_from=self.date_from.strftime('%Y%m%d%H%M%S'),
                                                             date_to=self.date_to.strftime('%Y%m%d%H%M%S')))
        return result

    def _open_mappingtable(self):
        db_config = self.config.database[self.sid]
        return mappingtable.open(self.sid, self.pid, db_config)

    def _create_lock_file(self):
        lock_key = 'bridge_{sid}_{pid}'.format(sid=self.sid, pid=self.pid)
        return LockFile(lock_key, self.env)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Bridge tables via CrossOven cookie')
    parser.add_argument('sid', type=str, help='site_id')
    parser.add_argument('pid', type=str, help='patner_id')
    parser.add_argument('env', type=str, choices=('prd', 'stg', 'dev'), help='environment')
    parser.add_argument('-t', '--target_hour', type=str, help='target hour(format: yyyymmddhh)')
    args = parser.parse_args()

    if not args.pid.startswith('rt'):
        raise Exception('pid must start with \'rt')

    bridge = Bridge(sid=args.sid,
                    pid=args.pid,
                    env=args.env,
                    target_hour=args.target_hour)
    bridge.execute()
