# -*- coding: utf-8 -*-

"""
Rtoaster の広告連携サービスのオプトアウトを行うバッチ
"""

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'production'

import argparse
import datetime
import gzip
import os
import shutil
import csv
import json
import urlparse
import lib.util         as util
import lib.mapping.logparse as logparse
import lib.mappingtable as mappingtable
from conf.config      import BatchConfig
from lib.lockfile     import LockFile
from lib.s3util       import S3Util

class RtOptout:

    S3_RT_OPTOUT_DIR = 'optout/rt/{sid}/{year}/{month}/{day}/'

    def __init__(self, sid, env, target_day=None):
        self.sid = sid
        self.target_day = target_day

        self.config = BatchConfig(env)
        self.env    = self.config.environment

        if target_day:
            target_datetime = datetime.datetime.strptime(self.target_day, '%Y%m%d')
        else:
            target_datetime = datetime.datetime.now() - datetime.timedelta(days=1)

        optout_info = {
            'sid'  : self.sid,
            'year' : target_datetime.strftime('%Y'),
            'month': target_datetime.strftime('%m'),
            'day'  : target_datetime.strftime('%d')
        }
        self.s3_log_bucket    = self.config.s3bucket_partner_log
        self.s3_rt_optout_dir = self.S3_RT_OPTOUT_DIR.format(**optout_info)
        self.local_base_dir   = self.config.tmp_optout_dir.format(target='rt')
        self.local_log_dir    = os.path.join(self.local_base_dir, 'log')

        self.logger    = self.config.logger_mapping
        self.logprefix = '[RtOptout {sid} {day}] '.format(sid=sid,
                                                          day=target_datetime.strftime('%Y%m%d'))

    def execute(self):
        self.logger.info(self.logprefix + '<Start optout>')

        # lock
        lock_key = 'optout_{sid}'.format(sid=self.sid)
        lock_file = LockFile(lock_key, self.env)
        log_files = []
        if not lock_file.lock():
            self.logger.warn(self.logprefix + 'Still running')
            self.logger.info(self.logprefix + '<End optout>')
            return

        try:
            # initialize work directory
            self.logger.info(self.logprefix + 'Initializing work directory')
            shutil.rmtree(self.local_log_dir, ignore_errors=True)
            os.makedirs(self.local_log_dir)

            # download logs
            self.logger.info(self.logprefix + 'Downloading log files')
            s3 = S3Util()
            keys = s3.download_files(self.s3_log_bucket, self.s3_rt_optout_dir, self.local_log_dir)
            if not keys:
                self.logger.info(self.logprefix + 'No log files')
                return

            # delete
            count = 0
            pids = self.config.mapping[self.sid].keys()
            log_files = [os.path.join(self.local_log_dir, f) for f in os.listdir(self.local_log_dir)]
            log_files = [f for f in log_files if os.path.isfile(f)]
            if log_files:
                for pid in pids:
                    if pid == 'co' or self.config.partner_mst[pid]['option'].get('no_mapping'):
                        continue
                    uids = []
                    for log_file in log_files:
                        for sid, uid in logparse.yield_from_fluentd(log_file, 'sid', 'uid'):
                            if sid != self.sid:
                                continue
                            uids.append(uid)
                    with mappingtable.open(self.sid, pid, self.config.database[self.sid]) as mapping:
                        count += mapping.delete_rows_by_user_ids(uids)

            self.logger.info(self.logprefix + '{count} user mappings have been deleted.'.format(count=count))

        except Exception, e:
            self.logger.exception(self.logprefix + 'Failed to optout')
            raise e

        finally:
            # unlock
            lock_file.unlock()
            self.logger.info(self.logprefix + '<End optout>')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Execute rt_optout')
    parser.add_argument('sid', type=str, help='site_id')
    parser.add_argument('env', type=str, choices=('prd', 'stg', 'dev'), help='environment')
    parser.add_argument('-t', '--target_day', type=str, help='target day(format: yyyymmdd)')
    args = parser.parse_args()

    optout = RtOptout(sid=args.sid,
                      env=args.env,
                      target_day=args.target_day)
    optout.execute()
