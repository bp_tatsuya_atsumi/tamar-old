# -*- coding: utf-8 -*-

from boto.ses import SESConnection


class SESUtil(object):
    REGION         = 'ap-northeast-1'
    AWS_ACCESS_KEY = 'AKIAJANBBP3TGSIL2ULQ'
    AWS_SECRET_KEY = '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'

    SEND_ENVS      = ['prd']
    FROM_ADDRESS   = 'coven-alert@brainpad.co.jp'
    TO_ADDRESS     = 'coven-alert@brainpad.co.jp'

    def __init__(self, env):
        self.env  = env
        self.conn = SESConnection(self.AWS_ACCESS_KEY, self.AWS_SECRET_KEY)

    def send_mail(self, subject, body):
        if not self.env in self.SEND_ENVS:
            return
        self.conn.send_email(source=self.FROM_ADDRESS,
                             subject=subject,
                             body=body,
                             to_addresses=self.TO_ADDRESS,
                             format='text',
                             reply_addresses=self.TO_ADDRESS,
                             return_path=self.FROM_ADDRESS)
