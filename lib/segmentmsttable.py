# -*- coding: utf-8 -*-

"""
OR Mapper for master data of segments.
This class uses a table formatted with 't_segmentmst_from_{seg_from}_to_{seg_to} .
"""

__author__  = 'Hitoshi Tsuda'
__status__  = 'production'
__version__ = '1.0.0'
__date__    = '2014/6/9'

import datetime
import MySQLdb
from   sqlalchemy                import bindparam, create_engine, text
from   sqlalchemy                import Table, Column, MetaData
from   sqlalchemy.dialects.mysql import TINYINT, VARCHAR
from   sqlalchemy.sql            import select


TABLE_NAME = 't_segmentmst_from_{seg_from}_to_{seg_to}'

def open(seg_from, seg_to, db_config):
    return SegmentMstTable(db_config['user'],
                           db_config['password'],
                           db_config['host'],
                           db_config['db_name'],
                           TABLE_NAME.format(seg_from=seg_from, seg_to=seg_to))

class SegmentMstTable():

    def __enter__(self):
        self.trans = self.conn.begin()
        return self

    def __exit__(self, exec_type, exec_value, traceback):
        if not exec_type:
            self.trans.commit()
            return True
        else:
            self.trans.rollback()
            return False

    def __del__(self):
        self.conn.close()

    def __init__(self, user, password, host, db_name, table_name):
        engine_str = 'mysql://{user}:{password}@{host}/{db_name}?charset=utf8'\
                        .format(user=user,
                                password=password,
                                host=host,
                                db_name=db_name)

        self.engine = create_engine(engine_str, encoding='utf8')
        self.metadata = MetaData()

        self.table = Table(table_name, self.metadata,
                           Column('segment_id_from', VARCHAR(length=50), primary_key=True),
                           Column('segment_id_to',   VARCHAR(length=50)),
                           Column('name',            VARCHAR(length=50)),
                           Column('description',     VARCHAR(length=1000)),
                           Column('status',          TINYINT))
        self.table_alias = self.table.alias('t')
        self.metadata.create_all(self.engine)
        self.conn = self.engine.connect()
        self.conn.execute("SET TIME_ZONE = '+9:00'")

    def select_row_by_segment_id(self, segment_id_from):
        select_statement = select([self.table])\
                            .where(self.table.c.segment_id_from == segment_id_from)
        return self.conn.execute(select_statement).fetchone()

    def select_all_rows(self):
        select_statement = select([self.table])
        return self.conn.execute(select_statement).fetchall()

    def update_row_by_segment_id(self, segment_id_from, values):
        update_statement = self.table.update()\
                           .where(self.table.c.segment_id_from == segment_id_from)\
                           .values(**values)
        result = self.conn.execute(update_statement)
        return result.rowcount

    def insert_row(self, segment_id_from, segment_id_to, name, description="", status=0):
        ins = self.table.insert().values(
                    segment_id_from=segment_id_from,
                    segment_id_to=segment_id_to,
                    name=name,
                    description=description,
                    status=status
              )
        result = self.conn.execute(ins)
        return result.is_insert

    def _delete_rows_by_ids(self, target_column, ids):
        count = 0
        for id in ids:
            delete = self.table.delete().where(target_column == id)
            result = self.conn.execute(delete)
            count += result.rowcount
        return count

    def delete_rows_by_segment_id(self, segment_id_from):
        return self._delete_rows_by_ids(self.table.c.segment_id_from, [segment_id_from])

    def delete_rows_by_segment_ids(self, segment_ids):
        return self._delete_rows_by_ids(self.table.c.segment_id_from, segment_ids)

    def truncate_table(self):
        trans = self.conn.begin()
        self.conn.execute(self.table.delete())
        trans.commit()
        return True

