# -*- coding: utf-8 -*-

import datetime
import MySQLdb
from   sqlalchemy                import bindparam, create_engine, text
from   sqlalchemy                import Table, Column, MetaData
from   sqlalchemy.dialects.mysql import BOOLEAN, DATETIME, TINYINT, VARCHAR
from   sqlalchemy.sql            import select


TABLE_NAME = 't_user_extsegdata_{sid}_{pid}'

def open(sid, pid, db_config):
    return ExtSegDataTable(db_config['user'],
                           db_config['password'],
                           db_config['host'],
                           db_config['db_name'],
                           TABLE_NAME.format(sid=sid, pid=pid))

class ExtSegDataTable():

    def __enter__(self):
        self.trans = self.conn.begin()
        return self

    def __exit__(self, exec_type, exec_value, traceback):
        if not exec_type:
            self.trans.commit()
            return True
        else:
            self.trans.rollback()
            return False

    def __del__(self):
        self.conn.close()

    def __init__(self, user, password, host, db_name, table_name):
        engine_str = 'mysql://{user}:{password}@{host}/{db_name}?charset=utf8'\
                        .format(user=user,
                                password=password,
                                host=host,
                                db_name=db_name)

        self.engine = create_engine(engine_str, encoding='utf8')
        self.metadata = MetaData()

        self.table_name = table_name
        self.table = Table(table_name, self.metadata,
                           Column('user_id',    VARCHAR(length=50), primary_key=True),
                           Column('segment_id', VARCHAR(length=50), primary_key=True),
                           Column('ttl',        DATETIME))
        self.table_alias = self.table.alias('t')
        self.metadata.create_all(self.engine)
        self.conn = self.engine.connect()
        self.conn.execute("SET TIME_ZONE = '+9:00'")

    def select_all_rows(self):
        select_statement = select([self.table])
        return self.conn.execute(select_statement).fetchall()

    def insert_row(self, user_id, segment_id, ttl=None):
        ins = self.table.insert().values(
                    user_id=user_id,
                    segment_id=segment_id,
                    ttl=ttl
              )
        result = self.conn.execute(ins)
        return result.is_insert

    def truncate_table(self):
        trans = self.conn.begin()
        self.conn.execute(self.table.delete())
        trans.commit()
        return True
