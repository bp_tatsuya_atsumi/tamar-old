# -*- coding: utf-8 -*-

"""
access_logからマッピングテーブルを構成するためのモジュール
"""

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'production'
__version__ = '1.0.0'
__date__    = '2014/6/2'

import argparse
import csv
import gzip
import json
import urlparse
import os
import re
from   conf.config import BatchConfig


TAMAR_USER_TYPE_1ST_PARTY_COOKIE = '1'
TAMAR_USER_TYPE_3RD_PARTY_COOKIE = '2'

def parse(sid, pid, env, log_dir, csv_dir):
    """
    ログのパース処理を実行する関数
    """

    # initialize
    config = BatchConfig(env)
    crypto = config.crypto

    # read log under directory
    log_files = [os.path.join(log_dir, f) for f in os.listdir(log_dir)]
    log_files = [f for f in log_files if os.path.isfile(f)]
    for log_file in log_files:

        # create csv filename
        # e.g. log/2013-09-26-20_ip-10-0-16-6_0.gz
        #   -> csv/2013-09-26-20_ip-10-0-16-6_0.csv
        log_filename = os.path.basename(log_file)
        log_filename_root, log_filename_ext = os.path.splitext(log_filename)
        csv_filename = log_filename_root + '.csv'
        csv_file = os.path.join(csv_dir, csv_filename)

        # parse and write csv
        if pid == 'co':
            with open(csv_file, mode='w') as outf:
                writer = _create_csv_writer(outf)
                for couid, uid in yield_from_fluentd_for_co(log_file, 'uid'):
                    try:
                        decrypted_uid = crypto.decrypt(uid)
                        if not decrypted_uid or not _is_utf8_str(decrypted_uid):
                            continue
                    except Exception:
                       continue
                    writer.writerow([
                                        decrypted_uid,
                                        couid,
                                        TAMAR_USER_TYPE_3RD_PARTY_COOKIE,
                                        couid
                                    ])
        elif pid == 'rs':
            with open(csv_file, mode='w') as outf:
                writer = _create_csv_writer(outf)
                for uid, pt_uid in yield_from_fluentd(log_file, 'uid', 'pt_uid'):
                    try:
                        writer.writerow([
                                            uid,
                                            pt_uid,
                                            TAMAR_USER_TYPE_1ST_PARTY_COOKIE,
                                            uid
                                        ])
                    except Exception:
                        continue
        elif pid == 'go':
            with open(csv_file, mode='w') as outf:
                writer = _create_csv_writer(outf)
                for uid, google_id in yield_from_fluentd(log_file, 'uid', 'google_gid'):
                    try:
                        decrypted_uid = crypto.decrypt(uid)
                        if not decrypted_uid or not _is_utf8_str(decrypted_uid):
                            continue
                    except Exception:
                        continue
                    writer.writerow([
                                        decrypted_uid,
                                        google_id,
                                        TAMAR_USER_TYPE_1ST_PARTY_COOKIE,
                                        decrypted_uid
                                    ])
        else:
            with open(csv_file, mode='w') as outf:
                writer = _create_csv_writer(outf)
                for uid, pt_uid in yield_from_fluentd(log_file, 'uid', 'pt_uid'):
                    try:
                        decrypted_uid = crypto.decrypt(uid)
                        if not decrypted_uid or not _is_utf8_str(decrypted_uid):
                            continue
                    except Exception:
                        continue
                    writer.writerow([
                                        decrypted_uid,
                                        pt_uid,
                                        TAMAR_USER_TYPE_1ST_PARTY_COOKIE,
                                        decrypted_uid
                                    ])

def _is_utf8_str(s):
    """
    エンコーディングがUTF-8かどうかを判定する関数
    """
    try:
        if isinstance(s, str):
            s.encode('utf-8')
        elif isinstance(s, unicode):
            s.decode('utf-8')
        else:
            return False
    except:
        return False
    else:
        return True

def _create_csv_writer(fp):
    """
    与えられたファイルポインタに関して
    CSVのwriterを作成する関数
    """
    return csv.writer(fp,
                      lineterminator='\n',
                      quoting=csv.QUOTE_NONE,
                      escapechar='\\')

def yield_from_fluentd(log_file, *keys):
    """
    fluentdのログからパラメータを抽出する関数
    """

    def __parse_json_line(json_line):
        path  = json.loads(json_line)['path']
        query = urlparse.urlparse(path).query

        parsed_query = urlparse.parse_qs(query)
        for key in keys:
            if key not in parsed_query:
                return None
        return ([ str(parsed_query[key][0]) for key in keys ])

    def __return_yield(log_file):
        with gzip.open(log_file, mode='rt') as inf:
            for row in csv.reader(inf, delimiter='\t'):
                if len(row) != 3:
                    continue
                r = __parse_json_line(row[2])
                if not r:
                    continue
                yield r

    return __return_yield(log_file)


def yield_from_fluentd_for_co(log_file, *keys):
    """
    fluentdのログからCrossOvenのidを取得する
    """

    def __parse_json_line(json_line):
        json_obj  = json.loads(json_line)
        json_keys = json_obj.keys()
        if 'setuid' not in json_keys or 'getuid' not in json_keys:
            return None
        
        path     = json_obj['path']
        uid_get  = json_obj['getuid']

        query        = urlparse.urlparse(path).query
        parsed_query = urlparse.parse_qs(query)
        for key in keys:
            if key not in parsed_query:
                return None

        coid = __get_coid(uid_get)
        if coid is None:
            return None

        values = [coid]
        values.extend([ str(parsed_query[key][0]) for key in keys ])
        return values

    def __get_coid(uid_get):
        re_coid = re.compile('co\.uid=(.*)')
        return re_coid.match(uid_get).groups()[0] if uid_get and re_coid.match(uid_get) else None

    def __return_yield(log_file):
        with gzip.open(log_file, mode='rt') as inf:
            for row in csv.reader(inf, delimiter='\t'):
                if len(row) != 3:
                    continue
                r = __parse_json_line(row[2])
                if not r:
                    continue
                yield r

    return __return_yield(log_file)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Parse log and make csv')
    parser.add_argument('sid', type=str, help='site_id')
    parser.add_argument('pid', type=str, help='partner_id')
    parser.add_argument('env', type=str, choices=('prd', 'stg', 'dev'), help='environment')
    parser.add_argument('log_dir', type=str, help='log directory')
    parser.add_argument('csv_dir', type=str, help='csv directory')
    args = parser.parse_args()

    parse(sid=args.sid,
          pid=args.pid,
          env=args.env,
          log_dir=args.log_dir,
          csv_dir=args.csv_dir)
