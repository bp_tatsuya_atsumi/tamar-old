# -*- coding: utf-8 -*-

import __builtin__
import errno
import os
import stat
import tempfile
import paramiko


def open(host, username, port=22, password=None, pkey_file=None):
    return SftpUtil(host, username, port, password, pkey_file)

def listdir(host, username, remote_dir, port=22, password=None, pkey_file=None, only_file=False, only_dir=False):
    with SftpUtil(host, username, port, password, pkey_file) as sftp:
        return sftp.listdir(remote_dir, only_file, only_dir)

def exists(host, username, remote_path, port=22, password=None, pkey_file=None):
    with SftpUtil(host, username, port, password, pkey_file) as sftp:
        return sftp.exists(remote_path)

def get(host, username, remote_file, local_dir, port=22, password=None, pkey_file=None):
    with SftpUtil(host, username, port, password, pkey_file) as sftp:
        sftp.get(remote_file, local_dir)

def put(host, username, local_file, remote_dir, port=22, password=None, pkey_file=None, rename=True, tmp_filename=None):
    with SftpUtil(host, username, port, password, pkey_file) as sftp:
        sftp.put(local_file, remote_dir, rename, tmp_filename)

def touch(host, username, remote_file, content='', port=22, password=None, pkey_file=None):
    with SftpUtil(host, username, port, password, pkey_file) as sftp:
        sftp.touch(remote_file, content)

def delete(host, username, remote_file, port=22, password=None, pkey_file=None):
    with SftpUtil(host, username, port, password, pkey_file) as sftp:
        sftp.delete(remote_file)


class SftpUtil(object):

    def __init__(self, host, username, port=22, password=None, pkey_file=None):
        self.host      = host
        self.username  = username
        self.port      = port
        self.password  = password
        self.pkey_file = pkey_file

    def __enter__(self):
        self.connect()
        return self

    def __exit__(self, exec_type, exec_value, traceback):
        self.close()

    def connect(self):
        self.transport = paramiko.Transport((self.host, self.port))
        if self.password:
            self.transport.connect(username=self.username, password=self.password)
        elif self.pkey_file:
            self.transport.connect(username=self.username, pkey=self._detect_pkey())
        else:
            raise ValueError('password and pkey_file is None')
        self.sftp = paramiko.SFTPClient.from_transport(self.transport)

    def _detect_pkey(self):
        pkeyf = __builtin__.open(self.pkey_file, 'r')
        pkey_head = pkeyf.readline()
        if 'RSA' in pkey_head:
            pkey_cls = paramiko.RSAKey
        elif 'DSA' in pkey_head:
            pkey_cls = paramiko.DSSKey
        else:
            raise ValueError('cannot identify private key type')
        pkeyf.seek(0)
        return pkey_cls.from_private_key(pkeyf)

    def close(self):
        try:
            self.transport.close()
        except:
            pass

    def listdir(self, remote_dir, only_file=False, only_dir=False):
        if only_file and only_dir:
            raise ValueError('Both only_file and only_dir are True')
        def _mode_filter(mode):
            if only_file:
                return stat.S_ISREG(mode)
            if only_dir:
                return stat.S_ISDIR(mode)
            return True
        return [attr.filename for attr in self.sftp.listdir_attr(remote_dir)
                    if _mode_filter(attr.st_mode)]

    def exists(self, remote_path):
        try:
            self.sftp.stat(remote_path)
        except IOError, e:
            if e.errno == errno.ENOENT:
                return False
            raise e
        else:
            return True

    def get(self, remote_file, local_dir):
        local_file = os.path.join(local_dir, os.path.basename(remote_file))
        if os.path.exists(local_file):
            raise Exception('Local file already exists: ' + local_file)

        if not self.exists(remote_file):
            raise Exception('Remote file not exists: ' + remote_file)

        self.sftp.get(remote_file, local_file)

    def put(self, local_file, remote_dir, rename=True, tmp_filename=None):
        if not os.path.exists(local_file):
            raise Exception('Local file not exists: ' + local_file)

        current_dir = self.sftp.getcwd()
        try:
            self.sftp.chdir(remote_dir)
        except:
            self.sftp.mkdir(remote_dir)
        self.sftp.chdir(current_dir)

        local_filename = os.path.basename(local_file)
        remote_file = os.path.join(remote_dir, local_filename)
        if self.exists(remote_file):
            raise Exception('Remote file already exists: ' + remote_file)

        if rename:
            if tmp_filename:
                remote_tmp_file = os.path.join(remote_dir, tmp_filename)
            else:
                remote_tmp_file = os.path.join(remote_dir, '.' + local_filename)
            if self.exists(remote_tmp_file):
                raise Exception('Remote tmpfile already exists: ' + remote_tmp_file)
            self.sftp.put(local_file, remote_tmp_file, confirm=True)
            self.sftp.rename(remote_tmp_file, remote_file)
        else:
            self.sftp.put(local_file, remote_file, confirm=True)

    def touch(self, remote_file, content=''):
        with tempfile.NamedTemporaryFile() as tempf:
            tempf.write(content)

            if self.exists(remote_file):
                raise Exception('Remote file already exists: ' + remote_file)
            self.sftp.put(tempf.name, remote_file, confirm=True)

    def delete(self, remote_file):
        if not self.exists(remote_file):
            raise Exception('Remote file not exists: ' + remote_file)
        self.sftp.remove(remote_file)
