# -*- coding: utf-8 -*-

from __future__ import absolute_import

import argparse
import importlib
import logging
from   multiprocessing              import Process
import os
import signal
import string
import time
import traceback
import boto.swf
import boto.swf.layer2              as     swf
from   daemon                       import DaemonContext
from   daemon.pidfile               import PIDLockFile
from   conf.config                  import BatchConfig
import lib.aws.credentials          as     credentials
import lib.const                    as     const
from   lib.workflow.decider.helper  import DecisionHelper, EventHelper
import lib.workflow.util            as     util


class DecisionTaskPoller(swf.Decider):

    #---------------------------------------------------------------------------
    #   Initialize
    #---------------------------------------------------------------------------
    def __init__(self, env):
        self.config  = BatchConfig(env)
        self.env     = self.config.environment
        self.domain  = self.config.workflow['domain']
        self.version = self.config.workflow['version']
        self._swf    = boto.swf.connect_to_region(
                           region_name=credentials.AWS_REGION,
                           aws_access_key_id=credentials.AWS_ACCESS_KEY,
                           aws_secret_access_key=credentials.AWS_SECRET_KEY
                       )
        self.logger  = self.config.get_prefixed_workflow_decision_logger(
                           self.__class__.__name__,
                           '[{0}] '.format(self.__class__.__name__)
                       )

        self.wftype_to_decider = self.config.workflow['wftype_to_decider']
        self.stop_requested    = False
        self.reload_requested  = False

    #---------------------------------------------------------------------------
    #   Poll
    #---------------------------------------------------------------------------
    def start(self, task_list):
        self.logger.info('Poll started: domain=%s, version=%s, task_list=%s',
                         self.domain, self.version, task_list)

        while True:
            if self.stop_requested:
                break

            if self.reload_requested:
                self.logger.info('Reloading')
                self.__init__(self.env)
                self.logger.info('Reloaded')

            self.logger.debug('Poll task started: task_list=%s', task_list)
            self._run(task_list)
            self.logger.debug('Poll task completed: task_list=%s', task_list)

        self.logger.info('Poll end: domain=%s, version=%s, task_list=%s',
                         self.domain, self.version, task_list)

    def stop(self):
        self.logger.info('Stop requested')
        self.stop_requested = True

    def reload(self):
        self.logger.info('Reload requested')
        self.reload_requested = True


    def _run(self, task_list):
        decision_task = self._poll(task_list)
        if not decision_task:
            return

        decisions       = swf.Layer1Decisions()
        event_helper    = EventHelper(decision_task)
        decision_helper = DecisionHelper(decisions, self.domain, self.version, task_list)

        wf_id           = event_helper.get_wf_id()
        wf_name         = event_helper.get_wf_name()
        event_id        = event_helper.get_event_id()
        event_type      = event_helper.get_event_type()

        try:
            self.logger.info('Decision started: wf_id=%s, event_id=%s, event_type=%s',
                             wf_id, event_id, event_type)
            decider = self._create_decider(wf_name, event_helper, decision_helper)
            decider.decide()

        except:
            self.logger.exception('Failed to execute decision: wf_id=%s, event_id=%s, event_type=%s',
                                  wf_id, event_id, event_type)
            try:
                reason  = 'decide failed'
                details = traceback.format_exc().splitlines()[-1]

                decision_helper.fail_workflow(reason=reason, details=details)

                self.logger.error('Failed to execute workflow: wf_id=%s, reason=%s, details="%s"',
                                  wf_id, reason, details)
            except:
                self.logger.exception('Failed to close workflow: wf_id=%s, event_id=%s, event_type=%s',
                                      wf_id, event_id, event_type)

        else:
            self.logger.info('Decision completed: wf_id=%s, event_id=%s, event_type=%s',
                             wf_id, event_id, event_type)
            try:
                self.complete(decisions=decisions)
            except:
                self.logger.exception('Failed to notify swf: wf_id=%s, event_id=%s, event_type=%s',
                                      wf_id, event_id, event_type)


    def _poll(self, task_list):
        def __poll(task_list, next_page_token=None):
            # boto v2.13.3 has problem: cannot set task_list argument
            # this problem fixed in v2.14.0
            _decision_task = self._swf.poll_for_decision_task(
                                 domain=self.domain,
                                 task_list=task_list,
                                 identity=util.create_poller_identity(),
                                 next_page_token=next_page_token
                             )
            self.last_tasktoken = _decision_task.get('taskToken')
            return _decision_task

        decision_task = __poll(task_list)
        if not 'events' in decision_task:
            return None

        next_page_token = decision_task.get('nextPageToken')
        while next_page_token:
            next_page = __poll(task_list, next_page_token)
            decision_task['events'].extend(next_page['events'])
            next_page_token = next_page.get('nextPageToken')

        return decision_task

    def _create_decider(self, wf_name, event_helper, decision_helper):
        if not wf_name in self.wftype_to_decider:
            raise ValueError('Unknown workflow type: {}'.format(wf_name))
        module_name = 'lib.workflow.decider.' + self.wftype_to_decider[wf_name]['module']
        class_name  = self.wftype_to_decider[wf_name]['class']

        decider_module = importlib.import_module(module_name)
        decider_class  = getattr(decider_module, class_name)
        return decider_class(self.env, event_helper, decision_helper)


    #---------------------------------------------------------------------------
    #   Daemonize
    #---------------------------------------------------------------------------
    def daemonize(self, pidfile=None):
        self.logger.info('=' * 80)
        self.logger.info('Daemon for poll decision task started')

        processes   = []
        pid_file    = PIDLockFile(pidfile) if pidfile else None
        stdout_file = open('/usr/local/tamar/log/workflow_decision.out', 'a+')
        stderr_file = open('/usr/local/tamar/log/workflow_decision.err', 'a+')

        def _func_handle_signal(signum, func_handle_signal_child):
            def _handle_signal(signum, frame):
                if os.getpid() == ppid:
                    for p in processes:
                        if p.is_alive():
                            self.logger.info('signal send to %d [%d]', p.pid, signum)
                            os.kill(p.pid, signum)
                    func_handle_signal_child()
                else:
                    func_handle_signal_child()
            return _handle_signal

        signal_map  = {
            signal.SIGTERM : _func_handle_signal(signal.SIGTERM, self.stop),
            signal.SIGHUP  : _func_handle_signal(signal.SIGHUP,  self.reload),
        }

        with DaemonContext(working_directory='.',
                           files_preserve=self._get_log_handler_streams(),
                           pidfile=pid_file,
                           stdout=stdout_file,
                           stderr=stderr_file,
                           signal_map=signal_map):

            ppid = os.getpid()
            tasklist_to_processnum = self.config.workflow['process_configs']['decider']
            tasklist_to_processnum['default'] = tasklist_to_processnum['default'] - 1
            for task_list, process_num in tasklist_to_processnum.items():
                for i in range(process_num):
                    p = Process(target=self.start,
                                args=(task_list,),
                                name=self._get_process_name(task_list, i + 1))
                    p.start()
                    processes.append(p)
                    time.sleep(1)

            self.start('default')
            for p in processes:
                if p.is_alive():
                    p.join()

        self.logger.info('Daemon for poll decision task end')
        self.logger.info('=' * 80)


    def _get_process_name(self, task_list, seqno):
        # aaa_bbb -> AaaBbb
        return '{}-{:d}'.format(
                   string.capwords(task_list, '_').replace('_', ''),
                   seqno
               )

    def _get_log_handler_streams(self):
        streams = []
        for logger_name in ['extsegdata', 'mapping', 'transfer', 'workflow_decision', 'workflow_activity']:
            streams.extend([
                h.stream
                for h in logging.getLogger(logger_name).handlers
                if isinstance(h, logging.StreamHandler)
            ])
        return streams


#---------------------------------------------------------------------------
#   Commandline function
#---------------------------------------------------------------------------
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='poll decision task')
    parser.add_argument('env', type=str, choices=const.ENVS, help='environment')
    parser.add_argument('--pidfile', type=str, help='pidfile')
    args = parser.parse_args()

    DecisionTaskPoller(args.env).daemonize(args.pidfile)
