# -*- coding: utf-8 -*-

from __future__ import absolute_import

import argparse
import importlib
import json
import logging
from   multiprocessing              import Process
import os
import signal
import string
import time
import traceback
import boto.swf
import boto.swf.layer2     as     swf
from   daemon              import DaemonContext
from   daemon.pidfile      import PIDLockFile
from   conf.config         import BatchConfig
import lib.aws.credentials as     credentials
import lib.const           as     const
import lib.workflow.util   as     util


class ActivityTaskPoller(swf.ActivityWorker):

    #---------------------------------------------------------------------------
    #   Initialize
    #---------------------------------------------------------------------------
    def __init__(self, env):
        self.config  = BatchConfig(env)
        self.env     = self.config.environment
        self.domain  = self.config.workflow['domain']
        self.version = self.config.workflow['version']
        self._swf    = boto.swf.connect_to_region(
                           region_name=credentials.AWS_REGION,
                           aws_access_key_id=credentials.AWS_ACCESS_KEY,
                           aws_secret_access_key=credentials.AWS_SECRET_KEY
                       )
        self.logger  = self.config.get_prefixed_workflow_activity_logger(
                           self.__class__.__name__,
                           '[{0}] '.format(self.__class__.__name__)
                       )

        self.acttype_to_activityworker = self.config.workflow['acttype_to_activityworker']
        self.stop_requested            = False
        self.reload_requested          = False

    #---------------------------------------------------------------------------
    #   Poll
    #---------------------------------------------------------------------------
    def start(self, task_list):
        self.logger.info('Poll started: domain=%s, version=%s, task_list=%s',
                         self.domain, self.version, task_list)

        while True:
            if self.stop_requested:
                break

            if self.reload_requested:
                self.logger.info('Reloading')
                self.__init__(self.env)
                self.logger.info('Reloaded')

            self.logger.debug('Poll task started: task_list=%s', task_list)
            self._run(task_list)
            self.logger.debug('Poll task completed: task_list=%s', task_list)

        self.logger.info('Poll end: domain=%s, version=%s, task_list=%s',
                         self.domain, self.version, task_list)

    def stop(self):
        self.logger.info('Stop requested')
        self.stop_requested = True

    def reload(self):
        self.logger.info('Reload requested')
        self.reload_requested = True


    def _run(self, task_list):
        act_task = self._poll(task_list)
        if not act_task:
            return

        act_id    = act_task['activityId']
        act_name  = act_task['activityType']['name']
        act_input = json.loads(act_task['input'])

        try:
            self.logger.info('Activity started: act_id=%s', act_id)
            result = self._create_worker(act_name, act_input).work()

        except:
            self.logger.exception('Activity failed: act_id=%s', act_id)
            try:
                self.fail(reason='activity task failed',
                          details=traceback.format_exc().splitlines()[-1])
            except:
                self.logger.exception('Notify SWF failed: act_id=%s', act_id)

        else:
            self.logger.info('Activity completed: act_id=%s', act_id)
            try:
                self.complete(result=json.dumps(result, separators=(',', ':')))
            except:
                self.logger.exception('Notify SWF failed: act_id=%s', act_id)


    def _poll(self, task_list):
        # boto v2.13.3 has problem: cannot set task_list argument
        # this problem fixed in v2.14.0
        act_task = self._swf.poll_for_activity_task(
                       domain=self.domain,
                       task_list=task_list,
                       identity=util.create_poller_identity()
                   )
        if not 'activityId' in act_task:
            return

        self.last_tasktoken = act_task.get('taskToken')
        return act_task

    def _create_worker(self, act_name, act_input):
        if not act_name in self.acttype_to_activityworker:
            raise ValueError('Unknown activity type: {}'.format(act_name))
        module_name = 'lib.workflow.activity_worker.' + self.acttype_to_activityworker[act_name]['module']
        class_name  = self.acttype_to_activityworker[act_name]['class']

        worker_module = importlib.import_module(module_name)
        worker_class  = getattr(worker_module, class_name)
        return worker_class.from_act_input(act_input)


    #---------------------------------------------------------------------------
    #   Daemonize
    #---------------------------------------------------------------------------
    def daemonize(self, pidfile=None):
        self.logger.info('=' * 80)
        self.logger.info('Daemon for poll activity task started')

        processes   = []
        pid_file    = PIDLockFile(pidfile) if pidfile else None
        stdout_file = open('/usr/local/tamar/log/workflow_activity.out', 'a+')
        stderr_file = open('/usr/local/tamar/log/workflow_activity.err', 'a+')

        def _func_handle_signal(signum, func_handle_signal_child):
            def _handle_signal(signum, frame):
                if os.getpid() == ppid:
                    for p in processes:
                        if p.is_alive():
                            self.logger.info('signal send to %d [%d]', p.pid, signum)
                            os.kill(p.pid, signum)
                    func_handle_signal_child()
                else:
                    func_handle_signal_child()
            return _handle_signal

        signal_map  = {
            signal.SIGTERM : _func_handle_signal(signal.SIGTERM, self.stop),
            signal.SIGHUP  : _func_handle_signal(signal.SIGHUP,  self.reload),
        }

        with DaemonContext(working_directory='.',
                           files_preserve=self._get_log_handler_streams(),
                           pidfile=pid_file,
                           stdout=stdout_file,
                           stderr=stderr_file,
                           signal_map=signal_map):

            ppid = os.getpid()
            tasklist_to_processnum = self.config.workflow['process_configs']['activity_worker']
            tasklist_to_processnum['default'] = tasklist_to_processnum['default'] - 1
            for task_list, process_num in tasklist_to_processnum.items():
                for i in range(process_num):
                    p = Process(target=self.start,
                                args=(task_list,),
                                name=self._get_process_name(task_list, i + 1))
                    p.start()
                    processes.append(p)
                    time.sleep(1)

            self.start('default')
            for p in processes:
                if p.is_alive():
                    p.join()

        self.logger.info('Daemon for poll activity task end')
        self.logger.info('=' * 80)


    def _get_process_name(self, task_list, seqno):
        # aaa_bbb -> AaaBbb
        return '{}-{:d}'.format(
                   string.capwords(task_list, '_').replace('_', ''),
                   seqno
               )

    def _get_log_handler_streams(self):
        streams = []
        for logger_name in ['extsegdata', 'mapping', 'transfer', 'workflow_decision', 'workflow_activity']:
            streams.extend([
                h.stream
                for h in logging.getLogger(logger_name).handlers
                if isinstance(h, logging.StreamHandler)
            ])
        return streams


#---------------------------------------------------------------------------
#   Commandline function
#---------------------------------------------------------------------------
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='poll activity task')
    parser.add_argument('env', type=str, choices=const.ENVS, help='environment')
    parser.add_argument('--pidfile', type=str, help='pidfile')
    args = parser.parse_args()

    ActivityTaskPoller(args.env).daemonize(args.pidfile)
