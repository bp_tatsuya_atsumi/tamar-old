# -*- coding: utf-8 -*-

from __future__ import absolute_import

import argparse
import datetime
import traceback
from   boto.exception      import SWFResponseError
import boto.swf
from   conf.config         import BatchConfig
import lib.aws.credentials as     credentials
import lib.const           as     const
import lib.workflow.util   as     util
import sys


#---------------------------------------------------------------------------
#   Mapping
#---------------------------------------------------------------------------
def start_mapping(env, target_hour=None):
    if not target_hour:
        target_datetime = datetime.datetime.now() - datetime.timedelta(hours=1)
        target_hour     = target_datetime.strftime('%Y%m%d%H')

    config     = BatchConfig(env)
    domain     = config.workflow['domain']
    version    = config.workflow['version']
    wf_type    = 'MappingMainWorkflow'
    wf_id      = util.create_workflow_id(wf_type, version)
    input_dict = {'env': env, 'target_hour': target_hour}

    return _start_workflow(config, domain, version, wf_type, wf_id, input_dict)

def start_divided_mapping(env, total_class, target_class, target_hour=None):
    if total_class <= 0 or target_class < 0 or target_class >= total_class:
        print "Invalid total_class or target_class setting..."
        sys.exit()
    if not target_hour:
        target_datetime = datetime.datetime.now() - datetime.timedelta(hours=1)
        target_hour     = target_datetime.strftime('%Y%m%d%H')

    config     = BatchConfig(env)
    domain     = config.workflow['domain']
    version    = config.workflow['version']
    wf_type    = 'MappingMainWorkflow'
    wf_id      = util.create_workflow_id(wf_type, version)
    input_dict = {'env': env, 'target_hour': target_hour, 
                  'total_class': total_class, 'target_class': target_class}

    return _start_workflow(config, domain, version, wf_type, wf_id, input_dict)


#---------------------------------------------------------------------------
#   Bridge
#---------------------------------------------------------------------------
def start_bridge(env, target_hour=None):
    if not target_hour:
        target_datetime = datetime.datetime.now() - datetime.timedelta(hours=2)
        target_hour     = target_datetime.strftime('%Y%m%d%H')

    config     = BatchConfig(env)
    domain     = config.workflow['domain']
    version    = config.workflow['version']
    wf_type    = 'BridgeMainWorkflow'
    wf_id      = util.create_workflow_id(wf_type, version)
    input_dict = {'env': env, 'target_hour': target_hour}

    return _start_workflow(config, domain, version, wf_type, wf_id, input_dict)


#---------------------------------------------------------------------------
#   ExtSegData
#---------------------------------------------------------------------------
def start_extsegdata(env, target_hour=None):
    if not target_hour:
        target_datetime = datetime.datetime.now() - datetime.timedelta(hours=1)
        target_hour     = target_datetime.strftime('%Y%m%d%H')

    config     = BatchConfig(env)
    domain     = config.workflow['domain']
    version    = config.workflow['version']
    wf_type    = 'ExtSegDataMainWorkflow'
    wf_id      = util.create_workflow_id(wf_type, version)
    input_dict = {'env': env, 'target_hour': target_hour}

    return _start_workflow(config, domain, version, wf_type, wf_id, input_dict)


#---------------------------------------------------------------------------
#   Optout RT
#---------------------------------------------------------------------------
def start_optout_rt(env, target_day=None):
    if not target_day:
        target_datetime = datetime.datetime.now() - datetime.timedelta(days=1)
        target_day      = target_datetime.strftime('%Y%m%d')

    config     = BatchConfig(env)
    domain     = config.workflow['domain']
    version    = config.workflow['version']
    wf_type    = 'OptoutRTMainWorkflow'
    wf_id      = util.create_workflow_id(wf_type, version)
    input_dict = {'env': env, 'target_day': target_day}

    return _start_workflow(config, domain, version, wf_type, wf_id, input_dict)


#---------------------------------------------------------------------------
#   Optout CO
#---------------------------------------------------------------------------
def start_optout_co(env, target_day=None):
    if not target_day:
        target_datetime = datetime.datetime.now() - datetime.timedelta(days=1)
        target_day      = target_datetime.strftime('%Y%m%d')

    config     = BatchConfig(env)
    domain     = config.workflow['domain']
    version    = config.workflow['version']
    wf_type    = 'OptoutCOMainWorkflow'
    wf_id      = util.create_workflow_id(wf_type, version)
    input_dict = {'env': env, 'target_day': target_day}

    return _start_workflow(config, domain, version, wf_type, wf_id, input_dict)


#---------------------------------------------------------------------------
#   Mapping report
#---------------------------------------------------------------------------
def start_mappingreport(env):
    config     = BatchConfig(env)
    domain     = config.workflow['domain']
    version    = config.workflow['version']
    wf_type    = 'MappingReportMainWorkflow'
    wf_id      = util.create_workflow_id(wf_type, version)
    input_dict = {'env': env}

    return _start_workflow(config, domain, version, wf_type, wf_id, input_dict)


#---------------------------------------------------------------------------
#   Transfer
#---------------------------------------------------------------------------
def start_transfer(sid, env, data_type, s3bucket, s3file):
    config     = BatchConfig(env)
    domain     = config.workflow['domain']
    version    = config.workflow['version']
    wf_type    = config.workflow['receivedatatype_to_mainwftype'].get(data_type)
    if not wf_type:
        raise ValueError('Unknown data_type: {}'.format(data_type))
    wf_id      = util.create_workflow_id(wf_type, version, sid)
    input_dict = {
        'sid'       : sid,
        'env'       : env,
        'data_type' : data_type,
        's3bucket'  : s3bucket,
        's3file'    : s3file,
    }

    # transfer deciderを新環境で起動させる。
    task_list = 'tl_default' if sid == '0001' else None

    return _start_workflow(config, domain, version, wf_type, wf_id, input_dict,
                           task_list=task_list)


#---------------------------------------------------------------------------
#   Transfer latest
#---------------------------------------------------------------------------
def start_transferlatest(sid, pid, env, data_type):
    config     = BatchConfig(env)
    domain     = config.workflow['domain']
    version    = config.workflow['version']
    wf_type    = config.workflow['receivedatatype_to_mainlatestwftype'].get(data_type)
    if not wf_type:
        raise ValueError('Unknown data_type: {}'.format(data_type))
    wf_id      = util.create_workflow_id(wf_type, version, sid, pid)
    input_dict = {
        'sid'       : sid,
        'pid'       : pid,
        'env'       : env,
        'data_type' : data_type,
    }

    return _start_workflow(config, domain, version, wf_type, wf_id, input_dict)


#---------------------------------------------------------------------------
#   TombSend
#---------------------------------------------------------------------------
def start_tombsend(env):
    config     = BatchConfig(env)
    domain     = config.workflow['domain']
    version    = config.workflow['version']
    wf_type    = 'TombSendMainWorkflow'
    wf_id      = util.create_workflow_id(wf_type, version)
    input_dict = {'env': env}

    return _start_workflow(config, domain, version, wf_type, wf_id, input_dict)


#---------------------------------------------------------------------------
#   Rawdata
#---------------------------------------------------------------------------
def start_rawdata_process(env, data_type, s3bucket, s3file):
    config     = BatchConfig(env)
    domain     = config.workflow['domain']
    version    = config.workflow['version']
    wf_type    = config.workflow['receivedatatype_to_mainwftype'].get(data_type)
    if not wf_type:
        raise ValueError('Unknown data_type: {}'.format(data_type))
    wf_id      = util.create_workflow_id(wf_type, version)
    input_dict = {
        'env'       : env,
        'data_type' : data_type,
        's3bucket'  : s3bucket,
        's3file'    : s3file,
    }

    return _start_workflow(config, domain, version, wf_type, wf_id, input_dict)


#---------------------------------------------------------------------------
#   Private functions
#---------------------------------------------------------------------------
def _get_logger(config):
    return config.get_prefixed_workflow_decision_logger(__name__, '[Starter] ')

def _connect_to_swf():
    return boto.swf.connect_to_region(
               region_name=credentials.AWS_REGION,
               aws_access_key_id=credentials.AWS_ACCESS_KEY,
               aws_secret_access_key=credentials.AWS_SECRET_KEY
           )

def _start_workflow(config, domain, version, wf_type, wf_id, input_dict, task_list=None):
    logger = _get_logger(config)
    try:
        run_id = _connect_to_swf().start_workflow_execution(
                     domain=domain,
                     workflow_id=wf_id,
                     workflow_name=wf_type,
                     workflow_version=version,
                     input=util.dict_to_json(input_dict),
                     task_list=task_list
                 )['runId']

        logger.info('Workflows started: wf_id=%s, run_id=%s', wf_id, run_id)
        return run_id

    except SWFResponseError, e:
        logger.exception('Failed to start workflow: wf_name=%s, status=%s, message="%s"',
                         wf_type, e.status, e.body.get('message'))
        raise e
    except:
        logger.exception('Failed to start workflow: wf_name=%s, laststacktrace="%s"',
                         wf_type, traceback.format_exc().splitlines()[-1])
        raise


#---------------------------------------------------------------------------
#   Commandline functions
#---------------------------------------------------------------------------
def start_mapping_from_cli(args):
    start_mapping(args.env, args.target_hour)

def start_divided_mapping_from_cli(args):
    start_divided_mapping(args.env, args.total_class, args.target_class, args.target_hour)

def start_bridge_from_cli(args):
    start_bridge(args.env, args.target_hour)

def start_extsegdata_from_cli(args):
    start_extsegdata(args.env, args.target_hour)

def start_optout_rt_from_cli(args):
    start_optout_rt(args.env, args.target_day)

def start_optout_co_from_cli(args):
    start_optout_co(args.env, args.target_day)

def start_mappingreport_from_cli(args):
    start_mappingreport(args.env)

def start_transfer_from_cli(args):
    start_transfer(args.sid, args.env, args.data_type, args.s3bucket, args.s3file)

def start_transferlatest_from_cli(args):
    start_transferlatest(args.sid, args.pid, args.env, args.data_type)

def start_tombsend_from_cli(args):
    start_tombsend(args.env)

def start_rawdata_process_from_cli(args):
    start_rawdata_process(args.env, args.data_type, args.s3bucket, args.s3file)

if __name__ == '__main__':

    # create command
    parser = argparse.ArgumentParser()

    # create subcommands
    subparsers = parser.add_subparsers(help='subcommands')

    # create "start_mapping" command
    parser_mapping = subparsers.add_parser('mapping', help='start mapping workflow')
    parser_mapping.add_argument('env', type=str, choices=const.ENVS, help='environment')
    parser_mapping.add_argument('-t', '--target_hour', type=str, help='target hour(format: yyyymmddhh)')
    parser_mapping.set_defaults(func=start_mapping_from_cli)

    # create "start_divided_mapping" command
    parser_divided_mapping = subparsers.add_parser('divided_mapping', help='start divided mapping workflow')
    parser_divided_mapping.add_argument('env', type=str, choices=const.ENVS, help='environment')
    parser_divided_mapping.add_argument('total_class', type=int, help='total number of classes')
    parser_divided_mapping.add_argument('target_class', type=int, help='target class')
    parser_divided_mapping.add_argument('-t', '--target_hour', type=str, help='target hour(format: yyyymmddhh)')
    parser_divided_mapping.set_defaults(func=start_divided_mapping_from_cli)

    # create "start_bridge" command
    parser_mapping = subparsers.add_parser('bridge', help='start bridge workflow')
    parser_mapping.add_argument('env', type=str, choices=const.ENVS, help='environment')
    parser_mapping.add_argument('-t', '--target_hour', type=str, help='target hour(format: yyyymmddhh)')
    parser_mapping.set_defaults(func=start_bridge_from_cli)

    # create "start_extsegdata" command
    parser_mapping = subparsers.add_parser('extsegdata', help='start extsegdata workflow')
    parser_mapping.add_argument('env', type=str, choices=const.ENVS, help='environment')
    parser_mapping.add_argument('-t', '--target_hour', type=str, help='target hour(format: yyyymmddhh)')
    parser_mapping.set_defaults(func=start_extsegdata_from_cli)

    # create "start_optout_rt" command
    parser_mapping = subparsers.add_parser('optout_rt', help='start optout_rt workflow')
    parser_mapping.add_argument('env', type=str, choices=const.ENVS, help='environment')
    parser_mapping.add_argument('-t', '--target_day', type=str, help='target hour(format: yyyymmdd)')
    parser_mapping.set_defaults(func=start_optout_rt_from_cli)

    # create "start_optout_co" command
    parser_mapping = subparsers.add_parser('optout_co', help='start optout_co workflow')
    parser_mapping.add_argument('env', type=str, choices=const.ENVS, help='environment')
    parser_mapping.add_argument('-t', '--target_day', type=str, help='target hour(format: yyyymmdd)')
    parser_mapping.set_defaults(func=start_optout_co_from_cli)

    # create "start_mappingreport" command
    parser_mappingreport = subparsers.add_parser('mappingreport', help='start mappingreport workflow')
    parser_mappingreport.add_argument('env', type=str, choices=const.ENVS, help='environment')
    parser_mappingreport.set_defaults(func=start_mappingreport_from_cli)

    # create "start_transfer" command
    parser_transfer = subparsers.add_parser('transfer', help='start transfer workflow')
    parser_transfer.add_argument('sid', type=str, help='site_id')
    parser_transfer.add_argument('env', type=str, choices=const.ENVS, help='environment')
    parser_transfer.add_argument('data_type', type=str, choices=const.RECEIVE_DATATYPES, help='data type')
    parser_transfer.add_argument('s3bucket', type=str, help='s3bucekt')
    parser_transfer.add_argument('s3file', type=str, help='s3file')
    parser_transfer.set_defaults(func=start_transfer_from_cli)

    # create "start_transferlatest" command
    parser_transferlatest = subparsers.add_parser('transferlatest', help='start transfer latest workflow')
    parser_transferlatest.add_argument('sid', type=str, help='site_id')
    parser_transferlatest.add_argument('pid', type=str, help='partner_id')
    parser_transferlatest.add_argument('env', type=str, choices=const.ENVS, help='environment')
    parser_transferlatest.add_argument('data_type', type=str, choices=const.RECEIVE_DATATYPES, help='data type')
    parser_transferlatest.set_defaults(func=start_transferlatest_from_cli)

    # create "start_tombsend" command
    parser_tombsend = subparsers.add_parser('tombsend', help='start tombsend workflow')
    parser_tombsend.add_argument('env', type=str, choices=const.ENVS, help='environment')
    parser_tombsend.set_defaults(func=start_tombsend_from_cli)

    # create "start_rawdata_process" command
    parser_rawdata_process = subparsers.add_parser('rawdata', help='start create rawdata workflow')
    parser_rawdata_process.add_argument('env', type=str, choices=const.ENVS, help='environment')
    parser_rawdata_process.add_argument('data_type', type=str, choices=const.RECEIVE_DATATYPES, help='data type')
    parser_rawdata_process.add_argument('s3bucket', type=str, help='s3bucekt')
    parser_rawdata_process.add_argument('s3file', type=str, help='s3file')
    parser_rawdata_process.set_defaults(func=start_rawdata_process_from_cli)

    # parse argument
    args = parser.parse_args()

    # execute command
    args.func(args)
