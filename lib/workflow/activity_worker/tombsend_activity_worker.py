# -*- coding: utf-8 -*-

from __future__ import absolute_import

import tools.transferer.tombsender as tombsender


class TombSendActivityWorker(object):

    def __init__(self, sid, env):
        self.sid = sid
        self.env = env

    @staticmethod
    def from_act_input(act_input):
        return TombSendActivityWorker(sid=act_input['sid'],
                                      env=act_input['env'])

    def work(self):
        return tombsender.send_tomb(sid=self.sid, env=self.env)
