# -*- coding: utf-8 -*-

from __future__ import absolute_import


class DummyActivityWorker(object):

    @staticmethod
    def from_act_input(act_input):
        return DummyActivityWorker()

    def work(self):
        return None
