# -*- coding: utf-8 -*-

from __future__ import absolute_import

import copy
import transfer
import tools.transferer.splitter       as splitter
import tools.transferer.itemmstupdator as itemmstupdator


#---------------------------------------------------------------------------
#   Transfer
#---------------------------------------------------------------------------
class TransferActivityWorker(object):

    def __init__(self, sid, pid, env, data_type, s3bucket, s3file, **option):
        self.sid       = sid
        self.pid       = pid
        self.env       = env
        self.data_type = data_type
        self.s3bucket  = s3bucket
        self.s3file    = s3file
        self.option    = option

    @staticmethod
    def from_act_input(act_input):
        copy_act_input = copy.deepcopy(act_input)
        return TransferActivityWorker(sid=copy_act_input.pop('sid'),
                                      pid=copy_act_input.pop('pid'),
                                      env=copy_act_input.pop('env'),
                                      data_type=copy_act_input.pop('data_type'),
                                      s3bucket=copy_act_input.pop('s3bucket', None),
                                      s3file=copy_act_input.pop('s3file', None),
                                      **copy_act_input)

    def work(self):
        return transfer.start_transfer(sid=self.sid,
                                       pid=self.pid,
                                       env=self.env,
                                       data_type=self.data_type,
                                       s3_bucket=self.s3bucket,
                                       s3_path=self.s3file,
                                       **self.option)


#---------------------------------------------------------------------------
#   Split file
#---------------------------------------------------------------------------
class SplitTransferFileActivityWorker(object):

    def __init__(self, sid, env, data_type, s3bucket, s3file):
        self.sid       = sid
        self.env       = env
        self.data_type = data_type
        self.s3bucket  = s3bucket
        self.s3file    = s3file

    @staticmethod
    def from_act_input(act_input):
        return SplitTransferFileActivityWorker(sid=act_input['sid'],
                                               env=act_input['env'],
                                               data_type=act_input['data_type'],
                                               s3bucket=act_input['s3bucket'],
                                               s3file=act_input['s3file'])

    def work(self):
        return splitter.split(sid=self.sid,
                              env=self.env,
                              data_type=self.data_type,
                              s3bucket=self.s3bucket,
                              s3file=self.s3file)


#---------------------------------------------------------------------------
#   Update item master
#---------------------------------------------------------------------------
class UpdateItemMstActivityWorker(object):

    def __init__(self, sid, env, s3bucket, s3file):
        self.sid      = sid
        self.env      = env
        self.s3bucket = s3bucket
        self.s3file   = s3file

    @staticmethod
    def from_act_input(act_input):
        return UpdateItemMstActivityWorker(sid=act_input['sid'],
                                           env=act_input['env'],
                                           s3bucket=act_input['s3bucket'],
                                           s3file=act_input['s3file'])

    def work(self):
        return itemmstupdator.update(sid=self.sid,
                                     env=self.env,
                                     s3bucket=self.s3bucket,
                                     s3file=self.s3file)
