# -*- coding: utf-8 -*-

from __future__ import absolute_import

from   mapping                import Mapping
from   bridge                 import Bridge
from   load_extsegdata        import ExtSegDataLoader
from   optout_rt              import RtOptout
from   optout_co              import CoOptout
import tools.mapping.reporter as     reporter


#---------------------------------------------------------------------------
#   Mapping
#---------------------------------------------------------------------------
class MappingActivityWorker(object):

    def __init__(self, sid, pid, env, target_hour):
        self.sid         = sid
        self.pid         = pid
        self.env         = env
        self.target_hour = target_hour

    @staticmethod
    def from_act_input(act_input):
        return MappingActivityWorker(sid=act_input['sid'],
                                     pid=act_input['pid'],
                                     env=act_input['env'],
                                     target_hour=act_input['target_hour'])

    def work(self):
        return Mapping(sid=self.sid,
                       pid=self.pid,
                       env=self.env,
                       target_hour=self.target_hour).execute()


#---------------------------------------------------------------------------
#   Bridge
#---------------------------------------------------------------------------
class BridgeActivityWorker(object):

    def __init__(self, sid, pid, env, target_hour):
        self.sid         = sid
        self.pid         = pid
        self.env         = env
        self.target_hour = target_hour

    @staticmethod
    def from_act_input(act_input):
        return BridgeActivityWorker(sid=act_input['sid'],
                                    pid=act_input['pid'],
                                    env=act_input['env'],
                                    target_hour=act_input['target_hour'])

    def work(self):
        return Bridge(sid=self.sid,
                      pid=self.pid,
                      env=self.env,
                      target_hour=self.target_hour).execute()


#---------------------------------------------------------------------------
#   ExtSegData
#---------------------------------------------------------------------------
class ExtSegDataActivityWorker(object):

    def __init__(self, sid, pid, env, target_hour):
        self.sid         = sid
        self.pid         = pid
        self.env         = env
        self.target_hour = target_hour

    @staticmethod
    def from_act_input(act_input):
        return ExtSegDataActivityWorker(sid=act_input['sid'],
                                        pid=act_input['pid'],
                                        env=act_input['env'],
                                        target_hour=act_input['target_hour'])

    def work(self):
        return ExtSegDataLoader(sid=self.sid,
                                pid=self.pid,
                                env=self.env,
                                target_hour=self.target_hour).execute()


#---------------------------------------------------------------------------
#   OptoutRT
#---------------------------------------------------------------------------
class OptoutRTActivityWorker(object):

    def __init__(self, sid, env, target_day):
        self.sid        = sid
        self.env        = env
        self.target_day = target_day

    @staticmethod
    def from_act_input(act_input):
        return OptoutRTActivityWorker(sid=act_input['sid'],
                                      env=act_input['env'],
                                      target_day=act_input['target_day'])

    def work(self):
        return RtOptout(sid=self.sid,
                        env=self.env,
                        target_day=self.target_day).execute()


#---------------------------------------------------------------------------
#   OptoutCO
#---------------------------------------------------------------------------
class OptoutCOActivityWorker(object):

    def __init__(self, env, target_day):
        self.env        = env
        self.target_day = target_day

    @staticmethod
    def from_act_input(act_input):
        return OptoutCOActivityWorker(env=act_input['env'],
                                      target_day=act_input['target_day'])

    def work(self):
        return CoOptout(env=self.env,
                        target_day=self.target_day).execute()


#---------------------------------------------------------------------------
#   MappingReport
#---------------------------------------------------------------------------
class MappingReportActivityWorker(object):

    def __init__(self, env):
        self.env = env

    @staticmethod
    def from_act_input(act_input):
        return MappingReportActivityWorker(env=act_input['env'])

    def work(self):
        return reporter.send_report(env=self.env)
