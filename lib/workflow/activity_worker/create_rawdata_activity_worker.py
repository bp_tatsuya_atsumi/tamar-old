# -*- coding: utf-8 -*-

from __future__ import absolute_import

import copy
from   lib.rawdata.rawdata_creator import RawdataCreator


class CreateRawdataActivityWorker(object):

    def __init__(self, sid, env, data_type, s3bucket, s3file):
        self.sid       = sid
        self.env       = env
        self.data_type = data_type
        self.s3bucket  = s3bucket
        self.s3file    = s3file

    @staticmethod
    def from_act_input(act_input):
        copy_act_input = copy.deepcopy(act_input)
        return CreateRawdataActivityWorker(sid=copy_act_input.pop('sid'),
                                           env=copy_act_input.pop('env'),
                                           data_type=copy_act_input.pop('data_type'),
                                           s3bucket=copy_act_input.pop('s3bucket', None),
                                           s3file=copy_act_input.pop('s3file', None))

    def work(self):
        return self._execute(sid=self.sid, env=self.env, data_type=self.data_type, s3bucket=self.s3bucket, s3file=self.s3file)

    def _execute(self, sid, env, data_type, s3bucket, s3file):
        rawdata_creator = RawdataCreator(sid, env, data_type, s3bucket, s3file)
        rawdata_creator.create()
