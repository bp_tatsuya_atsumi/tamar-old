# -*- coding: utf-8 -*-

from __future__ import absolute_import

import json
import lib.workflow.util as util
from functools import wraps


"""
HistoryEvent

WorkflowExecutionStarted                        : The workflow execution was started.
WorkflowExecutionCompleted                      : The workflow execution was closed due to successful completion.
WorkflowExecutionFailed                         : The workflow execution closed due to a failure.
WorkflowExecutionTimedOut                       : The workflow execution was closed because a time out was exceeded.
WorkflowExecutionCanceled                       : The workflow execution was successfully canceled and closed.
WorkflowExecutionTerminated                     : The workflow execution was terminated.
WorkflowExecutionContinuedAsNew                 : The workflow execution was closed and a new execution of the same type was created with the same workflowId.
WorkflowExecutionCancelRequested                : A request to cancel this workflow execution was made.
WorkflowExecutionSignaled                       : An external signal was received for the workflow execution.
DecisionTaskScheduled                           : A decision task was scheduled for the workflow execution.
DecisionTaskStarted                             : The decision task was dispatched to a decider.
DecisionTaskCompleted                           : The decider successfully completed a decision task by calling RespondDecisionTaskCompleted.
DecisionTaskTimedOut                            : The decision task timed out.
ActivityTaskScheduled                           : An activity task was scheduled for execution.
ScheduleActivityTaskFailed                      : Failed to process ScheduleActivityTask decision. This happens when the decision is not configured properly, for example the activity type specified is not registered.
ActivityTaskStarted                             : The scheduled activity task was dispatched to a worker.
ActivityTaskCompleted                           : An activity worker successfully completed an activity task by calling RespondActivityTaskCompleted.
ActivityTaskFailed                              : An activity worker failed an activity task by calling RespondActivityTaskFailed.
ActivityTaskTimedOut                            : The activity task timed out.
ActivityTaskCanceled                            : The activity task was successfully canceled.
ActivityTaskCancelRequested                     : A RequestCancelActivityTask decision was received by the system.
RequestCancelActivityTaskFailed                 : Failed to process RequestCancelActivityTask decision. This happens when the decision is not configured properly.
MarkerRecorded                                  : A marker was recorded in the workflow history as the result of a RecordMarker decision.
TimerStarted                                    : A timer was started for the workflow execution due to a StartTimer decision.
StartTimerFailed                                : Failed to process StartTimer decision. This happens when the decision is not configured properly, for example a timer already exists with the specified timer Id.
TimerFired                                      : A timer, previously started for this workflow execution, fired.
TimerCanceled                                   : A timer, previously started for this workflow execution, was successfully canceled.
CancelTimerFailed                               : Failed to process CancelTimer decision. This happens when the decision is not configured properly, for example no timer exists with the specified timer Id.
StartChildWorkflowExecutionInitiated            : A request was made to start a child workflow execution.
StartChildWorkflowExecutionFailed               : Failed to process StartChildWorkflowExecution decision. This happens when the decision is not configured properly, for example the workflow type specified is not registered.
ChildWorkflowExecutionStarted                   : A child workflow execution was successfully started.
ChildWorkflowExecutionCompleted                 : A child workflow execution, started by this workflow execution, completed successfully and was closed.
ChildWorkflowExecutionFailed                    : A child workflow execution, started by this workflow execution, failed to complete successfully and was closed.
ChildWorkflowExecutionTimedOut                  : A child workflow execution, started by this workflow execution, timed out and was closed.
ChildWorkflowExecutionCanceled                  : A child workflow execution, started by this workflow execution, was canceled and closed.
ChildWorkflowExecutionTerminated                : A child workflow execution, started by this workflow execution, was terminated.
SignalExternalWorkflowExecutionInitiated        : A request to signal an external workflow was made.
ExternalWorkflowExecutionSignaled               : A signal, requested by this workflow execution, was successfully delivered to the target external workflow execution.
SignalExternalWorkflowExecutionFailed           : The request to signal an external workflow execution failed.
RequestCancelExternalWorkflowExecutionInitiated : A request was made to request the cancellation of an external workflow execution.
ExternalWorkflowExecutionCancelRequested        : Request to cancel an external workflow execution was successfully delivered to the target execution.
RequestCancelExternalWorkflowExecutionFailed    : Request to cancel an external workflow execution failed.
"""
is_workflow_started_event                           = lambda event_type: event_type == 'WorkflowExecutionStarted'
is_workflow_completed_event                         = lambda event_type: event_type == 'WorkflowExecutionCompleted'
is_workflow_failed_event                            = lambda event_type: event_type == 'WorkflowExecutionFailed'
is_workflow_timed_out_event                         = lambda event_type: event_type == 'WorkflowExecutionTimedOut'
is_workflow_canceled_event                          = lambda event_type: event_type == 'WorkflowExecutionCanceled'
is_workflow_terminated_event                        = lambda event_type: event_type == 'WorkflowExecutionTerminated'
is_workflow_continued_as_new_event                  = lambda event_type: event_type == 'WorkflowExecutionContinuedAsNew'
is_workflow_cancel_requested_event                  = lambda event_type: event_type == 'WorkflowExecutionCancelRequested'
is_workflow_signaled_event                          = lambda event_type: event_type == 'WorkflowExecutionSignaled'
is_decision_task_scheduled_event                    = lambda event_type: event_type == 'DecisionTaskScheduled'
is_decision_task_started_event                      = lambda event_type: event_type == 'DecisionTaskStarted'
is_decision_task_completed_event                    = lambda event_type: event_type == 'DecisionTaskCompleted'
is_decision_task_timed_out_event                    = lambda event_type: event_type == 'DecisionTaskTimedOut'
is_activity_task_scheduled_event                    = lambda event_type: event_type == 'ActivityTaskScheduled'
is_activity_task_schedule_failed_event              = lambda event_type: event_type == 'ScheduleActivityTaskFailed'
is_activity_task_started_event                      = lambda event_type: event_type == 'ActivityTaskStarted'
is_activity_task_completed_event                    = lambda event_type: event_type == 'ActivityTaskCompleted'
is_activity_task_failed_event                       = lambda event_type: event_type == 'ActivityTaskFailed'
is_activity_task_timed_out_event                    = lambda event_type: event_type == 'ActivityTaskTimedOut'
is_activity_task_canceled_event                     = lambda event_type: event_type == 'ActivityTaskCanceled'
is_activity_task_cancel_requested_event             = lambda event_type: event_type == 'ActivityTaskCancelRequested'
is_activity_task_cancel_request_failed_event        = lambda event_type: event_type == 'RequestCancelActivityTaskFailed'
is_marker_recorded_event                            = lambda event_type: event_type == 'MarkerRecorded'
is_timer_started_event                              = lambda event_type: event_type == 'TimerStarted'
is_timer_start_failed_event                         = lambda event_type: event_type == 'StartTimerFailed'
is_timer_fired_event                                = lambda event_type: event_type == 'TimerFired'
is_timer_canceled_event                             = lambda event_type: event_type == 'TimerCanceled'
is_timer_cancel_failed_event                        = lambda event_type: event_type == 'CancelTimerFailed'
is_child_workflow_start_initiated_event             = lambda event_type: event_type == 'StartChildWorkflowExecutionInitiated'
is_child_workflow_start_failed_event                = lambda event_type: event_type == 'StartChildWorkflowExecutionFailed'
is_child_workflow_started_event                     = lambda event_type: event_type == 'ChildWorkflowExecutionStarted'
is_child_workflow_completed_event                   = lambda event_type: event_type == 'ChildWorkflowExecutionCompleted'
is_child_workflow_failed_event                      = lambda event_type: event_type == 'ChildWorkflowExecutionFailed'
is_child_workflow_timed_out_event                   = lambda event_type: event_type == 'ChildWorkflowExecutionTimedOut'
is_child_workflow_canceled_event                    = lambda event_type: event_type == 'ChildWorkflowExecutionCanceled'
is_child_workflow_terminated_event                  = lambda event_type: event_type == 'ChildWorkflowExecutionTerminated'
is_external_workflow_signal_initiated_event         = lambda event_type: event_type == 'SignalExternalWorkflowExecutionInitiated'
is_external_workflow_signaled_event                 = lambda event_type: event_type == 'ExternalWorkflowExecutionSignaled'
is_external_workflow_signal_failed_event            = lambda event_type: event_type == 'SignalExternalWorkflowExecutionFailed'
is_external_workflow_cancel_request_initiated_event = lambda event_type: event_type == 'RequestCancelExternalWorkflowExecutionInitiated'
is_external_workflow_cancel_requested_event         = lambda event_type: event_type == 'ExternalWorkflowExecutionCancelRequested'
is_external_workflow_cancel_request_failed_event    = lambda event_type: event_type == 'RequestCancelExternalWorkflowExecutionFailed'

def get_event_attributes_key(event_type):
    return event_type[0].lower() + event_type[1:] + 'EventAttributes'

def get_wf_id_name(event):
    attrs_key = get_event_attributes_key(event['eventType'])
    attrs     = event.get(attrs_key)
    if attrs:
        return attrs.get('workflowId'), attrs.get('workflowType', {}).get('name')

def get_act_id_name(event):
    attrs_key = get_event_attributes_key(event['eventType'])
    attrs     = event.get(attrs_key)
    if attrs:
        return attrs.get('activityId'), attrs.get('activityType', {}).get('name')


def decorate_task_list_for_migration(f):
    """一部のアクティビティの場合は新環境のタスクリストに変換する。
    """
    @wraps(f)
    def _wrapper(act_name, input_dict):
        old_task_list = f(act_name, input_dict)
        new_task_list = 'tl_{}'.format(old_task_list)   # 新環境のタスクリスト名
        if not input_dict:
            return old_task_list

        sid = input_dict.get('sid')
        pid = input_dict.get('pid')
        if sid is None and pid is None:
            return old_task_list

        # 転送バッチ
        if act_name.startswith('Transfer'):
            # すべて新環境
            return new_task_list
        # ブリッジ
        elif act_name == 'BridgeActivity':
            # すべて新環境
            return new_task_list
        # マッピング
        elif act_name == 'MappingActivity':
            # すべて新環境
            return new_task_list
        # オプトアウト
        elif act_name.startswith('Optout'):
            # すべて新環境
            return new_task_list

        # アイテムマスタ更新
        elif act_name == 'UpdateItemMstActivity':
            if sid == '0001':
                # BP公式のみ新環境
                return new_task_list

        elif act_name == 'SplitTransferFileActivity':
            if sid == '0001':
                # BP公式のみ新環境
                return new_task_list

        return old_task_list
    return _wrapper


@decorate_task_list_for_migration
def get_activity_task_list(act_name, input_dict):
    """
    default
        DummyActivity
        MappingActivity
        MappingReportActivity
        TransferSegmentMstActivity
        TransferConversionActivity
        TransferRecommendMstActivity
        TransferItemMstActivity
        TransferRemoveSegmentMstActivity
        TransferRemoveRecommendMstActivity
        TransferLatestSegmentMstActivity
        TransferLatestRecommendMstActivity
        TransferLatestItemMstActivity
        TransferLatestSegmentDataActivity
        UpdateItemMstActivity
    tomb
        TombSendActivity
    transfer_high_ma
        TransferSegmentDataActivity(pid=ma)
        TransferRecommendDataActivity(pid=ma)
    transfer_high_so
        TransferSegmentDataActivity(pid=so)
        TransferRecommendDataActivity(pid=so)
    transfer_high_so_gdo
        TransferSegmentDataActivity(sid=0009, pid=so)
        TransferRecommendDataActivity(sid=0009, pid=so)
    transfer_high_ac_gdo
        TransferSegmentDataActivity(sid=0009, pid=ac)
    transfer_high_other
        TransferSegmentDataActivity
        TransferRecommendDataActivity
        TransferMailRequestDataActivity
        TransferMailResponseDataActivity
        TransferMailSegmentDataActivity
    rawdata
        CreateRawdataActivity
    extsegdata
        ExtSegDataActivity
    """

    if act_name == 'BridgeActivity':
        return 'bridge'
    if act_name == 'TombSendActivity':
        return 'tomb'
    if act_name == 'SplitTransferFileActivity':
        return 'transfer_high_other'
    if (act_name == 'TransferSegmentDataActivity' or
        act_name == 'TransferRecommendDataActivity'):

        is_gdo = (input_dict.get('sid') == '0009')
        is_ma  = (input_dict.get('pid') == 'ma')
        is_so  = (input_dict.get('pid') == 'so')
        is_ac  = (input_dict.get('pid') == 'ac')

        if is_ma:
            if is_gdo:
                return 'transfer_high_ma_gdo'
            else:
                return 'transfer_high_ma'
        if is_so:
            if is_gdo:
                return 'transfer_high_so_gdo'
            else:
                return 'transfer_high_so'
        if is_ac and is_gdo:
            return 'transfer_high_ac_gdo'
        return 'transfer_high_other'

    if (act_name == 'TransferMailRequestDataActivity'  or
        act_name == 'TransferMailResponseDataActivity' or
        act_name == 'TransferMailSegmentDataActivity'):
        return 'transfer_high_other'

    if act_name == 'CreateRawdataActivity':
        return 'rawdata'

    if act_name == 'ExtSegDataActivity':
        return 'extsegdata'

    return 'default'


class EventHelper(object):
    def __init__(self, decision_task):
        self.decision_task = decision_task
        self.events        = [e for e in decision_task['events']
                                  if not e['eventType'].startswith('Decision')]

    # current workflow
    def get_wf_id(self):
        return self.decision_task['workflowExecution']['workflowId']

    def get_wf_name(self):
        return self.decision_task['workflowType']['name']

    # current event
    def get_event(self):
        return self.events[-1]

    def get_event_id(self):
        return self.get_event()['eventId']

    def get_event_type(self):
        return self.get_event()['eventType']

    def get_event_attrs(self):
        return self.get_event()[get_event_attributes_key(self.get_event_type())]

    def get_event_input_dict(self):
        return json.loads(self.get_event_attrs()['input'])

    # scheduled event
    def get_scheduled_event_id(self):
        return self.get_event_attrs()['scheduledEventId']

    def get_scheduled_event(self):
        return self._get_previous_event(self.get_scheduled_event_id())

    def get_scheduled_event_attrs(self):
        return self._get_previous_event_attrs(self.get_scheduled_event_id())

    # started event
    def get_started_event_id(self):
        return self.get_event_attrs()['startedEventId']

    def get_started_event(self):
        return self._get_previous_event(self.get_started_event_id())

    def get_started_event_attrs(self):
        return self._get_previous_event_attrs(self.get_started_event_id())

    def _get_previous_event(self, event_id):
        return [e for e in self.events if e['eventId'] == event_id][0]

    def _get_previous_event_attrs(self, event_id):
        event      = self._get_previous_event(event_id)
        event_type = event['eventType']
        return event[get_event_attributes_key(event_type)]

    # workflow history
    def get_initiated_child_workflow_id_names(self):
        return [get_wf_id_name(e) for e in self.events
                    if is_child_workflow_start_initiated_event(e['eventType'])]

    def get_scheduled_act_id_names(self):
        return [get_act_id_name(e) for e in self.events
                    if is_activity_task_scheduled_event(e['eventType'])]

    def get_started_act_id_names(self):
        return [get_act_id_name(e) for e in self.events
                    if is_activity_task_started_event(e['eventType'])]

    def count_child_workflow_events(self):
        def _count_events(func_condition):
            return sum([1 for e in self.events if func_condition(e['eventType'])])
        return {
            'initiated'    : _count_events(is_child_workflow_start_initiated_event),
            'start_failed' : _count_events(is_child_workflow_start_failed_event),
            'started'      : _count_events(is_child_workflow_started_event),
            'completed'    : _count_events(is_child_workflow_completed_event),
            'failed'       : _count_events(is_child_workflow_failed_event),
            'timed_out'    : _count_events(is_child_workflow_timed_out_event),
            'canceled'     : _count_events(is_child_workflow_canceled_event),
            'terminated'   : _count_events(is_child_workflow_terminated_event),
        }


class DecisionHelper(object):
    def __init__(self, decisions, domain, version, task_list):
        self.decisions = decisions
        self.domain    = domain
        self.version   = version
        self.task_list = task_list

    def complete_workflow(self, result=None):
        self.decisions.complete_workflow_execution(
            result=result
        )

    def fail_workflow(self, reason=None, details=None):
        self.decisions.fail_workflow_execution(
            reason=reason,
            details=details
        )

    def cancel_workflow(self, details=None):
        self.decisions.cancel_workflow_executions(
            details=details
        )

    def schedule_activity_task(self, act_id, act_name, input_dict):
        self.decisions.schedule_activity_task(
            activity_id=act_id,
            activity_type_name=act_name,
            activity_type_version=self.version,
            task_list=get_activity_task_list(act_name, input_dict),
            input=util.dict_to_json(input_dict)
        )

    def request_cancel_activity_task(self, act_id):
        self.decisions.request_cancel_activity_task(
            activity_id=act_id
        )

    def start_child_workflow(self, c_wf_id, c_wf_name, input_dict):
        self.decisions.start_child_workflow_execution(
            workflow_id=c_wf_id,
            workflow_type_name=c_wf_name,
            workflow_type_version=self.version,
            input=util.dict_to_json(input_dict)
        )

    def request_cancel_child_workflow(self, c_wf_id, control=None, run_id=None):
        self.decisions.request_cancel_external_workflow_execution(
            workflow_id=c_wf_id,
            control=control,
            run_id=run_id
        )


class MasterHelper(object):

    def __init__(self, site_mst, partner_mst, mapping_mst, workflow_mst):
        self.site_mst     = site_mst
        self.partner_mst  = partner_mst
        self.mapping_mst  = mapping_mst
        self.workflow_mst = workflow_mst

    @staticmethod
    def from_config(config):
        return MasterHelper(config.site_mst,
                            config.partner_mst,
                            config.mapping,
                            config.workflow)

    # distribute
    def distribute_mapping(self):
        sid_pids = []
        for sid, pid_attrs in self.mapping_mst.items():
            sid_pids.extend(
                [(sid, pid) for pid, attrs in pid_attrs.items()
                    if self.is_mapping_partner(pid)
                        and not attrs.get('no_mapping', False)]
            )
        return sid_pids

    def distribute_mapping_divided(self, total_class, target_class):
        sid_pids = []
        for sid, pid_attrs in self.mapping_mst.items():
            if int(sid) % total_class == target_class:
                sid_pids.extend(
                    [(sid, pid) for pid, attrs in pid_attrs.items()
                        if self.is_mapping_partner(pid)
                            and not attrs.get('no_mapping', False)]
                )
        return sid_pids

    def distribute_bridge(self):
        sid_pids = []
        for sid, pid_attrs in self.mapping_mst.items():
            if pid_attrs.has_key('rt'):
                attrs = pid_attrs.get('rt')
                if attrs.has_key('bridges') and not attrs.get('no_mapping', False):
                    sid_pids.extend(
                        [(sid, rt_pid) for rt_pid in attrs.get('bridges') if rt_pid.startswith('rt')]
                    )
        return sid_pids

    def distribute_extsegdata(self):
        sid_pids = []
        for sid, pid_attrs in self.mapping_mst.items():
            if pid_attrs.has_key('rt'):
                attrs = pid_attrs.get('rt')
                if attrs.has_key('receives'):
                    sid_pids.extend(
                        [(sid, datasource) for datasource in attrs.get('receives')]
                    )
        return sid_pids

    def distribute_optout_rt(self):
        return self.mapping_mst.keys()

    def distribute_transfer(self, sid, data_type):
        pids = [pid for pid, attrs
                    in self.mapping_mst.get(sid, {}).items()
                    if self.is_transfer_partner(pid)
                        and not attrs.get('no_transfer', False)
                        and self.is_transfer_data_type(pid, data_type)
                        and self.is_no_transfer_latest_data_type(pid, data_type)]

        if 'rt' in pids:
            pids.remove('rt')
            _bridged_rtsids = self.mapping_mst.get(sid, {})['rt'].get('bridges')
            if _bridged_rtsids:
                pids.extend(_bridged_rtsids)

        return pids

    #  partner_mst
    def is_mapping_partner(self, pid):
        return not self.is_no_mapping_partner(pid)

    def is_no_mapping_partner(self, pid):
        return self._get_partner_option(pid).get('no_mapping', False)

    def is_transfer_partner(self, pid):
        return not self.is_no_transfer_partner(pid)

    def is_no_transfer_partner(self, pid):
        return self._get_partner_option(pid).get('no_transfer', False)

    def is_transfer_data_type(self, pid, data_type):
        return data_type in self._get_partner_option(pid).get('transfer_data_types', [])

    def is_no_transfer_data_type(self, pid, data_type):
        return not self.is_transfer_data_type(pid, data_type)

    def is_transfer_latest_data_type(self, pid, data_type):
        return data_type in self._get_partner_option(pid).get('transfer_latest_data_types', [])

    def is_no_transfer_latest_data_type(self, pid, data_type):
        return not self.is_transfer_latest_data_type(pid, data_type)

    def _get_partner_option(self, pid):
        return self.partner_mst[pid].get('option', {})

    # mapping_mst
    def is_split_transfer_mapping(self, sid, pid):
        return self.mapping_mst.get(sid, {})\
                               .get(pid, {})\
                               .get('split_transfer', False)

    # workflow_mst
    def wfname_to_childwfattrs(self, wf_name):
        c_wf_attrs = self.workflow_mst['mainwftype_to_childwfattrs'][wf_name]
        return [(a['data_type'], a['workflow_type']) for a in c_wf_attrs]

    def childwfname_to_acttype(self, wf_name):
        return self.workflow_mst['childwftype_to_acttype'][wf_name]

    def datatype_to_childwfname(self, data_type):
        for chid_wf_attrs in self.workflow_mst['mainwftype_to_childwfattrs'].values():
            for child_wf_attr in chid_wf_attrs:
                if child_wf_attr['data_type'] == data_type:
                    return child_wf_attr['workflow_type']
        raise ValueError('unknown data_type')
