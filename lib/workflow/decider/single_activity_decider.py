# -*- coding: utf-8 -*-

from __future__ import absolute_import

from   lib.workflow.decider.base   import DeciderBase


class SingleActivityDecider(DeciderBase):
    """
    Workflow
        |- Activity
    """
    # workflow event
    def _handle_workflow_started_event(self):
        raise NotImplementedError()

    def _handle_workflow_cancel_requested_event(self):
        is_activity_task_scheduled = len(self.schedueled_act_id_names) > 0
        is_activity_task_started   = len(self.started_act_id_names)    > 0

        if not is_activity_task_scheduled:
            self.decision_helper.cancel_workflow(
                details='workflow cancel requested'
            )
            self._log_warn('Workflow canceled: wf_id=%s, event_id=%s',
                           self.wf_id, self.event_id)

        elif not is_activity_task_started:
            act_id, act_name = self.schedueled_act_id_names[0]

            self.decision_helper.request_cancel_activity_task(act_id=act_id)
            self._log_warn('Activity cancel requested: wf_id=%s, event_id=%s, act_id=%s',
                           self.wf_id, self.event_id, act_id)

        else:
            self._log_warn('Workflow cancel request ignored: wf_id=%s, event_id=%s',
                           self.wf_id, self.event_id)

    # activity task event
    def _handle_activity_task_completed_event(self):
        self.decision_helper.complete_workflow()
        self._log_info('Workflow completed: wf_id=%s, event_id=%s',
                       self.wf_id, self.event_id)

    def _handle_activity_task_schedule_failed_event(self):
        self.__handle_activity_task_failed_event(
            reason='activity task schedule failed',
            details=self.event_attrs['cause']
        )

    def _handle_activity_task_failed_event(self):
        self.__handle_activity_task_failed_event(
            reason=self.event_attrs['reason'],
            details=self.event_attrs['details']
        )

    def _handle_activity_task_timed_out_event(self):
        self.__handle_activity_task_failed_event(
            reason='activity task timed out',
            details=self.event_attrs['timeoutType']
        )

    def _handle_activity_task_cancel_requested_event(self):
        self.decision_helper.cancel_workflow(
            details='activity task cancel requested'
        )
        self._log_warn('Workflow canceled: wf_id=%s, event_id=%s',
                       self.wf_id, self.event_id)

    def _handle_activity_task_canceled_event(self):
        self.decision_helper.cancel_workflow(
            details='activity task cancel requested'
        )
        self._log_warn('Workflow canceled: wf_id=%s, event_id=%s',
                       self.wf_id, self.event_id)

    def __handle_activity_task_failed_event(self, reason, details):
        self.decision_helper.fail_workflow(reason=reason, details=details)
        self._log_error('Workflow failed: wf_id=%s, event_id=%s, reason=%s, details="%s"',
                        self.wf_id, self.event_id, reason, details)
