# -*- coding: utf-8 -*-

from __future__ import absolute_import

import copy
import json
from   lib.workflow.decider.parallel_workflow_decider import ParallelWorkflowDecider
from   lib.workflow.decider.single_activity_decider   import SingleActivityDecider
import lib.workflow.util as util


#---------------------------------------------------------------------------
#   Transfer
#---------------------------------------------------------------------------
class TransferMainWorkflowDecider(ParallelWorkflowDecider):
    """
    TransferMainWorkflow(siteA)
        |- TransferChildWorkflow(siteA, partnerA, data_typeA)
        |- TransferChildWorkflow(siteA, partnerB, data_typeB)
        |- SplitTransferFileChildWorkflow(siteA, partnerC, data_typeA)
    """
    def _handle_workflow_started_event(self):
        sid = self.event_input_dict['sid']

        has_split_transfer_mapping = False
        is_c_wf_started = False

        # e.g. TransferRecommendMstMain
        #        => [(recommendmst, TransferRecommendMstChild),
        #            (remove_recommendmst, TransferRemoveRecommendMstChild)]
        c_wf_attrs = self.master_helper.wfname_to_childwfattrs(self.wf_name)
        for c_wf_datatype, c_wf_name in c_wf_attrs:
            for pid in self.master_helper.distribute_transfer(sid, c_wf_datatype):
                if self._is_split_transfer_mapping(sid, pid, c_wf_datatype):
                    has_split_transfer_mapping = True
                    continue

                c_wf_input = copy.deepcopy(self.event_input_dict)
                c_wf_input.update(sid=sid, pid=pid, data_type=c_wf_datatype)
                c_wf_id = util.create_workflow_id(c_wf_name, self.version, sid, pid)

                is_c_wf_started = True
                self.decision_helper.start_child_workflow(c_wf_id=c_wf_id,
                                                          c_wf_name=c_wf_name,
                                                          input_dict=c_wf_input)
                self._log_info('ChildWorkflow started: wf_id=%s, event_id=%s, c_wf_id=%s',
                               self.wf_id, self.event_id, c_wf_id)

        # Split transfer file
        if has_split_transfer_mapping:
            c_wf_name = 'SplitTransferFileChildWorkflow'
            c_wf_datatype = self.master_helper.wfname_to_childwfattrs(self.wf_name)[0][0]
            c_wf_input = copy.deepcopy(self.event_input_dict)
            c_wf_input.update(sid=sid, data_type=c_wf_datatype)

            c_wf_id = util.create_workflow_id(c_wf_name, self.version, sid, c_wf_datatype)

            is_c_wf_started = True
            self.decision_helper.start_child_workflow(c_wf_id=c_wf_id,
                                                      c_wf_name=c_wf_name,
                                                      input_dict=c_wf_input)
            self._log_info('ChildWorkflow started: wf_id=%s, event_id=%s, c_wf_id=%s',
                           self.wf_id, self.event_id, c_wf_id)

        # If no child workflow started, complete workflow
        if not is_c_wf_started:
            self.decision_helper.complete_workflow(result='no mapping')
            self._log_info('Workflow completed: wf_id=%s, event_id=%s',
                           self.wf_id, self.event_id)

    def _is_split_transfer_mapping(self, sid, pid, data_type):
        return (data_type in ['segmentdata', 'recommenddata']
                    and self.master_helper.is_split_transfer_mapping(sid, pid))


class TransferChildWorkflowDecider(SingleActivityDecider):
    """
    TransferChildWorkflow(siteA, partnerA, data_typeA)
        |- TransferActivity(siteA, partnerA, data_typeA)
    """
    def _handle_workflow_started_event(self):
        act_name  = self.master_helper.childwfname_to_acttype(self.wf_name)
        act_input = copy.deepcopy(self.event_input_dict)
        act_id    = util.create_activity_id(act_name, self.version,
                                            act_input['sid'], act_input['pid'])

        self.decision_helper.schedule_activity_task(act_id=act_id,
                                                    act_name=act_name,
                                                    input_dict=act_input)
        self._log_info('Activity scheduled: wf_id=%s, event_id=%s, act_id=%s',
                       self.wf_id, self.event_id, act_id)


#---------------------------------------------------------------------------
#   Transfer latest
#---------------------------------------------------------------------------
class TransferLatestMainWorkflowDecider(TransferChildWorkflowDecider):
    """
    TransferLatestMainWorkflow(siteA, partnerA, data_typeA)
        |- TransferLatestActivity(siteA, partnerA, data_typeA)
    """
    pass


#---------------------------------------------------------------------------
#   Transfer item master
#---------------------------------------------------------------------------
class TransferItemMstMainWorkflowDecider(ParallelWorkflowDecider):
    """
    TransferMainWorkflow(siteA)
        |- UpdateItemMstChildWorkflow(siteA)
        |- TransferChildWorkflow(siteA, partnerA)
        |- TransferChildWorkflow(siteA, partnerB)
    """
    def _handle_workflow_started_event(self):
        sid = self.event_input_dict['sid']

        # Update item master
        c_wf_name  = 'UpdateItemMstChildWorkflow'
        c_wf_input = copy.deepcopy(self.event_input_dict)
        c_wf_id    = util.create_workflow_id(c_wf_name, self.version, sid)

        self.decision_helper.start_child_workflow(c_wf_id=c_wf_id,
                                                  c_wf_name=c_wf_name,
                                                  input_dict=c_wf_input)
        self._log_info('ChildWorkflow started: wf_id=%s, event_id=%s, c_wf_id=%s',
                       self.wf_id, self.event_id, c_wf_id)

        # Transfer to partner that send difference
        c_wf_name = 'TransferItemMstChildWorkflow'
        for pid in self.master_helper.distribute_transfer(sid, 'itemmst'):
            c_wf_input = copy.deepcopy(self.event_input_dict)
            c_wf_input.update(sid=sid, pid=pid)
            c_wf_id = util.create_workflow_id(c_wf_name, self.version, sid, pid)

            self.decision_helper.start_child_workflow(c_wf_id=c_wf_id,
                                                      c_wf_name=c_wf_name,
                                                      input_dict=c_wf_input)
            self._log_info('ChildWorkflow started: wf_id=%s, event_id=%s, c_wf_id=%s',
                           self.wf_id, self.event_id, c_wf_id)


#---------------------------------------------------------------------------
#   Split file
#---------------------------------------------------------------------------
class SplitTransferFileChildWorkflowDecider(ParallelWorkflowDecider, SingleActivityDecider):
    """
    SplitTransferFileChildWorkflow(siteA, partnerA, data_typeA)
        |- SplitTransferFileActivity(siteA, partnerA, data_typeA)
            |- TransferChildWorkflow(siteA, partnerA, data_typeA, split1)
            |- TransferChildWorkflow(siteA, partnerA, data_typeA, split2)
    """
    def _handle_workflow_started_event(self):
        act_name  = self.master_helper.childwfname_to_acttype(self.wf_name)
        act_input = copy.deepcopy(self.event_input_dict)
        act_id    = util.create_activity_id(act_name, self.version,
                                            act_input['sid'],
                                            act_input['data_type'])

        self.decision_helper.schedule_activity_task(act_id=act_id,
                                                    act_name=act_name,
                                                    input_dict=act_input)
        self._log_info('Activity scheduled: wf_id=%s, event_id=%s, act_id=%s',
                       self.wf_id, self.event_id, act_id)

    def _handle_activity_task_completed_event(self):
        scheduled_act_input = json.loads(self.scheduled_event_attrs['input'])

        sid = scheduled_act_input['sid']
        c_wf_datatype = scheduled_act_input['data_type']
        c_wf_name = self.master_helper.datatype_to_childwfname(c_wf_datatype)

        splitted_s3files = sorted(json.loads(self.event_attrs['result']))
        for pid in self.master_helper.distribute_transfer(sid, c_wf_datatype):
            if not self._is_split_transfer_mapping(sid, pid, c_wf_datatype):
                continue
            for i, splitted_s3file in enumerate(splitted_s3files):
                c_wf_input = copy.deepcopy(scheduled_act_input)
                c_wf_input.update(pid=pid, s3file=splitted_s3file, seqno=(i + 1))

                c_wf_id = util.create_workflow_id(c_wf_name, self.version, sid,
                                                  pid, str(i + 1))

                self.decision_helper.start_child_workflow(c_wf_id=c_wf_id,
                                                          c_wf_name=c_wf_name,
                                                          input_dict=c_wf_input)
                self._log_info('ChildWorkflow started: wf_id=%s, event_id=%s, c_wf_id=%s',
                               self.wf_id, self.event_id, c_wf_id)

    def _is_split_transfer_mapping(self, sid, pid, data_type):
        return (data_type in ['segmentdata', 'recommenddata']
                    and self.master_helper.is_split_transfer_mapping(sid, pid))


#---------------------------------------------------------------------------
#   Update item master
#---------------------------------------------------------------------------
class UpdateItemMstChildWorkflowDecider(SingleActivityDecider):
    """
    UpdateItemMstChildWorkflow(siteA)
        |- UpdateItemMstActivity(siteA)
    """
    def _handle_workflow_started_event(self):
        act_name  = self.master_helper.childwfname_to_acttype(self.wf_name)
        act_input = copy.deepcopy(self.event_input_dict)
        act_id    = util.create_activity_id(act_name, self.version,
                                            act_input['sid'])

        self.decision_helper.schedule_activity_task(act_id=act_id,
                                                    act_name=act_name,
                                                    input_dict=act_input)
        self._log_info('Activity scheduled: wf_id=%s, event_id=%s, act_id=%s',
                       self.wf_id, self.event_id, act_id)
