# -*- coding: utf-8 -*-

from __future__ import absolute_import

from   lib.workflow.decider.base import DeciderBase


class SerialActivityDecider(DeciderBase):
    """
    Workflow
        |- Activity - Activity - Activity
    """
    pass
