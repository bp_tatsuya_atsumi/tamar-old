# -*- coding: utf-8 -*-

from __future__ import absolute_import

import copy
from   lib.workflow.decider.parallel_workflow_decider import ParallelWorkflowDecider
from   lib.workflow.decider.single_activity_decider   import SingleActivityDecider
import lib.workflow.util as util


class TombSendMainWorkflowDecider(ParallelWorkflowDecider):
    """
    TombSendMainWorkflow
        |- TombSendChildWorkflow(siteA)
        |- TombSendChildWorkflow(siteB)
        |- TombSendChildWorkflow(siteC)
    """
    def _handle_workflow_started_event(self):
        c_wf_name = self.master_helper.wfname_to_childwfattrs(self.wf_name)[0][1]

        is_c_wf_started = False
        for sid in self.master_helper.site_mst.keys():
            c_wf_input = copy.deepcopy(self.event_input_dict)
            c_wf_input.update(sid=sid)

            c_wf_id = util.create_workflow_id(c_wf_name, self.version, sid)

            is_c_wf_started = True
            self.decision_helper.start_child_workflow(c_wf_id=c_wf_id,
                                                      c_wf_name=c_wf_name,
                                                      input_dict=c_wf_input)
            self._log_info('ChildWorkflow started: wf_id=%s, event_id=%s, c_wf_id=%s',
                           self.wf_id, self.event_id, c_wf_id)

        if not is_c_wf_started:
            self.decision_helper.complete_workflow(result='no site')
            self._log_info('Workflow completed: wf_id=%s, event_id=%s',
                           self.wf_id, self.event_id)


class TombSendChildWorkflowDecider(SingleActivityDecider):
    """
    TombSendChildWorkflow(siteA)
        |- TombSendActivity(siteA)
    """
    def _handle_workflow_started_event(self):
        act_name  = self.master_helper.childwfname_to_acttype(self.wf_name)
        act_input = copy.deepcopy(self.event_input_dict)
        act_id    = util.create_activity_id(act_name, self.version, act_input['sid'])

        self.decision_helper.schedule_activity_task(act_id=act_id,
                                                    act_name=act_name,
                                                    input_dict=act_input)
        self._log_info('Activity scheduled: wf_id=%s, event_id=%s, act_id=%s',
                       self.wf_id, self.event_id, act_id)
