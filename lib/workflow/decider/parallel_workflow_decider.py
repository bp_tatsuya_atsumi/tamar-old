# -*- coding: utf-8 -*-

from __future__ import absolute_import

from   lib.workflow.decider.base   import DeciderBase
import lib.workflow.util           as util


class ParallelWorkflowDecider(DeciderBase):
    """
    Workflow
        |- ChildWorkflow
        |- ChildWorkflow
        |- ChildWorkflow
    """
    # workflow event
    def _handle_workflow_started_event(self):
        raise NotImplementedError()

    def _handle_workflow_cancel_requested_event(self):
        c_wf_id_names = self.initiated_child_workflow_id_names
        for c_wf_id, c_wf_name in c_wf_id_names:
            self.decision_helper.request_cancel_child_workflow(c_wf_id=c_wf_id)
            self._log_warn('ChildWorkflow cancel requested: wf_id=%s, event_id=%s, c_wf_id=%s',
                           self.wf_id, self.event_id, c_wf_id)

    # child workflow event
    def _handle_child_workflow_completed_event(self):
        counts = self.child_workflow_counts
        self._log_info('ChildWorkflow statuses: %s', util.dict_to_json(counts))

        def _sum_counts(keys):
            return sum([c for k, c in counts.items() if k in keys])


        count_initiated = _sum_counts(['initiated'])
        count_closed    = _sum_counts(['completed', 'failed', 'timed_out', 'canceled',  'terminated'])
        if count_initiated == count_closed:
            if _sum_counts(['start_failed']) > 0:
                self._log_error('Some ChildWorkflows failed to start: wf_id=%s, c_wf_id=%s, cause=%s',
                                self.wf_id, self.event_attrs.get('workflowId'), self.event_attrs.get('cause'))

            self.decision_helper.complete_workflow(util.dict_to_json(counts))
            self._log_info('Workflow completed: wf_id=%s, event_id=%s',
                           self.wf_id, self.event_id)

    _handle_child_workflow_start_failed_event = _handle_child_workflow_completed_event
    _handle_child_workflow_failed_event       = _handle_child_workflow_completed_event
    _handle_child_workflow_timed_out_event    = _handle_child_workflow_completed_event
    _handle_child_workflow_canceled_event     = _handle_child_workflow_completed_event
    _handle_child_workflow_terminated_event   = _handle_child_workflow_completed_event
