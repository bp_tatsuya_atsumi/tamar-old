# -*- coding: utf-8 -*-

from __future__ import absolute_import

import copy
from   lib.workflow.decider.parallel_workflow_decider import ParallelWorkflowDecider
from   lib.workflow.decider.single_activity_decider   import SingleActivityDecider
import lib.workflow.util as util


#---------------------------------------------------------------------------
#   Mapping
#---------------------------------------------------------------------------
class MappingMainWorkflowDecider(ParallelWorkflowDecider):
    """
    MappingMainWorkflow
        |- MappingChildWorkflow(siteA, partnerA)
        |- MappingChildWorkflow(siteB, partnerB)
        |- MappingChildWorkflow(siteC, partnerC)
    """
    def _handle_workflow_started_event(self):
        c_wf_name = self.master_helper.wfname_to_childwfattrs(self.wf_name)[0][1]

        is_c_wf_started = False
        
        sid_pids = []
        if self.event_input_dict.has_key('total_class') \
                and self.event_input_dict.has_key('target_class'):
            sid_pids = self.master_helper.distribute_mapping_divided(self.event_input_dict['total_class'],
                                                                     self.event_input_dict['target_class'])
        else:
            sid_pids = self.master_helper.distribute_mapping()

        for sid, pid in sid_pids:
            c_wf_input = copy.deepcopy(self.event_input_dict)
            c_wf_input.update(sid=sid, pid=pid)

            c_wf_id = util.create_workflow_id(c_wf_name, self.version, sid, pid)

            is_c_wf_started = True
            self.decision_helper.start_child_workflow(c_wf_id=c_wf_id,
                                                      c_wf_name=c_wf_name,
                                                      input_dict=c_wf_input)
            self._log_info('ChildWorkflow started: wf_id=%s, event_id=%s, c_wf_id=%s',
                           self.wf_id, self.event_id, c_wf_id)

        if not is_c_wf_started:
            self.decision_helper.complete_workflow(result='no mapping')
            self._log_info('Workflow completed: wf_id=%s, event_id=%s',
                           self.wf_id, self.event_id)


class MappingChildWorkflowDecider(SingleActivityDecider):
    """
    MappingChildWorkflow(siteA, parterA)
        |- MappingActivity(siteA, parterA)
    """
    def _handle_workflow_started_event(self):
        act_name  = self.master_helper.childwfname_to_acttype(self.wf_name)
        act_input = copy.deepcopy(self.event_input_dict)
        act_id    = util.create_activity_id(act_name, self.version,
                                            act_input['sid'], act_input['pid'])

        self.decision_helper.schedule_activity_task(act_id=act_id,
                                                    act_name=act_name,
                                                    input_dict=act_input)
        self._log_info('Activity scheduled: wf_id=%s, event_id=%s, act_id=%s',
                       self.wf_id, self.event_id, act_id)


#---------------------------------------------------------------------------
#   Bridge
#---------------------------------------------------------------------------
class BridgeMainWorkflowDecider(ParallelWorkflowDecider):
    """
    BridgeMainWorkflow
        |- BridgeChildWorkflow(siteA, partnerA)
        |- BridgeChildWorkflow(siteB, partnerB)
        |- BridgeChildWorkflow(siteC, partnerC)
    """
    def _handle_workflow_started_event(self):
        c_wf_name = self.master_helper.wfname_to_childwfattrs(self.wf_name)[0][1]

        is_c_wf_started = False
        for sid, pid in self.master_helper.distribute_bridge():
            c_wf_input = copy.deepcopy(self.event_input_dict)
            c_wf_input.update(sid=sid, pid=pid)

            c_wf_id = util.create_workflow_id(c_wf_name, self.version, sid, pid)

            is_c_wf_started = True
            self.decision_helper.start_child_workflow(c_wf_id=c_wf_id,
                                                      c_wf_name=c_wf_name,
                                                      input_dict=c_wf_input)
            self._log_info('ChildWorkflow started: wf_id=%s, event_id=%s, c_wf_id=%s',
                           self.wf_id, self.event_id, c_wf_id)

        if not is_c_wf_started:
            self.decision_helper.complete_workflow(result='no mapping')
            self._log_info('Workflow completed: wf_id=%s, event_id=%s',
                           self.wf_id, self.event_id)


class BridgeChildWorkflowDecider(SingleActivityDecider):
    """
    BridgeChildWorkflow(siteA, parterA)
        |- BridgeActivity(siteA, parterA)
    """
    def _handle_workflow_started_event(self):
        act_name  = self.master_helper.childwfname_to_acttype(self.wf_name)
        act_input = copy.deepcopy(self.event_input_dict)
        act_id    = util.create_activity_id(act_name, self.version,
                                            act_input['sid'], act_input['pid'])

        self.decision_helper.schedule_activity_task(act_id=act_id,
                                                    act_name=act_name,
                                                    input_dict=act_input)
        self._log_info('Activity scheduled: wf_id=%s, event_id=%s, act_id=%s',
                       self.wf_id, self.event_id, act_id)


#---------------------------------------------------------------------------
#   ExtSegData
#---------------------------------------------------------------------------
class ExtSegDataMainWorkflowDecider(ParallelWorkflowDecider):
    """
    ExtSegDataMainWorkflow
        |- ExtSegDataChildWorkflow(siteA, partnerA)
        |- ExtSegDataChildWorkflow(siteB, partnerB)
        |- ExtSegDataChildWorkflow(siteC, partnerC)
    """
    def _handle_workflow_started_event(self):
        c_wf_name = self.master_helper.wfname_to_childwfattrs(self.wf_name)[0][1]

        is_c_wf_started = False
        for sid, pid in self.master_helper.distribute_extsegdata():
            c_wf_input = copy.deepcopy(self.event_input_dict)
            c_wf_input.update(sid=sid, pid=pid)

            c_wf_id = util.create_workflow_id(c_wf_name, self.version, sid, pid)

            is_c_wf_started = True
            self.decision_helper.start_child_workflow(c_wf_id=c_wf_id,
                                                      c_wf_name=c_wf_name,
                                                      input_dict=c_wf_input)
            self._log_info('ChildWorkflow started: wf_id=%s, event_id=%s, c_wf_id=%s',
                           self.wf_id, self.event_id, c_wf_id)

        if not is_c_wf_started:
            self.decision_helper.complete_workflow(result='no extsegdata')
            self._log_info('Workflow completed: wf_id=%s, event_id=%s',
                           self.wf_id, self.event_id)


class ExtSegDataChildWorkflowDecider(SingleActivityDecider):
    """
    ExtSegDataChildWorkflow(siteA, parterA)
        |- ExtSegDataActivity(siteA, parterA)
    """
    def _handle_workflow_started_event(self):
        act_name  = self.master_helper.childwfname_to_acttype(self.wf_name)
        act_input = copy.deepcopy(self.event_input_dict)
        act_id    = util.create_activity_id(act_name, self.version,
                                            act_input['sid'], act_input['pid'])

        self.decision_helper.schedule_activity_task(act_id=act_id,
                                                    act_name=act_name,
                                                    input_dict=act_input)
        self._log_info('Activity scheduled: wf_id=%s, event_id=%s, act_id=%s',
                       self.wf_id, self.event_id, act_id)


#---------------------------------------------------------------------------
#   OptoutRT
#---------------------------------------------------------------------------
class OptoutRTMainWorkflowDecider(ParallelWorkflowDecider):
    """
    OptoutRTMainWorkflow
        |- OptoutRTChildWorkflow(siteA, partnerA)
        |- OptoutRTChildWorkflow(siteB, partnerB)
        |- OptoutRTChildWorkflow(siteC, partnerC)
    """
    def _handle_workflow_started_event(self):
        c_wf_name = self.master_helper.wfname_to_childwfattrs(self.wf_name)[0][1]

        is_c_wf_started = False
        for sid in self.master_helper.distribute_optout_rt():
            c_wf_input = copy.deepcopy(self.event_input_dict)
            c_wf_input.update(sid=sid)

            c_wf_id = util.create_workflow_id(c_wf_name, self.version, sid)

            is_c_wf_started = True
            self.decision_helper.start_child_workflow(c_wf_id=c_wf_id,
                                                      c_wf_name=c_wf_name,
                                                      input_dict=c_wf_input)
            self._log_info('ChildWorkflow started: wf_id=%s, event_id=%s, c_wf_id=%s',
                           self.wf_id, self.event_id, c_wf_id)

        if not is_c_wf_started:
            self.decision_helper.complete_workflow(result='no optout log')
            self._log_info('Workflow completed: wf_id=%s, event_id=%s',
                           self.wf_id, self.event_id)


class OptoutRTChildWorkflowDecider(SingleActivityDecider):
    """
    OptoutRTChildWorkflow(siteA, parterA)
        |- OptoutRTActivity(siteA, parterA)
    """
    def _handle_workflow_started_event(self):
        act_name  = self.master_helper.childwfname_to_acttype(self.wf_name)
        act_input = copy.deepcopy(self.event_input_dict)
        act_id    = util.create_activity_id(act_name, self.version, act_input['sid'])

        self.decision_helper.schedule_activity_task(act_id=act_id,
                                                    act_name=act_name,
                                                    input_dict=act_input)
        self._log_info('Activity scheduled: wf_id=%s, event_id=%s, act_id=%s',
                       self.wf_id, self.event_id, act_id)


#---------------------------------------------------------------------------
#   OptoutCO
#---------------------------------------------------------------------------
class OptoutCOMainWorkflowDecider(ParallelWorkflowDecider):
    """
    OptoutCOMainWorkflow
        |- OptoutCOChildWorkflow(siteA, partnerA)
        |- OptoutCOChildWorkflow(siteB, partnerB)
        |- OptoutCOChildWorkflow(siteC, partnerC)
    """
    def _handle_workflow_started_event(self):
        c_wf_name = self.master_helper.wfname_to_childwfattrs(self.wf_name)[0][1]

        is_c_wf_started = False
        c_wf_input = copy.deepcopy(self.event_input_dict)

        c_wf_id = util.create_workflow_id(c_wf_name, self.version)

        is_c_wf_started = True
        self.decision_helper.start_child_workflow(c_wf_id=c_wf_id,
                                                  c_wf_name=c_wf_name,
                                                  input_dict=c_wf_input)
        self._log_info('ChildWorkflow started: wf_id=%s, event_id=%s, c_wf_id=%s',
                       self.wf_id, self.event_id, c_wf_id)

        if not is_c_wf_started:
            self.decision_helper.complete_workflow(result='no optout log')
            self._log_info('Workflow completed: wf_id=%s, event_id=%s',
                           self.wf_id, self.event_id)


class OptoutCOChildWorkflowDecider(SingleActivityDecider):
    """
    OptoutCOChildWorkflow(siteA, parterA)
        |- OptoutCOActivity(siteA, parterA)
    """
    def _handle_workflow_started_event(self):
        act_name  = self.master_helper.childwfname_to_acttype(self.wf_name)
        act_input = copy.deepcopy(self.event_input_dict)
        act_id    = util.create_activity_id(act_name, self.version)

        self.decision_helper.schedule_activity_task(act_id=act_id,
                                                    act_name=act_name,
                                                    input_dict=act_input)
        self._log_info('Activity scheduled: wf_id=%s, event_id=%s, act_id=%s',
                       self.wf_id, self.event_id, act_id)


#---------------------------------------------------------------------------
#   MappingReport
#---------------------------------------------------------------------------
class MappingReportMainWorkflowDecider(SingleActivityDecider):
    """
    MappingReportMainWorkflow
        |- MappingReportActivity
    """
    def _handle_workflow_started_event(self):
        act_name  = self.master_helper.childwfname_to_acttype(self.wf_name)
        act_input = copy.deepcopy(self.event_input_dict)
        act_id    = util.create_activity_id(act_name, self.version)

        self.decision_helper.schedule_activity_task(act_id=act_id,
                                                    act_name=act_name,
                                                    input_dict=act_input)
        self._log_info('Activity scheduled: wf_id=%s, event_id=%s, act_id=%s',
                       self.wf_id, self.event_id, act_id)
