# -*- coding: utf-8 -*-

from __future__ import absolute_import

from lib.workflow.decider.base import DeciderBase


class DummyWorkflowDecider(DeciderBase):
    """
    DummyWorkflow
    """
    def _handle_workflow_started_event(self):
        self.decision_helper.complete_workflow()
        self._log_info('Workflow completed: wf_id=%s, event_id=%s',
                       self.wf_id, self.event_id)

    def _handle_workflow_cancel_requested_event(self):
        self.decision_helper.cancel_workflow(
            details='workflow cancel requested'
        )
        self._log_info('Workflow canceled: wf_id=%s, event_id=%s',
                       self.wf_id, self.event_id)
