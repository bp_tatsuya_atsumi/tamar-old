# -*- coding: utf-8 -*-

from __future__ import absolute_import

from   conf.config                 import BatchConfig
import lib.workflow.decider.helper as     helper


class DeciderBase(object):

    def __init__(self, env, event_helper, decision_helper):
        self.event_helper    = event_helper
        self.decision_helper = decision_helper

        config               = BatchConfig(env)
        self.master_helper   = helper.MasterHelper.from_config(config)

        class_name           = self.__class__.__name__
        log_prefix           = '[{0}] '.format(class_name)
        log_funcs            = config.get_workflow_decision_log_funcs(class_name, log_prefix)
        self._log_info       = log_funcs[0]
        self._log_warn       = log_funcs[1]
        self._log_error      = log_funcs[2]
        self._log_except     = log_funcs[3]

    # basic
    @property
    def domain(self):
        return self.decision_helper.domain
    @property
    def version(self):
        return self.decision_helper.version
    @property
    def task_list(self):
        return self.decision_helper.task_list

    # current workflow
    @property
    def wf_id(self):
        return self.event_helper.get_wf_id()
    @property
    def wf_name(self):
        return self.event_helper.get_wf_name()
    @property
    def events(self):
        return self.event_helper.events

    # current event
    @property
    def event(self):
        return self.event_helper.get_event()
    @property
    def event_id(self):
        return self.event_helper.get_event_id()
    @property
    def event_type(self):
        return self.event_helper.get_event_type()
    @property
    def event_attrs(self):
        return self.event_helper.get_event_attrs()
    @property
    def event_input_dict(self):
        return self.event_helper.get_event_input_dict()

    # scheduled event
    @property
    def scheduled_event_id(self):
        return self.event_helper.get_scheduled_event_id()
    @property
    def scheduled_event(self):
        return self.event_helper.get_scheduled_event()
    @property
    def scheduled_event_attrs(self):
        return self.event_helper.get_scheduled_event_attrs()

    # started event
    @property
    def started_event_id(self):
        return self.event_helper.get_started_event_id()
    @property
    def started_event(self):
        return self.event_helper.get_started_event()
    @property
    def started_event_attrs(self):
        return self.event_helper.get_started_event_attrs()

    # workflow history
    @property
    def initiated_child_workflow_id_names(self):
        return self.event_helper.get_initiated_child_workflow_id_names()
    @property
    def schedueled_act_id_names(self):
        return self.event_helper.get_scheduled_act_id_names()
    @property
    def started_act_id_names(self):
        return self.event_helper.get_started_act_id_names()
    @property
    def child_workflow_counts(self):
        return self.event_helper.count_child_workflow_events()


    # decide
    def decide(self):
        if helper.is_workflow_started_event(self.event_type):                           self._handle_workflow_started_event()
        if helper.is_workflow_completed_event(self.event_type):                         self._handle_workflow_completed_event()
        if helper.is_workflow_failed_event(self.event_type):                            self._handle_workflow_failed_event()
        if helper.is_workflow_timed_out_event(self.event_type):                         self._handle_workflow_timed_out_event()
        if helper.is_workflow_canceled_event(self.event_type):                          self._handle_workflow_canceled_event()
        if helper.is_workflow_terminated_event(self.event_type):                        self._handle_workflow_terminated_event()
        if helper.is_workflow_continued_as_new_event(self.event_type):                  self._handle_workflow_continued_as_new_event()
        if helper.is_workflow_cancel_requested_event(self.event_type):                  self._handle_workflow_cancel_requested_event()
        if helper.is_workflow_signaled_event(self.event_type):                          self._handle_workflow_signaled_event()
        if helper.is_decision_task_scheduled_event(self.event_type):                    self._handle_decision_task_scheduled_event()
        if helper.is_decision_task_started_event(self.event_type):                      self._handle_decision_task_started_event()
        if helper.is_decision_task_completed_event(self.event_type):                    self._handle_decision_task_completed_event()
        if helper.is_decision_task_timed_out_event(self.event_type):                    self._handle_decision_task_timed_out_event()
        if helper.is_activity_task_scheduled_event(self.event_type):                    self._handle_activity_task_scheduled_event()
        if helper.is_activity_task_schedule_failed_event(self.event_type):              self._handle_activity_task_schedule_failed_event()
        if helper.is_activity_task_started_event(self.event_type):                      self._handle_activity_task_started_event()
        if helper.is_activity_task_completed_event(self.event_type):                    self._handle_activity_task_completed_event()
        if helper.is_activity_task_failed_event(self.event_type):                       self._handle_activity_task_failed_event()
        if helper.is_activity_task_timed_out_event(self.event_type):                    self._handle_activity_task_timed_out_event()
        if helper.is_activity_task_canceled_event(self.event_type):                     self._handle_activity_task_canceled_event()
        if helper.is_activity_task_cancel_requested_event(self.event_type):             self._handle_activity_task_cancel_requested_event()
        if helper.is_activity_task_cancel_request_failed_event(self.event_type):        self._handle_activity_task_cancel_request_failed_event()
        if helper.is_marker_recorded_event(self.event_type):                            self._handle_marker_recorded_event()
        if helper.is_timer_started_event(self.event_type):                              self._handle_timer_started_event()
        if helper.is_timer_start_failed_event(self.event_type):                         self._handle_timer_start_failed_event()
        if helper.is_timer_fired_event(self.event_type):                                self._handle_timer_fired_event()
        if helper.is_timer_canceled_event(self.event_type):                             self._handle_timer_canceled_event()
        if helper.is_timer_cancel_failed_event(self.event_type):                        self._handle_timer_cancel_failed_event()
        if helper.is_child_workflow_start_initiated_event(self.event_type):             self._handle_child_workflow_start_initiated_event()
        if helper.is_child_workflow_start_failed_event(self.event_type):                self._handle_child_workflow_start_failed_event()
        if helper.is_child_workflow_started_event(self.event_type):                     self._handle_child_workflow_started_event()
        if helper.is_child_workflow_completed_event(self.event_type):                   self._handle_child_workflow_completed_event()
        if helper.is_child_workflow_failed_event(self.event_type):                      self._handle_child_workflow_failed_event()
        if helper.is_child_workflow_timed_out_event(self.event_type):                   self._handle_child_workflow_timed_out_event()
        if helper.is_child_workflow_canceled_event(self.event_type):                    self._handle_child_workflow_canceled_event()
        if helper.is_child_workflow_terminated_event(self.event_type):                  self._handle_child_workflow_terminated_event()
        if helper.is_external_workflow_signal_initiated_event(self.event_type):         self._handle_external_workflow_signal_initiated_event()
        if helper.is_external_workflow_signaled_event(self.event_type):                 self._handle_external_workflow_signaled_event()
        if helper.is_external_workflow_signal_failed_event(self.event_type):            self._handle_external_workflow_signal_failed_event()
        if helper.is_external_workflow_cancel_request_initiated_event(self.event_type): self._handle_external_workflow_cancel_request_initiated_event()
        if helper.is_external_workflow_cancel_requested_event(self.event_type):         self._handle_external_workflow_cancel_requested_event()
        if helper.is_external_workflow_cancel_request_failed_event(self.event_type):    self._handle_external_workflow_cancel_request_failed_event()

    def _handle_workflow_started_event(self):                           pass
    def _handle_workflow_completed_event(self):                         pass
    def _handle_workflow_failed_event(self):                            pass
    def _handle_workflow_timed_out_event(self):                         pass
    def _handle_workflow_canceled_event(self):                          pass
    def _handle_workflow_terminated_event(self):                        pass
    def _handle_workflow_continued_as_new_event(self):                  pass
    def _handle_workflow_cancel_requested_event(self):                  pass
    def _handle_workflow_signaled_event(self):                          pass
    def _handle_decision_task_scheduled_event(self):                    pass
    def _handle_decision_task_started_event(self):                      pass
    def _handle_decision_task_completed_event(self):                    pass
    def _handle_decision_task_timed_out_event(self):                    pass
    def _handle_activity_task_scheduled_event(self):                    pass
    def _handle_activity_task_schedule_failed_event(self):              pass
    def _handle_activity_task_started_event(self):                      pass
    def _handle_activity_task_completed_event(self):                    pass
    def _handle_activity_task_failed_event(self):                       pass
    def _handle_activity_task_timed_out_event(self):                    pass
    def _handle_activity_task_canceled_event(self):                     pass
    def _handle_activity_task_cancel_requested_event(self):             pass
    def _handle_activity_task_cancel_request_failed_event(self):        pass
    def _handle_marker_recorded_event(self):                            pass
    def _handle_timer_started_event(self):                              pass
    def _handle_timer_start_failed_event(self):                         pass
    def _handle_timer_fired_event(self):                                pass
    def _handle_timer_canceled_event(self):                             pass
    def _handle_timer_cancel_failed_event(self):                        pass
    def _handle_child_workflow_start_initiated_event(self):             pass
    def _handle_child_workflow_start_failed_event(self):                pass
    def _handle_child_workflow_started_event(self):                     pass
    def _handle_child_workflow_completed_event(self):                   pass
    def _handle_child_workflow_failed_event(self):                      pass
    def _handle_child_workflow_timed_out_event(self):                   pass
    def _handle_child_workflow_canceled_event(self):                    pass
    def _handle_child_workflow_terminated_event(self):                  pass
    def _handle_external_workflow_signal_initiated_event(self):         pass
    def _handle_external_workflow_signaled_event(self):                 pass
    def _handle_external_workflow_signal_failed_event(self):            pass
    def _handle_external_workflow_cancel_request_initiated_event(self): pass
    def _handle_external_workflow_cancel_requested_event(self):         pass
    def _handle_external_workflow_cancel_request_failed_event(self):    pass
