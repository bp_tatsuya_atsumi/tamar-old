# -*- coding: utf-8 -*-

from __future__ import absolute_import

import datetime
import json
import os
import socket


def create_poller_identity():
    hostname = socket.gethostname()
    pid      = os.getpid()
    return kwargs_to_json(hostname=hostname, pid=pid)

def create_workflow_id(workflow_type, version, *args):
    return _create_id(workflow_type.replace('Workflow', ''), version, *args)

def create_activity_id(activity_type, version, *args):
    return _create_id(activity_type.replace('Activity', ''), version, *args)

def _create_id(name, version, *args):
    ids = [name, version]
    if args:
        ids.extend(args)
    now = datetime.datetime.now()
    ids.append(now.strftime('%Y%m%d-%H%M%S.') +
               '{:>03d}'.format(now.microsecond / 1000))
    return '-'.join(ids)

def dict_to_json(d):
    return json.dumps(d, separators=(',', ':'))

def kwargs_to_json(**kwargs):
    return dict_to_json(kwargs)
