# -*- coding: utf-8 -*-

"""
外部セグメントの共通クラス
"""

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'production'

from   conf.config                 import BatchConfig
from   flask                       import request
import flask_restful
from   flask.ext.httpauth          import HTTPBasicAuth
from   lib.partner_api.rt.rtoaster import RtoasterExtAPI
from   passlib.apps                import custom_app_context as pwd_context
import sys

class ExtAPIBase(flask_restful.Resource):

    environments = ['prd', 'stg', 'dev']
    env = sys.argv[1] if len(sys.argv) > 1 and sys.argv[1] in environments else 'dev'
    config = BatchConfig(env)
    rtoaster_api = RtoasterExtAPI(env)

    auth = HTTPBasicAuth()

    @staticmethod
    @auth.verify_password
    def verify_password(username, password):
        """
        Authorization ヘッダの Basic 認証情報から username / password を取得し
        ハッシュ化されたパスワードと比較する
        """
        try:
            pid = request.args.get('pid') if request.args.has_key('pid') else request.json.get('pid')
            sid = request.args.get('sid') if request.args.has_key('sid') else request.json.get('sid')
            if pid not in ExtAPIBase.config.mapping[sid]['rt']['receives']:
                return False

            partner_mst     = ExtAPIBase.config.partner_mst[pid]
            registered_user = partner_mst['option']['external']['user']
            hashed_pass     = partner_mst['option']['external']['pass']
        except:
            return False

        return registered_user == username and pwd_context.verify(password, hashed_pass)
