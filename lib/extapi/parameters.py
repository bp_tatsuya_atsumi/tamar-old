# -*- coding: utf-8 -*-

import datetime
import lib.extapi.errors as errors

class Parameter(object):
    def __init__(self, value):
        self.value = value

class PID(Parameter):
    def __init__(self, pid):
        Parameter.__init__(self, pid)
        if not (pid and isinstance(pid, unicode)):
            raise errors.IllegalParameter('pid must be unicode.')

class SID(Parameter):
    def __init__(self, sid):
        Parameter.__init__(self, sid)
        if not (sid and isinstance(sid, unicode)):
            raise errors.IllegalParameter('sid must be unicode.')

class SegID(Parameter):
    def __init__(self, seg_id):
        Parameter.__init__(self, seg_id)
        if not (seg_id and isinstance(seg_id, unicode)):
            raise errors.IllegalParameter('seg_id must be unicode.')
        try:
            seg_id.encode('ascii', 'strict')
        except UnicodeEncodeError:
            raise errors.IllegalParameter('seg_id must not contain multi-byte characters.')
        if len(seg_id) > 50:
            raise errors.IllegalParameter('seg_id must be shorter than 50 characters.')

class Name(Parameter):
    def __init__(self, name):
        Parameter.__init__(self, name)
        if not (name and isinstance(name, unicode)):
            raise errors.IllegalParameter('name must be unicode.')
        if len(name) > 50:
            raise errors.IllegalParameter('name must be shorter than 50 characters.')

class Description(Parameter):
    def __init__(self, description):
        Parameter.__init__(self, description)
        if description is not None and not isinstance(description, unicode):
            raise errors.IllegalParameter('description must be unicode.')
        if description is not None and len(description) > 1000:
            raise errors.IllegalParameter('description must be shorter than 1000 characters.')

class UIDs(Parameter):
    def __init__(self, uids):
        Parameter.__init__(self, uids)
        if uids is not None and not isinstance(uids, list):
            raise errors.IllegalParameter('uids must be list.')
        if uids is not None and len(uids) > 500:
            raise errors.IllegalParameter('length of uids must be less than 500 users.')

class TTL(Parameter):
    def __init__(self, ttl):
        Parameter.__init__(self, ttl)
        try:
            datetime.datetime.fromtimestamp(ttl)
        except:
            raise errors.IllegalParameter('ttl is invalid')
