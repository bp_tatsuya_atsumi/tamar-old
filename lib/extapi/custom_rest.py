# -*- coding: utf-8 -*-

import flask_restful
from   werkzeug.exceptions   import HTTPException
import lib.extapi.errors     as errors

class CustomRestAPI(flask_restful.Api):
    def handle_error(self, e):
        if isinstance(e, HTTPException):
            code = e.code
            data = {'status_code': e.code, 'message': e.description}
        else:
            # TODO 元のエラーをロギングする
            _e = errors.InternalError()
            code = _e.code
            data = {'status_code': _e.code, 'message': _e.description}
        return self.make_response(data, code)
