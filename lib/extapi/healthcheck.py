# -*- coding: utf-8 -*-

"""
uwsgi のヘルスチェック用クラス
"""

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'production'

from flask import jsonify
import flask_restful

class HealthCheckAPI(flask_restful.Resource):
    def get(self):
        return jsonify(healthcheck='OK')
