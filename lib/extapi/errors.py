# -*- coding: utf-8 -*-
from werkzeug.exceptions import HTTPException

class IllegalParameter(HTTPException):
    code = 400
    description = 'Illegal Parameter'

class OverLimit(HTTPException):
    code = 400
    description = 'The number of registered segments reached the limit.'

class SegmentDuplicated(HTTPException):
    code = 400
    description = 'Given segment is already registered.'

class SegmentNotFound(HTTPException):
    code = 404
    description = 'Segment Not Found'

class PostFailed(HTTPException):
    code = 500
    description = 'FAILED to add a new segment.'

class UpdateFailed(HTTPException):
    code = 500
    description = 'FAILED to update the segment.'

class DeleteFailed(HTTPException):
    code = 500
    description = 'FAILED to delete the segment.'

class StatusConflicted(HTTPException):
    code = 500
    description = 'FAILED: status conflicted.'

class InternalError(HTTPException):
    code = 500
    description = 'FAILED: system internal error.'
