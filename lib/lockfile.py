# -*- coding: utf-8 -*-

import os
from   conf.config import BatchConfig


class LockFile():
    def __init__(self, key, env=None):
        config         = BatchConfig(env)
        self.lock_file = os.path.join(config.lock_dir, key + '.lock')

    def __enter__(self):
        if not self.lock():
            raise AlreadyLocked()
        return self

    def __exit__(self, exec_type, exec_value, traceback):
        self.unlock()

    def is_running(self):
        return os.path.exists(self.lock_file)

    def lock(self):
        if self.is_running():
            return False
        open(self.lock_file,'w').close()
        return True

    def unlock(self):
        if self.is_running():
            os.remove(self.lock_file)
            return True
        return False

class AlreadyLocked(Exception):
    pass


class LastUpdateFile():
    def __init__(self, key, env=None):
        config                = BatchConfig(env)
        self.last_update_file = os.path.join(config.lock_dir, key + '_last_update.lock')
        self.logger           = config.logger_transfer

    def update_last_transfer_filename(self, current_filename):
        try:
            fp = open(self.last_update_file, 'w')
            fp.write(current_filename)
            fp.close()
            return True
        except:
            return False

    def get_last_update_filename(self):
        try:
            fp = open(self.last_update_file, 'r')
            last_transfer_file = fp.read()
            fp.close()
            return last_transfer_file
        except:
            return None

    def is_latest(self, current_transfer_filename):
        last_transfer_file = self.get_last_update_filename()
        if last_transfer_file:
            if last_transfer_file == current_transfer_filename:
                return True
            elif last_transfer_file < current_transfer_filename:
                self.update_last_transfer_filename(current_transfer_filename)
                return False
            else:
                raise Exception, 'Something wrong in S3'
        else:
            self.update_last_transfer_filename(current_transfer_filename)
            return False
