# -*- coding: utf-8 -*-

import datetime
import MySQLdb
from   sqlalchemy                import bindparam, create_engine, text
from   sqlalchemy                import Table, Column, MetaData
from   sqlalchemy.dialects.mysql import BOOLEAN, DATETIME, TINYINT, VARCHAR
from   sqlalchemy.sql            import select


TABLE_NAME = 't_user_mapping_{sid}_{pid}'

def open(sid, pid, db_config):
    return MappingTable(db_config['user'],
                        db_config['password'],
                        db_config['host'],
                        db_config['db_name'],
                        TABLE_NAME.format(sid=sid, pid=pid))

class MappingTable():
    PARTNER_UID_INDEX = 1

    def __enter__(self):
        self.trans = self.conn.begin()
        return self

    def __exit__(self, exec_type, exec_value, traceback):
        if not exec_type:
            self.trans.commit()
            return True
        else:
            self.trans.rollback()
            return False

    def __del__(self):
        self.conn.close()

    def __init__(self, user, password, host, db_name, table_name):
        engine_str = 'mysql://{user}:{password}@{host}/{db_name}?charset=utf8'\
                        .format(user=user,
                                password=password,
                                host=host,
                                db_name=db_name)

        self.engine = create_engine(engine_str, encoding='utf8')
        self.metadata = MetaData()

        self.table_name = table_name
        self.table = Table(table_name, self.metadata,
                           Column('user_id',         VARCHAR(length=50), primary_key=True),
                           Column('partner_user_id', VARCHAR(length=50), primary_key=True),
                           Column('tamar_user_type', TINYINT(unsigned=True)),
                           Column('tamar_user_id',   VARCHAR(length=50)),
                           Column('inserted_at',     DATETIME),
                           Column('updated_at',      DATETIME),
                           Column('is_deleted',      BOOLEAN))
        self.table_alias = self.table.alias('t')
        self.metadata.create_all(self.engine)
        self.conn = self.engine.connect()
        self.conn.execute("SET TIME_ZONE = '+9:00'")

        self._prepare_sqls()

    def _prepare_sqls(self):
        self.sqltext_select_ptuids_by_uid = text(
            select([self.table_alias.c.partner_user_id])\
                .where(self.table_alias.c.user_id == bindparam('user_id'))\
                .compile().string
        )
        self.sqltext_select_user_ids_by_ptuid = text(
            select([self.table_alias.c.user_id])\
                .where(self.table_alias.c.partner_user_id == bindparam('partner_user_id'))\
                .compile().string
        )
        self.sqltext_select_latest_mapping_partner_uid_by_user_id_until_n_days = text(
            select([self.table_alias.c.partner_user_id])\
                .where(self.table_alias.c.user_id == bindparam('user_id'))\
                .where(self.table_alias.c.updated_at >= bindparam('n_days_ago'))\
                .order_by(self.table_alias.c.inserted_at.asc())\
                .compile().string
        )

    def select_rows_by_user_id(self, user_id):
        select_statement = select([self.table])\
                            .where(self.table.c.user_id == user_id)
        return self.conn.execute(select_statement).fetchall()

    def select_rows_by_partner_user_id(self, partner_user_id):
        select_statement = select([self.table])\
                            .where(self.table.c.partner_user_id == partner_user_id)
        return self.conn.execute(select_statement).fetchall()

    def select_single_row_by_user_id_order_by_updated_at(self, user_id):
        select_statement = select([self.table])\
                            .where(self.table.c.user_id == user_id)\
                            .order_by(self.table.c.updated_at.desc())
        return self.conn.execute(select_statement).fetchone()

    def select_single_row_by_user_id_order_by_inserted_at(self, user_id):
        select_statement = select([self.table])\
                            .where(self.table.c.user_id == user_id)\
                            .order_by(self.table.c.inserted_at.desc())
        return self.conn.execute(select_statement).fetchone()

    def select_latest_mapping_partner_uid_by_user_id(self, user_id):
        select_statement = select([self.table])\
                            .where(self.table.c.user_id == user_id)\
                            .order_by(self.table.c.inserted_at.desc())
        result = self.conn.execute(select_statement).fetchone()
        if result:
            return result[self.PARTNER_UID_INDEX]
        return result

    def select_latest_mapping_partner_uid_by_user_id_until_n_days(self, user_id, days_ago):
        row = self.conn.execute(
                  self.sqltext_select_latest_mapping_partner_uid_by_user_id_until_n_days,
                  user_id=user_id,
                  n_days_ago=(datetime.datetime.now() + datetime.timedelta(days=-days_ago))
              ).fetchone()
        return row[0] if row else row

    def select_partner_uids_by_user_id(self, user_id):
        return [row[0] for row in self.conn.execute(self.sqltext_select_ptuids_by_uid, user_id=user_id).fetchall()]

    def select_user_ids_by_partner_user_id(self, partner_user_id):
        return [row[0] for row in self.conn.execute(self.sqltext_select_user_ids_by_ptuid, partner_user_id=partner_user_id).fetchall()]

    def select_all_rows(self):
        select_statement = select([self.table])
        return self.conn.execute(select_statement).fetchall()

    def insert_row(self, user_id, partner_user_id, tamar_user_type,
                   tamar_user_id, inserted_at, updated_at, is_deleted):
        ins = self.table.insert().values(
                    user_id=user_id,
                    partner_user_id=partner_user_id,
                    tamar_user_type=tamar_user_type,
                    tamar_user_id=tamar_user_id,
                    inserted_at=inserted_at,
                    updated_at=updated_at,
                    is_deleted=is_deleted
              )
        result = self.conn.execute(ins)
        return result.is_insert

    def upsert_row(self, user_id, partner_user_id, tamar_user_type, tamar_user_id, is_deleted=0):
        sql_upsert_mapping_table = """
            INSERT INTO `{mapping_table}` (
                user_id,
                partner_user_id,
                tamar_user_type,
                tamar_user_id,
                inserted_at,
                updated_at,
                is_deleted
            ) VALUES (
                '{user_id}',
                '{partner_user_id}',
                {tamar_user_type},
                '{tamar_user_id}',
                CURRENT_TIMESTAMP,
                CURRENT_TIMESTAMP,
                {is_deleted}
            ) ON DUPLICATE KEY UPDATE
                updated_at      = CURRENT_TIMESTAMP,
                tamar_user_type = {tamar_user_type},
                tamar_user_id   = '{tamar_user_id}',
                is_deleted      = {is_deleted}
        """
        query = sql_upsert_mapping_table.format(mapping_table=self.table_name,
                                                user_id=user_id,
                                                partner_user_id=partner_user_id,
                                                tamar_user_type=tamar_user_type,
                                                tamar_user_id=tamar_user_id,
                                                is_deleted=is_deleted)
        self.conn.execute(query)

    def _delete_rows_by_ids(self, target_column, ids):
        count = 0
        for id in ids:
            delete = self.table.delete().where(target_column == id)
            result = self.conn.execute(delete)
            count += result.rowcount
        return count

    def delete_rows_by_user_id(self, user_id):
        return self._delete_rows_by_ids(self.table.c.user_id, [user_id])

    def delete_rows_by_user_ids(self, user_ids):
        return self._delete_rows_by_ids(self.table.c.user_id, user_ids)

    def delete_rows_by_partner_user_ids(self, partner_user_ids):
        return self._delete_rows_by_ids(self.table.c.partner_user_id, partner_user_ids)

    def truncate_table(self):
        trans = self.conn.begin()
        self.conn.execute(self.table.delete())
        trans.commit()
        return True

