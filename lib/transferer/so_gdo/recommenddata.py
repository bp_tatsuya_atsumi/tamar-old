# -*- coding: utf-8 -*-

import datetime
import gzip
import json
import os
import lib.sftputil             as     sftputil
from   lib.transferer.base      import TransfererBase
from   lib.transferer.exception import NoS3Files
import lib.transferer.rtfile    as     rtfile
import lib.transferer.so.soutil as     soutil
import lib.util                 as     util


class RecommendDataTransferer(TransfererBase):
    SFTP_CONFIG = {
                    'dev' : {
                                'host'       : 'sftp-s.c-ovn.jp',
                                'port'       : 115,
                                'username'   : 'rtoaster9999',
                                'password'   : 'rtoaster9999',
                                'remote_dir' : '/receive/so/extra'
                            },
                    'stg' : {
                                'host'       : 'so-test.c-ovn.jp',
                                'port'       : 115,
                                'username'   : 'brainpad-so',
                                'pkey_file'  : 'virtual_partner/ssh_key/stg_scaleout_id_rsa',
                                'remote_dir' : 'extra'
                            },
                    'prd' : {
                                'host'       : 'up.scaleout.jp',
                                'port'       : 115,
                                'username'   : 'brainpad',
                                'pkey_file'  : 'settings/site/batch/ami/scaleout_id_dsa',
                                'remote_dir' : '/extra'
                            }
                  }

    def __init__(self, sid, pid, env, s3bucket, s3path, **option):
        TransfererBase.__init__(self, sid, pid, env, 'recommenddata',
                                s3bucket, s3path, **option)

        self.seqno      = self.option.get('seqno', 1)
        self.aid        = self.mapping_auth['aid']
        self.foreign_id = self.mapping_auth['foreign_id']

    def execute(self):
        self.logger.info('<Start recommenddata transfer process>')
        target_file, transfer_file = None, None

        try:
            self.logger.info('Downloading s3file')
            target_file, target_filename = self._download_s3file()
            self.result.target_filename  = target_filename

            transfer_file, transfer_filename = self._create_transfer_filename()
            self.logger.info('Transfer filename: {0}'.format(transfer_filename))

            self.logger.info('Creating transfer file')
            self._create_transfer_file(target_file, transfer_file)

            self.logger.info('Saving transfer file')
            self._save_converted_file(transfer_file)

            self.logger.info('Sending transfer file')
            if self._send_transfer_file(transfer_file):
                if self.result.counter.get('line.invalid') > 0:
                    return self.result.api_error('Some input error')
                return self.result.ok('OK')
            else:
                return self.result.ng('Sftp error')

        except NoS3Files, e:
            msg = 'File not exists in s3: {0} {1}'.format(e.s3bucket, e.s3path)
            self.logger.error(msg)
            return self.result.ng(msg)
        except:
            self.logger.exception('Failed to transfer')
            return self.result.ng('Unexpected error occured')
        finally:
            self.logger.info('Removing tmpfile')
            util.remove_files([target_file, transfer_file])

        self.logger.info('<End recommenddata transfer process>')

    def _create_transfer_filename(self):
        transfer_file = soutil.create_transfer_filename(
                            self.tmp_dir,
                            self.aid,
                            self.foreign_id,
                            filedatetime=datetime.datetime.now(),
                            filetype='recommend',
                            seqno=self.seqno
                        )
        return transfer_file, os.path.basename(transfer_file)

    def _create_transfer_file(self, target_file, transfer_file):
        with gzip.open(transfer_file, 'wb') as outf, \
             rtfile.open_recommenddata(target_file,
                                       self._open_mappingtable(),
                                       oldest_user=False) as reader:
            for row in reader:
                outf.write(self._transform_transfer_format(row) + '\n')

        self.result.increment_counter('user.rtoaster', reader.line_count_all)
        self.result.increment_counter('user.mapped',   reader.line_count_mapped)
        self.result.increment_counter('user.unmapped', reader.line_count_unmapped)
        self.result.increment_counter('user.invalid',  reader.line_count_invalid)

    def _transform_transfer_format(self, row):
        recs = []
        for mapped_uid in row.pop('mapped_uids'):
            row['aid'] = self.aid
            row['sid'] = self.foreign_id
            row['uid'] = mapped_uid
            recs.append(json.dumps(row, ensure_ascii=False,
                                   separators=(',', ':')).encode('utf-8'))
        return '\n'.join(recs)

    def _send_transfer_file(self, transfer_file):
        try:
            sftp_config = self.SFTP_CONFIG[self.env]
            sftputil.put(host=sftp_config['host'],
                         port=sftp_config['port'],
                         username=sftp_config['username'],
                         password=sftp_config.get('password'),
                         pkey_file=sftp_config.get('pkey_file'),
                         local_file=transfer_file,
                         remote_dir=sftp_config['remote_dir'],
                         rename=True)
        except:
            self.logger.exception('Failed to sftp put')
            return False
        else:
            return True
