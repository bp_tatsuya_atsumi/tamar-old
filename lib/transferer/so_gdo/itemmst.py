# -*- coding: utf-8 -*-

import gzip
import os
import re
from   lib.lockfile                   import LastUpdateFile
import lib.sftputil                   as     sftputil
from   lib.transferer.base            import TransfererBase
from   lib.transferer.exception       import NoS3Files
import lib.transferer.so.soutil       as     soutil
from   lib.transferer.transfer_result import TransferResult
import lib.util                       as     util


class ItemMstTransferer(TransfererBase):
    SFTP_CONFIG = {
                    'dev' : {
                                'host'       : 'sftp-s.c-ovn.jp',
                                'port'       : 115,
                                'username'   : 'rtoaster9999',
                                'password'   : 'rtoaster9999',
                                'remote_dir' : '/receive/so/content'
                            },
                    'stg' : {
                                'host'       : 'so-test.c-ovn.jp',
                                'port'       : 115,
                                'username'   : 'brainpad-so',
                                'pkey_file'  : 'virtual_partner/ssh_key/stg_scaleout_id_rsa',
                                'remote_dir' : 'content'
                            },
                    'prd' : {
                                'host'       : 'up.scaleout.jp',
                                'port'       : 115,
                                'username'   : 'brainpad',
                                'pkey_file'  : 'settings/site/batch/ami/scaleout_id_dsa',
                                'remote_dir' : '/content'
                            }
                  }

    def __init__(self, sid, pid, env, s3bucket, s3path, **option):
        TransfererBase.__init__(self, sid, pid, env, 'itemmst', s3bucket, s3path, **option)

        self.aid         = self.mapping_auth['aid'].encode('utf8')
        self.foreign_id  = self.mapping_auth['foreign_id'].encode('utf8')
        self.s3_path_cnv = self.s3path.replace('/raw', '/converted/' + pid)

        lastupdate_key       = 'itemmst_{sid}_{pid}'.format(sid=self.sid, pid=pid)
        self.lastupdate_file = LastUpdateFile(lastupdate_key, env)

    def execute(self):
        self.logger.info('<Start itemmst transfer process>')
        target_file, transfer_file = None, None

        try:
            result = TransferResult(self.sid, self.pid, self.env, 'itemmst')

            # download s3file
            self.logger.info('Downloading latest item master')
            target_file, target_filename = self._download_latest_s3file(self.s3path)
            result.target_filename = target_filename

            # check lastupdate
            self.logger.info('Checking lastupdate')
            if self.lastupdate_file.is_latest(target_filename):
                message = 'Already transfered: ' + target_filename
                self.logger.info(message)
                return result.ok(message)

            # create transfer filename
            transfer_file = soutil.create_transfer_filename(self.tmp_dir,
                                                            self.aid,
                                                            self.foreign_id,
                                                            filetype='item')
            self.logger.info('Transfer filename: ' + os.path.basename(transfer_file))

            # create transfer file
            self.logger.info('Creating transfer file')
            self._create_transfer_file(target_file, transfer_file)

            # save file
            self.logger.info('Saving transfer file')
            self.s3util.upload_single_file(transfer_file, self.s3bucket, self.s3_path_cnv)

            # send file
            self.logger.info('Sending transfer file')
            if not self._send_transformed_file(transfer_file):
                return result.ng('Sftp error')
            return result.ok('OK')

        except NoS3Files, e:
            msg = 'File not exists in s3: {0} {1}'.format(e.s3bucket, e.s3path)
            self.logger.error(msg)
            return result.ng(msg)

        except:
            self.logger.exception('Failed to transfer')
            return result.ng('Unexpected error occured')

        finally:
            self.logger.info('Removing tmpfile')
            util.remove_files([target_file, transfer_file])

        self.logger.info('<End itemmst transfer process>')

    def _create_transfer_file(self, target_file, transfer_file):
        with gzip.open(transfer_file, 'wb') as transferf:
             target_str = gzip.open(target_file).read()
             target_str = re.sub(r'"aid"\s*:\s*"[^"]*"',
                                 r'"aid":"{aid}"'.format(aid=self.aid),
                                 target_str)
             target_str = re.sub(r'"sid"\s*:\s*"[^"]*"',
                                 r'"sid":"{sid}"'.format(sid=self.foreign_id),
                                 target_str)
             transferf.write(target_str)

    def _send_transformed_file(self, local_file):
        try:
            sftp_config = self.SFTP_CONFIG[self.env]
            sftputil.put(host=sftp_config['host'],
                         port=sftp_config['port'],
                         username=sftp_config['username'],
                         password=sftp_config.get('password'),
                         pkey_file=sftp_config.get('pkey_file'),
                         local_file=local_file,
                         remote_dir=sftp_config['remote_dir'],
                         rename=True)
        except:
            self.logger.exception('Failed to sftp put')
            return False
        else:
            return True

