# -*- coding: utf-8 -*-

from __future__ import absolute_import

"""
転送バッチ共通クラス
"""

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'production'

from   conf.config                    import BatchConfig
import lib.mappingtable               as     mappingtable
from   lib.transferer.exception       import NoS3Files
from   lib.s3util                     import S3Util
from   lib.transferer.transfer_result import TransferResult
import os

class TransfererBase(object):

    def __init__(self, sid, pid, env, data_type, s3bucket, s3path, **option):
        self.sid       = sid
        self.pid       = pid
        self.env       = env
        self.data_type = data_type
        self.s3bucket  = s3bucket
        self.s3path    = s3path
        self.option    = option

        self.config    = BatchConfig(env)
        self.s3util    = S3Util()
        self.result    = TransferResult(sid, pid, env, data_type)

        self._setup_logging()

    @property
    def tmp_dir(self):
        return self.config.tmp_transfer_dir.format(
                   sid=self.sid, pid=self.pid, data_type=self.data_type
               )

    @property
    def site_config(self):
        return self.config.site_mst[self.sid]
    @property
    def partner_config(self):
        return self.config.partner_mst[self.pid]
    @property
    def mapping_config(self):
        return self.config.mapping[self.sid][self.pid]
    @property
    def db_config(self):
        return self.config.database[self.sid]

    @property
    def mapping_ttl(self):
        return self.mapping_config['ttl']
    @property
    def mapping_auth(self):
        return self.mapping_config['auth']
    @property
    def partner_option(self):
        return self.partner_config['option']

    def execute(self):
        raise NotImplementedError()

    def _setup_logging(self):
        datatype_to_logprefixdatatype = {
            'segmentmst'          : 'SegmentMst',
            'remove_segmentmst'   : 'RemoveSegmentMst',
            'segmentdata'         : 'SegmentData',
            'conversion'          : 'Conversion',
            'itemmst'             : 'ItemMst',
            'recommendmst'        : 'RecommendMst',
            'remove_recommendmst' : 'RemoveRecommendMst',
            'recommenddata'       : 'RecommendData',
            'mailrequest'         : 'MailRequest',
            'mailresponse'        : 'MailResponse',
            'mailsegmentdata'     : 'MailSegmentData'
        }
        log_prefix    = '[{data_type} {sid} {pid}] '.format(
                            data_type=datatype_to_logprefixdatatype[self.data_type],
                            sid=self.sid,
                            pid=self.pid
                        )
        logger_suffix = '{sid}.{pid}.{data_type}'.format(
                            sid=self.sid,
                            pid=self.pid,
                            data_type=self.data_type
                        )
        self.logger   = self.config.get_prefixed_transfer_logger(logger_suffix, log_prefix)

    def _open_mappingtable(self):
        return mappingtable.open(self.sid, self.pid, self.db_config)

    def _download_s3file(self):
        s3obj = self.s3util.download_single_file(self.s3bucket, self.s3path, self.tmp_dir)
        if not s3obj:
            raise NoS3Files(self.s3bucket, self.s3path)
        target_filename = os.path.basename(s3obj['key'])
        return os.path.join(self.tmp_dir, target_filename), target_filename

    def _download_latest_s3file(self, s3path):
        s3obj = self.s3util.download_single_file_order_by_filename(self.s3bucket, s3path, self.tmp_dir)
        if not s3obj:
            raise NoS3Files(self.s3bucket, s3path)
        target_filename = os.path.basename(s3obj['key'])
        return os.path.join(self.tmp_dir, target_filename), target_filename

    def _save_converted_file(self, converted_file):
        s3dir_converted = os.path.dirname(self.s3path).replace(
                              '/raw',
                              '/converted/{pid}'.format(pid=self.pid)
                          )
        self.s3util.upload_single_file(converted_file, self.s3bucket,
                                       s3dir_converted)
