# -*- coding: utf-8 -*-

"""
Rtoaster へのリクエストファイルを生成するバッチ
"""

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'production'

import contextlib
import csv
import datetime
import gzip
import os
import re
import traceback
import lib.sftputil             as     sftputil
from   lib.transferer.base      import TransfererBase
from   lib.transferer.exception import NoS3Files
import lib.util                 as     util


class MailRequestTransferer(TransfererBase):

    SFTP_CONFIG = {
        'dev' : {
            'host'       : 'sftp-s.c-ovn.jp',
            'port'       : 115,
            'username'   : 'rtoaster9999',
            'password'   : 'rtoaster9999',
            'remote_dir' : '/receive/rs/rt_request'
        },
        'stg' : {
            'host'       : 'sftp-s.c-ovn.jp',
            'port'       : 115,
            'username'   : 'rtoaster0019',
            'password'   : 'rtoaster0019',
            'remote_dir' : 'request'
        },
        'prd' : {
            'host'       : 'sftp-1.c-ovn.jp',
            'port'       : 115,
            'username'   : 'rtoaster0019',
            'password'   : 'rtoaster0019',
            'remote_dir' : 'request'
        }
    }

    def __init__(self, sid, pid, env, s3bucket, s3path, **option):
        super(MailRequestTransferer, self).__init__(
            sid, pid, env, 'mailrequest', s3bucket, s3path, **option
        )

    def execute(self):
        self.logger.info('<Start mailrequest transfer process>')
        target_file, transfer_file = None, None

        try:
            self.logger.info('Downloading s3file')
            target_file, target_filename = self._download_s3file()

            self.logger.info('Target filename: %s', target_filename)
            self.result.target_filename = target_filename

            transfer_file, transfer_filename = self._create_transfer_filename(target_filename)
            self.logger.info('Transfer filename: %s', transfer_filename)

            self.logger.info('Creating transfer file')
            self._create_transfer_file(target_file, transfer_file)

            self.logger.info('Saving transfer file')
            self._save_converted_file(transfer_file)

            self.logger.info('Sending transfer file')
            if not self._send_transfer_file(transfer_file):
                return self.result.ng('Sftp error')
            return self.result.ok('OK')

        except NoS3Files, e:
            msg = 'File not exists in s3: bucket={}, path={}'.format(e.s3bucket, e.s3path)
            self.logger.error(msg)
            return self.result.ng(msg)
        except:
            st = traceback.format_exc().splitlines()[-1]
            self.logger.exception('Failed to transfer: %s', st)
            return self.result.ng('Unexpected error: {}'.format(st))
        finally:
            self.logger.info('Removing tmpfile')
            util.remove_files([target_file, transfer_file])

            self.logger.info('<End mailrequest transfer process>')


    def _create_transfer_filename(self, target_filename):
        # e.g. 20140224_recommend.csv -> recommend_20140224_20140225202204.csv.gz
        r = re.compile('(?P<ymd>\\d{8})_(?P<recommend_pattern>\\w+).csv')
        m = r.match(target_filename)
        if not m:
            raise ValueError('Invalid filename: {}'.format(target_filename))

        transfer_filename = '{recommend_pattern}_{ymd}_{now}.csv.gz'.format(
                                recommend_pattern=m.group('recommend_pattern'),
                                ymd=m.group('ymd'),
                                now=datetime.datetime.now().strftime('%Y%m%d%H%M%S')
                            )
        return os.path.join(self.tmp_dir, transfer_filename), transfer_filename

    def _create_transfer_file(self, target_file, transfer_file):
        mappingtable = self._open_mappingtable()

        with contextlib.nested(
            open(target_file, 'r'),
            gzip.open(transfer_file, 'wb')
        ) as (inf, outf):
            reader = csv.DictReader(inf,
                                    delimiter=',',
                                    quotechar='"',
                                    quoting=csv.QUOTE_ALL,
                                    lineterminator='\r\n')
            writer = csv.DictWriter(outf,
                                    delimiter=',',
                                    quotechar='"',
                                    quoting=csv.QUOTE_ALL,
                                    lineterminator='\r\n',
                                    fieldnames=['MEMBER_ID'])

            if not reader.fieldnames or not 'MEMBER_ID' in reader.fieldnames:
                raise Exception('Invalid format: file={}'.format(os.path.basename(target_file)))

            uids = []
            for row in reader:
                self.result.increment_counter('user.responsys')
                mapping_rows = mappingtable.select_rows_by_partner_user_id(row['MEMBER_ID'])
                if not mapping_rows:
                    self.result.increment_counter('user.responsys.unmapped')
                    continue
                self.result.increment_counter('user.responsys.mapped')
                self.result.increment_counter('user.rtoaster', len(mapping_rows))
                uids.extend([m[0] for m in mapping_rows])

            unique_uids = sorted(list(set(uids)))
            self.result.increment_counter('user.rtoaster.unique', len(unique_uids))

            writer.writeheader()
            writer.writerows([{'MEMBER_ID': u} for u in unique_uids])

    def _send_transfer_file(self, transfer_file):
        try:
            sftp_config = self.SFTP_CONFIG[self.env]
            sftputil.put(host=sftp_config['host'],
                         port=sftp_config['port'],
                         username=sftp_config['username'],
                         password=sftp_config.get('password'),
                         pkey_file=sftp_config.get('pkey_file'),
                         local_file=transfer_file,
                         remote_dir=sftp_config['remote_dir'],
                         rename=True,
                         tmp_filename=os.path.basename(transfer_file) + '.tmp')
        except:
            self.logger.exception('Failed to sftp put: %s',
                                  traceback.format_exc().splitlines()[-1])
            return False
        else:
            return True
