# -*- coding: utf-8 -*-

"""
Responsys へリクエストデータを返却するためのデータ整形バッチ
"""

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'production'

import csv
import gzip
import math
import operator
import os
import re
import shutil
import traceback
import lib.sftputil             as     sftputil
from   lib.transferer.base      import TransfererBase
from   lib.transferer.exception import NoS3Files
import lib.util                 as     util

class MailResponseTransferer(TransfererBase):

    SFTP_CONFIG = {
        'dev' : {
            'host'       : 'sftp-s.c-ovn.jp',
            'port'       : 115,
            'username'   : 'rtoaster9999',
            'password'   : 'rtoaster9999',
            'remote_dir' : '/receive/rs/rs_response'
        },
        'stg' : {
            'host'       : 'sftp-s.c-ovn.jp',
            'port'       : 115,
            'username'   : 'responsys_next',
            'password'   : 'vA.cwn159',
            'remote_dir' : 'response'
        },
        'prd' : {
            'host'       : 'sftp-1.c-ovn.jp',
            'port'       : 115,
            'username'   : 'responsys_next',
            'password'   : 'vA.cwn159',
            'remote_dir' : 'response'
        }
    }

    RTOASTER_ERRORS = {
        '0' : 'No recommend',
        '1' : 'Invalid filename or rule error',
        '2' : 'Invalid fileformat',
        '9' : 'Other'
    }

    def __init__(self, sid, pid, env, s3bucket, s3path, **option):
        super(MailResponseTransferer, self).__init__(
            sid, pid, env, 'mailresponse', s3bucket, s3path, **option
        )

    def execute(self):
        self.logger.info('<Start mailresponse transfer process>')
        target_file, transfer_files = None, []

        try:
            self.logger.info('Downloading s3files')
            target_file, target_filename = self._download_s3file()

            self.logger.info('Target filename: %s', target_filename)
            self.result.target_filename = target_filename

            is_recommend_failed = target_filename.endswith('.err')

            if is_recommend_failed:
                self.logger.info('Notifying error')
                self._notify_error(target_file)

                self.logger.info('Creating error files')
                transfer_files = self._create_error_files(target_file)

            else:
                self.logger.info('Creating transfer files')
                transfer_files = self._create_transfer_files(target_file)

            self.logger.info('Saving transfer files')
            for transfer_file in transfer_files:
                self._save_converted_file(transfer_file)

            self.logger.info('Sending transfer files')
            if not self._send_transfer_files(transfer_files):
                return self.result.ng('SftpError')

            if is_recommend_failed:
                return self.result.ng('RecommendError')
            else:
                return self.result.ok('OK')

        except NoS3Files, e:
            msg = 'File not exists in s3: bucket={}, path={}'.format(e.s3bucket, e.s3path)
            self.logger.error(msg)
            return self.result.ng(msg)
        except:
            st = traceback.format_exc().splitlines()[-1]
            self.logger.exception('Failed to transfer: %s', st)
            return self.result.ng('Unexpected error: {}'.format(st))
        finally:
            self.logger.info('Removing tmpfile')
            util.remove_files([target_file])
            util.remove_files(transfer_files)

            self.logger.info('<End mailresponse transfer process>')

    def _create_transfer_files(self, target_file):

        # Load csv data
        mappingtable = self._open_mappingtable()
        with gzip.open(target_file, 'rb') as inf:
            reader = csv.reader(inf,
                                delimiter=',',
                                quotechar='"',
                                quoting=csv.QUOTE_ALL,
                                lineterminator='\r\n')

            fieldnames = next(reader, None)
            if not fieldnames or not fieldnames[0] in 'MEMBER_ID':
                raise Exception('Invalid format: file={}'.format(os.path.basename(target_file)))

            ptuid_updatedat_rows = []
            for row in reader:
                self.result.increment_counter('user.rtoaster')
                mapping_rows = mappingtable.select_rows_by_user_id(row[0])
                if not mapping_rows:
                    self.result.increment_counter('user.rtoaster.unmapped')
                    continue
                self.result.increment_counter('user.rtoaster.mapped')
                ptuid_updatedat_rows.extend(
                    [(m[1], m[5], row) for m in mapping_rows]
                )
            self.result.increment_counter('user.responsys', len(ptuid_updatedat_rows))

        # Unique ptuid
        ptuid_updatedat_rows.sort(key=operator.itemgetter(1), reverse=True)
        ptuid_updatedat_rows.sort(key=operator.itemgetter(0))
        ptuid_rows = [
            (ptuid, row) for i, (ptuid, updated_at, row)
            in enumerate(ptuid_updatedat_rows)
            if i == 0 or ptuid != ptuid_updatedat_rows[i - 1][0]
        ]
        ptuid_updatedat_rows = None
        self.result.increment_counter('user.responsys.unique', len(ptuid_rows))

        # Calculate file num
        file_num = int(math.ceil((len(fieldnames) - 1) * 1.0 / 34))
        self.logger.info('%d columns -> %d files', len(fieldnames), file_num)

        # Prepare column indexes
        def _get_index(seqno):
            st = (seqno - 1) * 34 + 1
            ed = st + 34
            return st, ed
        indexes = [_get_index(i + 1) for i in range(file_num)]

        # Prepare output files
        outfs   = []
        writers = []
        for i in range(file_num):
            transfer_file = self._create_transfer_filename(
                                os.path.basename(target_file), i + 1
                            )
            outf = open(transfer_file, 'w')
            writer = csv.writer(outf,
                                delimiter=',',
                                quotechar='"',
                                quoting=csv.QUOTE_ALL,
                                lineterminator='\r\n')
            outfs.append(outf)
            writers.append(writer)

        # Write headers
        for i, (st, ed) in enumerate(indexes):
            header = [fieldnames[0]]
            header.extend(fieldnames[st:ed])
            writers[i].writerow(header)

        # Write datas
        for ptuid, row in ptuid_rows:
            for i, (st, ed) in enumerate(indexes):
                data = [ptuid]
                data.extend(row[st:ed])
                writers[i].writerow(data)

        # Close output files
        for outf in outfs:
            outf.close()

        return [f.name for f in outfs]

    def _create_transfer_filename(self, target_filename, seqno):
        # e.g. recommend_20140224_20140225202204.csv.gz -> 20140224_recommend_1.csv
        r = re.compile('(?P<recommend_pattern>\\w+)_(?P<ymd>\\d{8})_(?:\\d{14}).csv.gz')
        m = r.match(target_filename)
        if not m:
            raise ValueError('Invalid filename: {}'.format(target_filename))

        transfer_filename = '{ymd}_{recommend_pattern}_{seqno:d}.csv'.format(
                                recommend_pattern=m.group('recommend_pattern'),
                                ymd=m.group('ymd'),
                                seqno=seqno
                            )
        return os.path.join(self.tmp_dir, transfer_filename)

    def _notify_error(self, target_file):
        with open(target_file) as inf:
            code = inf.read().strip()

        self.logger.error('Rtoaster recommend error: file=%s, code=%s, detail="%s"',
                          os.path.basename(target_file), code, self.RTOASTER_ERRORS.get(code))

    def _create_error_files(self, target_file):
        error_file = self._create_error_filename(os.path.basename(target_file))
        shutil.copyfile(target_file, error_file)
        return [error_file]

    def _create_error_filename(self, target_filename):
        # e.g. recommend_20140224_20140225202204.csv.gz.err -> 20140224_recommend_1.csv.err
        r = re.compile('(?P<recommend_pattern>\\w+)_(?P<ymd>\\d{8})_(?:\\d{14}).csv.gz.err')
        m = r.match(target_filename)
        if not m:
            raise ValueError('Invalid filename: {}'.format(target_filename))

        error_filename = '{ymd}_{recommend_pattern}_1.csv.err'.format(
                             recommend_pattern=m.group('recommend_pattern'),
                             ymd=m.group('ymd')
                         )
        return os.path.join(self.tmp_dir, error_filename)

    def _send_transfer_files(self, transfer_files):
        try:
            sftp_config = self.SFTP_CONFIG[self.env]
            with sftputil.open(host=sftp_config['host'],
                               port=sftp_config['port'],
                               username=sftp_config['username'],
                               password=sftp_config.get('password'),
                               pkey_file=sftp_config.get('pkey_file')) as sftp:
                for transfer_file in transfer_files:
                    sftp.put(local_file=transfer_file,
                             remote_dir=sftp_config['remote_dir'],
                             rename=True)
        except:
            self.logger.exception('Failed to sftp put: %s',
                                  traceback.format_exc().splitlines()[-1])
            return False
        else:
            return True
