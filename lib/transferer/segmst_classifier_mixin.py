# -*- coding: utf-8 -*-

"""
セグメントマスタを、追加・更新・削除の3つに分類して処理する Mix-in
"""

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'production'

import copy
from abc import ABCMeta, abstractmethod

class SegmentMstClassifierMixin(object):
    __metaclass__ = ABCMeta

    def _classify_segments(self, target_segments, registered_segments, check_description=True):
        """
        引数のセグメントを new, deleted, modified の 3つのステータスに分類する
        @param  target_segments      S3 上のセグメントマスタ情報（連想配列のリスト）
        @param  registered_segments  RDS に登録されているセグメントマスタ情報（連想配列のリスト）
        @param  check_description    "備考" に変更があった場合、modified に加えるかを指定する (デフォルト: True)
        @return (modified_segments, new_segments, deleted_segments)
        """
        colname_segment_id          = 'id'
        colname_segment_name        = 'name'
        colname_segment_description = 'description'

        modified_segments = []
        new_segments      = copy.copy(target_segments)
        deleted_segments  = copy.copy(registered_segments)
        for t in target_segments:
            r = [_r for _r in registered_segments if t[colname_segment_id] == _r['segment_id_from']]
            if r:
                r = r[0]
                modified = False
                if t[colname_segment_name].decode('utf-8') != r['name']:
                    r['name_modified'] = t[colname_segment_name].decode('utf-8')
                    modified = True
                if check_description and t[colname_segment_description].decode('utf-8') != r['description']:
                    r['description_modified'] = t[colname_segment_description].decode('utf-8')
                    modified = True
                if modified:
                    modified_segments.append(r)
                new_segments.remove(t)
                deleted_segments.remove(r)
        return modified_segments, new_segments, deleted_segments

    @abstractmethod
    def _modify_segments(self, segments, convf): pass

    @abstractmethod
    def _delete_segments(self, segments, convf): pass

    @abstractmethod
    def _add_segments(self, segments, convf): pass
