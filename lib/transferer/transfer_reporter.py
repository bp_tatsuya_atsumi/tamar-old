# -*- coding: utf-8 -*-

from collections import OrderedDict
import datetime
from conf.config import BatchConfig
from lib.sesutil import SESUtil
from lib.transferer.transfer_result import TransferResult


class TransferReporter():
    SEND_COND_ALL, SEND_COND_NOTZERO, SEND_COND_ERROR, SEND_COND_NONE = range(4)

    SEND_CONDS = {
        'prd': {
            'segmentmst'          : SEND_COND_ERROR,
            'remove_segmentmst'   : SEND_COND_ERROR,
            'segmentdata'         : SEND_COND_ALL,
            'conversion'          : SEND_COND_ERROR,
            'itemmst'             : SEND_COND_ALL,
            'recommendmst'        : SEND_COND_ERROR,
            'remove_recommendmst' : SEND_COND_ERROR,
            'recommenddata'       : SEND_COND_ALL,
            'mailrequest'         : SEND_COND_ALL,
            'mailresponse'        : SEND_COND_ALL,
            'mailsegmentdata'     : SEND_COND_ALL,
        },
        'stg': {
            'segmentmst'          : SEND_COND_ALL,
            'remove_segmentmst'   : SEND_COND_ALL,
            'segmentdata'         : SEND_COND_ALL,
            'conversion'          : SEND_COND_ALL,
            'itemmst'             : SEND_COND_ALL,
            'recommendmst'        : SEND_COND_ALL,
            'remove_recommendmst' : SEND_COND_ALL,
            'recommenddata'       : SEND_COND_ALL,
            'mailrequest'         : SEND_COND_ALL,
            'mailresponse'        : SEND_COND_ALL,
            'mailsegmentdata'     : SEND_COND_ALL,
        },
        'dev': {
            'segmentmst'          : SEND_COND_NONE,
            'remove_segmentmst'   : SEND_COND_NONE,
            'segmentdata'         : SEND_COND_NONE,
            'conversion'          : SEND_COND_NONE,
            'itemmst'             : SEND_COND_NONE,
            'recommendmst'        : SEND_COND_NONE,
            'remove_recommendmst' : SEND_COND_NONE,
            'recommenddata'       : SEND_COND_NONE,
            'mailrequest'         : SEND_COND_NONE,
            'mailresponse'        : SEND_COND_NONE,
            'mailsegmentdata'     : SEND_COND_NONE,
        },
    }

    ENVS_JP = {
        'prd': u'本番環境',
        'stg': u'検証環境',
        'dev': u'開発環境'
    }

    DATA_TYPES_JP = {
        'segmentmst'          : u'セグメントリストマスタ更新',
        'remove_segmentmst'   : u'セグメントリストデータ削除',
        'segmentdata'         : u'セグメントリストデータ',
        'conversion'          : u'コンバージョンログ',
        'itemmst'             : u'商品マスタ',
        'recommendmst'        : u'レコメンドリストマスタ更新',
        'remove_recommendmst' : u'レコメンドリストマスタ削除',
        'recommenddata'       : u'レコメンドリストデータ',
        'mailrequest'         : u'メールリクエストデータ',
        'mailresponse'        : u'メールレスポンスデータ',
        'mailsegmentdata'     : u'メールセグメントデータ',
    }

    RESULTS_JP = {
        TransferResult.RESULT_OK        : u'OK',
        TransferResult.RESULT_API_ERROR : u'NG(ApiError)',
        TransferResult.RESULT_NG        : u'NG(All)'
    }

    MAIL_SUBJECT = u'転送結果レポート [{env}] [{data_type}] [{result_jp}] [{sid}] [{pid}]'
    MAIL_BODY    = u'''
環境: {env_jp}
データ種: {data_type_jp}
サイト: {site_name_jp}
パートナー: {partner_name_jp}

対象ファイル: {target_filename}
転送結果: {result_jp}
メッセージ: {message}
開始日時: {start_datetime}
終了日時: {end_datetime}
件数:
{count}
'''

    def __init__(self, transfer_result):
        for k, v in vars(transfer_result).items():
            setattr(self, k, v)
        self.config = BatchConfig(transfer_result.env)

    def is_send_target(self):
        send_condition = self.SEND_CONDS[self.env][self.data_type]
        if send_condition == self.SEND_COND_NONE:
            return False
        if send_condition == self.SEND_COND_ALL:
            return True

        if self.result != TransferResult.RESULT_OK:
            return True
        if send_condition == self.SEND_COND_ERROR:
            return False
        if send_condition == self.SEND_COND_NOTZERO:
            if not self.counter:
                return False
            return True in [c > 0 for c in self.counter.values()]
        raise ValueError('invalid datatype')

    def to_report_format(self):

        def _format_datetime( dt):
            if not dt: return None
            return datetime.datetime.strftime(dt, '%Y-%m-%d %H:%M:%S.%f')

        site_info    = self.config.site_mst.get(self.sid, {})
        partner_info = self.config.partner_mst.get(self.pid, {})

        counter_dict = OrderedDict(sorted(self.counter.items(), key=lambda k: k[0]))
        body_counter = '\n'.join(['    {0}: {1}'.format(k, v) for k, v in counter_dict.items()])

        args = {}
        args['data_type']       = self.data_type
        args['data_type_jp']    = self.DATA_TYPES_JP[self.data_type]
        args['sid']             = self.sid
        args['site_name_jp']    = site_info.get('name_jp', '')
        args['pid']             = self.pid
        args['partner_name_jp'] = partner_info.get('name_jp', '')
        args['env']             = self.env
        args['env_jp']          = self.ENVS_JP[self.env]
        args['target_filename'] = self.target_filename
        args['result']          = self.result
        args['result_jp']       = self.RESULTS_JP.get(self.result, '')
        args['message']         = ','.join(self.messages)
        args['start_datetime']  = _format_datetime(self.start_datetime)
        args['end_datetime']    = _format_datetime(self.end_datetime)
        args['count']           = body_counter

        return self.MAIL_SUBJECT.format(**args), self.MAIL_BODY.format(**args)

    def send_report(self):
        if not self.is_send_target():
            return
        sesutil = SESUtil(self.env)
        mail_subject, mail_body = self.to_report_format()
        sesutil.send_mail(mail_subject, mail_body)
