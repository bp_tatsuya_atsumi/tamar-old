# -*- coding: utf-8 -*-

"""
MicroAd BLADE へレコメンドデータ連携するためのバッチ（GDOフォーマット用）
"""

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'production'

import contextlib
import datetime
import time
import traceback
import requests
from   lib.transferer.base            import TransfererBase
from   lib.transferer.exception       import NoS3Files
import lib.transferer.rtfile          as     rtfile
from   lib.transferer.transfer_result import TransferConvertedFile
import lib.util                       as     util

class RecommendDataTransferer(TransfererBase):

    ENDPOINT_URL = {
        'dev' : 'http://localhost:5000/rpl',
        'stg' : 'http://ma-test.c-ovn.jp/rpl',
        'prd' : 'http://bp.link.microad.jp/rpl',
    }
    S3DIR_MST   = '{sid}/recommend_list_mst/raw'
    API_VERSION = 1
    QPS         = 1000

    def __init__(self, sid, pid, env, s3bucket, s3path, **option):
        TransfererBase.__init__(self, sid, pid, env, 'recommenddata', s3bucket,
                                s3path, **option)

        self.s3dir_mst     = self.S3DIR_MST.format(sid=self.sid)
        self.endpoint      = self.ENDPOINT_URL[self.env]
        self.advertiser_id = self.mapping_auth['advertiser_id']
        self.ttl           = self.mapping_ttl

    def execute(self):
        self.logger.info('<Start recommenddata transfer process>')
        target_file, master_file, converted_file = None, None, None

        try:
            self.logger.info('Downloading latest recommend data')
            target_file, target_filename = self._download_s3file()
            self.result.target_filename  = target_filename

            self.logger.info('Downloading latest recommend master')
            master_file, _ = self._download_latest_s3file(self.s3dir_mst)

            self.logger.info('Loading recommend master')
            recname_to_recid = self._load_recommend_master(master_file)

            self.logger.info('Sending request')
            api_error_num  = 0
            converted_file = TransferConvertedFile.convert_path(target_file)

            with contextlib.nested(
                TransferConvertedFile(converted_file),
                rtfile.open_recommenddata(target_file, self._open_mappingtable())
            ) as (convf, reader):

                convf.write_header()
                for row in reader:
                    url = self._create_request_url(row, recname_to_recid)
                    if not url:
                        continue

                    time.sleep(1.0 / self.QPS)
                    self.result.increment_counter('request')
                    response = requests.get(url)
                    convf.write_get_log(url, None, response)

                    self.result.increment_counter('request.' + str(response.status_code))
                    if response.status_code != 200:
                        api_error_num += 1
                        self.logger.warn('ApiError: %s', response.text)

                self.result.increment_counter('user.rtoaster', reader.line_count_all)
                self.result.increment_counter('user.mapped',   reader.line_count_mapped)
                self.result.increment_counter('user.unmapped', reader.line_count_unmapped)
                self.result.increment_counter('user.invalid',  reader.line_count_invalid)

            self.logger.info('Saving transfer file')
            self._save_converted_file(converted_file)

            if api_error_num > 0:
                return self.result.api_error('{:d} API Errors'.format(api_error_num))
            return self.result.ok('OK')

        except NoS3Files, e:
            msg = 'File not exists in s3: {0} {1}'.format(e.s3bucket, e.s3path)
            self.logger.error(msg)
            return self.result.ng(msg)
        except:
            st = traceback.format_exc().splitlines()[-1]
            self.logger.exception('Failed to transfer: laststacktrace="%s"', st)
            return self.result.ng('Unexpected error: laststacktrace={}'.format(st))
        finally:
            self.logger.info('Removing tmpfile')
            util.remove_files([target_file, master_file, converted_file])

        self.logger.info('<End recommenddata transfer process>')

    def _load_recommend_master(self, master_file):
        with rtfile.open_recommendmst(master_file) as reader:
            return {row['name']: row['id'] for row in reader}

    def _create_request_url(self, row, recname_to_recid):
        if not 'item_lists' in row:
            return None
        item_lists = []
        for recname, item_codes in row['item_lists'].items():
            recid = recname_to_recid.get(recname.encode('utf-8'))
            if not recid:
                continue
            item_ids = [str(util.code_to_long_id(c)) for c in item_codes]
            item_lists.append(recid + '-' + '.'.join(item_ids))
        if not item_lists:
            return None
        return '{endpoint}?v={version}&u={mapped_uid}&rpl={advertiser_id}-{ttl}:{item_lists}'.format(
                   endpoint=self.endpoint,
                   version=self.API_VERSION,
                   mapped_uid=row['mapped_uid'],
                   advertiser_id=self.advertiser_id,
                   ttl=self._get_ttl_unixtimestamp(),
                   item_lists=','.join(item_lists)
               )

    def _get_ttl_unixtimestamp(self):
        ttl_dt = (datetime.datetime.now() + datetime.timedelta(days=self.ttl))
        return long(time.mktime(ttl_dt.timetuple()))
