# -*- coding: utf-8 -*-

"""
MicroAd BLADE へアイテムマスタ連携するためのバッチ（GDOフォーマット用）
"""

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'production'

from   collections              import OrderedDict
import csv
import datetime
import gzip
import json
import os
import time
import traceback
import lib.sftputil             as     sftputil
from   lib.transferer.base      import TransfererBase
from   lib.transferer.exception import NoS3Files
import lib.util                 as     util

class ItemMstTransferer(TransfererBase):

    SFTP_CONFIG = {
        'dev' : {
            'host'       : 'sftp-s.c-ovn.jp',
            'port'       : 115,
            'username'   : 'rtoaster9999',
            'password'   : 'rtoaster9999',
            'remote_dir' : '/receive/ma'
        },
        'stg' : {
            'host'       : 'ma-test.c-ovn.jp',
            'port'       : 115,
            'username'   : 'brainpad-ma',
            'pkey_file'  : 'virtual_partner/ssh_key/stg_microad_id_rsa',
            'remote_dir' : 'receive'
        },
        'prd' : {
            'host'       : 'dex.microad.jp',
            'port'       : 22,
            'username'   : 'brainpad',
            'pkey_file'  : 'settings/site/batch/ami/microad_id_rsa',
            'remote_dir' : '/data'
        }
    }
    LINE_NUM_LIMIT = 10000

    def __init__(self, sid, pid, env, s3bucket, s3path, **option):
        TransfererBase.__init__(self, sid, pid, env, 'itemmst', s3bucket, s3path, **option)
        self.advertiser_id = self.mapping_auth['advertiser_id']

    def execute(self):
        self.logger.info('<Start itemmst transfer process>')
        old_target_file, new_target_file, transfer_files = None, None, []

        try:
            self.logger.info('Downloading s3files')
            old_target_file, new_target_file = self._download_s3files()

            old_target_filename = os.path.basename(old_target_file)
            new_target_filename = os.path.basename(new_target_file)
            self.result.target_filename = ', '.join([old_target_filename,
                                                     new_target_filename])

            self.logger.info('Loading old file')
            old_code_to_row = self._load_target_file(old_target_file)

            self.logger.info('Loading new file')
            new_code_to_row = self._load_target_file(new_target_file)

            self.logger.info('Creating transfer files')
            transfer_files = self._create_transfer_files(old_code_to_row,
                                                         new_code_to_row)

            self.logger.info('Saving transfer files')
            self._save_converted_files(transfer_files)

            self.logger.info('Sending transfer files')
            if not self._send_transfer_files(transfer_files):
                return self.result.ng('Sftp error')
            return self.result.ok('OK')

        except NoS3Files, e:
            msg = 'File not exists in s3: bucket={}, path={}'.format(e.s3bucket, e.s3path)
            self.logger.error(msg)
            return self.result.ng(msg)
        except:
            st = traceback.format_exc().splitlines()[-1]
            self.logger.exception('Failed to transfer: laststacktrace="%s"', st)
            return self.result.ng('Unexpected error: laststacktrace={}'.format(st))
        finally:
            self.logger.info('Removing tmpfile')
            util.remove_files([old_target_file, new_target_file])
            util.remove_files(transfer_files)

        self.logger.info('<End itemmst transfer process>')

    def _download_s3files(self):
        s3objs = self.s3util.list_dir(self.s3bucket,
                                      os.path.dirname(self.s3path))
        s3objs = [o for o in s3objs if o['key'] <= self.s3path]
        if not s3objs or len(s3objs) < 2:
            raise NoS3Files(self.s3bucket, self.s3path)

        s3objs.sort(key=lambda o: o['key'], reverse=True)
        if s3objs[0]['key'] != self.s3path:
            raise NoS3Files(self.s3bucket, self.s3path)
        old_s3obj = s3objs[1]
        new_s3obj = s3objs[0]

        self.s3util.download_single_file(self.s3bucket, old_s3obj['key'], self.tmp_dir)
        self.s3util.download_single_file(self.s3bucket, new_s3obj['key'], self.tmp_dir)

        _to_file = lambda k: os.path.join(self.tmp_dir, os.path.basename(k))
        return _to_file(old_s3obj['key']), _to_file(new_s3obj['key']),

    def _load_target_file(self, target_file):
        code_to_row = OrderedDict()
        with gzip.open(target_file) as f:
            for line in f:
                line = line.strip()
                if not line:
                    continue
                try:
                    row = json.loads(line)
                except ValueError:
                    continue
                code_to_row[row['code']] = row
        return code_to_row

    def _create_transfer_files(self, old_code_to_row, new_code_to_row):
        transfer_files = []

        def _get_writer(func_create_filename):
            transfer_file = func_create_filename()
            transfer_fp   = gzip.open(transfer_file, 'wb')
            transfer_files.append(transfer_file)
            return transfer_fp, csv.writer(transfer_fp,
                                           delimiter='\t',
                                           lineterminator='\n')
        def _get_update_writer():
            return _get_writer(self._create_update_filename)
        def _get_delete_writer():
            return _get_writer(self._create_delete_filename)

        # update
        update_rows = self._get_update_rows(old_code_to_row, new_code_to_row)
        update_fp, update_writer = _get_update_writer()
        for i, row in enumerate(update_rows):
            if i and i % self.LINE_NUM_LIMIT == 0:
                update_fp.close()
                time.sleep(1)
                update_fp, update_writer = _get_update_writer()
            update_writer.writerow(
                self._transform_update_format(row)
            )
        update_fp.close()
        self.result.increment_counter('item.rtoaster.update', len(update_rows))

        # delete
        delete_codes = self._get_delete_codes(old_code_to_row, new_code_to_row)
        delete_fp, delete_writer = _get_delete_writer()
        for i, code in enumerate(delete_codes):
            if i and i % self.LINE_NUM_LIMIT == 0:
                delete_fp.close()
                time.sleep(1)
                delete_fp, delete_writer = _get_delete_writer()
            delete_writer.writerow(
                self._transform_delete_format(code)
            )
        delete_fp.close()
        self.result.increment_counter('item.rtoaster.delete', len(delete_codes))

        return transfer_files

    def _create_update_filename(self):
        now = datetime.datetime.strftime(datetime.datetime.now(), '%Y%m%d%H%M%S')
        filename = 'brainpad_product_master_{advertiser_id}.{datetime}.gz'.format(
                       advertiser_id=self.advertiser_id,
                       datetime=now
                   )
        return os.path.join(self.tmp_dir, filename)

    def _create_delete_filename(self):
        now = datetime.datetime.strftime(datetime.datetime.now(), '%Y%m%d%H%M%S')
        filename = 'brainpad_product_delete_{advertiser_id}.{datetime}.gz'.format(
                       advertiser_id=self.advertiser_id,
                       datetime=now
                   )
        return os.path.join(self.tmp_dir, filename)

    def _get_update_rows(self, old_code_to_row, new_code_to_row):
        update_rows = []
        for new_code, new_row in new_code_to_row.items():
            old_row = old_code_to_row.get(new_code)
            if not old_row:
                update_rows.append(new_row)
                continue
            if (old_row['name']     != new_row['name']     or
                old_row['imageUrl'] != new_row['imageUrl'] or
                old_row['url']      != new_row['url']      or
                old_row['field4']   != new_row['field4']   or
                old_row['field1']   != new_row['field1']):
                update_rows.append(new_row)
                continue
        return update_rows

    def _get_delete_codes(self, old_code_to_row, new_code_to_row):
        return list(set(old_code_to_row.keys()) - set(new_code_to_row.keys()))

    def _transform_update_format(self, row):
        def _format_str(u, limit_len=100):
            if not u:
                return ''
            return util.strip_str(u.encode('utf-8').translate(None, '\t\n'),
                                  limit_len)
        def _cut_url(u):
            return u[:1000] if len(u) > 1000 else u

        return [
            self.sid,                           # sid
            util.code_to_long_id(row['code']),  # code
            _format_str(row['name'], 50),       # name
            _cut_url(row['imageUrl']),          # img_utl
            _cut_url(row['url']),               # link_url
            _format_str(row['field4'], 50),     # price
            _format_str(row['field1']),         # desctiption
            self.advertiser_id                  # advertiser_id
        ]

    def _transform_delete_format(self, code):
        return [
            self.sid,                           # sid
            util.code_to_long_id(code),         # code
            self.advertiser_id                  # advertiser_id
        ]

    def _save_converted_files(self, transfer_files):
        for transfer_file in transfer_files:
            self._save_converted_file(transfer_file)

    def _send_transfer_files(self, transfer_files):
        try:
            sftp_config = self.SFTP_CONFIG[self.env]
            with sftputil.open(host=sftp_config['host'],
                               port=sftp_config['port'],
                               username=sftp_config['username'],
                               password=sftp_config.get('password'),
                               pkey_file=sftp_config.get('pkey_file')) as sftp:

                remote_dir = sftp_config['remote_dir']
                for transfer_file in transfer_files:
                    sftp.put(transfer_file, remote_dir, rename=False)

                    complete_filename = os.path.basename(transfer_file) + '.completed'
                    remote_complete_file = os.path.join(remote_dir, complete_filename)
                    sftp.touch(remote_complete_file)
        except:
            self.logger.exception('Failed to sftp put')
            return False
        else:
            return True
