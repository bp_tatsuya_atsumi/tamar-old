## Protocol Buffers対応

定義済みprotocol formatをコンパイルして利用します。
定義済みprotocol formatは本ディレクトリのddp.protoとなります。

```
$ protoc -I=./ --python_out=./ ./ddp.proto
```

コンパイルが正しく終了すると、本ディレクトリに ddp_pb2.proto というファイルが作成されます。
Google連携のセグメントデータ転送は上述の ddp_pb2.proto が正しくコンパイルされているということを前提に動作しますので注意してください。
