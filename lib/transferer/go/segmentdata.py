# -*- coding: utf-8 -*-

"""
DoubleClick Bid Managerへオーディエンスデータを送るためのバッチ
"""

__author__ = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__ = 'production'

import json
import traceback
import requests

import ddp_pb2                        as     DDP
from   google.protobuf.text_format    import MessageToString
import lib.segmentmsttable            as     segmentmsttable
from   lib.transferer.base            import TransfererBase
from   lib.transferer.exception       import NoS3Files
import lib.transferer.rtfile          as     rtfile
from   lib.transferer.transfer_result import TransferConvertedFile
import lib.util                       as     util

class SegmentDataTransferer(TransfererBase):

    FLUSH_LIMIT  = 100
    ENDPOINT_URL = {
        'dev' : u'https://cm.g.doubleclick.net/upload?nid=brainpad_dmp',
        'stg' : u'https://cm.g.doubleclick.net/upload?nid=brainpad_dmp',
        'prd' : u'https://cm.g.doubleclick.net/upload?nid=brainpad_dmp'
    }

    API_ERROR_CODE = {
        '0'  : 'NO_ERROR',
        '1'  : 'PARTIAL_SUCCESS',
        '2'  : 'PERMISSION_DENIED',
        '3'  : 'BAD_DATA',
        '4'  : 'BAD_COOKIE',
        '5'  : 'BAD_ATTRIBUTE_ID',
        '6'  : 'CLOSED_ATTRIBUTE_ID',
        '7'  : 'BAD_NETWORK_ID',
        '8'  : 'REQUEST_TOO_BIG',
        '9'  : 'EMPTY_REQUERST',
        '10' : 'INTERNAL_ERROR',
        '12' : 'BAD_TIMESTAMP',
        '13' : 'NUM_ERROR_CODES'
    }

    def __init__(self, sid, pid, env, s3bucket, s3path, **option):
        TransfererBase.__init__(self, sid, pid, env, 'segmentdata', s3bucket,
                                s3path, **option)

        self.endpoint = self.ENDPOINT_URL[env]
        self.api_error_occurred = False
        self.unknown_segments   = []

    def execute(self):
        self.logger.info('<Start segmentdata transferer process>')
        target_file, converted_file = None, None

        try:
            self.logger.info('Downloading s3file')
            target_file, target_filename = self._download_s3file()
            self.result.target_filename = target_filename

            converted_file = TransferConvertedFile.convert_path(target_file)

            with TransferConvertedFile(converted_file) as convf:
                convf.write_header()

                self.logger.info('Sending request')
                self.api_error_occurred = self._send_requests(target_file, convf)

            self.logger.info('Saving transfer file')
            self._save_converted_file(converted_file)

            if self.api_error_occurred:
                return self.result.api_error('Some api error occurred')
            else:
                return self.result.ok('OK')

        except NoS3Files, e:
            msg = 'File nt exists in s3: {s3bucket} {s3path}'.format(s3bucket=e.s3bucket,
                                                                     s3path=e.s3path)
            self.logger.error(msg)
            return self.result.ng(msg)
        except:
            st = traceback.format_exc().splitlines()[-1]
            self.logger.exception('Failed to transfer: laststacktrace="%s"', st)
            return self.result.ng('Unexpected error: laststacktrace={}'.format(st))
        finally:
            self.logger.info('Removing tmpfile')
            util.remove_files([target_file, converted_file])
            self.logger.info('<End segmentdata transfer process>')

    def _send_requests(self, target_file, fp_converted):
        """
        セグメントデータの内容を読み込み、Googleへリクエストを送信する。
        実際の送信処理は'_send_each_request'メソッドが行う
        """
        self._create_segment_mst_map()
        with rtfile.open_segmentdata(target_file, self._open_mappingtable(), oldest_user=False) as rt_file_reader:
            self.uids_by_segid = {}
            row_counter = 0

            for segdata_row in rt_file_reader:
                self._create_ptuid_list_by_ptsegid(segdata_row)
                row_counter += 1

                # row_counterがFLUSH_LIMITを超えた場合はリクエストを実行
                if row_counter >= self.FLUSH_LIMIT:
                    row_counter = 0
                    self.api_error_occurred = self._execute_request(fp_converted) | self.api_error_occurred
                    self.uids_by_segid = {}

            # ファイルを全て読み込んだ際に残っているリクエストを実行する
            if row_counter > 0:
                self.api_error_occurred = self._execute_request(fp_converted) | self.api_error_occurred

            self.result.increment_counter('user.rtoaster', rt_file_reader.line_count_all)
            self.result.increment_counter('user.mapped',   rt_file_reader.line_count_mapped)
            self.result.increment_counter('user.unmapped', rt_file_reader.line_count_unmapped)
            self.result.increment_counter('user.invalid',  rt_file_reader.line_count_invalid)

            return self.api_error_occurred

    def _create_segment_mst_map(self):
        """
        セグメントマスタのマッピング表を作成する
        keyをRtoasterのセグメントIDとし、Google側のセグメントIDを保持したDictinary
        """
        self.rt_segmst = {}
        with segmentmsttable.open(self.sid, self.pid, self.db_config) as segmst:
            rt_segmst = segmst.select_all_rows()
            for segmst_row in rt_segmst:
                self.rt_segmst[segmst_row['segment_id_from']] = segmst_row['segment_id_to']

    def _create_ptuid_list_by_ptsegid(self, segdata_row):
        """
        一行ごとに読み込んだRtoasterから出力データを
        セグメントID毎の変換後のUIDのリストを持ったdictionaryを作成する
        """
        for rt_segid in segdata_row['attrs']:
            pt_uids  = segdata_row['mapped_uids']
            try:
                pt_segid = self.rt_segmst[str(rt_segid)]
                if pt_segid not in self.uids_by_segid:
                    self.uids_by_segid[pt_segid] = pt_uids
                else:
                    self.uids_by_segid[pt_segid].extend(pt_uids)
                self.result.increment_counter('segment.known')
            except:
                self.result.increment_counter('segment.unknown')
                if rt_segid in self.unknown_segments:
                    self.logger.warn('segment id [{segid}] is not registered.'.format(segid=rt_segid))
                    self.unknown_segments.append(rt_segid)

    def _execute_request(self, fp_converted):
        """
        実際にデータ送信を行う処理
        """
        headers = {'content-type' : 'application/octet-stream'}
        ddp_response = DDP.UpdateUsersDataResponse()
        is_api_error = False

        for pt_segid, pt_uids in self.uids_by_segid.items():
            ddp = DDP.UpdateUserDataRequest()
            for pt_uid in pt_uids:
                ops = ddp.ops.add()
                ops.user_list_id = int(pt_segid)
                ops.user_id      = pt_uid

            serializedData = ddp.SerializeToString()
            response = requests.post(self.endpoint, data=serializedData, headers=headers)
            self.result.increment_counter('request.' + str(response.status_code))
            ddp_response.ParseFromString(response.content)
            if response.status_code != 200 or ddp_response.status != 0:
                is_api_error = True
                try:
                    message = 'Api error: {0} {1}'.format(response.text, self.API_ERROR_CODE[str(ddp_response.status)])
                    self.result.add_message(message)
                    self.logger.warn(message)
                except:
                    self.logger.warn('Api error: something wrong in user_id or partner_user_id')

            fp_converted.write_post_log_for_go(self.endpoint, MessageToString(ddp).replace('\n', ''), self.API_ERROR_CODE[str(ddp_response.status)])

        return is_api_error
