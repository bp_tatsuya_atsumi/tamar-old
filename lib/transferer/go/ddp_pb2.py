# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ddp.proto

from google.protobuf.internal import enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import descriptor_pb2
# @@protoc_insertion_point(imports)




DESCRIPTOR = _descriptor.FileDescriptor(
  name='ddp.proto',
  package='',
  serialized_pb='\n\tddp.proto\"\xa6\x01\n\x11UserDataOperation\x12\x11\n\x07user_id\x18\x01 \x01(\t:\x00\x12\x17\n\x0cuser_list_id\x18\x04 \x01(\x03:\x01\x30\x12\"\n\x17time_added_to_user_list\x18\x05 \x01(\x03:\x01\x30\x12*\n\x1ftime_added_to_user_list_in_usec\x18\x08 \x01(\x03:\x01\x30\x12\x15\n\x06\x64\x65lete\x18\x06 \x01(\x08:\x05\x66\x61lse\"8\n\x15UpdateUserDataRequest\x12\x1f\n\x03ops\x18\x01 \x03(\x0b\x32\x12.UserDataOperation\"W\n\tErrorInfo\x12\x17\n\x0cuser_list_id\x18\x02 \x01(\x03:\x01\x30\x12\x11\n\x07user_id\x18\x03 \x01(\t:\x00\x12\x1e\n\nerror_code\x18\x04 \x01(\x0e\x32\n.ErrorCode\"Q\n\x17UpdateUsersDataResponse\x12\x1a\n\x06status\x18\x01 \x01(\x0e\x32\n.ErrorCode\x12\x1a\n\x06\x65rrors\x18\x02 \x03(\x0b\x32\n.ErrorInfo*\x89\x02\n\tErrorCode\x12\x0c\n\x08NO_ERROR\x10\x00\x12\x12\n\x0ePARTIAL_SUCCES\x10\x01\x12\x15\n\x11PERMISSION_DENIDE\x10\x02\x12\x0c\n\x08\x42\x41\x44_DATA\x10\x03\x12\x0e\n\nBAD_COOKIE\x10\x04\x12\x14\n\x10\x42\x41\x44_ATTRIBUTE_ID\x10\x05\x12\x17\n\x13\x43LOSED_ATTRIBUTE_ID\x10\x06\x12\x12\n\x0e\x42\x41\x44_NETWORK_ID\x10\x07\x12\x13\n\x0fREQUEST_TOO_BIG\x10\x08\x12\x11\n\rEMPTY_REQUEST\x10\t\x12\x12\n\x0eINTERNAL_ERROR\x10\n\x12\x11\n\rBAD_TIMESTAMP\x10\x0c\x12\x13\n\x0fNUM_ERROR_CODES\x10\r')

_ERRORCODE = _descriptor.EnumDescriptor(
  name='ErrorCode',
  full_name='ErrorCode',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='NO_ERROR', index=0, number=0,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='PARTIAL_SUCCES', index=1, number=1,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='PERMISSION_DENIDE', index=2, number=2,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='BAD_DATA', index=3, number=3,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='BAD_COOKIE', index=4, number=4,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='BAD_ATTRIBUTE_ID', index=5, number=5,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='CLOSED_ATTRIBUTE_ID', index=6, number=6,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='BAD_NETWORK_ID', index=7, number=7,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='REQUEST_TOO_BIG', index=8, number=8,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='EMPTY_REQUEST', index=9, number=9,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='INTERNAL_ERROR', index=10, number=10,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='BAD_TIMESTAMP', index=11, number=12,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='NUM_ERROR_CODES', index=12, number=13,
      options=None,
      type=None),
  ],
  containing_type=None,
  options=None,
  serialized_start=413,
  serialized_end=678,
)

ErrorCode = enum_type_wrapper.EnumTypeWrapper(_ERRORCODE)
NO_ERROR = 0
PARTIAL_SUCCES = 1
PERMISSION_DENIDE = 2
BAD_DATA = 3
BAD_COOKIE = 4
BAD_ATTRIBUTE_ID = 5
CLOSED_ATTRIBUTE_ID = 6
BAD_NETWORK_ID = 7
REQUEST_TOO_BIG = 8
EMPTY_REQUEST = 9
INTERNAL_ERROR = 10
BAD_TIMESTAMP = 12
NUM_ERROR_CODES = 13



_USERDATAOPERATION = _descriptor.Descriptor(
  name='UserDataOperation',
  full_name='UserDataOperation',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='user_id', full_name='UserDataOperation.user_id', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=True, default_value=unicode("", "utf-8"),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='user_list_id', full_name='UserDataOperation.user_list_id', index=1,
      number=4, type=3, cpp_type=2, label=1,
      has_default_value=True, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='time_added_to_user_list', full_name='UserDataOperation.time_added_to_user_list', index=2,
      number=5, type=3, cpp_type=2, label=1,
      has_default_value=True, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='time_added_to_user_list_in_usec', full_name='UserDataOperation.time_added_to_user_list_in_usec', index=3,
      number=8, type=3, cpp_type=2, label=1,
      has_default_value=True, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='delete', full_name='UserDataOperation.delete', index=4,
      number=6, type=8, cpp_type=7, label=1,
      has_default_value=True, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=14,
  serialized_end=180,
)


_UPDATEUSERDATAREQUEST = _descriptor.Descriptor(
  name='UpdateUserDataRequest',
  full_name='UpdateUserDataRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ops', full_name='UpdateUserDataRequest.ops', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=182,
  serialized_end=238,
)


_ERRORINFO = _descriptor.Descriptor(
  name='ErrorInfo',
  full_name='ErrorInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='user_list_id', full_name='ErrorInfo.user_list_id', index=0,
      number=2, type=3, cpp_type=2, label=1,
      has_default_value=True, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='user_id', full_name='ErrorInfo.user_id', index=1,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=True, default_value=unicode("", "utf-8"),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='error_code', full_name='ErrorInfo.error_code', index=2,
      number=4, type=14, cpp_type=8, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=240,
  serialized_end=327,
)


_UPDATEUSERSDATARESPONSE = _descriptor.Descriptor(
  name='UpdateUsersDataResponse',
  full_name='UpdateUsersDataResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='status', full_name='UpdateUsersDataResponse.status', index=0,
      number=1, type=14, cpp_type=8, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='errors', full_name='UpdateUsersDataResponse.errors', index=1,
      number=2, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=329,
  serialized_end=410,
)

_UPDATEUSERDATAREQUEST.fields_by_name['ops'].message_type = _USERDATAOPERATION
_ERRORINFO.fields_by_name['error_code'].enum_type = _ERRORCODE
_UPDATEUSERSDATARESPONSE.fields_by_name['status'].enum_type = _ERRORCODE
_UPDATEUSERSDATARESPONSE.fields_by_name['errors'].message_type = _ERRORINFO
DESCRIPTOR.message_types_by_name['UserDataOperation'] = _USERDATAOPERATION
DESCRIPTOR.message_types_by_name['UpdateUserDataRequest'] = _UPDATEUSERDATAREQUEST
DESCRIPTOR.message_types_by_name['ErrorInfo'] = _ERRORINFO
DESCRIPTOR.message_types_by_name['UpdateUsersDataResponse'] = _UPDATEUSERSDATARESPONSE

class UserDataOperation(_message.Message):
  __metaclass__ = _reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _USERDATAOPERATION

  # @@protoc_insertion_point(class_scope:UserDataOperation)

class UpdateUserDataRequest(_message.Message):
  __metaclass__ = _reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _UPDATEUSERDATAREQUEST

  # @@protoc_insertion_point(class_scope:UpdateUserDataRequest)

class ErrorInfo(_message.Message):
  __metaclass__ = _reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _ERRORINFO

  # @@protoc_insertion_point(class_scope:ErrorInfo)

class UpdateUsersDataResponse(_message.Message):
  __metaclass__ = _reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _UPDATEUSERSDATARESPONSE

  # @@protoc_insertion_point(class_scope:UpdateUsersDataResponse)


# @@protoc_insertion_point(module_scope)
