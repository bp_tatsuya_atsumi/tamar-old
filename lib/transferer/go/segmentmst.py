# -*- coding: utf-8 -*-

"""
Google(DoubleClick Data Platform)とセグメントマスタ連携を行うモジュール
"""

__author__ = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__ = ''

import json
import traceback
from   lib.transferer.base                    import TransfererBase
from   lib.transferer.exception               import NoS3Files
import lib.transferer.rtfile                  as     rtfile
import lib.segmentmsttable                    as     segmentmsttable
from   lib.transferer.transfer_result         import TransferConvertedFile
from   lib.partner_api.go.googleads           import ddp
from   lib.transferer.segmst_classifier_mixin import SegmentMstClassifierMixin
import lib.util                               as     util


class SegmentMstTransferer(TransfererBase, SegmentMstClassifierMixin):

    def __init__(self, sid, pid, env, s3bucket, s3path, **option):

        super(SegmentMstTransferer, self).__init__(
            sid, pid, env, 'segmentmst', s3bucket, s3path, **option
        )

        ddp_client                = ddp.DdpClient.LoadFromStorage()
        self.dmp_userlist_service = ddp_client.GetService('DmpUserListService', version='v201402')
        self.clientCustomerId = {'product': self.mapping_auth['product'],
                                 'entityId': self.mapping_auth['entityId']}

    def execute(self):
        """ 
        処理を実行させるためのメインのメソッド
        """

        def _retrieve_all_target_segments():
            return [row for row in rtfile.open_segmentmst(target_file)]

        def _retrieve_all_registered_segments():
            with segmentmsttable.open(self.sid, self.pid, self.db_config) as segmst:
                registered_segment_rowproxies = segmst.select_all_rows()
            return [dict(rsr.items()) for rsr in registered_segment_rowproxies]

        self.logger.info('<Start segmentmst transfer process>')
        target_file, converted_file = None, None

        try:
            self.logger.info('Downloading s3file')
            target_file, target_filename = self._download_s3file()
            self.result.target_filename = target_filename

            converted_file = TransferConvertedFile.convert_path(target_file)

            target_segments     = _retrieve_all_target_segments()
            registered_segments = _retrieve_all_registered_segments()

            modified_segments, new_segments, deleted_segments = \
                    self._classify_segments(target_segments, registered_segments)

            api_error_occurred = False
            with TransferConvertedFile(converted_file) as convf:
                convf.write_header()
                if new_segments:
                    api_error_occured = self._add_segments(new_segments, convf) | api_error_occurred
                if modified_segments:
                    api_error_occured = self._modify_segments(modified_segments, convf) | api_error_occurred
                if deleted_segments:
                    api_error_occured = self._delete_segments(deleted_segments, convf) | api_error_occurred

            self.logger.info('Saving transfer file')
            self._save_converted_file(converted_file)

            if api_error_occurred:
                return self.result.api_error('Some api error occured')
            else:
                return self.result.ok('OK')

        except NoS3Files, e:
            msg = 'File not exists in s3: {backet} {path}'.format(backet=e.s3bucket, path=e.s3path)
            self.logger.error(msg)
            return self.result.ng(msg)
        except:
            st = traceback.format_exc().splitlines()[-1]
            self.logger.exception('Failed to transfer: laststacktrace="%s"', st)
            return self.result.ng('Unexpected error: laststacktrace={}'.format(st))
        finally:
            self.logger.info('Removing tmpfile')
            util.remove_files([target_file, converted_file])
            self.logger.info('<End segmentdata transfer process>')

    def _classify_segments(self, target_segments, registered_segments):
        """
        @override SegmentMstClassifierMixin
        引数のセグメントを new, deleted, modified の3つのステータスに分類する
        SegmentMstClassifierMixin の _classifiy_segments を呼び、カウンタに登録する
        """
        modified_segments, new_segments, deleted_segments =\
            SegmentMstClassifierMixin._classify_segments(self, target_segments, registered_segments, check_description=False)
        self.result.increment_counter('segment.modified', len(modified_segments))
        self.result.increment_counter('segment.new',      len(new_segments))
        self.result.increment_counter('segment.deleted',  len(deleted_segments))

        return modified_segments, new_segments, deleted_segments
    
    def _add_segments(self, segments, confv):
        """
        @override SegmentsMstClassifierMixin
        引数に与えられたセグメントを追加する。
        DDPのセグメント追加APIは、登録に成功するとセグメントIDを返却するために、
        これをDBに保存する
        """
        api_error_occurred = False
        self.logger.info('Sending new segment request')

        for segment in segments:
            try:
                operation = {
                                'operator' : 'ADD',
                                'operand' : { 
                                                 'xsi_type'    : 'BasicUserList',
                                                 'name'        : unicode(segment['name'], 'utf-8'),
                                                 'description' : unicode(segment['description'], 'utf-8') 
                                             }
                             }
                operations = [operation]

                self.result.increment_counter('request')
                dmp_userlists = self.dmp_userlist_service.mutate(self.clientCustomerId, 
                                                                 operations)
                self.result.increment_counter('request.success')
                for dmp_userlist in dmp_userlists['value']:
                    segment['segment_id_to'] = str(dmp_userlist['id'])
                    with segmentmsttable.open(self.sid, self.pid, self.db_config) as segmst:
                        segmst.insert_row(segment['id'], segment['segment_id_to'], 
                                          segment['name'], segment['description'])
            except Exception as e:
                api_error_occurred = True
                self.result.add_message(e.message)
                self.logger.error(segment['name'] + ' : ' + e.message)
                
        return api_error_occurred

    def _modify_segments(self, segments, convf):
        """
        @override SegmentMstClassifierMixin
        セグメント名の更新を行います
        """
        def _update_database(_segment, _payload):
            num = segmst.update_row_by_segment_id(_segment['segment_id_from'], _payload)
            if num == 1:
                self.logger.info('Segment \'{nam}\'({segid}) is updated to {name_modified}.'\
                                 .format(name=_segment['name'].encode('utf-8'),
                                         segid=_segment['segment_id_from'],
                                         name_modified=_segment['name_modified'].encode('utf-8')))
            else:
                message = 'DB error: Failed to update segment {segid}'.format(segid=_segment['segment_id_from'])
                self.result.add_message(message)
                self.logger.error(message)

        api_error_occurred = False
        self.logger.info('Sending new segment request')

        with segmentmsttable.open(self.sid, self.pid, self.db_config) as segmst:
            for segment in segments:
                try:
                    payload = {'name': segment['name_modified']}
                    operation = {
                                    'operator' : 'SET',
                                    'operand' : { 
                                                    'xsi_type'    : 'BasicUserList',
                                                    'id'          : segment['segment_id_to'],
                                                    'name'        : segment['name_modified'],
                                                }
                                 }
                    operations = [operation]

                    self.result.increment_counter('request')
                    dmp_userlists = self.dmp_userlist_service.mutate(self.clientCustomerId, 
                                                                     operations)
                    self.result.increment_counter('request.success')
                    for dmp_userlist in dmp_userlists['value']:
                        _update_database(segment, payload)

                except Exception as e:
                    api_error_occured = True
                    self.result.add_message(e.message)
                    self.logger.error(segment['name_modified'] + ' : ' + e.message)
                
            return api_error_occurred


    def _delete_segments(self, segments, convf):
        """
        @override SegmentMstClassifierMixin
        引数に与えられたセグメントをデータベースから削除し、
        連携先ではdisable状態に変更する
        """
        def _delete_from_database(_segment):
            num = segmst.delete_rows_by_segment_id(_segment['segment_id_from'])
            if num == 1:
                self.logger.info('Segment \'{name}\'({segid}) is deleted.'\
                                  .format(name=_segment['name'].encode('utf-8'),
                                          segid=_segment['segment_id_from']))
            else:
                message = 'DB error: Failed to delete segment {segid}'.format(segid=_segment['segment_id_from'])
                self.result.add_message(message)
                self.logger.error(message)

        api_error_occurred = False
        self.logger.info('Sending new segment request')

        with segmentmsttable.open(self.sid, self.pid, self.db_config) as segmst:
            try:
                for segment in segments:
                    payload = {'name': segment['name']}
                    operation = {
                                    'operator' : 'SET',
                                    'operand' : { 
                                                    'xsi_type'    : 'BasicUserList',
                                                    'id'          : segment['segment_id_to'],
                                                    'name'        : "[REMOVED]" + segment['name'],
                                                    'status'      : 'CLOSED'
                                                 }
                                 }
                    operations = [operation]

                    self.result.increment_counter('request')
                    dmp_userlists = self.dmp_userlist_service.mutate(self.clientCustomerId, 
                                                                     operations)
                    self.result.increment_counter('request.success')
                    for dmp_userlist in dmp_userlists['value']:
                        _delete_from_database(segment)
            except Exception as e:
                api_error_occured = True
                self.result.add_message(e.message)
                self.logger.error(segment['name'] + " : " + e.message)
            finally:
                return api_error_occurred
