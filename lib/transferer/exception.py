# -*- coding: utf-8 -*-

from __future__ import absolute_import


class NoS3Files(Exception):
    def __init__(self, s3bucket, s3path):
        self.s3bucket = s3bucket
        self.s3path   = s3path

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return 'No files {} {}'.format(self.s3bucket, self.s3path)
