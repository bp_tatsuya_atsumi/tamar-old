# -*- coding: utf-8 -*-

from collections import Counter
from collections import OrderedDict
import datetime
import gzip
import json
import os


class TransferResult(object):
    RESULT_OK, RESULT_API_ERROR, RESULT_NG = range(3)

    def __init__(self, sid, pid, env, data_type):
        self.sid             = sid
        self.pid             = pid
        self.env             = env
        self.data_type       = data_type

        self.target_filename = None
        self.result          = self.RESULT_OK
        self.messages        = []
        self.start_datetime  = datetime.datetime.now()
        self.end_datetime    = None
        self.counter         = Counter()

    def ok(self, message=None):
        return self._end(self.RESULT_OK, message)

    def api_error(self, message=None):
        return self._end(self.RESULT_API_ERROR, message)

    def ng(self, message=None):
        return self._end(self.RESULT_NG, message)

    def _end(self, result, message=None):
        self.result  = result
        self.end_datetime = datetime.datetime.now()
        self.add_message(message)
        return self

    def add_message(self, message):
        if len(self.messages) < 5:
            self.messages.append(message)

    def is_ok(self):
        return self.result == self.RESULT_OK

    def is_api_error(self):
        return self.result == self.RESULT_API_ERROR

    def is_ng(self):
        return self.result == self.RESULT_NG

    def increment_counter(self, counter_name, num=1):
        self.counter[counter_name] += num

    def decrement_counter(self, counter_name, num=1):
        self.increment_counter(counter_name, -num)

    def to_json(self):
        dict_result = OrderedDict()

        def _put_item(k, v):
            if v is None: return
            dict_result[k] = v

        def _format_datetime(dt):
            if not dt: return None
            return datetime.datetime.strftime(dt, '%Y-%m-%d %H:%M:%S.%f')

        _put_item('data_type',       self.data_type)
        _put_item('sid',             self.sid)
        _put_item('pid',             self.pid)
        _put_item('env',             self.env)
        _put_item('target_filename', self.target_filename)
        _put_item('result',          self.result)
        _put_item('message',         ','.join(self.messages))
        _put_item('datetime.start',  _format_datetime(self.start_datetime))
        _put_item('datetime.end',    _format_datetime(self.end_datetime))
        _put_item('counter',         OrderedDict(sorted(self.counter.items(),
                                                        key=lambda k: k[0])))
        return json.dumps(dict_result, indent=4)


class TransferConvertedFile(gzip.GzipFile):
    def __init__(self, file, mode='wb'):
        gzip.GzipFile.__init__(self, file, mode)

    @staticmethod
    def convert_path(file):
        filename = os.path.basename(file)
        dirname  = os.path.dirname(file)
        part = filename.split('.')[0]
        return os.path.join(dirname, part + '_converted.tsv.gz')

    def write_header(self):
        try:
            self.write('\t'.join([
                                    'request.endpoint',
                                    'request.data',
                                    'response.status_code',
                                    'response.text',
                                 ])
                       + '\n'
                      )
        except:
            return

    def _remove_retcode(self, string):
        if not string:
            return ''
        if isinstance(string, unicode):
            string = string.encode('utf-8')
        return string.replace('\n', '').replace('\r','').replace(' ', '')

    def _pp(self, obj):
        if not obj:
            return ''
        elif isinstance(obj, list) or isinstance(obj, dict):
            orig = json.dumps(obj, separators=(',', ':'))
            return eval("u'''%s'''" % orig).encode('utf-8')
        else:
            return obj

    def write_get_log(self, endpoint, params, response):
        try:
            self.write('\t'.join([
                                    endpoint,
                                    self._pp(params),
                                    str(response.status_code),
                                    self._remove_retcode(response.text)
                                 ])
                       + '\n'
                      )
        except:
            return

    def write_post_log(self, endpoint, data, response):
        try:
            self.write('\t'.join([
                                    endpoint,
                                    self._pp(data),
                                    str(response.status_code),
                                    self._remove_retcode(response.text)
                                 ])
                       + '\n'
                      )
        except:
            return

    def write_post_log_for_go(self, endpoint, payload, error_status):
        try:
            self.write('\t'.join([
                                    endpoint,
                                    self._pp(payload),
                                    error_status
                                ])
                        + '\n'
                      )
        except:
            return
