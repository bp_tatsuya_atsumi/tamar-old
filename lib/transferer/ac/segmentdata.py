# -*- coding: utf-8 -*-

"""
Advertising.com へセグメント連携するためのバッチ
"""

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'production'

import contextlib
import datetime
import json
import lib.util              as util
import lib.transferer.rtfile as rtfile
import requests
import traceback
from   collections                    import OrderedDict
from   lib.transferer.base            import TransfererBase
from   lib.transferer.exception       import NoS3Files
from   lib.transferer.transfer_result import TransferConvertedFile
from   requests                       import ConnectionError

class SegmentDataTransferer(TransfererBase):

    ENDPOINT_URL = {
                        'dev' : 'http://localhost:5000/api/segmentdata',
                        'stg' : 'http://ac-test.c-ovn.jp/api/segmentdata',
                        'prd' : 'http://ingest.at.atwola.com/ingestor/applications/jpbpad/targeting'
                    }
    API_DOCUMENT    = 'TDI'
    API_VERSION     = '1.0'
    API_SOURCE      = 'jpbpad'
    API_OK_STATUS_CODE = 204
    API_SEND_UNIT_PER_REQUEST = 100

    def __init__(self, sid, pid, env, s3bucket, s3path, **option):
        TransfererBase.__init__(self, sid, pid, env, 'segmentdata', s3bucket, s3path, **option)
        self.endpoint = self.ENDPOINT_URL[env]
        self.start_time  = int((datetime.datetime.utcnow() - datetime.datetime(1970, 1, 1)).total_seconds())

    def execute(self):
        self.logger.info('<Start segmentdata transfer process>')
        target_file, converted_file = None, None

        try:
            self.logger.info('Downloading latest segment data')
            target_file, target_filename = self._download_s3file()
            self.result.target_filename = target_filename

            self.logger.info('Sending request')
            converted_file = TransferConvertedFile.convert_path(target_file)
            row_idx   = 0
            query_str = ""
            with contextlib.nested(
                TransferConvertedFile(converted_file),
                rtfile.open_segmentdata(target_file, self._open_mappingtable())
            ) as (convf, reader):

                convf.write_header()

                for row in reader:
                    query_str += self._transform_transfer_format(row)
                    row_idx += 1
                    if row_idx % self.API_SEND_UNIT_PER_REQUEST == 0:
                        response = self._send_request(self.endpoint, query_str)
                        convf.write_post_log(self.endpoint, query_str.encode('string_escape'), response)
                        query_str = ""

                if not self._is_all_sent(query_str):
                    response = self._send_request(self.endpoint, query_str)
                    convf.write_post_log(self.endpoint, query_str.encode('string_escape'), response)

                self.logger.info('Finish sending {count} request(s)'.format(count=str(row_idx)))
                self.result.increment_counter('user.rtoaster', reader.line_count_all)
                self.result.increment_counter('user.mapped',   reader.line_count_mapped)
                self.result.increment_counter('user.unmapped', reader.line_count_unmapped)
                self.result.increment_counter('user.invalid',  reader.line_count_invalid)

            self.logger.info('Saving transfer file')
            self._save_converted_file(converted_file)

            if reader.line_count_invalid > 0:
                self.logger.warn('invalid line numbers: {nums}'.format(nums=','.join(reader.invalid_line_nums)))
                self.logger.warn('invalid lines\n{lines}'.format(lines='\n'.join(reader.invalid_lines)))
                return self.result.api_error('Some input error')
            return self.result.ok('OK')

        except NoS3Files, e:
            msg = 'File not exists in s3: {0} {1}'.format(e.s3bucket, e.s3path)
            self.logger.error(msg)
            return self.result.ng(msg)
        except:
            st = traceback.format_exc().splitlines()[-1]
            self.logger.exception('Failed to transfer: laststacktrace="%s"', st)
            return self.result.ng('Unexpected error: laststacktrace={}'.format(st))
        finally:
            self.logger.info('Removing tmpfile')
            util.remove_files([target_file, converted_file])

        self.logger.info('<End segmentdata transfer process>')

    def _init_query_dict(self):
        query_dict = OrderedDict()
        query_dict['document']  = self.API_DOCUMENT
        query_dict['version']   = self.API_VERSION
        query_dict['source']    = self.API_SOURCE
        query_dict['generationEpoch'] = self.start_time
        query_dict['users']     = []

        return query_dict

    def _transform_transfer_format(self, row):
        """
        row is like:
        {'mapped_uid': 'puid', 'attrs': [326231904, 1538571055], 'pid': 'puid', 'uid': '4276f650-1e6c-11e2-edd0-0019b9e2ccb4', 'sid': '9999'}

        request is like:  # separated by "\n", not by ","
        {"document":"TDI","version":"1.0","source":"jpbpad","generationEpoch":1385344767,"users":[{"destUserId":"1995bcc1qnn133","segments":[{"srcSegmentId":"25841"},{"srcSegmentId":"25946"}]}]}
        {"document":"TDI","version":"1.0","source":"jpbpad","generationEpoch":1385344769,"users":[{"destUserId":"1909ph416pkvsq","segments":[{"srcSegmentId":"25841"},{"srcSegmentId":"26319"},{"srcSegmentId":"26316"},{"srcSegmentId":"25946"}]}]}
        {"document":"TDI","version":"1.0","source":"jpbpad","generationEpoch":1385344771,"users":[{"destUserId":"18tb15o0jn4guf","segments":[{"srcSegmentId":"25841"},{"srcSegmentId":"27485"},{"srcSegmentId":"25946"}]}]}

        for easy understanding  # separated by "\n", not by ","
        {
            "document": "TDI",
            "version": "1.0",
            "source": "jpbpad",
            "generationEpoch": "1385344769",
            "users": [
                {
                    "destUserId": "uid",
                    "segments": [
                        {
                            "srcSegmentId": "2062202026",
                            "generationEpoch": "1385344769"
                        },
                        {
                            "srcSegmentId": "2062202027",
                            "generationEpoch": "1385344769"
                        }
                    ]
                }
            ]
        }
        {
            "document": "TDI",
            ....
        """

        query_dict = self._init_query_dict()
        tmp = {}
        tmp['destUserId'] = row['mapped_uid']
        tmp['segments']   = []
        for attr in row['attrs']:
            tmp['segments'].append({'srcSegmentId': str(attr), 'qualificationEpoch': self.start_time})
        query_dict['users'].append(tmp)

        return json.dumps(query_dict, ensure_ascii=True) + "\n"

    def _send_request(self, url, query_str):
        try:
            self.result.increment_counter('request')
            headers = {'content-type': 'application/json'}
            r = requests.post(url, data=query_str, headers=headers)
            self.result.increment_counter('request.' + str(r.status_code))

            if r.status_code != self.API_OK_STATUS_CODE:
                self.logger.warn('HTTP STATUS CODE IS NOT {ok}. STATUS CODE IS: {actual}'\
                                .format(ok=self.API_OK_STATUS_CODE,
                                        actual=str(r.status_code)))
                self.logger.warn(query_str)
        except ValueError, e:
            raise e
        except ConnectionError, e:
            raise e

        return r

    def _is_all_sent(self, data):
        return len(data) == 0
