# -*- coding: utf-8 -*-

"""
ScaleOut へアイテムマスタ連携するためのバッチ
"""

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'production'

from   collections                    import OrderedDict
import gzip
import json
import os
import lib.sftputil                   as     sftputil
from   lib.transferer.base            import TransfererBase
from   lib.transferer.exception       import NoS3Files
import lib.transferer.rtfile          as     rtfile
import lib.transferer.so.soutil       as     soutil
from   lib.transferer.transfer_result import TransferResult
import lib.util                       as     util

class ItemMstTransferer(TransfererBase):

    SFTP_CONFIG = {
                    'dev' : {
                                'host'       : 'sftp-s.c-ovn.jp',
                                'port'       : 115,
                                'username'   : {
                                                    'brainpad' : 'rtoaster9999',
                                                    'dcap'     : 'rtoaster9999'
                                               },
                                'password'   : 'rtoaster9999',
                                'remote_dir' : '/receive/so/content'
                            },
                    'stg' : {
                                'host'       : 'so-test.c-ovn.jp',
                                'port'       : 115,
                                'username'   : {
                                                    'brainpad' : 'brainpad-so',
                                                    'dcap'     : 'brainpad-so'
                                               },
                                'pkey_file'  : 'virtual_partner/ssh_key/stg_scaleout_id_rsa',
                                'remote_dir' : 'content'
                            },
                    'prd' : {
                                'host'       : 'up.scaleout.jp',
                                'port'       : 115,
                                'username'   : {
                                                    'brainpad' : 'brainpad',
                                                    'dcap'     : 'dcap'
                                               },
                                'pkey_file'  : 'settings/site/batch/ami/scaleout_id_dsa',
                                'remote_dir' : '/content'
                            }
                  }

    S3_PATH_LATEST_ITEMMST = '{sid}/item_mst/latest'

    def __init__(self, sid, pid, env, s3bucket, s3path, **option):
        TransfererBase.__init__(self, sid, pid, env, 'itemmst', s3bucket, s3path, **option)

        self.s3_path_latest = self.S3_PATH_LATEST_ITEMMST.format(sid=sid)
        self.s3_path_cnv    = self.s3_path_latest.replace('/latest', '/converted/' + pid)
        self.aid            = self.mapping_auth['aid'].encode('utf8')
        self.foreign_id     = self.mapping_auth['foreign_id'].encode('utf8')

    def execute(self):
        self.logger.info('<Start itemmst transfer process>')
        target_file, transfer_file = None, None

        try:
            result = TransferResult(self.sid, self.pid, self.env, 'itemmst')

            self.logger.info('Downloading latest item master')
            target_file, target_filename = self._download_latest_s3file(self.s3_path_latest)
            result.target_filename = target_filename

            transfer_file = soutil.create_transfer_filename(self.tmp_dir,
                                                            self.aid,
                                                            self.foreign_id,
                                                            filetype='item')
            self.logger.info('Transfer filename: ' + os.path.basename(transfer_file))

            self.logger.info('Creating transfer file')
            with gzip.open(transfer_file, 'wb') as outf, \
                 rtfile.open_itemmst(target_file) as reader:
                for row in reader:
                    outf.write(self._transform_transfer_format(row) + '\n')

            result.increment_counter('item.rtoaster', reader.line_count_all)

            self.logger.info('Sending transfer file')
            send_result = self._send_transformed_file(transfer_file)

            self.logger.info('Saving transfer file')
            self.s3util.upload_single_file(transfer_file, self.s3bucket, self.s3_path_cnv)

            if not send_result:
                return result.ng('Sftp error')
            return result.ok('OK')

        except NoS3Files, e:
            msg = 'File not exists in s3: {0} {1}'.format(e.s3bucket, e.s3path)
            self.logger.error(msg)
            return result.ng(msg)

        except:
            self.logger.exception('Failed to transfer')
            return result.ng('Unexpected error occured')

        finally:
            self.logger.info('Removing tmpfile')
            util.remove_files([target_file, transfer_file])

            self.logger.info('<End itemmst transfer process>')

    def _transform_transfer_format(self, row):
        data = OrderedDict()
        data['aid']       = self.aid
        data['sid']       = self.foreign_id
        data['code']      = row['code']
        data['name']      = row['name']
        data['imageUrl']  = row['img_url']
        data['url']       = row['link_url']
        data['price']     = row['price']
        data['comment']   = row['description']
        return json.dumps(data, ensure_ascii=False, separators=(',', ':'))

    def _send_transformed_file(self, local_file):
        try:
            sftp_config = self.SFTP_CONFIG[self.env]
            username = sftp_config['username'][self.aid]
            sftputil.put(host=sftp_config['host'],
                         port=sftp_config['port'],
                         username=username,
                         password=sftp_config.get('password'),
                         pkey_file=sftp_config.get('pkey_file'),
                         local_file=local_file,
                         remote_dir=sftp_config['remote_dir'],
                         rename=True)
        except:
            self.logger.exception('Failed to sftp put')
            return False
        else:
            return True
