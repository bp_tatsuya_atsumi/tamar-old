# -*- coding: utf-8 -*-

"""
ScaleOut 用バッチのユーティリティクラス
"""

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'production'

import datetime
import os
from   lib.sesutil import SESUtil

def create_transfer_filename(localdir,
                             aid,
                             foreign_id,
                             filedatetime=None,
                             filetype=None,
                             seqno=None):
    # {aid}-{yyyymmdd}-{hhmmss}-{foreign_id}(-{filetype})(-{seqno}).json.gz
    if filedatetime:
        dt = filedatetime
    else:
        dt = datetime.datetime.now()
    elems = [
                 aid,
                 dt.strftime('%Y%m%d'),
                 dt.strftime('%H%M%S'),
                 foreign_id
            ]
    if filetype:
        elems.append(filetype)
    if seqno:
        elems.append('{:03d}'.format(seqno))
    return os.path.join(localdir, '-'.join(elems) + '.json.gz')


MAIL_SUBJECT = u'転送状況レポート [{env}] [{data_type}] [{sid}] [{pid}] [{complete_file_num}/{total_file_num}]'
MAIL_BODY    = u'''
対象ファイル: {target_filename}
転送対象行数: {total_line_num}
転送対象ファイル数: {total_file_num}
転送完了ファイル数: {complete_file_num}
開始日時: {start_datetime}
'''

def report_transfer_status(transfer_result, total_line_num, total_file_num, complete_file_num):
    if total_file_num == 1:
        return
    subject = MAIL_SUBJECT.format(
                  env=transfer_result.env,
                  data_type=transfer_result.data_type,
                  sid=transfer_result.sid,
                  pid=transfer_result.pid,
                  complete_file_num=complete_file_num,
                  total_file_num=total_file_num
              )
    body    = MAIL_BODY.format(
                  target_filename=transfer_result.target_filename,
                  total_line_num=total_line_num,
                  total_file_num=total_file_num,
                  complete_file_num=complete_file_num,
                  start_datetime=datetime.datetime.strftime(
                                     transfer_result.start_datetime,
                                     '%Y-%m-%d %H:%M:%S.%f'
                                 )
              )
    SESUtil(transfer_result.env).send_mail(subject, body)
