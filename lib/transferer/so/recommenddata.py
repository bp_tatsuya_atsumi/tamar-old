# -*- coding: utf-8 -*-

"""
ScaleOut へレコメンドデータ連携するためのバッチ
"""

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'production'

import datetime
import gzip
import json
import math
import os
import lib.sftputil                 as     sftputil
import lib.transferer.rtfile        as     rtfile
import lib.transferer.so.soutil     as     soutil
import lib.util                     as     util
import traceback
from collections                    import OrderedDict
from lib.transferer.base            import TransfererBase
from lib.transferer.exception       import NoS3Files
from lib.transferer.transfer_result import TransferResult

class RecommendDataTransferer(TransfererBase):
    SFTP_CONFIG = {
                    'dev' : {
                                'host'       : 'sftp-s.c-ovn.jp',
                                'port'       : 115,
                                'username'   : {
                                                    'brainpad' : 'rtoaster9999',
                                                    'dcap'     : 'rtoaster9999'
                                               },
                                'password'   : 'rtoaster9999',
                                'remote_dir' : '/receive/so/extra'
                            },
                    'stg' : {
                                'host'       : 'so-test.c-ovn.jp',
                                'port'       : 115,
                                'username'   : {
                                                    'brainpad' : 'brainpad-so',
                                                    'dcap'     : 'brainpad-so'
                                               },
                                'pkey_file'  : 'virtual_partner/ssh_key/stg_scaleout_id_rsa',
                                'remote_dir' : 'extra'
                            },
                    'prd' : {
                                'host'       : 'up.scaleout.jp',
                                'port'       : 115,
                                'username'   : {
                                                    'brainpad' : 'brainpad',
                                                    'dcap'     : 'dcap'
                                               },
                                'pkey_file'  : 'settings/site/batch/ami/scaleout_id_dsa',
                                'remote_dir' : '/extra'
                            }
                  }
    S3_PATH_RECOMMENDMST = '{sid}/recommend_list_mst/raw'
    SPLIT_LINE_NUM       = 1000000

    def __init__(self, sid, pid, env, s3bucket, s3path, **option):
        TransfererBase.__init__(self, sid, pid, env, 'recommenddata', s3bucket, s3path, **option)
        self.s3_path_mst = self.S3_PATH_RECOMMENDMST.format(sid=sid)

        self.aid        = self.mapping_auth['aid']
        self.foreign_id = self.mapping_auth['foreign_id']

    def execute(self):
        self.logger.info('<Start recommenddata transfer process>')
        target_file, master_file, transfer_files = None, None, []

        try:
            result = TransferResult(self.sid, self.pid, self.env, 'recommenddata')

            self.logger.info('Downloading recommend data')
            target_file, target_filename = self._download_s3file()
            result.target_filename = target_filename

            self.logger.info('Downloading latest recommend master')
            master_file, _ = self._download_latest_s3file(self.s3_path_mst)

            self.logger.info('Loading recommend master')
            with rtfile.open_recommendmst(master_file) as reader:
                recid_recnames = {row['id']: row['name'] for row in reader}

            total_line_num = sum(1 for l in gzip.open(target_file))
            total_file_num = int(math.ceil(total_line_num * 1.0 / self.SPLIT_LINE_NUM))
            self.logger.info('Target file: {:d} lines -> split to {:d} files'.format(total_line_num, total_file_num))

            # prepare for create file
            filedatetime  = datetime.datetime.now()
            transfer_file = None
            transferf     = None
            fileseqno     = 0

            def _init_transfer_file():
                transfer_file = soutil.create_transfer_filename(
                                    self.tmp_dir,
                                    self.aid,
                                    self.foreign_id,
                                    filedatetime=filedatetime,
                                    filetype='recommend',
                                    seqno=fileseqno
                                )
                transfer_files.append(transfer_file)
                self.logger.info('Creating transfer file: {} [{:d} of {:d}]'
                                .format(os.path.basename(transfer_file), fileseqno, total_file_num))
                return transfer_file, gzip.open(transfer_file, 'wb')

            def _send_transfer_file():
                transferf.close()
                self.logger.info('Saving transfer file [{:d} of {:d}]'.format(fileseqno, total_file_num))
                self._save_converted_file(transfer_file)
                self.logger.info('Sending transfer file [{:d} of {:d}]'.format(fileseqno, total_file_num))
                is_send_suceess = self.send_transformed_file(transfer_file)
                if is_send_suceess:
                    self.logger.info('Reporting transfer status [{:d} of {:d}]'.format(fileseqno, total_file_num))
                    soutil.report_transfer_status(result,
                                                  total_line_num,
                                                  total_file_num,
                                                  fileseqno)
                return is_send_suceess

            # create and send transfer files
            with rtfile.open_recommenddata(target_file,
                                           self._open_mappingtable(),
                                           oldest_user=False) as reader:
                i = 0
                fileseqno += 1
                transfer_file, transferf = _init_transfer_file()
                for i, row in enumerate(reader):

                    if i % self.SPLIT_LINE_NUM == 0 and i > 0:
                        if not _send_transfer_file():
                            return result.ng('Sftp error')
                        fileseqno += 1
                        transfer_file, transferf = _init_transfer_file()

                    transferf.write(self.transform_transfer_format(row, recid_recnames) + '\n')

                if not _send_transfer_file():
                    return result.ng('Sftp error')

            result.increment_counter('user.rtoaster', reader.line_count_all)
            result.increment_counter('user.mapped',   reader.line_count_mapped)
            result.increment_counter('user.unmapped', reader.line_count_unmapped)
            result.increment_counter('user.invalid',  reader.line_count_invalid)

            if reader.line_count_invalid > 0:
                self.logger.warn('invalid line numbers: ' + ','.join([str(n) for n in reader.invalid_line_nums]))
                self.logger.warn('invalid lines\n' + '\n'.join(reader.invalid_lines))
                return result.api_error('Some invalid lines')
            return result.ok('OK')

        except NoS3Files, e:
            msg = 'File not exists in s3: {0} {1}'.format(e.s3bucket, e.s3path)
            self.logger.error(msg)
            return result.ng(msg)

        except:
            st = traceback.format_exc().splitlines()[-1]
            self.logger.exception('Failed to transfer: laststacktrace="%s"', st)
            return result.ng('Unexpected error: laststacktrace={}'.format(st))

        finally:
            self.logger.info('Removing tmpfile')
            util.remove_files([target_file, master_file])
            util.remove_files(transfer_files)

            self.logger.info('<End recommenddata transfer process>')

    def transform_transfer_format(self, row, recid_recnames):
        recs = []
        data = OrderedDict()
        data['aid'] = self.aid
        data['sid'] = self.foreign_id
        for mapped_uid in row['mapped_uids']:
            data['uid']        = mapped_uid
            data['item_lists'] = {recid_recnames[recid]: item_codes for recid, item_codes in row['item_lists'].items()}
            recs.append(json.dumps(data, separators=(',', ':')))
        return '\n'.join(recs)

    def send_transformed_file(self, local_file):
        try:
            sftp_config = self.SFTP_CONFIG[self.env]
            username = sftp_config['username'][self.aid]
            sftputil.put(host=sftp_config['host'],
                         port=sftp_config['port'],
                         username=username,
                         password=sftp_config.get('password'),
                         pkey_file=sftp_config.get('pkey_file'),
                         local_file=local_file,
                         remote_dir=sftp_config['remote_dir'],
                         rename=True)
        except:
            self.logger.exception('Failed to sftp put')
            return False
        else:
            return True
