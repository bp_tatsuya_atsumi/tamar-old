# -*- coding: utf-8 -*-

"""
メールセグメントデータを、セグメント毎にファイル分割するデータ整形バッチ
"""

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'production'

import datetime
import gzip
import os
import lib.sftputil             as     sftputil
from   lib.transferer.base      import TransfererBase
from   lib.transferer.exception import NoS3Files
from   sets                     import Set
import lib.transferer.rtfile    as     rtfile
import lib.util                 as     util
import traceback
import zipfile

class MailSegmentDataTransferer(TransfererBase):

    def __init__(self, sid, pid, env, s3bucket, s3path, **option):
        TransfererBase.__init__(self, sid, pid, env, 'mailsegmentdata', s3bucket, s3path, **option)

    def execute(self):
        self.logger.info('<Start mailsegmentdata transfer process>')
        target_file = None
        transfer_files = []

        try:
            self.logger.info('Downloading s3file')
            target_file, target_filename = self._download_s3file()
            self.result.target_filename  = target_filename

            self.logger.info('Creating transfer file')
            transfer_files = self._create_transfer_files(target_file)

            self.logger.info('Saving {} transfer file(s)'.format(len(transfer_files)))
            for transfer_file in transfer_files:
                self._save_converted_file(transfer_file)

            self.logger.info('Sending {} transfer file(s)'.format(len(transfer_files)))
            if not self._send_transfer_files(transfer_files):
                raise Exception()

        except NoS3Files, e:
            msg = 'File not exists in s3: {0} {1}'.format(e.s3bucket, e.s3path)
            self.logger.error(msg)
            return self.result.ng(msg)
        except:
            self.logger.exception('Failed to transfer')
            return self.result.ng('Unexpected error occured')
        finally:
            self.logger.info('Removing tmpfile')
            transfer_files.append(target_file)
            util.remove_files(transfer_files)

        self.logger.info('<End mailsegmentdata transfer process>')
        return self.result.ok('OK')

    def _create_transfer_files(self, target_file):
        transfer_files = []

        # Write
        segments = Set([])
        writers  = {}
        try:
            with rtfile.open_mailsegmentdata(target_file, None, no_mapping=True) as reader:
                for row in reader:
                    current_segments = Set(row['attrs'])
                    new_segments = current_segments - segments
                    if len(new_segments) > 0:
                        segments = segments | new_segments
                        for new_segment in new_segments:
                            self.result.increment_counter('segment.num')
                            tmp_filepath = self._create_transfer_filename(new_segment)
                            transfer_files.append(tmp_filepath)
                            writers[new_segment] = gzip.open(tmp_filepath.encode('utf-8'), 'wb')
                            writers[new_segment].write('MEMBER_ID\r\n')
                    for current_segment in current_segments:
                        writers[current_segment].write(row['uid'].encode('utf-8') + '\r\n')
                    self.result.increment_counter('user.rtoaster')
        finally:
            for writer in writers.values():
                writer.close()

        return transfer_files

    def _create_transfer_filename(self, segmentname):
        filedatetime = datetime.datetime.now()
        filename =  u'{segmentname}_{yyyymmdd}_{sid}.csv.gz'\
                    .format(yyyymmdd=datetime.datetime.strftime(filedatetime, '%Y%m%d'),
                            segmentname=segmentname,
                            sid=self.sid)
        return os.path.join(self.tmp_dir, filename)

    def _send_transfer_files(self, transfer_files):
        try:
            sftp_config = self.mapping_auth
            with sftputil.open(host=sftp_config['host'],
                               port=sftp_config['port'],
                               username=sftp_config['username'],
                               password=sftp_config.get('password'),
                               pkey_file=sftp_config.get('pkey_file')) as sftp:
                 for transfer_file in transfer_files:
                     sftp.put(local_file=transfer_file,
                              remote_dir=sftp_config['remote_dir'],
                              rename=True)
                     self.result.increment_counter('segment.sftp.ok')
        except:
            self.logger.exception('Failed to sftp put: %s',
                                  traceback.format_exc().splitlines()[-1])
            return False
        else:
            return True
