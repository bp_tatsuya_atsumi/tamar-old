# -*- coding: utf-8 -*-

"""
IntimateMerger へオーディエンスデータを送るためのバッチ
"""

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'production'

import json
import traceback
import requests
import lib.segmentmsttable            as     segmentmsttable
from   lib.transferer.im.base         import IMBase
from   lib.transferer.exception       import NoS3Files
import lib.transferer.rtfile          as     rtfile
from   lib.transferer.transfer_result import TransferConvertedFile
import lib.util                       as     util

class SegmentDataTransferer(IMBase):

    FLUSH_LIMIT        = 20000
    REQUEST_USER_LIMIT = 1000
    ENDPOINT_URL = {
        'dev' : u'http://localhost:5000/segment/set?customer_id={customer_id}&token={{token}}',
        'stg' : u'http://im-test.c-ovn.jp/segment/set?customer_id={customer_id}&token={{token}}',
        'prd' : u'https://im-apps.net/audience/api/v1/segment/set?customer_id={customer_id}&token={{token}}'
    }

    def __init__(self, sid, pid, env, s3bucket, s3path, **option):
        IMBase.__init__(self, sid, pid, env, 'segmentdata', s3bucket, s3path, **option)
        self.customer_id = self.mapping_auth['customer_id']
        self.endpoint    = self.ENDPOINT_URL[self.env].format(customer_id=self.customer_id)

    def execute(self):
        self.logger.info('<Start segmentdata transfer process>')
        target_file, converted_file = None, None

        try:
            self.logger.info('Downloading s3file')
            target_file, target_filename = self._download_s3file()
            self.result.target_filename = target_filename

            converted_file = TransferConvertedFile.convert_path(target_file)

            with TransferConvertedFile(converted_file) as convf:
                convf.write_header()

                self.logger.info('Sending request')
                api_error_occurred = self._send_requests(target_file, convf)

            self.logger.info('Saving transfer file')
            self._save_converted_file(converted_file)

            if api_error_occurred:
                return self.result.api_error('Some api error occured')
            elif self.result.counter['segment.unknown']:
                return self.result.api_error('Some segment is unknown and wasn\'t delivered.')
            else:
                return self.result.ok('OK')

        except NoS3Files, e:
            msg = 'File not exists in s3: {0} {1}'.format(e.s3bucket, e.s3path)
            self.logger.error(msg)
            return self.result.ng(msg)
        except:
            st = traceback.format_exc().splitlines()[-1]
            self.logger.exception('Failed to transfer: laststacktrace="%s"', st)
            return self.result.ng('Unexpected error: laststacktrace={}'.format(st))
        finally:
            self.logger.info('Removing tmpfile')
            util.remove_files([target_file, converted_file])
            self.logger.info('<End segmentdata transfer process>')

    def _send_requests(self, target_file, fp_converted):
        """
        セグメントデータの内容を読み込み、IntimateMeger へリクエストを送信する。
        実際の送信処理は '_send_each_request メソッドが行う
        """

        seg_uids = {}

        def classify_attrs(r_segdata):
            for attr in r_segdata['attrs']:
                pt_uid = r_segdata['mapped_uid']
                if attr not in seg_uids:
                    seg_uids[attr] = {'ptuids': [pt_uid] }
                else:
                    seg_uids[attr]['ptuids'].append(pt_uid)

        def execute_request():
            api_error_occurred = False
            for rt_segid, v in seg_uids.items():
                if not v.has_key('pt_segid'):
                    with segmentmsttable.open(self.sid, self.pid, self.db_config) as segmst:
                        r_segmst = segmst.select_row_by_segment_id(str(rt_segid))
                    if r_segmst:
                        self.result.increment_counter('segment.known')
                        v['pt_segid'] = r_segmst['segment_id_to']
                    else:
                        v['pt_segid'] = None
                        self.result.increment_counter('segment.unknown')
                        self.logger.warn('segment id [{segid}] is not registered.'.format(segid=rt_segid))
                if v.get('pt_segid') and len(v['ptuids']) > 0:
                    api_error_occurred = self._send_each_request(v['pt_segid'], v['ptuids'], fp_converted) | api_error_occurred
                v['ptuids'] = []
            return api_error_occurred

        api_error_occurred = False
        with rtfile.open_segmentdata(target_file, self._open_mappingtable()) as reader:
            row_counter = 0

            for r_segdata in reader:
                classify_attrs(r_segdata)
                row_counter += 1

                if row_counter >= self.FLUSH_LIMIT:
                    row_counter = 0
                    api_error_occurred = execute_request() | api_error_occurred

            if row_counter > 0:
                api_error_occurred = execute_request() | api_error_occurred

            self.result.increment_counter('user.rtoaster', reader.line_count_all)
            self.result.increment_counter('user.mapped',   reader.line_count_mapped)
            self.result.increment_counter('user.unmapped', reader.line_count_unmapped)
            self.result.increment_counter('user.invalid',  reader.line_count_invalid)

        return api_error_occurred

    def _send_each_request(self, pt_segment_id, pt_uids, fp_converted):
        """
        実際に IntimateMerger へデータ送信を行う。
        1回のリクエストあたりのユーザー数は 'REQUEST_USER_LIMIT 以下になるように分割送信する。
        """

        api_error_occurred = False
        while len(pt_uids) > 0:
            _pt_uids  = pt_uids[ - self.REQUEST_USER_LIMIT :]
            payload = {'segment_id'   : pt_segment_id, 'audience_ids' : _pt_uids}
            headers = {'content-type': 'application/json'}
            self.result.increment_counter('request')
            response = requests.post(self.endpoint.format(token=self.token), data=json.dumps(payload), headers=headers)
            self.result.increment_counter('request.' + str(response.status_code))
            if response.status_code != 200:
                api_error_occurred = True
                message = 'Api error: {0}'.format(response.text)
                self.result.add_message(message)
                self.logger.warn(message)

            fp_converted.write_post_log(self.endpoint.format(token=self.current_token), payload, response)

            pt_uids = pt_uids[0: - self.REQUEST_USER_LIMIT]
        return api_error_occurred
