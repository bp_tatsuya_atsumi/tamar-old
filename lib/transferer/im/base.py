# -*- coding: utf-8 -*-

"""
Common class for IntimateMerger batches.
This class mainly manages tokens from IntimateMerger.
"""

__author__  = 'Hitoshi Tsuda'
__status__  = 'production'
__version__ = '1.0.0'
__date__    = '2014/6/4'

import datetime
import json
import requests
from   lib.transferer.base import TransfererBase


class IMBase(TransfererBase):

    TOKEN_ENDPOINT_URL = {
        'dev' : 'http://localhost:5000/token/get?username={username}&password={password}&api_key={api_key}',
        'stg' : 'http://im-test.c-ovn.jp/token/get?username={username}&password={password}&api_key={api_key}',
        'prd' : 'https://im-apps.net/audience/api/v1/token/get?username={username}&password={password}&api_key={api_key}'
    }
    TOKEN = None
    LAST_TIME = None

    def __init__(self, sid, pid, env, datatype, s3bucket, s3path, **option):
        TransfererBase.__init__(self, sid, pid, env, datatype, s3bucket, s3path, **option)
        self.env = env
        self.datatype = datatype
        self.token_endpoint  = self.TOKEN_ENDPOINT_URL[self.env]\
                               .format(username=self.partner_option['api_config']['username'],
                                       password=self.partner_option['api_config']['password'],
                                       api_key=self.partner_option['api_config']['api_key'])

    @property
    def token(self):
        if self._is_token_expired():
            self._refresh_token()
        return self.TOKEN

    @property
    def current_token(self):
        return self.TOKEN

    def _is_token_expired(self):
        return self.LAST_TIME == None or\
               self.LAST_TIME + datetime.timedelta(hours=3) < datetime.datetime.now()

    def _refresh_token(self):
        response = requests.get(self.token_endpoint)
        if response.status_code != 200:
            self.logger.exception('Failed to refresh token')
            raise Exception('Failed to refresh token')
        self.TOKEN     = json.loads(response.text)['token']
        self.LAST_TIME = datetime.datetime.now()
