# -*- coding: utf-8 -*-

"""
IntimateMerger とセグメントマスタ連携を行うバッチ
"""

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'production'

import json
import requests
import traceback
from   lib.transferer.im.base                 import IMBase
from   lib.transferer.segmst_classifier_mixin import SegmentMstClassifierMixin
from   lib.transferer.exception               import NoS3Files
import lib.transferer.rtfile                  as     rtfile
import lib.segmentmsttable                    as     segmentmsttable
from   lib.transferer.transfer_result         import TransferConvertedFile
import lib.util                               as     util

class SegmentMstTransferer(IMBase, SegmentMstClassifierMixin):

    ENDPOINT_URL = {
        'post': {
            'dev' : u'http://localhost:5000/segment?customer_id={customer_id}&token={{token}}',
            'stg' : u'http://im-test.c-ovn.jp/segment?customer_id={customer_id}&token={{token}}',
            'prd' : u'https://im-apps.net/audience/api/v1/segment/?customer_id={customer_id}&token={{token}}'
        },
        'put': {
            'dev' : u'http://localhost:5000/segment/{{seg_id}}/?customer_id={customer_id}&token={{token}}',
            'stg' : u'http://im-test.c-ovn.jp/segment/{{seg_id}}/?customer_id={customer_id}&token={{token}}',
            'prd' : u'https://im-apps.net/audience/api/v1/segment/{{seg_id}}/?customer_id={customer_id}&token={{token}}'
        }
    }

    def __init__(self, sid, pid, env, s3bucket, s3path, **option):
        IMBase.__init__(self, sid, pid, env, 'segmentmst', s3bucket, s3path, **option)
        self.customer_id  = self.mapping_auth['customer_id']
        self.endpoint = {
            "post": self.ENDPOINT_URL['post'][self.env].format(customer_id=self.customer_id),
            "put" : self.ENDPOINT_URL['put'][self.env].format(customer_id=self.customer_id)
        }

    def execute(self):
        def _retrieve_all_target_segments():
            return [row for row in rtfile.open_segmentmst(target_file)]

        def _retrieve_all_registered_segments():
            with segmentmsttable.open(self.sid, self.pid, self.db_config) as segmst:
                registered_segment_rowproxies = segmst.select_all_rows()
            return [dict(rsr.items()) for rsr in registered_segment_rowproxies]

        self.logger.info('<Start segmentdata transfer process>')
        target_file, converted_file = None, None

        try:
            self.logger.info('Downloading s3file')
            target_file, target_filename = self._download_s3file()
            self.result.target_filename = target_filename

            converted_file = TransferConvertedFile.convert_path(target_file)

            target_segments     = _retrieve_all_target_segments()
            registered_segments = _retrieve_all_registered_segments()

            modified_segments, new_segments, deleted_segments =\
                self._classify_segments(target_segments, registered_segments)

            api_error_occurred = False
            with TransferConvertedFile(converted_file) as convf:
                convf.write_header()
                if modified_segments:
                    api_error_occurred = self._modify_segments(modified_segments, convf) | api_error_occurred
                if deleted_segments:
                    api_error_occurred = self._delete_segments(deleted_segments, convf) | api_error_occurred
                if new_segments:
                    api_error_occurred = self._add_segments(new_segments, convf) | api_error_occurred

            self.logger.info('Saving transfer file')
            self._save_converted_file(converted_file)

            if api_error_occurred:
                return self.result.api_error('Some api error occured')
            else:
                return self.result.ok('OK')

        except NoS3Files, e:
            msg = 'File not exists in s3: {0} {1}'.format(e.s3bucket, e.s3path)
            self.logger.error(msg)
            return self.result.ng(msg)
        except:
            st = traceback.format_exc().splitlines()[-1]
            self.logger.exception('Failed to transfer: laststacktrace="%s"', st)
            return self.result.ng('Unexpected error: laststacktrace={}'.format(st))
        finally:
            self.logger.info('Removing tmpfile')
            util.remove_files([target_file, converted_file])
            self.logger.info('<End segmentdata transfer process>')

    def _classify_segments(self, target_segments, registered_segments):
        """
        @override SegmentMstClassifierMixin
        引数のセグメントを new, deleted, modified の 3つのステータスに分類する
        SegmentMstClassifierMixin の _classify_segments を呼び、カウンタへ登録する
        """
        modified_segments, new_segments, deleted_segments =\
            SegmentMstClassifierMixin._classify_segments(self, target_segments, registered_segments, check_description=False)
        self.result.increment_counter('segment.modified', len(modified_segments))
        self.result.increment_counter('segment.new',      len(new_segments))
        self.result.increment_counter('segment.deleted',  len(deleted_segments))
        return modified_segments, new_segments, deleted_segments

    def _send_request(self, method_type, endpoint, payload, convf):
        try:
            method = getattr(requests, method_type)
        except AttributeError:
            self.logger.exception('Unknown method_type was given')
            raise Exception()

        headers = {'content-type': 'application/json'}
        self.result.increment_counter('request')
        response = method(endpoint, data=json.dumps(payload), headers=headers)
        self.result.increment_counter('request.' + str(response.status_code))
        convf.write_post_log(endpoint, payload, response)
        return response

    def _modify_segments(self, segments, convf):
        """
        @override SegmentMstClassifierMixin
        セグメント名の更新を行います
        """
        def _update_database(_segment, _payload):
            num = segmst.update_row_by_segment_id(_segment['segment_id_from'], _payload)
            if num == 1:
                self.logger.info('Segment \'{name}\'({segid}) is updated to {name_modified}.'\
                                 .format(name=_segment['name'].encode('utf-8'),
                                         segid=_segment['segment_id_from'],
                                         name_modified=_segment['name_modified'].encode('utf-8')))
            else:
                message = 'DB error: Failed to update segment {segid}'.format(segid=_segment['segment_id_from'])
                self.result.add_message(message)
                self.logger.error(message)

        # main
        api_error_occurred = False
        self.logger.info('Sending renaming request to modified segments')

        with segmentmsttable.open(self.sid, self.pid, self.db_config) as segmst:
            for segment in segments:
                payload = {'name': segment['name_modified']}
                response = self._send_request(
                    "put",
                    self.endpoint['put'].format(token=self.token,seg_id=segment['segment_id_to']),
                    payload,
                    convf
                )
                if response.status_code == 200:
                    _update_database(segment, payload)
                else:
                    api_error_occurred = True
                    message = 'Api error: {0}'.format(response.text)
                    self.result.add_message(message)
                    self.logger.error(message)
        return api_error_occurred

    def _delete_segments(self, segments, convf):
        """
        @override SegmentMstClassifierMixin
        引数に与えられたセグメントをデータベースから削除する。
        """
        def _delete_from_database(_segment):
            num = segmst.delete_rows_by_segment_id(_segment['segment_id_from'])
            if num == 1:
                self.logger.info('Segment \'{name}\'({segid}) is deleted.'\
                                 .format(name=_segment['name'].encode('utf-8'),
                                         segid=_segment['segment_id_from']))
            else:
                message = 'DB error: Failed to delete segment {segid}'.format(segid=_segment['segment_id_from'])
                self.result.add_message(message)
                self.logger.error(message)

        # main
        api_error_occurred = False
        self.logger.info('Sending renaming request to deleted segments')

        with segmentmsttable.open(self.sid, self.pid, self.db_config) as segmst:
            for segment in segments:

                response = self._send_request(
                    "put",
                    self.endpoint['put'].format(token=self.token,seg_id=segment['segment_id_to']),
                    {'name': "[REMOVED] " + segment['name']},
                    convf
                )
                if response.status_code == 200:
                    _delete_from_database(segment)
                else:
                    api_error_occurred = True
                    message = 'Api error: {0}'.format(response.text)
                    self.result.add_message(message)
                    self.logger.error(message)
        return api_error_occurred

    def _add_segments(self, segments, convf):
        """
        @override SegmentMstClassifierMixin
        引数に与えられたセグメントを追加する。
        IntimateMerger のセグメント追加 API は、登録に成功するとセグメント ID を返却するため、これを DB に保存する。
        """
        api_error_occurred = False
        self.logger.info('Sending new segment request')

        for segment in segments:
            response = self._send_request("post", self.endpoint['post'].format(token=self.token), {'name': segment['name']}, convf)

            if response.status_code == 200:
                segment['segment_id_to'] = str(json.loads(response.text)['id'])
                with segmentmsttable.open(self.sid, self.pid, self.db_config) as segmst:
                    segmst.insert_row(segment['id'], segment['segment_id_to'], segment['name'], segment['description'])
            else:
                api_error_occurred = True
                message = 'Api error: {0}'.format(response.text)
                self.result.add_message(message)
                self.logger.warn(message)

        return api_error_occurred
