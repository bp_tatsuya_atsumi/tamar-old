# -*- coding: utf-8 -*-

"""
MicroAd BLADE へコンバージョンデータを連携するためのバッチ
"""

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'production'

import datetime
import os
import re
import lib.sftputil             as     sftputil
from   lib.transferer.base      import TransfererBase
from   lib.transferer.exception import NoS3Files
import lib.transferer.rtfile    as     rtfile
import lib.util                 as     util

class ConversionTransferer(TransfererBase):
    SFTP_CONFIG = {
                    'dev' : {
                                'host'        : 'sftp-s.c-ovn.jp',
                                'port'        : 115,
                                'username'    : 'rtoaster9999',
                                'password'    : 'rtoaster9999',
                                'remote_dir'  : '/receive'
                            },
                    'stg' : {
                                'host'        : 'ma-test.c-ovn.jp',
                                'port'        : 115,
                                'username'    : 'brainpad-ma',
                                'pkey_file'   : 'virtual_partner/ssh_key/stg_microad_id_rsa',
                                'remote_dir'  : 'receive'
                            },
                    'prd' : {
                                'host'        : 'blade.microad.jp',
                                'port'        : 22,
                                'username'    : 'brainpad',
                                'pkey_file'   : 'settings/site/batch/ami/microad_id_rsa',
                                'remote_dir'  : '/data'
                            }
                  }

    def __init__(self, sid, pid, env, s3bucket, s3path):
        TransfererBase.__init__(self, sid, pid, env, 'conversion', s3bucket, s3path)

    @property
    def advertiser_id(self):
        return self.mapping_auth['advertiser_id']
    @property
    def sftp_config(self):
        return self.SFTP_CONFIG[self.env]

    def execute(self):
        self.logger.info('<Start conversion transfer process>')
        target_file, transfer_file = None, None

        try:
            self.logger.info('Downloading s3file')
            target_file, target_filename = self._download_s3file()
            self.result.target_filename  = target_filename

            transfer_file, transfer_filename = self._create_transfer_filename(target_filename)
            self.logger.info('Transfer filename: {0}'.format(transfer_filename))

            self.logger.info('Creating transfer file')
            self._create_transfer_file(target_file, transfer_file)

            self.logger.info('Saving transfer file')
            self._save_transfer_file(transfer_file)

            self.logger.info('Sending transfer file')
            if self._send_transfer_file(transfer_file):
                if self.result.counter.get('line.invalid') > 0:
                    return self.result.api_error('Some input error')
                return self.result.ok('OK')
            else:
                return self.result.ng('Sftp error')

        except NoS3Files, e:
            msg = 'File not exists in s3: {0} {1}'.format(e.s3bucket, e.s3path)
            self.logger.error(msg)
            return self.result.ng(msg)
        except:
            self.logger.exception('Failed to transfer')
            return self.result.ng('Unexpected error occured')
        finally:
            self.logger.info('Removing tmpfile')
            util.remove_files([target_file, transfer_file])

            self.logger.info('<End Conversion transfer process>')

    def _create_transfer_filename(self, target_filename):
        r = re.compile('\\A(\\w+)_(\\d{14})_(\\d{3,4}).json.gz\\Z')
        m = r.match(target_filename)
        if not m:
            raise ValueError('Invalid target filename: ' + target_filename)

        targetfile_datetime = m.groups()[1]
        yyyymmddhh          = targetfile_datetime[:len('yyyymmddhh')]
        transfer_filename   = 'brainpad_conversion_log_{dsp_account_id}.{yyyymmddhh}'.format(
                                  dsp_account_id=self.advertiser_id,
                                  yyyymmddhh=yyyymmddhh
                              )
        return os.path.join(self.tmp_dir, transfer_filename), transfer_filename

    def _create_transfer_file(self, target_file, transfer_file):
        with open(transfer_file, 'w') as outf, \
             rtfile.open_conversion(target_file, self._open_mappingtable()) as reader:
            for row in reader:
                outf.write(self._transform_transfer_format(row))

        self.result.increment_counter('line.rtoaster', reader.line_count_all)
        self.result.increment_counter('line.mapped',   reader.line_count_mapped)
        self.result.increment_counter('line.unmapped', reader.line_count_unmapped)
        self.result.increment_counter('line.invalid',  reader.line_count_invalid)

    def _transform_transfer_format(self, row):
        tsv_format = ['sid', 'unit_id', 'dsp_account_id', 'mapped_uid', 'item_id', 'figure', 'price', 'timestamp']

        row['dsp_account_id'] = self.advertiser_id
        row['item_id']        = util.code_to_long_id(row['code'])
        row['timestamp']      = (datetime.datetime.strptime(row['datetime'],
                                                           '%Y-%m-%d %H:%M:%S')
                                                  .strftime('%Y%m%d%H%M%S'))

        return '\t'.join([str(row[colname]) for colname in tsv_format]) + '\n'

    def _save_transfer_file(self, transfer_file):
        converted_file  = util.gzip_file(transfer_file)
        s3dir_converted = os.path.dirname(self.s3path).replace(
                              '/raw',
                              '/converted/{pid}'.format(pid=self.pid)
                          )
        self.s3util.upload_single_file(converted_file,
                                       self.s3bucket,
                                       s3dir_converted)
        os.remove(converted_file)

    def _send_transfer_file(self, transfer_file):
        try:
            sftputil.put(host=self.sftp_config['host'],
                         port=self.sftp_config['port'],
                         username=self.sftp_config['username'],
                         password=self.sftp_config.get('password'),
                         pkey_file=self.sftp_config.get('pkey_file'),
                         local_file=transfer_file,
                         remote_dir=self.sftp_config['remote_dir'],
                         rename=True)
        except:
            self.logger.exception('Failed to sftp put')
            return False
        else:
            return True
