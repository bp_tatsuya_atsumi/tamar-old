# -*- coding: utf-8 -*-

"""
MicroAd BLADE へアイテムマスタを連携するためのバッチ
"""

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'production'

import csv
import datetime
import gzip
import os
import time
from   conf.config                    import BatchConfig
import lib.sftputil                   as     sftputil
from   lib.transferer.base            import TransfererBase
from   lib.transferer.exception       import NoS3Files
import lib.transferer.rtfile          as     rtfile
import lib.util                       as     util

class ItemMstTransferer(TransfererBase):

    SFTP_CONFIG = {
                    'dev' : {
                                'host'       : 'sftp-s.c-ovn.jp',
                                'port'       : 115,
                                'username'   : 'rtoaster9999',
                                'password'   : 'rtoaster9999',
                                'remote_dir' : '/receive'
                            },
                    'stg' : {
                                'host'       : 'ma-test.c-ovn.jp',
                                'port'       : 115,
                                'username'   : 'brainpad-ma',
                                'pkey_file'  : 'virtual_partner/ssh_key/stg_microad_id_rsa',
                                'remote_dir' : 'receive'
                            },
                    'prd' : {
                                'host'       : 'dex.microad.jp',
                                'port'       : 22,
                                'username'   : 'brainpad',
                                'pkey_file'  : 'settings/site/batch/ami/microad_id_rsa',
                                'remote_dir' : '/data'
                            }
                  }
    LINE_NUM_LIMIT = 10000

    def __init__(self, sid, pid, env, s3bucket, s3path, **option):
        TransfererBase.__init__(self, sid, pid, env, 'itemmst', s3bucket, s3path, **option)
        self.s3_dir_converted = os.path.dirname(self.s3path).replace(
                                    '/raw',
                                    '/converted/{pid}'.format(pid=self.pid)
                                )

        config = BatchConfig(env)
        self.auth    = config.mapping[sid][pid]['auth']

    def create_update_filename(self):
        now = datetime.datetime.strftime(datetime.datetime.now(), '%Y%m%d%H%M%S')
        filename = 'brainpad_product_master_{advertiser_id}.{datetime}.gz'\
                        .format(advertiser_id=self.auth['advertiser_id'], datetime=now)
        return os.path.join(self.tmp_dir, filename)

    def create_delete_filename(self):
        now = datetime.datetime.strftime(datetime.datetime.now(), '%Y%m%d%H%M%S')
        filename = 'brainpad_product_delete_{advertiser_id}.{datetime}.gz'\
                        .format(advertiser_id=self.auth['advertiser_id'], datetime=now)
        return os.path.join(self.tmp_dir, filename)

    def create_transfer_files(self, target_file, transfer_files):

        def _get_writer(func_create_filename):
            file = func_create_filename()
            fp = gzip.open(file, 'wb')
            return file, fp, csv.writer(fp, delimiter='\t', lineterminator='\n')

        def _get_update_writer():
            return _get_writer(self.create_update_filename)

        def _get_delete_writer():
            return _get_writer(self.create_delete_filename)

        line_count_update, line_count_delete = 0, 0

        update_file, update_fp, update_writer = _get_update_writer()
        transfer_files.append(update_file)

        delete_file, delete_fp, delete_writer = _get_delete_writer()
        transfer_files.append(delete_file)

        with rtfile.open_itemmst(target_file) as reader:
            for row in reader:
                if row['status'] == '0':
                    if line_count_update % self.LINE_NUM_LIMIT == 0 and line_count_update > 0:
                        update_fp.close()
                        time.sleep(1)
                        update_file, update_fp, update_writer = _get_update_writer()
                        transfer_files.append(update_file)

                    update_writer.writerow(self.transform_update_format(row))
                    line_count_update += 1
                else:
                    if line_count_delete % self.LINE_NUM_LIMIT == 0 and line_count_delete > 0:
                        delete_fp.close()
                        time.sleep(1)
                        delete_file, delete_fp, delete_writer = _get_delete_writer()
                        transfer_files.append(delete_file)

                    delete_writer.writerow(self.transform_delete_format(row))
                    line_count_delete += 1

        update_fp.close()
        if os.path.getsize(update_file) == 0 and line_count_update > 0:
            os.remove(update_file)
            transfer_files.remove(update_file)

        delete_fp.close()
        if os.path.getsize(delete_file) == 0 and line_count_delete > 0:
            os.remove(delete_file)
            transfer_files.remove(delete_file)

        self.result.increment_counter('item.rtoaster.update', line_count_update)
        self.result.increment_counter('item.rtoaster.delete', line_count_delete)

    def transform_update_format(self, row_in):
        _cut_url = lambda u: u[:1000] if len(u) > 1000 else u

        row_out = []
        row_out.append(self.sid)
        row_out.append(util.code_to_long_id(row_in['code']))
        row_out.append(util.strip_str(row_in['name'], 50))
        row_out.append(_cut_url(row_in['img_url']))
        row_out.append(_cut_url(row_in['link_url']))
        row_out.append(util.strip_str(row_in['price'], 50))
        row_out.append(util.strip_str(row_in['description']))
        row_out.append(self.auth['advertiser_id'])

        return row_out

    def transform_delete_format(self, row_in):
        row_out = []
        row_out.append(self.sid)
        row_out.append(util.code_to_long_id(row_in['code']))
        row_out.append(self.auth['advertiser_id'])
        return row_out

    def send_transformed_files(self, transfer_files):
        try:
            sftp_config = self.SFTP_CONFIG[self.env]
            with sftputil.open(host=sftp_config['host'],
                               port=sftp_config['port'],
                               username=sftp_config['username'],
                               password=sftp_config.get('password'),
                               pkey_file=sftp_config.get('pkey_file')) as sftp:

                remote_dir = sftp_config['remote_dir']
                for transfer_file in transfer_files:
                    sftp.put(transfer_file, remote_dir, rename=False)

                    complete_filename = os.path.basename(transfer_file) + '.completed'
                    remote_complete_file = os.path.join(remote_dir, complete_filename)
                    sftp.touch(remote_complete_file)

        except:
            self.logger.exception('Failed to sftp put')
            return False
        else:
            return True

    def execute(self):
        self.logger.info('<Start itemmst transfer process>')
        target_file, transfer_files = None, []

        try:
            self.logger.info('Downloading latest s3file')
            target_file, target_filename = self._download_s3file()
            self.result.target_filename = target_filename

            self.logger.info('Creating transfer files')
            self.create_transfer_files(target_file, transfer_files)
            self.logger.info('Transfer filename: ' + ','.join([os.path.basename(f) for f in transfer_files]))

            self.logger.info('Sending transfer file')
            send_result = self.send_transformed_files(transfer_files)
            if not send_result:
                return self.result.ng('Sftp error')

            self.logger.info('Saving transfer file')
            for transfer_file in transfer_files:
                self.s3util.upload_single_file(transfer_file,
                                               self.s3bucket,
                                               self.s3_dir_converted)

            return self.result.ok('OK')

        except NoS3Files, e:
            msg = 'File not exists in s3: bucket={}, path={}'.format(e.s3bucket, e.s3path)
            self.logger.error(msg)
            return self.result.ng(msg)
        except:
            self.logger.exception('Failed to transfer')
            return self.result.ng('Unexpected error occured')
        finally:
            self.logger.info('Removing tmpfile')
            util.remove_files([target_file])
            util.remove_files(transfer_files)

            self.logger.info('<End itemmst transfer process>')
