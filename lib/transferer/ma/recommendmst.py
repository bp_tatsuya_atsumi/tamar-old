# -*- coding: utf-8 -*-

"""
MicroAd BLADE へレコメンドマスタ連携するためのバッチ
"""

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'production'

import json
import os
import requests
import traceback
from   lib.transferer.base            import TransfererBase
from   lib.transferer.exception       import NoS3Files
import lib.transferer.rtfile          as     rtfile
from   lib.transferer.transfer_result import TransferConvertedFile
import lib.util                       as     util

class RecommendMstTransferer(TransfererBase):

    ENDPOINT_URL = {
        'dev' : 'http://localhost:5000/recommendlist',
        'stg' : 'http://ma-test.c-ovn.jp/recommendlist',
        'prd' : 'https://blade.microad.jp/blade_connect/recommendlist'
    }

    APP_KEY                       = 'brainpad'
    COLNAME_RECOMMEND_ID          = 'id'
    COLNAME_RECOMMEND_NAME        = 'name'
    COLNAME_RECOMMEND_DESCRIPTION = 'description'

    def __init__(self, sid, pid, env, s3bucket, s3path, **option):
        super(RecommendMstTransferer, self).__init__(
            sid, pid, env, 'recommendmst', s3bucket, s3path, **option
        )

        self.endpoint      = self.ENDPOINT_URL[self.env]
        self.login_id      = self.mapping_auth['login_id']
        self.password      = self.mapping_auth['password']
        self.advertiser_id = self.mapping_auth['advertiser_id']

    def execute(self):
        self.logger.info('<Start recommendmst transfer process>')
        target_file, converted_file = None, None

        try:
            self.logger.info('Downloading s3file')
            target_file, target_filename = self._download_s3file()

            self.logger.info('Target filename: %s', target_filename)
            self.result.target_filename = target_filename

            self.logger.info('Creating request datas')
            request_datas = self._create_request_datas(target_file)

            self.logger.info('Sending request')
            api_errors     = []
            converted_file = TransferConvertedFile.convert_path(target_file)

            with TransferConvertedFile(converted_file) as convf:
                convf.write_header()

                for request_data in request_datas:
                    recommend_num_request = len([k for k in request_data.keys() if k.startswith('rpl_id_')])
                    self.result.increment_counter('recommend.request', recommend_num_request)

                    self.result.increment_counter('request')
                    response = requests.post(self.endpoint, data=request_data)
                    convf.write_post_log(self.endpoint, request_data, response)

                    self.result.increment_counter('request.' + str(response.status_code))
                    if response.status_code != 200:
                        api_errors.append(response.text)
                        self.logger.warn('API Error: response="%s"', response.text)
                    else:
                        response_json = json.loads(response.text)
                        recommend_num_response = response_json.get('rpl_count', 0)
                        self.result.increment_counter('recommend.response', recommend_num_response)

                        if recommend_num_response != recommend_num_request:
                            msg = 'Update count error: request={0}, response={1}'.format(
                                      recommend_num_request, recommend_num_response
                                  )
                            api_errors.append(msg)
                            self.logger.warn(msg)

            self.logger.info('Saving converted file')
            self._save_converted_file(converted_file)

            if api_errors:
                return self.result.api_error(
                           '{:d} API Errors: responses="{}"'.format(
                               len(api_errors), ', '.join(api_errors)
                           )
                       )
            return self.result.ok('OK')

        except NoS3Files, e:
            msg = 'File not exists in s3: bucket={}, path={}'.format(e.s3bucket, e.s3path)
            self.logger.error(msg)
            return self.result.ng(msg)
        except:
            st = traceback.format_exc().splitlines()[-1]
            self.logger.exception('Failed to transfer: laststacktrace="%s"', st)
            return self.result.ng('Unexpected error: laststacktrace="{}"'.format(st))
        finally:
            self.logger.info('Removing tmpfile')
            util.remove_files([target_file, converted_file])

            self.logger.info('<End segmentmst transfer process>')

    def _create_request_datas(self, target_file):
        def _create_request_data(row):
            return {
                'app_key'       : self.APP_KEY,
                'login_id'      : self.login_id,
                'password'      : self.password,
                'advertiser_id' : self.advertiser_id,
                'rpl_id_1'      : row['id'],
                'rpl_name_1'    : util.strip_str(row['name']),
                'rpl_note_1'    : util.strip_str(row['description'])
            }
        with rtfile.open_recommendmst(target_file) as reader:
            return [_create_request_data(row) for row in reader]

    def _save_converted_file(self, converted_file):
        s3dir_converted = os.path.dirname(self.s3path).replace(
                              '/raw',
                              '/converted/{pid}'.format(pid=self.pid)
                          )
        self.s3util.upload_single_file(converted_file, self.s3bucket,
                                       s3dir_converted)
