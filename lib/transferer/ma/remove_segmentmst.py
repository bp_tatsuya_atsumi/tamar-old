# -*- coding: utf-8 -*-

import json
import os
import requests
import traceback
from   lib.transferer.base            import TransfererBase
from   lib.transferer.exception       import NoS3Files
import lib.transferer.rtfile          as     rtfile
from   lib.transferer.transfer_result import TransferConvertedFile
import lib.util                       as     util


class RemoveSegmentMstTransferer(TransfererBase):

    #---------------------------------------------------------------------------
    #   Constants
    #---------------------------------------------------------------------------
    ENDPOINT_URL = {
        'dev' : 'http://localhost:5000/segmentlist/remove_bulk',
        'stg' : 'http://ma-test.c-ovn.jp/segmentlist/remove_bulk',
        'prd' : 'https://blade.microad.jp/blade_connect/segmentlist/remove_bulk'
    }

    APP_KEY = 'brainpad'


    #---------------------------------------------------------------------------
    #   Initialize
    #---------------------------------------------------------------------------
    def __init__(self, sid, pid, env, s3bucket, s3path, **option):
        super(RemoveSegmentMstTransferer, self).__init__(
            sid, pid, env, 'remove_segmentmst', s3bucket, s3path, **option
        )

        self.endpoint      = self.ENDPOINT_URL[self.env]
        self.login_id      = self.mapping_auth['login_id']
        self.password      = self.mapping_auth['password']
        self.advertiser_id = self.mapping_auth['advertiser_id']


    #---------------------------------------------------------------------------
    #   Publics
    #---------------------------------------------------------------------------
    def execute(self):
        self.logger.info('<Start remove_segmentmst transfer process>')
        old_target_file, new_target_file, converted_file = None, None, None

        try:
            self.logger.info('Downloading s3files')
            (old_target_file, old_target_filename,
             new_target_file, new_target_filename) = self._download_s3files()

            self.logger.info('Target filename: %s, %s', old_target_filename, new_target_filename)
            self.result.target_filename = old_target_filename + ', ' + new_target_filename

            self.logger.info('Determining removed segment')
            removed_segment_ids = self._determine_removed_segment_id(
                                      old_target_file, new_target_file
                                  )
            if not removed_segment_ids:
                self.logger.info('No segment removed')
                return self.result.ok('No segment removed')

            self.logger.info('Creating request datas')
            request_datas = self._create_request_datas(removed_segment_ids)

            self.logger.info('Sending request')
            api_errors     = []
            converted_file = TransferConvertedFile.convert_path(new_target_file)

            with TransferConvertedFile(converted_file) as convf:
                convf.write_header()

                for request_data in request_datas:
                    segment_num_request = sum([1 for k in request_data.keys() if k.startswith('seg_id_')])
                    self.result.increment_counter('segment.request', segment_num_request)

                    self.result.increment_counter('request')
                    response = requests.post(self.endpoint, data=request_data)
                    convf.write_post_log(self.endpoint, request_data, response)

                    self.result.increment_counter('request.' + str(response.status_code))
                    if response.status_code != 200:
                        api_errors.append(response.text)
                        self.logger.warn('API Error: response="%s"', response.text)
                    else:
                        response_json = json.loads(response.text)
                        segment_num_response = response_json['segment_count']
                        self.result.increment_counter('segment.response', segment_num_response)

                        if segment_num_response != segment_num_request:
                            msg = 'Update count error: request={:d}, response={:d}'.format(
                                      segment_num_request, segment_num_response
                                  )
                            api_errors.append(msg)
                            self.logger.warn(msg)

            self.logger.info('Saving converted file')
            self._save_converted_file(converted_file)

            if api_errors:
                return self.result.api_error(
                           '{:d} API Errors: responses="{}"'.format(
                               len(api_errors), ', '.join(api_errors)
                           )
                       )
            return self.result.ok('OK')

        except NoS3Files, e:
            msg = 'File not exists in s3: bucket={}, path={}'.format(e.s3bucket, e.s3path)
            self.logger.error(msg)
            return self.result.ng(msg)
        except:
            st = traceback.format_exc().splitlines()[-1]
            self.logger.exception('Failed to transfer: laststacktrace="%s"', st)
            return self.result.ng('Unexpected error: laststacktrace="{}"'.format(st))
        finally:
            self.logger.info('Removing tmpfile')
            util.remove_files([old_target_file, new_target_file, converted_file])

            self.logger.info('<End remove_segmentmst transfer process>')


    #---------------------------------------------------------------------------
    #   Privates
    #---------------------------------------------------------------------------
    def _download_s3files(self):
        s3objs = self.s3util.list_dir(self.s3bucket,
                                      os.path.dirname(self.s3path))
        s3objs = [o for o in s3objs if o['key'] <= self.s3path]
        if not s3objs or len(s3objs) < 2:
            raise NoS3Files(self.s3bucket, self.s3path)

        s3objs.sort(key=lambda o: o['key'], reverse=True)
        if s3objs[0]['key'] != self.s3path:
            raise NoS3Files(self.s3bucket, self.s3path)
        old_s3obj = s3objs[1]
        new_s3obj = s3objs[0]

        self.s3util.download_single_file(self.s3bucket, old_s3obj['key'], self.tmp_dir)
        self.s3util.download_single_file(self.s3bucket, new_s3obj['key'], self.tmp_dir)

        old_target_filename = os.path.basename(old_s3obj['key'])
        new_target_filename = os.path.basename(new_s3obj['key'])
        return (
            os.path.join(self.tmp_dir, old_target_filename),
            old_target_filename,
            os.path.join(self.tmp_dir, new_target_filename),
            new_target_filename
        )

    def _determine_removed_segment_id(self, old_target_file, new_target_file):
        old_segment_ids = set(row['id'] for row in rtfile.open_segmentmst(old_target_file))
        new_segment_ids = set(row['id'] for row in rtfile.open_segmentmst(new_target_file))
        return sorted(list(old_segment_ids - new_segment_ids))

    def _create_request_datas(self, removed_segment_ids):
        request_datas = []
        for i, segment_id in enumerate(removed_segment_ids):
            if i % 100 == 0:
                request_datas.append(self._create_request_data_header())
            request_datas[-1].update({'seg_id_' + str(i % 100 + 1): segment_id})
        return request_datas

    def _create_request_data_header(self):
        return {
            'app_key'       : self.APP_KEY,
            'login_id'      : self.login_id,
            'password'      : self.password,
            'advertiser_id' : self.advertiser_id
        }

    def _save_converted_file(self, converted_file):
        s3dir_converted = os.path.dirname(self.s3path).replace(
                              '/raw',
                              '/converted_remove/{pid}'.format(pid=self.pid)
                          )
        self.s3util.upload_single_file(converted_file, self.s3bucket,
                                       s3dir_converted)
