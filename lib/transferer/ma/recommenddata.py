# -*- coding: utf-8 -*-

"""
MicroAd BLADE へレコメンドデータ連携するためのバッチ
"""

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'production'

import datetime
import os
import time
import traceback
import requests
from   lib.transferer.base            import TransfererBase
from   lib.transferer.exception       import NoS3Files
import lib.transferer.rtfile          as     rtfile
from   lib.transferer.transfer_result import TransferConvertedFile
import lib.util                       as     util

class RecommendDataTransferer(TransfererBase):
    ENDPOINT_URL = {
                        'dev' : 'http://localhost:5000/rpl',
                        'stg' : 'http://ma-test.c-ovn.jp/rpl',
                        'prd' : 'http://bp.link.microad.jp/rpl',
                   }
    API_VERSION = 1
    QPS = 1000

    def __init__(self, sid, pid, env, s3bucket, s3path, **option):
        TransfererBase.__init__(self, sid, pid, env, 'recommenddata', s3bucket, s3path, **option)
        self.ttl      = self.mapping_config['ttl']
        self.endpoint = self.ENDPOINT_URL[self.env]

    def execute(self):
        self.logger.info('<Start recommenddata transfer process>')
        target_file, converted_file = None, None

        try:
            self.logger.info('Downloading latest s3file')
            target_file, target_filename = self._download_s3file()
            self.result.target_filename = target_filename

            self.logger.info('Sending request')
            api_error_occured = False
            converted_file = TransferConvertedFile.convert_path(target_file)
            with TransferConvertedFile(converted_file) as convf, \
                 rtfile.open_recommenddata(target_file, self._open_mappingtable()) as reader:

                convf.write_header()

                for row in reader:

                    time.sleep(1.0 / self.QPS)
                    self.result.increment_counter('request')
                    url = self.endpoint + '?' + self._transform_transfer_format(row)
                    response = requests.get(url)

                    convf.write_get_log(url, None, response)

                    self.result.increment_counter('request.' + str(response.status_code))
                    if response.status_code != 200:
                        api_error_occured = True
                        message = 'Api error: {0}'.format(response.text)
                        self.result.add_message(message)
                        self.logger.warn(message)

            self.result.increment_counter('user.rtoaster', reader.line_count_all)
            self.result.increment_counter('user.mapped',   reader.line_count_mapped)
            self.result.increment_counter('user.unmapped', reader.line_count_unmapped)
            self.result.increment_counter('user.invalid',  reader.line_count_invalid)

            if reader.line_count_invalid > 0:
                api_error_occured = True
                self.logger.warn('invalid line numbers: ' + ','.join([str(n) for n in reader.invalid_line_nums]))
                self.logger.warn('invalid lines\n' + '\n'.join(reader.invalid_lines))

            self.logger.info('Saving transfer file')
            self.s3util.upload_single_file(converted_file,
                                           self.s3bucket,
                                           os.path.dirname(self.s3path).replace('/raw', '/converted/{pid}'.format(pid=self.pid)))

            if api_error_occured:
                return self.result.api_error('Some api error occured')
            else:
                return self.result.ok('OK')

        except NoS3Files, e:
            msg = 'File not exists in s3: {0} {1}'.format(e.s3bucket, e.s3path)
            self.logger.error(msg)
            return self.result.ng(msg)
        except:
            st = traceback.format_exc().splitlines()[-1]
            self.logger.exception('Failed to transfer: laststacktrace="%s"', st)
            return self.result.ng('Unexpected error: laststacktrace={}'.format(st))
        finally:
            self.logger.info('Removing tmpfile')
            util.remove_files([target_file, converted_file])

            self.logger.info('<End recommenddata transfer process>')

    def _get_ttl_unixtimestamp(self):
        ttl_dt = (datetime.datetime.now() + datetime.timedelta(days=self.ttl))
        return long(time.mktime(ttl_dt.timetuple()))

    def _transform_transfer_format(self, row):
        query_string = 'v={version}&u={mapped_uid}&rpl={advertiser_id}-{ttl}:{item_lists}'

        item_lists = []
        for list_id, item_codes in row['item_lists'].items():
            item_ids = [str(util.code_to_long_id(c)) for c in item_codes]
            item_lists.append(list_id + '-' + '.'.join(item_ids))

        return query_string.format(
                    version=self.API_VERSION,
                    mapped_uid=row['mapped_uid'],
                    advertiser_id=self.mapping_auth['advertiser_id'],
                    ttl=self._get_ttl_unixtimestamp(),
                    item_lists=','.join(item_lists)
               )

