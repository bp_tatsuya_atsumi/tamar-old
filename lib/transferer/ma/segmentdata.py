# -*- coding: utf-8 -*-

"""
MicroAd BLADE へセグメント情報を送るためのバッチ
"""

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'production'

import contextlib
import datetime
import time
import traceback
import requests
from   lib.transferer.base            import TransfererBase
from   lib.transferer.exception       import NoS3Files
import lib.transferer.rtfile          as     rtfile
from   lib.transferer.transfer_result import TransferConvertedFile
import lib.util                       as     util

class SegmentDataTransferer(TransfererBase):

    ENDPOINT_URL = {
        'dev' : 'http://localhost:5000/sl',
        'stg' : 'http://ma-test.c-ovn.jp/sl',
        'prd' : 'http://bp.link.microad.jp/sl'
    }
    API_VERSION = 1
    QPS         = 1000

    def __init__(self, sid, pid, env, s3bucket, s3path, **option):
        TransfererBase.__init__(self, sid, pid, env, 'segmentdata', s3bucket,
                                s3path, **option)

        self.endpoint      = self.ENDPOINT_URL[self.env]
        self.advertiser_id = self.mapping_auth['advertiser_id']
        self.ttl           = self.mapping_ttl

    def execute(self):
        self.logger.info('<Start segmentdata transfer process>')
        target_file, converted_file = None, None

        try:
            self.logger.info('Downloading segment data')
            target_file, target_filename = self._download_s3file()
            self.result.target_filename  = target_filename

            self.logger.info('Sending request')
            api_error_num  = 0
            converted_file = TransferConvertedFile.convert_path(target_file)

            with contextlib.nested(
                TransferConvertedFile(converted_file),
                rtfile.open_segmentdata(target_file, self._open_mappingtable())
            ) as (convf, reader):

                convf.write_header()
                for row in reader:
                    url = self._create_request_url(row)
                    if not url:
                        continue

                    time.sleep(1.0 / self.QPS)
                    self.result.increment_counter('request')
                    response = requests.get(url)
                    convf.write_get_log(url, None, response)

                    self.result.increment_counter('request.' + str(response.status_code))
                    if response.status_code != 200:
                        api_error_num += 1
                        self.logger.warn('ApiError: %s', response.text)

                self.result.increment_counter('user.rtoaster', reader.line_count_all)
                self.result.increment_counter('user.mapped',   reader.line_count_mapped)
                self.result.increment_counter('user.unmapped', reader.line_count_unmapped)
                self.result.increment_counter('user.invalid',  reader.line_count_invalid)

            self.logger.info('Saving transfer file')
            self._save_converted_file(converted_file)

            if api_error_num > 0:
                return self.result.api_error('{:d} API Errors'.format(api_error_num))
            return self.result.ok('OK')

        except NoS3Files, e:
            msg = 'File not exists in s3: {0} {1}'.format(e.s3bucket, e.s3path)
            self.logger.error(msg)
            return self.result.ng(msg)
        except:
            st = traceback.format_exc().splitlines()[-1]
            self.logger.exception('Failed to transfer: laststacktrace="%s"', st)
            return self.result.ng('Unexpected error: laststacktrace={}'.format(st))
        finally:
            self.logger.info('Removing tmpfile')
            util.remove_files([target_file, converted_file])

        self.logger.info('<End segmentdata transfer process>')

    def _create_request_url(self, row):
        if not 'attrs' in row:
            return None
        segids = [str(attr) for attr in row['attrs']]
        if not segids:
            return None

        return '{endpoint}?v={version}&u={mapped_uid}&sl={advertiser_id}-{ttl}:{segment_ids}'.format(
                   endpoint=self.endpoint,
                   version=self.API_VERSION,
                   mapped_uid=row['mapped_uid'],
                   advertiser_id=self.advertiser_id,
                   ttl=self._get_ttl_unixtimestamp(),
                   segment_ids=','.join(segids)
               )

    def _get_ttl_unixtimestamp(self):
        ttl_dt = (datetime.datetime.now() + datetime.timedelta(days=self.ttl))
        return long(time.mktime(ttl_dt.timetuple()))
