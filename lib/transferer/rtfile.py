# -*- coding: utf-8 -*-

import csv
import gzip
import json


def open_segmentmst(filename):
    return SegmentMstReader(filename)

def open_recommendmst(filename):
    return RecommendMstReader(filename)

def open_itemmst(filename):
    return ItemMstReader(filename)

def open_segmentdata(filename, mapping_table, check_days_ago=500, oldest_user=True, no_mapping=False):
    return SegmentDataReader(filename, mapping_table, check_days_ago, oldest_user, no_mapping)

def open_mailsegmentdata(filename, mapping_table, check_days_ago=500, oldest_user=True, no_mapping=False):
    return MailSegmentDataReader(filename, mapping_table, check_days_ago, oldest_user, no_mapping)

def open_conversion(filename, mapping_table, check_days_ago=500, oldest_user=True, no_mapping=False):
    return ConversionReader(filename, mapping_table, check_days_ago, oldest_user, no_mapping)

def open_recommenddata(filename, mapping_table, check_days_ago=500, oldest_user=True, no_mapping=False):
    return RecommendDataReader(filename, mapping_table, check_days_ago, oldest_user, no_mapping)

class MstReader(csv.DictReader):

    def __init__(self, filename):
        self._gzipfile = gzip.open(filename, 'rb')
        csv.DictReader.__init__(self, self._gzipfile)

    @property
    def line_count_all(self):
        # all - 1(header)
        return self.line_num - 1

    def close(self):
        self._gzipfile.close()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()

class SegmentMstReader(MstReader):
    """
    Format:
        csv with header
    Layout:
        pid, sid, id, name, description, ignore
    """
    pass

class RecommendMstReader(MstReader):
    """
    Format:
        csv with header
    Layout:
        pid, sid, id, name, description
    """
    pass

class ItemMstReader(MstReader):
    """
    Format:
        csv with header
    Layout:
        pid, sid, code, name, img_url, link_url, price, description, status, last_modified_date
    """
    pass

class DataReader(gzip.GzipFile):

    def __init__(self, filename, mapping_table, check_days_ago=500, oldest_user=True, no_mapping=False):
        gzip.GzipFile.__init__(self, filename, 'rb')
        self._mapping_table  = mapping_table
        self._check_days_ago = check_days_ago
        self._oldest_user    = oldest_user
        self._no_mapping     = no_mapping

        self.line_count_all      = 0
        self.line_count_invalid  = 0
        self.line_count_unmapped = 0
        self.line_count_mapped   = 0
        self.invalid_line_nums   = []
        self.invalid_lines       = []

    def next(self):
        while True:
            line = self.readline()
            if not line:
                raise StopIteration()

            self.line_count_all += 1
            line = line.strip()
            if not line:
                continue

            try:
                row = json.loads(line)
            except ValueError:
                self.line_count_invalid += 1
                self._add_invalid_line(self.line_count_all, line)
                continue

            if not u'uid' in row:
                self.line_count_invalid += 1
                self._add_invalid_line(self.line_count_all, line)
                continue

            if not self._no_mapping:
                if self._oldest_user:
                    mapped_uid = self._get_oldest_mapped_uid(row[u'uid'])
                    if not mapped_uid:
                        self.line_count_unmapped += 1
                        continue
                    self.line_count_mapped += 1
                    row[u'mapped_uid'] = mapped_uid
                else:
                    mapped_uids = self._get_all_mapped_uids(row[u'uid'])
                    if not mapped_uids:
                        self.line_count_unmapped += 1
                        continue
                    self.line_count_mapped += len(mapped_uids)
                    row[u'mapped_uids'] = mapped_uids

            return row

    def _add_invalid_line(self, line_num, line):
        self.invalid_line_nums.append(line_num)
        self.invalid_lines.append(line)

    def _get_oldest_mapped_uid(self, uid):
        return self._mapping_table.select_latest_mapping_partner_uid_by_user_id_until_n_days(uid, self._check_days_ago)

    def _get_all_mapped_uids(self, uid):
        return self._mapping_table.select_partner_uids_by_user_id(uid)

class SegmentDataReader(DataReader):
    """
    Format:
        JSON
    Example:
        {"pid":"rtoaster", "sid":"0001", "uid":"abcde", "attrs": [1,2,3,4,5]}
    """
    pass

class MailSegmentDataReader(DataReader):
    """
    Format:
        JSON
    Example:
        {"pid":"rtoaster", "sid":"0001", "uid":"abcde", "attrs": ["seg1","seg2","seg3","seg4","seg5"]}
    """
    pass

class ConversionReader(DataReader):
    """
    Format:
        JSON
    Example:
        {"pid":"rtoaster","sid":"0001","uid":"abcde","code":"123abc",
         "datetime":"2013-09-15 23:00:00","unit_id":1,"figure":2,"price":1000.0}
    """
    pass

class RecommendDataReader(DataReader):
    """
    Format:
        JSON
    Example:
        {"pid":"rtoaster","sid":"0001", "uid":"abcde","item_list":{"1":[123,456],"2":[789]}
    """
    pass
