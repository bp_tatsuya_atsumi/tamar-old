# -*- coding: utf-8 -*-

"""
外部セグメントのデータをファイル出力するクラス
"""

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'production'

import datetime
import gzip
import json
from   lib.const                          import DATASOURCE
import lib.extsegdatatable                as     extsegdatatable
from   lib.transferer.rt.segmentdata.base import SegmentDataBase
import lib.util                           as     util
import os
import traceback

class SegmentDataTransferer(SegmentDataBase):

    QUERY_SELECT = """
        SELECT t_ext.user_id              AS user_id,
               segment_id                 AS segment_id,
               IF(ttl > {date_now}, 1, 0) AS is_valid
          FROM DB_TAMAR_{sid}.t_user_extsegdata_{sid}_{ds} t_ext
         WHERE ttl > {date_yesterday}
         ORDER BY t_ext.user_id
    """

    QUERY_SELECT_JOIN = """
        SELECT t_mapping.user_id          AS user_id,
               segment_id                 AS segment_id,
               IF(ttl > {date_now}, 1, 0) AS is_valid
          FROM DB_TAMAR_{sid}.t_user_extsegdata_{sid}_{ds} t_ext
         INNER JOIN DB_TAMAR_{sid}.t_user_mapping_{sid}_{ds} t_mapping
            ON t_ext.user_id = t_mapping.partner_user_id
         WHERE ttl > {date_yesterday}
         ORDER BY t_ext.user_id
    """

    def __init__(self, sid, pid, env, s3bucket, s3path, **option):
        SegmentDataBase.__init__(self, sid, pid, env, s3bucket, s3path, **option)
        self.date_now       = datetime.datetime.now()
        self.date_yesterday = (datetime.datetime.now() - datetime.timedelta(days=1)).replace(hour=0, minute=0, second=0)

    def datasource(self):
        return DATASOURCE[self.ds]

    def rta(self):
        return self.config.mapping[self.sid]['rt']['rta']

    def execute(self):
        self.logger.info('<Start segmentdata transfer process>')

        try:
            transfer_file, transfer_filename = self._create_transfer_filename()
            self.logger.info('Transfer filename: {0}'.format(transfer_filename))

            self.logger.info('Creating transfer file')
            self._create_transfer_file(transfer_file)

            self.logger.info('Saving transfer file')
            self._save_converted_file(transfer_file)

            self.logger.info('Sending transfer file')
            if self._send_transfer_file(transfer_file):
                return self.result.ok('OK')
            else:
                return self.result.ng('Sftp error')

        except Exception, e:
            self.logger.exception('Failed to transfer: [%s]', traceback.format_exc().splitlines()[-1])
            raise e
        finally:
            self.logger.info('<End segmentdata transfer process>')
            util.remove_files([transfer_file])

    def _transform_transfer_format(self, uid, attrs, map_segids):
        mapped_attrs = [ map_segids[str(segid)] for segid in attrs if map_segids.get(str(segid)) ]
        return { "uid":uid, "attrs": { self.datasource() : mapped_attrs } }

    def _create_transfer_filename(self):
        transfer_filename = 'extsegdata_{rta}_{yyyymmddhhmm}_{ds}.json.gz'\
               .format(yyyymmddhhmm=self.start_time.strftime('%Y%m%d%H%M'),
                       ds=self.datasource(), rta=self.rta())
        return os.path.join(self.tmp_dir, transfer_filename), transfer_filename

    def _create_transfer_file(self, transfer_file):
        map_segids = self._retrieve_all_segment_mappings(self.ds, self.sid)
        results    = self._retrieve_target_rows()

        with gzip.open(transfer_file, 'wb') as fp:
            uid = attrs = None
            for row in results:
                if uid != row['user_id']:
                    if attrs is not None:
                        fp.write(json.dumps(self._transform_transfer_format(uid, attrs, map_segids), ensure_ascii=False, separators=(',', ':')) + '\n')
                    uid   = row['user_id']
                    attrs = []
                if row['is_valid']:
                    attrs.append(row['segment_id'])
            if attrs is not None:
                fp.write(json.dumps(self._transform_transfer_format(uid, attrs, map_segids), ensure_ascii=False, separators=(',', ':')) + '\n')

    def _save_converted_file(self, transfer_file):
        s3dir_converted = self.s3path.replace(
                              '/raw',
                              '/converted/{pid}'.format(pid=self.pid)
                          )
        self.s3util.upload_single_file(transfer_file, self.s3bucket, s3dir_converted)

    def _open_extsegdatatable(self):
        db_config = self.config.database[self.sid]
        return extsegdatatable.open(self.sid, self.ds, db_config)

    def _retrieve_target_rows(self):
        with self._open_extsegdatatable() as con:
            if self.config.partner_mst[self.ds]['option'].get('external', {}).get('user_id_translation', False):
                query = self.QUERY_SELECT_JOIN
            else:
                query = self.QUERY_SELECT
            result = con.conn.execute(query.format(sid=self.sid,
                                                   ds=self.ds,
                                                   date_now=self.date_now.strftime('%Y%m%d%H%M%S'),
                                                   date_yesterday=self.date_yesterday.strftime('%Y%m%d%H%M%S')))
        return result
