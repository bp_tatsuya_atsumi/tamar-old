# -*- coding: utf-8 -*-

"""
Rtoaster の外部セグメント取り込み用のデータを生成・SFTP転送するバッチの共通クラス
"""

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'production'

from   abc                 import ABCMeta, abstractmethod
from   collections         import OrderedDict
import datetime
import json
import lib.segmentmsttable as     segmentmsttable
import lib.sftputil        as     sftputil
from   lib.transferer.base import TransfererBase


class SegmentDataBase(TransfererBase):

    __metaclass__ = ABCMeta

    SFTP_CONFIG = {
                    'dev' : {
                                'host'       : 'sftp-s.c-ovn.jp',
                                'port'       : 115,
                                'username'   : 'rtoaster9999',
                                'password'   : 'rtoaster9999',
                                'remote_dir' : '/receive/segment_feed/data'
                            },
                    'stg' : {
                                'host'       : 'sftp-s.c-ovn.jp',
                                'port'       : 115,
                                'username'   : 'segment_feed',
                                'pkey_file'  : 'virtual_partner/ssh_key/stg_segment_feed_rsa',
                                'remote_dir' : '/data'
                            },
                    'prd' : {
                                'host'       : 'sftp-1.c-ovn.jp',
                                'port'       : 115,
                                'username'   : 'segment_feed',
                                'pkey_file'  : 'settings/site/batch/ami/segment_feed_rsa',
                                'remote_dir' : '/data'
                            }
                  }

    def __init__(self, sid, pid, env, s3bucket, s3path, **option):
        TransfererBase.__init__(self, sid, pid, env, 'segmentdata', s3bucket, s3path, **option)
        self.ds         = pid[2:]
        self.start_time = datetime.datetime.now()

    @abstractmethod
    def datasource(self): pass

    @abstractmethod
    def rta(self): pass

    def _retrieve_all_segment_mappings(self, key_origin, key_target):
        with segmentmsttable.open(key_origin, 'rt'+key_target, self.db_config) as segmst:
            registered_segment_rowproxies = segmst.select_all_rows()
        return {rsr['segment_id_from']:rsr['segment_id_to'] for rsr in registered_segment_rowproxies}

    def _send_transfer_file(self, transfer_file):
        try:
            sftp_config = self.SFTP_CONFIG[self.env]
            username    = sftp_config['username']
            sftputil.put(host=sftp_config['host'],
                         port=sftp_config['port'],
                         username=username,
                         password=sftp_config.get('password'),
                         pkey_file=sftp_config.get('pkey_file'),
                         local_file=transfer_file,
                         remote_dir=sftp_config['remote_dir'],
                         rename=True)
        except:
            self.logger.exception('Failed to sftp put')
            return False
        else:
            return True
