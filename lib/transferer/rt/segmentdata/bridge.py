# -*- coding: utf-8 -*-

"""
セグメントフィード（TamarBridge）用データ転送バッチ
"""

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'production'

from   collections                        import OrderedDict
import gzip
import json
from   lib.transferer.exception           import NoS3Files
import lib.transferer.rtfile              as     rtfile
from   lib.transferer.rt.segmentdata.base import SegmentDataBase
import lib.util                           as     util
import os
import re
import traceback


class SegmentDataTransferer(SegmentDataBase):

    def __init__(self, sid, pid, env, s3bucket, s3path, **option):
        SegmentDataBase.__init__(self, sid, pid, env, s3bucket, s3path, **option)

    def datasource(self):
        return self.config.mapping[self.sid]['rt']['rta']

    def rta(self):
        return self.config.mapping[self.ds]['rt']['rta']

    def execute(self):
        self.logger.info('<Start segmentdata transfer process>')
        target_file, transfer_file = None, None

        try:
            self.logger.info('Downloading s3file')
            target_file, target_filename = self._download_s3file()
            self.result.target_filename  = target_filename

            transfer_file, transfer_filename = self._create_transfer_filename(target_file)
            self.logger.info('Transfer filename: {0}'.format(transfer_filename))

            self.logger.info('Creating transfer file')
            self._create_transfer_file(target_file, transfer_file)

            self.logger.info('Saving transfer file')
            self._save_converted_file(transfer_file)

            self.logger.info('Sending transfer file')
            if self._send_transfer_file(transfer_file):
                if self.result.counter.get('line.invalid') > 0:
                    return self.result.api_error('Some input error')
                return self.result.ok('OK')
            else:
                return self.result.ng('Sftp error')

        except NoS3Files, e:
            msg = 'File not exists in s3: {0} {1}'.format(e.s3bucket, e.s3path)
            self.logger.error(msg)
            return self.result.ng(msg)

        except:
            st = traceback.format_exc().splitlines()[-1]
            self.logger.exception('Failed to transfer: laststacktrace="%s"', st)
            return self.result.ng('Unexpected error: laststacktrace={}'.format(st))

        finally:
            self.logger.info('Removing tmpfile')
            util.remove_files([target_file, transfer_file])

        self.logger.info('<End segmentdata transfer process>')

    def _create_transfer_filename(self, target_file):
        target_filename = os.path.basename(target_file)
        r = re.compile('\\A(\\w+)_(\\d{14})_(\\d{3,4}).json.gz\\Z')
        m = r.match(target_filename)
        if m:
            transfer_filename = 'extsegdata_{rta}_{yyyymmddhhmm}_{ds}.json.gz'\
                   .format(yyyymmddhhmm=self.start_time.strftime('%Y%m%d%H%M'),
                           ds=self.datasource(), rta=self.rta())
        else:
            raise ValueError('Invalid target filename: ' + target_filename)
        return os.path.join(self.tmp_dir, transfer_filename), transfer_filename

    def _transform_transfer_format(self, row, map_segids):
        recs = []
        data = OrderedDict()
        data['attrs'] = OrderedDict()
        # TBD UID が重複した場合の対策
        for mapped_uid in row.pop('mapped_uids'):
            data['uid'] = mapped_uid.encode('utf-8')
            data['attrs'][self.datasource()] =\
                [ map_segids[str(segid)] for segid in row['attrs'] if map_segids.get(str(segid)) ]
            recs.append(json.dumps(data, ensure_ascii=False, separators=(',', ':')))
            data['attrs'].clear()
        return '\n'.join(recs)

    def _create_transfer_file(self, target_file, transfer_file):
        map_segids = self._retrieve_all_segment_mappings(self.sid, self.ds)
        with gzip.open(transfer_file, 'wb') as outf, \
             rtfile.open_segmentdata(target_file,
                                     self._open_mappingtable(),
                                     oldest_user=False) as reader:
            for row in reader:
                outf.write(self._transform_transfer_format(row, map_segids) + '\n')

        self.result.increment_counter('user.rtoaster', reader.line_count_all)
        self.result.increment_counter('user.mapped',   reader.line_count_mapped)
        self.result.increment_counter('user.unmapped', reader.line_count_unmapped)
        self.result.increment_counter('user.invalid',  reader.line_count_invalid)
