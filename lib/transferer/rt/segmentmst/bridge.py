# -*- coding: utf-8 -*-

"""
Rtoaster のセグメントフィード機能を利用して、外部セグメントのマスタを登録するバッチ
"""

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'production'

import json
import requests
import traceback
from   lib.transferer.base                    import TransfererBase
from   lib.transferer.segmst_classifier_mixin import SegmentMstClassifierMixin
from   lib.transferer.exception               import NoS3Files
import lib.transferer.rtfile                  as     rtfile
import lib.segmentmsttable                    as     segmentmsttable
from   lib.transferer.transfer_result         import TransferConvertedFile
import lib.util                               as     util

class SegmentMstTransferer(TransfererBase, SegmentMstClassifierMixin):

    ENDPOINT_URL = {
        'dev' : u'http://stroustrup.brainpad.co.jp/manager/api/private/co/extseg/segment?ds={ds}&rta={rta}',
        'stg' : u'http://rt-test.c-ovn.jp/extseg/segment?ds={ds}&rta={rta}',
        'prd' : u'https://admin.rtoaster.jp/api/private/co/extseg/segment?ds={ds}&rta={rta}'
    }

    def __init__(self, sid, pid, env, s3bucket, s3path, **option):
        TransfererBase.__init__(self, sid, pid, env, 'segmentmst', s3bucket, s3path, **option)
        self.dest_sid = pid[2:]
        self.dept_config = self.config.mapping[self.sid]['rt']
        self.dest_config = self.config.mapping[self.dest_sid]['rt']
        self.endpoint = self.ENDPOINT_URL[self.env].format(ds=self.dept_config['rta'],
                                                           rta=self.dest_config['rta'])

    def execute(self):
        def _retrieve_all_target_segments():
            return [row for row in rtfile.open_segmentmst(target_file)]

        def _retrieve_all_registered_segments():
            with segmentmsttable.open(self.sid, self.pid, self.db_config) as segmst:
                registered_segment_rowproxies = segmst.select_all_rows()
            return [dict(rsr.items()) for rsr in registered_segment_rowproxies]

        self.logger.info('<Start segmentdata transfer process>')
        target_file, converted_file = None, None

        try:
            self.logger.info('Downloading s3file')
            target_file, target_filename = self._download_s3file()
            self.result.target_filename = target_filename

            converted_file = TransferConvertedFile.convert_path(target_file)

            target_segments     = _retrieve_all_target_segments()
            registered_segments = _retrieve_all_registered_segments()

            modified_segments, new_segments, deleted_segments =\
                self._classify_segments(target_segments, registered_segments)

            api_error_occurred = False
            with TransferConvertedFile(converted_file) as convf:
                convf.write_header()
                if modified_segments:
                    api_error_occurred = self._modify_segments(modified_segments, convf) | api_error_occurred
                if deleted_segments:
                    api_error_occurred = self._delete_segments(deleted_segments, convf) | api_error_occurred
                if new_segments:
                    api_error_occurred = self._add_segments(new_segments, convf) | api_error_occurred

            self.logger.info('Saving transfer file')
            self._save_converted_file(converted_file)

            if api_error_occurred:
                return self.result.api_error('Some api error occured')
            else:
                return self.result.ok('OK')

        except NoS3Files, e:
            msg = 'File not exists in s3: {0} {1}'.format(e.s3bucket, e.s3path)
            self.logger.error(msg)
            return self.result.ng(msg)
        except:
            st = traceback.format_exc().splitlines()[-1]
            self.logger.exception('Failed to transfer: laststacktrace="%s"', st)
            return self.result.ng('Unexpected error: laststacktrace={}'.format(st))
        finally:
            self.logger.info('Removing tmpfile')
            util.remove_files([target_file, converted_file])
            self.logger.info('<End segmentdata transfer process>')

    def _classify_segments(self, target_segments, registered_segments):
        """
        @override SegmentMstClassifierMixin
        引数のセグメントを new, deleted, modified の 3つのステータスに分類する
        SegmentMstClassifierMixin の _classify_segments を呼び、カウンタへ登録する
        """
        modified_segments, new_segments, deleted_segments =\
            SegmentMstClassifierMixin._classify_segments(self, target_segments, registered_segments)
        self.result.increment_counter('segment.modified', len(modified_segments))
        self.result.increment_counter('segment.new',      len(new_segments))
        self.result.increment_counter('segment.deleted',  len(deleted_segments))
        return modified_segments, new_segments, deleted_segments

    def _send_request(self, method_type, endpoint, payload, convf):
        try:
            method = getattr(requests, method_type)
        except AttributeError:
            self.logger.exception('Unknown method_type was given')
            raise Exception('Unknown method_type was given')

        headers = {'content-type': 'application/json'}
        self.result.increment_counter('request')
        self.result.increment_counter('request.{method_type}'.format(method_type=method_type))
        response = method(endpoint, data=json.dumps(payload), headers=headers)
        self.result.increment_counter('request.{method_type}.{status_code}'\
                                      .format(method_type=method_type,status_code=str(response.status_code)))
        convf.write_post_log(endpoint, payload, response)
        return response

    def _modify_segments(self, segments, convf):
        """
        @override SegmentMstClassifierMixin
        セグメント名、備考の更新を行う。
        """
        def _make_payload(_segment):
            _name        = _segment['name_modified']        if _segment.has_key('name_modified')        else _segment['name']
            _description = _segment['description_modified'] if _segment.has_key('description_modified') else _segment['description']
            return {
                "id"  : _segment['segment_id_to'],
                'name': _name,
                'memo': _description
            }

        def _update_database(_segment, _segmst):
            payload = {
                'name'       : _segment['name_modified'],
                'description': _segment['description_modified']
            }
            num = _segmst.update_row_by_segment_id(_segment['segment_id_from'], payload)
            if num == 1:
                self.logger.info('Segment \'{name}\'({segid}) is updated.'\
                                 .format(name=_segment['name'].encode('utf-8'),
                                         segid=_segment['segment_id_from']))
            else:
                message = 'DB error: Failed to update segment {segid}'.format(segid=_segment['segment_id_from'])
                self.result.add_message(message)
                self.logger.error(message)

        # main
        api_error_occurred = False
        self.logger.info('Sending renaming request to modified segments')

        with segmentmsttable.open(self.sid, self.pid, self.db_config) as segmst:
            for segment in segments:
                payload  = _make_payload(segment)
                response = self._send_request("put", self.endpoint, payload, convf)
                if response.status_code == 200:
                    segment['name_modified']        = payload['name']
                    segment['description_modified'] = payload['memo']
                    _update_database(segment, segmst)
                else:
                    api_error_occurred = True
                    message = 'Api error: {0}'.format(response.text)
                    self.result.add_message(message)
                    self.logger.error(message)
        return api_error_occurred

    def _delete_segments(self, segments, convf):
        """
        @override SegmentMstClassifierMixin
        引数に与えられたセグメントをデータベースから削除する。
        """
        def _make_payload(_segment):
            return {
                "id"    : _segment['segment_id_to'],
                "action": "STOP"
            }

        def _delete_from_database(_segment, _segmst):
            num = _segmst.delete_rows_by_segment_id(_segment['segment_id_from'])
            if num == 1:
                self.logger.info('Segment \'{name}\'({segid}) is deleted.'\
                                 .format(name=_segment['name'].encode('utf-8'),
                                         segid=_segment['segment_id_from']))
            else:
                message = 'DB error: Failed to delete segment {segid}'.format(segid=_segment['segment_id_from'])
                self.result.add_message(message)
                self.logger.error(message)

        # main
        api_error_occurred = False
        self.logger.info('Sending renaming request to deleted segments')

        with segmentmsttable.open(self.sid, self.pid, self.db_config) as segmst:
            for segment in segments:
                payload  = _make_payload(segment)
                response = self._send_request("put", self.endpoint, payload, convf)
                if response.status_code == 200:
                    _delete_from_database(segment, segmst)
                else:
                    api_error_occurred = True
                    message = 'Api error: {0}'.format(response.text)
                    self.result.add_message(message)
                    self.logger.error(message)
        return api_error_occurred

    def _add_segments(self, segments, convf):
        """
        @override SegmentMstClassifierMixin
        引数に与えられたセグメントを追加する。
        登録に成功するとセグメント ID を返却するため、これを DB に保存する。
        """
        def _make_payload(_segment):
            return {
                "name": _segment['name'],
                "memo": _segment['description']
            }

        def _is_over_limit():
            """
            現在登録されている数 + 登録すべき数が登録上限数を超えていないかを確認する
            """
            response = self._send_request("get", self.endpoint, None, convf)
            if response.status_code == 200:
                response_json = json.loads(response.text)
                return len(segments) + response_json['count'] > response_json['limit']
            else:
                self.logger.exception('Failed to get external segment information from Rtoaster')
                raise requests.HTTPError()

        self.logger.info('Checking the limit to register new segments')
        if _is_over_limit():
            message = 'Cannot register new segments:OVER LIMIT'
            self.result.add_message(message)
            self.logger.exception(message)
            return True

        self.logger.info('Sending new segment request')
        api_error_occurred = False
        for segment in segments:
            payload  = _make_payload(segment)
            response = self._send_request("post", self.endpoint, payload, convf)
            if response.status_code == 200:
                segment['segment_id_to'] = str(json.loads(response.text)['id'])
                with segmentmsttable.open(self.sid, self.pid, self.db_config) as segmst:
                    segmst.insert_row(segment['id'], segment['segment_id_to'], segment['name'], segment['description'])
            else:
                api_error_occurred = True
                message = 'Api error: {0}'.format(response.text)
                self.result.add_message(message)
                self.logger.warn(message)

        return api_error_occurred
