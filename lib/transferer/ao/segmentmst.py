# -*- coding: utf-8 -*-

"""
AudienceOne へセグメントマスタ連携するためのバッチ
"""

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'production'

import datetime
import csv
import gzip
import os
import re
from   conf.config                    import BatchConfig
import lib.sftputil                   as     sftputil
from   lib.transferer.base            import TransfererBase
from   lib.transferer.exception       import NoS3Files
from   lib.transferer.transfer_result import TransferResult
import lib.util                       as     util

class SegmentMstTransferer(TransfererBase):

    COLNAME_SEGMENT_ID   = 'id'
    COLNAME_SEGMENT_NAME = 'name'

    SFTP_CONFIG = {
                    'dev' : {
                                'host'       : 'sftp-s.c-ovn.jp',
                                'port'       : 115,
                                'username'   : 'rtoaster9999',
                                'password'   : 'rtoaster9999',
                                'remote_dir' : '/receive/{YYYYMMDD}'
                            },
                    'stg' : {
                                'host'       : 'ao-test.c-ovn.jp',
                                'port'       : 115,
                                'username'   : 'brainpad-ao',
                                'pkey_file'  : 'virtual_partner/ssh_key/stg_ao_id_rsa',
                                'remote_dir' : 'receive/{YYYYMMDD}'
                            },
                    'prd' : {
                                'host'       : 'storage.audienceone.jp',
                                'port'       : 22,
                                'username'   : 'brainpad',
                                'pkey_file'  : 'settings/site/batch/ami/audienceone_id_rsa',
                                'remote_dir' : 'link_data/{YYYYMMDD}'
                            },
                  }

    def __init__(self, sid, pid, env, s3bucket, s3path, **option):
        TransfererBase.__init__(self, sid, pid, env, 'segmentmst', s3bucket, s3path, **option)

        self.config      = BatchConfig(env)
        self.sftp_config = self.SFTP_CONFIG[self.env]
        self.start_time  = datetime.datetime.now()
        self.remote_dir  = self.sftp_config['remote_dir'].format(
                               YYYYMMDD=self.start_time.strftime('%Y-%m-%d')
                           )
        self.client_id   = self.mapping_auth['client_id']
        self.client_name = self.mapping_auth['client_name']

        self.transfer_result = TransferResult(sid, pid, env, 'segmentmst')

    def execute(self):
        self.logger.info('<Start segmentmst transfer process>')
        target_file, transfer_file = None, None

        try:
            self.logger.info('Downloading s3file')
            target_file, target_filename = self._download_latest_s3file(self.s3path)
            self.transfer_result.target_filename = target_filename

            transfer_filename = self._create_transfer_filename(target_file)
            transfer_file     = os.path.join(self.tmp_dir, transfer_filename)
            self.logger.info('Transfer filename: {0}'.format(transfer_filename))

            self.logger.info('Creating transfer file')
            api_error_occured = False
            with open(transfer_file, 'w') as transfer_fp:
                for json_obj in self._get_target_fp(target_file):
                    transformed_data = self._transform_transfer_format(json_obj)
                    transfer_fp.write(transformed_data)

                    self.transfer_result.increment_counter('line.rtoaster')

            self.logger.info('Sending transfer file')
            send_result = self._send_transformed_file(transfer_file)

            self.logger.info('Saving transfer file')
            self.s3util.upload_single_file(transfer_file,
                                           self.s3bucket,
                                           self.s3path.replace('/raw', '/converted/{pid}'.format(pid=self.pid)))

            if send_result:
                if api_error_occured:
                    return self.transfer_result.api_error('Some input error')
                return self.transfer_result.ok('OK')
            else:
                return self.transfer_result.ng('SFTP error')
        except NoS3Files, e:
            msg = 'File not exists in s3: {0} {1}'.format(e.s3bucket, e.s3path)
            self.logger.error(msg)
            return self.result.ng(msg)
        except:
            self.logger.exception('Failed to transfer')
            return self.transfer_result.ng('Unexpected error occured')

        finally:
            self.logger.info('Removing tmpfile')
            util.remove_files([target_file, transfer_file])

            self.logger.info('<End Segmentmst transfer process>')

    def _get_target_fp(self, path):
        return csv.DictReader(gzip.open(path, 'rb'))

    def _transform_transfer_format(self, json_obj):
        return '\t'.join(['1',
                          str(json_obj[self.COLNAME_SEGMENT_ID]),
                          str(json_obj[self.COLNAME_SEGMENT_NAME])]) + '\n'

    def _get_current_time_format(self):
        start_time = self.start_time.strftime('%Y%m%d%H%M%S')
        necessary_create_datetime = 'yyyymmddhhmmss'
        return start_time[:len(necessary_create_datetime)]

    def _create_transfer_filename(self, target_file):
        target_filename = os.path.basename(target_file)
        r = re.compile('\\A(\\w+)_(\\d{14})_(\\d{3,4}).csv.gz\\Z')
        m = r.match(target_filename)
        if m:
            return 'brainpad_{client_id}_{sid}_master_{yyyymmddhhmmss}.tsv'\
                   .format(yyyymmddhhmmss=self._get_current_time_format(),
                           client_id=self.client_id,
                           sid=self.sid)
        else:
            raise ValueError('Invalid target filename: ' + target_filename)

    def _send_transformed_file(self, local_file):
        try:
            sftp_config = self.SFTP_CONFIG[self.env]
            if 'password' in sftp_config:
                sftputil.put(host=sftp_config['host'],
                             port=sftp_config['port'],
                             username=sftp_config['username'],
                             password=sftp_config['password'],
                             local_file=local_file,
                             remote_dir=self.remote_dir,
                             rename=True)
            elif 'pkey_file' in sftp_config:
                sftputil.put(host=sftp_config['host'],
                             port=sftp_config['port'],
                             username=sftp_config['username'],
                             pkey_file=sftp_config['pkey_file'],
                             local_file=local_file,
                             remote_dir=self.remote_dir,
                             rename=True)
            else:
                raise Exception('SFTP config error')

            return True

        except:
            self.logger.exception('Failed to sftp put')
            return False
