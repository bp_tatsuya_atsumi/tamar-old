# -*- coding: utf-8 -*-

"""
AudienceOne へセグメントデータ連携するためのバッチ
"""

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'production'

import contextlib
import datetime
import bz2
import os
import re

from   lib.lockfile                   import LastUpdateFile
import lib.sftputil                   as     sftputil
from   lib.transferer.base            import TransfererBase
from   lib.transferer.exception       import NoS3Files
import lib.transferer.rtfile          as     rtfile
import lib.util                       as     util

class SegmentDataTransferer(TransfererBase):

    COLNAME_SEGMENT_UID   = 'uid'
    COLNAME_SEGMENT_ATTRS = 'attrs'

    SFTP_CONFIG = {
                    'dev' : {
                                'host'       : 'sftp-s.c-ovn.jp',
                                'port'       : 115,
                                'username'   : 'rtoaster9999',
                                'password'   : 'rtoaster9999',
                                'remote_dir' : '/receive/{YYYYMMDD}'
                            },
                    'stg' : {
                                'host'       : 'ao-test.c-ovn.jp',
                                'port'       : 115,
                                'username'   : 'brainpad-ao',
                                'pkey_file'  : 'virtual_partner/ssh_key/stg_ao_id_rsa',
                                'remote_dir' : 'receive/{YYYYMMDD}'
                            },
                    'prd' : {
                                'host'       : 'storage.audienceone.jp',
                                'port'       : 22,
                                'username'   : 'brainpad',
                                'pkey_file'  : 'settings/site/batch/ami/audienceone_id_rsa',
                                'remote_dir' : 'link_data/{YYYYMMDD}'
                            },
                  }

    def __init__(self, sid, pid, env, s3bucket, s3path, **option):
        TransfererBase.__init__(self, sid, pid, env, 'segmentdata', s3bucket, s3path, **option)
        self.sftp_config = self.SFTP_CONFIG[self.env]
        self.start_time  = datetime.datetime.now()

        self.client_id   = self.config.mapping[sid][pid]['auth']['client_id']
        self.client_name = self.config.mapping[sid][pid]['auth']['client_name']

        lastupdate_key       = 'segmentdata_{sid}_ao'.format(sid=self.sid)
        self.lastupdate_file = LastUpdateFile(lastupdate_key, env)

    def execute(self):
        self.logger.info('<Start segmentdata transfer process>')
        target_file, transfer_file = None, None

        try:
            self.logger.info('Downloading s3file')
            target_file, target_filename = self._download_s3file()
            self.result.target_filename = target_filename

            if self._has_transfered_today(target_filename):
                message = 'Already transfered today: {0}'.format(target_filename)
                self.logger.info(message)
                return self.result.ng(message)

            self.logger.info('Checking lastupdate')
            if self.lastupdate_file.is_latest(target_filename):
                message = 'Already transfered: {0}'.format(target_filename)
                self.logger.info(message)

                self.logger.info('Sending end file')
                send_endfile_result = self._send_endfile()
                return self.result.ok(message)

            transfer_filename = self._create_transfer_filename(target_file)
            transfer_file     = os.path.join(self.tmp_dir, transfer_filename)
            self.logger.info('Transfer filename: {0}'.format(transfer_filename))

            self.logger.info('Creating transfer file')
            json_load_error_occured = False

            with contextlib.nested(
                bz2.BZ2File(transfer_file, 'w'),
                rtfile.open_segmentdata(target_file, self._open_mappingtable())
            ) as (transfer_fp, reader):
                for row in reader:
                    transformed_data = self._transform_transfer_format(row)
                    transfer_fp.write(transformed_data)

                self.result.increment_counter('user.rtoaster', reader.line_count_all)
                self.result.increment_counter('user.mapped',   reader.line_count_mapped)
                self.result.increment_counter('user.unmapped', reader.line_count_unmapped)
                self.result.increment_counter('user.invalid',  reader.line_count_invalid)

            self.logger.info('Sending transfer file')
            send_datafile_result = self._send_transformed_file(transfer_file)

            self.logger.info('Sending end file')
            send_endfile_result = self._send_endfile()

            self.logger.info('Saving transfer file')
            self._save_converted_file(transfer_file)

            if send_datafile_result and send_endfile_result:
                if json_load_error_occured:
                    return self.result.api_error('Some input error')
            else:
                return self.result.ng('SFTP error')

            return self.result.ok('OK')

        except NoS3Files, e:
            msg = 'File not exists in s3: {0} {1}'.format(e.s3bucket, e.s3path)
            self.logger.error(msg)

            self.logger.info('Sending end file')
            self._send_endfile()
            return self.result.ng(msg)
        except:
            self.logger.exception('Failed to transfer')
            return self.result.ng('Unexpected error occured')

        finally:
            self.logger.info('Removing tmpfile')
            util.remove_files([target_file, transfer_file])

            self.logger.info('<End Segmentdata transfer process>')

    def _calculate_remote_dir(self):
        remote_dir_time = datetime.datetime.now()
        if remote_dir_time.hour < 4:
            remote_dir_time -= datetime.timedelta(days=1)
        return self.sftp_config['remote_dir'].format(YYYYMMDD=remote_dir_time.strftime('%Y-%m-%d'))

    def _transform_transfer_format(self, json_obj):
        segment_ids = json_obj[self.COLNAME_SEGMENT_ATTRS]
        transformed_data = ''
        for segment_id in segment_ids:
            transformed_data += '\t'.join([json_obj['mapped_uid'].encode('utf-8'),
                                           str(segment_id)]) + '\n'
        return transformed_data

    def _get_current_time_format(self):
        start_time = self.start_time.strftime('%Y%m%d%H%M%S')
        necessary_create_datetime = 'yyyymmddhhmmss'
        return start_time[:len(necessary_create_datetime)]

    def _create_transfer_filename(self, target_file):
        target_filename = os.path.basename(target_file)
        r = re.compile('\\A(\\w+)_(\\d{14})_(\\d{3,4}).json.gz\\Z')
        m = r.match(target_filename)
        if m:
            return 'brainpad_{client_id}_{sid}_member_{yyyymmddhhmmss}.tsv.bz2'\
                   .format(yyyymmddhhmmss=self._get_current_time_format(),
                           client_id=self.client_id,
                           sid=self.sid)
        else:
            raise ValueError('Invalid target filename: ' + target_filename)

    def _create_end_filename(self):
        return 'brainpad_{client_id}_{sid}_endfile'.format(client_id=self.client_id, sid=self.sid)

    def _send_transformed_file(self, local_file):
        remote_dir = self._calculate_remote_dir()
        try:
            sftp_config = self.SFTP_CONFIG[self.env]
            if 'password' in sftp_config:
                sftputil.put(host=sftp_config['host'],
                             port=sftp_config['port'],
                             username=sftp_config['username'],
                             password=sftp_config['password'],
                             local_file=local_file,
                             remote_dir=remote_dir,
                             rename=True)
            elif 'pkey_file' in sftp_config:
                sftputil.put(host=sftp_config['host'],
                             port=sftp_config['port'],
                             username=sftp_config['username'],
                             pkey_file=sftp_config['pkey_file'],
                             local_file=local_file,
                             remote_dir=remote_dir,
                             rename=True)
            else:
                raise Exception('SFTP config error')

            return True

        except:
            self.logger.exception('Failed to sftp put')
            return False

    def _send_endfile(self):
            end_filename = self._create_end_filename()
            end_file     = os.path.join(self.tmp_dir, end_filename)
            with open(end_file, 'w'):
                os.utime(end_file, None)
            send_result = self._send_transformed_file(end_file)
            return send_result

    def _has_transfered_today(self, target_filename):
        updated_filename = self.lastupdate_file.get_last_update_filename()
        if not updated_filename:
            return False

        r = re.compile('\\A(\\w+)_(\\d{8})(\\d{6})_(\\d{3,4}).json.gz\\Z')
        m_target = r.match(target_filename)
        if m_target:
            target_date = datetime.datetime.strptime(m_target.groups()[1], '%Y%m%d')
        else:
            raise ValueError('Invalid target filename: ' + target_filename)

        m_update = r.match(updated_filename)
        if m_update:
            update_date = datetime.datetime.strptime(m_update.groups()[1], '%Y%m%d')
        else:
            raise ValueError('Invalid target filename: ' + target_filename)

        if target_date <= update_date:
            return True
        else:
            return False
