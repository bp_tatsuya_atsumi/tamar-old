# -*- coding: utf-8 -*-
import base64
from Crypto.Cipher import AES
from Crypto import Random

class AesCrypto:
    DEFAULT_KEY = 'Q!w233BwL/w|dTQ2'
    BLOCK_SIZE  = AES.block_size

    def __init__(self, key=DEFAULT_KEY):
        self.key = key

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return False
        return self.key == other.key

    def _pkcs5_pad(self, s):
        return s + (self.BLOCK_SIZE - len(s) % self.BLOCK_SIZE) * chr(self.BLOCK_SIZE - len(s) % self.BLOCK_SIZE)

    def _pkcs5_upad(self, s):
        return s[0:-ord(s[-1])]

    def encrypt(self, plaintext):
        iv = Random.new().read(self.BLOCK_SIZE)
        crypto = AES.new(self.key, mode=AES.MODE_CBC, IV=iv)
        b = crypto.encrypt(self._pkcs5_pad(plaintext))
        return base64.urlsafe_b64encode(iv + b)

    def decrypt(self, ciphertext):
        b = base64.urlsafe_b64decode(ciphertext)
        iv = b[:self.BLOCK_SIZE]
        crypto = AES.new(self.key, mode=AES.MODE_CBC, IV=iv)
        return self._pkcs5_upad(crypto.decrypt(b[self.BLOCK_SIZE:]))
