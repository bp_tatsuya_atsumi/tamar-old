# -*- coding: utf-8 -*-

"""
api.scaleout.jpにAPIでアクセスするためのモジュール
"""

import json
from   lib.partner_api.so.api_token import ApiToken
import os
import requests
import time
import urllib

class ApiConnector(object):

    def __init__(self, env):
        self.api_token      = ApiToken(env)
        self.token_endpoint = self.api_token.token_endpoint
        self.headers        = self.api_token.get_api_headers()

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        pass

    def _get_child_ids(self, child_name, parent_name, parent_ids, filtering_id_name=None, filtering_ids=None):
        child_ids    = []
        include_name = urllib.quote('["{child_name}"]'.format(child_name=child_name))

        for parent_id in parent_ids:
            url = os.path.join(
                self.token_endpoint,
                '{parent_name}/{parent_id}.json?includes={include_name}'.format(
                    parent_name  = parent_name,
                    parent_id    = parent_id,
                    include_name = include_name
                    )
                )
            r = requests.get(url, headers=self.headers)
            time.sleep(1)
            if r.status_code != 200:
                raise requests.exceptions.HTTPError()
            child_list = json.loads(r.text)[child_name]
            if child_list:
                if filtering_id_name is None:
                    for child in child_list:
                        child_id = str(child['id'])
                        child_ids.append(child_id)
                else:
                    for child in child_list:
                        if str(child[filtering_id_name]) in filtering_ids:
                            child_id = str(child['id'])
                            child_ids.append(child_id)

        return child_ids

    def get_order_ids_from_advertiser_ids(self, advertiser_ids):
        return self._get_child_ids('orders', 'advertisers', advertiser_ids)

    def get_cv_pixel_ids_from_advertiser_ids(self, advertiser_ids):
        return self._get_child_ids('conversion_pixels', 'advertisers', advertiser_ids)

    def get_schedule_ids_from_advertiser_ids(self, advertiser_ids, order_ids):
        return self._get_child_ids('schedules', 'advertisers', advertiser_ids, 'order_id', order_ids)
