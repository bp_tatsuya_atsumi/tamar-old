# -*- coding: utf-8 -*-

"""
api.scaleout.jpにアクセスするためのトークンを取得
"""
from   conf.config import BatchConfig
import copy
import json
import os
import requests


class ApiToken(object):

    TOKEN_HEADERS = {'content-type': 'application/json'}
    PID           = 'so'
    TOKEN         = None

    def __init__(self, env):
        self.config         = BatchConfig(env)
        self.partner_config = self.config.partner_mst[self.PID]
        self.partner_option = self.partner_config['option']
        self.token_endpoint = self.partner_option['api_config']['url']
        self.username       = self.partner_option['api_config']['username']
        self.password       = self.partner_option['api_config']['password']
        self.payload        = {'login': self.username, 'password': self.password}
        self.token_url      = os.path.join(self.token_endpoint, 'token.json')

        self._set_token()
        self._set_api_headers()

    def __del__(self):
        self.__delete_token()

    def _set_token(self):
        r = requests.post(self.token_url, data=json.dumps(self.payload), headers=self.TOKEN_HEADERS)
        if r.status_code != 200:
            raise requests.exceptions.HTTPError
        self.TOKEN = json.loads(r.text)['api_token']

    def get_token(self):
        return self.TOKEN

    def _set_api_headers(self):
        headers = copy.deepcopy(self.TOKEN_HEADERS)
        token = self.get_token()
        headers['Authorization'] = 'login={username};api_token={api_token}'.format(username=self.username, api_token=token)
        self.API_HEADERS = headers

    def get_api_headers(self):
        return self.API_HEADERS

    def __delete_token(self):
        requests.delete(self.token_url, data=json.dumps(self.payload), headers=self.API_HEADERS)
