# -*- coding: utf-8 -*-

import json
import requests

class RtoasterExtAPI(object):

    ENDPOINT_URLS = {
        'dev' : u'http://stroustrup.brainpad.co.jp/manager/api/private/co/extseg/segment?ds={ds}&rta={rta}',
        'stg' : u'http://124.32.238.250:10080/manager/api/private/co/extseg/segment?ds={ds}&rta={rta}',
        'prd' : u'https://admin.rtoaster.jp/api/private/co/extseg/segment?ds={ds}&rta={rta}'
    }

    def __init__(self, env):
        self.env = env
        self.endpoint_url = self.ENDPOINT_URLS[env]

    def get(self, pid, rta, ds, payload):
        return self._send_request(pid, rta, ds, 'get', payload)

    def post(self, pid, rta, ds, payload):
        return self._send_request(pid, rta, ds, 'post', payload)

    def put(self, pid, rta, ds, payload):
        return self._send_request(pid, rta, ds, 'put', payload)

    def delete(self, pid, rta, ds, payload):
        return self._send_request(pid, rta, ds, 'delete', payload)

    def _send_request(self, pid, rta, ds, method_type, payload):
        method   = getattr(requests, method_type)
        headers  = {'content-type': 'application/json'}
        url      = self.endpoint_url.format(ds=ds, rta=rta)
        response = method(url, data=json.dumps(payload), headers=headers)
        return response

    def is_over_limit(self, pid, rta, ds):
        response = self._send_request(pid, rta, ds, 'get', None)
        if response.status_code == 200:
            response_json = json.loads(response.text)
            return response_json['count'] >= response_json['limit']
        else:
            raise requests.HTTPError()
