# Rtoaster 外部セグメント用 API

セグメントの登録・更新・連携停止に利用する。

データソース自体の削除を行う場合、下記コマンドを発行する：

    curl -H "Accept: application/json" -H "Content-type: application/json" -X PUT "http://stroustrup.brainpad.co.jp/manager/api/private/co/extseg/datasource?rta={rta}&ds={datasource}" -d '{"action":"STOP"}'
