# -*- coding: utf-8 -*-

__author__ = 'CrossOven開発チーム<coven-dev@brainpad.co.jp>'

import os
import sys
import urllib
import urllib2
from xml.etree import ElementTree

import suds.client
import suds.mx.literal
import suds.xsd.doctor

import lib.partner_api.go.googleads.common
import lib.partner_api.go.googleads.errors

_CHUNK_SIZE = 16 * 1024

_SERVICE_MAP = {
    'v201402': {
        'DmpUserListService': 'dmp',
    },
}

class DdpClient(object):
    _YAML_KEY = 'ddp'
    _REQUIRED_INIT_VALUES = ('user_agent', 'client_customer_id')
    _OPTIONAL_INIT_VALUES = ('validate_only', 'partial_failure', 'https_proxy')
    _SOAP_SERVICE_FORMAT = '%s/api/ddp/%s/%s/%s?wsdl'

    @classmethod
    def LoadFromStorage(
            cls, path='lib/partner_api/go/googleads.yaml'):
        return cls(**lib.partner_api.go.googleads.common.LoadFromStorage(
            path, cls._YAML_KEY, cls._REQUIRED_INIT_VALUES,
            cls._OPTIONAL_INIT_VALUES))

    def __init__(
            self, oauth2_client, user_agent,
            client_customer_id, validate_only=False, partial_failure=False,
            https_proxy=None):
        self.oauth2_client = oauth2_client
        self.user_agent = user_agent
        self.client_customer_id = client_customer_id
        self.validate_only = validate_only
        self.partial_failure = partial_failure
        self.https_proxy = https_proxy

    def GetService(self, service_name, version=sorted(_SERVICE_MAP.keys())[-1],
                   server='https://ddp.googleapis.com'):
        if server[-1] == '/': server = server[:-1]
        try:
            proxy_option = None
            if self.https_proxy:
                proxy_option = {
                    'https:': self.https_proxy
                }

            client = suds.client.Client(
                self._SOAP_SERVICE_FORMAT %
                (server, _SERVICE_MAP[version][service_name], version, service_name),
                proxy=proxy_option)
        except KeyError:
            if version in _SERVICE_MAP:
                raise lib.partner_api.go.googleads.errors.GoogleAdsValueError(
                    'Unrecognized service for the DDP API. Service given: %s '
                    'Supported services: %s'
                    % (service_name, _SERVICE_MAP[version].keys()))
            else:
                raise lib.partner_api.go.googleads.errors.GoogleAdsValueError(
                    'Unrecognized version of the DDP API. Version given: %s '
                    'Supported versions: %s' % (version, _SERVICE_MAP.keys()))

        return lib.partner_api.go.googleads.common.SudsServiceProxy(
            client, _DdpHeaderHandler(self, version))

class _DdpHeaderHandler(lib.partner_api.go.googleads.common.HeaderHandler):
    _LIB_SIG = lib.partner_api.go.googleads.common.GenerateLibSig('DdpApi-Python')
    _SOAP_HEADER_CLASS = ('{https://ddp.googleapis.com/api/ddp/dmp/%s}'
                          'SoapHeader')
    _CONTENT_TYPE = 'application/x-www-form-urlencoded'

    def __init__(self, ddp_client, version):
        self._ddp_client = ddp_client
        self._version = version

    def SetHeaders(self, suds_client):
        header = suds_client.factory.create(self._SOAP_HEADER_CLASS % self._version)
        header.userAgent = ''.join([self._ddp_client.user_agent, self._LIB_SIG])
        header.clientCustomerId = self._ddp_client.client_customer_id
        header.validateOnly = self._ddp_client.validate_only
        header.partialFailure = self._ddp_client.partial_failure

        suds_client.set_options(
                soapheaders=header,
                headers=self._ddp_client.oauth2_client.CreateHttpHeader())
