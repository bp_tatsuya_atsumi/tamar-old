# -*- coding: utf-8 -*-

import argparse
import os
import MySQLdb
from conf.config import BatchConfig

TARGET_TABLE = 't_user_extsegdata_{sid}_{pid}'
TEMP_TABLE   = 'temp_user_extsegdata'
WORK_TABLE   = 'w_user_extsegdata_{sid}_{pid}'

SQL_CREATE_TEMPORARY_TABLE = """
CREATE TEMPORARY TABLE `{work_table}` (
    user_id    VARCHAR(255),
    segment_id VARCHAR(255),
    ttl        timestamp
)
"""

SQL_TRUNCATE_WORK_TABLE = """
TRUNCATE TABLE `{work_table}`
"""

SQL_LOAD_TO_WORK_TABLE = """
LOAD DATA LOCAL INFILE '{file}'
    REPLACE
    INTO TABLE `{work_table}`
    FIELDS
        TERMINATED BY ','
    LINES
        TERMINATED BY '\n'
    (   user_id,
        segment_id,
        ttl
    )
"""

SQL_UPSERT_MAPPING_TABLE = """
INSERT INTO `{target_table}` (
    user_id,
    segment_id,
    ttl
)
SELECT
    t.user_id,
    t.segment_id,
    t.ttl
FROM
    `{work_table}` AS t
ON DUPLICATE KEY UPDATE
    ttl = t.ttl
"""


def load(sid, pid, env, csv_dir, use_temporary_table=True, delete_work_table=True):

    # load config
    config = BatchConfig(env)
    if not sid in config.database:
        raise ValueError('Database config not exists: ' + sid)
    dbconfig = config.database[sid]

    # check csv directory
    if not os.path.exists(csv_dir):
        raise ValueError('Not exists: ' + csv_dir)
    if not os.listdir(csv_dir):
        return

    # connect database
    conn = MySQLdb.connect(host=dbconfig['host'],
                           port=int(dbconfig['port']),
                           user=dbconfig['user'],
                           passwd=dbconfig['password'],
                           db=dbconfig['db_name'])
    cur = conn.cursor()
    try:
        # set timezone
        cur.execute("SET TIME_ZONE = '+9:00'")

        # determine table name
        target_table = TARGET_TABLE.format(sid=sid, pid=pid)
        if use_temporary_table:
            work_table = TEMP_TABLE
        else:
            work_table = WORK_TABLE.format(sid=sid, pid=pid)

        # initialize work table
        if use_temporary_table:
            query_c = SQL_CREATE_TEMPORARY_TABLE.format(work_table=work_table)
            cur.execute(query_c)
        else:
            query_t = SQL_TRUNCATE_WORK_TABLE.format(work_table=work_table)
            cur.execute(query_t)

        # load data from csv
        for f in os.listdir(csv_dir):
            csv_file = os.path.join(csv_dir, f)
            query_l = SQL_LOAD_TO_WORK_TABLE.format(file=csv_file,
                                                    work_table=work_table)
            cur.execute(query_l)

        # upsert mappig table
        query_u = SQL_UPSERT_MAPPING_TABLE.format(work_table=work_table,
                                                  target_table=target_table)
        cur.execute(query_u)

        # commit
        conn.commit()

        # truncate work table
        if not use_temporary_table and delete_work_table:
            query_t = SQL_TRUNCATE_WORK_TABLE.format(work_table=work_table)
            cur.execute(query_t)

    except MySQLdb.Error, e:
        conn.rollback()
        raise e
    finally:
        conn.close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Load csv into mapping table')
    parser.add_argument('sid', type=str, help='site_id')
    parser.add_argument('pid', type=str, help='partner_id')
    parser.add_argument('env', type=str, choices=('prd', 'stg', 'dev'), help='environment')
    parser.add_argument('csv_dir', type=str, help='csv directory')
    parser.add_argument('--use-temporary-table', dest='use_temporary_table', action='store_true')
    parser.add_argument('--no-use-temporary-table', dest='use_temporary_table', action='store_false')
    parser.add_argument('--delete-work-table', dest='delete_work_table', action='store_true')
    parser.add_argument('--no-delete-work-table', dest='delete_work_table', action='store_false')
    parser.set_defaults(delete_work_table=True)
    args = parser.parse_args()

    load(sid=args.sid,
         pid=args.pid,
         env=args.env,
         csv_dir=args.csv_dir,
         use_temporary_table=args.use_temporary_table,
         delete_work_table=args.delete_work_table)
