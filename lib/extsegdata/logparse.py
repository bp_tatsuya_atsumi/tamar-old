# -*- coding: utf-8 -*-

"""
api-data のログからユーザーセグメントを構成するためのモジュール
"""

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'production'
__version__ = '1.0.0'
__date__    = '2014/8/21'

import argparse
from   conf.config import BatchConfig
import csv
import datetime
import gzip
import json
import lib.util    as     util
import os
import time

def parse(sid, pid, env, log_dir, csv_dir):
    """
    ログのパース処理を実行する関数
    """

    # initialize
    config = BatchConfig(env)

    # read log under directory
    log_files = [os.path.join(log_dir, f) for f in os.listdir(log_dir)]
    log_files = [f for f in log_files if os.path.isfile(f)]
    for log_file in log_files:

        # create csv filename
        # e.g. log/2013-09-26-20_ip-10-0-16-6_0.gz
        #   -> csv/2013-09-26-20_ip-10-0-16-6_0.csv
        log_filename = os.path.basename(log_file)
        log_filename_root, log_filename_ext = os.path.splitext(log_filename)
        csv_filename = log_filename_root + '.csv'
        csv_file = os.path.join(csv_dir, csv_filename)

        # parse and write csv
        with open(csv_file, mode='w') as outf:
            writer = _create_csv_writer(outf)
            for seg_id, uids, ttl in yield_from_fluentd(log_file):
                if ttl is None:
                    ttl = datetime.datetime.utcnow() + datetime.timedelta(days=3)
                else:
                    ttl = datetime.datetime.utcfromtimestamp(ttl)
                for uid in uids:
                    try:
                        writer.writerow([uid, seg_id, ttl])
                    except UnicodeEncodeError:
                        continue

def _create_csv_writer(fp):
    """
    与えられたファイルポインタに関して
    CSVのwriterを作成する関数
    """
    return csv.writer(fp,
                      lineterminator='\n',
                      quoting=csv.QUOTE_NONE,
                      escapechar='\\')

def yield_from_fluentd(log_file):
    """
    fluentdのログからパラメータを抽出する関数
    """

    def __return_yield(log_file):
        with gzip.open(log_file, mode='rt') as inf:
            for row in csv.reader(inf, delimiter='\t'):
                if len(row) != 3:
                    continue
                r = json.loads(row[2])
                try:
                    yield (r['seg_id'], r['uids'], r['ttl'])
                except:
                    continue

    return __return_yield(log_file)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Parse log and make csv')
    parser.add_argument('sid', type=str, help='site_id')
    parser.add_argument('pid', type=str, help='partner_id')
    parser.add_argument('env', type=str, choices=('prd', 'stg', 'dev'), help='environment')
    parser.add_argument('log_dir', type=str, help='log directory')
    parser.add_argument('csv_dir', type=str, help='csv directory')
    args = parser.parse_args()

    parse(sid=args.sid,
          pid=args.pid,
          env=args.env,
          log_dir=args.log_dir,
          csv_dir=args.csv_dir)
