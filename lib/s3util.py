# -*- coding: utf-8 -*-

import os
import time
from   boto.exception     import S3ResponseError
from   boto.s3.connection import S3Connection
from   boto.s3.key        import Key
from   boto.utils         import compute_md5


AWS_ACCESS_KEY = 'AKIAJANBBP3TGSIL2ULQ'
AWS_SECRET_KEY = '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'
PATH_SEP = '/'

class S3Util():
    def __init__(self, access_key=AWS_ACCESS_KEY,
                       secret_key=AWS_SECRET_KEY):
        self.access_key = access_key
        self.secret_key = secret_key
        self.conn = self._connect()

    def _connect(self):
        return S3Connection(self.access_key, self.secret_key)

    def _get_bucket(self, s3bucket):
        return self.conn.get_bucket(s3bucket, validate=False)

    def _is_dir(self, keyname):
        return keyname.endswith(PATH_SEP)

    def _is_file(self, keyname):
        return not keyname.endswith(PATH_SEP)

    def _to_dir(self, keyname):
        if not keyname or self._is_dir(keyname):
            return keyname
        return keyname + PATH_SEP

    def _key_to_dict(self, key):
        if isinstance(key.name, unicode):
            name = key.name.encode('utf-8')
        else:
            name = str(key.name)
        is_dir = self._is_dir(name)
        return {
                'key'           : name,
                'is_dir'        : is_dir,
                'is_file'       : not is_dir,
                'size'          : 0 if is_dir else key.size,
                'last_modified' : str(key.last_modified)
                }

    def _keys_to_dicts(self, keys):
        return [self._key_to_dict(k) for k in keys]

    def exists(self, s3bucket, s3key):
        '''
        Return true if object exists

        Args:
            s3bucket: bucket name
            s3key: object key (e.g. path1/data.txt)

        Returns:
            True if exists, False if not exists
        '''
        bucket = self._get_bucket(s3bucket)
        key = Key(bucket, s3key)
        return key.exists()

    def list_dir(self, s3bucket, s3dir, recursive=False):
        '''
        List objects in the s3 directory

        Args:
            s3bucket: bucket name
            s3dir: s3 directory (e.g. path1/path2/)
            recursive: if True, list recursive

        Returns:
            List of dictionary. Each row represents s3object.
            example:
                    {
                     'key'           : 'path1/path2/data.txt',
                     'is_dir'        : False,
                     'is_file'       : True,
                     'size'          : 128,
                     'last_modified' : '2013-09-28T19:32:03.000Z'
                    }
        '''
        keys = self._list_dir(s3bucket, s3dir, recursive)
        return self._keys_to_dicts(keys)

    def _list_dir(self, s3bucket, s3dir, recursive=False, only_file=False):
        bucket = self._get_bucket(s3bucket)
        s3dir= self._to_dir(s3dir)
        keys = bucket.list(s3dir)

        # Determine target object type
        is_target_obj = lambda k: True
        if only_file:
            is_target_obj = lambda k: self._is_file(k.name)

        # If recursive is off, list only under directory
        is_leaf = lambda k: True
        if not recursive:
            is_leaf = lambda k: k.name.rstrip(PATH_SEP).count(PATH_SEP) == s3dir.count(PATH_SEP)

        return [k for k in keys if is_leaf(k) and is_target_obj(k)]

    def download_files(self, s3bucket, s3dir, localdir, recursive=False):
        '''
        Download files from the s3 directory

        Args:
            s3bucket: bucket name
            s3dir: s3 directory (e.g. path1/path2/)
            localdir: target local directory (e.g. /tmp/data/)
            recursive: if True, download recursive

        Returns:
            List of s3object dictionary that you download. (see listdir)

        Raises:
            ValueError: occured if localdir is not a directory.
        '''
        if not os.path.exists(localdir):
            os.mkdir(localdir)
        if not os.path.isdir(localdir):
            raise ValueError('Is not a directory: ' + localdir)

        # If recursive is off, download only files under directory
        only_file = True if not recursive else False

        # List files
        s3dir = self._to_dir(s3dir)
        keys = self._list_dir(s3bucket, s3dir, recursive, only_file)
        for key in keys:
            localpath = os.path.join(localdir,
                                     os.path.relpath(key.name, start=s3dir))

            # Make directory or download file
            if self._is_dir(key.name):
                if not os.path.exists(localpath):
                    os.makedirs(localpath)
            else:
                tmpdir = os.path.dirname(localpath)
                if not os.path.exists(tmpdir):
                    os.makedirs(tmpdir)
                key.get_contents_to_filename(localpath)

        return self._keys_to_dicts(keys)

    def download_single_file(self, s3bucket, s3file, localdir):
        '''
        Download file from the s3 directory

        Args:
            s3bucket: bucket name
            s3file: s3 file (e.g. path1/file.txt)
            localdir: target local directory (e.g. /tmp/data/)

        Returns:
            Dictionary of s3object that you download.(see listdir)
            If key not exsists, None return.

        Raises:
            ValueError: occurs if localdir is not a directory.
        '''
        if not os.path.exists(localdir):
            os.mkdir(localdir)
        if not os.path.isdir(localdir):
            raise ValueError('Is not a directory: ' + localdir)

        bucket = self._get_bucket(s3bucket)
        key = bucket.get_key(s3file)
        if not key or self._is_dir(key.name):
            return None
        localfile = os.path.join(localdir, os.path.basename(key.name))
        key.get_contents_to_filename(localfile)
        return self._key_to_dict(key)

    def download_single_file_order_by_filename(self, s3bucket, s3dir, localdir, order_by='desc'):
        '''
        Download file sorted by 'key' from the s3 directory
        Args:
            s3bucket: bucket name
            s3dir: s3 dir (e.g. path1/path2/)
            localdir: target local directory (e.g. /tmp/data/)
            order_by: sort order

        Returns:
            Dictionary of s3object that you download.(see listdir)
            If key not exists, None return.

        Raises:
            ValueError: occurs if localdir is not a directory
        '''
        s3file_list = self.list_dir(s3bucket, s3dir)
        if not s3file_list:
            return None
        if order_by == 'asc':
            s3file_list.sort(cmp=lambda x,y: cmp(x['key'], y['key']))
        else:
            s3file_list.sort(cmp=lambda x,y: cmp(y['key'], x['key']))
        return self.download_single_file(s3bucket, s3file_list[0]['key'], localdir)

    def upload_files(self, localdir, s3bucket, s3dir, recursive=False, max_retry_count=5):
        '''
        Upload files to the s3 directory

        Args:
            localdir: source local directory (e.g. /tmp/data/)
            s3bucket: bucket name
            s3dir: s3 directory (e.g. path1/path2/)
            recursive: if True, upload recursive
                       if False, updload only files under directory
            max_retry_count: max retry count

        Raises:
            ValueError: occured if localdir not exists or is not a directory.
        '''
        if not os.path.exists(localdir):
            raise ValueError('Not exists: ' + localdir)
        if not os.path.isdir(localdir):
            raise ValueError('Is not a directory: ' + localdir)

        # If recursive is off, updload only files under directory
        localfiles = []
        if recursive:
            for (root, dirs, files) in os.walk(localdir):
                localfiles.extend([os.path.join(root, f) for f in files])
        else:
            localfiles = [os.path.join(localdir, f) for f in os.listdir(localdir)]
            localfiles = [f for f in localfiles if os.path.isfile(f)]

        if not localfiles:
            return

        bucket = self._get_bucket(s3bucket)
        for localfile in localfiles:
            relpath = os.path.relpath(localfile, start=localdir)
            s3file  = os.path.join(s3dir, relpath)
            self._upload_single_file(localfile, bucket, s3file, max_retry_count)

    def upload_single_file(self, localfile, s3bucket, s3dir, max_retry_count=5):
        '''
        Upload file to the s3 directory. Empty directory will be ignored.

        Args:
            localfile: source local file (e.g. /tmp/data.txt)
            s3bucket: bucket name
            s3dir: s3 directory (e.g. path1/path2/)
            max_retry_count: max retry count
        '''
        if not os.path.exists(localfile):
            raise ValueError('Not exists: ' + localfile)
        if not os.path.isfile(localfile):
            raise ValueError('Is not a file: ' + localfile)

        bucket = self._get_bucket(s3bucket)
        s3file = os.path.join(s3dir, os.path.basename(localfile))
        self._upload_single_file(localfile, bucket, s3file, max_retry_count)

    def _upload_single_file(self, localfile, s3bucket, s3file, max_retry_count=5):
        md5 = compute_md5(open(localfile))
        for i in range(max_retry_count + 1):
            try:
                key = Key(s3bucket, s3file)
                key.set_contents_from_filename(localfile, md5=md5)
                return
            except S3ResponseError:
                if i == max_retry_count:
                    raise
                time.sleep(i + 1)

    def delete_files(self, s3bucket, s3dir):
        '''
        Delete files from s3

        Args:
            s3bucket: bucket name
            s3dir: s3 directory (e.g. path1/path2/)

        Returns:
            List of s3object dictionary that you delete. (see listdir)
        '''
        keys = self._list_dir(s3bucket, s3dir, recursive=True, only_file=False)
        bucket = self._get_bucket(s3bucket)
        bucket.delete_keys(keys)
        return self._keys_to_dicts(keys)

    def delete_single_file(self, s3bucket, s3file):
        '''
        Delete file from s3

        Args:
            s3bucket: bucket name
            s3file: s3 object (e.g. path1/path2/)

        Returns:
            Dictionary of s3object that you delete.(see listdir)
            If key not exsists, None return.
        '''
        bucket = self._get_bucket(s3bucket)
        key = bucket.get_key(s3file)
        if not key:
            return None
        key.delete()
        return self._key_to_dict(key)

    def copy_files(self, src_s3bucket, src_s3dir, dst_s3bucket, dst_s3dir):
        '''
        Copy file from s3 to s3

        Args:
            src_s3bucket: bucket name
            src_s3dir: s3 directory (e.g. path1/path2/)
            dst_s3bucket: bucket name
            dst_s3dir: s3 directory (e.g. path1/path2/)

        Returns:
            List of new s3object dictionary that you copy. (see listdir)
        '''
        return self._copy_files(src_s3bucket, src_s3dir, dst_s3bucket, dst_s3dir)

    def copy_single_file(self, src_s3bucket, src_s3file, dst_s3bucket, dst_s3dir):
        '''
        Copy file from s3 to s3

        Args:
            src_s3bucket: bucket name
            src_s3file: s3 object (e.g. path1/path2/data.txt)
            dst_s3bucket: bucket name
            dst_s3dir: s3 object (e.g. path1/path2/)

        Returns:
            Dictionary of new s3object that you copy.(see listdir)
            If key not exsists, None return.
        '''
        return self._copy_file(src_s3bucket, src_s3file, dst_s3bucket, dst_s3dir)

    def _copy_files(self, src_s3bucket, src_s3dir, dst_s3bucket, dst_s3dir, delete_src=False):
        keys = self._list_dir(src_s3bucket, src_s3dir, recursive=True)
        new_keys = []
        for key in keys:
            relkey = os.path.relpath(key.name, start=src_s3dir)
            if self._is_dir(key.name):
                relkey = self._to_dir(relkey)
            new_key = key.copy(dst_s3bucket, os.path.join(dst_s3dir, relkey))
            if delete_src:
                key.delete()
            new_keys.append(new_key)
        return self._keys_to_dicts(new_keys)

    def _copy_file(self, src_s3bucket, src_s3file, dst_s3bucket, dst_s3dir, delete_src=False):
        bucket = self._get_bucket(src_s3bucket)
        key = bucket.get_key(src_s3file)
        if not key:
            return None
        dst_s3file = os.path.join(dst_s3dir, os.path.basename(src_s3file))
        new_key = key.copy(dst_s3bucket, dst_s3file)
        if delete_src:
            key.delete()
        return self._key_to_dict(new_key)

    def move_files(self, src_s3bucket, src_s3dir, dst_s3bucket, dst_s3dir):
        '''
        Move file from s3 to s3

        Args:
            src_s3bucket: bucket name
            src_s3dir: s3 directory (e.g. path1/path2/)
            dst_s3bucket: bucket name
            dst_s3dir: s3 directory (e.g. path1/path2/)

        Returns:
            List of new s3object dictionary that you move. (see listdir)
        '''
        return self._copy_files(src_s3bucket, src_s3dir, dst_s3bucket, dst_s3dir, delete_src=True)

    def move_single_file(self, src_s3bucket, src_s3file, dst_s3bucket, dst_s3dir):
        '''
        Move file from s3 to s3

        Args:
            src_s3bucket: bucket name
            src_s3file: s3 object (e.g. path1/path2/data.txt)
            dst_s3bucket: bucket name
            dst_s3dir: s3 object (e.g. path1/path2/)

        Returns:
            Dictionary of new s3object that you move.(see listdir)
            If key not exsists, None return.
        '''
        return self._copy_file(src_s3bucket, src_s3file, dst_s3bucket, dst_s3dir, delete_src=True)
