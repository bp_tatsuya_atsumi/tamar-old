# -*- coding: utf-8 -*-

"""
rawdataそのものを表すクラス
"""

from   conf.config              import BatchConfig
from   lib.s3util               import S3Util
from   lib.transferer.exception import NoS3Files
import os
import tarfile

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'development'
__version__ = ''
__date__    = ''

class Rawdata():

    def __init__(self, env, s3bucket, s3file):
        self.config      = BatchConfig(env)
        self.env         = self.config.environment

        self.rawdata_name    = os.path.basename(s3file)
        self.s3_rawdata_path = s3file

        self.s3util = S3Util()
        self.s3_transfer_bucket   = s3bucket
        self.local_base_dir       = self.config.tmp_rawdata_dir

        self.logger = self.config.logger_rawdata
        self.logprefix = '[Rawdata {s3file}] '.format(s3file=self.s3_rawdata_path)

    def get_rawdata_from_s3(self):
        s3obj = self.s3util.download_single_file(self.s3_transfer_bucket, self.s3_rawdata_path, self.local_base_dir)
        if not s3obj:
            raise NoS3Files(self.s3_transfer_bucket, self.s3_rawdata_path)
        self.local_rawdata_path = '{path}/{name}'.format(path=self.local_base_dir, name=self.rawdata_name)

    def decompress_rawdata(self):
        with tarfile.open(self.local_rawdata_path, 'r:gz') as tar:
            tar.extractall(path=self.local_base_dir)
            tar.close()

        self.decompressed_rawdata_names = [member.name for member in tarfile.open(self.local_rawdata_path).getmembers() if member.isfile()]

    def upload_converted_rawdata(self, s3_converted_rawdata_dir, local_converted_rawdata_path):
        self.s3util.upload_single_file(local_converted_rawdata_path, self.s3_transfer_bucket, s3_converted_rawdata_dir)
