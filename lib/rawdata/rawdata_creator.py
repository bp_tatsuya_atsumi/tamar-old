# -*- coding: utf-8 -*-

from __future__ import absolute_import

"""
rawdataの生成管理を行うクラス
"""

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'development'
__version__ = ''
__date__    = ''

import argparse
from   conf.config                      import BatchConfig
import csv
import datetime
import lib.const                        as     const
from   lib.mappingtable                 import MappingTable
from   lib.partner_api.so.api_connector import ApiConnector
from   lib.rawdata.rawdata              import Rawdata
from   lib.rawdata.rawdata_header       import RawdataHeader
import os
import shutil
import tarfile

class RawdataCreator(object):
    MAPPING_TABLE_NAME = 't_user_mapping_{sid}_so'

    def __init__(self, sid, env, data_type, s3bucket, s3file):
        self.sid             = sid
        self.config          = BatchConfig(env)
        self.env             = self.config.environment
        self.local_base_dir  = self.config.tmp_rawdata_dir

        self._initialize_tmp_dir()

        self.rawdata_header = RawdataHeader()

        self.rawdata = Rawdata(env, s3bucket, s3file)

        base_name        = os.path.basename(s3file)
        my_date          = datetime.datetime.strptime(base_name, 'brainpad-%Y%m%d.tgz').date()
        self.target_date = my_date.strftime('%Y%m%d')

        self.log_prefix_str = '[{data_type}] '
        self.log_suffix_str = '{sid} {data_type} '

        log_prefix = self.log_prefix_str.format(data_type=data_type)
        log_suffix = self.log_suffix_str.format(sid='', data_type=data_type)
        self.logger = self.config.get_prefixed_rawdata_logger(log_prefix, log_suffix)

        advertiser_ids = [self.config.mapping[sid]['so']['advertiser_id']]

        with ApiConnector(env) as my_connector:
            self.order_ids     = my_connector.get_order_ids_from_advertiser_ids(advertiser_ids)
            self.order_id_dict    = self._convert_list_to_dict(self.order_ids)
            self.schedule_ids  = my_connector.get_schedule_ids_from_advertiser_ids(advertiser_ids, self.order_id_dict)
            self.schedule_id_dict = self._convert_list_to_dict(self.schedule_ids)
            self.cv_pixel_ids  = my_connector.get_cv_pixel_ids_from_advertiser_ids(advertiser_ids)
            self.cv_pixel_id_dict = self._convert_list_to_dict(self.cv_pixel_ids)

    def _initialize_tmp_dir(self):
        shutil.rmtree(self.local_base_dir, ignore_errors=True)
        os.makedirs(self.local_base_dir)

    def create(self):
        self.logger.info('<Start create rawdata process>')

        self.logger.info('Download s3file')
        self.rawdata.get_rawdata_from_s3()

        self.logger.info('Decompress rawdata')
        self.rawdata.decompress_rawdata()

        self.logger.info('prepare to convert')

        self._convert_rawdata()
        self._upload_converted_rawdata()

    def _convert_rawdata(self):
        for local_rawdata_name in self.rawdata.decompressed_rawdata_names:
            local_rawdata_path = '{path}/{name}'.format(path=self.local_base_dir, name=local_rawdata_name)

            data_type = self.__detect_data_type(local_rawdata_path)

            log_prefix = self.log_prefix_str.format(data_type=data_type)
            log_suffix = self.log_suffix_str.format(sid='', data_type=data_type)
            self.logger = self.config.get_prefixed_rawdata_logger(log_prefix, log_suffix)

            self.logger.info('start to process')
            if data_type == 'conversion':
                self.__convert_conversion_file(local_rawdata_path)
            else:
                self.__convert_order_and_click_file(local_rawdata_path, data_type)
            self.logger.info('finish processing')


    def __convert_order_and_click_file(self, local_rawdata_path, data_type):
        TIMESTAMP_COLUMN_NUM = 1
        IP_COLUMN_NUM        = 2
        ID_COLUMN_NUM        = 3
        ORDER_ID_COLUMN_NUM  = 6
        CSV_HEADER           = self.rawdata_header.ORDER_AND_CLICK_HEADER_LIST

        output_path = '{base_path}/{data_type}_{sid}_{date}.csv'.format(base_path=self.local_base_dir,
                                                                        data_type=data_type, sid=self.sid,
                                                                        date=self.target_date)

        with open(output_path, 'w') as fp:
            csv_writer = csv.writer(fp, lineterminator='\n', quotechar='"', quoting=csv.QUOTE_ALL)
            csv_writer.writerow(CSV_HEADER)

            conn = self.__connect_database()

            for row in csv.reader(open(local_rawdata_path, 'r'), delimiter='\t'):
                if not self._is_in_order_ids(row[ORDER_ID_COLUMN_NUM], self.order_id_dict):
                    continue
                row.pop()

                row[IP_COLUMN_NUM]        = self.__convert_hex_ip_str_to_ipaddress(row[IP_COLUMN_NUM])
                row[TIMESTAMP_COLUMN_NUM] = self.__convert_unixtimestamp_to_datetime(row[TIMESTAMP_COLUMN_NUM])

                rtoaster_ids = self.__mapped_scaleout_id_to_rtoaster_id(conn, row[ID_COLUMN_NUM])
                if rtoaster_ids:
                    for rtoaster_id in rtoaster_ids:
                        row[ID_COLUMN_NUM] = rtoaster_id
                        csv_writer.writerow(row)

            conn.conn.close()

    def __convert_conversion_file(self, local_rawdata_path):
        CV_PIXEL_ID_COLUMN_NUM = 0
        TIMESTAMP_COLUMN_NUM   = 1
        ID_COLUMN_NUM          = 2
        SCHEDULE_ID_COLUMN_NUM = 6
        TIMESTAMP2_COLUMN_NUM  = 7
        CSV_HEADER             = self.rawdata_header.CONVERSION_HEADER_LIST

        output_path = '{base_path}/conversion_{sid}_{date}.csv'.format(base_path=self.local_base_dir,
                                                                       sid=self.sid, date=self.target_date)

        with open(output_path, 'w') as fp:
            csv_writer = csv.writer(fp, lineterminator='\n', quotechar='"', quoting=csv.QUOTE_ALL)
            csv_writer.writerow(CSV_HEADER)

            conn = self.__connect_database()
            for row in csv.reader(open(local_rawdata_path, 'r'), delimiter='\t'):
                if len(row) < SCHEDULE_ID_COLUMN_NUM or \
                        not self._is_in_schedule_ids(row[SCHEDULE_ID_COLUMN_NUM], self.schedule_id_dict) or \
                        not self._is_in_cv_pixel_ids(row[CV_PIXEL_ID_COLUMN_NUM], self.cv_pixel_id_dict):
                    continue
                row.pop()

                row[TIMESTAMP_COLUMN_NUM] = self.__convert_unixtimestamp_to_datetime(row[TIMESTAMP_COLUMN_NUM])
                if len(row) >= TIMESTAMP2_COLUMN_NUM and len(row[TIMESTAMP2_COLUMN_NUM]):
                    row[TIMESTAMP2_COLUMN_NUM] = self.__convert_unixtimestamp_to_datetime(row[TIMESTAMP2_COLUMN_NUM][:-3])

                rtoaster_ids = self.__mapped_scaleout_id_to_rtoaster_id(conn, row[ID_COLUMN_NUM])
                if rtoaster_ids:
                    for rtoaster_id in rtoaster_ids:
                        row[ID_COLUMN_NUM] = rtoaster_id
                        csv_writer.writerow(row)
            conn.conn.close()

    def __convert_unixtimestamp_to_datetime(self, unixtimestamp_str):
        return datetime.datetime.fromtimestamp(int(unixtimestamp_str)).strftime('%Y-%m-%d %H:%M:%S')

    def __convert_hex_ip_str_to_ipaddress(self, hex_ip_str):
        hex_ip_list = [hex_ip_str[i*2:i*2+2] for i in range(4)]
        return '.'.join([str(int(x, 16)) for x in hex_ip_list])

    def __connect_database(self):
        return MappingTable(self.config.database[self.sid]['user'],
                            self.config.database[self.sid]['password'],
                            self.config.database[self.sid]['host'],
                            self.config.database[self.sid]['db_name'],
                            self.MAPPING_TABLE_NAME.format(sid=self.sid))

    def __detect_data_type(self, rawdata_path):
        if 'order' in rawdata_path:
            return 'order'
        elif 'click' in rawdata_path:
            return 'click'
        else:
            return 'conversion'

    def __mapped_scaleout_id_to_rtoaster_id(self, conn, scaleout_id):
        return [row[0] for row in conn.select_rows_by_partner_user_id(scaleout_id)]

    def _upload_converted_rawdata(self):
        rawdata_path_list = []
        for data_type in ['order', 'click', 'conversion']:
            output_path = '{base_path}/{data_type}_{sid}_{date}.csv'.format(base_path=self.local_base_dir,
                                                                            data_type=data_type,
                                                                            sid=self.sid,
                                                                            date=self.target_date)
            rawdata_path_list.append(output_path)
        compressed_path = self._compress_rawdata(rawdata_path_list)
        s3_converted_rawdata_path = "rawdata/converted/{sid}".format(sid=self.sid)
        self.rawdata.upload_converted_rawdata(s3_converted_rawdata_path, compressed_path)
        self.logger.info('Upload converted file')

    def _compress_rawdata(self, rawdata_path_list):
        tar_file_path = '{base_path}/{sid}_{date}.tgz'.format(base_path = self.local_base_dir,
                                                              sid       = self.sid,
                                                              date      = self.target_date)
        with tarfile.open(tar_file_path, 'w:gz') as tar_file:
            for rawdata_path in rawdata_path_list:
                tar_file.add(rawdata_path, arcname=os.path.basename(rawdata_path))
        return tar_file_path

    def _is_in_order_ids(self, order_id, order_ids):
        return order_id in order_ids

    def _is_in_schedule_ids(self, schedule_id, schedule_ids):
        return schedule_id in schedule_ids

    def _is_in_cv_pixel_ids(self, cv_pixel_id, cv_pixel_ids):
        return cv_pixel_id in cv_pixel_ids

    def _convert_list_to_dict(self, my_list):
        return { val: '' for val in my_list }

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='rawdata creator')
    parser.add_argument('sid', type=str, help='site id')
    parser.add_argument('env', type=str, choices=const.ENVS, help='environment')
    parser.add_argument('data_type', type=str, choices=const.RECEIVE_DATATYPES, help='environment')
    parser.add_argument('s3bucket', type=str, help='environment')
    parser.add_argument('s3file', type=str, help='environment')
    args = parser.parse_args()

    creator = RawdataCreator(sid=args.sid,
                             env=args.env,
                             data_type=args.data_type,
                             s3bucket=args.s3bucket,
                             s3file=args.s3file)
    creator.create()
