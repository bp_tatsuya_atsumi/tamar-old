# -*- coding: utf-8 -*-

from __future__ import absolute_import

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'development'
__version__ = ''
__date__    = ''

class RawdataHeader(object):

    def __init__(self):
        self.ORDER_AND_CLICK_HEADER_LIST = [u"種別",u"アクセス時刻",u"IP",u"UID",u"ページID",u"ロボット判定",u"オーダーID",u"内部ID",u"スケジュールID",u"クリエイティブID",u"デバイス",u"キャリア",u"OS",u"ブラウザ",u"リファラドメイン",u"初回アクセスフラグ"]
        self.ORDER_AND_CLICK_HEADER_LIST = map(lambda x: x.encode('cp932'), self.ORDER_AND_CLICK_HEADER_LIST)
        self.STR_ORDER_AND_CLICK_HEADER  = self.get_str_csv_header(self.ORDER_AND_CLICK_HEADER_LIST)

        self.CONVERSION_HEADER_LIST = [u"ピクセルID",u"CV発生時刻",u"UID",u"CV対象",u"ビュー・クリック種別",u"対象ID",u"スケジュールID",u"CV対象最終アクセス時刻"]
        self.CONVERSION_HEADER_LIST = map(lambda x: x.encode('cp932'), self.CONVERSION_HEADER_LIST)
        self.STR_CONVERSION_HEADER  = self.get_str_csv_header(self.CONVERSION_HEADER_LIST)

    def get_str_csv_header(self, csv_header_list):
        
        str_csv_header = ','.join(map(lambda x:'"' + x + '"', csv_header_list))
        return str_csv_header
