# -*- coding: utf-8 -*-

"""
utility的な関数をまとめたモジュール
"""

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'production'
__version__ = '1.0.0'
__date__    = '2014/5/20'

import csv
import gzip
import hashlib
import os
import urlparse
import datetime

def is_valid_params(params):
    """
    params
    """
    request_keys = params.keys()

    RTOASTER_VALID_KEYS = ['sid', 'pid', 'uid']

    if set(request_keys) == set(RTOASTER_VALID_KEYS):
        return True
    else:
        return False

def strip_str(s, limit_len=100, encoding='utf-8'):
    def _strip_unicode(u):
        STR_THREE_POINT = u'…'
        if len(u) <= limit_len:
            return u
        return u[:(limit_len - len(STR_THREE_POINT))] + STR_THREE_POINT

    if s is None:
        return s
    if isinstance(s, str):
        u = unicode(s,encoding)
        return _strip_unicode(u).encode(encoding)
    elif isinstance(s, unicode):
        return _strip_unicode(s)
    else:
        raise ValueError('is not str or unicode')

def code_to_long_id(code):
    if not code:
        raise ValueError('code is empty')
    h128 = hashlib.md5(code).hexdigest()
    h64  = h128[0:16]
    return long(h64, base=16) % (9223372036854775807 + 1)

def code_to_int_id(code):
    if not code:
        raise ValueError('code is empty')
    h128 = hashlib.md5(code).hexdigest()
    h32  = h128[0:8]
    return int(h32, base=16) % (2147483647 + 1)

def remove_files(files):
    for f in files:
        if f and os.path.exists(f) and os.path.isfile(f):
            os.remove(f)

def gzip_file(src_file, dst_file=None):
    if not src_file :
        raise ValueError('src_file is none or empty')
    if src_file.endswith('.gz'):
        raise ValueError('invalid src_file')
    if src_file == dst_file:
        raise ValueError('src_file equals dst_file')

    if not dst_file:
        dst_file = src_file + '.gz'
    with open(src_file, 'rb') as srcf:
        with gzip.open(dst_file, 'wb') as dstf:
            dstf.write(srcf.read())
    return dst_file

def gunzip_file(src_file, dst_file=None):
    if not src_file:
        raise ValueError('src_file is none or empty')
    if not src_file.endswith('.gz'):
        raise ValueError('invalid src_file')
    if src_file == dst_file:
        raise ValueError('src_file equals dst_file')

    if not dst_file:
        dst_file = src_file.rstrip('.gz')
    with gzip.open(src_file, 'rb') as srcf:
        with open(dst_file, 'wb') as dstf:
            dstf.write(srcf.read())
    return dst_file

def create_n_delta_days_datetime_str(format_str='%Y%m%d', delta_days=0):
    """
    現在日時に対して任意のフォーマットでx日前/x日後の文字列を生成する関数

    format_str : フォーマット(必須)
    delta_days : n日前, n日後の指定(正の値でn日前を指定する)

    文字列生成に失敗した場合はFalseを返す
    """

    try:
        now = datetime.datetime.now()
        calculated_datetime = now - datetime.timedelta(delta_days)

        return calculated_datetime.strftime(format_str)

    except:
        return False
    
