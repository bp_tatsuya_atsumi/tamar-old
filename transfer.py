# -*- coding: utf-8 -*-

import argparse
import importlib
import os
import re
from   conf.config  import BatchConfig
import lib.const    as     const
from   lib.lockfile import LockFile
from   lib.transferer.transfer_reporter import TransferReporter


SETTINGS = {
                'segmentmst'            : {
                                            'class_name'  : 'SegmentMstTransferer',
                                            's3_path'     : '{sid}/segment_list_mst/raw',
                                          },
                'remove_segmentmst'     : {
                                            'class_name'  : 'RemoveSegmentMstTransferer',
                                            's3_path'     : '{sid}/segment_list_mst/raw',
                                          },
                'segmentdata'           : {
                                            'class_name'  : 'SegmentDataTransferer',
                                            's3_path'     : '{sid}/segment_list_data/raw',
                                          },
                'conversion'            : {
                                            'class_name'  : 'ConversionTransferer',
                                            's3_path'     : '{sid}/conversion_log/raw',
                                          },
                'itemmst'               : {
                                            'class_name'  : 'ItemMstTransferer',
                                            's3_path'     : '{sid}/item_mst/raw',
                                          },
                'recommendmst'          : {
                                            'class_name'  : 'RecommendMstTransferer',
                                            's3_path'     : '{sid}/recommend_list_mst/raw',
                                          },
                'remove_recommendmst'   : {
                                            'class_name'  : 'RemoveRecommendMstTransferer',
                                            's3_path'     : '{sid}/recommend_list_mst/raw',
                                          },
                'recommenddata'         : {
                                            'class_name'  : 'RecommendDataTransferer',
                                            's3_path'     : '{sid}/recommend_list_data/raw',
                                          },
                'mailrequest'           : {
                                            'class_name'  : 'MailRequestTransferer',
                                            's3_path'     : '{sid}/mail_request_data/raw',
                                          },
                'mailresponse'          : {
                                            'class_name'  : 'MailResponseTransferer',
                                            's3_path'     : '{sid}/mail_response_data/raw',
                                          },
                'mailsegmentdata'       : {
                                            'class_name'  : 'MailSegmentDataTransferer',
                                            's3_path'     : '{sid}/mail_list_data/raw',
                                          },
           }

def start_transfer(sid, pid, env, data_type, s3_bucket=None, s3_path=None, **option):

    # load config
    config = BatchConfig(env)
    logger = config.logger_transfer
    logprefix = '[Transfer {sid} {pid} {data_type}] '\
                    .format(sid=sid, pid=pid, data_type=data_type)

    logger.info(logprefix + '<Start transfer>')

    # lock
    if not s3_bucket and not s3_path:
        lock_key = 'transfer_{sid}_{pid}_{data_type}'\
                        .format(sid=sid, pid=pid, data_type=data_type)
        lock_file = LockFile(lock_key, env)
        if not lock_file.lock():
            logger.warn(logprefix + 'Still running')
            logger.info(logprefix + '<End transfer>')
            return

    try:
        # initialize
        tmp_transfer_dir = config.tmp_transfer_dir.format(sid=sid, pid=pid, data_type=data_type)
        if not os.path.exists(tmp_transfer_dir):
            os.makedirs(tmp_transfer_dir)

        # target file
        _s3_bucket = s3_bucket if s3_bucket else config.s3bucket_transfer_data
        _s3_path   = s3_path   if s3_path   else SETTINGS[data_type]['s3_path'].format(sid=sid)

        # create transfer object
        TransfererClass = _get_transferer_class(sid, pid, data_type)
        if option:
            transferer = TransfererClass(sid, pid, env, _s3_bucket, _s3_path, **option)
        else:
            transferer = TransfererClass(sid, pid, env, _s3_bucket, _s3_path)

        # execute transfer
        transfer_result = transferer.execute()
        logger.info(logprefix + 'Success to transfer\n%s', transfer_result.to_json())

        # send report
        try:
            TransferReporter(transfer_result).send_report()
        except:
            logger.exception(logprefix + 'Failed to send report')

    except Exception, e:
        logger.exception(logprefix + 'Failed to transfer')
        raise e
    finally:
        # unlock
        if not s3_bucket and not s3_path:
            lock_file.unlock()
        logger.info(logprefix + '<End transfer>')


def _get_transferer_class(sid, pid, data_type):
    if sid == '0009' and data_type in ['recommenddata', 'itemmst']:
        _pid = pid + '_gdo'
    elif pid.startswith('rt'):
        _pid = 'rt'
    else:
        _pid = pid

    if _pid == 'rt':
        if re.match('rt[0-9]{4}', pid):
            module_name = 'lib.transferer.{pid}.{data_type}.bridge'.format(pid=_pid, data_type=data_type)
        else:
            module_name = 'lib.transferer.{pid}.{data_type}.external'.format(pid=_pid, data_type=data_type)
    else:
        module_name   = 'lib.transferer.{pid}.{data_type}'.format(pid=_pid, data_type=data_type)

    class_name    = SETTINGS[data_type]['class_name']
    import_module = importlib.import_module(module_name)
    return getattr(import_module, class_name)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='data transferer')
    parser.add_argument('sid', type=str, help='site id')
    parser.add_argument('pid', type=str, help='partner id')
    parser.add_argument('env', type=str, choices=const.ENVS, help='environment')
    parser.add_argument('data_type',   type=str, choices=const.DATATYPES, help='data type')
    parser.add_argument('--s3_bucket', type=str, help='s3bucket')
    parser.add_argument('--s3_path',   type=str, help='s3path')
    args = parser.parse_args()

    start_transfer(sid=args.sid,
                   pid=args.pid,
                   env=args.env,
                   data_type=args.data_type,
                   s3_bucket=args.s3_bucket,
                   s3_path=args.s3_path)
