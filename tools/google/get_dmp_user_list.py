# -*- coding: utf-8 -*-

"""
DMPに連携しているenetityId毎のデータを確認するためのスクリプト

引数にentityIdを与えて利用します。

Example:
    export PYTHONPATH=.:$PYTHONPATH; python tools/google/get_dmp_user_list.py 375335    
"""

from __future__ import absolute_import

import argparse
from lib.partner_api.go.googleads import ddp
import logging

logging.basicConfig(level=logging.INFO)
logging.getLogger('suds.client').setLevel(logging.DEBUG)

def main(client, entity_id):
    dmp_userlist_service = client.GetService('DmpUserListService', version='v201402')

    clientCustomerId = {'product' : 'INVITE_ADVERTISER',
                        'entityId' : entity_id}

    serviceSelector = {
                        "fields" : ['Name', 'Status', 'Description', 'Size', 'SizeRange']
                      }
    try:
        dmp_userlists = dmp_userlist_service.get(clientCustomerId, serviceSelector)
        for dmp_userlist in dmp_userlists['entries']:
            print dmp_userlist
    except Exception as e:
        print e.message


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('entity_id', type=int, help='entityId')
    args = parser.parse_args()

    ddp_client = ddp.DdpClient.LoadFromStorage()

    main(client=ddp_client, entity_id=args.entity_id)
