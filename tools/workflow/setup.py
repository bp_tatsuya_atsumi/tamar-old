# -*- coding: utf-8 -*-

from __future__ import absolute_import

import argparse
import boto.swf
from   boto.swf.exceptions import SWFTypeAlreadyExistsError
from   conf.config         import BatchConfig
import lib.aws.credentials as     credentials
import lib.const           as     const


#---------------------------------------------------------------------------
#   Register
#---------------------------------------------------------------------------
def register(args):
    def _register_workflow_type(workflow_name):
        try:
            def _get_config_value(key, default_key):
                wftype_config = config.workflow['wftype_configs'].get(workflow_name, {})
                return wftype_config.get(key, config.workflow[default_key])

            swf.register_workflow_type(
                domain=domain,
                name=workflow_name,
                version=version,
                task_list=_get_config_value(
                    'task_list',
                    'default_wftype_task_list'
                ),
                default_child_policy=_get_config_value(
                    'child_policy',
                    'default_wftype_child_policy'
               ),
                default_execution_start_to_close_timeout=_get_config_value(
                    'execution_start_to_close_timeout',
                    'default_wftype_execution_start_to_close_timeout'
                ),
                default_task_start_to_close_timeout=_get_config_value(
                    'wftype_task_start_to_close_timeout',
                    'default_wftype_task_start_to_close_timeout'
                )
            )
        except SWFTypeAlreadyExistsError:
            print '{:<14} {:<50} already exists'.format('WorkflowType', '{}({})'.format(workflow_name, version))
        else:
            print '{:<14} {:<50} registered successfully'.format('WorkflowType', '{}({})'.format(workflow_name, version))

    def _register_activity_type(activity_name):
        try:
            def _get_config_value(key, default_key):
                activity_config = config.workflow['acttype_configs'].get(activity_name, {})
                return activity_config.get(key, config.workflow[default_key])

            swf.register_activity_type(
                domain=domain,
                name=activity_name,
                version=version,
                task_list=_get_config_value(
                    'task_list',
                    'default_acttype_task_list'
                ),
                default_task_schedule_to_close_timeout=_get_config_value(
                    'schedule_to_close_timeout',
                    'default_acttype_task_schedule_to_close_timeout'
                ),
                default_task_schedule_to_start_timeout=_get_config_value(
                    'schedule_to_start_timeout',
                    'default_acttype_task_schedule_to_start_timeout'
                ),
                default_task_start_to_close_timeout=_get_config_value(
                    'start_to_close_timeout',
                    'default_acttype_task_start_to_close_timeout'
                ),
                default_task_heartbeat_timeout=_get_config_value(
                    'heartbeat_timeout',
                    'default_acttype_task_heartbeat_timeout'
                )
            )
        except SWFTypeAlreadyExistsError:
            print '{:<14} {:<50} already exists'.format('ActivityType', '{}({})'.format(activity_name, version))
        else:
            print '{:<14} {:<50} registered successfully'.format('ActivityType', '{}({})'.format(activity_name, version))

    swf       = _connect_to_swf()
    config    = BatchConfig(args.env)
    domain    = config.workflow['domain']
    version   = config.workflow['version']

    map(_register_workflow_type, config.workflow['wftypes'])
    map(_register_activity_type, config.workflow['acttypes'])


#---------------------------------------------------------------------------
#   Deprecate
#---------------------------------------------------------------------------
def deprecate(args):
    def _deprecate_workflow_type(workflow_name):
        try:
            swf.deprecate_workflow_type(
                domain=domain,
                workflow_name=workflow_name,
                workflow_version=version
            )
        except:
            print '{:<14} {:<50} deprecate failed'.format('WorkflowType', '{}({})'.format(workflow_name, version))
        else:
            print '{:<14} {:<50} deprecated successfully'.format('WorkflowType', '{}({})'.format(workflow_name, version))

    def _deprecate_activity_type(activity_name):
        try:
            swf.deprecate_activity_type(
                domain=domain,
                activity_name=activity_name,
                activity_version=version
            )
        except:
            print '{:<14} {:<50} deprecate failed'.format('ActivityType', '{}({})'.format(activity_name, version))
        else:
            print '{:<14} {:<50} deprecated successfully'.format('ActivityType', '{}({})'.format(activity_name, version))

    swf       = _connect_to_swf()
    config    = BatchConfig(args.env)
    domain    = config.workflow['domain']
    version   = args.version

    workflow_types = swf.list_workflow_types(domain=domain, registration_status='REGISTERED')['typeInfos']
    workflow_names = [w['workflowType']['name'] for w in workflow_types if w['workflowType']['version'] == version]
    map(_deprecate_workflow_type, workflow_names)

    activity_types = swf.list_activity_types(domain=domain, registration_status='REGISTERED')['typeInfos']
    activity_names = [a['activityType']['name'] for a in activity_types if a['activityType']['version'] == version]
    map(_deprecate_activity_type, activity_names)


#---------------------------------------------------------------------------
#   Private function
#---------------------------------------------------------------------------
def _connect_to_swf():
    return boto.swf.connect_to_region(
              region_name=credentials.AWS_REGION,
              aws_access_key_id=credentials.AWS_ACCESS_KEY,
              aws_secret_access_key=credentials.AWS_SECRET_KEY
           )


#---------------------------------------------------------------------------
#   Commandline function
#---------------------------------------------------------------------------
if __name__ == '__main__':

    # create command
    parser = argparse.ArgumentParser()

    # create subcommands
    subparsers = parser.add_subparsers(help='subcommands')

    # create "register" command
    parser_register = subparsers.add_parser('register', help='register settings')
    parser_register.add_argument('env', type=str, choices=const.ENVS, help='environment')
    parser_register.set_defaults(func=register)

    # create "deprecate" command
    parser_deprecate = subparsers.add_parser('deprecate', help='deprecate settings')
    parser_deprecate.add_argument('env', type=str, choices=const.ENVS, help='environment')
    parser_deprecate.add_argument('version', type=str, help='version')
    parser_deprecate.set_defaults(func=deprecate)

    # parse argument
    args = parser.parse_args()

    # execute command
    args.func(args)
