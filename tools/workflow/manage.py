# -*- coding: utf-8 -*-

from __future__ import absolute_import

import argparse
import datetime
import itertools
import pprint
import sys
import time
import traceback
import boto.swf
from   conf.config         import BatchConfig
import lib.aws.credentials as credentials
import lib.const           as const


def _setup(env):
    config = BatchConfig(args.env)
    return (
                _connect_to_swf(),
                config.workflow['domain'],
                config.workflow['version'],
                config.workflow['task_list']
           )

def _connect_to_swf():
    return boto.swf.connect_to_region(
              region_name=credentials.AWS_REGION,
              aws_access_key_id=credentials.AWS_ACCESS_KEY,
              aws_secret_access_key=credentials.AWS_SECRET_KEY
           )

def _list_active_workflows(swf, domain, hours):
    target_date = datetime.datetime.now() - datetime.timedelta(hours=hours)
    return swf.list_open_workflow_executions(
               domain, oldest_date=time.mktime(target_date.timetuple())
           )

def _list_closed_workflows(swf, domain, hours):
    target_date = datetime.datetime.now() - datetime.timedelta(hours=hours)
    return swf.list_closed_workflow_executions(
               domain, start_oldest_date=time.mktime(target_date.timetuple())
           )

def _format_timestamp(timestamp):
    return datetime.datetime.fromtimestamp(timestamp).strftime("%Y-%m-%d %H:%M:%S")

"""
List workflows
"""
def list_workflows(args):
    swf, domain, version, task_list = _setup(args.env)
    func_list = (_list_active_workflows if args.status == 'active'
                                        else _list_closed_workflows)
    execution_infos = func_list(swf, domain, args.hours)['executionInfos']
    if args.debug_enabled:
        pprint.PrettyPrinter(indent=2).pprint(execution_infos)
    for execution_info in execution_infos:
        _print_workflow(swf, domain, execution_info, args.debug_enabled)

def list_closed_workflows(args):
    swf, domain, version, task_list = _setup(args.env)

    execution_infos = _list_closed_workflows(swf, domain, args.hours)['executionInfos']
    if args.debug_enabled:
        pprint.PrettyPrinter(indent=2).pprint(execution_infos)
    for execution_info in execution_infos:
        _print_workflow(swf, domain, execution_info, args.debug_enabled)

def _print_workflow(swf, domain, execution_info, debug_enabled=False):
    _print_boundary()
    _print_workflow_detail(swf, domain, execution_info, debug_enabled)
    _print_workflow_events(swf, domain, execution_info, debug_enabled)

def _print_boundary():
    print '-' * 200

def _print_workflow_detail(swf, domain, execution_info, debug_enabled=False):
    FORMAT_WF       = '{:<40} {:<50} {:<50} {:<10} {:<15} {:<20}'

    run_id          = execution_info['execution']['runId']
    workflow_id     = execution_info['execution']['workflowId']
    workflow_info   = swf.describe_workflow_execution(domain, run_id, workflow_id)

    workflow_type   = '{0}({1})'.format(execution_info['workflowType']['name'],
                                       execution_info['workflowType']['version'])
    status          = execution_info['executionStatus']
    open_counts     = '/'.join([
                          str(workflow_info['openCounts']['openChildWorkflowExecutions']),
                          str(workflow_info['openCounts']['openDecisionTasks']),
                          str(workflow_info['openCounts']['openActivityTasks'])
                      ])
    start_datetime  = _format_timestamp(execution_info['startTimestamp'])

    print FORMAT_WF.format('(WorkflowType)', '(WorkflowId)', '(RunId)', '(Status)', '(OpenCounts)', '(StartDatetime)')
    print FORMAT_WF.format(workflow_type,    workflow_id,    run_id,    status,     open_counts,    start_datetime)
    if debug_enabled:
        print ''
        print FORMAT_WF.format('(Workflow-debug)', '', '', '', '', '')
        pprint.PrettyPrinter(indent=2).pprint(workflow_info)
        print ''

def _print_workflow_events(swf, domain, execution_info, debug_enabled=False):
    FORMAT_EVENTS   = '    {:<40}{:<40}{:<40}{:<40}{:<40}'

    run_id          = execution_info['execution']['runId']
    workflow_id     = execution_info['execution']['workflowId']
    workflow_events = swf.get_workflow_execution_history(domain, run_id, workflow_id)['events']

    _format_event   = lambda e: '{0:02d}-{1}'.format(e['eventId'], e['eventType'])
    event_strs      = [_format_event(e) for e in workflow_events]
    event_strs      = itertools.izip_longest(*[iter(event_strs)] * 5, fillvalue='')

    print FORMAT_EVENTS.format('(Events)', '', '', '', '')
    for event_str in event_strs:
        print FORMAT_EVENTS.format(*event_str)

    if debug_enabled:
        print ''
        print FORMAT_EVENTS.format('(Events-debug)', '', '', '', '')
        pprint.PrettyPrinter(indent=2).pprint(workflow_events)
        print ''

"""
List settings
"""
def list_settings(args):
    print 'Domains'
    print '-' * 180
    list_domains(args)
    print ''

    print 'Workflow Types'
    print '-' * 180
    list_workflow_types(args)
    print ''

    print 'Activity Types'
    print '-' * 180
    list_activity_types(args)

"""
List domains
"""
def list_domains(args):
    FORMAT_DOMAIN = '{:<16}{:<42}{:<12}{:<18}'

    print FORMAT_DOMAIN.format('(Domain)', '(Description)', '(Status)', '(RetentionDays)')
    swf = _connect_to_swf()
    domain_infos = swf.list_domains('REGISTERED')['domainInfos']
    for domain_info in domain_infos:
        domain_detail = swf.describe_domain(domain_info['name'])
        print FORMAT_DOMAIN.format(
                  domain_info['name'],
                  domain_info['description'],
                  domain_info['status'],
                  domain_detail['configuration']['workflowExecutionRetentionPeriodInDays']
              )

"""
List workflow types
"""
def list_workflow_types(args):
    FORMAT_WFTYPE = '{:<16}{:<42}{:<12}{:<22}{:<14}{:<16}{:<54}'

    swf, domain, version, task_list = _setup(args.env)
    workflow_types =  swf.list_workflow_types(domain, 'REGISTERED')['typeInfos']

    print FORMAT_WFTYPE.format(
              '(Domain)',
              '(WorkflowType)',
              '(Version)',
              '(CreationDate)',
              '(TaskList)',
              '(ChildPolicy)',
              '(Timeout ExecutionStartToClose / TaskStartToClose)'
          )
    for workflow_type in workflow_types:
        name          = workflow_type['workflowType']['name']
        version       = workflow_type['workflowType']['version']
        configuration = swf.describe_workflow_type(domain, name, version)['configuration']
        print FORMAT_WFTYPE.format(
                  domain,
                  name,
                  version,
                  _format_timestamp(workflow_type['creationDate']),
                  configuration['defaultTaskList']['name'],
                  configuration['defaultChildPolicy'],
                  '{} / {}'.format(
                      configuration['defaultExecutionStartToCloseTimeout'],
                      configuration['defaultTaskStartToCloseTimeout']
                  )
              )

"""
List activity types
"""
def list_activity_types(args):
    FORMAT_ACTTYPE = '{:<16}{:<42}{:<12}{:<22}{:<74}'

    swf, domain, version, task_list = _setup(args.env)
    activity_types = swf.list_activity_types(domain, 'REGISTERED')['typeInfos']

    print FORMAT_ACTTYPE.format(
              '(Domain)',
              '(ActivityType)',
              '(Version)',
              '(CreationDate)',
              '(Timeout ScheduleToStart / ScheduleToClose / StartToClose / Hearbeat)'
          )
    for activity_type in activity_types:
        name          = activity_type['activityType']['name']
        version       = activity_type['activityType']['version']
        configuration = swf.describe_activity_type(domain, name, version)['configuration']
        print FORMAT_ACTTYPE.format(
                  domain,
                  name,
                  version,
                  _format_timestamp(activity_type['creationDate']),
                  '{} / {} / {} / {}'.format(
                      configuration['defaultTaskScheduleToStartTimeout'],
                      configuration['defaultTaskScheduleToCloseTimeout'],
                      configuration['defaultTaskStartToCloseTimeout'],
                      configuration['defaultTaskHeartbeatTimeout']
                  )
              )

"""
Count tasks
"""
def count_tasks(args):
    swf, domain, version, task_list = _setup(args.env)
    print 'Decision tasks: {:d}'.format(swf.count_pending_decision_tasks(domain, task_list)['count'])
    print 'Activity tasks: {:d}'.format(swf.count_pending_activity_tasks(domain, task_list)['count'])

"""
Terminate workflow
"""
def terminate(args):
    def _terminate(workflow_id):
        try:
            swf.terminate_workflow_execution(
                domain=domain,
                workflow_id=workflow_id,
                child_policy='TERMINATE'
            )
        except:
            traceback.print_exc()
            return False
        else:
            print '{} terminated'.format(workflow_id)
            return True

    swf, domain, version, task_list = _setup(args.env)
    if args.workflow_id == 'all':
        execution_infos = _list_active_workflows(swf, domain, hours=24)['executionInfos']
        workflow_ids    = [e['execution']['workflowId'] for e in execution_infos]
    else:
        workflow_ids    = [args.workflow_id]

    if not all([_terminate(w) for w in workflow_ids]):
        sys.exit(1)

"""
Cancel workflow
"""
def cancel(args):
    def _cancel(workflow_id):
        try:
            swf.request_cancel_workflow_execution(
                domain=domain,
                workflow_id=workflow_id
            )
        except:
            traceback.print_exc()
            return False
        else:
            print '{} cancel requested'.format(workflow_id)
            return True

    swf, domain, version, task_list = _setup(args.env)
    if args.workflow_id == 'all':
        execution_infos = _list_active_workflows(swf, domain, hours=24)['executionInfos']
        workflow_ids    = [e['execution']['workflowId'] for e in execution_infos]
    else:
        workflow_ids    = [args.workflow_id]

    if not all([_cancel(w) for w in workflow_ids]):
        sys.exit(1)


if __name__ == '__main__':

    # create command
    parser = argparse.ArgumentParser()

    # create subcommands
    subparsers = parser.add_subparsers(help='subcommands')

    # create "list_workflows" command
    parser_listworkflows = subparsers.add_parser('list_workflows', help='list workflows')
    parser_listworkflows.add_argument('env', type=str, choices=const.ENVS, help='environment')
    parser_listworkflows.add_argument('status', type=str, choices=('active', 'closed'), help='status')
    parser_listworkflows.add_argument('hours', type=int, nargs='?', default=1, help='hours')
    parser_listworkflows.add_argument('--debug', dest='debug_enabled', action='store_true')
    parser_listworkflows.set_defaults(func=list_workflows)

    # create "list_settings" command
    parser_listsettings = subparsers.add_parser('list_settings', help='list settings')
    parser_listsettings.add_argument('env', type=str, choices=const.ENVS, default='dev', help='environment')
    parser_listsettings.set_defaults(func=list_settings)

    # create "list_domains" command
    parser_listwftypes = subparsers.add_parser('list_domains', help='list domains')
    parser_listwftypes.set_defaults(func=list_domains)

    # create "list_wftypes" command
    parser_listwftypes = subparsers.add_parser('list_wftypes', help='list workflow types')
    parser_listwftypes.add_argument('env', type=str, choices=const.ENVS, help='environment')
    parser_listwftypes.set_defaults(func=list_workflow_types)

    # create "list_acttypes" command
    parser_listacttypes = subparsers.add_parser('list_acttypes', help='list activity types')
    parser_listacttypes.add_argument('env', type=str, choices=const.ENVS, help='environment')
    parser_listacttypes.set_defaults(func=list_activity_types)

    # create "count_tasks" command
    parser_counttasks = subparsers.add_parser('count_tasks', help='count pending tasks')
    parser_counttasks.add_argument('env', type=str, choices=const.ENVS, help='environment')
    parser_counttasks.set_defaults(func=count_tasks)

    # create "terminate" command
    parser_terminate = subparsers.add_parser('terminate', help='terminate workflows')
    parser_terminate.add_argument('env', type=str, choices=const.ENVS, help='environment')
    parser_terminate.add_argument('workflow_id', type=str, help='workflow_id')
    parser_terminate.set_defaults(func=terminate)

    # create "cancel" command
    parser_cancel = subparsers.add_parser('cancel', help='request cancel workflows')
    parser_cancel.add_argument('env', type=str, choices=const.ENVS, help='environment')
    parser_cancel.add_argument('workflow_id', type=str, help='workflow_id')
    parser_cancel.set_defaults(func=cancel)

    # parse argument
    args = parser.parse_args()

    # execute command
    args.func(args)
