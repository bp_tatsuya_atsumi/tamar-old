# -*- coding: utf-8 -*-

from __future__ import absolute_import

import argparse
import gzip
import os
from   conf.config import BatchConfig
from   lib.s3util  import S3Util
import lib.util    as     util


def split(sid, env, data_type, s3bucket, s3file, split_line_num=1000000):
    config      = BatchConfig(env)
    work_dir    = os.path.join(config.tmp_tools_dir.format(tool_name='splitter'))
    logger      = _setup_logger(config, sid, data_type)
    s3util      = S3Util()
    s3dir_split = os.path.join(os.path.dirname(s3file), 'split/')

    gzfile = None
    splitted_gzfiles = []
    try:
        logger.info('<Start split>')

        logger.info('Downloading s3file: %s', os.path.join(s3bucket, s3file))
        gzfile = _download_file(s3bucket, s3file, work_dir)

        logger.info('Splitting file: %s', os.path.basename(gzfile))
        splitted_file_pattern = _get_splitted_file_pattern(gzfile)

        def _setup_file():
            splitted_gzfile = splitted_file_pattern.format(fileseqno)
            splitted_gzfiles.append(splitted_gzfile)
            logger.info('Writing split file: %s', os.path.basename(splitted_gzfile))
            return splitted_gzfile, gzip.open(splitted_gzfile, mode='wb')

        def _close_file():
            splitted_gzf.close()
            logger.info('Uploading split file: %s', os.path.basename(splitted_gzfile))
            s3util.upload_single_file(splitted_gzfile, s3bucket, s3dir_split)
            os.remove(splitted_gzfile)

        with gzip.open(gzfile) as gzf:
            fileseqno = 1
            splitted_gzfile, splitted_gzf = _setup_file()
            for i, line in enumerate(gzf):
                if i and i % split_line_num == 0:
                    _close_file()
                    fileseqno += 1
                    splitted_gzfile, splitted_gzf = _setup_file()
                splitted_gzf.write(line)
            _close_file()

    except Exception, e:
        logger.exception('Failed to split file: %s', os.path.join(s3bucket, s3file))
        raise e
    else:
        logger.info('Splitted to %d files', len(splitted_gzfiles))
        return [os.path.join(s3dir_split, os.path.basename(f)) for f in splitted_gzfiles]
    finally:
        util.remove_files([gzfile])
        util.remove_files(splitted_gzfiles)
        logger.info('<End split>')


def _setup_logger(config, sid, data_type):
    log_prefix    = '[SplitTransferFile {sid} {data_type}] '.format(
                        sid=sid,
                        data_type=data_type
                    )
    logger_suffix = '{sid}.{data_type}.split'.format(
                        sid=sid,
                        data_type=data_type
                    )
    return config.get_prefixed_transfer_logger(logger_suffix, log_prefix)

def _download_file(s3bucket, s3file, work_dir):
    s3obj = S3Util().download_single_file(s3bucket, s3file, work_dir)
    if not s3obj:
        raise Exception('S3file not exists: {}'.format(os.path.join(s3bucket, s3file)))
    return os.path.join(work_dir, os.path.basename(s3obj['key']))

def _get_splitted_file_pattern(gzfile):
    # e.g. dir/filename.csv.gz -> dir/filename_0001.csv.gz
    exts = []
    filename = os.path.basename(gzfile)
    filename_root, filename_ext = os.path.splitext(filename)
    while filename_ext:
        exts.insert(0, filename_ext)
        filename_root, filename_ext = os.path.splitext(filename_root)
    return os.path.join(os.path.dirname(gzfile),
                        filename_root + '_{:04d}' + ''.join(exts))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='split transfer file')
    parser.add_argument('sid', type=str, help='site id')
    parser.add_argument('env', type=str, choices=('prd', 'stg', 'dev'), help='environment')
    parser.add_argument('data_type', type=str, choices=('segmentdata', 'recommenddata'), help='data type')
    parser.add_argument('s3bucket', type=str, help='s3file')
    parser.add_argument('s3file', type=str, help='s3file')
    parser.add_argument('-l', '--split_line_num', type=int, default=1000000, help='split line num')
    args = parser.parse_args()

    split(args.sid,
          args.env,
          args.data_type,
          args.s3bucket,
          args.s3file,
          args.split_line_num)
