# -*- coding: utf-8 -*-

from __future__ import absolute_import

import argparse
import datetime
import os
import traceback
from   boto.utils   import parse_ts
from   conf.config  import BatchConfig
import lib.const    as     const
from   lib.s3util   import S3Util


#---------------------------------------------------------------------------
#   Constants
#---------------------------------------------------------------------------
S3DIRS = [
    '{sid}/segment_list_mst',
    '{sid}/segment_list_data',
    '{sid}/recommend_list_mst',
    '{sid}/recommend_list_data',
    '{sid}/conversion_log',
    '{sid}/mail_request_data',
    '{sid}/mail_response_data',
    '{sid}/mail_list_data'
]


#---------------------------------------------------------------------------
#   Public functions
#---------------------------------------------------------------------------
def send_tomb(sid, env, expiration_days=3):
    config = BatchConfig(env)
    logger = config.get_prefixed_transfer_logger(
                 'tombsender.{sid}'.format(sid=sid),
                  '[TombSender {sid}] '.format(sid=sid)
             )

    try:
        logger.info('<Start send tomb>')

        s3util   = S3Util()
        s3bucket = config.s3bucket_transfer_data

        for s3dir_format in S3DIRS:
            s3dir_root = s3dir_format.format(sid=sid)

            # Get file list
            s3objs = s3util.list_dir(s3bucket, s3dir_root, recursive=True)
            if not s3objs:
                logger.info('No files found: sid=%s, s3bucket=%s, s3dir=%s',
                            sid, s3bucket, s3dir_root)
                continue

            # Move expired files to tomb
            expired_file_count = 0
            for s3obj in s3objs:
                if not s3obj['is_file']:
                    continue
                last_modified = parse_ts(s3obj['last_modified'])
                if datetime.datetime.utcnow() - last_modified > datetime.timedelta(days=expiration_days):
                    s3key     = s3obj['key']
                    s3dir_src = os.path.dirname(s3key)
                    s3dir_dst = os.path.join('tomb', s3dir_src)

                    expired_file_count += 1
                    s3util.move_single_file(s3bucket, s3key, s3bucket, s3dir_dst)

            logger.info('%d files send to tomb: sid=%s, s3bucket=%s, s3dir=%s',
                        expired_file_count, sid, s3bucket, s3dir_root)

    except:
        logger.exception('Failed to send tomb: laststacktrace="%s"',
                         traceback.format_exc().splitlines()[-1])
        raise
    finally:
        logger.info('<End send tomb>')


#---------------------------------------------------------------------------
#   Commandline function
#---------------------------------------------------------------------------
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='tomb sender')
    parser.add_argument('sid', type=str, help='site id')
    parser.add_argument('env', type=str, choices=const.ENVS, help='environment')
    parser.add_argument('expiration_days', type=int, nargs='?', default=3, help='expiration days')
    args = parser.parse_args()

    send_tomb(args.sid, args.env, args.expiration_days)
