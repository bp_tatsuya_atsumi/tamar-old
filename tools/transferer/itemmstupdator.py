# -*- coding: utf-8 -*-

from __future__ import absolute_import

import argparse
import csv
from   collections              import OrderedDict
from   conf.config              import BatchConfig
import gzip
import lib.const                as     const
from   lib.s3util               import S3Util
from   lib.transferer.exception import NoS3Files
import lib.transferer.rtfile    as     rtfile
import lib.util                 as     util
import os
import shutil
import sys
import traceback

# FIXME 処理改善するまでの暫定対応
csv.field_size_limit(sys.maxint)

#---------------------------------------------------------------------------
#   Constants
#---------------------------------------------------------------------------
S3DIR_RAW         = '{sid}/item_mst/raw/'
S3FILENAME_RAW    = 'item_mst_{datetime}_{sid}.csv.gz'

S3DIR_LATEST      = '{sid}/item_mst/latest/'
S3FILENAME_LATEST = 'item_mst_latest_{datetime}_{sid}.csv.gz'
FIELDNAMES_LATEST = [
    'pid', 'sid', 'code', 'id', 'name', 'img_url', 'link_url', 'price',
    'description', 'status', 'last_modified_date'
]


#---------------------------------------------------------------------------
#   Public functions
#---------------------------------------------------------------------------
def update(sid, env, s3bucket, s3file):
    config   = BatchConfig(env)
    logger   = config.get_prefixed_transfer_logger(
                   'itemmstupdator.{sid}'.format(sid=sid),
                   '[ItemMstUpdate {sid}] '.format(sid=sid)
               )

    logger.info('<Start update itemmst>')
    work_dir = os.path.join(config.tmp_tools_dir.format(tool_name='itemmstupdator'), sid)
    func_s3get, func_s3getlatest, func_s3put = _create_s3funcs(s3bucket, work_dir)

    try:
        if sid == '0009':
            logger.info('Skipped because rtoaster send all records')
            return

        logger.info('Initializing work dirctory')
        _initialize_directory(work_dir)

        logger.info('Downloading raw file')
        raw_file = func_s3get(s3file)
        if not raw_file:
            raise NoS3Files(s3bucket, s3file)
        latest_file_new = raw_file.replace('item_mst', 'item_mst_latest')

        logger.info('Downloading latest file')
        s3dir_latest = S3DIR_LATEST.format(sid=sid)
        latest_file_old = func_s3getlatest(s3dir_latest)
        if not latest_file_old:
            latest_file_old = os.path.join(work_dir, S3FILENAME_LATEST.format(datetime='00000000000000', sid=sid))
            gzip.open(latest_file_old, 'wb').close()

        logger.info('raw_file=%s', os.path.basename(raw_file))
        logger.info('latest_file_old=%s', os.path.basename(latest_file_old))
        logger.info('latest_file_new=%s', os.path.basename(latest_file_new))

        # Read raw files and store
        logger.info('Reading raw files')
        records_raw = OrderedDict([(row['code'], row) for row in rtfile.open_itemmst(raw_file)])

        # Write latest file
        logger.info('Writing latest file')
        with gzip.open(latest_file_new, mode='w') as csvgzf:
            writer = csv.DictWriter(csvgzf, FIELDNAMES_LATEST, lineterminator='\n')
            writer.writeheader()

            for code, row in records_raw.items():
                row['id'] = util.code_to_long_id(code)
                writer.writerow(row)
            for row in rtfile.open_itemmst(latest_file_old):
                if not row['code'] in records_raw:
                    writer.writerow(row)

        logger.info('Uploading latest file')
        func_s3put(latest_file_new, s3dir_latest)

    except NoS3Files, e:
        logger.exception('File not exists in s3: bucket=%s, path=%s',
                         e.s3bucket, e.s3path)
        raise e
    except:
        logger.exception('Failed to update itemmst: laststacktrace="%s"',
                         traceback.format_exc().splitlines()[-1])
        raise
    finally:
        logger.info('<End update itemmst>')


#---------------------------------------------------------------------------
#   Private functions
#---------------------------------------------------------------------------
def _create_s3funcs(s3bucket, work_dir):
    s3util = S3Util()

    def _download(s3file):
        o = s3util.download_single_file(s3bucket, s3file, work_dir)
        if not o:
            return None
        return os.path.join(work_dir, os.path.basename(o['key']))

    def _download_latest(s3dir):
        o = s3util.download_single_file_order_by_filename(s3bucket, s3dir, work_dir)
        if not o:
            return None
        return os.path.join(work_dir, os.path.basename(o['key']))

    def _upload(localfile, s3dir):
        s3util.upload_single_file(localfile, s3bucket, s3dir)

    return _download, _download_latest, _upload

def _initialize_directory(dir):
    shutil.rmtree(dir, ignore_errors=True)
    if not os.path.exists(dir):
        os.makedirs(dir)


#---------------------------------------------------------------------------
#   Commandline function
#---------------------------------------------------------------------------
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Update latest itemmst')
    parser.add_argument('sid', type=str, help='site_id')
    parser.add_argument('env', type=str, choices=const.ENVS, help='environment')
    parser.add_argument('s3bucket', type=str, help='s3bucket')
    parser.add_argument('s3file', type=str, help='s3file')
    args = parser.parse_args()

    update(args.sid, args.env, args.s3bucket, args.s3file)
