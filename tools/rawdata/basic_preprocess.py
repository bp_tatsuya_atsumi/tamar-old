# -*- coding: utf-8 -*-

import argparse
import os
import tarfile
import datetime
import json
import csv

IDX_UNIXTIMESTAMP = 1
IDX_RT_UID = 16

START_DATETIME = '2013-09-01 00:00:00'
END_DATETIME   = '2013-10-31 23:59:59'

def _extract_from_tar(tar_name, work_path):
    tar = tarfile.open(os.path.join(work_path, tar_name))
    contents_name = [member.name for member in tar.getmembers()]
    tar.extractall(work_path)
    tar.close()
    return contents_name

def __transform_rtuid_column(rtuid_column):
    rtuid_column = rtuid_column.replace('{', '[')
    rtuid_column = rtuid_column.replace('}', ']')
    rtuid_column = rtuid_column.replace('(', '"')
    rtuid_column = rtuid_column.replace(')', '"')
    return json.loads(rtuid_column)
    

def _process_line(line):
    line = line.rstrip()
    columns = line.split('\t')
    
    result = []
    if __timestamp_checker(START_DATETIME, END_DATETIME, columns[IDX_UNIXTIMESTAMP]):
        rt_uids = columns[IDX_RT_UID]
        columns[IDX_UNIXTIMESTAMP] = __datetime_to_stime(
                                        __timestamp_to_datetime(
                                            int(columns[IDX_UNIXTIMESTAMP])
                                        )
                                    )
        for rt_uid in __transform_rtuid_column(rt_uids):
            columns[IDX_RT_UID]        = rt_uid
            result.append(columns)
    return result
                

def __datetime_to_stime(date):
    return date.strftime('%Y-%m-%d %H:%M:%S')

def __stime_to_datetime(stime):
    return datetime.datetime.strptime(stime, '%Y-%m-%d %H:%M:%S')

def __timestamp_to_datetime(unixtimestamp):
    return datetime.datetime.fromtimestamp(unixtimestamp)

def __timestamp_checker(start_stime, end_stime, stimestamp):
    start_time  = __stime_to_datetime(start_stime)
    end_time    = __stime_to_datetime(end_stime)
    target_time = __timestamp_to_datetime(int(stimestamp))
    if start_time <= target_time and target_time <= end_time:
        return True
    else:
        return False

def _create_csv_writer(work_path, filename):
    return csv.writer(open(os.path.join(work_path, filename), 'w'),
                      delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)

def normalization(rawdata_path, work_path):
    ORDER_RAWDATA      = 'order.csv'
    CLICK_RAWDATA      = 'click.csv'
    CONVERSION_RAWDATA = 'conversion.csv'

    order_csv      = _create_csv_writer(work_path, ORDER_RAWDATA)
    click_csv      = _create_csv_writer(work_path, CLICK_RAWDATA)
    conversion_csv = _create_csv_writer(work_path, CONVERSION_RAWDATA)

    for file in sorted(os.listdir(rawdata_path)):
        contents_name = _extract_from_tar(os.path.join(rawdata_path, file), work_path)
        for rawdata_name in contents_name:
            print rawdata_name
            fp = open(os.path.join(work_path, rawdata_name))
            if 'order' in rawdata_name:
                global IDX_RT_UID
                IDX_RT_UID = 16
                for line in fp:
                    normalized_lines = _process_line(line)
                    if normalized_lines:
                        for normalized_line in normalized_lines:
                            order_csv.writerow(normalized_line)
            elif 'click' in rawdata_name:
                global IDX_RT_UID
                IDX_RT_UID = 16
                for line in fp:
                    normalized_lines = _process_line(line)
                    if normalized_lines:
                        for normalized_line in normalized_lines:
                            click_csv.writerow(normalized_line)
            elif 'conversion' in rawdata_name:
                global IDX_RT_UID
                IDX_RT_UID = 8
                for line in fp:
                    normalized_lines = _process_line(line)
                    if normalized_lines:
                        for normalized_line in normalized_lines:
                            conversion_csv.writerow(normalized_line)
            else:
                pass
            fp.close()
            os.remove(os.path.join(work_path, rawdata_name))
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('rawdata_path', type=str, help='absolute path to rawdata directory')
    parser.add_argument('work_path', type=str, help='absolute path to working directory')
    args = parser.parse_args()
    
    if not os.path.exists(args.work_path):
        os.mkdir(args.work_path)

    normalization(args.rawdata_path, args.work_path)
