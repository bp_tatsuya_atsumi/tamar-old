# -*- coding: utf-8 -*-

from __future__ import absolute_import

"""
rawdataの生成管理を行うクラス
"""

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'development'
__version__ = ''
__date__    = ''

import argparse
from   conf.config                import BatchConfig
import csv
import datetime
import lib.const                  as     const
from   lib.rawdata.rawdata_header import RawdataHeader
from   lib.s3util                 import S3Util
from   lib.transferer.exception   import NoS3Files
import os
import shutil
import tarfile

class CreateSpotRawdata(object):

    def __init__(self, sid, env, data_type, s3bucket, start_date, end_date):
        self.config = BatchConfig(env)
        self.env    = self.config.environment
        self.sid    = sid

        self.s3util              = S3Util()
        self.s3_transfer_bucket  = s3bucket
        self.s3_spot_rawdata_dir = 'rawdata/spot/{sid}'.format(sid=self.sid)
        self.local_base_dir      = self.config.tmp_converted_rawdata_dir

        self.str_start_date = start_date
        self.str_end_date   = end_date

        self.start_date = datetime.datetime.strptime(start_date, '%Y%m%d').date()
        self.end_date   = datetime.datetime.strptime(end_date,   '%Y%m%d').date()

        self.decompressed_rawdata_path_list = []

        self.logger    = self.config.logger_rawdata
        self.logprefix = '[Rawdata {start_date}_{end_date}] '.format(start_date=self.start_date,
                                                                     end_date=self.end_date)

        self.rawdata_header             = RawdataHeader()
        self.ORDER_AND_CLICK_CSV_HEADER = self.rawdata_header.STR_ORDER_AND_CLICK_HEADER
        self.CONVERSION_CSV_HEADER      = self.rawdata_header.STR_CONVERSION_HEADER

    def create(self):
        self._initialize_tmp_dir()
        self._get_converted_rawdata_files_from_s3()
        self._merge_rawdata_files()
        self._compress_spot_rawdata()
        self._upload_spot_rawdata()

    def _initialize_tmp_dir(self):
        shutil.rmtree(self.local_base_dir, ignore_errors=True)
        os.makedirs(self.local_base_dir)

    def _convert_date_to_s3_rawdata_path(self, my_date):
        str_date        = my_date.strftime('%Y%m%d')
        s3_rawdata_path = 'rawdata/converted/{sid}/{sid}_{str_date}.tgz'.format(sid=self.sid,
                                                                                str_date=str_date)
        return s3_rawdata_path

    def _get_converted_rawdata_files_from_s3(self):
        my_date = self.start_date
        while my_date <= self.end_date:
            s3_rawdata_path = self._convert_date_to_s3_rawdata_path(my_date)
            if self.s3util.exists(self.s3_transfer_bucket, s3_rawdata_path):
                self._get_converted_rawdata_file_from_s3(s3_rawdata_path)
            my_date += datetime.timedelta(days=1)

    def _get_converted_rawdata_file_from_s3(self, s3_rawdata_path):
        s3obj = self.s3util.download_single_file(self.s3_transfer_bucket, s3_rawdata_path, self.local_base_dir)
        if not s3obj:
            raise NoS3Files(self.s3_transfer_bucket, s3_rawdata_path)
        s3_rawdata_basename = os.path.basename(s3_rawdata_path)
        local_rawdata_path  = '{path}/{name}'.format(path=self.local_base_dir, name=s3_rawdata_basename)
        with tarfile.open(local_rawdata_path, 'r:gz') as tar:
            tar.extractall(path=self.local_base_dir)
            tar.close()

        decompressed_rawdata_names = [member.name for member in tarfile.open(local_rawdata_path).getmembers() if member.isfile()]
        self.decompressed_rawdata_path_list.extend(map(lambda x: os.path.join(self.local_base_dir, x), decompressed_rawdata_names))

    def _merge_rawdata_files(self):
        self.spot_rawdata_path_list = []
        for rawdata_type in ('order', 'click', 'conversion'):
            rawdata_path = '{path}/{rawdata_type}_{sid}_{start_date}_{end_date}.csv'.format(path=self.local_base_dir,
                                                                                            rawdata_type=rawdata_type,
                                                                                            sid=self.sid,
                                                                                            start_date=self.str_start_date,
                                                                                            end_date=self.str_end_date)
            self.spot_rawdata_path_list.append(rawdata_path)

        with open(self.spot_rawdata_path_list[0], 'w')    as fp_order, \
                open(self.spot_rawdata_path_list[1], 'w') as fp_click, \
                open(self.spot_rawdata_path_list[2], 'w') as fp_conversion:

            fp_order.write(self.ORDER_AND_CLICK_CSV_HEADER + '\n')
            fp_click.write(self.ORDER_AND_CLICK_CSV_HEADER + '\n')
            fp_conversion.write(self.CONVERSION_CSV_HEADER + '\n')

            for decompressed_rawdata_path in self.decompressed_rawdata_path_list:
                with open(decompressed_rawdata_path, 'r') as fp_decompressed:
                    rawdata_type = self.__detect_rawdata_type(decompressed_rawdata_path)
                    if   rawdata_type == 'order':      fp_spot = fp_order
                    elif rawdata_type == 'click':      fp_spot = fp_click
                    elif rawdata_type == 'conversion': fp_spot = fp_conversion
                    fp_decompressed.next()
                    for line in fp_decompressed:
                        fp_spot.write(line)

    def _compress_spot_rawdata(self):
        local_tar_file_path = '{base_path}/{sid}_{start_date}_{end_date}.tgz'.format(base_path=self.local_base_dir,
                                                                                     sid=self.sid,
                                                                                     start_date=self.str_start_date,
                                                                                     end_date=self.str_end_date)

        with tarfile.open(local_tar_file_path, 'w:gz') as tar_file:
            for rawdata_path in self.spot_rawdata_path_list:
                tar_file.add(rawdata_path, arcname=os.path.basename(rawdata_path))
        self.local_tar_file_path = local_tar_file_path

    def _upload_spot_rawdata(self):
        self.s3util.upload_single_file(self.local_tar_file_path, self.s3_transfer_bucket, self.s3_spot_rawdata_dir)

    def __detect_rawdata_type(self, rawdata_path):
        rawdata_basename = os.path.basename(rawdata_path)
        if   rawdata_basename.startswith('order'):      return 'order'
        elif rawdata_basename.startswith('click'):      return 'click'
        elif rawdata_basename.startswith('conversion'): return 'conversion'

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='spot rawdata creator')
    parser.add_argument('sid', type=str, help='site id')
    parser.add_argument('env', type=str, choices=const.ENVS, help='environment')
    parser.add_argument('data_type', type=str, choices=const.RECEIVE_DATATYPES, help='environment')
    parser.add_argument('s3bucket', type=str, help='environment')
    parser.add_argument('start_date', type=str, help='start_date')
    parser.add_argument('end_date', type=str, help='end_date')
    args = parser.parse_args()

    creator = CreateSpotRawdata(sid=args.sid,
                                env=args.env,
                                data_type=args.data_type,
                                s3bucket=args.s3bucket,
                                start_date=args.start_date,
                                end_date=args.end_date)
    creator.create()
