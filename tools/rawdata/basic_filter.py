# -*- coding: utf-8 -*-

import argparse
import csv
import os

# order or click
TARGET_ORDER_ID = ['1585', '1586', '1587', '1588', '1589', '1590', '1591', \
                   '1592', '1639', '1640', '1641', '1642', '1643', '2045']
IDX_ORDER_ID = 6
IDX_RT_UID   = 16

# conversion
TARGET_PIXEL_ID = ['2374']
IDX_PIXEL_ID = 0

def filter(cookie_list_path, target_data_path):
    cookie_list = _get_cookie_list(cookie_list_path)

    csv_reader  = csv.reader(open(target_data_path))
    csv_writer  = csv.writer(open(_create_output_filepath(target_data_path), 'w'),
                             delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)

    if 'conversion' in target_data_path:
        global IDX_RT_UID
        IDX_RT_UID = 8

    for row in csv_reader:
        if row[IDX_RT_UID] in cookie_list \
                and _is_target_client(row):
           csv_writer.writerow(row) 

def _get_cookie_list(cookie_list_path):
    rt_uid_list = []
    for line in open(cookie_list_path):
        line = line.rstrip()
        rt_uid_list.append(line)
    return rt_uid_list

def _create_output_filepath(target_data_path):
    dirname  = os.path.dirname(target_data_path)
    filename = os.path.basename(target_data_path)

    new_filename = 'filtered_' + filename

    return os.path.join(dirname, new_filename)

def _is_target_client(row):
    if len(row) == 17:
        return row[IDX_ORDER_ID] in TARGET_ORDER_ID
    else:
        return row[IDX_PIXEL_ID] in TARGET_PIXEL_ID
        
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('cookie_list_path', type=str, help='cookie list path')
    parser.add_argument('target_data_path', type=str, help='target data path')

    args = parser.parse_args()

    filter(args.cookie_list_path, args.target_data_path)
