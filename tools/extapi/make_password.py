# -*- coding: utf-8 -*-

"""
ランダムなパスワードとそのハッシュ値を返すプログラム
"""

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'production'

from passlib.apps import custom_app_context as pwd_context
import string
import random

pw_length = 16
password = ''.join([random.choice(string.ascii_letters + string.digits) for i in range(pw_length)])
hashed_password = pwd_context.encrypt(password)
print password
print hashed_password
