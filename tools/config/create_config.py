# -*- coding: utf-8 -*-

from __future__ import absolute_import

"""
mapping.jsonからSQLファイルを生成するクラス
"""

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'development'
__version__ = ''
__date__    = ''

import argparse
from   conf.config import BaseConfig
import lib.const as const
import os


class CreateConfig(object):

    def __init__(self, env, sid):
        self.env = env
        self.sid = sid

        config           = BaseConfig(self.env)
        self.mapping     = config.mapping
        self.partner_mst = config.partner_mst

    def create(self):
        query = self._get_query()

        sql_file_path = self._get_sql_file_path()

        self._create_new_sql_file(query, sql_file_path)

    def _get_query(self):
        query = self._get_str_sql_start()

        pids           = self._get_pids()
        unmapping_pids = self._get_unmapping_pids()
        for pid in pids:
            if pid in unmapping_pids:
                continue
            elif pid == 'co':
                query += self._get_str_sql_mapping_co()
            elif pid in ('bp1', 'go', 'im'):
                query += self._get_str_sql_mapping_with_segmentmst(pid)
            else:
                query += self._get_str_sql_mapping(pid)

        for to_bridge in self._get_bridge_sids():
            query += self._get_str_sql_bridge(to_bridge)

        to_sid = 'rt'+str(self.sid)
        for data_source in self._get_data_sources():
            query += self._get_str_sql_data_source(to_sid, data_source)

        query += self._get_str_sql_stop()

        return query

    def _get_pids(self):
        return sorted(self.mapping[self.sid].keys())

    def _get_unmapping_pids(self):
        unmapping_pids = []
        for pid, pid_attrs in self.partner_mst.iteritems():
            if pid_attrs.get('option', {}).get('no_mapping', False):
                unmapping_pids.append(pid)
        return sorted(unmapping_pids)

    def _get_bridge_sids(self):
        return sorted(self.mapping[self.sid].get('rt', {}).get('bridges',[]))

    def _get_data_sources(self):
        return sorted(self.mapping[self.sid].get('rt', {}).get('receives', []))

    def _get_str_sql_mapping(self, pid):
        query = self._read_file('tools/config/template_sql/mapping.sql')
        return query.format(sid=self.sid, pid=pid)

    def _get_str_sql_mapping_co(self):
        query = self._read_file('tools/config/template_sql/mapping_co.sql')
        return query.format(sid=self.sid)

    def _get_str_sql_mapping_with_segmentmst(self, pid):
        query = self._read_file('tools/config/template_sql/mapping_with_segmentmst.sql')
        return query.format(sid=self.sid, pid=pid)

    def _get_str_sql_bridge(self, to_bridge):
        query = self._read_file('tools/config/template_sql/bridge.sql')
        return query.format(from_sid=self.sid, to_bridge=to_bridge)

    def _get_str_sql_data_source(self, to_sid, data_source):
        query = self._read_file('tools/config/template_sql/data_source.sql')
        return query.format(sid=self.sid, to_sid=to_sid, data_source=data_source)

    def _get_str_sql_start(self):
        query = self._read_file('tools/config/template_sql/start.sql')
        return query.format(sid=self.sid)

    def _get_str_sql_stop(self):
        query = self._read_file('tools/config/template_sql/stop.sql')
        return query.format(sid=self.sid)

    def _get_sql_file_path(self):
        sql_dir_name  = 'settings/database/'
        sql_file_name = 'DB_TAMAR_{sid}.sql'.format(sid=self.sid)
        sql_file_path = os.path.join(sql_dir_name, sql_file_name)
        return sql_file_path

    def _create_new_sql_file(self, query, sql_file_path):
        print "Sql file path:", sql_file_path
        with open(sql_file_path, 'w') as f:
            f.write(query)

    def _read_file(self, file_name):
        with open(file_name, 'r') as f:
            buf = f.read()
        return buf

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='create config')
    parser.add_argument('env', type=str, choices=const.ENVS, help='environment')
    parser.add_argument('sid', type=str, help='site id')
    args = parser.parse_args()

    creator = CreateConfig(env=args.env, sid=args.sid)
    creator.create()
