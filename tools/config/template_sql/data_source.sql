--
-- Table structure for table `t_segmentmst_from_{data_source}_to_{to_sid}`
--
CREATE TABLE IF NOT EXISTS `t_segmentmst_from_{data_source}_to_{to_sid}` (
  `segment_id_from` varchar(255) NOT NULL COMMENT 'ユーザID',
  `segment_id_to` varchar(255) NOT NULL COMMENT 'パートナーユーザID',
  `name` varchar(255) NOT NULL COMMENT 'セグメント名',
  `description` varchar(1000) DEFAULT NULL COMMENT '備考',
  `status` tinyint NOT NULL DEFAULT 0 COMMENT 'ステータス',
  PRIMARY KEY (`segment_id_from`,`segment_id_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='セグメントマスタ';

--
-- Table structure for table `t_user_extsegdata_{sid}_{data_source}`
--
CREATE TABLE IF NOT EXISTS `t_user_extsegdata_{sid}_{data_source}` (
  `user_id` varchar(255) NOT NULL,
  `segment_id` varchar(255) DEFAULT NULL,
  `ttl` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`,`segment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `w_user_extsegdata_{sid}_{data_source}`
--
CREATE TABLE IF NOT EXISTS `w_user_extsegdata_{sid}_{data_source}` (
  `user_id` varchar(255) NOT NULL,
  `segment_id` varchar(255) DEFAULT NULL,
  `ttl` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


