# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import print_function

import argparse
import traceback
import os
import shutil
import time
import MySQLdb
from   conf.config import BatchConfig
from   lib.s3util  import S3Util
import lib.util    as     util


SQL_LOAD_CSV = '''
LOAD DATA LOCAL INFILE '{file}'
    REPLACE
    INTO TABLE `{mapping_table}`
    FIELDS
        TERMINATED BY '\t'
    LINES
        TERMINATED BY '\n'
    (
        @updated_at_uxt
    ,   partner_user_id
    ,   @partner_sid
    ,   @unknown_test
    ,   user_id
    )
    SET
        tamar_user_type = '1'
    ,   tamar_user_id   = user_id
    ,   inserted_at     = FROM_UNIXTIME(@updated_at_uxt)
    ,   updated_at      = FROM_UNIXTIME(@updated_at_uxt)
    ,   is_deleted      = FALSE
'''

def confirm_execution(sid, env, s3file, split_line_num=1000000):
    print('sid            : {0}'.format(args.sid))
    print('env            : {0}'.format(args.env))
    print('s3file         : {0}'.format(args.s3file))
    print('split_line_num : {0}'.format(args.split_line_num))
    print ('')
    confirm = raw_input('OK? [y/n] ')
    if confirm == 'y':
        print('')
        execute(sid, env, s3file, split_line_num)

def execute(sid, env, s3file, split_line_num=1000000):
    try:
        # load config
        config = _load_config(sid, env)

        # prepare
        work_dir           = _initialize_work_directory(config, sid)
        tsvgz_file         = _download_file(config.s3bucket_partner_log, s3file, work_dir)
        tsv_file           = _gunzip_file(tsvgz_file)
        splitted_tsv_files = _split_file(tsv_file, split_line_num)

        # load files
        _load_files(sid, config.database[sid], splitted_tsv_files)

        # clear
        _initialize_work_directory(config, sid)

    except Exception as e:
        traceback.print_exc()
        raise e

def _log(process_name):
    def __log(func):
        def ___log(*args, **kwargs):
            print('{0} start'.format(process_name))
            st = time.time()
            result = func(*args, **kwargs)
            print('{} end: {:f} [sec]'.format(process_name, time.time() - st))
            return result
        return ___log
    return __log

@_log('load config')
def _load_config(sid, env):
    config = BatchConfig(env)
    if not sid in config.database:
        raise ValueError('database config not exists: ' + sid)
    return config

@_log('initialize work directory')
def _initialize_work_directory(config, sid):
    work_dir = os.path.join(config.tmp_tools_dir.format(tool_name='migration_so'), sid)
    shutil.rmtree(work_dir, ignore_errors=True)
    os.makedirs(work_dir)
    return work_dir

@_log('download file')
def _download_file(s3bucket, s3file, work_dir):
    s3obj = S3Util().download_single_file(s3bucket, s3file, work_dir)
    if not s3obj:
        raise Exception('s3file not exists')
    return os.path.join(work_dir, os.path.basename(s3obj['key']))

@_log('gunzip file')
def _gunzip_file(tsvgz_file):
    return util.gunzip_file(tsvgz_file)

@_log('sort file')
def _sort_file(tsv_file):
    sorted_tsv_file = tsv_file + '_sorted'
    cmd = 'sort -t "\t" -k 5,5 -k 2,2 -o {output_file} {input_file}'
    cmd = cmd.format(input_file=tsv_file, output_file=sorted_tsv_file)
    if os.system(cmd) != 0:
        raise Exception('sort failed')
    return sorted_tsv_file

@_log('split file')
def _split_file(tsv_file, split_line_num):
    splitted_tsv_file_prefix = tsv_file + '_splitted.'
    cmd = 'split -l {split_line_num} -a 4 -d {input_file} {output_file_prefix}'
    cmd = cmd.format(split_line_num=split_line_num,
                     input_file=tsv_file,
                     output_file_prefix=splitted_tsv_file_prefix)
    if os.system(cmd) != 0:
        raise Exception('split failed')
    work_dir = os.path.dirname(tsv_file)
    splitted_tsv_files = [os.path.join(work_dir, f)
                            for f in os.listdir(work_dir) if '_splitted.' in f]
    print('    splitted to {:d} files'.format(len(splitted_tsv_files)))
    return splitted_tsv_files

@_log('load files')
def _load_files(sid, dbconfig, tsv_files):
    conn = MySQLdb.connect(host=dbconfig['host'],
                           port=int(dbconfig['port']),
                           user=dbconfig['user'],
                           passwd=dbconfig['password'],
                           db=dbconfig['db_name'])
    cur = conn.cursor()
    try:
        cur.execute("SET TIME_ZONE = '+9:00'")

        ins_count = 0
        mapping_table = 't_user_mapping_{sid}_so'.format(sid=sid)
        for idx, tsv_file in enumerate(tsv_files):
            print('    {:s} -> {:s} [{:d} of {:d}]'.format(tsv_file,
                                                           mapping_table,
                                                           idx + 1,
                                                           len(tsv_files)))
            query = SQL_LOAD_CSV.format(file=tsv_file,
                                        mapping_table=mapping_table)
            ins_count +=  cur.execute(query)
            conn.commit()
        print('    {:d} lines loaded'.format(ins_count))

    except MySQLdb.Error, e:
        traceback.print_exc()
        conn.rollback()
        raise e
    finally:
        conn.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='migrate scaleout mapping table')
    parser.add_argument('sid', type=str, help='site id')
    parser.add_argument('env', type=str, choices=('prd', 'stg', 'dev'), help='environment')
    parser.add_argument('s3file', type=str, help='s3file')
    parser.add_argument('-l', '--split_line_num', type=int, default=1000000, help='split line num')
    args = parser.parse_args()

    confirm_execution(args.sid, args.env, args.s3file, args.split_line_num)
