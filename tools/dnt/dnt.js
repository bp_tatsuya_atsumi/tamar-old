
function createCoven(sid) {
    if (typeof (Rtoaster) === "undefined") {
        return;
    }
    var CrossOven = {
        host        : 'p-rt.c-ovn.jp',
        optout_path : '/rt_optout',
        optin_path  : '/rt_optin',
        timeout     : 3000,
        DNT_KEY     : '_rt.dnt'
    };
    CrossOven.Cookie = (function(CO) {
        var T = {};
        var send_beacon
        T.getDNT = function() {
            return Rtoaster.Cookie.get(CO.DNT_KEY);
        };
        T.setDNT = function() {
            var date = new Date();
            date.setTime( date.getTime() + 365*24*60*60*1000 );
            var obj = {
                key     : CO.DNT_KEY,
                value   : '1',
                expires : date.toGMTString(),
                path    : '/'
            }
            Rtoaster.Cookie.set(obj);
            var img    = new Image();
            img.src = 'http://' + CO.host + CO.optout_path
                      + '?sid=' + Rtoaster._E(sid)
                      + '&uid=' + Rtoaster._E(Rtoaster.Cookie.get('_rt.uid'))
                      + '&i='   + Math.random();
        };
        T.removeDNT = function() {
            var obj = {
                key: CO.DNT_KEY,
                path    : '/'
            };
            Rtoaster.Cookie.remove(obj);
            var img    = new Image();
            img.src = 'http://' + CO.host + CO.optin_path
                      + '?sid=' + Rtoaster._E(sid)
                      + '&uid=' + Rtoaster._E(Rtoaster.Cookie.get('_rt.uid'))
                      + '&i='   + Math.random();
        }
        return T;
    })(CrossOven);
    return CrossOven;
}
