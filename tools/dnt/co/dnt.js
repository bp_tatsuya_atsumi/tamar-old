
function createCoven() {
    if (typeof (Rtoaster) === "undefined") {
        return;
    }
    var CrossOven = {
        host        : 'p-co.c-ovn.jp',
        optout_path : '/co_optout',
        optin_path  : '/co_optin',
        timeout     : 3000,
        DNT_KEY     : 'co.dnt'
    };
    CrossOven.Cookie = (function(CO) {
        var T = {};
        var send_beacon
        T.getDNT = function() {
            return Rtoaster.Cookie.get(CO.DNT_KEY);
        };
        T.setDNT = function() {
            var date = new Date();
            date.setTime( date.getTime() + 365*24*60*60*1000 );
            var obj = {
                key     : CO.DNT_KEY,
                value   : '1',
                expires : date.toGMTString(),
                domain  : 'c-ovn.jp',
                path    : '/'
            }
            Rtoaster.Cookie.set(obj);
            var img    = new Image();
            img.src = 'http://' + CO.host + CO.optout_path
                      + '?uid=' + Rtoaster._E(Rtoaster.Cookie.get('co.uid'))
                      + '&i='   + Math.random();
        };
        T.removeDNT = function() {
            var obj = {
                key     : CO.DNT_KEY,
                domain  : 'c-ovn.jp',
                path    : '/'
            }
            Rtoaster.Cookie.remove(obj);
            var img    = new Image();
            img.src = 'http://' + CO.host + CO.optin_path
                      + '?uid=' + Rtoaster._E(Rtoaster.Cookie.get('co.uid'))
                      + '&i='   + Math.random();
        }
        return T;
    })(CrossOven);
    return CrossOven;
}
