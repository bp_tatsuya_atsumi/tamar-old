# -*- coding: utf-8 -*-

from __future__ import absolute_import

import argparse
import traceback
import MySQLdb
from   conf.config import BatchConfig
import lib.const   as     const
from   lib.sesutil import SESUtil


#---------------------------------------------------------------------------
#   Public functions
#---------------------------------------------------------------------------
def send_report(env):
    config = BatchConfig(env)
    logger = _get_logger(config)

    try:
        logger.info('<Start mapping report>')

        logger.info('Creating mail subject')
        mail_subject = _create_mail_subject(config)
        logger.debug(mail_subject)

        logger.info('Creating mail body')
        mail_body = _create_mail_body(config)
        logger.debug(mail_body)

        logger.info('Sending mail')
        SESUtil(env).send_mail(mail_subject, mail_body)

    except:
        logger.exception('Failed to report mapping results: laststacktrace="%s"',
                         traceback.format_exc().splitlines()[-1])
        raise
    finally:
        logger.info('<End mapping report>')


#---------------------------------------------------------------------------
#   Private functions
#---------------------------------------------------------------------------
def _get_logger(config):
    return config.get_prefixed_mapping_logger(__name__, '[MappingReport] ')

def _create_mail_subject(config):
    return u'マッピング結果レポート[{env}]'.format(env=config.environment)

def _create_mail_body(config):
    TEMPLATE_MAIL_BODY = u"""
マッピング情報: {client_info}
データベース名: {db}
テーブル名: {table}
マッピング実績：{sync}

"""

    MAPPING_TABLE = 't_user_mapping_{sid}_{pid}'

    SQL_SELECT_SYNC_ACTUAL_VALUE = """
SELECT
    table_schema,
    table_name,
    table_rows
FROM
    information_schema.tables
WHERE
    table_schema = '{db_name}'
        AND
    table_name = '{mapping_table}'
"""

    def retrieve_mapping_info(sid, pid, db_config, site_config, mapping):
        try:
            # connect database
            conn = MySQLdb.connect(host=db_config['host'],
                                   port=int(db_config['port']),
                                   user=db_config['user'],
                                   passwd=db_config['password'],
                                   db=db_config['db_name'])
            cur = conn.cursor()

            # determine table name
            mapping_table = MAPPING_TABLE.format(sid=sid, pid=pid)

            query = SQL_SELECT_SYNC_ACTUAL_VALUE.format(
                        db_name=db_config['db_name'],
                        mapping_table=mapping_table
                    )

            cur.execute(query)

            row = cur.fetchone()
            if row:
                if pid.startswith('rt'):
                    partner_name = partner_config['name_jp'] + '[{pid}]'.format(pid=pid)
                else:
                    partner_name = partner_config['name_jp']
                key = u'{site_name} <=> {partner_name}'.format(
                          site_name=site_config['name_jp'], partner_name=partner_name
                      )
                mail_bodys.append(
                    TEMPLATE_MAIL_BODY.format(client_info=key,
                                              db=row[0],
                                              table=row[1],
                                              sync=int(row[2]))
                )
        except MySQLdb.Error:
            logger.exception('Database Error: laststacktrace="%s"',
                             traceback.format_exc().splitlines()[-1])
        finally:
            try:
                conn.close()
            except:
                pass

    # main
    logger = _get_logger(config)

    mail_bodys = []
    for sid in sorted(config.site_mst.keys()):
        db_config   = config.database.get(sid)
        site_config = config.site_mst.get(sid)
        mapping     = config.mapping.get(sid)

        if not (db_config and site_config and mapping):
            continue

        for pid in sorted(mapping.keys()):
            partner_config = config.partner_mst.get(pid)
            if not partner_config:
                continue

            pids = []
            bridges = mapping.get(pid).get('bridges')
            if pid == 'rt':
                if bridges:
                    pids = bridges
                else:
                    continue
            else:
                pids.append(pid)

            for pid in pids:
                try:
                    retrieve_mapping_info(sid, pid, db_config, site_config, mapping)
                except MySQLdb.Error:
                    continue

    return ''.join(mail_bodys)


#---------------------------------------------------------------------------
#   Commandline function
#---------------------------------------------------------------------------
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Send mapping actual value from mapping table')
    parser.add_argument('env', type=str, choices=const.ENVS, help='environment')
    args = parser.parse_args()

    send_report(args.env)
