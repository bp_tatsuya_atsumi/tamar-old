# -*- coding: utf-8 -*-

"""
外部からのセグメントデータに対する操作を処理するAPI
"""

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'production'

from   flask                  import Flask, jsonify
from   flask.ext.restful      import reqparse
import json
from   lib.const              import DATASOURCE
from   lib.extapi.base        import ExtAPIBase
from   lib.extapi.custom_rest import CustomRestAPI
import lib.extapi.errors      as     errors
from   lib.extapi.healthcheck import HealthCheckAPI
import lib.extapi.parameters  as     params
import logging
import requests
import werkzeug

app = Flask(__name__)

class SegmentDataAPI(ExtAPIBase):

    @ExtAPIBase.auth.login_required
    def post(self):
        """
        セグメントに紐づく3rd party のクッキーを登録する
        """
        try:
            try:
                parser = reqparse.RequestParser()
                parser.add_argument('pid',    type=params.PID,   required=True, location='json')
                parser.add_argument('sid',    type=params.SID,   required=True, location='json')
                parser.add_argument('seg_id', type=params.SegID, required=True, location='json')
                parser.add_argument('uids',   type=params.UIDs,  required=True, location='json')
                parser.add_argument('ttl',    type=params.TTL,                  location='json')
                args   = parser.parse_args()
                pid    = args['pid'].value
                sid    = args['sid'].value
                seg_id = args['seg_id'].value
                uids   = args['uids'].value
                ttl    = None if args['ttl'] is None else args['ttl'].value
                ds     = DATASOURCE[pid]
                rta    = SegmentDataAPI.config.mapping[sid]['rt']['rta']
            except (errors.IllegalParameter, werkzeug.exceptions.ClientDisconnected) as e:
                app.logger.warn(e.description)
                raise errors.IllegalParameter()
            except Exception, e:
                raise errors.IllegalParameter()

            try:
                headers = {'content-type': 'application/json'}
                url     = 'http://localhost:8888/extseg.data.{sid}.{pid}'
                result = requests.post(url.format(sid=sid, pid=pid),
                                       headers=headers,
                                       data=json.dumps({'pid':pid, 'sid':sid, 'seg_id':seg_id, 'uids':uids, 'ttl':ttl }))
            except requests.exceptions.ConnectionError:
                app.logger.error('[ERROR] td-agent is not ready.')
                raise errors.InternalError()

            if result.status_code == 200:
                return jsonify(status='ok')
            else:
                app.logger.error('[ERROR] data transfer to td-agent failed.')
                raise errors.InternalError()

        except Exception, e:
            app.logger.exception('[ERROR]')
            raise e

api = CustomRestAPI(app)
api.add_resource(SegmentDataAPI, '/v1/segdata')
api.add_resource(HealthCheckAPI, '/healthcheck')
app.logger.setLevel(logging.WARN)
# FIXME debug モードにしないと uwsgi ログに出力されない
app.debug = True


if __name__ == '__main__':
    api.app.run(debug=True)
