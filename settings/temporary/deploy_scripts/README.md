#AutoScaling設定について

###注意

環境に応じて以下の項目変更になります。

* launch configuration名
* ami id
* instance type
* user data
* auto scaling group名
* min size
* max size
* desired capacity
* ELB名
* tag

###設定手順
site-webの設定例です。

####Cloud-init user-data作成
> $ cat userdata_site_web

```
#cloud-config
repo_upgrade: none
runcmd:
- DIR=/usr/local/tamar/repos/tamar
- cd $DIR && git pull
#- cp $DIR/web-site/td-agent/td-agent-site-web.conf /etc/td-agent/td-agent.conf
- cp $DIR/web-site/nginx/nginx-site.conf /etc/nginx/nginx.conf
- cp $DIR/web-site/nginx/conf.d/webserver-site.conf /etc/nginx/conf.d/webserver-site.conf
- cp $DIR/web-site/uwsgi/uwsgi.ini /etc/uwsgi/uwsgi.ini
- sed -i -e 's/= dev/= prd/' /etc/uwsgi/uwsgi.ini
- cp $DIR/web-site/uwsgi/init.d/uwsgi /etc/init.d/uwsgi
- cp $DIR/web-site/nginx/html/robots.txt /usr/share/nginx/html/robots.txt
- /etc/init.d/uwsgi start
- /etc/init.d/nginx start
```

####launch configuration作成
```
$ aws autoscaling create-launch-configuration \
--profile bp \
--launch-configuration-name tamar-site-web-lc \
--image-id ami-974dd496 \
--key-name tamar \
--security-groups sg-d0dc3fbf sg-d3dc3fbc \
--instance-type t1.micro \
--instance-monitoring '{"Enabled": false}' \
--associate-public-ip-address \
--user-data "`cat userdata_site_web | base64`"
```

####launch configuration確認
```
$ aws autoscaling describe-launch-configurations --profile bp --launch-configuration-names tamar-site-web-lc
```

####auto scaling group作成
```
$ aws autoscaling create-auto-scaling-group \
--profile bp \
--auto-scaling-group-name tamar-site-web-grp \
--launch-configuration-name tamar-site-web-lc \
--min-size 1 \
--max-size 2 \
--desired-capacity 1 \
--load-balancer-names testelb \
--health-check-type EC2 \
--vpc-zone-identifier subnet-0f9dca67 \
--termination-policies ClosestToNextInstanceHour \
--tags Key=Name,Value=test-site-web,PropagateAtLaunch=true
```

####auto scaling group確認
```
$ aws autoscaling describe-auto-scaling-groups --profile bp --auto-scaling-group-names tamar-site-web-grp
```

####動作確認
> IPアドレスを確認し、ログイン

```
$ aws autoscaling describe-auto-scaling-instances --profile bp --output text

ap-northeast-1a i-xxxxxxxx      tamar-site-web-grp      HEALTHY InService       tamar-site-web-lc
$ aws ec2 describe-instances --profile bp --instance-ids i-xxxxxxxx --output text
```

> Cloud-initが正常に動作したかテスト

```
sh /home/vagrant/tamar/deploy_scripts/userdata_site_web_test.sh
```

####auto scaling group削除
始めにdesired capacity, min sizeを0にして、インスタンスを停止する。
```
$ aws autoscaling update-auto-scaling-group --profile bp --auto-scaling-group-name tamar-site-web-grp --desired-capacity 0 --min-size 0
```

削除
```
$ aws autoscaling delete-auto-scaling-group --profile bp --auto-scaling-group-name tamar-site-web-grp
```

####launch configuration削除
```
$ aws autoscaling delete-launch-configuration --profile bp --launch-configuration-name tamar-site-web-lc
```


####アップデート後インスタンス入れ替え
```
$ aws autoscaling describe-auto-scaling-instances --profile bp --output text
```

```
$ aws autoscaling terminate-instance-in-auto-scaling-group \
--profile bp \
--instance-id i-xxxxxxxx
```

####インスタンスを停止すると共にdesired-capacityを下げる
```
$ aws autoscaling terminate-instance-in-auto-scaling-group \
--profile bp \
--instance-id 
--should-decrement-desired-capacity
```

####TODO
---
repo_upgrade: all  
or  
repo_upgrade: none  

---
aws autoscaling put-scaling-policy  
http://dev.classmethod.jp/cloud/aws/comprehend-auto-scaling-desired-capacity/
http://mtl.recruit.co.jp/blog/2011/10/amazon_webservice_autoscaling.html

---
put-notification-configuration  
--topic-arn  
http://docs.aws.amazon.com/general/latest/gr/aws-arns-and-namespaces.html

---

####user dataテスト
> $ aws ec2 run-instances --profile bp --image-id ami-55009954 --instance-type t1.micro --subnet-id subnet-0f9dca67 --security-group-ids sg-d0dc3fbf --user-data "`cat userdata_test2 | base64`" --key-name tamar

http://qiita.com/YamaguchiRei/items/91eed4186a814dd23765

---
* 他考慮点  
--spot-price

