#!/bin/sh
DIR=/usr/local/tamar/repos/tamar
cd $DIR && git log | head
diff $DIR/web-site/nginx/nginx-site.conf /etc/nginx/nginx.conf
diff $DIR/web-site/nginx/conf.d/webserver-site.conf /etc/nginx/conf.d/webserver-site.conf
diff $DIR/web-site/uwsgi/uwsgi.ini /etc/uwsgi/uwsgi.ini
echo "it is OK if the files differ. Please check if s/= dev/= prd/ was done."
diff $DIR/web-site/uwsgi/init.d/uwsgi /etc/init.d/uwsgi
diff $DIR/web-site/nginx/html/robots.txt /usr/share/nginx/html/robots.txt
ps aux | grep uwsgi
ps aux | grep nginx
