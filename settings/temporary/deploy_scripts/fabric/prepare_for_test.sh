#!/bin/sh
rm -f /tmp/etc/nginx/nginx.conf
rm -f /tmp/etc/nginx/conf.d/webserver-site.conf
rm -f /tmp/etc/td-agent/td-agent.conf
rm -f /tmp/uwsgi/uwsgi.ini
rm -f /tmp/etc/init.d/uwsgi
mkdir -p /tmp/etc/nginx/conf.d
mkdir -p /tmp/etc/td-agent/
mkdir -p /tmp/uwsgi/
mkdir -p /tmp/etc/init.d/
