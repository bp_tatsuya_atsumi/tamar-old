# -*- coding: utf-8 -*-
import boto.ec2, sys
from boto.ec2.blockdevicemapping import EBSBlockDeviceType, BlockDeviceMapping
from time import sleep
import datetime

tagval = "test"
region = "ap-northeast-1"
d = datetime.datetime.today()
timeappendix = d.strftime("%Y%m%d%H%M%S")
conn = boto.ec2.connect_to_region(region)

"""
# create ami
ebs = EBSBlockDeviceType() 
ebs.snapshot_id = 'snap-43fc9f66' 
block_map = BlockDeviceMapping() 
block_map['/dev/sda1'] = ebs 

conn.register_image(
  name='testAAA',
  description='descriptionAAA',
  architecture='x86_64',
  kernel_id='aki-44992845',
  root_device_name='/dev/sda1',
  block_device_map=block_map,
  dry_run=False
)
"""


instance = conn.get_all_reservations()[0].instances[0]
stg_instance_id = instance.id
stg_volume_id = instance.block_device_mapping['/dev/sda1'].volume_id
stg_az = instance.placement
print stg_volume_id
print stg_az
sys.exit()
#instance_id = i-0f9c570a
#volume_id = vol-3b43b46d

def main():
  # instance id, volume id取得
  reservations = conn.get_all_reservations(filters={"tag-value":tagval})
  stg_instance_id = reservations[0].instances[0].id
  stg_volume_id = reservations[0].instances[0].block_device_mapping['/dev/sda1'].volume_id
  stg_az = reservations[0].instances[0].placement

  # stop instance
  conn.stop_instances(instance_ids=[stg_instance_id])
  my_wait(
    interval=10,
    retry_count=30,
    exec_update_command='conn.get_all_reservations(filters={"instance_id":"' + stg_instance_id + '"})[0].instances[0].update',
    exec_command='conn.get_all_reservations(filters={"instance_id":"' + stg_instance_id + '"})[0].instances[0].state',
    expect='stopped'
  )

  # create snapshot
  stg_snapshot = conn.create_snapshot(stg_volume_id, 'stg-' + tagval + '-' + timeappendix)
  print stg_snapshot.id
  my_wait(
    interval=10,
    retry_count=30,
    exec_update_command='conn.get_all_snapshots(snapshot_ids=["' + stg_snapshot.id + '"])[0].update',
    exec_command='conn.get_all_snapshots(snapshot_ids=["' + stg_snapshot.id + '"])[0].status',
    expect='completed'
  )

  # create volume
  new_vol = stg_snapshot.create_volume(stg_az)

  # start instance
  conn.start_instances(instance_ids=[stg_instance_id])
  my_wait(
    interval=10,
    retry_count=30,
    exec_update_command='conn.get_all_reservations(filters={"instance_id":"' + stg_instance_id + '"})[0].instances[0].update',
    exec_command='conn.get_all_reservations(filters={"instance_id":"' + stg_instance_id + '"})[0].instances[0].state',
    expect='running'
  )

  # attach volume
  my_wait(
    interval=10,
    retry_count=30,
    exec_update_command='conn.get_all_volumes(volume_ids=["' + new_vol.id + '"])[0].update',
    exec_command='conn.get_all_volumes(volume_ids=["' + new_vol.id + '"])[0].status',
    expect='available'
  )
  conn.attach_volume (new_vol.id, stg_instance_id, "/dev/sdf1")
  my_wait(
    interval=10,
    retry_count=30,
    exec_update_command='conn.get_all_volumes(volume_ids=["' + new_vol.id + '"])[0].update',
    exec_command='conn.get_all_volumes(volume_ids=["' + new_vol.id + '"])[0].status',
    expect='in-use'
  )

  """
  mount
  copy
  umount
  """

def my_wait(interval, retry_count, exec_update_command, exec_command, expect):
  loop_count = 1
  while loop_count <= retry_count:
    exec(exec_update_command)
    exec("status = " + exec_command)
    if status == expect:
      break
    print "command excuted: " + exec_command
    print "status expected: " + expect
    print "State: " + status + " / Retried: " + str(loop_count) + " / Remaining: " + str(retry_count - loop_count)
    sleep(interval)
    loop_count += 1

def detach_volume(vol_id):
  # detach volume
  conn.detach_volume(vol_id)
  my_wait(
    interval=10,
    retry_count=30,
    exec_update_command='conn.get_all_volumes(volume_ids=["' + vol_id + '"])[0].update',
    exec_command='conn.get_all_volumes(volume_ids=["' + vol_id + '"])[0].status',
    expect='available'
  )

  # create snapshot

if __name__ == "__main__":
  #main()
  detach_volume('vol-29f90f7f')
