# -*- coding: utf-8 -*-
import boto.ec2
from boto.ec2.blockdevicemapping import EBSBlockDeviceType, BlockDeviceMapping
from time import sleep
import datetime
from fabric.api import local, run, env, cd, lcd, abort, settings, sudo, task
from fabric.colors import red, green
from fabric.contrib.console import confirm
import sys, ConfigParser

# TODO
# boto情報のアップデートが遅い
# conn.get_all_snapshots().update()
"""
import boto.ec2
conn = boto.ec2.connect_to_region("ap-northeast-1")
print conn.get_all_snapshots()
"""
# なぜかsnapshotが大量に出力される

CONFIG_FILE = 'settings.ini'

conf = ConfigParser.SafeConfigParser()
conf.read(CONFIG_FILE)
region = conf.get('parameters', 'region')
env.tagname = ""

env.use_ssh_config = True
env.dryrun = False

d = datetime.datetime.today()
timeappendix = d.strftime("%Y%m%d%H%M%S")

def _get_boto_ec2_connection():
  #conn = boto.ec2.connect_to_region("ap-northeast-1")
  conn = boto.ec2.connect_to_region(region)
  return conn

"""
def tmp_test():
  instance_id = 'i-0f9c570a'
  #instance = conn.get_all_reservations(instance_ids='i-0f9c570a')[0].instances[0];
  instance = conn.get_all_reservations(instance_ids=instance_id)[0].instances[0];
  #instance = conn.get_all_reservations()[0].instances[0];
  stg_instance_id = instance.id
  stg_volume_id = instance.block_device_mapping['/dev/sda1'].volume_id
  stg_az = instance.placement
  print stg_volume_id
  print stg_az
"""

def prepare_for_test():
  local('sh prepare_for_test.sh')

def production():
  env.environment = "production"

def staging():
  env.environment = "staging"

def dryrun():
  env.dryrun = True

def deploy(role):
  if env.environment != "staging":
    abort(red("environment must be staging"))

  # check arguments
  _check_role(role)

  # confirm
  _confirm(role)

  # do deploy
  _deploy(role)

def prepare1(role):
  if env.environment != "production":
    abort(red("environment must be production"))

  # check arguments
  _check_role(role)

  # confirm
  _confirm(role)

  # get boto ec2 connection
  conn = _get_boto_ec2_connection()

  # get instance_id
  instance_id = _get_instance_id(role, conn)
  if instance_id == "":
    abort(red("unable to get instance id"))

  # do prepare1
  _prepare1(role, conn, instance_id)

def prepare2(role):
  if env.environment != "production":
    abort(red("environment must be production"))

  # get boto ec2 connection
  conn = _get_boto_ec2_connection()

  # get instance_id
  instance_id = _get_instance_id(role, conn)
  if instance_id == "":
    abort(red("unable to get instance id"))

  # check arguments
  _check_role(role)

  # confirm
  _confirm(role)

  # do release
  _prepare2(role, conn, instance_id)

def release(role):
  # AMI作成
  # TODO インスタンスが停止したことを確認する必要あり
  my_run('aws ec2 create-image --profile xxxx --instance-id ' + instance_id + ' --name ' + ami_name)

  # AutoScaling launch configへAMIを追加
  set_ami_to_launch_config()
  uncopy()

def _deploy(role):
  ini = env.environment + '-' + role

  # 設定読み込み
  sudo_tasks = _get_lines_param(ini, 'sudo_tasks')

  with cd(conf.get(ini, 'work_dir_path')):
    # git pull
    my_run('git pull')

    # sudoタスク実行
    for i in sudo_tasks:
      my_sudo(i) 

  # TODO テスト
  #test()
    
def _get_instance_info(instance_id, device='/dev/sda1'):
  instance = conn.get_all_reservations(instance_ids=[instance_id])[0].instances[0];
  info = {
    'instance':instance,
    'volume_id':instance.block_device_mapping[device].volume_id,
    'az':instance.placement,
    'ip_address':instance.ip_address
  }

  return info

def _prepare1(role, conn, instance_id):
  ini = env.environment + '-' + role

  # volume id, availability zone, ip address取得
  stg_instance_info = _get_instance_info(instance_id)

  print "IP address is " + green(stg_instance_info['ip_address'])

  # stop instance
  conn.stop_instances(instance_ids=[instance_id])
  my_wait(
    interval=10,
    retry_count=30,
    exec_update_command='conn.get_all_reservations(instance_ids=["' + instance_id + '"])[0].instances[0].update()',
    exec_command='conn.get_all_reservations(instance_ids=["' + instance_id + '"])[0].instances[0].state',
    expect='stopped'
  )

  # create snapshot ( snapshot 1 )
  stg_snapshot = conn.create_snapshot(stg_instance_info['volume_id'], 'snapshot-' + env.instance_name + '-' + timeappendix)
  print stg_snapshot.id
  my_wait(
    interval=10,
    retry_count=30,
    #exec_update_command='conn.get_all_snapshots(snapshot_ids=["' + stg_snapshot.id + '"])[0].update()',
    exec_update_command='boto.ec2.snapshot.Snapshot(conn).update()',
    exec_command='conn.get_all_snapshots(snapshot_ids=["' + stg_snapshot.id + '"])[0].status',
    expect='completed'
  )

  # create volume
  new_vol = stg_snapshot.create_volume(stg_instance_info['az'])

  # start instance
  conn.start_instances(instance_ids=[instance_id])
  my_wait(
    interval=10,
    retry_count=30,
    exec_update_command='conn.get_all_reservations(instance_ids=["' + instance_id + '"])[0].instances[0].update()',
    exec_command='conn.get_all_reservations(instance_ids=["' + instance_id + '"])[0].instances[0].state',
    expect='running'
  )

  # attach volume to staging instance
  my_wait(
    interval=10,
    retry_count=30,
    exec_update_command='conn.get_all_volumes(volume_ids=["' + new_vol.id + '"])[0].update()',
    exec_command='conn.get_all_volumes(volume_ids=["' + new_vol.id + '"])[0].status',
    expect='available'
  )
  conn.attach_volume (new_vol.id, instance_id, "/dev/sdf1")
  my_wait(
    interval=10,
    retry_count=30,
    exec_update_command='conn.get_all_volumes(volume_ids=["' + new_vol.id + '"])[0].update()',
    exec_command='conn.get_all_volumes(volume_ids=["' + new_vol.id + '"])[0].status',
    expect='in-use'
  )

  new_instance = conn.get_all_reservations(instance_ids=[instance_id])[0].instances[0]
  new_instance.update()
  print "IP address now changed to " + green(new_instance.ip_address)

def _prepare2(role, conn, instance_id):
  ini = env.environment + '-' + role

  # volume id, availability zone, ip address取得
  stg_instance_info = _get_instance_info(instance_id, device='/dev/sdf1')
  new_vol = {'id':stg_instance_info['volume_id']}

  # mount, 設定コピー, umount
  sudo_tasks = _get_lines_param(ini, 'sudo_tasks')

  with cd(conf.get(ini, 'work_dir_path')):
    # sudoタスク実行
    for i in sudo_tasks:
      my_sudo(i) 

  # detach volume from staging instance
  conn.detach_volume(new_vol['id'])
  my_wait(
    interval=10,
    retry_count=30,
    exec_update_command='conn.get_all_volumes(volume_ids=["' + new_vol['id'] + '"])[0].update()',
    exec_command='conn.get_all_volumes(volume_ids=["' + new_vol['id'] + '"])[0].status',
    expect='available'
  )

  # create snapshot ( snapshot 2 )
  prd_snapshot = conn.create_snapshot(new_vol['id'], 'snapshot-' + env.instance_name + '-' + timeappendix)
  print prd_snapshot.id
  my_wait(
    interval=10,
    retry_count=30,
    #exec_update_command='conn.get_all_snapshots(snapshot_ids=["' + prd_snapshot.id + '"])[0].update()',
    exec_update_command='boto.ec2.snapshot.Snapshot(conn).update()',
    exec_command='conn.get_all_snapshots(snapshot_ids=["' + prd_snapshot.id + '"])[0].status',
    expect='completed'
  )

  # create ami ( arch x86_64 )
  ebs = EBSBlockDeviceType() 
  ebs.snapshot_id = prd_snapshot.id 
  block_map = BlockDeviceMapping() 
  block_map['/dev/sda1'] = ebs 

  new_ami_id = conn.register_image(
    name='new' + conf.get(ini, 'instance_name')
    description='descriptionAAA',
    architecture='x86_64',
    kernel_id=conf.get(ini, 'kernel_id')
    root_device_name='/dev/sda1',
    block_device_map=block_map,
    dry_run=False
  )
  my_wait(
    interval=10,
    retry_count=30,
    exec_update_command='conn.get_all_images(image_ids=["' + new_ami_id + '"],owners="self")[0].update()',
    exec_command='conn.get_all_images(image_ids=["' + new_ami_id + '"],owners="self")[0].state',
    expect='available'
  )

  sys.exit()

  """
  stop instance
  create snapshot ( snapshot 1 )
  create volume ( volume 1 )
  attach volume to staging instance
  mount
  copy settings
  umount
  detach volume from staging instance
  create snapshot ( snapshot 2 )
  create ami ( arch x86_64 )
  replace ami of autoscaling group
    aws autoscaling create-launch-configuration --kernel-id aki-44992845
    kernel idを指定する必要あり
  delete volume 1
  """

def my_sudo(command):
  # almost equivalent to
  # print sudo(stop_command) if env.dryrun == False else stop_command
  if env.dryrun == True:
    print command
    return ""
  else:
    return sudo(command)

def my_run(command):
  if env.dryrun == True:
    print command
    return ""
  else:
    return run(command)

def _make_ami():
  my_run('aws ec2 create-image --profile xxxx --instance-id ' + instance_id + ' --name ' + ami_name)
  set_ami_to_launch_config()
  uncopy()
  
def release_site_web():
  """
  nginx
  fluentd
  python
  uwsgi
  ElastiCache
  """
  pull()
  copy()
  test()
  stop_service()
  make_ami()
  set_ami_to_launch_config()

def release_partner_web():
  """
  nginx
  fluentd
  """
  pull()
  copy()
  test()
  stop_service()
  make_ami()
  set_ami_to_launch_config()

def release_partner_aggregator():
  """
  fluentd
  S3
  """
  pull()
  copy()
  test()
  stop_service()
  make_ami()
  set_ami_to_launch_config()

def release_sftp():
  """
  sftpd
  """
  groupadd()
  useradd()
  make_ami()

def release_batch():
  """
  python
  RDS
  S3
  SQS
  SES
  SNS
  """
  pull()
  copy()
  test()
  stop_service()
  make_ami()
  set_ami_to_launch_config()

  # ローカルテスト用
  """
  with lcd('/home/vagrant/tamar'):
    local('git pull')
    local('cp web-partner/nginx/nginx-partner.conf ~/')
  """

def _get_lines_param(ini, key):
  lines = conf.get(ini, key)
  param_list = lines.strip().splitlines()
  return param_list

def _check_role(role):
  roles = _get_lines_param('parameters', 'roles')
  if not role in roles:
    print "Please select role from the list below."
    print green(roles)
    abort(red("role is invalid"))

def _confirm(role):
  if not confirm("Are you sure you want to deploy " + green(role.upper()) + " resources to " + green(env.environment.upper()) + " env.environment?", default=False):
    abort(red("Aborting your request."))
  
def _get_instance_id(role, conn):
  ini = env.environment + '-' + role

  env.instance_name = conf.get(env.environment + '-' + role, 'instance_name')
  print env.instance_name

  instance_id = conn.get_all_reservations(filters={"tag-value":env.instance_name})[0].instances[0].id

  return instance_id

def my_wait(interval, retry_count, exec_update_command, exec_command, expect):
  loop_count = 1
  while loop_count <= retry_count:
    print "command excuted: " + exec_command
    print "status expected: " + expect

    exec(exec_update_command)
    exec("status = " + exec_command)
    print "status you get: " + status

    if status == expect:
      break
    print "State: " + status + " / Retried: " + str(loop_count) + " / Remaining: " + str(retry_count - loop_count)
    sleep(interval)
    loop_count += 1

