### 概要
settings.ini
各環境において、何をコピーして、何を再起動して、何を停止するのかなどを設定します。

settings.iniの
work_dir_pathにgit pullします。
sudo_tasksをsudo権限で実行します。

### セットアップ
$ pip install fabric
$ pip install boto
$ vi ~/.boto
[Credentials]
aws_access_key_id=xxxxxxxxxxxxxxxxxx
aws_secret_access_key=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

### 実行方法
dryrunをつけると、
sudo_tasksのコマンドが出力されるのみで、実行されません。
sudo_tasks以外は今のところ実行されるので注意してください。

####DEPLOY（テスト用）
/tmp/etc/nginx/などのディレクトリを作成します。
prepare_for_test.sh
が実行されるだけです。

```
$ fab -H localhost dryrun prepare_for_test
$ fab -H localhost prepare_for_test
```

####DEPLOY（ステージング環境）
```
$ fab -H localhost dryrun staging deploy:site-web
$ fab -H localhost staging deploy:site-web
```

####RELEASE（本番環境）
ssh_configの設定
```
.ssh/config
Host test
  HostName 54.238.135.61
  User ec2-user
  StrictHostKeyChecking no
  PasswordAuthentication no
  IdentityFile ~/.ssh/tamar.pem
  IdentitiesOnly yes
  LogLevel FATAL
```

prepare1（volume attachまで）
```
$ fab -H test dryrun production prepare1:site-web
$ fab -H test production prepare1:site-web
```

ここで再度ssh_configを設定

prepare2（AMI作成まで）
```
$ fab -H test dryrun production prepare2:site-web
$ fab -H test production prepare2:site-web
```

release（AutoScaling設定まで）
**未完成**
```
$ fab -H test dryrun production release:site-web
$ fab -H test production release:site-web
```

