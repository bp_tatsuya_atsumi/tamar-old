# 管理者用設定
sudo passwd
sudo su -

---------------------------------------
# yumパッケージ更新
yum -y update

---------------------------------------
# タイムゾーン設定
cp -p /usr/share/zoneinfo/Japan /etc/localtime

---------------------------------------
# 言語設定
echo 'LANG="ja_JP.UTF-8"' > /etc/sysconfig/i18n

---------------------------------------
# ulimits設定
vim /etc/security/limits.conf

```
* soft nofile 65536
* hard nofile 65536
```

---------------------------------------
# 不要サービスの停止
service acpid stop
service auditd stop
service ip6tables stop
service iptables stop
service lvm2-monitor stop
service mdmonitor stop
service messagebus stop
service netfs stop
service sendmail stop

chkconfig acpid off
chkconfig auditd off
chkconfig ip6tables off
chkconfig iptables off
chkconfig lvm2-monitor off
chkconfig mdmonitor off
chkconfig messagebus off
chkconfig netfs off
chkconfig sendmail off

---------------------------------------
# Gitインストール
yum -y install git.x86_64

---------------------------------------
# Python2.7インストール
yum -y install openssl-devel.x86_64
yum -y install python27.x86_64
yum -y install python27-devel.x86_64

## setuptoolsインストール
cd /tmp/
wget https://pypi.python.org/packages/source/s/setuptools/setuptools-1.1.6.tar.gz
tar zxvf setuptools-1.1.6.tar.gz
cd setuptools-1.1.6
/usr/bin/python2.7 setup.py install

## pipインストール
easy_install pip

## パッケージインストール
sudo yum -y install gcc-c++
sudo yum -y install mysql mysql-devel

sudo pip install awscli
sudo pip install boto
sudo pip install Fabric
sudo pip install Flask
sudo pip install MySQL-python
sudo pip install pycrypto
sudo pip install python-memcached
sudo pip install SQLAlchemy
sudo pip install uWSGI

sudo pip install pep8
sudo pip install pytest
sudo pip install mock
sudo pip install requests

---------------------------------------
# tamarユーザの追加
useradd tamar -d /usr/local/tamar

# tamarユーザにsudo権限追加
visudo

<!--
(編集方法)
## Same thing without a password
# %wheel        ALL=(ALL)       NOPASSWD: ALL
tamar   ALL=(ALL)       NOPASSWD: ALL
-->

---------------------------------------
# アプリケーションユーザ設定
sudo su - tamar

# ディレクトリ作成
mkdir tmp
mkdir log
mkdir repos

# Git設定
## SSH鍵
mkdir .ssh

## sshのconfig設定
cat << EOF > ~/.ssh/config
Host github.com
  Hostname          github.com
  User              git
  Port              22
  IdentityFile      ~/.ssh/id_rsa_github_crossoven
  TCPKeepAlive      yes
  IdentitiesOnly    yes
EOF
chmod 644 ~/.ssh/config
