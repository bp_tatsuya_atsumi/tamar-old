# tamarユーザ
sudo su - tamar

# 設定情報取得
git clone git@github.com:rindai87/tamar.git /usr/local/tamar/repos/tamar/

---------------------------------------
# OS
vi /etc/sysctl.conf
```
net.core.somaxconn = 10000
net.core.rmem_max = 16777216
net.core.rmem_default = 2097152
net.core.wmem_max = 16777216
net.core.wmem_default = 2097152
net.core.optmem_max = 10000000
net.core.message_burst = 400
net.core.netdev_max_backlog = 3000
net.unix.max_dgram_qlen = 400
net.ipv4.tcp_rmem = 4096 87380 16777216
net.ipv4.tcp_wmem = 4096 87380 16777216
net.ipv4.tcp_mem = 8388608 8388608 8388608
net.ipv4.tcp_fin_timeout = 10
net.ipv4.tcp_tw_reuse = 1
net.ipv4.ip_local_port_range = 1024 65000
```

## ファイルディスクリプタ上限値の変更
vi /etc/security/limits.conf
```
* soft nofile 65536
* hard nofile 65536
```

## OS再起動
shutdown -r now

#### 各設定についての説明

|設定|説明|
|:--|:--|
|net.core.somaxconn = 10000|TCPソケットが受け付けた接続要求を格納する、キューの最大長|
|net.core.rmem_max = 16777216|よく分かりません|
|net.core.rmem_default = 2097152|よく分かりません|
|net.core.wmem_max = 16777216|よく分かりません|
|net.core.wmem_default = 2097152|よく分かりません|
|net.core.optmem_max = 10000000|よく分かりません|
|net.core.message_burst = 400|よく分かりません|
|net.core.netdev_max_backlog = 3000|ネットワークのバッファ|
|net.unix.max_dgram_qlen = 400|よく分かりません|
|net.ipv4.tcp_rmem = 4096 87380 16777216|よく分かりません|
|net.ipv4.tcp_wmem = 4096 87380 16777216|よく分かりません|
|net.ipv4.tcp_mem = 8388608 8388608 8388608|TCP通信で使用するメモリ上限 最少/圧縮/最大|
|net.ipv4.tcp_fin_timeout = 10|TCP FINの時間|
|net.ipv4.tcp_tw_reuse = 1|TCP TIME_WAITの再使用|
|net.ipv4.ip_local_port_range = 1024 65000|ポート番号の使用範囲|
|soft nofile 65536|ファイルディスクリプタ数soft limit上限値|
|hard nofile 65536|ファイルディスクリプタ数hard limit上限値|

---------------------------------------
# Nginx
## インストール
sudo yum -y install nginx

#####*CentOSの場合*
デフォルトではyum installできないので、先に以下のrepositoryを登録
sudo vi /etc/yum.repos.d/nginx.repo
```
[nginx]
name=nginx repo
baseurl=http://nginx.org/packages/centos/6/$basearch/
gpgcheck=0
enabled=1
```

# 不要ファイル削除
sudo rm /etc/nginx/conf.d/virtual.conf
sudo rm /usr/share/nginx/html/404.html
sudo rm /usr/share/nginx/html/50x.html
sudo rm /usr/share/nginx/html/index.html
sudo rm /usr/share/nginx/html/nginx-logo.png

## configファイル配置
sudo cp /usr/local/tamar/repos/tamar/settings/site-web/nginx/nginx-site.conf /etc/nginx/nginx.conf
sudo cp /usr/local/tamar/repos/tamar/settings/site-web/nginx/conf.d/* /etc/nginx/conf.d/

## robots.txt
sudo cp /usr/local/tamar/repos/tamar/settings/site-web/nginx/html/robots.txt /usr/share/nginx/html/

## 自動起動設定
sudo chkconfig nginx off

## 起動
sudo service nginx start

---------------------------------------
# uWSGI
sudo mkdir /var/log/uwsgi
sudo chown tamar:tamar /var/log/uwsgi

## configファイル配置
sudo mkdir /etc/uwsgi
sudo cp /usr/local/tamar/repos/tamar/settings/site-web/uwsgi/uwsgi.ini /etc/uwsgi/

## initスクリプト配置
sudo cp /usr/local/tamar/repos/tamar/settings/site-web/uwsgi/init.d/uwsgi /etc/init.d/

## 自動起動設定
sudo chkconfig --add uwsgi
sudo chkconfig uwsgi off

## 起動
sudo service uwsgi start
