5 9 * * * cd /usr/local/tamar/repos/tamar; /usr/bin/python27 mapping_report.py prd

# setting for sid:0001
# ma
5 0-23/1 * * * cd /usr/local/tamar/repos/tamar; /usr/bin/python27 mapping.py 0001 ma prd
*/15 * * * * cd /usr/local/tamar/repos/tamar; /usr/bin/python27 transfer.py 0001 ma prd segmentmst
*/15 * * * * cd /usr/local/tamar/repos/tamar; /usr/bin/python27 transfer.py 0001 ma prd remove_segmentmst
*/15 * * * * cd /usr/local/tamar/repos/tamar; /usr/bin/python27 transfer.py 0001 ma prd conversion
00 9 * * * cd /usr/local/tamar/repos/tamar; /usr/bin/python27 transfer.py 0001 ma prd segmentdata
# ao
7 0-23/1 * * * cd /usr/local/tamar/repos/tamar; /usr/bin/python27 mapping.py 0001 ao prd
20 9 * * * cd /usr/local/tamar/repos/tamar; /usr/bin/python27 transfer.py 0001 ao prd segmentmst
25 9 * * * cd /usr/local/tamar/repos/tamar; /usr/bin/python27 transfer.py 0001 ao prd segmentdata
# so
10 0-23/1 * * * cd /usr/local/tamar/repos/tamar; /usr/bin/python27 mapping.py 0001 ao prd

# setting for sid:0003
13 0-23/1 * * * cd /usr/local/tamar/repos/tamar; /usr/bin/python27 mapping.py 0003 ma prd
16 0-23/1 * * * cd /usr/local/tamar/repos/tamar; /usr/bin/python27 mapping.py 0003 so prd

# setting for sid:0004
19 0-23/1 * * * cd /usr/local/tamar/repos/tamar; /usr/bin/python27 mapping.py 0004 ma prd
