/* Drop User */
GRANT USAGE ON *.* TO 'tamaruser'@'localhost';
DROP USER tamaruser@localhost;

/* Create User */
-- ユーザ作成
CREATE USER tamaruser@localhost IDENTIFIED BY '#!TawaR?u5er';
