--
-- Current Database: `DB_TAMAR_0087`
--

CREATE DATABASE IF NOT EXISTS `DB_TAMAR_0087` DEFAULT CHARACTER SET utf8;

USE `DB_TAMAR_0087`;

--
-- Table structure for table `t_user_mapping_0087_co`
--
CREATE TABLE IF NOT EXISTS `t_user_mapping_0087_co` (
  `user_id` varchar(255) NOT NULL COMMENT 'ユーザID',
  `partner_user_id` varchar(255) NOT NULL COMMENT 'パートナーユーザID',
  `tamar_user_type` tinyint(3) unsigned NOT NULL COMMENT 'TamarユーザID種別 : 1=1st Party Cookie, 2=3rd Party Cookie, ',
  `tamar_user_id` varchar(255) NOT NULL COMMENT 'TamarユーザID',
  `inserted_at` timestamp NOT NULL DEFAULT 0 COMMENT '登録日時',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日時',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ : 0:FALSE=未削除, 1:TRUE=削除済, ',
  PRIMARY KEY (`user_id`,`partner_user_id`),
  KEY `idx_t_user_mapping_0087_co_01` (`user_id`, `updated_at`, `inserted_at`),
  KEY `idx_t_user_mapping_0087_co_02` (`partner_user_id`, `updated_at`),
  KEY `idx_t_user_mapping_0087_co_03` (`updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ユーザマッピング';

--
-- Table structure for table `w_user_mapping_0087_co`
--
CREATE TABLE IF NOT EXISTS `w_user_mapping_0087_co` (
  `user_id` varchar(255) DEFAULT NULL,
  `partner_user_id` varchar(255) DEFAULT NULL,
  `tamar_user_type` tinyint(3) unsigned DEFAULT NULL,
  `tamar_user_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `t_segmentmst_from_0087_to_rt0059`
--
CREATE TABLE IF NOT EXISTS `t_segmentmst_from_0087_to_rt0059` (
  `segment_id_from` varchar(255) NOT NULL COMMENT 'ユーザID',
  `segment_id_to` varchar(255) NOT NULL COMMENT 'パートナーユーザID',
  `name` varchar(255) NOT NULL COMMENT 'セグメント名',
  `description` varchar(1000) DEFAULT NULL COMMENT '備考',
  `status` tinyint NOT NULL DEFAULT 0 COMMENT 'ステータス',
  PRIMARY KEY (`segment_id_from`,`segment_id_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='セグメントマスタ';

--
-- Table structure for table `t_user_mapping_0087_rt0059`
--
CREATE TABLE IF NOT EXISTS `t_user_mapping_0087_rt0059` (
  `user_id` varchar(255) NOT NULL COMMENT 'ユーザID',
  `partner_user_id` varchar(255) NOT NULL COMMENT 'パートナーユーザID',
  `tamar_user_type` tinyint(3) unsigned NOT NULL COMMENT 'TamarユーザID種別 : 1=1st Party Cookie, 2=3rd Party Cookie, ',
  `tamar_user_id` varchar(255) NOT NULL COMMENT 'TamarユーザID',
  `inserted_at` timestamp NOT NULL DEFAULT 0 COMMENT '登録日時',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日時',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ : 0:FALSE=未削除, 1:TRUE=削除済, ',
  PRIMARY KEY (`user_id`,`partner_user_id`),
  KEY `idx_t_user_mapping_0087_rt0059_01` (`user_id`, `updated_at`, `inserted_at`),
  KEY `idx_t_user_mapping_0087_rt0059_02` (`partner_user_id`, `updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ユーザマッピング';

--
-- Table structure for table `w_user_mapping_0087_rt0059`
--
CREATE TABLE IF NOT EXISTS `w_user_mapping_0087_rt0059` (
  `user_id` varchar(255) DEFAULT NULL,
  `partner_user_id` varchar(255) DEFAULT NULL,
  `tamar_user_type` tinyint(3) unsigned DEFAULT NULL,
  `tamar_user_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `t_segmentmst_from_0087_to_rt0085`
--
CREATE TABLE IF NOT EXISTS `t_segmentmst_from_0087_to_rt0085` (
  `segment_id_from` varchar(255) NOT NULL COMMENT 'ユーザID',
  `segment_id_to` varchar(255) NOT NULL COMMENT 'パートナーユーザID',
  `name` varchar(255) NOT NULL COMMENT 'セグメント名',
  `description` varchar(1000) DEFAULT NULL COMMENT '備考',
  `status` tinyint NOT NULL DEFAULT 0 COMMENT 'ステータス',
  PRIMARY KEY (`segment_id_from`,`segment_id_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='セグメントマスタ';

--
-- Table structure for table `t_user_mapping_0087_rt0085`
--
CREATE TABLE IF NOT EXISTS `t_user_mapping_0087_rt0085` (
  `user_id` varchar(255) NOT NULL COMMENT 'ユーザID',
  `partner_user_id` varchar(255) NOT NULL COMMENT 'パートナーユーザID',
  `tamar_user_type` tinyint(3) unsigned NOT NULL COMMENT 'TamarユーザID種別 : 1=1st Party Cookie, 2=3rd Party Cookie, ',
  `tamar_user_id` varchar(255) NOT NULL COMMENT 'TamarユーザID',
  `inserted_at` timestamp NOT NULL DEFAULT 0 COMMENT '登録日時',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日時',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ : 0:FALSE=未削除, 1:TRUE=削除済, ',
  PRIMARY KEY (`user_id`,`partner_user_id`),
  KEY `idx_t_user_mapping_0087_rt0085_01` (`user_id`, `updated_at`, `inserted_at`),
  KEY `idx_t_user_mapping_0087_rt0085_02` (`partner_user_id`, `updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ユーザマッピング';

--
-- Table structure for table `w_user_mapping_0087_rt0085`
--
CREATE TABLE IF NOT EXISTS `w_user_mapping_0087_rt0085` (
  `user_id` varchar(255) DEFAULT NULL,
  `partner_user_id` varchar(255) DEFAULT NULL,
  `tamar_user_type` tinyint(3) unsigned DEFAULT NULL,
  `tamar_user_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `t_segmentmst_from_0087_to_rt0086`
--
CREATE TABLE IF NOT EXISTS `t_segmentmst_from_0087_to_rt0086` (
  `segment_id_from` varchar(255) NOT NULL COMMENT 'ユーザID',
  `segment_id_to` varchar(255) NOT NULL COMMENT 'パートナーユーザID',
  `name` varchar(255) NOT NULL COMMENT 'セグメント名',
  `description` varchar(1000) DEFAULT NULL COMMENT '備考',
  `status` tinyint NOT NULL DEFAULT 0 COMMENT 'ステータス',
  PRIMARY KEY (`segment_id_from`,`segment_id_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='セグメントマスタ';

--
-- Table structure for table `t_user_mapping_0087_rt0086`
--
CREATE TABLE IF NOT EXISTS `t_user_mapping_0087_rt0086` (
  `user_id` varchar(255) NOT NULL COMMENT 'ユーザID',
  `partner_user_id` varchar(255) NOT NULL COMMENT 'パートナーユーザID',
  `tamar_user_type` tinyint(3) unsigned NOT NULL COMMENT 'TamarユーザID種別 : 1=1st Party Cookie, 2=3rd Party Cookie, ',
  `tamar_user_id` varchar(255) NOT NULL COMMENT 'TamarユーザID',
  `inserted_at` timestamp NOT NULL DEFAULT 0 COMMENT '登録日時',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日時',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ : 0:FALSE=未削除, 1:TRUE=削除済, ',
  PRIMARY KEY (`user_id`,`partner_user_id`),
  KEY `idx_t_user_mapping_0087_rt0086_01` (`user_id`, `updated_at`, `inserted_at`),
  KEY `idx_t_user_mapping_0087_rt0086_02` (`partner_user_id`, `updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ユーザマッピング';

--
-- Table structure for table `w_user_mapping_0087_rt0086`
--
CREATE TABLE IF NOT EXISTS `w_user_mapping_0087_rt0086` (
  `user_id` varchar(255) DEFAULT NULL,
  `partner_user_id` varchar(255) DEFAULT NULL,
  `tamar_user_type` tinyint(3) unsigned DEFAULT NULL,
  `tamar_user_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `t_segmentmst_from_0087_to_rt0088`
--
CREATE TABLE IF NOT EXISTS `t_segmentmst_from_0087_to_rt0088` (
  `segment_id_from` varchar(255) NOT NULL COMMENT 'ユーザID',
  `segment_id_to` varchar(255) NOT NULL COMMENT 'パートナーユーザID',
  `name` varchar(255) NOT NULL COMMENT 'セグメント名',
  `description` varchar(1000) DEFAULT NULL COMMENT '備考',
  `status` tinyint NOT NULL DEFAULT 0 COMMENT 'ステータス',
  PRIMARY KEY (`segment_id_from`,`segment_id_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='セグメントマスタ';

--
-- Table structure for table `t_user_mapping_0087_rt0088`
--
CREATE TABLE IF NOT EXISTS `t_user_mapping_0087_rt0088` (
  `user_id` varchar(255) NOT NULL COMMENT 'ユーザID',
  `partner_user_id` varchar(255) NOT NULL COMMENT 'パートナーユーザID',
  `tamar_user_type` tinyint(3) unsigned NOT NULL COMMENT 'TamarユーザID種別 : 1=1st Party Cookie, 2=3rd Party Cookie, ',
  `tamar_user_id` varchar(255) NOT NULL COMMENT 'TamarユーザID',
  `inserted_at` timestamp NOT NULL DEFAULT 0 COMMENT '登録日時',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日時',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ : 0:FALSE=未削除, 1:TRUE=削除済, ',
  PRIMARY KEY (`user_id`,`partner_user_id`),
  KEY `idx_t_user_mapping_0087_rt0088_01` (`user_id`, `updated_at`, `inserted_at`),
  KEY `idx_t_user_mapping_0087_rt0088_02` (`partner_user_id`, `updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ユーザマッピング';

--
-- Table structure for table `w_user_mapping_0087_rt0088`
--
CREATE TABLE IF NOT EXISTS `w_user_mapping_0087_rt0088` (
  `user_id` varchar(255) DEFAULT NULL,
  `partner_user_id` varchar(255) DEFAULT NULL,
  `tamar_user_type` tinyint(3) unsigned DEFAULT NULL,
  `tamar_user_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- add GRANT to tamaruser
GRANT  SELECT,INSERT,UPDATE,DELETE,DROP,CREATE TEMPORARY TABLES ON DB_TAMAR_0087.* TO tamaruser@'%';