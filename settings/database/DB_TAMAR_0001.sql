--
-- Current Database: `DB_TAMAR_0001`
--
CREATE DATABASE IF NOT EXISTS `DB_TAMAR_0001` DEFAULT CHARACTER SET utf8;

USE `DB_TAMAR_0001`;

--
-- Table structure for table `t_user_mapping_0001_ma`
--
DROP TABLE IF EXISTS `t_user_mapping_0001_ma`;
CREATE TABLE IF NOT EXISTS `t_user_mapping_0001_ma` (
  `user_id` varchar(255) NOT NULL COMMENT 'ユーザID',
  `partner_user_id` varchar(255) NOT NULL COMMENT 'パートナーユーザID',
  `tamar_user_type` tinyint(3) unsigned NOT NULL COMMENT 'TamarユーザID種別 : 1=1st Party Cookie, 2=3rd Party Cookie, ',
  `tamar_user_id` varchar(255) NOT NULL COMMENT 'TamarユーザID',
  `inserted_at` timestamp NOT NULL DEFAULT 0 COMMENT '登録日時',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日時',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ : 0:FALSE=未削除, 1:TRUE=削除済, ',
  PRIMARY KEY (`user_id`,`partner_user_id`),
  KEY `idx_t_user_mapping_0001_ma_01` (`user_id`, `updated_at`, `inserted_at`),
  KEY `idx_t_user_mapping_0001_ma_02` (`partner_user_id`, `updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ユーザマッピング';

--
-- Table structure for table `w_user_mapping_0001_ma`
--
DROP TABLE IF EXISTS `w_user_mapping_0001_ma`;
CREATE TABLE IF NOT EXISTS `w_user_mapping_0001_ma` (
  `user_id` varchar(255) DEFAULT NULL,
  `partner_user_id` varchar(255) DEFAULT NULL,
  `tamar_user_type` tinyint(3) unsigned DEFAULT NULL,
  `tamar_user_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `t_user_mapping_0001_ao`
--

DROP TABLE IF EXISTS `t_user_mapping_0001_ao`;
CREATE TABLE IF NOT EXISTS `t_user_mapping_0001_ao` (
  `user_id` varchar(255) NOT NULL COMMENT 'ユーザID',
  `partner_user_id` varchar(255) NOT NULL COMMENT 'パートナーユーザID',
  `tamar_user_type` tinyint(3) unsigned NOT NULL COMMENT 'TamarユーザID種別 : 1=1st Party Cookie, 2=3rd Party Cookie, ',
  `tamar_user_id` varchar(255) NOT NULL COMMENT 'TamarユーザID',
  `inserted_at` timestamp NOT NULL DEFAULT 0 COMMENT '登録日時',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日時',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ : 0:FALSE=未削除, 1:TRUE=削除済, ',
  PRIMARY KEY (`user_id`,`partner_user_id`),
  KEY `idx_t_user_mapping_0001_ao_01` (`user_id`, `updated_at`, `inserted_at`),
  KEY `idx_t_user_mapping_0001_ao_02` (`partner_user_id`, `updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ユーザマッピング';

--
-- Table structure for table `w_user_mapping_0001_ao`
--

DROP TABLE IF EXISTS `w_user_mapping_0001_ao`;
CREATE TABLE IF NOT EXISTS `w_user_mapping_0001_ao` (
  `user_id` varchar(255) DEFAULT NULL,
  `partner_user_id` varchar(255) DEFAULT NULL,
  `tamar_user_type` tinyint(3) unsigned DEFAULT NULL,
  `tamar_user_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `t_user_mapping_0001_so`
--
DROP TABLE IF EXISTS `t_user_mapping_0001_so`;
CREATE TABLE `t_user_mapping_0001_so` (
  `user_id` varchar(255) NOT NULL COMMENT 'ユーザID',
  `partner_user_id` varchar(255) NOT NULL COMMENT 'パートナーユーザID',
  `tamar_user_type` tinyint(3) unsigned NOT NULL COMMENT 'TamarユーザID種別 : 1=1st Party Cookie, 2=3rd Party Cookie, ',
  `tamar_user_id` varchar(255) NOT NULL COMMENT 'TamarユーザID',
  `inserted_at` timestamp NOT NULL DEFAULT 0 COMMENT '登録日時',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日時',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ : 0:FALSE=未削除, 1:TRUE=削除済, ',
  PRIMARY KEY (`user_id`,`partner_user_id`),
  KEY `idx_t_user_mapping_0001_so_01` (`user_id`, `updated_at`, `inserted_at`),
  KEY `idx_t_user_mapping_0001_so_02` (`partner_user_id`, `updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ユーザマッピング';

--
-- Table structure for table `w_user_mapping_0001_so`
--
DROP TABLE IF EXISTS `w_user_mapping_0001_so`;
CREATE TABLE `w_user_mapping_0001_so` (
  `user_id` varchar(255) DEFAULT NULL,
  `partner_user_id` varchar(255) DEFAULT NULL,
  `tamar_user_type` tinyint(3) unsigned DEFAULT NULL,
  `tamar_user_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `t_user_mapping_0001_ac`
--
DROP TABLE IF EXISTS `t_user_mapping_0001_ac`;
CREATE TABLE `t_user_mapping_0001_ac` (
  `user_id` varchar(255) NOT NULL COMMENT 'ユーザID',
  `partner_user_id` varchar(255) NOT NULL COMMENT 'パートナーユーザID',
  `tamar_user_type` tinyint(3) unsigned NOT NULL COMMENT 'TamarユーザID種別 : 1=1st Party Cookie, 2=3rd Party Cookie, ',
  `tamar_user_id` varchar(255) NOT NULL COMMENT 'TamarユーザID',
  `inserted_at` timestamp NOT NULL DEFAULT 0 COMMENT '登録日時',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日時',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ : 0:FALSE=未削除, 1:TRUE=削除済, ',
  PRIMARY KEY (`user_id`,`partner_user_id`),
  KEY `idx_t_user_mapping_0001_ac_01` (`user_id`, `updated_at`, `inserted_at`),
  KEY `idx_t_user_mapping_0001_ac_02` (`partner_user_id`, `updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ユーザマッピング';

--
-- Table structure for table `w_user_mapping_0001_ac`
--
DROP TABLE IF EXISTS `w_user_mapping_0001_ac`;
CREATE TABLE `w_user_mapping_0001_ac` (
  `user_id` varchar(255) DEFAULT NULL,
  `partner_user_id` varchar(255) DEFAULT NULL,
  `tamar_user_type` tinyint(3) unsigned DEFAULT NULL,
  `tamar_user_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `t_user_mapping_0001_im`
--
DROP TABLE IF EXISTS `t_user_mapping_0001_im`;
CREATE TABLE `t_user_mapping_0001_im` (
  `user_id` varchar(255) NOT NULL COMMENT 'ユーザID',
  `partner_user_id` varchar(255) NOT NULL COMMENT 'パートナーユーザID',
  `tamar_user_type` tinyint(3) unsigned NOT NULL COMMENT 'TamarユーザID種別 : 1=1st Party Cookie, 2=3rd Party Cookie, ',
  `tamar_user_id` varchar(255) NOT NULL COMMENT 'TamarユーザID',
  `inserted_at` timestamp NOT NULL DEFAULT 0 COMMENT '登録日時',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日時',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ : 0:FALSE=未削除, 1:TRUE=削除済, ',
  PRIMARY KEY (`user_id`,`partner_user_id`),
  KEY `idx_t_user_mapping_0001_im_01` (`user_id`, `updated_at`, `inserted_at`),
  KEY `idx_t_user_mapping_0001_im_02` (`partner_user_id`, `updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ユーザマッピング';

--
-- Table structure for table `w_user_mapping_0001_im`
--
DROP TABLE IF EXISTS `w_user_mapping_0001_im`;
CREATE TABLE `w_user_mapping_0001_im` (
  `user_id` varchar(255) DEFAULT NULL,
  `partner_user_id` varchar(255) DEFAULT NULL,
  `tamar_user_type` tinyint(3) unsigned DEFAULT NULL,
  `tamar_user_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `t_segmentmst_from_0001_to_im`
--
DROP TABLE IF EXISTS `t_segmentmst_from_0001_to_im`;
CREATE TABLE `t_segmentmst_from_0001_to_im` (
  `segment_id_from` varchar(50)    NOT NULL              COMMENT 'ユーザID',
  `segment_id_to`   varchar(50)    NOT NULL              COMMENT 'パートナーユーザID',
  `name`            varchar(50)    NOT NULL              COMMENT 'セグメント名',
  `description`     varchar(1000)           DEFAULT NULL COMMENT '備考',
  `status`          tinyint        NOT NULL DEFAULT 0    COMMENT 'ステータス',
  PRIMARY KEY (`segment_id_from`,`segment_id_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='セグメントマスタ';


--
-- Table structure for table `t_segmentmst_from_0001_to_go`
--
DROP TABLE IF EXISTS `t_segmentmst_from_0001_to_go`;
CREATE TABLE `t_segmentmst_from_0001_to_go` (
  `segment_id_from` varchar(255)    NOT NULL              COMMENT 'ユーザID',
  `segment_id_to`   varchar(255)    NOT NULL              COMMENT 'パートナーユーザID',
  `name`            varchar(255)    NOT NULL              COMMENT 'セグメント名',
  `description`     varchar(1000)           DEFAULT NULL COMMENT '備考',
  `status`          tinyint        NOT NULL DEFAULT 0    COMMENT 'ステータス',
  PRIMARY KEY (`segment_id_from`,`segment_id_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='セグメントマスタ';


--
-- Table structure for table `t_user_mapping_0001_co`
--
DROP TABLE IF EXISTS `t_user_mapping_0001_co`;
CREATE TABLE `t_user_mapping_0001_co` (
  `user_id` varchar(255) NOT NULL COMMENT 'ユーザID',
  `partner_user_id` varchar(255) NOT NULL COMMENT 'パートナーユーザID',
  `tamar_user_type` tinyint(3) unsigned NOT NULL COMMENT 'TamarユーザID種別 : 1=1st Party Cookie, 2=3rd Party Cookie, ',
  `tamar_user_id` varchar(255) NOT NULL COMMENT 'TamarユーザID',
  `inserted_at` timestamp NOT NULL DEFAULT 0 COMMENT '登録日時',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日時',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ : 0:FALSE=未削除, 1:TRUE=削除済, ',
  PRIMARY KEY (`user_id`,`partner_user_id`),
  KEY `idx_t_user_mapping_0001_co_01` (`user_id`, `updated_at`, `inserted_at`),
  KEY `idx_t_user_mapping_0001_co_02` (`partner_user_id`, `updated_at`),
  KEY `idx_t_user_mapping_0001_co_03` (`updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ユーザマッピング';

--
-- Table structure for table `w_user_mapping_0001_co`
--
DROP TABLE IF EXISTS `w_user_mapping_0001_co`;
CREATE TABLE `w_user_mapping_0001_co` (
  `user_id` varchar(255) DEFAULT NULL,
  `partner_user_id` varchar(255) DEFAULT NULL,
  `tamar_user_type` tinyint(3) unsigned DEFAULT NULL,
  `tamar_user_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `t_user_mapping_0001_go`
--
DROP TABLE IF EXISTS `t_user_mapping_0001_go`;
CREATE TABLE `t_user_mapping_0001_go` (
  `user_id` varchar(255) NOT NULL COMMENT 'ユーザID',
  `partner_user_id` varchar(255) NOT NULL COMMENT 'パートナーユーザID',
  `tamar_user_type` tinyint(3) unsigned NOT NULL COMMENT 'TamarユーザID種別 : 1=1st Party Cookie, 2=3rd Party Cookie, ',
  `tamar_user_id` varchar(255) NOT NULL COMMENT 'TamarユーザID',
  `inserted_at` timestamp NOT NULL DEFAULT 0 COMMENT '登録日時',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日時',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ : 0:FALSE=未削除, 1:TRUE=削除済, ',
  PRIMARY KEY (`user_id`,`partner_user_id`),
  KEY `idx_t_user_mapping_0001_go_01` (`user_id`, `updated_at`, `inserted_at`),
  KEY `idx_t_user_mapping_0001_go_02` (`partner_user_id`, `updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ユーザマッピング';

--
-- Table structure for table `w_user_mapping_0001_go`
--
DROP TABLE IF EXISTS `w_user_mapping_0001_go`;
CREATE TABLE `w_user_mapping_0001_go` (
  `user_id` varchar(255) DEFAULT NULL,
  `partner_user_id` varchar(255) DEFAULT NULL,
  `tamar_user_type` tinyint(3) unsigned DEFAULT NULL,
  `tamar_user_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `t_segmentmst_from_0001_to_rt0041`
--
DROP TABLE IF EXISTS `t_segmentmst_from_0001_to_rt0041`;
CREATE TABLE `t_segmentmst_from_0001_to_rt0041` (
  `segment_id_from` varchar(255)    NOT NULL              COMMENT 'ユーザID',
  `segment_id_to`   varchar(255)    NOT NULL              COMMENT 'パートナーユーザID',
  `name`            varchar(255)    NOT NULL              COMMENT 'セグメント名',
  `description`     varchar(1000)           DEFAULT NULL COMMENT '備考',
  `status`          tinyint        NOT NULL DEFAULT 0    COMMENT 'ステータス',
  PRIMARY KEY (`segment_id_from`,`segment_id_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='セグメントマスタ';

--
-- Table structure for table `t_user_mapping_0001_rt0041`
--
DROP TABLE IF EXISTS `t_user_mapping_0001_rt0041`;
CREATE TABLE `t_user_mapping_0001_rt0041` (
  `user_id` varchar(255) NOT NULL COMMENT 'ユーザID',
  `partner_user_id` varchar(255) NOT NULL COMMENT 'パートナーユーザID',
  `tamar_user_type` tinyint(3) unsigned NOT NULL COMMENT 'TamarユーザID種別 : 1=1st Party Cookie, 2=3rd Party Cookie, ',
  `tamar_user_id` varchar(255) NOT NULL COMMENT 'TamarユーザID',
  `inserted_at` timestamp NOT NULL DEFAULT 0 COMMENT '登録日時',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日時',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ : 0:FALSE=未削除, 1:TRUE=削除済, ',
  PRIMARY KEY (`user_id`,`partner_user_id`),
  KEY `idx_t_user_mapping_0001_rt0041_01` (`user_id`, `updated_at`, `inserted_at`),
  KEY `idx_t_user_mapping_0001_rt0041_02` (`partner_user_id`, `updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ユーザマッピング';

--
-- Table structure for table `w_user_mapping_0001_rt0041`
--
DROP TABLE IF EXISTS `w_user_mapping_0001_rt0041`;
CREATE TABLE `w_user_mapping_0001_rt0041` (
  `user_id` varchar(255) DEFAULT NULL,
  `partner_user_id` varchar(255) DEFAULT NULL,
  `tamar_user_type` tinyint(3) unsigned DEFAULT NULL,
  `tamar_user_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `t_segmentmst_from_deltacube_to_rt0001`
--
DROP TABLE IF EXISTS `t_segmentmst_from_deltacube_to_rt0001`;
CREATE TABLE `t_segmentmst_from_deltacube_to_rt0001` (
  `segment_id_from` varchar(255)    NOT NULL              COMMENT 'ユーザID',
  `segment_id_to`   varchar(255)    NOT NULL              COMMENT 'パートナーユーザID',
  `name`            varchar(255)    NOT NULL              COMMENT 'セグメント名',
  `description`     varchar(1000)           DEFAULT NULL COMMENT '備考',
  `status`          tinyint        NOT NULL DEFAULT 0    COMMENT 'ステータス',
  PRIMARY KEY (`segment_id_from`,`segment_id_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='セグメントマスタ';

--
-- Table structure for table `t_user_extsegdata_0001_deltacube`
--
DROP TABLE IF EXISTS `t_user_extsegdata_0001_deltacube`;
CREATE TABLE `t_user_extsegdata_0001_deltacube` (
  `user_id`    varchar(255) NOT NULL,
  `segment_id` varchar(255) DEFAULT NULL,
  `ttl`        datetime     DEFAULT NULL,
  PRIMARY KEY (`user_id`,`segment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `w_user_extsegdata_0001_deltacube`
--
DROP TABLE IF EXISTS `w_user_extsegdata_0001_deltacube`;
CREATE TABLE `w_user_extsegdata_0001_deltacube` (
  `user_id`    varchar(255) NOT NULL,
  `segment_id` varchar(255) DEFAULT NULL,
  `ttl`        datetime     DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- add GRANT to tamaruser
GRANT  SELECT,INSERT,UPDATE,DELETE,DROP,CREATE TEMPORARY TABLES ON DB_TAMAR_0001.* TO tamaruser@'%';
