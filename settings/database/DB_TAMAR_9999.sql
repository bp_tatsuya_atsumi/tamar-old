--
-- Current Database: `DB_TAMAR_9999`
--
CREATE DATABASE IF NOT EXISTS `DB_TAMAR_9999` DEFAULT CHARACTER SET utf8;

USE `DB_TAMAR_9999`;

--
-- Table structure for table `t_user_mapping_9999_bp1`
--
DROP TABLE IF EXISTS `t_user_mapping_9999_bp1`;
CREATE TABLE IF NOT EXISTS `t_user_mapping_9999_bp1` (
  `user_id` varchar(255) NOT NULL COMMENT 'ユーザID',
  `partner_user_id` varchar(255) NOT NULL COMMENT 'パートナーユーザID',
  `tamar_user_type` tinyint(3) unsigned NOT NULL COMMENT 'TamarユーザID種別 : 1=1st Party Cookie, 2=3rd Party Cookie, ',
  `tamar_user_id` varchar(255) NOT NULL COMMENT 'TamarユーザID',
  `inserted_at` timestamp NOT NULL DEFAULT 0 COMMENT '登録日時',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日時',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ : 0:FALSE=未削除, 1:TRUE=削除済, ',
  PRIMARY KEY (`user_id`,`partner_user_id`),
  KEY `idx_t_user_mapping_9999_bp1_01` (`user_id`, `updated_at`, `inserted_at`),
  KEY `idx_t_user_mapping_9999_bp1_02` (`partner_user_id`, `updated_at`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ユーザマッピング';

--
-- Table structure for table `w_user_mapping_9999_bp1`
--
DROP TABLE IF EXISTS `w_user_mapping_9999_bp1`;
CREATE TABLE IF NOT EXISTS `w_user_mapping_9999_bp1` (
  `user_id` varchar(255) DEFAULT NULL,
  `partner_user_id` varchar(255) DEFAULT NULL,
  `tamar_user_type` tinyint(3) unsigned DEFAULT NULL,
  `tamar_user_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `t_user_mapping_9999_ma`
--
DROP TABLE IF EXISTS `t_user_mapping_9999_ma`;
CREATE TABLE IF NOT EXISTS `t_user_mapping_9999_ma` (
  `user_id` varchar(255) NOT NULL COMMENT 'ユーザID',
  `partner_user_id` varchar(255) NOT NULL COMMENT 'パートナーユーザID',
  `tamar_user_type` tinyint(3) unsigned NOT NULL COMMENT 'TamarユーザID種別 : 1=1st Party Cookie, 2=3rd Party Cookie, ',
  `tamar_user_id` varchar(255) NOT NULL COMMENT 'TamarユーザID',
  `inserted_at` timestamp NOT NULL DEFAULT 0 COMMENT '登録日時',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日時',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ : 0:FALSE=未削除, 1:TRUE=削除済, ',
  PRIMARY KEY (`user_id`,`partner_user_id`),
  KEY `idx_t_user_mapping_9999_ma_01` (`user_id`, `updated_at`, `inserted_at`),
  KEY `idx_t_user_mapping_9999_ma_02` (`partner_user_id`, `updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ユーザマッピング';


--
-- Table structure for table `w_user_mapping_9999_ma`
--
DROP TABLE IF EXISTS `w_user_mapping_9999_ma`;
CREATE TABLE IF NOT EXISTS `w_user_mapping_9999_ma` (
  `user_id` varchar(255) DEFAULT NULL,
  `partner_user_id` varchar(255) DEFAULT NULL,
  `tamar_user_type` tinyint(3) unsigned DEFAULT NULL,
  `tamar_user_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `t_user_mapping_9999_ao`
--
DROP TABLE IF EXISTS `t_user_mapping_9999_ao`;
CREATE TABLE IF NOT EXISTS `t_user_mapping_9999_ao` (
  `user_id` varchar(255) NOT NULL COMMENT 'ユーザID',
  `partner_user_id` varchar(255) NOT NULL COMMENT 'パートナーユーザID',
  `tamar_user_type` tinyint(3) unsigned NOT NULL COMMENT 'TamarユーザID種別 : 1=1st Party Cookie, 2=3rd Party Cookie, ',
  `tamar_user_id` varchar(255) NOT NULL COMMENT 'TamarユーザID',
  `inserted_at` timestamp NOT NULL DEFAULT 0 COMMENT '登録日時',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日時',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ : 0:FALSE=未削除, 1:TRUE=削除済, ',
  PRIMARY KEY (`user_id`,`partner_user_id`),
  KEY `idx_t_user_mapping_9999_ao_01` (`user_id`, `updated_at`, `inserted_at`),
  KEY `idx_t_user_mapping_9999_ao_02` (`partner_user_id`, `updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ユーザマッピング';

--
-- Table structure for table `w_user_mapping_9999_ao`
--
DROP TABLE IF EXISTS `w_user_mapping_9999_ao`;
CREATE TABLE `w_user_mapping_9999_ao` (
  `user_id` varchar(255) DEFAULT NULL,
  `partner_user_id` varchar(255) DEFAULT NULL,
  `tamar_user_type` tinyint(3) unsigned DEFAULT NULL,
  `tamar_user_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `t_user_mapping_9999_so`
--
DROP TABLE IF EXISTS `t_user_mapping_9999_so`;
CREATE TABLE `t_user_mapping_9999_so` (
  `user_id` varchar(255) NOT NULL COMMENT 'ユーザID',
  `partner_user_id` varchar(255) NOT NULL COMMENT 'パートナーユーザID',
  `tamar_user_type` tinyint(3) unsigned NOT NULL COMMENT 'TamarユーザID種別 : 1=1st Party Cookie, 2=3rd Party Cookie, ',
  `tamar_user_id` varchar(255) NOT NULL COMMENT 'TamarユーザID',
  `inserted_at` timestamp NOT NULL DEFAULT 0 COMMENT '登録日時',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日時',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ : 0:FALSE=未削除, 1:TRUE=削除済, ',
  PRIMARY KEY (`user_id`,`partner_user_id`),
  KEY `idx_t_user_mapping_9999_so_01` (`user_id`, `updated_at`, `inserted_at`),
  KEY `idx_t_user_mapping_9999_so_02` (`partner_user_id`, `updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ユーザマッピング';

--
-- Table structure for table `w_user_mapping_9999_so`
--
DROP TABLE IF EXISTS `w_user_mapping_9999_so`;
CREATE TABLE `w_user_mapping_9999_so` (
  `user_id` varchar(255) DEFAULT NULL,
  `partner_user_id` varchar(255) DEFAULT NULL,
  `tamar_user_type` tinyint(3) unsigned DEFAULT NULL,
  `tamar_user_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `t_user_mapping_9999_ac`
--
DROP TABLE IF EXISTS `t_user_mapping_9999_ac`;
CREATE TABLE `t_user_mapping_9999_ac` (
  `user_id` varchar(255) NOT NULL COMMENT 'ユーザID',
  `partner_user_id` varchar(255) NOT NULL COMMENT 'パートナーユーザID',
  `tamar_user_type` tinyint(3) unsigned NOT NULL COMMENT 'TamarユーザID種別 : 1=1st Party Cookie, 2=3rd Party Cookie, ',
  `tamar_user_id` varchar(255) NOT NULL COMMENT 'TamarユーザID',
  `inserted_at` timestamp NOT NULL DEFAULT 0 COMMENT '登録日時',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日時',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ : 0:FALSE=未削除, 1:TRUE=削除済, ',
  PRIMARY KEY (`user_id`,`partner_user_id`),
  KEY `idx_t_user_mapping_9999_ac_01` (`user_id`, `updated_at`, `inserted_at`),
  KEY `idx_t_user_mapping_9999_ac_02` (`partner_user_id`, `updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ユーザマッピング';

--
-- Table structure for table `w_user_mapping_9999_ac`
--
DROP TABLE IF EXISTS `w_user_mapping_9999_ac`;
CREATE TABLE `w_user_mapping_9999_ac` (
  `user_id` varchar(255) DEFAULT NULL,
  `partner_user_id` varchar(255) DEFAULT NULL,
  `tamar_user_type` tinyint(3) unsigned DEFAULT NULL,
  `tamar_user_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `t_user_mapping_9999_co`
--
DROP TABLE IF EXISTS `t_user_mapping_9999_co`;
CREATE TABLE `t_user_mapping_9999_co` (
  `user_id` varchar(255) NOT NULL COMMENT 'ユーザID',
  `partner_user_id` varchar(255) NOT NULL COMMENT 'パートナーユーザID',
  `tamar_user_type` tinyint(3) unsigned NOT NULL COMMENT 'TamarユーザID種別 : 1=1st Party Cookie, 2=3rd Party Cookie, ',
  `tamar_user_id` varchar(255) NOT NULL COMMENT 'TamarユーザID',
  `inserted_at` timestamp NOT NULL DEFAULT 0 COMMENT '登録日時',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日時',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ : 0:FALSE=未削除, 1:TRUE=削除済, ',
  PRIMARY KEY (`user_id`,`partner_user_id`),
  KEY `idx_t_user_mapping_9999_co_01` (`user_id`, `updated_at`, `inserted_at`),
  KEY `idx_t_user_mapping_9999_co_02` (`partner_user_id`, `updated_at`),
  KEY `idx_t_user_mapping_9999_co_03` (`updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ユーザマッピング';

--
-- Table structure for table `w_user_mapping_9999_co`
--
DROP TABLE IF EXISTS `w_user_mapping_9999_co`;
CREATE TABLE `w_user_mapping_9999_co` (
  `user_id` varchar(255) DEFAULT NULL,
  `partner_user_id` varchar(255) DEFAULT NULL,
  `tamar_user_type` tinyint(3) unsigned DEFAULT NULL,
  `tamar_user_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `t_user_mapping_9999_rs`
--

DROP TABLE IF EXISTS `t_user_mapping_9999_rs`;
CREATE TABLE IF NOT EXISTS `t_user_mapping_9999_rs` (
  `user_id` varchar(255) NOT NULL COMMENT 'ユーザID',
  `partner_user_id` varchar(255) NOT NULL COMMENT 'パートナーユーザID',
  `tamar_user_type` tinyint(3) unsigned NOT NULL COMMENT 'TamarユーザID種別 : 1=1st Party Cookie, 2=3rd Party Cookie, ',
  `tamar_user_id` varchar(255) NOT NULL COMMENT 'TamarユーザID',
  `inserted_at` timestamp NOT NULL DEFAULT 0 COMMENT '登録日時',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日時',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ : 0:FALSE=未削除, 1:TRUE=削除済, ',
  PRIMARY KEY (`user_id`,`partner_user_id`),
  KEY `idx_t_user_mapping_9999_rs_01` (`user_id`, `updated_at`, `inserted_at`),
  KEY `idx_t_user_mapping_9999_rs_02` (`partner_user_id`, `updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ユーザマッピング';

--
-- Table structure for table `w_user_mapping_9999_rs`
--

DROP TABLE IF EXISTS `w_user_mapping_9999_rs`;
CREATE TABLE IF NOT EXISTS `w_user_mapping_9999_rs` (
  `user_id` varchar(255) DEFAULT NULL,
  `partner_user_id` varchar(255) DEFAULT NULL,
  `tamar_user_type` tinyint(3) unsigned DEFAULT NULL,
  `tamar_user_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `t_user_mapping_9999_im`
--
DROP TABLE IF EXISTS `t_user_mapping_9999_im`;
CREATE TABLE `t_user_mapping_9999_im` (
  `user_id` varchar(255) NOT NULL COMMENT 'ユーザID',
  `partner_user_id` varchar(255) NOT NULL COMMENT 'パートナーユーザID',
  `tamar_user_type` tinyint(3) unsigned NOT NULL COMMENT 'TamarユーザID種別 : 1=1st Party Cookie, 2=3rd Party Cookie, ',
  `tamar_user_id` varchar(255) NOT NULL COMMENT 'TamarユーザID',
  `inserted_at` timestamp NOT NULL DEFAULT 0 COMMENT '登録日時',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日時',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ : 0:FALSE=未削除, 1:TRUE=削除済, ',
  PRIMARY KEY (`user_id`,`partner_user_id`),
  KEY `idx_t_user_mapping_9999_im_01` (`user_id`, `updated_at`, `inserted_at`),
  KEY `idx_t_user_mapping_9999_im_02` (`partner_user_id`, `updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ユーザマッピング';

--
-- Table structure for table `w_user_mapping_9999_im`
--
DROP TABLE IF EXISTS `w_user_mapping_9999_im`;
CREATE TABLE `w_user_mapping_9999_im` (
  `user_id` varchar(255) DEFAULT NULL,
  `partner_user_id` varchar(255) DEFAULT NULL,
  `tamar_user_type` tinyint(3) unsigned DEFAULT NULL,
  `tamar_user_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `t_segmentmst_from_9999_to_im`
--
DROP TABLE IF EXISTS `t_segmentmst_from_9999_to_im`;
CREATE TABLE `t_segmentmst_from_9999_to_im` (
  `segment_id_from` varchar(255)    NOT NULL              COMMENT 'ユーザID',
  `segment_id_to`   varchar(255)    NOT NULL              COMMENT 'パートナーユーザID',
  `name`            varchar(255)    NOT NULL              COMMENT 'セグメント名',
  `description`     varchar(1000)           DEFAULT NULL COMMENT '備考',
  `status`          tinyint        NOT NULL DEFAULT 0    COMMENT 'ステータス',
  PRIMARY KEY (`segment_id_from`,`segment_id_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='セグメントマスタ';

--
-- Table structure for table `t_segmentmst_from_9999_to_rt9998`
--
DROP TABLE IF EXISTS `t_segmentmst_from_9999_to_rt9998`;
CREATE TABLE `t_segmentmst_from_9999_to_rt9998` (
  `segment_id_from` varchar(255)    NOT NULL              COMMENT 'ユーザID',
  `segment_id_to`   varchar(255)    NOT NULL              COMMENT 'パートナーユーザID',
  `name`            varchar(255)    NOT NULL              COMMENT 'セグメント名',
  `description`     varchar(1000)           DEFAULT NULL COMMENT '備考',
  `status`          tinyint        NOT NULL DEFAULT 0    COMMENT 'ステータス',
  PRIMARY KEY (`segment_id_from`,`segment_id_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='セグメントマスタ';

--
-- Table structure for table `t_segmentmst_from_9999_to_bp1`
--
DROP TABLE IF EXISTS `t_segmentmst_from_9999_to_bp1`;
CREATE TABLE `t_segmentmst_from_9999_to_bp1` (
  `segment_id_from` varchar(255)    NOT NULL              COMMENT 'ユーザID',
  `segment_id_to`   varchar(255)    NOT NULL              COMMENT 'パートナーユーザID',
  `name`            varchar(255)    NOT NULL              COMMENT 'セグメント名',
  `description`     varchar(1000)           DEFAULT NULL COMMENT '備考',
  `status`          tinyint        NOT NULL DEFAULT 0    COMMENT 'ステータス',
  PRIMARY KEY (`segment_id_from`,`segment_id_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='セグメントマスタ';

--
-- Table structure for table `t_segmentmst_from_9999_to_go`
--
DROP TABLE IF EXISTS `t_segmentmst_from_9999_to_go`;
CREATE TABLE `t_segmentmst_from_9999_to_go` (
  `segment_id_from` varchar(255)    NOT NULL              COMMENT 'ユーザID',
  `segment_id_to`   varchar(255)    NOT NULL              COMMENT 'パートナーユーザID',
  `name`            varchar(255)    NOT NULL              COMMENT 'セグメント名',
  `description`     varchar(1000)           DEFAULT NULL COMMENT '備考',
  `status`          tinyint        NOT NULL DEFAULT 0    COMMENT 'ステータス',
  PRIMARY KEY (`segment_id_from`,`segment_id_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='セグメントマスタ';

--
-- Table structure for table `t_segmentmst_from_bp1_to_rt9999`
--
DROP TABLE IF EXISTS `t_segmentmst_from_bp1_to_rt9999`;
CREATE TABLE `t_segmentmst_from_bp1_to_rt9999` (
  `segment_id_from` varchar(255)    NOT NULL              COMMENT 'ユーザID',
  `segment_id_to`   varchar(255)    NOT NULL              COMMENT 'パートナーユーザID',
  `name`            varchar(255)    NOT NULL              COMMENT 'セグメント名',
  `description`     varchar(1000)           DEFAULT NULL COMMENT '備考',
  `status`          tinyint        NOT NULL DEFAULT 0    COMMENT 'ステータス',
  PRIMARY KEY (`segment_id_from`,`segment_id_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='セグメントマスタ';

--
-- Table structure for table `t_user_mapping_9999_go`
--
DROP TABLE IF EXISTS `t_user_mapping_9999_go`;
CREATE TABLE `t_user_mapping_9999_go` (
  `user_id` varchar(255) NOT NULL COMMENT 'ユーザID',
  `partner_user_id` varchar(255) NOT NULL COMMENT 'パートナーユーザID',
  `tamar_user_type` tinyint(3) unsigned NOT NULL COMMENT 'TamarユーザID種別 : 1=1st Party Cookie, 2=3rd Party Cookie, ',
  `tamar_user_id` varchar(255) NOT NULL COMMENT 'TamarユーザID',
  `inserted_at` timestamp NOT NULL DEFAULT 0 COMMENT '登録日時',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日時',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ : 0:FALSE=未削除, 1:TRUE=削除済, ',
  PRIMARY KEY (`user_id`,`partner_user_id`),
  KEY `idx_t_user_mapping_9999_go_01` (`user_id`, `updated_at`, `inserted_at`),
  KEY `idx_t_user_mapping_9999_go_02` (`partner_user_id`, `updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ユーザマッピング';

--
-- Table structure for table `w_user_mapping_9999_go`
--
DROP TABLE IF EXISTS `w_user_mapping_9999_go`;
CREATE TABLE `w_user_mapping_9999_go` (
  `user_id` varchar(255) DEFAULT NULL,
  `partner_user_id` varchar(255) DEFAULT NULL,
  `tamar_user_type` tinyint(3) unsigned DEFAULT NULL,
  `tamar_user_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `t_user_mapping_9999_rt9998`
--
DROP TABLE IF EXISTS `t_user_mapping_9999_rt9998`;
CREATE TABLE `t_user_mapping_9999_rt9998` (
  `user_id` varchar(255) NOT NULL COMMENT 'ユーザID',
  `partner_user_id` varchar(255) NOT NULL COMMENT 'パートナーユーザID',
  `tamar_user_type` tinyint(3) unsigned NOT NULL COMMENT 'TamarユーザID種別 : 1=1st Party Cookie, 2=3rd Party Cookie, ',
  `tamar_user_id` varchar(255) NOT NULL COMMENT 'TamarユーザID',
  `inserted_at` timestamp NOT NULL DEFAULT 0 COMMENT '登録日時',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日時',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ : 0:FALSE=未削除, 1:TRUE=削除済, ',
  PRIMARY KEY (`user_id`,`partner_user_id`),
  KEY `idx_t_user_mapping_9999_rt9998_01` (`user_id`, `updated_at`, `inserted_at`),
  KEY `idx_t_user_mapping_9999_rt9998_02` (`partner_user_id`, `updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ユーザマッピング';

--
-- Table structure for table `t_user_mapping_9999_rt`
--
DROP TABLE IF EXISTS `t_user_mapping_9999_rt`;
CREATE TABLE `t_user_mapping_9999_rt` (
  `user_id` varchar(50) NOT NULL,
  `partner_user_id` varchar(50) NOT NULL,
  `tamar_user_type` tinyint(3) unsigned DEFAULT NULL,
  `tamar_user_id` varchar(50) DEFAULT NULL,
  `inserted_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`user_id`,`partner_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `w_user_mapping_9999_rt`
--
DROP TABLE IF EXISTS `w_user_mapping_9999_rt`;
CREATE TABLE `w_user_mapping_9999_rt` (
  `user_id` varchar(255) DEFAULT NULL,
  `partner_user_id` varchar(255) DEFAULT NULL,
  `tamar_user_type` tinyint(3) unsigned DEFAULT NULL,
  `tamar_user_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `t_segmentmst_from_im_to_rt9999`
--
DROP TABLE IF EXISTS `t_segmentmst_from_im_to_rt9999`;
CREATE TABLE `t_segmentmst_from_im_to_rt9999` (
  `segment_id_from` varchar(255)    NOT NULL              COMMENT 'ユーザID',
  `segment_id_to`   varchar(255)    NOT NULL              COMMENT 'パートナーユーザID',
  `name`            varchar(255)    NOT NULL              COMMENT 'セグメント名',
  `description`     varchar(1000)           DEFAULT NULL COMMENT '備考',
  `status`          tinyint        NOT NULL DEFAULT 0    COMMENT 'ステータス',
  PRIMARY KEY (`segment_id_from`,`segment_id_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='セグメントマスタ';

--
-- Table structure for table `t_user_extsegdata_9999_bp1`
--
DROP TABLE IF EXISTS `t_user_extsegdata_9999_bp1`;
CREATE TABLE `t_user_extsegdata_9999_bp1` (
  `user_id`    varchar(255) NOT NULL,
  `segment_id` varchar(255) DEFAULT NULL,
  `ttl`        datetime     DEFAULT NULL,
  PRIMARY KEY (`user_id`,`segment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `w_user_extsegdata_9999_bp1`
--
DROP TABLE IF EXISTS `w_user_extsegdata_9999_bp1`;
CREATE TABLE `w_user_extsegdata_9999_bp1` (
  `user_id`    varchar(255) NOT NULL,
  `segment_id` varchar(255) DEFAULT NULL,
  `ttl`        datetime     DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `t_segmentmst_from_deltacube_to_rt9999`
--
DROP TABLE IF EXISTS `t_segmentmst_from_deltacube_to_rt9999`;
CREATE TABLE `t_segmentmst_from_deltacube_to_rt9999` (
  `segment_id_from` varchar(255)    NOT NULL              COMMENT 'ユーザID',
  `segment_id_to`   varchar(255)    NOT NULL              COMMENT 'パートナーユーザID',
  `name`            varchar(255)    NOT NULL              COMMENT 'セグメント名',
  `description`     varchar(1000)           DEFAULT NULL COMMENT '備考',
  `status`          tinyint        NOT NULL DEFAULT 0    COMMENT 'ステータス',
  PRIMARY KEY (`segment_id_from`,`segment_id_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='セグメントマスタ';

--
-- Table structure for table `t_user_extsegdata_9999_deltacube`
--
DROP TABLE IF EXISTS `t_user_extsegdata_9999_deltacube`;
CREATE TABLE `t_user_extsegdata_9999_deltacube` (
  `user_id`    varchar(255) NOT NULL,
  `segment_id` varchar(255) DEFAULT NULL,
  `ttl`        datetime     DEFAULT NULL,
  PRIMARY KEY (`user_id`,`segment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `w_user_extsegdata_9999_deltacube`
--
DROP TABLE IF EXISTS `w_user_extsegdata_9999_deltacube`;
CREATE TABLE `w_user_extsegdata_9999_deltacube` (
  `user_id`    varchar(255) NOT NULL,
  `segment_id` varchar(255) DEFAULT NULL,
  `ttl`        datetime     DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `t_user_extsegdata_9999_im`
--
DROP TABLE IF EXISTS `t_user_extsegdata_9999_im`;
CREATE TABLE `t_user_extsegdata_9999_im` (
  `user_id`    varchar(255) NOT NULL,
  `segment_id` varchar(255) DEFAULT NULL,
  `ttl`        datetime     DEFAULT NULL,
  PRIMARY KEY (`user_id`,`segment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `w_user_extsegdata_9999_im`
--
DROP TABLE IF EXISTS `w_user_extsegdata_9999_im`;
CREATE TABLE `w_user_extsegdata_9999_im` (
  `user_id`    varchar(255) NOT NULL,
  `segment_id` varchar(255) DEFAULT NULL,
  `ttl`        datetime     DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- add GRANT to tamaruser
GRANT  SELECT,INSERT,UPDATE,DELETE,DROP,CREATE TEMPORARY TABLES ON DB_TAMAR_9999.* TO tamaruser@'%';

