--
-- Current Database: `DB_TAMAR_0066`
--
CREATE DATABASE IF NOT EXISTS `DB_TAMAR_0066` DEFAULT CHARACTER SET utf8;

USE `DB_TAMAR_0066`;

--
-- Table structure for table `t_segmentmst_from_deltacube_to_rt0066`
--
DROP TABLE IF EXISTS `t_segmentmst_from_deltacube_to_rt0066`;
CREATE TABLE `t_segmentmst_from_deltacube_to_rt0066` (
  `segment_id_from` varchar(255)    NOT NULL              COMMENT 'ユーザID',
  `segment_id_to`   varchar(255)    NOT NULL              COMMENT 'パートナーユーザID',
  `name`            varchar(255)    NOT NULL              COMMENT 'セグメント名',
  `description`     varchar(1000)           DEFAULT NULL COMMENT '備考',
  `status`          tinyint        NOT NULL DEFAULT 0    COMMENT 'ステータス',
  PRIMARY KEY (`segment_id_from`,`segment_id_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='セグメントマスタ';

--
-- Table structure for table `t_user_extsegdata_0066_deltacube`
--
DROP TABLE IF EXISTS `t_user_extsegdata_0066_deltacube`;
CREATE TABLE `t_user_extsegdata_0066_deltacube` (
  `user_id`    varchar(255) NOT NULL,
  `segment_id` varchar(255) DEFAULT NULL,
  `ttl`        datetime     DEFAULT NULL,
  PRIMARY KEY (`user_id`,`segment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `w_user_extsegdata_0066_deltacube`
--
DROP TABLE IF EXISTS `w_user_extsegdata_0066_deltacube`;
CREATE TABLE `w_user_extsegdata_0066_deltacube` (
  `user_id`    varchar(255) NOT NULL,
  `segment_id` varchar(255) DEFAULT NULL,
  `ttl`        datetime     DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- add GRANT to tamaruser
GRANT  SELECT,INSERT,UPDATE,DELETE,DROP,CREATE TEMPORARY TABLES ON DB_TAMAR_0066.* TO tamaruser@'%';
