
# tamarユーザ
sudo su - tamar

# 設定情報取得
git clone git@github.com:rindai87/tamar.git /usr/local/tamar/repos/tamar/

---------------------------------------
# Fluentd
## インストール
sudo curl -L http://toolbelt.treasure-data.com/sh/install-redhat.sh | sh

## プラグインインストール
sudo /usr/lib64/fluent/ruby/bin/fluent-gem install fluent-plugin-rewrite-tag-filter
sudo /usr/lib64/fluent/ruby/bin/fluent-gem install fluent-plugin-forest
sudo /usr/lib64/fluent/ruby/bin/fluent-gem install fluent-plugin-config-expander

## configファイル配置
sudo cp /usr/local/tamar/repos/tamar/settings/partner-web/td-agent/td-agent-partner-aggregator.conf /etc/td-agent/td-agent.conf

## 自動起動設定
sudo chkconfig td-agent off

## 起動
sudo service td-agent start
