# Project: Tamar

マッピング情報と呼ばれる Rtoaster の 1st party cookie と、各社 DSP の 3rd party cookie の紐付けを管理し、各 DSP へデータ連携するシステムです。

本プロジェクトは以下のロールから成り立っています：


## site (CIDR: 10.1.0.0/16)

### site-web (CIDR: 10.1.10.0/24, 10.1.11.0/24)

Rtoaster からピギーバックされるエンドポイントです。

クライアントごとにどの DSP とクッキーシンクを行うかを判断し、JavaScript を返却します。


### site-sftp (CIDR: 10.1.20.0/24, 10.1.21.0/24)

Rtoaster からのデータを受け取るサーバです。

ワークフロー（AWS SWF）はデータドリブンで処理を起動するため、各 DSP へデータ連携する指示塔の役割も果たします。

クライアントや連携先から SFTP 接続が行われるため、インスタンスの置き換え時のホストキーの変更や、SecurityGroup の接続元IP の管理に注意する必要があります。


### site-batch (CIDR: 10.1.30.0/24, 10.1.31.0/24)

実際にバッチ処理を行うサーバです。

SWF の decider, worker が動作し、各 DSP へデータ連携を行います。


## partner (CIDR: 10.2.0.0/16)

### partner-web (CIDR: 10.2.10.0/24, 10.2.11.0/24)

各社 DSP からリダイレクトされるエンドポイントです。

リクエストURI には、Rtoaster の 1st party cookie と DSP の 3rd party cookie がパラメータとして付与されており、nginx でこのログを保存します。

fluentd が常時ログを tail し、マッピング情報を partner-aggregator へ受け渡します。


### partner-aggregator (CIDR: 10.2.20.0/24, 10.2.21.0/24)

fluentd のみが常駐し、partner-web から受け渡されたデータを S3 へアップロードします。


## api (CIDR: 10.3.0.0/16)

### api-mst (CIDR: 10.3.10.0/24, 10.3.11.0/24)

パートナーからコールされ、外部セグメントマスタを Rtoaster に登録します。

### api-data (CIDR: 10.3.20.0/24, 10.3.21.0/24)

パートナーからコールされ、外部セグメントデータを api-aggregator へ受け渡します。

### api-aggregator (CIDR: 10.3.30.0/24, 10.3.31.0/24)

fluentd のみが常駐し、api-data から受け渡されたデータを S3 へアップロードします。
