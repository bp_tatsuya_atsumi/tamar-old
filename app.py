# -*- coding: utf-8 -*-

"""
ピギーバック用のコードを返却するアプリケーション
* site-web で実行される
* Rtoaster.track() のレスポンスデータに site-web へのリクエストコードが返却され、そこから呼び出される
"""

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'production'

from flask import Flask
from flask import request
from flask import Response
from conf.config import ApplicationConfig
import lib.util as util
import urllib
import sys

app = Flask(__name__, static_folder='static')

ENVIRONMENT = 'dev'
if len(sys.argv) > 1:
    ENVIRONMENT = sys.argv[1]

config                    = ApplicationConfig(ENVIRONMENT)
app.config['PARTNER_MST'] = config.partner_mst
app.config['MAPPING']     = config.mapping
app.config['CRYPTO']      = config.crypto

@app.route('/s-sync', methods=['GET'])
def sync():
    if not util.is_valid_params(request.args):
        return Response('', mimetype='text/javascript')
    if request.cookies.get('co.dnt'):
        return Response('', mimetype='text/javascript')

    sid = request.args.get('sid')
    uid = request.args.get('uid')
    pid = request.args.get('pid')

    try:
        connecting_systems = app.config['MAPPING'][sid]

        js_header    = "(function(){"
        js_header   += "var dnt=navigator.msDoNotTrack||navigator.doNotTrack;"
        js_header   += "if(dnt&&(dnt==='1'||dnt==='yes'))return;"
        js_header   += "if(document.cookie.indexOf('_rt.dnt=1')>=0)return;"
        js_template  = "var p=(document.location.protocol==='https:')?'{https_endpoint}':'{http_endpoint}';"
        js_template += "(new Image()).src=p;"
        js_footer    = "})();"

        piggyback_code = ''
        for connecting_system in connecting_systems:
            partner_info = app.config['PARTNER_MST'][connecting_system]
            if connecting_system == 'go':
                piggyback_code += "if(document.cookie.indexOf('go.itv=1')<0){"
                piggyback_code += "var dt=new Date();"
                piggyback_code += "dt.setDate(dt.getDate()+30);"
                piggyback_code += "str='go.itv=1;expires='+dt.toGMTString()+';';"
                piggyback_code += "document.cookie=str;"

            if connecting_system == 'ydmp':
                piggyback_code += "if(Rtoaster.yahoo_brand_id){"
                piggyback_code += "var p=(document.location.protocol==='https:')?"
                piggyback_code += "'" + partner_info['http_sync_endpoint'].format(uid=urllib.quote(uid)) + "'+Rtoaster.yahoo_brand_id:"
                piggyback_code += "'" + partner_info['https_sync_endpoint'].format(uid=urllib.quote(uid)) + "'+Rtoaster.yahoo_brand_id;"
                piggyback_code += "(new Image()).src=p;}"
            else:
                piggyback_code += js_template.format(http_endpoint=partner_info['http_sync_endpoint'],
                                                     https_endpoint=partner_info['https_sync_endpoint'])\
                                             .format(sid=sid, pid=pid, uid=urllib.quote(app.config['CRYPTO'].encrypt(uid)))

            if connecting_system == 'go':
                piggyback_code += '}'

        if not piggyback_code:
            return Response('', mimetype='text/javascript')

        return Response(js_header + piggyback_code + js_footer, mimetype='text/javascript')

    except:
        return Response('', mimetype='text/javascript')

@app.route('/healthcheck', methods=['GET'])
def healthcheck():
    return Response('This app is runnning now ...', mimetype='text/html')

if __name__ == '__main__':
    app.run(debug=True)
