# -*- coding: utf-8 -*-

from __future__ import absolute_import

import os
import pytest
from conf.config import BatchConfig


@pytest.fixture
def lock_dir(tmpdir):
    return _mkdir(tmpdir, 'lock')

@pytest.fixture
def tmp_transfer_dir(tmpdir):
    return _mkdir(tmpdir, 'tmp/transfer')

@pytest.fixture
def tmp_optout_dir(tmpdir):
    return _mkdir(tmpdir, 'tmp/optout')

@pytest.fixture
def tmp_mapping_dir(tmpdir):
    return _mkdir(tmpdir, 'tmp/mapping')

@pytest.fixture
def tmp_extsegdata_dir(tmpdir):
    return _mkdir(tmpdir, 'tmp/extsegdata')

@pytest.fixture
def tmp_tools_dir(tmpdir):
    return _mkdir(tmpdir, 'tmp/tools')

@pytest.fixture
def func_init_config(lock_dir, tmp_mapping_dir, tmp_extsegdata_dir, tmp_optout_dir, tmp_transfer_dir, tmp_tools_dir):
    config = BatchConfig('dev')
    config.lock_dir           = str(lock_dir)
    config.tmp_mapping_dir    = os.path.join(str(tmp_mapping_dir),    '{sid}/{pid}')
    config.tmp_extsegdata_dir = os.path.join(str(tmp_extsegdata_dir), '{sid}/{pid}')
    config.tmp_optout_dir     = os.path.join(str(tmp_optout_dir),     '{target}')
    config.tmp_transfer_dir   = os.path.join(str(tmp_transfer_dir),   '{sid}/{pid}/{data_type}')
    config.tmp_tools_dir      = os.path.join(str(tmp_tools_dir),      '{tool_name}')
    def _init_mock_config(self, env):
        for k, v in vars(config).items():
            setattr(self, k, v)
    return _init_mock_config

def _mkdir(tmpdir, dir):
    d = tmpdir
    for dirname in dir.split('/'):
        d = d.join(dirname)
        if d.check(exists=0):
            d.mkdir()
    return d
