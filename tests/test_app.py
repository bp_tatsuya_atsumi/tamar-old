# -*- coding: utf-8 -*-

from mock import Mock
import app

def init_app():
    app.app.config['TESTING'] = True
    return app.app.test_client()

def test_sync_valid_params():
    flask_app = init_app()
    rv = flask_app.get('/s-sync?sid=9999&uid=test&pid=rtoaster')
    assert rv.status_code == 200
    assert rv.content_type == 'text/javascript; charset=utf-8'
    assert rv.data != ''

def test_sync_invalid_params():
    flask_app = init_app()
    rv = flask_app.get('/s-sync?sid=9999&uid=test')
    assert rv.status_code == 200
    assert rv.content_type == 'text/javascript; charset=utf-8'
    assert rv.data == ''

def test_helthcheck():
    flask_app = init_app()
    rv = flask_app.get('/healthcheck')
    assert rv.status_code == 200
    assert rv.content_type == 'text/html; charset=utf-8'
    assert rv.data == 'This app is runnning now ...'
