# -*- coding: utf-8 -*-

from __future__ import absolute_import

import optout_rt
import contextlib
import datetime
import pytest
import gzip
from mock import patch
from conf.config import BaseConfig
from sets import Set

ENV = 'dev'
SID = '9999'

LOG_DATA1 = '2013-10-18T00:00:00+09:00\t9999\t{"remote":"-","host":"-","user":"-","method":"GET","path":"/rt_optout?uid=test1&sid=9999","code":"200","size":"43"}'
LOG_DATA2 = '2013-10-18T00:10:00+09:00\t9999\t{"remote":"-","host":"-","user":"-","method":"GET","path":"/rt_optout?uid=test2&sid=9999","code":"200","size":"43"}'

@pytest.fixture
def log_dir(tmp_optout_dir):
    return tmp_optout_dir.mkdir('rt').mkdir('log')

class CurrentDatetime(datetime.datetime):
    @classmethod
    def now(cls):
        return cls(2013, 10, 19, 23, 59, 59, 0)

@contextlib.contextmanager
def _patches_with_default_action(func_init_config):
    with contextlib.nested (
        patch('datetime.datetime', CurrentDatetime),
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('lib.s3util.S3Util.download_files'),
        patch('shutil.rmtree'),
        patch('os.makedirs'),
        patch('lib.mappingtable.MappingTable.delete_rows_by_user_ids'),
    ) as (m_today, m_config, m_s3download, m_rmtree, m_mkdirs, m_delete):
        m_s3download.return_value = []
        m_delete.return_value     = 0
        yield (m_s3download, m_delete)

def _create_gzip(file, lines):
    with gzip.open(file, 'wb') as f:
        f.write('\n'.join(lines))

def test_execute(log_dir, func_init_config):

    # prepare
    data_file = log_dir.join('test_file.gz')
    _create_gzip(str(data_file), [LOG_DATA1])
    config      = BaseConfig('dev')
    num_of_pids = len([pid for pid in config.mapping[SID].keys() if pid != 'co' and not config.partner_mst[pid]['option'].get('no_mapping')])

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3download, m_delete = mocks
        m_s3download.return_value = [{ 'is_dir'  : False,
                                       'is_file' : True,
                                       'key'     : 'test_file.gz' }]
        optout_rt_obj = optout_rt.RtOptout(SID, ENV)
        optout_rt_obj.execute()

    # assert
    assert m_s3download.call_count == 1
    assert m_s3download.call_args_list[0][0] == (u'dev-partner-log', u'optout/rt/9999/2013/10/18/', str(log_dir))
    assert m_delete.call_count == num_of_pids
    for i in range(num_of_pids):
        assert m_delete.call_args_list[i][0][0] == ['test1']

def test_execute_with_several_logs(log_dir, func_init_config):

    # prepare
    data_file1 = log_dir.join('test_file1.gz')
    _create_gzip(str(data_file1), [LOG_DATA1])
    data_file2 = log_dir.join('test_file2.gz')
    _create_gzip(str(data_file2), [LOG_DATA2])
    config      = BaseConfig('dev')
    num_of_pids = len([pid for pid in config.mapping[SID].keys() if pid != 'co' and not config.partner_mst[pid]['option'].get('no_mapping')])

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3download, m_delete = mocks
        m_s3download.return_value = [{ 'is_dir'  : False,
                                       'is_file' : True,
                                       'key'     : 'test_file1.gz' },
                                     { 'is_dir'  : False,
                                       'is_file' : True,
                                       'key'     : 'test_file2.gz' } ]
        optout_rt_obj = optout_rt.RtOptout(SID, ENV)
        optout_rt_obj.execute()

    # assert
    assert m_s3download.call_count == 1
    assert m_s3download.call_args_list[0][0] == (u'dev-partner-log', u'optout/rt/9999/2013/10/18/', str(log_dir))
    assert m_delete.call_count == num_of_pids
    for i in range(num_of_pids):
        assert m_delete.call_args_list[i][0][0] == ['test1', 'test2']

def test_execute_specify_day(log_dir, func_init_config):

    # prepare
    data_file = log_dir.join('test_file.gz')
    _create_gzip(str(data_file), [LOG_DATA1])
    config      = BaseConfig('dev')
    num_of_pids = len([pid for pid in config.mapping[SID].keys() if pid != 'co' and not config.partner_mst[pid]['option'].get('no_mapping')])

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3download, m_delete = mocks
        m_s3download.return_value = [{ 'is_dir'  : False,
                                       'is_file' : True,
                                       'key'     : 'test_file.gz' }]
        optout_rt.RtOptout(SID, ENV, '20991231').execute()

    # assert
    assert m_s3download.call_count == 1
    assert m_s3download.call_args_list[0][0] == (u'dev-partner-log', u'optout/rt/9999/2099/12/31/', str(log_dir))
    assert m_delete.call_count == num_of_pids

def test_execute_no_logs(log_dir, func_init_config):

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3download, m_delete = mocks
        m_s3download.return_value = []
        optout_rt.RtOptout(SID, ENV).execute()

    # assert
    assert m_s3download.call_count == 1
    assert m_s3download.call_args_list[0][0] == (u'dev-partner-log', u'optout/rt/9999/2013/10/18/', str(log_dir))
    assert m_delete.call_count == 0

