# -*- coding: utf-8 -*-

from __future__ import absolute_import

from lib.s3util import S3Util

import contextlib
import boto.s3.key
from   boto.exception import S3ResponseError
from   boto.utils     import compute_md5
from   mock           import patch
import pytest


S3BUCKET = 's3bucket'
S3DIR    = 's3dir'


def test_key_to_dict():
    key = boto.s3.key.Key(name='test')
    key.size = 10
    result = S3Util()._key_to_dict(key)
    assert result == {
                         'key'           : 'test',
                         'is_dir'        : False,
                         'is_file'       : True,
                         'size'          : 10,
                         'last_modified' : 'None'
                     }

def test_key_to_dict_unicode():
    key = boto.s3.key.Key(name=u'テスト')
    key.size = 10
    result = S3Util()._key_to_dict(key)
    assert result == {
                         'key'           : 'テスト',
                         'is_dir'        : False,
                         'is_file'       : True,
                         'size'          : 10,
                         'last_modified' : 'None'
                     }

def test_upload_single_file(tmpdir):
    upload_file = tmpdir.join('test.txt')
    with open(str(upload_file), 'wb') as f:
        f.write('test')

    with contextlib.nested (
        patch('time.sleep'),
        patch('boto.s3.key.Key.set_contents_from_filename')
    ) as (m_sleep, m_put):
        S3Util().upload_single_file(str(upload_file), S3BUCKET, S3DIR)

    assert m_put.call_count == 1
    assert m_put.call_args_list[0][0] == (str(upload_file),)
    assert m_put.call_args_list[0][1] == {'md5': compute_md5(open(str(upload_file)))}

def test_upload_single_file_retry(tmpdir):
    upload_file = tmpdir.join('test.txt')
    with open(str(upload_file), 'wb') as f:
        f.write('test')

    with contextlib.nested (
        patch('time.sleep'),
        patch('boto.s3.key.Key.set_contents_from_filename')
    ) as (m_sleep, m_put):
        m_put.side_effect = [
            S3ResponseError(None, None, None),
            S3ResponseError(None, None, None),
            S3ResponseError(None, None, None),
            None
        ]
        S3Util().upload_single_file(str(upload_file), S3BUCKET, S3DIR)

    assert m_put.call_count == 4
    assert m_put.call_args_list[0][0] == (str(upload_file),)
    assert m_put.call_args_list[0][1] == {'md5': compute_md5(open(str(upload_file)))}
    assert m_put.call_args_list[1][0] == (str(upload_file),)
    assert m_put.call_args_list[1][1] == {'md5': compute_md5(open(str(upload_file)))}
    assert m_put.call_args_list[2][0] == (str(upload_file),)
    assert m_put.call_args_list[2][1] == {'md5': compute_md5(open(str(upload_file)))}
    assert m_put.call_args_list[3][0] == (str(upload_file),)
    assert m_put.call_args_list[3][1] == {'md5': compute_md5(open(str(upload_file)))}

def test_upload_single_file_retry_over(tmpdir):
    upload_file = tmpdir.join('test.txt')
    with open(str(upload_file), 'wb') as f:
        f.write('test')

    with contextlib.nested (
        patch('time.sleep'),
        patch('boto.s3.key.Key.set_contents_from_filename')
    ) as (m_sleep, m_put):
        m_put.side_effect = S3ResponseError(None, None, None)
        with pytest.raises(S3ResponseError):
            S3Util().upload_single_file(str(upload_file), S3BUCKET, S3DIR)

    assert m_put.call_count == 6
    assert m_put.call_args_list[0][0] == (str(upload_file),)
    assert m_put.call_args_list[0][1] == {'md5': compute_md5(open(str(upload_file)))}
    assert m_put.call_args_list[1][0] == (str(upload_file),)
    assert m_put.call_args_list[1][1] == {'md5': compute_md5(open(str(upload_file)))}
    assert m_put.call_args_list[2][0] == (str(upload_file),)
    assert m_put.call_args_list[2][1] == {'md5': compute_md5(open(str(upload_file)))}
    assert m_put.call_args_list[3][0] == (str(upload_file),)
    assert m_put.call_args_list[3][1] == {'md5': compute_md5(open(str(upload_file)))}
    assert m_put.call_args_list[4][0] == (str(upload_file),)
    assert m_put.call_args_list[4][1] == {'md5': compute_md5(open(str(upload_file)))}
    assert m_put.call_args_list[5][0] == (str(upload_file),)
    assert m_put.call_args_list[5][1] == {'md5': compute_md5(open(str(upload_file)))}
