# -*- coding: utf-8 -*-

from datetime import datetime
from datetime import timedelta
from lib.segmentmsttable import SegmentMstTable
from mock import patch
import contextlib
import MySQLdb
import pytest

def init():
    DB_USER  = 'tamaruser'
    DB_PASS  = '#!TawaR?u5er'
    DB_HOST  = 'localhost'
    DB_NAME  = 'DB_TAMAR_9999'
    TBL_NAME = 't_segmentmst_from_9999_to_bp1'

    segmentmst_table = SegmentMstTable(DB_USER, DB_PASS, DB_HOST, DB_NAME, TBL_NAME)
    segmentmst_table.truncate_table()
    sample_data = [{
                    'segment_id_from' : 'rt-segid1',
                    'segment_id_to'   : 'pt-segid1',
                    'name'            : u'セグメント1',
                    'description'     : u'備考1',
                    'status'          : 0
                   },{
                    'segment_id_from' : 'rt-segid2',
                    'segment_id_to'   : 'pt-segid2',
                    'name'            : 'セグメント2',
                    'description'     : '備考2',
                    'status'          : 0
                   },{
                    'segment_id_from' : 'rt-segid3',
                    'segment_id_to'   : 'pt-segid3',
                    'name'            : 'セグメント3',
                    'description'     : '備考3',
                    'status'          : 1
                   }]

    for data in sample_data:
        segmentmst_table.insert_row(data['segment_id_from'],
                                    data['segment_id_to'],
                                    data['name'],
                                    data['description'],
                                    data['status'])
    return segmentmst_table


def test_select_row_by_segment_id():
    segmentmst_table = init()
    segment_id       = 'rt-segid1'
    result           = segmentmst_table.select_row_by_segment_id(segment_id)
    assert result[0] == 'rt-segid1'
    assert result[1] == 'pt-segid1'
    assert result[2] == u'セグメント1'
    assert result[3] == u'備考1'
    assert result[4] == 0

def test_select_row_by_segment_id_when_not_exist_segment_id():
    segmentmst_table = init()
    segment_id       = 'not-exist-segment-id'
    result           = segmentmst_table.select_row_by_segment_id(segment_id)
    assert result == None

def test_select_all_rows():
    segmentmst_table = init()
    result           = segmentmst_table.select_all_rows()
    assert len(result) == 3

def test_insert_row():
    segmentmst_table = init()

    insert_data = {
                    'segment_id_from' : 'rt-sample4',
                    'segment_id_to'   : 'pt-sample4',
                    'name'            : u'セグメント4',
                    'description'     : u'備考4',
                    'status'          : 0
                  }
    result = segmentmst_table.insert_row(insert_data['segment_id_from'],
                                         insert_data['segment_id_to'],
                                         insert_data['name'],
                                         insert_data['description'],
                                         insert_data['status'])
    assert result == True

    result = segmentmst_table.select_row_by_segment_id(insert_data['segment_id_from'])
    assert result[0] == insert_data['segment_id_from']
    assert result[1] == insert_data['segment_id_to']
    assert result[2] == insert_data['name']
    assert result[3] == insert_data['description']
    assert result[4] == insert_data['status']

def test_update_row_by_segment_id():
    segmentmst_table = init()
    update_data = {
                    'name'        : u'セグメント1-1',
                    'description' : u'備考1-1',
                    'status'      : 1
                  }
    result = segmentmst_table.update_row_by_segment_id('rt-segid1', update_data)
    assert result == 1

    result = segmentmst_table.select_row_by_segment_id('rt-segid1')
    assert result[0] == 'rt-segid1'
    assert result[1] == 'pt-segid1'
    assert result[2] == u'セグメント1-1'
    assert result[3] == u'備考1-1'
    assert result[4] == 1

def test_update_row_by_segment_id_when_not_exist_segment_id():
    segmentmst_table = init()
    update_data = {
                    'name'               : u'セグメント1-1',
                    'description'        : u'備考1-1',
                    'status'             : 1
                  }
    result = segmentmst_table.update_row_by_segment_id('not-exist-segid', update_data)
    assert result == 0

def test_delete_rows_by_segment_id():
    segment_id = 'rt-segid1'
    with init() as segmentmst_table:
        records       = segmentmst_table.select_row_by_segment_id(segment_id)
        deleted_count = segmentmst_table.delete_rows_by_segment_id(segment_id)
        empty_record  = segmentmst_table.select_row_by_segment_id(segment_id)
    assert deleted_count == 1
    assert empty_record  == None

def test_delete_rows_by_segment_id_with_error():
    segment_id = 'rt-segid1'
    with contextlib.nested (
        patch('lib.segmentmsttable.SegmentMstTable.delete_rows_by_segment_id'),
        patch('sqlalchemy.engine.Transaction.rollback'),
    ) as (m_delete, m_rollback):
        m_delete.side_effect = MySQLdb.Error()
        with pytest.raises(MySQLdb.Error):
            with init() as segmentmst_table:
                segmentmst_table.delete_rows_by_segment_id(segment_id)
        assert m_delete.call_count   == 1
        assert m_rollback.call_count == 1

def test_delete_rows_by_segment_ids():
    segment_id1 = 'rt-segid1'
    segment_id2 = 'rt-segid2'
    with init() as segmentmst_table:
        deleted_count = segmentmst_table.delete_rows_by_segment_ids([segment_id1, segment_id2])
        empty_record1 = segmentmst_table.select_row_by_segment_id(segment_id1)
        empty_record2 = segmentmst_table.select_row_by_segment_id(segment_id2)
    assert deleted_count == 2
    assert empty_record1 == None
    assert empty_record2 == None

def test_delete_rows_by_segment_id_when_not_exist_segment_id():
    segment_id = 'not-exist-user-id'
    with init() as segmentmst_table:
        deleted_count = segmentmst_table.delete_rows_by_segment_id(segment_id)
    assert deleted_count == 0

def test_truncate_table():
    segmentmst_table = init()
    segmentmst_table.truncate_table()
    segment_id = 'segment_id'
    result = segmentmst_table.select_row_by_segment_id(segment_id)
    assert result == None

