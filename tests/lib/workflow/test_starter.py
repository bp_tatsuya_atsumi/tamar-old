# -*- coding: utf-8 -*-


from   boto.exception       import SWFResponseError
from   conf.config          import BatchConfig
import contextlib
import datetime
import lib.workflow.starter as     starter
import json
from   mock                 import patch
import pytest


SID              = '9999'
PID              = 'bp1'
ENV              = 'dev'
DATATYPE         = 'segmentdata'
DATATYPE_RAWDATA = 'rawdata'
S3BUCKET         = 's3bucket'
S3FILE           = 's3file'
DOMAIN           = 'DummyDomain'
VERSION          = '1.01'
WFID             = 'DummyWorkflowId'
RUNID            = 'DummyRunId'


#---------------------------------------------------------------------------
#   Mocks and patches
#---------------------------------------------------------------------------
class MockDatetime(datetime.datetime):
    @classmethod
    def now(cls):
        return cls(2013, 10, 19, 23, 59, 59, 0)

@contextlib.contextmanager
def _patches():
    def _mock_init_config():
        config = BatchConfig(ENV)
        config.workflow = {}
        config.workflow['domain']  = DOMAIN
        config.workflow['version'] = VERSION
        config.workflow['receivedatatype_to_mainwftype'] = {DATATYPE        : 'TransferWorkflow',
                                                            DATATYPE_RAWDATA: 'CreateRawdataMainWorkflow'
                                                            }
        config.workflow['receivedatatype_to_mainlatestwftype'] = {DATATYPE: 'TransferWorkflow'}
        def __mock_init_config(self, env):
            for k, v in vars(config).items():
                setattr(self, k, v)
        return __mock_init_config

    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=_mock_init_config()),
        patch('lib.workflow.util.create_workflow_id'),
        patch('boto.swf.layer1.Layer1.start_workflow_execution'),
        patch('datetime.datetime', MockDatetime)
    ) as (mock_config, mock_wfid, mock_startwf, mock_datetime):
        mock_wfid.return_value    = WFID
        mock_startwf.return_value = {'runId': RUNID}

        yield (mock_wfid, mock_startwf)


#---------------------------------------------------------------------------
#   Test for start_mapping
#---------------------------------------------------------------------------
def test_start_mapping():
    with _patches() as (mock_wfid, mock_startwf):
        assert starter.start_mapping(ENV) == RUNID

        assert mock_wfid.call_count == 1
        assert mock_wfid.call_args_list[0][0] == ('MappingMainWorkflow', VERSION)

        assert mock_startwf.call_count == 1
        assert mock_startwf.call_args_list[0][1]['domain']            == DOMAIN
        assert mock_startwf.call_args_list[0][1]['workflow_id']       == WFID
        assert mock_startwf.call_args_list[0][1]['workflow_name']     == 'MappingMainWorkflow'
        assert mock_startwf.call_args_list[0][1]['workflow_version']  == VERSION
        assert json.loads(mock_startwf.call_args_list[0][1]['input']) == {'env': ENV, 'target_hour': '2013101922'}

def test_start_mapping_specify_hour():
    with _patches() as (mock_wfid, mock_startwf):
        assert starter.start_mapping(ENV, '2014012022') == RUNID

        assert mock_wfid.call_count == 1
        assert mock_wfid.call_args_list[0][0] == ('MappingMainWorkflow', VERSION)

        assert mock_startwf.call_count == 1
        assert mock_startwf.call_args_list[0][1]['domain']            == DOMAIN
        assert mock_startwf.call_args_list[0][1]['workflow_id']       == WFID
        assert mock_startwf.call_args_list[0][1]['workflow_name']     == 'MappingMainWorkflow'
        assert mock_startwf.call_args_list[0][1]['workflow_version']  == VERSION
        assert json.loads(mock_startwf.call_args_list[0][1]['input']) == {'env': ENV, 'target_hour': '2014012022'}

def test_start_mapping_swferror():
    with _patches() as (mock_wfid, mock_startwf):
        mock_startwf.side_effect = SWFResponseError(status='status',
                                                    reason='reason',
                                                    body={'message': 'SWFError'})
        with pytest.raises(SWFResponseError):
            starter.start_mapping(ENV)

        assert mock_wfid.call_count == 1
        assert mock_wfid.call_args_list[0][0] == ('MappingMainWorkflow', VERSION)

        assert mock_startwf.call_count == 1
        assert mock_startwf.call_args_list[0][1]['domain']            == DOMAIN
        assert mock_startwf.call_args_list[0][1]['workflow_id']       == WFID
        assert mock_startwf.call_args_list[0][1]['workflow_name']     == 'MappingMainWorkflow'
        assert mock_startwf.call_args_list[0][1]['workflow_version']  == VERSION
        assert json.loads(mock_startwf.call_args_list[0][1]['input']) == {'env': ENV, 'target_hour': '2013101922'}

def test_start_mapping_unexpectederror():
    with _patches() as (mock_wfid, mock_startwf):
        mock_startwf.side_effect = Exception('Failed to start workflow')
        with pytest.raises(Exception):
            starter.start_mapping(ENV)

        assert mock_wfid.call_count == 1
        assert mock_wfid.call_args_list[0][0] == ('MappingMainWorkflow', VERSION)

        assert mock_startwf.call_count == 1
        assert mock_startwf.call_args_list[0][1]['domain']            == DOMAIN
        assert mock_startwf.call_args_list[0][1]['workflow_id']       == WFID
        assert mock_startwf.call_args_list[0][1]['workflow_name']     == 'MappingMainWorkflow'
        assert mock_startwf.call_args_list[0][1]['workflow_version']  == VERSION
        assert json.loads(mock_startwf.call_args_list[0][1]['input']) == {'env': ENV, 'target_hour': '2013101922'}


#---------------------------------------------------------------------------
#   Test for start_bridge
#---------------------------------------------------------------------------
def test_start_bridge():
    with _patches() as (mock_wfid, mock_startwf):
        assert starter.start_bridge(ENV) == RUNID

        assert mock_wfid.call_count == 1
        assert mock_wfid.call_args_list[0][0] == ('BridgeMainWorkflow', VERSION)

        assert mock_startwf.call_count == 1
        assert mock_startwf.call_args_list[0][1]['domain']            == DOMAIN
        assert mock_startwf.call_args_list[0][1]['workflow_id']       == WFID
        assert mock_startwf.call_args_list[0][1]['workflow_name']     == 'BridgeMainWorkflow'
        assert mock_startwf.call_args_list[0][1]['workflow_version']  == VERSION
        assert json.loads(mock_startwf.call_args_list[0][1]['input']) == {'env': ENV, 'target_hour': '2013101921'}

def test_start_bridge_specify_hour():
    with _patches() as (mock_wfid, mock_startwf):
        assert starter.start_bridge(ENV, '2014012022') == RUNID

        assert mock_wfid.call_count == 1
        assert mock_wfid.call_args_list[0][0] == ('BridgeMainWorkflow', VERSION)

        assert mock_startwf.call_count == 1
        assert mock_startwf.call_args_list[0][1]['domain']            == DOMAIN
        assert mock_startwf.call_args_list[0][1]['workflow_id']       == WFID
        assert mock_startwf.call_args_list[0][1]['workflow_name']     == 'BridgeMainWorkflow'
        assert mock_startwf.call_args_list[0][1]['workflow_version']  == VERSION
        assert json.loads(mock_startwf.call_args_list[0][1]['input']) == {'env': ENV, 'target_hour': '2014012022'}

def test_start_bridge_swferror():
    with _patches() as (mock_wfid, mock_startwf):
        mock_startwf.side_effect = SWFResponseError(status='status',
                                                    reason='reason',
                                                    body={'message': 'SWFError'})
        with pytest.raises(SWFResponseError):
            starter.start_bridge(ENV)

        assert mock_wfid.call_count == 1
        assert mock_wfid.call_args_list[0][0] == ('BridgeMainWorkflow', VERSION)

        assert mock_startwf.call_count == 1
        assert mock_startwf.call_args_list[0][1]['domain']            == DOMAIN
        assert mock_startwf.call_args_list[0][1]['workflow_id']       == WFID
        assert mock_startwf.call_args_list[0][1]['workflow_name']     == 'BridgeMainWorkflow'
        assert mock_startwf.call_args_list[0][1]['workflow_version']  == VERSION
        assert json.loads(mock_startwf.call_args_list[0][1]['input']) == {'env': ENV, 'target_hour': '2013101921'}

def test_start_bridge_unexpectederror():
    with _patches() as (mock_wfid, mock_startwf):
        mock_startwf.side_effect = Exception('Failed to start workflow')
        with pytest.raises(Exception):
            starter.start_bridge(ENV)

        assert mock_wfid.call_count == 1
        assert mock_wfid.call_args_list[0][0] == ('BridgeMainWorkflow', VERSION)

        assert mock_startwf.call_count == 1
        assert mock_startwf.call_args_list[0][1]['domain']            == DOMAIN
        assert mock_startwf.call_args_list[0][1]['workflow_id']       == WFID
        assert mock_startwf.call_args_list[0][1]['workflow_name']     == 'BridgeMainWorkflow'
        assert mock_startwf.call_args_list[0][1]['workflow_version']  == VERSION
        assert json.loads(mock_startwf.call_args_list[0][1]['input']) == {'env': ENV, 'target_hour': '2013101921'}


#---------------------------------------------------------------------------
#   Test for start_mappingreport
#---------------------------------------------------------------------------
def test_start_mappingreport():
    with _patches() as (mock_wfid, mock_startwf):
        assert starter.start_mappingreport(ENV) == RUNID

        assert mock_wfid.call_count == 1
        assert mock_wfid.call_args_list[0][0] == ('MappingReportMainWorkflow', VERSION)

        assert mock_startwf.call_count == 1
        assert mock_startwf.call_args_list[0][1]['domain']            == DOMAIN
        assert mock_startwf.call_args_list[0][1]['workflow_id']       == WFID
        assert mock_startwf.call_args_list[0][1]['workflow_name']     == 'MappingReportMainWorkflow'
        assert mock_startwf.call_args_list[0][1]['workflow_version']  == VERSION
        assert json.loads(mock_startwf.call_args_list[0][1]['input']) == {'env': ENV}

def test_start_mappingreport_swferror():
    with _patches() as (mock_wfid, mock_startwf):
        mock_startwf.side_effect = SWFResponseError(status='status',
                                                    reason='reason',
                                                    body={'message': 'SWFError'})
        with pytest.raises(SWFResponseError):
            starter.start_mappingreport(ENV)

        assert mock_wfid.call_count == 1
        assert mock_wfid.call_args_list[0][0] == ('MappingReportMainWorkflow', VERSION)

        assert mock_startwf.call_count == 1
        assert mock_startwf.call_args_list[0][1]['domain']            == DOMAIN
        assert mock_startwf.call_args_list[0][1]['workflow_id']       == WFID
        assert mock_startwf.call_args_list[0][1]['workflow_name']     == 'MappingReportMainWorkflow'
        assert mock_startwf.call_args_list[0][1]['workflow_version']  == VERSION
        assert json.loads(mock_startwf.call_args_list[0][1]['input']) == {'env': ENV}

def test_start_mappingreport_unexpectederror():
    with _patches() as (mock_wfid, mock_startwf):
        mock_startwf.side_effect = Exception('Failed to start workflow')
        with pytest.raises(Exception):
            starter.start_mappingreport(ENV)

        assert mock_wfid.call_count == 1
        assert mock_wfid.call_args_list[0][0] == ('MappingReportMainWorkflow', VERSION)

        assert mock_startwf.call_count == 1
        assert mock_startwf.call_args_list[0][1]['domain']            == DOMAIN
        assert mock_startwf.call_args_list[0][1]['workflow_id']       == WFID
        assert mock_startwf.call_args_list[0][1]['workflow_name']     == 'MappingReportMainWorkflow'
        assert mock_startwf.call_args_list[0][1]['workflow_version']  == VERSION
        assert json.loads(mock_startwf.call_args_list[0][1]['input']) == {'env': ENV}


#---------------------------------------------------------------------------
#   Test for start_transfer
#---------------------------------------------------------------------------
def test_start_transfer():
    with _patches() as (mock_wfid, mock_startwf):
        assert starter.start_transfer(SID, ENV, DATATYPE, S3BUCKET, S3FILE) == RUNID

        assert mock_wfid.call_count == 1
        assert mock_wfid.call_args_list[0][0] == ('TransferWorkflow', VERSION, SID)

        assert mock_startwf.call_count == 1
        assert mock_startwf.call_args_list[0][1]['domain']            == DOMAIN
        assert mock_startwf.call_args_list[0][1]['workflow_id']       == WFID
        assert mock_startwf.call_args_list[0][1]['workflow_name']     == 'TransferWorkflow'
        assert mock_startwf.call_args_list[0][1]['workflow_version']  == VERSION
        assert json.loads(mock_startwf.call_args_list[0][1]['input']) == {'sid': SID, 'env': ENV, 'data_type': DATATYPE, 's3bucket': S3BUCKET, 's3file': S3FILE}

def test_start_transfer_unknown_datatype():
    with _patches() as (mock_wfid, mock_startwf):
        with pytest.raises(ValueError):
            starter.start_transfer(SID, ENV, 'test', S3BUCKET, S3FILE)

        assert mock_wfid.call_count    == 0
        assert mock_startwf.call_count == 0

def test_start_transfer_swferror():
    with _patches() as (mock_wfid, mock_startwf):
        mock_startwf.side_effect = SWFResponseError(status='status',
                                                    reason='reason',
                                                    body={'message': 'SWFError'})
        with pytest.raises(SWFResponseError):
            starter.start_transfer(SID, ENV, DATATYPE, S3BUCKET, S3FILE)

        assert mock_wfid.call_count == 1
        assert mock_wfid.call_args_list[0][0] == ('TransferWorkflow', VERSION, SID)

        assert mock_startwf.call_count == 1
        assert mock_startwf.call_args_list[0][1]['domain']            == DOMAIN
        assert mock_startwf.call_args_list[0][1]['workflow_id']       == WFID
        assert mock_startwf.call_args_list[0][1]['workflow_name']     == 'TransferWorkflow'
        assert mock_startwf.call_args_list[0][1]['workflow_version']  == VERSION
        assert json.loads(mock_startwf.call_args_list[0][1]['input']) == {'sid': SID, 'env': ENV, 'data_type': DATATYPE, 's3bucket': S3BUCKET, 's3file': S3FILE}

def test_start_transfer_unexpectederror():
    with _patches() as (mock_wfid, mock_startwf):
        mock_startwf.side_effect = Exception('Failed to start workflow')
        with pytest.raises(Exception):
            starter.start_transfer(SID, ENV, DATATYPE, S3BUCKET, S3FILE)

        assert mock_wfid.call_count == 1
        assert mock_wfid.call_args_list[0][0] == ('TransferWorkflow', VERSION, SID)

        assert mock_startwf.call_count == 1
        assert mock_startwf.call_args_list[0][1]['domain']            == DOMAIN
        assert mock_startwf.call_args_list[0][1]['workflow_id']       == WFID
        assert mock_startwf.call_args_list[0][1]['workflow_name']     == 'TransferWorkflow'
        assert mock_startwf.call_args_list[0][1]['workflow_version']  == VERSION
        assert json.loads(mock_startwf.call_args_list[0][1]['input']) == {'sid': SID, 'env': ENV, 'data_type': DATATYPE, 's3bucket': S3BUCKET, 's3file': S3FILE}


#---------------------------------------------------------------------------
#   Test for start_transferlatest
#---------------------------------------------------------------------------
def test_start_transferlatest():
    with _patches() as (mock_wfid, mock_startwf):
        assert starter.start_transferlatest(SID, PID, ENV, DATATYPE) == RUNID

        assert mock_wfid.call_count == 1
        assert mock_wfid.call_args_list[0][0] == ('TransferWorkflow', VERSION, SID, PID)

        assert mock_startwf.call_count == 1
        assert mock_startwf.call_args_list[0][1]['domain']            == DOMAIN
        assert mock_startwf.call_args_list[0][1]['workflow_id']       == WFID
        assert mock_startwf.call_args_list[0][1]['workflow_name']     == 'TransferWorkflow'
        assert mock_startwf.call_args_list[0][1]['workflow_version']  == VERSION
        assert json.loads(mock_startwf.call_args_list[0][1]['input']) == {'sid': SID, 'pid': PID, 'env': ENV, 'data_type': DATATYPE}

def test_start_transferlatest_unknown_datatype():
    with _patches() as (mock_wfid, mock_startwf):
        with pytest.raises(ValueError):
            starter.start_transferlatest(SID, PID, ENV, 'test')

        assert mock_wfid.call_count    == 0
        assert mock_startwf.call_count == 0

def test_start_transferlatest_swferror():
    with _patches() as (mock_wfid, mock_startwf):
        mock_startwf.side_effect = SWFResponseError(status='status',
                                                    reason='reason',
                                                    body={'message': 'SWFError'})
        with pytest.raises(SWFResponseError):
            starter.start_transferlatest(SID, PID, ENV, DATATYPE)

        assert mock_wfid.call_count == 1
        assert mock_wfid.call_args_list[0][0] == ('TransferWorkflow', VERSION, SID, PID)

        assert mock_startwf.call_count == 1
        assert mock_startwf.call_args_list[0][1]['domain']            == DOMAIN
        assert mock_startwf.call_args_list[0][1]['workflow_id']       == WFID
        assert mock_startwf.call_args_list[0][1]['workflow_name']     == 'TransferWorkflow'
        assert mock_startwf.call_args_list[0][1]['workflow_version']  == VERSION
        assert json.loads(mock_startwf.call_args_list[0][1]['input']) == {'sid': SID, 'pid': PID, 'env': ENV, 'data_type': DATATYPE}

def test_start_transferlatest_unexpectederror():
    with _patches() as (mock_wfid, mock_startwf):
        mock_startwf.side_effect = Exception('Failed to start workflow')
        with pytest.raises(Exception):
            starter.start_transferlatest(SID, PID, ENV, DATATYPE)

        assert mock_wfid.call_count == 1
        assert mock_wfid.call_args_list[0][0] == ('TransferWorkflow', VERSION, SID, PID)

        assert mock_startwf.call_count == 1
        assert mock_startwf.call_args_list[0][1]['domain']            == DOMAIN
        assert mock_startwf.call_args_list[0][1]['workflow_id']       == WFID
        assert mock_startwf.call_args_list[0][1]['workflow_name']     == 'TransferWorkflow'
        assert mock_startwf.call_args_list[0][1]['workflow_version']  == VERSION
        assert json.loads(mock_startwf.call_args_list[0][1]['input']) == {'sid': SID, 'pid': PID, 'env': ENV, 'data_type': DATATYPE}


#---------------------------------------------------------------------------
#   Test for start_tombsend
#---------------------------------------------------------------------------
def test_start_tombsend():
    with _patches() as (mock_wfid, mock_startwf):
        assert starter.start_tombsend(ENV) == RUNID

        assert mock_wfid.call_count == 1
        assert mock_wfid.call_args_list[0][0] == ('TombSendMainWorkflow', VERSION)

        assert mock_startwf.call_count == 1
        assert mock_startwf.call_args_list[0][1]['domain']            == DOMAIN
        assert mock_startwf.call_args_list[0][1]['workflow_id']       == WFID
        assert mock_startwf.call_args_list[0][1]['workflow_name']     == 'TombSendMainWorkflow'
        assert mock_startwf.call_args_list[0][1]['workflow_version']  == VERSION
        assert json.loads(mock_startwf.call_args_list[0][1]['input']) == {'env': ENV}

def test_start_tombsend_swferror():
    with _patches() as (mock_wfid, mock_startwf):
        mock_startwf.side_effect = SWFResponseError(status='status',
                                                    reason='reason',
                                                    body={'message': 'SWFError'})
        with pytest.raises(SWFResponseError):
            starter.start_tombsend(ENV)

        assert mock_wfid.call_count == 1
        assert mock_wfid.call_args_list[0][0] == ('TombSendMainWorkflow', VERSION)

        assert mock_startwf.call_count == 1
        assert mock_startwf.call_args_list[0][1]['domain']            == DOMAIN
        assert mock_startwf.call_args_list[0][1]['workflow_id']       == WFID
        assert mock_startwf.call_args_list[0][1]['workflow_name']     == 'TombSendMainWorkflow'
        assert mock_startwf.call_args_list[0][1]['workflow_version']  == VERSION
        assert json.loads(mock_startwf.call_args_list[0][1]['input']) == {'env': ENV}

def test_start_tombsend_unexpectederror():
    with _patches() as (mock_wfid, mock_startwf):
        mock_startwf.side_effect = Exception('Failed to start workflow')
        with pytest.raises(Exception):
            starter.start_tombsend(ENV)

        assert mock_wfid.call_count == 1
        assert mock_wfid.call_args_list[0][0] == ('TombSendMainWorkflow', VERSION)

        assert mock_startwf.call_count == 1
        assert mock_startwf.call_args_list[0][1]['domain']            == DOMAIN
        assert mock_startwf.call_args_list[0][1]['workflow_id']       == WFID
        assert mock_startwf.call_args_list[0][1]['workflow_name']     == 'TombSendMainWorkflow'
        assert mock_startwf.call_args_list[0][1]['workflow_version']  == VERSION
        assert json.loads(mock_startwf.call_args_list[0][1]['input']) == {'env': ENV}


#---------------------------------------------------------------------------
#   Test for start_rawdata_process
#---------------------------------------------------------------------------
def test_start_rawdata_process():
    with _patches() as (mock_wfid, mock_startwf):
        assert starter.start_rawdata_process(ENV, DATATYPE_RAWDATA, S3BUCKET, S3FILE) == RUNID

        assert mock_wfid.call_count == 1
        assert mock_wfid.call_args_list[0][0] == ('CreateRawdataMainWorkflow', VERSION)

        assert mock_startwf.call_count == 1
        assert mock_startwf.call_args_list[0][1]['domain']            == DOMAIN
        assert mock_startwf.call_args_list[0][1]['workflow_id']       == WFID
        assert mock_startwf.call_args_list[0][1]['workflow_name']     == 'CreateRawdataMainWorkflow'
        assert mock_startwf.call_args_list[0][1]['workflow_version']  == VERSION
        assert json.loads(mock_startwf.call_args_list[0][1]['input']) == {'env': ENV, 'data_type': DATATYPE_RAWDATA, 's3bucket': S3BUCKET, 's3file': S3FILE}

def test_start_rawdata_process_swferror():
    with _patches() as (mock_wfid, mock_startwf):
        mock_startwf.side_effect = SWFResponseError(status='status',
                                                    reason='reason',
                                                    body={'message': 'SWFError'})
        with pytest.raises(SWFResponseError):
            starter.start_rawdata_process(ENV, DATATYPE_RAWDATA, S3BUCKET, S3FILE)

        assert mock_wfid.call_count == 1
        assert mock_wfid.call_args_list[0][0] == ('CreateRawdataMainWorkflow', VERSION)

        assert mock_startwf.call_count == 1
        assert mock_startwf.call_args_list[0][1]['domain']            == DOMAIN
        assert mock_startwf.call_args_list[0][1]['workflow_id']       == WFID
        assert mock_startwf.call_args_list[0][1]['workflow_name']     == 'CreateRawdataMainWorkflow'
        assert mock_startwf.call_args_list[0][1]['workflow_version']  == VERSION
        assert json.loads(mock_startwf.call_args_list[0][1]['input']) == {'env': ENV, 'data_type': DATATYPE_RAWDATA, 's3bucket': S3BUCKET, 's3file': S3FILE}

def test_start_rawdata_process_unexpectederror():
    with _patches() as (mock_wfid, mock_startwf):
        mock_startwf.side_effect = Exception('Failed to start workflow')
        with pytest.raises(Exception):
            starter.start_rawdata_process(ENV, DATATYPE_RAWDATA, S3BUCKET, S3FILE)

        assert mock_wfid.call_count == 1
        assert mock_wfid.call_args_list[0][0] == ('CreateRawdataMainWorkflow', VERSION)

        assert mock_startwf.call_count == 1
        assert mock_startwf.call_args_list[0][1]['domain']            == DOMAIN
        assert mock_startwf.call_args_list[0][1]['workflow_id']       == WFID
        assert mock_startwf.call_args_list[0][1]['workflow_name']     == 'CreateRawdataMainWorkflow'
        assert mock_startwf.call_args_list[0][1]['workflow_version']  == VERSION
        assert json.loads(mock_startwf.call_args_list[0][1]['input']) == {'env': ENV, 'data_type': DATATYPE_RAWDATA, 's3bucket': S3BUCKET, 's3file': S3FILE}
