# -*- coding: utf-8 -*-

from __future__ import absolute_import

from   collections       import OrderedDict
import contextlib
import datetime
import json
from   mock              import patch
import lib.workflow.util as util


class MockDatetime(datetime.datetime):
    @classmethod
    def now(cls):
        return cls(2013, 10, 19, 23, 40, 59, 123456)

def test_create_poller_identity():
    with contextlib.nested(
        patch('socket.gethostname'), patch('os.getpid')
    ) as (mock_gethostname, mock_getpid):
        mock_gethostname.return_value = 'tamar-dev-batch1'
        mock_getpid.return_value      = 998

        assert util.create_poller_identity() == '{"hostname":"tamar-dev-batch1","pid":998}'
        assert mock_gethostname.call_count   == 1
        assert mock_getpid.call_count        == 1

def test_create_workflow_id():
    with patch('datetime.datetime', MockDatetime):
        assert util.create_workflow_id('UnitTestWorkflow', '1.01')                 == 'UnitTest-1.01-20131019-234059.123'
        assert util.create_workflow_id('UnitTestWf',       '1.01')                 == 'UnitTestWf-1.01-20131019-234059.123'
        assert util.create_workflow_id('UnitTestWorkflow', '1.01', 'arg1')         == 'UnitTest-1.01-arg1-20131019-234059.123'
        assert util.create_workflow_id('UnitTestWorkflow', '1.01', 'arg1', 'arg2') == 'UnitTest-1.01-arg1-arg2-20131019-234059.123'

def test_create_activity_id():
    with patch('datetime.datetime', MockDatetime):
        assert util.create_activity_id('UnitTestActivity', '1.01')                 == 'UnitTest-1.01-20131019-234059.123'
        assert util.create_activity_id('UnitTestAct',      '1.01')                 == 'UnitTestAct-1.01-20131019-234059.123'
        assert util.create_activity_id('UnitTestActivity', '1.01', 'arg1')         == 'UnitTest-1.01-arg1-20131019-234059.123'
        assert util.create_activity_id('UnitTestActivity', '1.01', 'arg1', 'arg2') == 'UnitTest-1.01-arg1-arg2-20131019-234059.123'

def test_dict_to_json():
    input_dict = OrderedDict()
    input_dict['key1'] = 'value1'
    input_dict['key2'] = 'value2'
    assert util.dict_to_json(input_dict) == '{"key1":"value1","key2":"value2"}'

def test_kwargs_to_json():
    assert json.loads(util.kwargs_to_json(key1='value1', key2='value2')) == dict(key1='value1', key2='value2')
