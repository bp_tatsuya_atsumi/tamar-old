# -*- coding: utf-8 -*-

from   conf.config                 import BatchConfig
from   sets                        import Set
import lib.workflow.decider.helper as helper

ENV='dev'
SID='9999'

def _assert_task_list(task_list, act_name, input_dict={}):
    assert helper.get_activity_task_list(act_name, input_dict) == task_list


def test_get_activity_task_list():


    # default
    _assert_task_list('default', 'DummyActivity')
    _assert_task_list('default', 'MappingActivity')
    _assert_task_list('default', 'MappingReportActivity')
    _assert_task_list('default', 'TransferSegmentMstActivity')
    _assert_task_list('default', 'TransferConversionActivity')
    _assert_task_list('default', 'TransferRecommendMstActivity')
    _assert_task_list('default', 'TransferItemMstActivity')
    _assert_task_list('default', 'UpdateItemMstActivity')
    _assert_task_list('default', 'TransferRemoveSegmentMstActivity')
    _assert_task_list('default', 'TransferRemoveRecommendMstActivity')
    _assert_task_list('default', 'TransferLatestSegmentMstActivity')
    _assert_task_list('default', 'TransferLatestRecommendMstActivity')
    _assert_task_list('default', 'TransferLatestItemMstActivity')
    _assert_task_list('default', 'DummyActivity')
    _assert_task_list('default', 'DummyActivity')

    # bridge
    _assert_task_list('bridge', 'BridgeActivity')

    # tomb
    _assert_task_list('tomb', 'TombSendActivity')

    # transfer_high_ma
    _assert_task_list('tl_transfer_high_ma', 'TransferSegmentDataActivity',   {'sid': '9999', 'pid': 'ma'})
    _assert_task_list('tl_transfer_high_ma', 'TransferRecommendDataActivity', {'sid': '9999', 'pid': 'ma'})

    # transfer_high_ma_gdo
    _assert_task_list('tl_transfer_high_ma_gdo', 'TransferSegmentDataActivity',   {'sid': '0009', 'pid': 'ma'})
    _assert_task_list('tl_transfer_high_ma_gdo', 'TransferRecommendDataActivity', {'sid': '0009', 'pid': 'ma'})


    # transfer_high_so
    _assert_task_list('tl_transfer_high_so', 'TransferSegmentDataActivity',   {'sid': '9999', 'pid': 'so'})
    _assert_task_list('tl_transfer_high_so', 'TransferRecommendDataActivity', {'sid': '9999', 'pid': 'so'})

    # transfer_high_so_gdo
    _assert_task_list('tl_transfer_high_so_gdo', 'TransferSegmentDataActivity',   {'sid': '0009', 'pid': 'so'})
    _assert_task_list('tl_transfer_high_so_gdo', 'TransferRecommendDataActivity', {'sid': '0009', 'pid': 'so'})

    # transfer_high_ac_gdo
    _assert_task_list('tl_transfer_high_ac_gdo', 'TransferSegmentDataActivity',   {'sid': '0009', 'pid': 'ac'})

    # transfer_high_other
    _assert_task_list('tl_transfer_high_other', 'TransferSegmentDataActivity',   {'sid': '9999', 'pid': 'ac'})
    _assert_task_list('tl_transfer_high_other', 'TransferRecommendDataActivity', {'sid': '9999', 'pid': 'ao'})
    _assert_task_list('transfer_high_other', 'TransferMailRequestDataActivity')
    _assert_task_list('transfer_high_other', 'TransferMailResponseDataActivity')
    _assert_task_list('transfer_high_other', 'TransferMailSegmentDataActivity')
    _assert_task_list('transfer_high_other', 'SplitTransferFileActivity')

    # rawdata
    _assert_task_list('rawdata', 'CreateRawdataActivity')

    # extsegdata
    _assert_task_list('extsegdata', 'ExtSegDataActivity')


class TestMigration(object):
    """新環境と旧環境の振り向けテスト
    """

    def test_get_transfer_activity_task_list_migrated_to_new_env(self):
        """新環境へ振り向けられる転送タスクリストのテスト
        """
        # BrainPad公式はすべて新環境
        ## ac
        _assert_task_list('tl_transfer_high_other', 'TransferSegmentDataActivity', {'sid': '0001', 'pid': 'ac'})

        ## ao
        _assert_task_list('tl_default', 'TransferSegmentMstActivity', {'sid': '0001', 'pid': 'ao'})
        _assert_task_list('tl_transfer_high_other', 'TransferSegmentDataActivity', {'sid': '0001', 'pid': 'ao'})

        ## go
        _assert_task_list('tl_default', 'TransferSegmentMstActivity', {'sid': '0001', 'pid': 'go'})
        _assert_task_list('tl_transfer_high_other', 'TransferSegmentDataActivity', {'sid': '0001', 'pid': 'go'})

        ## im
        _assert_task_list('tl_default', 'TransferSegmentMstActivity', {'sid': '0001', 'pid': 'im'})
        _assert_task_list('tl_transfer_high_other', 'TransferSegmentDataActivity', {'sid': '0001', 'pid': 'im'})

        ## ma
        _assert_task_list('tl_default', 'TransferSegmentMstActivity', {'sid': '0001', 'pid': 'ma'})
        _assert_task_list('tl_transfer_high_ma', 'TransferSegmentDataActivity', {'sid': '0001', 'pid': 'ma'})
        _assert_task_list('tl_default', 'TransferConversionActivity', {'sid': '0001', 'pid': 'ma'})
        _assert_task_list('tl_default', 'TransferRecommendMstActivity', {'sid': '0001', 'pid': 'ma'})
        _assert_task_list('tl_transfer_high_ma', 'TransferRecommendDataActivity', {'sid': '0001', 'pid': 'ma'})
        _assert_task_list('tl_default', 'TransferItemMstActivity', {'sid': '0001', 'pid': 'ma'})

        ## so
        _assert_task_list('tl_transfer_high_so', 'TransferSegmentDataActivity', {'sid': '0001', 'pid': 'so'})
        _assert_task_list('tl_transfer_high_so', 'TransferRecommendDataActivity', {'sid': '0001', 'pid': 'so'})
        _assert_task_list('tl_default', 'TransferItemMstActivity', {'sid': '0001', 'pid': 'so'})

        ## rs
        _assert_task_list('tl_transfer_high_other', 'TransferMailRequestDataActivity', {'sid': '0001', 'pid': 'rs'})
        _assert_task_list('tl_transfer_high_other', 'TransferMailResponseDataActivity', {'sid': '0001', 'pid': 'rs'})

        ## sy
        _assert_task_list('tl_transfer_high_other', 'TransferMailSegmentDataActivity', {'sid': '0001', 'pid': 'sy'})

        ## bridge
        _assert_task_list('tl_default', 'TransferSegmentMstActivity', {'sid': '0001', 'pid': 'rt0043'})
        _assert_task_list('tl_transfer_high_other', 'TransferSegmentDataActivity', {'sid': '0001', 'pid': 'rt0043'})

        ## external
        _assert_task_list('extsegdata', 'ExtSegDataActivity', {'sid': '0001', 'pid': 'deltacube'})


        # BrainPad公式以外は、so, ao以外すべて新環境
        ## ac
        _assert_task_list('tl_transfer_high_other', 'TransferSegmentDataActivity', {'sid': '0003', 'pid': 'ac'})

        ## ac_gdo
        _assert_task_list('tl_transfer_high_ac_gdo', 'TransferSegmentDataActivity',   {'sid': '0009', 'pid': 'ac'})

        ## go
        _assert_task_list('tl_default', 'TransferSegmentMstActivity', {'sid': '0009', 'pid': 'go'})
        _assert_task_list('tl_transfer_high_other', 'TransferSegmentDataActivity', {'sid': '0009', 'pid': 'go'})

        ## im
        _assert_task_list('tl_default', 'TransferSegmentMstActivity', {'sid': '0009', 'pid': 'im'})
        _assert_task_list('tl_transfer_high_other', 'TransferSegmentDataActivity', {'sid': '0001', 'pid': 'im'})

        ## bridge
        _assert_task_list('tl_default', 'TransferSegmentMstActivity', {'sid': '0009', 'pid': 'rt0043'})
        _assert_task_list('tl_transfer_high_other', 'TransferSegmentDataActivity', {'sid': '0009', 'pid': 'rt0043'})

        ## external
        _assert_task_list('extsegdata', 'ExtSegDataActivity', {'sid': '0009', 'pid': 'deltacube'})

        ## rs
        _assert_task_list('tl_transfer_high_other', 'TransferMailRequestDataActivity', {'sid': '0011', 'pid': 'rs'})
        _assert_task_list('tl_transfer_high_other', 'TransferMailResponseDataActivity', {'sid': '0051', 'pid': 'rs'})

        ## sy
        _assert_task_list('tl_transfer_high_other', 'TransferMailSegmentDataActivity', {'sid': '0021', 'pid': 'sy'})

        ## ma
        _assert_task_list('tl_default', 'TransferSegmentMstActivity', {'sid': '0011', 'pid': 'ma'})
        _assert_task_list('tl_transfer_high_ma', 'TransferSegmentDataActivity', {'sid': '0007', 'pid': 'ma'})
        _assert_task_list('tl_default', 'TransferConversionActivity', {'sid': '0050', 'pid': 'ma'})
        _assert_task_list('tl_default', 'TransferRecommendMstActivity', {'sid': '0062', 'pid': 'ma'})
        _assert_task_list('tl_transfer_high_ma', 'TransferRecommendDataActivity', {'sid': '0073', 'pid': 'ma'})
        _assert_task_list('tl_default', 'TransferItemMstActivity', {'sid': '0080', 'pid': 'ma'})

        ## ma_gdo
        _assert_task_list('tl_transfer_high_ma_gdo', 'TransferRecommendDataActivity', {'sid': '0009', 'pid': 'ma'})

        ## so
        _assert_task_list('tl_transfer_high_so', 'TransferSegmentDataActivity', {'sid': '0007', 'pid': 'so'})
        _assert_task_list('tl_transfer_high_so', 'TransferRecommendDataActivity', {'sid': '0050', 'pid': 'so'})
        _assert_task_list('tl_default', 'TransferItemMstActivity', {'sid': '0062', 'pid': 'so'})

        ## so_gdo
        _assert_task_list('tl_transfer_high_so_gdo', 'TransferSegmentDataActivity', {'sid': '0009', 'pid': 'so'})
        _assert_task_list('tl_transfer_high_so_gdo', 'TransferRecommendDataActivity', {'sid': '0009', 'pid': 'so'})

        ## ao
        _assert_task_list('tl_default', 'TransferSegmentMstActivity', {'sid': '0009', 'pid': 'ao'})
        _assert_task_list('tl_transfer_high_other', 'TransferSegmentDataActivity', {'sid': '0009', 'pid': 'ao'})

    def test_get_bridge_activity_task_list_migrated_to_new_env(self):
        """新環境に振り向けられるブリッジ
        """
        _assert_task_list('tl_bridge', 'BridgeActivity', {'sid': '0001', 'pid': 'rt0043'})
        _assert_task_list('tl_bridge', 'BridgeActivity', {'sid': '0002', 'pid': 'rt0043'})

    def test_get_mapping_activity_task_list_migrated_to_new_env(self):
        """新環境に振り向けられるマッピング
        """
        _assert_task_list('tl_default', 'MappingActivity', {'sid': '0001', 'pid': 'co'})
        _assert_task_list('tl_default', 'MappingActivity', {'sid': '0002', 'pid': 'ao'})

    def test_get_update_itemmst_activity_task_list_migrated_to_new_env(self):
        """新環境に振り向けられるアイテムマスタ更新
        """
        _assert_task_list('tl_default', 'UpdateItemMstActivity', {'sid': '0001'})

    def test_get_update_itemmst_activity_task_list_migrated_to_old_env(self):
        """旧環境に振り向けられるアイテムマスタ更新
        """
        _assert_task_list('default', 'UpdateItemMstActivity', {'sid': '0019'})

    def test_get_split_transfer_activity_task_list_migrated_to_new_env(self):
        """新環境に振り向けられる入力ファイル分割タスク
        """
        _assert_task_list('tl_transfer_high_other', 'SplitTransferFileActivity', {'sid': '0001', 'pid': 'so'})

    def test_get_split_transfer_activity_task_list_migrated_to_old_env(self):
        """旧環境に振り向けられる入力ファイル分割タスク
        """
        _assert_task_list('transfer_high_other', 'SplitTransferFileActivity', {'sid': '0009', 'pid': 'ma'})

    def test_get_optout_activity_task_list_migrated_to_new_env(self):
        """新環境に振り向けられるオプトアウト
        """
        _assert_task_list('tl_default', 'OptoutRTActivity', {'sid': '0001'})
        _assert_task_list('tl_default', 'OptoutRTActivity', {'sid': '0009'})
        _assert_task_list('tl_default', 'OptoutCOActivity', {'sid': '0001'})
        _assert_task_list('tl_default', 'OptoutCOActivity', {'sid': '0009'})


def _target():
    config = BatchConfig(ENV)
    return helper.MasterHelper(config.site_mst, config.partner_mst, config.mapping, config.workflow)

def test_distribute_mapping():
    master_helper = _target()
    pairs_of_sid_pid = master_helper.distribute_mapping()
    assert Set(pairs_of_sid_pid) == Set([('9999', 'bp1'), ('9999', 'ma'), ('9999', 'so'), ('9999', 'ao'),
                                         ('9999', 'ac'),  ('9999', 'co'), ('9999', 'rs'), ('9999', 'go'),
                                         ('9999', 'im'),  ('9998', 'so')])

def test_distribute_mapping_divided_total_class_3_target_class_0():
    master_helper = _target()
    pairs_of_sid_pid = master_helper.distribute_mapping_divided(3, 0)
    assert Set(pairs_of_sid_pid) == Set([('9999', 'bp1'), ('9999', 'ma'), ('9999', 'so'), ('9999', 'ao'),
                                         ('9999', 'ac'),  ('9999', 'co'), ('9999', 'rs'), ('9999', 'go'),
                                         ('9999', 'im')])

def test_distribute_mapping_divided_total_class_3_target_class_1():
    master_helper = _target()
    pairs_of_sid_pid = master_helper.distribute_mapping_divided(3, 1)
    assert Set(pairs_of_sid_pid) == Set([])

def test_distribute_mapping_divided_total_class_3_target_class_2():
    master_helper = _target()
    pairs_of_sid_pid = master_helper.distribute_mapping_divided(3, 2)
    assert Set(pairs_of_sid_pid) == Set([('9998', 'so')])

def test_distribute_mapping_divided_total_class_5_target_class_0():
    master_helper = _target()
    pairs_of_sid_pid = master_helper.distribute_mapping_divided(5, 0)
    assert Set(pairs_of_sid_pid) == Set([])

def test_distribute_mapping_divided_total_class_5_target_class_1():
    master_helper = _target()
    pairs_of_sid_pid = master_helper.distribute_mapping_divided(5, 1)
    assert Set(pairs_of_sid_pid) == Set([])

def test_distribute_mapping_divided_total_class_5_target_class_2():
    master_helper = _target()
    pairs_of_sid_pid = master_helper.distribute_mapping_divided(5, 2)
    assert Set(pairs_of_sid_pid) == Set([])

def test_distribute_mapping_divided_total_class_5_target_class_3():
    master_helper = _target()
    pairs_of_sid_pid = master_helper.distribute_mapping_divided(5, 3)
    assert Set(pairs_of_sid_pid) == Set([('9998', 'so')])

def test_distribute_mapping_divided_total_class_5_target_class_4():
    master_helper = _target()
    pairs_of_sid_pid = master_helper.distribute_mapping_divided(5, 4)
    assert Set(pairs_of_sid_pid) == Set([('9999', 'bp1'), ('9999', 'ma'), ('9999', 'so'), ('9999', 'ao'),
                                         ('9999', 'ac'),  ('9999', 'co'), ('9999', 'rs'), ('9999', 'go'),
                                         ('9999', 'im')])

def test_distribute_bridge():
    master_helper = _target()
    pairs_of_sid_pid = master_helper.distribute_bridge()
    assert pairs_of_sid_pid == [('9999', 'rt9998')]

def test_distribute_extsegdata():
    master_helper = _target()
    pairs_of_sid_pid = master_helper.distribute_extsegdata()
    assert pairs_of_sid_pid == [('9999', 'bp1'), ('9999', 'im'), ('9999', 'deltacube')]

def test_distribute_optout_rt():
    master_helper = _target()
    sids = master_helper.distribute_optout_rt()
    assert sids == ['9999', '9998']

def test_distribute_transfer():
    master_helper = _target()
    pids = master_helper.distribute_transfer('9999', 'segmentdata')
    assert pids == ['ac', 'ma', 'im', 'ao', 'bp1', 'so', 'go', 'rt9998']
