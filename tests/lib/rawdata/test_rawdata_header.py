# -*- coding: utf-8 -*-

from __future__ import absolute_import

from lib.rawdata.rawdata_header import RawdataHeader

STR_ORDER_AND_CLICK_HEADER = u'"種別","アクセス時刻","IP","UID","ページID","ロボット判定","オーダーID","内部ID","スケジュールID","クリエイティブID","デバイス","キャリア","OS","ブラウザ","リファラドメイン","初回アクセスフラグ"'.encode('cp932')

STR_CONVERSION_HEADER    = u'"ピクセルID","CV発生時刻","UID","CV対象","ビュー・クリック種別","対象ID","スケジュールID","CV対象最終アクセス時刻"'.encode('cp932')

def test_rawdata_header(func_init_config):

    rawdata_header_obj = RawdataHeader()
    assert rawdata_header_obj.STR_ORDER_AND_CLICK_HEADER == STR_ORDER_AND_CLICK_HEADER
    assert rawdata_header_obj.STR_CONVERSION_HEADER      == STR_CONVERSION_HEADER
