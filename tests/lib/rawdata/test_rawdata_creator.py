# -*- coding: utf-8 -*-

from   conf.config                 import BatchConfig
import contextlib
from   lib.rawdata.rawdata_creator import RawdataCreator
from   mock                        import Mock
from   mock                        import patch
import os
import pytest
import tarfile

SID       = '9999'
ENV       = 'dev'
DATA_TYPE = 'rawdata'
S3BUCKET  = 's3bucket'
S3FILE    = 's3_path/rawdata/raw/brainpad-20140515.tgz'

ORDER_LOG1 = 'bc\t1400112002\t99a40107\ttest_uesr3\t8085\t0\t1\t7378\t21486\t24250\tUnknown\tUnknown\tWindows Vista\tInternet Explorer 9\t0\t{(12345)}'
ORDER_LOG2 = 'bc\t1400112003\t99a40107\ttest_uesr4\t8085\t0\t2\t7378\t21486\t24250\tUnknown\tUnknown\tWindows Vista\tInternet Explorer 9\t0\t{(12345)}'

CLICK_LOG1 = 'cl\t1400113388\t7a12ef6b\ttest_user3\t8392\t0\t1\t7346\t21047\t23118\tSC-04E\tdocomo\tAndroid 4.3\tAndroid\t0\t{(12345)}'
CLICK_LOG2 = 'cl\t1400113389\t7a12ef6b\ttest_user4\t8392\t0\t2\t7346\t21047\t23118\tSC-04E\tdocomo\tAndroid 4.3\tAndroid\t0\t{(12345)}'

CONVERSION_LOG1 = '1\t1400112014\ttest_user3\tcreative\tview\t24746\t1\t1382597028548\t{(12345),(6789)}'
CONVERSION_LOG2 = '2\t1400112014\ttest_user4\t{}'

ORDER_ID_COLUMN_NUM    = 6
SCHEDULE_ID_COLUMN_NUM = 6
CV_PIXEL_ID_COLUMN_NUM = 0

ROW_ORDER      = ORDER_LOG1.split('\t')
ROW_CLICK      = CLICK_LOG1.split('\t')
ROW_CONVERSION = CONVERSION_LOG1.split('\t')


def _create_mock_order_ids():
    ids = [1,2]
    return ids

def _create_mock_schedule_ids():
    ids = [1,2,3,4]
    return ids

def _create_mock_cv_pixel_ids():
    ids = [1,2]
    return ids

def _create_text_file(dirname, filename, lines):
    path = str(dirname.join(filename))
    with open(path, 'w') as f:
        f.write('\n'.join(lines))
    return path

@contextlib.contextmanager
def _patches(func_init_config):
    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('lib.mappingtable.MappingTable.select_rows_by_partner_user_id'),
        patch('lib.rawdata.rawdata.Rawdata'),
        patch('lib.s3util.S3Util.upload_single_file'),
        patch('lib.partner_api.so.api_connector.ApiConnector.__init__', new=func_init_config),
        patch('lib.partner_api.so.api_connector.ApiConnector.get_order_ids_from_advertiser_ids'),
        patch('lib.partner_api.so.api_connector.ApiConnector.get_schedule_ids_from_advertiser_ids'),
        patch('lib.partner_api.so.api_connector.ApiConnector.get_cv_pixel_ids_from_advertiser_ids'),
        ) as (m_config, m_select, m_rawdata, m_s3put, m_api_connector, m_order_ids, m_schedule_ids, m_cv_pixel_ids):
        m_order_ids.return_value    = _create_mock_order_ids()
        m_schedule_ids.return_value = _create_mock_schedule_ids()
        m_cv_pixel_ids.return_value = _create_mock_cv_pixel_ids()

        yield(m_select, m_rawdata, m_s3put)

def test_convert_rawdata(tmpdir, func_init_config):
    rawdata_dir = tmpdir.mkdir('rawdata_creator').mkdir('convert_rawdata')

    order_file_name      = '20140515.order.tsv'
    click_file_name      = '20140515.click.tsv'
    conversion_file_name = '20140515.conversion.tsv'

    order_file_path      = _create_text_file(rawdata_dir, order_file_name,      [ORDER_LOG1,      ORDER_LOG2])
    click_file_path      = _create_text_file(rawdata_dir, click_file_name,      [CLICK_LOG1,      CLICK_LOG2])
    conversion_file_path = _create_text_file(rawdata_dir, conversion_file_name, [CONVERSION_LOG1, CONVERSION_LOG2])

    with _patches(func_init_config) as (m_select, m_rawdata, m_s3put):
        m_select.return_value = [['test_user_1', 'value1', 'value2', 'value4', 'value5'],
                                 ['test_user_2', 'value2', 'value3', 'value5', 'value6']]

        rawdata_creator = RawdataCreator(SID, ENV, DATA_TYPE, S3BUCKET, S3FILE)
        rawdata_creator.local_base_dir = str(rawdata_dir)

        rawdata_creator.rawdata.decompressed_rawdata_names = [order_file_name, click_file_name, conversion_file_name]

        rawdata_creator._convert_rawdata()

        rawdata_path_list = []
        basename_list     = []
        for data_type in ['order', 'click', 'conversion']:
            output_path = '{base_path}/{data_type}_9999_20140515.csv'.format(base_path=rawdata_creator.local_base_dir,
                                                                             data_type=data_type)
            basename_list.append(os.path.basename(output_path))
            assert os.path.exists(output_path)
            assert output_path == os.path.join(str(rawdata_dir), '{data_type}_9999_20140515.csv'.format(data_type=data_type))
            rawdata_path_list.append(output_path)

        rawdata_creator._compress_rawdata(rawdata_path_list)
        compressed_path = '{base_path}/9999_20140515.tgz'.format(base_path=rawdata_creator.local_base_dir)
        assert os.path.exists(compressed_path)
        assert compressed_path == os.path.join(str(rawdata_dir), '9999_20140515.tgz')

        basename_list.sort()
        with tarfile.open(compressed_path, 'r:gz') as tar_file:
            decompressed_file_name_list = sorted(map(lambda x: x.name, tar_file.getmembers()))
        assert basename_list == decompressed_file_name_list

def test_ids_exist(func_init_config):

    with _patches(func_init_config) as (m_select, m_rawdata, m_s3put):
        m_select.return_value = [['test_user_1', 'value1', 'value2', 'value4', 'value5'],
                                 ['test_user_2', 'value2', 'value3', 'value5', 'value6']]

        rawdata_creator = RawdataCreator(SID, ENV, DATA_TYPE, S3BUCKET, S3FILE)
        flag_order_id_order    = rawdata_creator._is_in_order_ids(ROW_ORDER[ORDER_ID_COLUMN_NUM], ['1','2'])
        flag_order_id_click    = rawdata_creator._is_in_order_ids(ROW_CLICK[ORDER_ID_COLUMN_NUM], ['1','2'])
        flag_schedule_id_order = rawdata_creator._is_in_schedule_ids(ROW_CONVERSION[SCHEDULE_ID_COLUMN_NUM], ['1','2'])
        flag_cv_pixel_id       = rawdata_creator._is_in_cv_pixel_ids(ROW_CONVERSION[CV_PIXEL_ID_COLUMN_NUM], ['1','2'])

    assert flag_order_id_order    == True
    assert flag_order_id_click    == True
    assert flag_schedule_id_order == True
    assert flag_cv_pixel_id       == True

def test_ids_not_exist(func_init_config):

    with _patches(func_init_config) as (m_select, m_rawdata, m_s3put):
        m_select.return_value = [['test_user_1', 'value1', 'value2', 'value4', 'value5'],
                                 ['test_user_2', 'value2', 'value3', 'value5', 'value6']]

        rawdata_creator = RawdataCreator(SID, ENV, DATA_TYPE, S3BUCKET, S3FILE)
        flag_order_id_order    = rawdata_creator._is_in_order_ids(ROW_ORDER[ORDER_ID_COLUMN_NUM], ['8','9'])
        flag_order_id_click    = rawdata_creator._is_in_order_ids(ROW_CLICK[ORDER_ID_COLUMN_NUM], ['8','9'])
        flag_schedule_id_order = rawdata_creator._is_in_schedule_ids(ROW_CONVERSION[SCHEDULE_ID_COLUMN_NUM], ['8','9'])
        flag_cv_pixel_id       = rawdata_creator._is_in_cv_pixel_ids(ROW_CONVERSION[CV_PIXEL_ID_COLUMN_NUM], ['8','9'])

    assert flag_order_id_order    == False
    assert flag_order_id_click    == False
    assert flag_schedule_id_order == False
    assert flag_cv_pixel_id       == False

