# -*- coding: utf-8 -*-

from lib.rawdata.rawdata import Rawdata

import contextlib
import datetime
import pytest
import tarfile
from conf.config              import BatchConfig
from lib.transferer.exception import NoS3Files
from mock                     import patch

ENV      = 'dev'
S3BUCKET = 's3bucket'
S3FILE   = 's3_path/rawdata/raw/brainpad-20140515.tgz'

def _create_tgz_file(dirname, filename, archive_file_list):
    path = str(dirname.join(filename))
    with tarfile.open(path, 'w:gz') as tar:
        for archive_file in archive_file_list:
            tar.add(str(archive_file)) 

def _create_text_file(dirname, filename):
    path = str(dirname.join(filename))
    with open(path, 'w') as f:
        f.write('test')
    return path

class MockDate(datetime.date):
    @classmethod
    def today(cls):
        return cls(2014, 5, 30)

class MockDatetime(datetime.datetime):
    @classmethod
    def now(cls):
        return cls(2014, 5, 30, 10, 15, 20, 123456)

def _assert_exists(*paths):
    for path in paths:
        assert path.check(exists=1)

def _assert_not_exists(*paths):
    for path in paths:
        assert path.check(exists=0)

@contextlib.contextmanager
def _patches(func_init_config):
    with contextlib.nested (
            patch('conf.config.BatchConfig.__init__', new=func_init_config),
            patch('lib.s3util.S3Util.download_single_file'),
            patch('lib.s3util.S3Util.upload_single_file'),
            ) as (m_config, m_s3get, m_s3put):
        m_s3get.return_value = {'key': S3FILE}

        yield (m_s3get, m_s3put)

def test_get_rawdata_from_s3(func_init_config):
    expected_path = '/path/to/target'
    expected_name = 'brainpad-20140515.tgz'
    with _patches(func_init_config) as (m_s3get, m_s3put):
        m_s3get.return_value = {
                'key'           : expected_name,
                'is_dir'        : False,
                'is_file'       : True,
                'size'          : 100,
                'last_modified' : '2014-05-15T00:00:00.000Z'
        }

        rawdata = Rawdata(ENV, S3BUCKET, S3FILE)
        rawdata.local_base_dir = expected_path
        rawdata.rawdata_name   = expected_name
        rawdata.get_rawdata_from_s3()

        expected_local_rawdata_path = '{path}/{name}'.format(path=expected_path, name=expected_name)

        assert rawdata.local_rawdata_path == expected_local_rawdata_path

def test_get_rawdata_from_s3_not_exist(func_init_config):
    with _patches(func_init_config) as (m_s3get, m_s3put):
        m_s3get.return_value = None

        with pytest.raises(NoS3Files):
            rawdata = Rawdata(ENV, S3BUCKET, S3FILE)
            rawdata.get_rawdata_from_s3()

def test_decompress_rawdata(tmpdir, func_init_config):
    base_dir    = tmpdir.mkdir('decompress_rawdata')
    rawdata_dir = base_dir.mkdir('rawdata_dir')

    order_file_path      = _create_text_file(rawdata_dir, '20140515.order.tsv')
    click_file_path      = _create_text_file(rawdata_dir, '20140515.click.tsv')
    conversion_file_path = _create_text_file(rawdata_dir, '20140515.conversion.tsv')

    tgz_filename = 'brainpad-20150515.tgz'
    _create_tgz_file(rawdata_dir, tgz_filename, [order_file_path, click_file_path, conversion_file_path])

    rawdata = Rawdata(ENV, S3BUCKET, S3FILE)
    rawdata.local_rawdata_path = str(rawdata_dir.join(tgz_filename))

    rawdata.decompress_rawdata()

    rawdata.decompressed_rawdata_paths = [order_file_path, click_file_path, conversion_file_path]

def test_decompress_rawdata_not_exist_tgzfile(tmpdir, func_init_config):
    rawdata = Rawdata(ENV, S3BUCKET, S3FILE)
    rawdata.local_rawdata_path = str('/not/exist/path/file.tgz')

    with pytest.raises(IOError):
        rawdata = Rawdata(ENV, S3BUCKET, S3FILE)
        rawdata.local_rawdata_path = str('/not/exist/path/file.tgz')
        rawdata.decompress_rawdata()

def test_upload_converted_rawdata(tmpdir, func_init_config):
    base_dir        = tmpdir.mkdir('decompress_rawdata')
    rawdata_dir     = base_dir.mkdir('rawdata_dir')
    order_file_path = _create_text_file(rawdata_dir, '20140515.order.tsv')

    with _patches(func_init_config) as (m_s3get, m_s3put):
        rawdata = Rawdata(ENV, S3BUCKET, S3FILE)
        rawdata.upload_converted_rawdata('/path/to/s3/dir', order_file_path)

def test_upload_converted_rawdata_with_s3put_exception(tmpdir, func_init_config):
    base_dir        = tmpdir.mkdir('decompress_rawdata')
    rawdata_dir     = base_dir.mkdir('rawdata_dir')
    order_file_path = _create_text_file(rawdata_dir, '20140515.order.tsv')

    with _patches(func_init_config) as (m_s3get, m_s3put):
        m_s3put.side_effect = Exception('Failed to put file to s3')

        rawdata = Rawdata(ENV, S3BUCKET, S3FILE)
        with pytest.raises(Exception):
            rawdata.upload_converted_rawdata('path/to/s3/dir', order_file_path)

