# -*- coding: utf-8 -*-

from datetime         import datetime
from datetime         import timedelta
from lib.mappingtable import MappingTable
from mock             import patch
from sqlalchemy.sql   import text
import contextlib
import MySQLdb
import pytest

def init():
    DB_USER  = 'tamaruser'
    DB_PASS  = '#!TawaR?u5er'
    DB_HOST  = 'localhost'
    DB_NAME  = 'DB_TAMAR_9999'
    TBL_NAME = 't_user_mapping_9999_bp1'

    mapping_table = MappingTable(DB_USER, DB_PASS, DB_HOST, DB_NAME, TBL_NAME)
    mapping_table.truncate_table()
    sample_data = [{
                    'user_id'         : 'rt-sample',
                    'partner_user_id' : 'pt-sample',
                    'tamar_user_type' : 1,
                    'tamar_user_id'   : 'co-sample',
                    'inserted_at'     : text('NOW() - INTERVAL 1 HOUR'),
                    'updated_at'      : text('NOW() - INTERVAL 1 HOUR'),
                    'is_deleted'      : 0
                   },
                   {
                    'user_id'         : 'rt-sample',
                    'partner_user_id' : 'pt-sample-other',
                    'tamar_user_type' : 1,
                    'tamar_user_id'   : 'co-sample',
                    'inserted_at'     : text('NOW() - INTERVAL 5 HOUR'),
                    'updated_at'      : text('NOW() - INTERVAL 5 HOUR'),
                    'is_deleted'      : 0
                   },
                   {
                    'user_id'         : 'rt-sample',
                    'partner_user_id' : 'pt-sample-other2',
                    'tamar_user_type' : 1,
                    'tamar_user_id'   : 'co-sample',
                    'inserted_at'     : text('NOW() - INTERVAL 30 DAY'),
                    'updated_at'      : text('NOW() - INTERVAL 30 DAY'),
                    'is_deleted'      : 0
                   },
                   {
                    'user_id'         : 'rt-sample',
                    'partner_user_id' : 'pt-sample-other3',
                    'tamar_user_type' : 1,
                    'tamar_user_id'   : 'co-sample',
                    'inserted_at'     : text('NOW() - INTERVAL 50 DAY'),
                    'updated_at'      : text('NOW() - INTERVAL 50 DAY'),
                    'is_deleted'      : 0
                   },
                   {
                    'user_id'         : 'rt-sample2',
                    'partner_user_id' : 'pt-sample2',
                    'tamar_user_type' : 1,
                    'tamar_user_id'   : 'co-sample2',
                    'inserted_at'     : text('NOW()'),
                    'updated_at'      : text('NOW()'),
                    'is_deleted'      : 0
                   },
                   {
                    'user_id'         : 'rt-sample2-other',
                    'partner_user_id' : 'pt-sample2',
                    'tamar_user_type' : 1,
                    'tamar_user_id'   : 'co-sample2',
                    'inserted_at'     : text('NOW()'),
                    'updated_at'      : text('NOW()'),
                    'is_deleted'      : 0
                   },
                   {
                    'user_id'         : 'rt-sample3',
                    'partner_user_id' : 'pt-sample3',
                    'tamar_user_type' : 1,
                    'tamar_user_id'   : 'co-sample3',
                    'inserted_at'     : text('NOW()'),
                    'updated_at'      : text('NOW()'),
                    'is_deleted'      : 1
                   }]

    for data in sample_data:
        mapping_table.insert_row(data['user_id'],
                                 data['partner_user_id'],
                                 data['tamar_user_type'],
                                 data['tamar_user_id'],
                                 data['inserted_at'],
                                 data['updated_at'],
                                 data['is_deleted'])
    return mapping_table


def test_select_rows_by_user_id():
    mapping_table = init()

    user_id = 'rt-sample'
    result  = mapping_table.select_rows_by_user_id(user_id)
    assert result[0][0] == 'rt-sample'
    assert result[0][1] == 'pt-sample'
    assert result[0][2] == 1
    assert result[0][3] == 'co-sample'
    assert result[0][6] == False

    assert result[1][0] == 'rt-sample'
    assert result[1][1] == 'pt-sample-other'
    assert result[1][2] == 1
    assert result[1][3] == 'co-sample'
    assert result[1][6] == False

def test_select_rows_by_user_id_when_not_exist_user_id():
    mapping_table = init()

    user_id = 'not-exist-user-id'
    result  = mapping_table.select_rows_by_user_id(user_id)
    assert result == []
    assert len(result) == 0

def test_select_rows_by_partner_user_id():
    mapping_table = init()

    partner_user_id = 'pt-sample2'
    result = mapping_table.select_rows_by_partner_user_id(partner_user_id)
    assert result[0][0] == 'rt-sample2'
    assert result[0][1] == 'pt-sample2'
    assert result[0][2] == 1
    assert result[0][3] == 'co-sample2'
    assert result[0][6] == False

    assert result[1][0] == 'rt-sample2-other'
    assert result[1][1] == 'pt-sample2'
    assert result[1][2] == 1
    assert result[1][3] == 'co-sample2'
    assert result[1][6] == False


def test_select_rows_by_partner_user_id():
    mapping_table = init()

    partner_user_id = 'not-exist-partner-user-id'
    result = mapping_table.select_rows_by_partner_user_id(partner_user_id)
    assert result == []
    assert len(result) == 0

def test_select_single_row_by_user_id_order_by_updated_at():
    mapping_table = init()

    user_id = 'rt-sample'
    result  = mapping_table.select_single_row_by_user_id_order_by_updated_at(user_id)
    assert result[0] == user_id
    assert result[1] == 'pt-sample'
    assert result[2] == 1
    assert result[3] == 'co-sample'
    assert result[6] == False

def test_select_single_row_by_user_id_order_by_inserted_at():
    mapping_table = init()

    user_id = 'rt-sample'
    result  = mapping_table.select_single_row_by_user_id_order_by_inserted_at(user_id)
    assert result[0] == user_id
    assert result[1] == 'pt-sample'
    assert result[2] == 1
    assert result[3] == 'co-sample'
    assert result[6] == False

def test_select_latest_mapping_partner_uid_by_user_id():
    mapping_table = init()

    user_id = 'rt-sample'
    result = mapping_table.select_latest_mapping_partner_uid_by_user_id(user_id)
    assert result == 'pt-sample'

    result = mapping_table.select_latest_mapping_partner_uid_by_user_id('not-exist-uid')
    assert result is None

def test_select_latest_mapping_partner_uid_by_user_id_until_n_days():
    mapping_table = init()

    user_id = 'rt-sample'
    result  = mapping_table.select_latest_mapping_partner_uid_by_user_id_until_n_days(user_id, 40)
    assert result == 'pt-sample-other2'

    result  = mapping_table.select_latest_mapping_partner_uid_by_user_id_until_n_days(user_id, 60)
    assert result == 'pt-sample-other3'

def test_select_partner_uids_by_user_id():
    mapping_table = init()

    user_id = 'rt-sample'
    result  = mapping_table.select_partner_uids_by_user_id(user_id)
    assert sorted(result) == sorted(['pt-sample', 'pt-sample-other2', 'pt-sample-other3', 'pt-sample-other'])

    user_id = 'rt-sample2'
    result  = mapping_table.select_partner_uids_by_user_id(user_id)
    assert result == ['pt-sample2']

    user_id = 'rt-sampleX'
    result  = mapping_table.select_partner_uids_by_user_id(user_id)
    assert result == []

def test_select_user_ids_by_partner_user_id():
    mapping_table = init()

    partner_user_id = 'pt-sample'
    result  = mapping_table.select_user_ids_by_partner_user_id(partner_user_id)
    assert result == ['rt-sample']

    partner_user_id = 'pt-sample2'
    result  = mapping_table.select_user_ids_by_partner_user_id(partner_user_id)
    assert sorted(result) == sorted(['rt-sample2', 'rt-sample2-other'])

    partner_user_id = 'pt-sampleX'
    result  = mapping_table.select_user_ids_by_partner_user_id(partner_user_id)
    assert result == []

def test_select_all_rows():
    mapping_table = init()

    result = mapping_table.select_all_rows()
    assert len(result) == 7

def test_insert_row():
    mapping_table = init()

    insert_data = {
                    'user_id'         : 'rt-sample4',
                    'partner_user_id' : 'pt-sample4',
                    'tamar_user_type' : 1,
                    'tamar_user_id'   : 'co-sample4',
                    'inserted_at'     : datetime.utcnow(),
                    'updated_at'      : datetime.utcnow(),
                    'is_deleted'      : 0
                  }
    result = mapping_table.insert_row(insert_data['user_id'],
                                      insert_data['partner_user_id'],
                                      insert_data['tamar_user_type'],
                                      insert_data['tamar_user_id'],
                                      insert_data['inserted_at'],
                                      insert_data['updated_at'],
                                      insert_data['is_deleted'])
    assert result == True

    result = mapping_table.select_rows_by_user_id(insert_data['user_id'])
    assert result[0][0] == insert_data['user_id']
    assert result[0][1] == insert_data['partner_user_id']
    assert result[0][2] == insert_data['tamar_user_type']
    assert result[0][3] == insert_data['tamar_user_id']
    assert result[0][6] == insert_data['is_deleted']

def test_upsert_row_insert():
    mapping_table = init()

    insert_data = {
                    'user_id'         : 'rt-sample4',
                    'partner_user_id' : 'pt-sample4',
                    'tamar_user_type' : 2,
                    'tamar_user_id'   : 'co-sample4'
                  }
    mapping_table.upsert_row(insert_data['user_id'],
                             insert_data['partner_user_id'],
                             insert_data['tamar_user_type'],
                             insert_data['tamar_user_id'])

    result = mapping_table.select_rows_by_user_id(insert_data['user_id'])
    assert result[0][0] == insert_data['user_id']
    assert result[0][1] == insert_data['partner_user_id']
    assert result[0][2] == insert_data['tamar_user_type']
    assert result[0][3] == insert_data['tamar_user_id']

def test_upsert_row_update():
    mapping_table = init()

    update_data = {
                    'user_id'         : 'rt-sample3',
                    'partner_user_id' : 'pt-sample3',
                    'tamar_user_type' : 2,
                    'tamar_user_id'   : 'co-sample3'
                  }

    mapping_table.upsert_row(update_data['user_id'],
                             update_data['partner_user_id'],
                             update_data['tamar_user_type'],
                             update_data['tamar_user_id'])

    result = mapping_table.select_rows_by_user_id(update_data['user_id'])
    assert result[0][0] == update_data['user_id']
    assert result[0][1] == update_data['partner_user_id']
    assert result[0][2] == update_data['tamar_user_type']
    assert result[0][3] == update_data['tamar_user_id']

def test_delete_rows_by_user_id():

    user_id = 'rt-sample'
    with init() as mapping_table:
        records       = mapping_table.select_rows_by_user_id(user_id)
        deleted_count = mapping_table.delete_rows_by_user_id(user_id)
        empty_records = mapping_table.select_rows_by_user_id(user_id)
    assert deleted_count == len(records)
    assert len(empty_records) == 0

def test_delete_rows_by_user_id_with_error():

    user_id = 'rt-sample'
    with contextlib.nested (
        patch('lib.mappingtable.MappingTable.delete_rows_by_user_id'),
        patch('sqlalchemy.engine.Transaction.rollback'),
    ) as (m_delete, m_rollback):
        m_delete.side_effect = MySQLdb.Error()
        with pytest.raises(MySQLdb.Error):
            with init() as mapping_table:
                mapping_table.delete_rows_by_user_id(user_id)
        assert m_delete.call_count   == 1
        assert m_rollback.call_count == 1

def test_delete_rows_by_user_id_with_error_3rd():

    user_id = 'rt-sample'
    with contextlib.nested (
        patch('lib.mappingtable.MappingTable.delete_rows_by_user_id'),
        patch('sqlalchemy.engine.Transaction.rollback'),
    ) as (m_delete, m_rollback):
        m_delete.side_effect = [True,True,MySQLdb.Error(),True]
        with pytest.raises(MySQLdb.Error):
            with init() as mapping_table:
                # correctly executed
                mapping_table.delete_rows_by_user_id(user_id)
                mapping_table.delete_rows_by_user_id(user_id)
                # executed but raises error
                mapping_table.delete_rows_by_user_id(user_id)
                # never executed
                mapping_table.delete_rows_by_user_id(user_id)
        assert m_delete.call_count   == 3
        assert m_rollback.call_count == 1

def test_delete_rows_by_user_ids():

    user_id1 = 'rt-sample'
    user_id2 = 'rt-sample2'
    with init() as mapping_table:
        records1       = mapping_table.select_rows_by_user_id(user_id1)
        records2       = mapping_table.select_rows_by_user_id(user_id2)
        deleted_count  = mapping_table.delete_rows_by_user_ids([user_id1, user_id2])
        empty_records1 = mapping_table.select_rows_by_user_id(user_id1)
        empty_records2 = mapping_table.select_rows_by_user_id(user_id2)
    assert deleted_count == len(records1) + len(records2)
    assert len(empty_records1) + len(empty_records2) == 0

def test_delete_rows_by_user_id_when_not_exist_user_id():

    user_id       = 'not-exist-user-id'
    with init() as mapping_table:
        deleted_count = mapping_table.delete_rows_by_user_id(user_id)
    assert deleted_count == 0

def test_delete_rows_by_partner_user_ids():

    user_id1 = 'pt-sample'
    user_id2 = 'pt-sample2'
    with init() as mapping_table:
        records1       = mapping_table.select_rows_by_partner_user_id(user_id1)
        records2       = mapping_table.select_rows_by_partner_user_id(user_id2)
        deleted_count  = mapping_table.delete_rows_by_partner_user_ids([user_id1, user_id2])
        empty_records1 = mapping_table.select_rows_by_partner_user_id(user_id1)
        empty_records2 = mapping_table.select_rows_by_partner_user_id(user_id2)
    assert deleted_count == len(records1) + len(records2)
    assert len(empty_records1) + len(empty_records2) == 0

def test_truncate_table():

    mapping_table = init()
    mapping_table.truncate_table()
    user_id = 'user_id'
    result = mapping_table.select_rows_by_user_id(user_id)
    assert result == []
    assert len(result) == 0

