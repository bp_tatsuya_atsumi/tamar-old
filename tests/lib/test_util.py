# -*- coding: utf-8 -*-

import gzip
import datetime
import pytest
from werkzeug import ImmutableMultiDict
import lib.util as util
from mock import patch


def test_valid_params():
    params = ImmutableMultiDict([('sid', '001'),
                                 ('uid', 'hoge'),
                                 ('pid', 'rtoaster')])
    assert util.is_valid_params(params) is True

def test_lack_of_params():
    params = ImmutableMultiDict([('sid', '001'),
                                 ('uid', 'hoge')])
    assert util.is_valid_params(params) is False

def test_exces_of_params():
    params = ImmutableMultiDict([('sid', '001'),
                                 ('uid', 'hoge'),
                                 ('pid', 'rtoaster'),
                                 ('no-need-param', 'test')])
    assert util.is_valid_params(params) is False

def test_invalid_params():
    params = ImmutableMultiDict([('ssid', '001'),
                                 ('uid', 'hoge'),
                                 ('pid', 'rtoaster')])
    assert util.is_valid_params(params) is False

def test_strip_str():

    # utf8str
    actual = util.strip_str('あ' * 99)
    assert actual == 'あ' * 99
    actual = util.strip_str('あ' * 100)
    assert actual == 'あ' * 100
    actual = util.strip_str('あ' * 101)
    assert actual == 'あ' * 99 + '…'

    # unicode
    actual = util.strip_str(u'あ' * 99)
    assert actual == u'あ' * 99
    actual = util.strip_str(u'あ' * 100)
    assert actual == u'あ' * 100
    actual = util.strip_str(u'あ' * 101)
    assert actual == u'あ' * 99 + u'…'

    # other
    assert util.strip_str(None) is None
    with pytest.raises(ValueError):
        util.strip_str([])

def test_code_to_long_id():

    def _test_code_to_long_id(code):
        id64 = util.code_to_long_id(code)
        assert id64 != None
        assert isinstance(id64, long)
        assert 0 <= id64 <= 9223372036854775807

    # str
    map(_test_code_to_long_id, [('test' * i) for i in range(1, 1000)])

    # unicode
    map(_test_code_to_long_id, [(u'test' * i) for i in range(1, 1000)])

    # empty or None
    with pytest.raises(ValueError):
        util.code_to_long_id('')
    with pytest.raises(ValueError):
        util.code_to_long_id(None)

def test_code_to_int_id():

    def _test_code_to_int_id(code):
        id32 = util.code_to_int_id(code)
        assert id32 != None
        assert isinstance(id32, int)
        assert 0 <= id32 <= 2147483647

    # str
    map(_test_code_to_int_id, [('test' * i) for i in range(1, 1000)])

    # unicode
    map(_test_code_to_int_id, [(u'test' * i) for i in range(1, 1000)])

    # empty or None
    with pytest.raises(ValueError):
        util.code_to_int_id('')
    with pytest.raises(ValueError):
        util.code_to_int_id(None)

def test_remove_files(tmpdir):
    file1 = tmpdir.join('file1')
    file1.write('')
    file2 = tmpdir.join('file2')
    dir1  = tmpdir.mkdir('dir1')

    assert file1.check(exists=1)
    assert file1.check(exists=1)
    assert dir1.check(exists=1)

    util.remove_files([str(file1), str(file2), None, str(dir1)])
    assert file1.check(exists=0)
    assert file1.check(exists=0)
    assert dir1.check(exists=1)

def test_gzip_file(tmpdir):
    src_file = tmpdir.join('src_file.txt')
    src_file.write('test_gzip_file')
    dst_file = tmpdir.join('src_file.txt.gz')

    result = util.gzip_file(str(src_file))
    assert result == str(dst_file)
    assert dst_file.check(exists=1)
    assert gzip.open(result, 'rb').read() == 'test_gzip_file'

    with pytest.raises(ValueError): util.gzip_file(None)
    with pytest.raises(ValueError): util.gzip_file('already_gziped.txt.gz')
    with pytest.raises(ValueError): util.gzip_file('same_file.txt', 'same_file.txt')
    with pytest.raises(IOError):    util.gzip_file('not_exists.txt')

def test_gunzip_file(tmpdir):
    src_file = tmpdir.join('src_file.txt.gz')
    gzip.open(str(src_file), 'wb').write('test_gunzip_file')
    dst_file = tmpdir.join('src_file.txt')

    result = util.gunzip_file(str(src_file))
    assert result == str(dst_file)
    assert dst_file.check(exists=1)
    assert open(result, 'rb').read() == 'test_gunzip_file'

    with pytest.raises(ValueError): util.gunzip_file(None)
    with pytest.raises(ValueError): util.gunzip_file('not_gziped.txt')
    with pytest.raises(ValueError): util.gunzip_file('same_file.txt.gz', 'same_file.txt.gz')
    with pytest.raises(IOError):    util.gunzip_file('not_exists.txt.gz')

def test_create_n_delta_days_datetime_str():
    datetime_obj = datetime.datetime.strptime('2014-05-20 00:00:00', '%Y-%m-%d %H:%M:%S')
    with patch('datetime.datetime') as m_datetime:
        m_datetime.now.return_value = datetime_obj

        assert util.create_n_delta_days_datetime_str() == '20140520'
        assert util.create_n_delta_days_datetime_str('%Y-%m-%d') == '2014-05-20'
        assert util.create_n_delta_days_datetime_str('%Y-%m-%d', 0) == '2014-05-20'
        assert util.create_n_delta_days_datetime_str(format_str='%Y-%m-%d') == '2014-05-20'
        assert not util.create_n_delta_days_datetime_str(1)

        assert util.create_n_delta_days_datetime_str('%Y-%m-%d', 1) == '2014-05-19'
        assert util.create_n_delta_days_datetime_str('%Y%m%d', 1) == '20140519'
        assert util.create_n_delta_days_datetime_str('%Y%m%d', -1) == '20140521'

        assert not util.create_n_delta_days_datetime_str('%Y%m%d', 'hogehoge')
