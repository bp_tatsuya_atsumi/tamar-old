# -*- coding: utf-8 -*-
import pytest
from lib.crypto import AesCrypto

def test_aes_no_padding():
    _assert_before_after_encryption(AesCrypto(),
                                    'tamar_cryptotest')

def test_aes_padding():
    _assert_before_after_encryption(AesCrypto(),
                                    'cryptotest')

def test_aes_16byte_key():
    _assert_before_after_encryption(AesCrypto('x' * 16),
                                    'cryptotest')

def test_aes_24byte_key():
    _assert_before_after_encryption(AesCrypto('x' * 24),
                                    'cryptotest')

def test_aes_32byte_key():
    _assert_before_after_encryption(AesCrypto('x' * 32),
                                    'cryptotest')

def test_aes_unsupported_key_bytes():
    with pytest.raises(ValueError):
        AesCrypto('x' * 8).encrypt('cryptotest')

def _assert_before_after_encryption(crypto, plaintext):
    ciphertext = crypto.encrypt(plaintext)
    assert plaintext == crypto.decrypt(ciphertext)
    print('{0} -> {1}'.format(plaintext, ciphertext))
