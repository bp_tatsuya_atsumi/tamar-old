# -*- coding: utf-8 -*-

from lib.lockfile import LockFile
from lib.lockfile import AlreadyLocked
from lib.lockfile import LastUpdateFile

import pytest
from mock import patch
from mock import Mock


# Test for Class 'LockFile'
def test_lockfile_lock(lock_dir):
    def _init_mock_config(self, env):
        self.lock_dir = str(lock_dir)

    lock_file_canlock    = lock_dir.join('canlock.lock')
    lock_file_cannotlock = lock_dir.join('cannotlock.lock')
    lock_file_cannotlock.write('')

    with patch('conf.config.BatchConfig.__init__', new=_init_mock_config):
        assert lock_file_canlock.check(exists=0)
        assert lock_file_cannotlock.check(exists=1)

        assert LockFile('canlock', 'dev').lock()
        assert not LockFile('cannotlock', 'dev').lock()

        assert lock_dir.join('canlock.lock').check(exists=1)
        assert lock_dir.join('cannotlock.lock').check(exists=1)

def test_lockfile_is_running(lock_dir):
    def _init_mock_config(self, env):
        self.lock_dir = str(lock_dir)

    lock_file_running    = lock_dir.join('running.lock')
    lock_file_notrunning = lock_dir.join('notrunning.lock')
    lock_file_running.write('')

    with patch('conf.config.BatchConfig.__init__', new=_init_mock_config):
        assert lock_file_running.check(exists=1)
        assert lock_file_notrunning.check(exists=0)

        assert LockFile('running', 'dev').is_running()
        assert not LockFile('notrunning', 'dev').is_running()

        assert lock_file_running.check(exists=1)
        assert lock_file_notrunning.check(exists=0)

def test_lockfile_unlock(lock_dir):
    def _init_mock_config(self, env):
        self.lock_dir = str(lock_dir)

    lock_file_unlock    = lock_dir.join('unlock.lock')
    lock_file_notunlock = lock_dir.join('notunlock.lock')
    lock_file_notlocked = lock_dir.join('notlocked.lock')
    lock_file_unlock.write('')
    lock_file_notunlock.write('')

    with patch('conf.config.BatchConfig.__init__', new=_init_mock_config):
        assert lock_file_unlock.check(exists=1)
        assert lock_file_notunlock.check(exists=1)
        assert lock_file_notlocked.check(exists=0)

        assert LockFile('unlock', 'dev').unlock()
        assert not LockFile('notlocked', 'dev').unlock()

        assert lock_file_unlock.check(exists=0)
        assert lock_file_notunlock.check(exists=1)
        assert lock_file_notlocked.check(exists=0)

def test_lockfile_withcontext(lock_dir):
    def _init_mock_config(self, env):
        self.lock_dir = str(lock_dir)

    lock_file_canlock    = lock_dir.join('canlock.lock')
    lock_file_cannotlock = lock_dir.join('cannotlock.lock')
    lock_file_cannotlock.write('')

    with patch('conf.config.BatchConfig.__init__', new=_init_mock_config):
        assert lock_file_canlock.check(exists=0)
        assert lock_file_cannotlock.check(exists=1)

        with LockFile('canlock', 'dev'):
            assert lock_file_canlock.check(exists=1)
        assert lock_file_canlock.check(exists=0)

        with pytest.raises(AlreadyLocked):
            with LockFile('cannotlock', 'dev'):
                assert False
        assert lock_file_cannotlock.check(exists=1)


# Test for Class 'LastUpdateFile'
def test_lastupdatefile_update_last_transfer_filename(lock_dir):
    def _init_mock_config(self, env):
        self.lock_dir        = str(lock_dir)
        self.logger_transfer = Mock()

    with patch('conf.config.BatchConfig.__init__', new=_init_mock_config):
        lastupdate_file  = LastUpdateFile('test', 'dev')
        current_filename = 'filetype1_20131015200000_9999.json.gz'

        lastupdate_file.update_last_transfer_filename(current_filename)

        assert lock_dir.join('test_last_update.lock').check(exists=1)
        assert lock_dir.join('test_last_update.lock').read() == current_filename

def test_lastupdatefile_get_last_update_filename(lock_dir):
    def _init_mock_config(self, env):
        self.lock_dir        = str(lock_dir)
        self.logger_transfer = Mock()

    expected_filename = 'filetype_20131015200000_9999.json.gz'
    lock_dir.join('test_last_update.lock').write(expected_filename)

    with patch('conf.config.BatchConfig.__init__', new=_init_mock_config):
        actual_filename = LastUpdateFile('test', 'dev').get_last_update_filename()
        assert actual_filename == expected_filename

def test_lastupdatefile_is_latest(lock_dir):
    def _init_mock_config(self, env):
        self.lock_dir        = str(lock_dir)
        self.logger_transfer = Mock()

    lock_dir.join('test1_last_update.lock').write('filetype_20131015200000_9999.json.gz')
    lock_dir.join('test2_last_update.lock').write('filetype_20131015200000_9999.json.gz')
    lock_dir.join('test3_last_update.lock').write('filetype_20131015200000_9999.json.gz')

    with patch('conf.config.BatchConfig.__init__', new=_init_mock_config):
        assert LastUpdateFile('test1', 'dev').is_latest('filetype_20131015210000_9999.json.gz') is False
        assert LastUpdateFile('test2', 'dev').is_latest('filetype_20131015200000_9999.json.gz') is True
        with pytest.raises(Exception):
            LastUpdateFile('test3', 'dev').is_latest('filetype_20131015190000_9999.json.gz')
