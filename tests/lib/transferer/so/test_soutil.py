# -*- coding: utf-8 -*-

import lib.transferer.so.soutil as soutil

import datetime
from   mock import patch


class CurrentDatetime(datetime.datetime):
    @classmethod
    def now(cls):
        return cls(2013, 10, 19, 23, 40, 59, 0)

def test_create_transfer_filename():
    localdir     = '/tmp/'
    aid          = 'brainpad'
    foreign_id   = '1234123456789012'
    filedatetime = datetime.datetime(2014, 1, 31, 15, 01, 35, 0)
    filetype     = 'recommend'
    seqno        = 3

    with patch('datetime.datetime', CurrentDatetime):
        result = soutil.create_transfer_filename(localdir, aid, foreign_id)
        assert result == '/tmp/brainpad-20131019-234059-1234123456789012.json.gz'

        result = soutil.create_transfer_filename(localdir, aid, foreign_id, filetype=filetype)
        assert result == '/tmp/brainpad-20131019-234059-1234123456789012-recommend.json.gz'

        result = soutil.create_transfer_filename(localdir, aid, foreign_id, filetype=filetype, seqno=seqno)
        assert result == '/tmp/brainpad-20131019-234059-1234123456789012-recommend-003.json.gz'

    result = soutil.create_transfer_filename(localdir, aid, foreign_id, filedatetime)
    assert result == '/tmp/brainpad-20140131-150135-1234123456789012.json.gz'

    result = soutil.create_transfer_filename(localdir, aid, foreign_id, filedatetime, filetype)
    assert result == '/tmp/brainpad-20140131-150135-1234123456789012-recommend.json.gz'

    result = soutil.create_transfer_filename(localdir, aid, foreign_id, filedatetime, filetype, seqno)
    assert result == '/tmp/brainpad-20140131-150135-1234123456789012-recommend-003.json.gz'
