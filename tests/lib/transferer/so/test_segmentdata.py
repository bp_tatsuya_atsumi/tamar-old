# -*- coding: utf-8 -*-

from lib.transferer.so.segmentdata import SegmentDataTransferer

import contextlib
import datetime
import gzip
import json
import os
from   mock import patch
import pytest
from   lib.transferer.exception import NoS3Files


ENV     = 'dev'
SID     = '9999'
SID_GDO = '9998'
PID     = 'so'

S3BUCKET    = 'dev-transfer-data'
S3PATH      = '9999/segment_list_data/raw/split/segmentdata.json.gz'
S3DIR_CNV   = '9999/segment_list_data/converted/so/split'
S3_PATH_MST = '9999/segment_list_mst/raw'
SEQNO       = 3

DATA1  = '{"pid": "rtoaster", "sid": "9999", "uid": "rt_uid1", "attrs": [10, 11, 12]}'
DATA2  = '{"pid": "rtoaster", "sid": "9999", "uid": "rt_uid2", "attrs": [20, 21, 22]}'

MASTER_HEADER = '"id","name","description","ignore"'
MASTER1       = '"10","セグメント10","概要10","ignore_partner"'
MASTER2       = '"11","セグメント11","概要11","ignore_partner"'
MASTER3       = '"12","セグメント12","概要12","ignore_partner"'
MASTER4       = '"20","セグメント20","概要20","ignore_partner"'
MASTER5       = '"21","セグメント21","概要21","ignore_partner"'
MASTER6       = '"22","セグメント22","概要22","ignore_partner"'


class CurrentDatetime(datetime.datetime):
    @classmethod
    def now(cls):
        return cls(2013, 10, 19, 23, 40, 59, 0)

@pytest.fixture
def data_dir(tmp_transfer_dir):
    return tmp_transfer_dir.mkdir(SID).mkdir(PID).mkdir('segmentdata')

@contextlib.contextmanager
def _patches_with_default_action(func_init_config, data_dir):
    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('lib.s3util.S3Util.download_single_file'),
        patch('lib.s3util.S3Util.download_single_file_order_by_filename'),
        patch('lib.transferer.so.soutil.create_transfer_filename'),
        patch('lib.mappingtable.MappingTable.select_partner_uids_by_user_id'),
        patch('lib.sftputil.put'),
        patch('lib.s3util.S3Util.upload_single_file'),
        patch('datetime.datetime', CurrentDatetime)
    ) as (m_config, m_s3get, m_s3get_latest, m_filename, m_mapping, m_sftpput, m_s3put, m_now):
        m_s3get.return_value        = {'key': '9999/segment_list_data/raw/split/segmentdata.json.gz'}
        m_s3get_latest.return_value = {'key': '9999/segment_list_mst/raw/segmentmst.csv.gz'}
        m_mapping.side_effect       = _convert_uid
        m_filename.return_value = os.path.join(str(data_dir), 'transfer_file-003.json.gz')

        yield (m_s3get, m_s3get_latest, m_filename, m_mapping, m_sftpput, m_s3put, m_now)

def _convert_uid(*args):
    tbl = {
        'rt_uid1': ['pt_uid1'],
        'rt_uid2': ['pt_uid2']
    }
    return tbl.get(args[0])

def _target(sid=SID):
    option = {'seqno': SEQNO}
    return SegmentDataTransferer(sid, PID, ENV, S3BUCKET, S3PATH, **option)

def _create_gzip(file, lines):
    with gzip.open(file, 'wb') as f:
        f.write('\n'.join(lines))

def test_transform_transfer_format():
    row = {
            'aid'         : 'rtoaster',
            'sid'         : '9999',
            'uid'         : 'rt_uid1',
            'mapped_uids' : ['pt_uid1'],
            'attrs'       : [10,20]
          }
    result = _target()._transform_transfer_format(row, {'10':'セグメント10', '20':'セグメント20'})
    assert json.loads(result) == json.loads('{"aid":"brainpad","sid":"1234123456789012","uid":"pt_uid1","attrs":{"__Rtoaster__":true,"セグメント10":true,"セグメント20":true}}')

def test_send_transfer_file():
    with patch('lib.sftputil.put') as m_sftpput:
        result = _target()._send_transfer_file('item1.gz')

        assert result
        assert m_sftpput.call_count == 1
        assert m_sftpput.call_args_list[0][1]['host']       == 'sftp-s.c-ovn.jp'
        assert m_sftpput.call_args_list[0][1]['port']       == 115
        assert m_sftpput.call_args_list[0][1]['username']   == 'rtoaster9999'
        assert m_sftpput.call_args_list[0][1]['password']   == 'rtoaster9999'
        assert m_sftpput.call_args_list[0][1]['local_file'] == 'item1.gz'
        assert m_sftpput.call_args_list[0][1]['remote_dir'] == '/receive/so/attrs'
        assert m_sftpput.call_args_list[0][1]['rename']     == True

def test_send_transfer_file_gdo():
    with patch('lib.sftputil.put') as m_sftpput:
        result = _target(SID_GDO)._send_transfer_file('item1.gz')

        assert result
        assert m_sftpput.call_count == 1
        assert m_sftpput.call_args_list[0][1]['host']       == 'sftp-s.c-ovn.jp'
        assert m_sftpput.call_args_list[0][1]['port']       == 115
        assert m_sftpput.call_args_list[0][1]['username']   == 'rtoaster9998'
        assert m_sftpput.call_args_list[0][1]['password']   == 'rtoaster9999'
        assert m_sftpput.call_args_list[0][1]['local_file'] == 'item1.gz'
        assert m_sftpput.call_args_list[0][1]['remote_dir'] == '/receive/so/attrs'
        assert m_sftpput.call_args_list[0][1]['rename']     == True

def test_send_transfer_file_failed():
    with patch('lib.sftputil.put') as m_sftpput:
        m_sftpput.side_effect = Exception()

        result = _target()._send_transfer_file('item1.gz')

        assert not result
        assert m_sftpput.call_count == 1
        assert m_sftpput.call_args_list[0][1]['host']       == 'sftp-s.c-ovn.jp'
        assert m_sftpput.call_args_list[0][1]['port']       == 115
        assert m_sftpput.call_args_list[0][1]['username']   == 'rtoaster9999'
        assert m_sftpput.call_args_list[0][1]['password']   == 'rtoaster9999'
        assert m_sftpput.call_args_list[0][1]['local_file'] == 'item1.gz'
        assert m_sftpput.call_args_list[0][1]['remote_dir'] == '/receive/so/attrs'
        assert m_sftpput.call_args_list[0][1]['rename']     == True

def test_execute(func_init_config, data_dir):

    # prepare
    data_file     = data_dir.join('segmentdata.json.gz')
    master_file   = data_dir.join('segmentmst.csv.gz')
    transfer_file = data_dir.join('transfer_file-003.json.gz')

    _create_gzip(str(data_file), [DATA1, DATA2])
    _create_gzip(str(master_file), [MASTER_HEADER, MASTER1, MASTER2, MASTER3, MASTER4, MASTER5, MASTER6])

    # execute
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_s3get_latest, m_filename, m_mapping, m_sftpput, m_s3put, m_now = mocks

        result = _target().execute()

    # assert
    assert result.is_ok()
    assert result.counter['user.rtoaster'] == 2
    assert result.counter['user.mapped']   == 2
    assert result.counter['user.unmapped'] == 0
    assert result.counter['user.invalid']  == 0
    assert data_file.check(exists=0)
    assert transfer_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3PATH, str(data_dir))
    assert m_s3get_latest.call_count == 1
    assert m_s3get_latest.call_args_list[0][0] == (S3BUCKET, S3_PATH_MST, str(data_dir))

    assert m_filename.call_count == 1
    assert m_filename.call_args_list[0][0]                 == (str(data_dir), 'brainpad', '1234123456789012')
    assert m_filename.call_args_list[0][1]['filedatetime'] == datetime.datetime(2013, 10, 19, 23, 40, 59, 0)
    assert m_filename.call_args_list[0][1]['seqno']        == 3

    assert m_mapping.call_count == 2
    assert m_mapping.call_args_list[0][0] == ('rt_uid1',)
    assert m_mapping.call_args_list[1][0] == ('rt_uid2',)

    assert m_sftpput.call_count == 1
    assert m_sftpput.call_args_list[0][1]['host']       == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[0][1]['port']       == 115
    assert m_sftpput.call_args_list[0][1]['username']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['password']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['local_file'] == str(transfer_file)
    assert m_sftpput.call_args_list[0][1]['remote_dir'] == '/receive/so/attrs'
    assert m_sftpput.call_args_list[0][1]['rename']     == True

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(transfer_file), S3BUCKET, S3DIR_CNV)

def test_execute_s3data_notfound(func_init_config, data_dir):

    # execute
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_s3get_latest, m_filename, m_mapping, m_sftpput, m_s3put, m_now = mocks
        m_s3get.side_effect = [None]

        result = _target().execute()

    # assert
    assert result.is_ng()
    assert result.counter['user.rtoaster'] == 0
    assert result.counter['user.mapped']   == 0
    assert result.counter['user.unmapped'] == 0
    assert result.counter['user.invalid']  == 0

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3PATH, str(data_dir))
    assert m_s3get_latest.call_count == 0
    assert m_filename.call_count     == 0
    assert m_mapping.call_count      == 0
    assert m_s3put.call_count        == 0

def test_execute_master_notfound(func_init_config, data_dir):

    # execute
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_s3get_latest, m_filename, m_mapping, m_sftpput, m_s3put, m_now = mocks
        m_s3get_latest.side_effect = [None]

        result = _target().execute()

    # assert
    assert result.is_ng()
    assert result.counter['user.rtoaster'] == 0
    assert result.counter['user.mapped']   == 0
    assert result.counter['user.unmapped'] == 0
    assert result.counter['user.invalid']  == 0

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3PATH, str(data_dir))
    assert m_s3get_latest.call_count == 1
    assert m_s3get_latest.call_args_list[0][0] == (S3BUCKET, S3_PATH_MST, str(data_dir))

    assert m_filename.call_count == 0
    assert m_mapping.call_count  == 0
    assert m_s3put.call_count    == 0

def test_execute_sftp_error(func_init_config, data_dir):

    # prepare
    data_file     = data_dir.join('segmentdata.json.gz')
    master_file   = data_dir.join('segmentmst.csv.gz')
    transfer_file = data_dir.join('transfer_file-003.json.gz')

    _create_gzip(str(data_file), [DATA1, DATA2])
    _create_gzip(str(master_file), [MASTER_HEADER, MASTER1, MASTER2, MASTER3, MASTER4, MASTER5, MASTER6])

    # execute
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_s3get_latest, m_filename, m_mapping, m_sftpput, m_s3put, m_now = mocks
        m_sftpput.side_effect = Exception()

        result = _target().execute()

    # assert
    assert result.is_ng()
    assert result.counter['user.rtoaster'] == 2
    assert result.counter['user.mapped']   == 2
    assert result.counter['user.unmapped'] == 0
    assert result.counter['user.invalid']  == 0
    assert data_file.check(exists=0)
    assert transfer_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3PATH, str(data_dir))
    assert m_s3get_latest.call_count == 1
    assert m_s3get_latest.call_args_list[0][0] == (S3BUCKET, S3_PATH_MST, str(data_dir))

    assert m_filename.call_count == 1
    assert m_filename.call_args_list[0][0]                 == (str(data_dir), 'brainpad', '1234123456789012')
    assert m_filename.call_args_list[0][1]['filedatetime'] == datetime.datetime(2013, 10, 19, 23, 40, 59, 0)
    assert m_filename.call_args_list[0][1]['seqno']        == 3

    assert m_mapping.call_count == 2
    assert m_mapping.call_args_list[0][0] == ('rt_uid1',)
    assert m_mapping.call_args_list[1][0] == ('rt_uid2',)

    assert m_sftpput.call_count == 1
    assert m_sftpput.call_args_list[0][1]['host']       == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[0][1]['port']       == 115
    assert m_sftpput.call_args_list[0][1]['username']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['password']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['local_file'] == str(transfer_file)
    assert m_sftpput.call_args_list[0][1]['remote_dir'] == '/receive/so/attrs'
    assert m_sftpput.call_args_list[0][1]['rename']     == True

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(transfer_file), S3BUCKET, S3DIR_CNV)

def test_execute_unexpected_error(func_init_config, data_dir):

    # prepare
    data_file     = data_dir.join('segmentdata.json.gz')
    master_file   = data_dir.join('segmentmst.csv.gz')
    transfer_file = data_dir.join('transfer_file-003.json.gz')

    _create_gzip(str(data_file), [DATA1, DATA2])
    _create_gzip(str(master_file), [MASTER_HEADER, MASTER1, MASTER2, MASTER3, MASTER4, MASTER5, MASTER6])

    # execute
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_s3get_latest, m_filename, m_mapping, m_sftpput, m_s3put, m_now = mocks
        m_mapping.side_effect = Exception()

        result = _target().execute()

    # assert
    assert result.is_ng()
    assert result.counter['user.rtoaster'] == 0
    assert result.counter['user.mapped']   == 0
    assert result.counter['user.unmapped'] == 0
    assert result.counter['user.invalid']  == 0
    assert data_file.check(exists=0)
    assert transfer_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3PATH, str(data_dir))
    assert m_s3get_latest.call_count == 1
    assert m_s3get_latest.call_args_list[0][0] == (S3BUCKET, S3_PATH_MST, str(data_dir))

    assert m_filename.call_count == 1
    assert m_filename.call_args_list[0][0]                 == (str(data_dir), 'brainpad', '1234123456789012')
    assert m_filename.call_args_list[0][1]['filedatetime'] == datetime.datetime(2013, 10, 19, 23, 40, 59, 0)
    assert m_filename.call_args_list[0][1]['seqno']        == 3

    assert m_mapping.call_count == 1
    assert m_mapping.call_args_list[0][0] == ('rt_uid1',)

    assert m_sftpput.call_count == 0
    assert m_s3put.call_count   == 0
