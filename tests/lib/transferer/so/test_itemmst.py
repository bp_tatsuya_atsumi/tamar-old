# -*- coding: utf-8 -*-

from lib.transferer.so.itemmst import ItemMstTransferer

import contextlib
import gzip
import os
from   mock import patch
import pytest


ENV = 'dev'
SID = '9999'
PID = 'so'

S3_BUCKET      = 'dev-transfer-data'
S3_PATH_RAW    = '9999/item_mst/raw'
S3_PATH_LATEST = '9999/item_mst/latest'
S3_PATH_CNV    = '9999/item_mst/converted/so'

CSV_HEADER = 'pid,sid,code,name,img_url,link_url,price,description,status,last_modified_date'
CSV_DATA1  = 'rtoaster,999,Item_1000,商品名1000,http://imageurl/1000.jpg,http://linkurl/1000,"<br>1,000<br>",説明1000,0,2013-09-06 15:20:53'
CSV_DATA2  = 'rtoaster,999,Item_2000,商品名2000,http://imageurl/2000.jpg,http://linkurl/2000,"<br>2,000<br>",説明2000,0,2013-09-06 15:20:53'
CSV_DATA3  = 'rtoaster,999,Item_3000,商品名3000,http://imageurl/3000.jpg,http://linkurl/3000,"<br>3,000<br>",説明3000,0,2013-09-06 15:20:53'
CSV_DATA4  = 'rtoaster,999,Item_4000,商品名4000,http://imageurl/4000.jpg,http://linkurl/4000,"<br>4,000<br>",説明4000,1,2013-09-06 15:20:53'
CSV_DATA5  = 'rtoaster,999,Item_5000,商品名5000,http://imageurl/5000.jpg,http://linkutl/5000,"<br>5,000<br>",説明5000,1,2013-09-06 15:20:53'

@pytest.fixture
def data_dir(tmp_transfer_dir):
    return tmp_transfer_dir.mkdir(SID).mkdir(PID).mkdir('itemmst')

@contextlib.contextmanager
def _patches_with_default_action(func_init_config, data_dir):
    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('lib.s3util.S3Util.download_single_file_order_by_filename'),
        patch('lib.transferer.so.soutil.create_transfer_filename'),
        patch('lib.sftputil.put'),
        patch('lib.s3util.S3Util.upload_single_file'),
    ) as (m_config, m_s3get, m_filename, m_sftpput, m_s3put):
        m_s3get.return_value       = {'key': '9999/item_mst/latest/itemmst_latest_20131001201510_9999.csv.gz'}
        m_filename.return_value    = os.path.join(str(data_dir), 'transfer_file.json.gz')

        yield (m_s3get, m_filename, m_sftpput, m_s3put)

def _target():
    return ItemMstTransferer(SID, PID, ENV, S3_BUCKET, S3_PATH_RAW)

def _create_gzip(file, lines):
    with gzip.open(file, 'wb') as f:
        f.write('\n'.join(lines))

def test_transform_transfer_format():
    target = _target()
    target.foreign_id = 'foreign_id'
    row = {
        'pid'         : 'pid',
        'sid'         : 'sid',
        'code'        : 'code',
        'name'        : u'商品名称'.encode('utf8'),
        'img_url'     : 'http://domain/img.jpg',
        'link_url'    : 'http://domain/link',
        'price'       : u'商品金額'.encode('utf8'),
        'description' : u'商品概要'.encode('utf8')
    }
    result = target._transform_transfer_format(row)
    assert result == '{"aid":"brainpad","sid":"foreign_id","code":"code","name":"商品名称","imageUrl":"http://domain/img.jpg","url":"http://domain/link","price":"商品金額","comment":"商品概要"}'

def test_send_transformed_file():
    with patch('lib.sftputil.put') as m_sftpput:
        result = _target()._send_transformed_file('item1.gz')

        assert result
        assert m_sftpput.call_count == 1
        assert m_sftpput.call_args_list[0][1]['host']       == 'sftp-s.c-ovn.jp'
        assert m_sftpput.call_args_list[0][1]['port']       == 115
        assert m_sftpput.call_args_list[0][1]['username']   == 'rtoaster9999'
        assert m_sftpput.call_args_list[0][1]['password']   == 'rtoaster9999'
        assert m_sftpput.call_args_list[0][1]['local_file'] == 'item1.gz'
        assert m_sftpput.call_args_list[0][1]['remote_dir'] == '/receive/so/content'
        assert m_sftpput.call_args_list[0][1]['rename']     == True

def test_send_transformed_file_failed():
    with patch('lib.sftputil.put') as m_sftpput:
        m_sftpput.side_effect = Exception()

        result = _target()._send_transformed_file('item1.gz')

        assert not result
        assert m_sftpput.call_count == 1
        assert m_sftpput.call_args_list[0][1]['host']       == 'sftp-s.c-ovn.jp'
        assert m_sftpput.call_args_list[0][1]['port']       == 115
        assert m_sftpput.call_args_list[0][1]['username']   == 'rtoaster9999'
        assert m_sftpput.call_args_list[0][1]['password']   == 'rtoaster9999'
        assert m_sftpput.call_args_list[0][1]['local_file'] == 'item1.gz'
        assert m_sftpput.call_args_list[0][1]['remote_dir'] == '/receive/so/content'
        assert m_sftpput.call_args_list[0][1]['rename']     == True

def test_execute(func_init_config, data_dir):

    # prepare
    data_file     = data_dir.join('itemmst_latest_20131001201510_9999.csv.gz')
    transfer_file = data_dir.join('transfer_file.json.gz')

    _create_gzip(str(data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2, CSV_DATA3, CSV_DATA4, CSV_DATA5])

    # execute
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_filename, m_sftpput, m_s3put = mocks

        result = _target().execute()

    # assert
    assert result.is_ok()
    assert result.counter['item.rtoaster'] == 5
    assert data_file.check(exists=0)
    assert transfer_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3_BUCKET, S3_PATH_LATEST, str(data_dir))

    assert m_filename.call_count == 1
    assert m_filename.call_args_list[0][0]             == (str(data_dir), 'brainpad', '1234123456789012')
    assert m_filename.call_args_list[0][1]['filetype'] == 'item'

    assert m_sftpput.call_count == 1
    assert m_sftpput.call_args_list[0][1]['host']       == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[0][1]['port']       == 115
    assert m_sftpput.call_args_list[0][1]['username']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['password']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['local_file'] == str(transfer_file)
    assert m_sftpput.call_args_list[0][1]['remote_dir'] == '/receive/so/content'
    assert m_sftpput.call_args_list[0][1]['rename']     == True

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(transfer_file), S3_BUCKET, S3_PATH_CNV)

def test_execute_s3file_notfound(func_init_config, data_dir):

    # execute
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_filename, m_sftpput, m_s3put = mocks
        m_s3get.return_value = None

        result = _target().execute()

    # assert
    assert result.is_ng()
    assert result.counter['item.rtoaster'] == 0

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3_BUCKET, S3_PATH_LATEST, str(data_dir))

    assert m_filename.call_count == 0
    assert m_sftpput.call_count  == 0
    assert m_s3put.call_count    == 0

def test_execute_sftp_error(func_init_config, data_dir):

    # prepare
    data_file     = data_dir.join('itemmst_latest_20131001201510_9999.csv.gz')
    transfer_file = data_dir.join('transfer_file.json.gz')

    _create_gzip(str(data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2, CSV_DATA3, CSV_DATA4, CSV_DATA5])

    # execute
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_filename, m_sftpput, m_s3put = mocks
        m_sftpput.side_effect = Exception()

        result = _target().execute()

    # assert
    assert result.is_ng()
    assert result.counter['item.rtoaster'] == 5
    assert data_file.check(exists=0)
    assert transfer_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3_BUCKET, S3_PATH_LATEST, str(data_dir))

    assert m_filename.call_count == 1
    assert m_filename.call_args_list[0][0]             == (str(data_dir), 'brainpad', '1234123456789012')
    assert m_filename.call_args_list[0][1]['filetype'] == 'item'

    assert m_sftpput.call_count == 1
    assert m_sftpput.call_args_list[0][1]['host']       == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[0][1]['port']       == 115
    assert m_sftpput.call_args_list[0][1]['username']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['password']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['local_file'] == str(transfer_file)
    assert m_sftpput.call_args_list[0][1]['remote_dir'] == '/receive/so/content'
    assert m_sftpput.call_args_list[0][1]['rename']     == True

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(transfer_file), S3_BUCKET, S3_PATH_CNV)

def test_execute_unexpected_error(func_init_config, data_dir):

    # prepare
    data_file     = data_dir.join('itemmst_latest_20131001201510_9999.csv.gz')
    transfer_file = data_dir.join('transfer_file.json.gz')

    _create_gzip(str(data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2, CSV_DATA3, CSV_DATA4, CSV_DATA5])

    # execute
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_filename, m_sftpput, m_s3put = mocks
        m_filename.side_effect = Exception()

        result = _target().execute()

    # assert
    assert result.is_ng()
    assert result.counter['item.rtoaster'] == 0
    assert data_file.check(exists=0)
    assert transfer_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3_BUCKET, S3_PATH_LATEST, str(data_dir))

    assert m_filename.call_count == 1
    assert m_filename.call_args_list[0][0]             == (str(data_dir), 'brainpad', '1234123456789012')
    assert m_filename.call_args_list[0][1]['filetype'] == 'item'

    assert m_sftpput.call_count == 0
    assert m_s3put.call_count   == 0
