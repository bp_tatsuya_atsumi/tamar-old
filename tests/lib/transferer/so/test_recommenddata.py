# -*- coding: utf-8 -*-

from lib.transferer.so.recommenddata import RecommendDataTransferer

import contextlib
import datetime
import gzip
import os
import pytest
from lib.transferer.exception import NoS3Files
from mock import patch


ENV = 'dev'
SID = '9999'
PID = 'so'

S3_BUCKET   = 'dev-transfer-data'
S3_PATH_RAW = '9999/recommend_list_data/raw/segmentdata.json.gz'
S3_PATH_CNV = '9999/recommend_list_data/converted/so'
S3_PATH_MST = '9999/recommend_list_mst/raw'

DATA1 = '{"pid": "rtoaster", "sid": "9999", "uid": "rt_uid1", "item_lists": {"1": ["rtoaster", "l2mixer"], "2": ["rtoaster", "l2mixer"]}}'
DATA2 = '{"pid": "rtoaster", "sid": "9999", "uid": "rt_uid2", "item_lists": {"1": ["crossoven", "semanticfinder"], "2": ["crossoven", "semanticfinder"]}}'

MASTER_HEADER = '"id","name","description"'
MASTER1       = '"1","basket",""'
MASTER2       = '"2","history",""'

class CurrentDatetime(datetime.datetime):
    @classmethod
    def now(cls):
        return cls(2013, 10, 19, 23, 40, 59, 0)

@pytest.fixture
def data_dir(tmp_transfer_dir):
    return tmp_transfer_dir.mkdir(SID).mkdir(PID).mkdir('recommenddata')

@contextlib.contextmanager
def _patches_with_default_action(func_init_config, data_dir):
    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('lib.s3util.S3Util.download_single_file'),
        patch('lib.s3util.S3Util.download_single_file_order_by_filename'),
        patch('lib.transferer.so.soutil.create_transfer_filename'),
        patch('lib.mappingtable.MappingTable.select_partner_uids_by_user_id'),
        patch('lib.sftputil.put'),
        patch('lib.s3util.S3Util.upload_single_file'),
        patch('datetime.datetime', CurrentDatetime)
    ) as (m_config, m_s3get, m_s3get_latest, m_filename, m_mapping, m_sftpput, m_s3put, m_now):
        m_s3get.side_effect        = [ {'key': '9999/recommend_list_data/raw/recommenddata.json.gz'} ]
        m_s3get_latest.side_effect = [ {'key': '9999/recommend_list_mst/raw/recommendmst.csv.gz'} ]
        m_mapping.side_effect      = _convert_uid
        m_filename.side_effect     = [
                                        os.path.join(str(data_dir), 'transfer_file-001.json.gz'),
                                        os.path.join(str(data_dir), 'transfer_file-002.json.gz'),
                                        os.path.join(str(data_dir), 'transfer_file-003.json.gz')
                                     ]

        yield (m_s3get, m_s3get_latest, m_filename, m_mapping, m_sftpput, m_s3put, m_now)

def _convert_uid(*args):
    tbl = {
        'rt_uid1': ['pt_uid1'],
        'rt_uid2': ['pt_uid2'],
    }
    return tbl.get(args[0])

def _target():
    return RecommendDataTransferer(SID, PID, ENV, S3_BUCKET, S3_PATH_RAW)

def _create_gzip(file, lines):
    with gzip.open(file, 'wb') as f:
        f.write('\n'.join(lines))

def test_transform_transfer_format():
    target = _target()
    target.foreign_id = 'foreign_id'
    recid_recnames = {
                        '1': 'basket',
                        '2': 'history',
                     }
    row = {
            'pid'         : 'rtoaster',
            'sid'         : '9999',
            'uid'         : 'rt_uid1',
            'mapped_uids' : ['pt_uid1'],
            'item_lists'  : {
                                 '1' : ['rtoaster',  'l2mixer'],
                                 '2' : ['crossoven', 'semanticfinder'],
                            }
          }
    result = target.transform_transfer_format(row, recid_recnames)
    assert result == '{"aid":"brainpad","sid":"foreign_id","uid":"pt_uid1","item_lists":{"basket":["rtoaster","l2mixer"],"history":["crossoven","semanticfinder"]}}'

def test_send_transformed_file():
    with patch('lib.sftputil.put') as m_sftpput:
        result = _target().send_transformed_file('item1.gz')

        assert result
        assert m_sftpput.call_count == 1
        assert m_sftpput.call_args_list[0][1]['host']       == 'sftp-s.c-ovn.jp'
        assert m_sftpput.call_args_list[0][1]['port']       == 115
        assert m_sftpput.call_args_list[0][1]['username']   == 'rtoaster9999'
        assert m_sftpput.call_args_list[0][1]['password']   == 'rtoaster9999'
        assert m_sftpput.call_args_list[0][1]['local_file'] == 'item1.gz'
        assert m_sftpput.call_args_list[0][1]['remote_dir'] == '/receive/so/extra'
        assert m_sftpput.call_args_list[0][1]['rename']     == True

def test_send_transformed_file_failed():
    with patch('lib.sftputil.put') as m_sftpput:
        m_sftpput.side_effect = Exception()

        result = _target().send_transformed_file('item1.gz')

        assert not result
        assert m_sftpput.call_count == 1
        assert m_sftpput.call_args_list[0][1]['host']       == 'sftp-s.c-ovn.jp'
        assert m_sftpput.call_args_list[0][1]['port']       == 115
        assert m_sftpput.call_args_list[0][1]['username']   == 'rtoaster9999'
        assert m_sftpput.call_args_list[0][1]['password']   == 'rtoaster9999'
        assert m_sftpput.call_args_list[0][1]['local_file'] == 'item1.gz'
        assert m_sftpput.call_args_list[0][1]['remote_dir'] == '/receive/so/extra'
        assert m_sftpput.call_args_list[0][1]['rename']     == True

def test_execute(func_init_config, data_dir):

    # prepare
    data_file     = data_dir.join('recommenddata.json.gz')
    master_file   = data_dir.join('recommendmst.csv.gz')
    transfer_file = data_dir.join('transfer_file-001.json.gz')

    _create_gzip(str(data_file),   [DATA1, DATA2])
    _create_gzip(str(master_file), [MASTER_HEADER, MASTER1, MASTER2])

    # execute
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_s3get_latest, m_filename, m_mapping, m_sftpput, m_s3put, m_now = mocks

        result = _target().execute()

    # assert
    assert result.is_ok()
    assert result.counter['user.rtoaster'] == 2
    assert result.counter['user.mapped']   == 2
    assert result.counter['user.unmapped'] == 0
    assert result.counter['user.invalid']  == 0
    assert data_file.check(exists=0)
    assert master_file.check(exists=0)
    assert transfer_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3_BUCKET, S3_PATH_RAW, str(data_dir))
    assert m_s3get_latest.call_count == 1
    assert m_s3get_latest.call_args_list[0][0] == (S3_BUCKET, S3_PATH_MST, str(data_dir))

    assert m_filename.call_count == 1
    assert m_filename.call_args_list[0][0]                 == (str(data_dir), 'brainpad', '1234123456789012')
    assert m_filename.call_args_list[0][1]['filedatetime'] == datetime.datetime(2013, 10, 19, 23, 40, 59, 0)
    assert m_filename.call_args_list[0][1]['filetype']     == 'recommend'
    assert m_filename.call_args_list[0][1]['seqno']        == 1

    assert m_mapping.call_count == 2
    assert m_mapping.call_args_list[0][0] == (u'rt_uid1',)
    assert m_mapping.call_args_list[1][0] == (u'rt_uid2',)

    assert m_sftpput.call_count == 1
    assert m_sftpput.call_args_list[0][1]['host']       == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[0][1]['port']       == 115
    assert m_sftpput.call_args_list[0][1]['username']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['password']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['local_file'] == str(transfer_file)
    assert m_sftpput.call_args_list[0][1]['remote_dir'] == '/receive/so/extra'
    assert m_sftpput.call_args_list[0][1]['rename']     == True

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(transfer_file), S3_BUCKET, S3_PATH_CNV)

def test_execute_s3data_notfound(func_init_config, data_dir):

    # execute
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_s3get_latest, m_filename, m_mapping, m_sftpput, m_s3put, m_now = mocks
        m_s3get.side_effect = [None]

        result = _target().execute()

    # assert
    assert result.is_ng()
    assert result.counter['user.rtoaster'] == 0
    assert result.counter['user.mapped']   == 0
    assert result.counter['user.unmapped'] == 0
    assert result.counter['user.invalid']  == 0

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3_BUCKET, S3_PATH_RAW, str(data_dir))

    assert m_filename.call_count    == 0
    assert m_mapping.call_count     == 0
    assert m_s3put.call_count       == 0

def test_execute_s3master_notfound(func_init_config, data_dir):

    # prepare
    data_file = data_dir.join('recommenddata.json.gz')
    _create_gzip(str(data_file), [DATA1, DATA2])

    # execute
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_s3get_latest, m_filename, m_mapping, m_sftpput, m_s3put, m_now = mocks
        m_s3get.side_effect = [{'key': 'recommenddata.json.gz'}, None]

        result = _target().execute()

    # assert
    assert result.is_ng()
    assert result.counter['user.rtoaster'] == 0
    assert result.counter['user.mapped']   == 0
    assert result.counter['user.unmapped'] == 0
    assert result.counter['user.invalid']  == 0
    assert data_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3_BUCKET, S3_PATH_RAW, str(data_dir))
    assert m_s3get_latest.call_count == 1
    assert m_s3get_latest.call_args_list[0][0] == (S3_BUCKET, S3_PATH_MST, str(data_dir))

    assert m_filename.call_count == 0
    assert m_mapping.call_count  == 0
    assert m_sftpput.call_count  == 0
    assert m_s3put.call_count    == 0

def test_execute_sftp_error(func_init_config, data_dir):

    # prepare
    data_file     = data_dir.join('recommenddata.json.gz')
    master_file   = data_dir.join('recommendmst.csv.gz')
    transfer_file = data_dir.join('transfer_file-001.json.gz')

    _create_gzip(str(data_file),   [DATA1, DATA2])
    _create_gzip(str(master_file), [MASTER_HEADER, MASTER1, MASTER2])

    # execute
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_s3get_latest, m_filename, m_mapping, m_sftpput, m_s3put, m_now = mocks
        m_sftpput.side_effect = Exception()

        result = _target().execute()

    # assert
    assert result.is_ng()
    assert result.counter['user.rtoaster'] == 0
    assert result.counter['user.mapped']   == 0
    assert result.counter['user.unmapped'] == 0
    assert result.counter['user.invalid']  == 0
    assert data_file.check(exists=0)
    assert master_file.check(exists=0)
    assert transfer_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3_BUCKET, S3_PATH_RAW, str(data_dir))
    assert m_s3get_latest.call_count == 1
    assert m_s3get_latest.call_args_list[0][0] == (S3_BUCKET, S3_PATH_MST, str(data_dir))

    assert m_filename.call_count == 1
    assert m_filename.call_args_list[0][0]                 == (str(data_dir), 'brainpad', '1234123456789012')
    assert m_filename.call_args_list[0][1]['filedatetime'] == datetime.datetime(2013, 10, 19, 23, 40, 59, 0)
    assert m_filename.call_args_list[0][1]['filetype']     == 'recommend'
    assert m_filename.call_args_list[0][1]['seqno']        == 1

    assert m_mapping.call_count == 2
    assert m_mapping.call_args_list[0][0] == (u'rt_uid1',)
    assert m_mapping.call_args_list[1][0] == (u'rt_uid2',)

    assert m_sftpput.call_count == 1
    assert m_sftpput.call_args_list[0][1]['host']       == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[0][1]['port']       == 115
    assert m_sftpput.call_args_list[0][1]['username']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['password']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['local_file'] == str(transfer_file)
    assert m_sftpput.call_args_list[0][1]['remote_dir'] == '/receive/so/extra'
    assert m_sftpput.call_args_list[0][1]['rename']     == True

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(transfer_file), S3_BUCKET, S3_PATH_CNV)

def test_execute_unexpected_error(func_init_config, data_dir):

    # prepare
    data_file     = data_dir.join('recommenddata.json.gz')
    master_file   = data_dir.join('recommendmst.csv.gz')
    transfer_file = data_dir.join('transfer_file-001.json.gz')

    _create_gzip(str(data_file),   [DATA1, DATA2])
    _create_gzip(str(master_file), [MASTER_HEADER, MASTER1, MASTER2])

    # execute
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_s3get_latest, m_filename, m_mapping, m_sftpput, m_s3put, m_now = mocks
        m_mapping.side_effect = Exception()

        result = _target().execute()

    # assert
    assert result.is_ng()
    assert result.counter['user.rtoaster'] == 0
    assert result.counter['user.mapped']   == 0
    assert result.counter['user.unmapped'] == 0
    assert result.counter['user.invalid']  == 0
    assert data_file.check(exists=0)
    assert master_file.check(exists=0)
    assert transfer_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3_BUCKET, S3_PATH_RAW, str(data_dir))
    assert m_s3get_latest.call_count == 1
    assert m_s3get_latest.call_args_list[0][0] == (S3_BUCKET, S3_PATH_MST, str(data_dir))

    assert m_filename.call_count == 1
    assert m_filename.call_args_list[0][0]                 == (str(data_dir), 'brainpad', '1234123456789012')
    assert m_filename.call_args_list[0][1]['filedatetime'] == datetime.datetime(2013, 10, 19, 23, 40, 59, 0)
    assert m_filename.call_args_list[0][1]['filetype']     == 'recommend'
    assert m_filename.call_args_list[0][1]['seqno']        == 1

    assert m_mapping.call_count == 1
    assert m_mapping.call_args_list[0][0] == (u'rt_uid1',)

    assert m_sftpput.call_count == 0
    assert m_s3put.call_count   == 0

def test_execute_multifiles(func_init_config, data_dir):

    # prepare
    data_file      = data_dir.join('recommenddata.json.gz')
    master_file    = data_dir.join('recommendmst.csv.gz')
    transfer_file1 = data_dir.join('transfer_file-001.json.gz')
    transfer_file2 = data_dir.join('transfer_file-002.json.gz')
    transfer_file3 = data_dir.join('transfer_file-003.json.gz')

    _create_gzip(str(data_file),   [DATA1, DATA1, DATA1, DATA1, DATA1])
    _create_gzip(str(master_file), [MASTER_HEADER, MASTER1, MASTER2])

    # execute
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_s3get_latest, m_filename, m_mapping, m_sftpput, m_s3put, m_now = mocks

        target = _target()
        target.SPLIT_LINE_NUM = 2
        result = target.execute()

    # assert
    assert result.is_ok()
    assert result.counter['user.rtoaster'] == 5
    assert result.counter['user.mapped']   == 5
    assert result.counter['user.unmapped'] == 0
    assert result.counter['user.invalid']  == 0
    assert data_file.check(exists=0)
    assert master_file.check(exists=0)
    assert transfer_file1.check(exists=0)
    assert transfer_file2.check(exists=0)
    assert transfer_file3.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3_BUCKET, S3_PATH_RAW, str(data_dir))
    assert m_s3get_latest.call_count == 1
    assert m_s3get_latest.call_args_list[0][0] == (S3_BUCKET, S3_PATH_MST, str(data_dir))

    assert m_filename.call_count == 3
    assert m_filename.call_args_list[0][0]                 == (str(data_dir), 'brainpad', '1234123456789012')
    assert m_filename.call_args_list[0][1]['filedatetime'] == datetime.datetime(2013, 10, 19, 23, 40, 59, 0)
    assert m_filename.call_args_list[0][1]['filetype']     == 'recommend'
    assert m_filename.call_args_list[0][1]['seqno']        == 1

    assert m_filename.call_args_list[1][0]                 == (str(data_dir), 'brainpad', '1234123456789012')
    assert m_filename.call_args_list[1][1]['filedatetime'] == datetime.datetime(2013, 10, 19, 23, 40, 59, 0)
    assert m_filename.call_args_list[1][1]['filetype']     == 'recommend'
    assert m_filename.call_args_list[1][1]['seqno']        == 2

    assert m_filename.call_args_list[2][0]                 == (str(data_dir), 'brainpad', '1234123456789012')
    assert m_filename.call_args_list[2][1]['filedatetime'] == datetime.datetime(2013, 10, 19, 23, 40, 59, 0)
    assert m_filename.call_args_list[2][1]['filetype']     == 'recommend'
    assert m_filename.call_args_list[2][1]['seqno']        == 3

    assert m_mapping.call_count == 5
    assert m_mapping.call_args_list[0][0] == (u'rt_uid1',)
    assert m_mapping.call_args_list[1][0] == (u'rt_uid1',)
    assert m_mapping.call_args_list[2][0] == (u'rt_uid1',)
    assert m_mapping.call_args_list[3][0] == (u'rt_uid1',)
    assert m_mapping.call_args_list[4][0] == (u'rt_uid1',)

    assert m_sftpput.call_count == 3
    assert m_sftpput.call_args_list[0][1]['host']       == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[0][1]['port']       == 115
    assert m_sftpput.call_args_list[0][1]['username']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['password']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['local_file'] == str(transfer_file1)
    assert m_sftpput.call_args_list[0][1]['remote_dir'] == '/receive/so/extra'
    assert m_sftpput.call_args_list[0][1]['rename']     == True

    assert m_sftpput.call_args_list[1][1]['host']       == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[1][1]['port']       == 115
    assert m_sftpput.call_args_list[1][1]['username']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[1][1]['password']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[1][1]['local_file'] == str(transfer_file2)
    assert m_sftpput.call_args_list[1][1]['remote_dir'] == '/receive/so/extra'
    assert m_sftpput.call_args_list[1][1]['rename']     == True

    assert m_sftpput.call_args_list[2][1]['host']       == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[2][1]['port']       == 115
    assert m_sftpput.call_args_list[2][1]['username']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[2][1]['password']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[2][1]['local_file'] == str(transfer_file3)
    assert m_sftpput.call_args_list[2][1]['remote_dir'] == '/receive/so/extra'
    assert m_sftpput.call_args_list[2][1]['rename']     == True

    assert m_s3put.call_count == 3
    assert m_s3put.call_args_list[0][0] == (str(transfer_file1), S3_BUCKET, S3_PATH_CNV)

def test_execute_multifiles_no_modulo(func_init_config, data_dir):

    # prepare
    data_file      = data_dir.join('recommenddata.json.gz')
    master_file    = data_dir.join('recommendmst.csv.gz')
    transfer_file1 = data_dir.join('transfer_file-001.json.gz')
    transfer_file2 = data_dir.join('transfer_file-002.json.gz')
    transfer_file3 = data_dir.join('transfer_file-003.json.gz')

    _create_gzip(str(data_file),   [DATA1, DATA1, DATA1, DATA1, DATA1, DATA1])
    _create_gzip(str(master_file), [MASTER_HEADER, MASTER1, MASTER2])

    # execute
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_s3get_latest, m_filename, m_mapping, m_sftpput, m_s3put, m_now = mocks

        target = _target()
        target.SPLIT_LINE_NUM = 2
        result = target.execute()

    # assert
    assert result.is_ok()
    assert result.counter['user.rtoaster'] == 6
    assert result.counter['user.mapped']   == 6
    assert result.counter['user.unmapped'] == 0
    assert result.counter['user.invalid']  == 0
    assert data_file.check(exists=0)
    assert master_file.check(exists=0)
    assert transfer_file1.check(exists=0)
    assert transfer_file2.check(exists=0)
    assert transfer_file3.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get_latest.call_count == 1
    assert m_filename.call_count == 3
    assert m_mapping.call_count == 6
    assert m_sftpput.call_count == 3
    assert m_s3put.call_count == 3

def test_execute_invalid_lines(func_init_config, data_dir):

    # prepare
    data_file      = data_dir.join('recommenddata.json.gz')
    master_file    = data_dir.join('recommendmst.csv.gz')

    _create_gzip(str(data_file),   ['{}'])
    _create_gzip(str(master_file), [MASTER_HEADER, MASTER1, MASTER2])

    # execute
    with _patches_with_default_action(func_init_config, data_dir):
        result = _target().execute()

    # assert
    assert result.is_api_error()
    assert result.counter['user.rtoaster'] == 1
    assert result.counter['user.mapped']   == 0
    assert result.counter['user.unmapped'] == 0
    assert result.counter['user.invalid']  == 1
