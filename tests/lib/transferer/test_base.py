# -*- coding: utf-8 -*-

import contextlib
import pytest
import os
from lib.transferer.base      import TransfererBase
from lib.transferer.exception import NoS3Files
from mock                     import patch

ENV = 'dev'
SID = '9999'
PID = 'ac'
DATATYPE    = 'segmentdata'
S3_BUCKET   = 'dev-transfer-data'
S3_PATH_RAW = '9999/segment_list_data/raw/segmentdata.json.gz'
S3_PATH_MST = '9999/segment_list_mst/raw'

@pytest.fixture
def data_dir(tmp_transfer_dir):
    return tmp_transfer_dir.mkdir(SID).mkdir(PID).mkdir(DATATYPE)

def _target():
    return TransfererBase(SID, PID, ENV, DATATYPE, S3_BUCKET, S3_PATH_RAW)

@contextlib.contextmanager
def _patches_with_default_action(func_init_config, data_dir):
    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('lib.s3util.S3Util.download_single_file'),
        patch('lib.s3util.S3Util.download_single_file_order_by_filename')
    ) as (m_config, m_s3get, m_s3get_latest):
        yield (m_s3get, m_s3get_latest)

def test_download_s3file(func_init_config, data_dir):
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_s3get_latest = mocks
        m_s3get.return_value = {
                                    'key'           : '/path/to/target_file.json.gz',
                                    'is_dir'        : False,
                                    'is_file'       : True,
                                    'size'          : 100,
                                    'last_modified' : '2013-10-01%00:00:00.000Z'
                               }

        result = _target()._download_s3file()
        assert result == (
            os.path.join(str(data_dir), 'target_file.json.gz'),
            'target_file.json.gz'
        )

def test_download_s3file_not_exits(func_init_config, data_dir):
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_s3get_latest = mocks
        m_s3get.return_value = None
        with pytest.raises(NoS3Files):
            result = _target()._download_s3file()

def test_download_latest_s3file(func_init_config, data_dir):
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_s3get_latest = mocks
        m_s3get_latest.return_value = {
                                    'key'           : '/path/to/target_file.json.gz',
                                    'is_dir'        : False,
                                    'is_file'       : True,
                                    'size'          : 100,
                                    'last_modified' : '2013-10-01%00:00:00.000Z'
                               }

        result = _target()._download_latest_s3file(S3_PATH_MST)
        assert result == (
            os.path.join(str(data_dir), 'target_file.json.gz'),
            'target_file.json.gz'
        )
