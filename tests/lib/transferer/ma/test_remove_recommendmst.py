# -*- coding: utf-8 -*-

from lib.transferer.ma.remove_recommendmst import RemoveRecommendMstTransferer

import contextlib
import json
import gzip
import os
from   mock                     import Mock
from   mock                     import patch
import pytest
from   lib.transferer.exception import NoS3Files


ENV        = 'dev'
SID        = '9999'
PID        = 'ma'
S3BUCKET   = 's3bucket'
S3FILE     = 's3path/recommendmst/raw/recommendmst_20130103.csv.gz'
S3DIR      = 's3path/recommendmst/raw'
S3DIR_CONV = 's3path/recommendmst/converted_remove/ma'

CSV_HEADER = '"pid","sid","id","name","description"'
CSV_DATA1  = '"rtoaster","9999","100","name100","note100"'
CSV_DATA2  = '"rtoaster","9999","200","name200","note200"'
CSV_DATA3  = '"rtoaster","9999","300","name300","note300"'
CSV_DATA4  = '"rtoaster","9999","400","name400","note400"'
CSV_DATA5  = '"rtoaster","9999","500","name500","note500"'

@pytest.fixture
def data_dir(tmp_transfer_dir):
    return tmp_transfer_dir.mkdir(SID).mkdir(PID).mkdir('remove_recommendmst')

@contextlib.contextmanager
def _patches(func_init_config):
    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('lib.s3util.S3Util.list_dir'),
        patch('lib.s3util.S3Util.download_single_file'),
        patch('requests.post'),
        patch('lib.s3util.S3Util.upload_single_file'),
    ) as (m_config, m_s3ls, m_s3get, m_requests, m_s3put):
        m_s3ls.return_value = [
            {'key': 's3path/recommendmst/raw/recommendmst_20130101.csv.gz'},
            {'key': 's3path/recommendmst/raw/recommendmst_20130102.csv.gz'},
            {'key': 's3path/recommendmst/raw/recommendmst_20130103.csv.gz'}
        ]
        m_requests.return_value = _create_mock_response(200, 1)

        yield (m_s3ls, m_s3get, m_requests, m_s3put)

def _init():
    return RemoveRecommendMstTransferer(SID, PID, ENV, S3BUCKET, S3FILE)

def _create_gzip(file, lines):
    with gzip.open(file, 'wb') as f:
        f.write('\n'.join(lines))

def _create_mock_response(status_code, update_count):
    response             = Mock()
    response.status_code = status_code
    response.text        = json.dumps({'rpl_count': update_count})
    return response


def test_download_s3files(tmp_transfer_dir, data_dir, func_init_config):
    with _patches(func_init_config) as (m_s3ls, m_s3get, m_requests, m_s3put):
        result = _init()._download_s3files()
        assert result == (
                   os.path.join(str(data_dir), 'recommendmst_20130102.csv.gz'),
                   'recommendmst_20130102.csv.gz',
                   os.path.join(str(data_dir), 'recommendmst_20130103.csv.gz'),
                   'recommendmst_20130103.csv.gz'
               )

def test__download_s3files_s3file_not_exist(tmp_transfer_dir, data_dir, func_init_config):
    with _patches(func_init_config) as (m_s3ls, m_s3get, m_requests, m_s3put):
        m_s3ls.return_value = []
        with pytest.raises(NoS3Files):
            _init()._download_s3files()

def test_create_request_datas():
    result = _init()._create_request_datas(['1', '2', '3'])
    expected_result = [
        {
            'app_key'       : 'brainpad',
            'login_id'      : 'login_id_9999',
            'password'      : 'password_9999',
            'advertiser_id' : 'advertiser_id_9999',
            'rpl_id_1'      : '1'
        },
        {
            'app_key'       : 'brainpad',
            'login_id'      : 'login_id_9999',
            'password'      : 'password_9999',
            'advertiser_id' : 'advertiser_id_9999',
            'rpl_id_1'      : '2'
        },
        {
            'app_key'       : 'brainpad',
            'login_id'      : 'login_id_9999',
            'password'      : 'password_9999',
            'advertiser_id' : 'advertiser_id_9999',
            'rpl_id_1'      : '3'
        }
    ]
    assert result == expected_result

def test_execute(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    old_data_file = data_dir.join('recommendmst_20130102.csv.gz')
    new_data_file = data_dir.join('recommendmst_20130103.csv.gz')
    _create_gzip(str(old_data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2, CSV_DATA3])
    _create_gzip(str(new_data_file), [CSV_HEADER, CSV_DATA1])
    converted_file = data_dir.join('recommendmst_20130103_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3ls, m_s3get, m_requests, m_s3put):
        result = _init().execute()

    # assert
    assert result.is_ok()
    assert result.counter['recommend.request']  == 2
    assert result.counter['recommend.response'] == 2
    assert result.counter['request']            == 2
    assert result.counter['request.200']        == 2
    assert old_data_file.check(exists=0)
    assert new_data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3ls.call_count == 1
    assert m_s3ls.call_args_list[0][0] == (S3BUCKET, S3DIR)

    assert m_s3get.call_count == 2
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, 's3path/recommendmst/raw/recommendmst_20130102.csv.gz', str(data_dir))
    assert m_s3get.call_args_list[1][0] == (S3BUCKET, 's3path/recommendmst/raw/recommendmst_20130103.csv.gz', str(data_dir))

    assert m_requests.call_count == 2
    assert m_requests.call_args_list[0][0] == ('http://localhost:5000/recommendlist/remove_bulk',)
    assert m_requests.call_args_list[0][1]['data'] == {
               'app_key'       : 'brainpad',
               'login_id'      : 'login_id_9999',
               'password'      : 'password_9999',
               'advertiser_id' : 'advertiser_id_9999',
               'rpl_id_1'      : '200',
           }
    assert m_requests.call_args_list[1][0] == ('http://localhost:5000/recommendlist/remove_bulk',)
    assert m_requests.call_args_list[1][1]['data'] == {
               'app_key'       : 'brainpad',
               'login_id'      : 'login_id_9999',
               'password'      : 'password_9999',
               'advertiser_id' : 'advertiser_id_9999',
               'rpl_id_1'      : '300',
           }

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3BUCKET, S3DIR_CONV)

def test_execute_s3file_notfound(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # execute
    with _patches(func_init_config) as (m_s3ls, m_s3get, m_requests, m_s3put):
        m_s3ls.return_value = []
        result = _init().execute()

    # assert
    assert result.is_ng()
    assert result.counter['recommend.request']  == 0
    assert result.counter['recommend.response'] == 0
    assert result.counter['request']            == 0
    assert result.counter['request.200']        == 0

    assert m_s3ls.call_count == 1
    assert m_s3ls.call_args_list[0][0] == (S3BUCKET, S3DIR)

    assert m_s3get.call_count       == 0
    assert m_requests.call_count    == 0
    assert m_s3put.call_count       == 0

def test_execute_no_removed_recommend(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    old_data_file = data_dir.join('recommendmst_20130102.csv.gz')
    new_data_file = data_dir.join('recommendmst_20130103.csv.gz')
    _create_gzip(str(old_data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2, CSV_DATA3])
    _create_gzip(str(new_data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2, CSV_DATA3])
    converted_file = data_dir.join('recommendmst_20130103_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3ls, m_s3get, m_requests, m_s3put):
        result = _init().execute()

    # assert
    assert result.is_ok()
    assert result.counter['recommend.request']  == 0
    assert result.counter['recommend.response'] == 0
    assert result.counter['request']            == 0
    assert result.counter['request.200']        == 0
    assert old_data_file.check(exists=0)
    assert new_data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3ls.call_count == 1
    assert m_s3ls.call_args_list[0][0] == (S3BUCKET, S3DIR)

    assert m_s3get.call_count == 2
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, 's3path/recommendmst/raw/recommendmst_20130102.csv.gz', str(data_dir))
    assert m_s3get.call_args_list[1][0] == (S3BUCKET, 's3path/recommendmst/raw/recommendmst_20130103.csv.gz', str(data_dir))

    assert m_requests.call_count == 0
    assert m_s3put.call_count    == 0

def test_execute_empty_old_file(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    old_data_file = data_dir.join('recommendmst_20130102.csv.gz')
    new_data_file = data_dir.join('recommendmst_20130103.csv.gz')
    _create_gzip(str(old_data_file), [])
    _create_gzip(str(new_data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2, CSV_DATA3])
    converted_file = data_dir.join('recommendmst_20130103_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3ls, m_s3get, m_requests, m_s3put):
        result = _init().execute()

    # assert
    assert result.is_ok()
    assert result.counter['recommend.request']  == 0
    assert result.counter['recommend.response'] == 0
    assert result.counter['request']            == 0
    assert result.counter['request.200']        == 0
    assert old_data_file.check(exists=0)
    assert new_data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3ls.call_count == 1
    assert m_s3ls.call_args_list[0][0] == (S3BUCKET, S3DIR)

    assert m_s3get.call_count == 2
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, 's3path/recommendmst/raw/recommendmst_20130102.csv.gz', str(data_dir))
    assert m_s3get.call_args_list[1][0] == (S3BUCKET, 's3path/recommendmst/raw/recommendmst_20130103.csv.gz', str(data_dir))

    assert m_requests.call_count == 0
    assert m_s3put.call_count    == 0

def test_execute_empty_new_file(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    old_data_file = data_dir.join('recommendmst_20130102.csv.gz')
    new_data_file = data_dir.join('recommendmst_20130103.csv.gz')
    _create_gzip(str(old_data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2])
    _create_gzip(str(new_data_file), [])
    converted_file = data_dir.join('recommendmst_20130103_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3ls, m_s3get, m_requests, m_s3put):
        result = _init().execute()

    # assert
    assert result.is_ok()
    assert result.counter['recommend.request']  == 2
    assert result.counter['recommend.response'] == 2
    assert result.counter['request']            == 2
    assert result.counter['request.200']        == 2
    assert old_data_file.check(exists=0)
    assert new_data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3ls.call_count == 1
    assert m_s3ls.call_args_list[0][0] == (S3BUCKET, S3DIR)

    assert m_s3get.call_count == 2
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, 's3path/recommendmst/raw/recommendmst_20130102.csv.gz', str(data_dir))
    assert m_s3get.call_args_list[1][0] == (S3BUCKET, 's3path/recommendmst/raw/recommendmst_20130103.csv.gz', str(data_dir))

    assert m_requests.call_count == 2
    assert m_requests.call_args_list[0][0] == ('http://localhost:5000/recommendlist/remove_bulk',)
    assert m_requests.call_args_list[0][1]['data'] == {
               'app_key'       : 'brainpad',
               'login_id'      : 'login_id_9999',
               'password'      : 'password_9999',
               'advertiser_id' : 'advertiser_id_9999',
               'rpl_id_1'      : '100',
           }
    assert m_requests.call_args_list[1][0] == ('http://localhost:5000/recommendlist/remove_bulk',)
    assert m_requests.call_args_list[1][1]['data'] == {
               'app_key'       : 'brainpad',
               'login_id'      : 'login_id_9999',
               'password'      : 'password_9999',
               'advertiser_id' : 'advertiser_id_9999',
               'rpl_id_1'      : '200',
           }

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3BUCKET, S3DIR_CONV)

def test_execute_api_error(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    old_data_file = data_dir.join('recommendmst_20130102.csv.gz')
    new_data_file = data_dir.join('recommendmst_20130103.csv.gz')
    _create_gzip(str(old_data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2, CSV_DATA3])
    _create_gzip(str(new_data_file), [CSV_HEADER, CSV_DATA1])
    converted_file = data_dir.join('recommendmst_20130103_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3ls, m_s3get, m_requests, m_s3put):
        m_requests.return_value = _create_mock_response(500, 2)
        result = _init().execute()

    # assert
    assert result.is_api_error()
    assert result.counter['recommend.request']  == 2
    assert result.counter['recommend.response'] == 0
    assert result.counter['request']            == 2
    assert result.counter['request.200']        == 0
    assert result.counter['request.500']        == 2
    assert old_data_file.check(exists=0)
    assert new_data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3ls.call_count == 1
    assert m_s3ls.call_args_list[0][0] == (S3BUCKET, S3DIR)

    assert m_s3get.call_count == 2
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, 's3path/recommendmst/raw/recommendmst_20130102.csv.gz', str(data_dir))
    assert m_s3get.call_args_list[1][0] == (S3BUCKET, 's3path/recommendmst/raw/recommendmst_20130103.csv.gz', str(data_dir))

    assert m_requests.call_count == 2
    assert m_requests.call_args_list[0][0] == ('http://localhost:5000/recommendlist/remove_bulk',)
    assert m_requests.call_args_list[0][1]['data'] == {
               'app_key'       : 'brainpad',
               'login_id'      : 'login_id_9999',
               'password'      : 'password_9999',
               'advertiser_id' : 'advertiser_id_9999',
               'rpl_id_1'      : '200',
           }
    assert m_requests.call_args_list[1][0] == ('http://localhost:5000/recommendlist/remove_bulk',)
    assert m_requests.call_args_list[1][1]['data'] == {
               'app_key'       : 'brainpad',
               'login_id'      : 'login_id_9999',
               'password'      : 'password_9999',
               'advertiser_id' : 'advertiser_id_9999',
               'rpl_id_1'      : '300',
           }

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3BUCKET, S3DIR_CONV)

def test_execute_update_count_error(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    old_data_file = data_dir.join('recommendmst_20130102.csv.gz')
    new_data_file = data_dir.join('recommendmst_20130103.csv.gz')
    _create_gzip(str(old_data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2, CSV_DATA3])
    _create_gzip(str(new_data_file), [CSV_HEADER, CSV_DATA1])
    converted_file = data_dir.join('recommendmst_20130103_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3ls, m_s3get, m_requests, m_s3put):
        m_requests.return_value = _create_mock_response(200, 0)
        result = _init().execute()

    # assert
    assert result.is_api_error()
    assert result.counter['recommend.request']  == 2
    assert result.counter['recommend.response'] == 0
    assert result.counter['request']            == 2
    assert result.counter['request.200']        == 2
    assert old_data_file.check(exists=0)
    assert new_data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3ls.call_count == 1
    assert m_s3ls.call_args_list[0][0] == (S3BUCKET, S3DIR)

    assert m_s3get.call_count == 2
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, 's3path/recommendmst/raw/recommendmst_20130102.csv.gz', str(data_dir))
    assert m_s3get.call_args_list[1][0] == (S3BUCKET, 's3path/recommendmst/raw/recommendmst_20130103.csv.gz', str(data_dir))

    assert m_requests.call_count == 2
    assert m_requests.call_args_list[0][0] == ('http://localhost:5000/recommendlist/remove_bulk',)
    assert m_requests.call_args_list[0][1]['data'] == {
               'app_key'       : 'brainpad',
               'login_id'      : 'login_id_9999',
               'password'      : 'password_9999',
               'advertiser_id' : 'advertiser_id_9999',
               'rpl_id_1'      : '200',
           }
    assert m_requests.call_args_list[1][0] == ('http://localhost:5000/recommendlist/remove_bulk',)
    assert m_requests.call_args_list[1][1]['data'] == {
               'app_key'       : 'brainpad',
               'login_id'      : 'login_id_9999',
               'password'      : 'password_9999',
               'advertiser_id' : 'advertiser_id_9999',
               'rpl_id_1'      : '300',
           }

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3BUCKET, S3DIR_CONV)

def test_execute_unexpected_error(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    old_data_file = data_dir.join('recommendmst_20130102.csv.gz')
    new_data_file = data_dir.join('recommendmst_20130103.csv.gz')
    _create_gzip(str(old_data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2, CSV_DATA3])
    _create_gzip(str(new_data_file), [CSV_HEADER, CSV_DATA1])
    converted_file = data_dir.join('recommendmst_20130103_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3ls, m_s3get, m_requests, m_s3put):
        m_requests.side_effect = Exception('Failed to request')
        result = _init().execute()

    # assert
    assert result.is_ng()
    assert result.counter['recommend.request']  == 1
    assert result.counter['recommend.response'] == 0
    assert result.counter['request']            == 1
    assert result.counter['request.200']        == 0
    assert old_data_file.check(exists=0)
    assert new_data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3ls.call_count == 1
    assert m_s3ls.call_args_list[0][0] == (S3BUCKET, S3DIR)

    assert m_s3get.call_count == 2
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, 's3path/recommendmst/raw/recommendmst_20130102.csv.gz', str(data_dir))
    assert m_s3get.call_args_list[1][0] == (S3BUCKET, 's3path/recommendmst/raw/recommendmst_20130103.csv.gz', str(data_dir))

    assert m_requests.call_count == 1
    assert m_requests.call_args_list[0][0] == ('http://localhost:5000/recommendlist/remove_bulk',)
    assert m_requests.call_args_list[0][1]['data'] == {
               'app_key'       : 'brainpad',
               'login_id'      : 'login_id_9999',
               'password'      : 'password_9999',
               'advertiser_id' : 'advertiser_id_9999',
               'rpl_id_1'      : '200',
           }

    assert m_s3put.call_count == 0
