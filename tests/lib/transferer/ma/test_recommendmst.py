# -*- coding: utf-8 -*-

from lib.transferer.ma.recommendmst import RecommendMstTransferer

import contextlib
import gzip
import json
import os
from   mock                     import Mock
from   mock                     import patch
import pytest
from   lib.transferer.exception import NoS3Files


ENV        = 'dev'
SID        = '9999'
PID        = 'ma'
S3BUCKET   = 's3bucket'
S3FILE     = 's3path/recommendmst/raw/recommendmst.csv.gz'
S3DIR_CONV = 's3path/recommendmst/converted/ma'

CSV_HEADER = '"pid","sid","id","name","description"'
CSV_DATA1  = '"rtoaster","9999","100","name100","note100"'
CSV_DATA2  = '"rtoaster","9999","200","name200","note200"'
CSV_DATA3  = '"rtoaster","9999","300","name300","note300"'

@pytest.fixture
def data_dir(tmp_transfer_dir):
    return tmp_transfer_dir.mkdir(SID).mkdir(PID).mkdir('recommendmst')

@contextlib.contextmanager
def _patches(func_init_config):
    with contextlib.nested(
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('lib.s3util.S3Util.download_single_file'),
        patch('requests.post'),
        patch('lib.s3util.S3Util.upload_single_file'),
    ) as (m_config, m_s3get, m_requests, m_s3put):
        m_s3get.return_value    = {'key': 's3path/recommendmst/recommendmst.csv.gz'}
        m_requests.return_value = _create_mock_response(200, 1)

        yield (m_s3get, m_requests, m_s3put)

def _init():
    return RecommendMstTransferer(SID, PID, ENV, S3BUCKET, S3FILE)

def _create_gzip(file, lines):
    with gzip.open(file, 'wb') as f:
        f.write('\n'.join(lines))

def _create_mock_response(status_code, update_count):
    response             = Mock()
    response.status_code = status_code
    response.text        = json.dumps({'rpl_count': update_count})
    return response


def test_download_s3file(tmp_transfer_dir, data_dir, func_init_config):
    with _patches(func_init_config) as (m_s3get, m_requests, m_s3put):
        m_s3get.return_value = {
            'key'           : 'path/to/target_file.csv.gz',
            'is_dir'        : False,
            'is_file'       : True,
            'size'          : 100,
            'last_modified' : '2013-10-01T00:00:00.000Z'
        }

        result = _init()._download_s3file()
        assert result == (
                   os.path.join(str(data_dir), 'target_file.csv.gz'),
                   'target_file.csv.gz'
               )

def test_download_s3file_s3file_not_exist(tmp_transfer_dir, data_dir, func_init_config):
    with _patches(func_init_config) as (m_s3get, m_requests, m_s3put):
        m_s3get.return_value = None

        with pytest.raises(NoS3Files):
            _init()._download_s3file()

def test_create_request_datas(data_dir):
    data_file = data_dir.join('target_file.csv.gz')
    test_recs = [
        CSV_HEADER,
        '"rtoaster","9999","1","レコメンド名_1","レコメンド概要_1"',
        '"rtoaster","9999","2","レコメンド名_2","レコメンド概要_2"',
        '"rtoaster","9999","3","レコメンド名_3","レコメンド概要_3"'
    ]
    _create_gzip(str(data_file), test_recs)

    result = _init()._create_request_datas(str(data_file))
    expected_result = [
        {
            'app_key'       : 'brainpad',
            'login_id'      : 'login_id_9999',
            'password'      : 'password_9999',
            'advertiser_id' : 'advertiser_id_9999',
            'rpl_id_1'      : '1',
            'rpl_name_1'    : 'レコメンド名_1',
            'rpl_note_1'    : 'レコメンド概要_1'
        },
        {
            'app_key'       : 'brainpad',
            'login_id'      : 'login_id_9999',
            'password'      : 'password_9999',
            'advertiser_id' : 'advertiser_id_9999',
            'rpl_id_1'      : '2',
            'rpl_name_1'    : 'レコメンド名_2',
            'rpl_note_1'    : 'レコメンド概要_2'
        },
        {
            'app_key'       : 'brainpad',
            'login_id'      : 'login_id_9999',
            'password'      : 'password_9999',
            'advertiser_id' : 'advertiser_id_9999',
            'rpl_id_1'      : '3',
            'rpl_name_1'    : 'レコメンド名_3',
            'rpl_note_1'    : 'レコメンド概要_3'
        }
    ]
    assert result == expected_result

def test_execute(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    data_file = data_dir.join('recommendmst.csv.gz')
    _create_gzip(str(data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2, CSV_DATA3])
    converted_file = data_dir.join('recommendmst_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3get, m_requests, m_s3put):
        result = _init().execute()

    # assert
    assert result.is_ok()
    assert result.counter['recommend.request']  == 3
    assert result.counter['recommend.response'] == 3
    assert result.counter['request']            == 3
    assert result.counter['request.200']        == 3
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))

    assert m_requests.call_count == 3
    assert m_requests.call_args_list[0][0] == ('http://localhost:5000/recommendlist', )
    assert m_requests.call_args_list[0][1]['data'] == {
               'app_key'       : 'brainpad',
               'login_id'      : 'login_id_9999',
               'password'      : 'password_9999',
               'advertiser_id' : 'advertiser_id_9999',
               'rpl_id_1'      : '100',
               'rpl_name_1'    : 'name100',
               'rpl_note_1'    : 'note100',
           }
    assert m_requests.call_args_list[1][0] == ('http://localhost:5000/recommendlist', )
    assert m_requests.call_args_list[1][1]['data'] == {
               'app_key'       : 'brainpad',
               'login_id'      : 'login_id_9999',
               'password'      : 'password_9999',
               'advertiser_id' : 'advertiser_id_9999',
               'rpl_id_1'      : '200',
               'rpl_name_1'    : 'name200',
               'rpl_note_1'    : 'note200',
           }
    assert m_requests.call_args_list[2][0] == ('http://localhost:5000/recommendlist', )
    assert m_requests.call_args_list[2][1]['data'] == {
               'app_key'       : 'brainpad',
               'login_id'      : 'login_id_9999',
               'password'      : 'password_9999',
               'advertiser_id' : 'advertiser_id_9999',
               'rpl_id_1'      : '300',
               'rpl_name_1'    : 'name300',
               'rpl_note_1'    : 'note300',
           }

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3BUCKET, S3DIR_CONV)

def test_execute_s3file_notfound(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # execute
    with _patches(func_init_config) as (m_s3get, m_requests, m_s3put):
        m_s3get.return_value = None
        result = _init().execute()

    # assert
    assert result.is_ng()
    assert not result.counter

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))

    assert m_requests.call_count    == 0
    assert m_s3put.call_count       == 0

def test_execute_no_csv_header(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    data_file = data_dir.join('recommendmst.csv.gz')
    _create_gzip(str(data_file), [CSV_DATA1, CSV_DATA2, CSV_DATA3])
    converted_file = data_dir.join('recommendmst_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3get, m_requests, m_s3put):
        result = _init().execute()

    # assert
    assert result.is_ng()
    assert not result.counter
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))

    assert m_requests.call_count == 0
    assert m_s3put.call_count    == 0

def test_execute_invalid_csv_header(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    data_file = data_dir.join('recommendmst.csv.gz')
    _create_gzip(str(data_file), ['foo,bar', CSV_DATA1, CSV_DATA2, CSV_DATA3])
    converted_file = data_dir.join('recommendmst_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3get, m_requests, m_s3put):
        result = _init().execute()

    # assert
    assert result.is_ng()
    assert not result.counter
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))

    assert m_requests.call_count == 0
    assert m_s3put.call_count    == 0

def test_execte_not_csv_record(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    data_file = data_dir.join('recommendmst.csv.gz')
    _create_gzip(str(data_file), [CSV_HEADER])
    converted_file = data_dir.join('recommendmst_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3get, m_requests, m_s3put):
        result = _init().execute()

    # assert
    assert result.is_ok()
    assert not result.counter
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))

    assert m_requests.call_count == 0

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3BUCKET, S3DIR_CONV)

def test_execute_invalid_csv_record(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    data_file = data_dir.join('recommendmst.csv.gz')
    _create_gzip(str(data_file), [
                                      CSV_HEADER,
                                      CSV_DATA1,
                                      '"rtoaster","9999","200","name200"',
                                      '',
                                      '"rtoaster","9999","300","name300","note300","other300"'
                                 ])
    converted_file = data_dir.join('recommendmst_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3get, m_requests, m_s3put):
        result = _init().execute()

    # assert
    assert result.is_ok()
    assert result.counter['recommend.request']  == 3
    assert result.counter['recommend.response'] == 3
    assert result.counter['request']            == 3
    assert result.counter['request.200']        == 3
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))

    assert m_requests.call_count == 3
    assert m_requests.call_args_list[0][0] == ('http://localhost:5000/recommendlist', )
    assert m_requests.call_args_list[0][1]['data'] == {
               'app_key'       : 'brainpad',
               'login_id'      : 'login_id_9999',
               'password'      : 'password_9999',
               'advertiser_id' : 'advertiser_id_9999',
               'rpl_id_1'      : '100',
               'rpl_name_1'    : 'name100',
               'rpl_note_1'    : 'note100',
           }
    assert m_requests.call_args_list[1][0] == ('http://localhost:5000/recommendlist', )
    assert m_requests.call_args_list[1][1]['data'] == {
               'app_key'       : 'brainpad',
               'login_id'      : 'login_id_9999',
               'password'      : 'password_9999',
               'advertiser_id' : 'advertiser_id_9999',
               'rpl_id_1'      : '200',
               'rpl_name_1'    : 'name200',
               'rpl_note_1'    : None,
           }
    assert m_requests.call_args_list[2][0] == ('http://localhost:5000/recommendlist', )
    assert m_requests.call_args_list[2][1]['data'] == {
               'app_key'       : 'brainpad',
               'login_id'      : 'login_id_9999',
               'password'      : 'password_9999',
               'advertiser_id' : 'advertiser_id_9999',
               'rpl_id_1'      : '300',
               'rpl_name_1'    : 'name300',
               'rpl_note_1'    : 'note300',
           }

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3BUCKET, S3DIR_CONV)

def test_execute_empty_csv_file(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    data_file = data_dir.join('recommendmst.csv.gz')
    _create_gzip(str(data_file), [])
    converted_file = data_dir.join('recommendmst_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3get, m_requests, m_s3put):
        result = _init().execute()

    # assert
    assert result.is_ok()
    assert not result.counter
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))

    assert m_requests.call_count == 0

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3BUCKET, S3DIR_CONV)

def test_execute_api_error(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    data_file = data_dir.join('recommendmst.csv.gz')
    _create_gzip(str(data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2, CSV_DATA3])
    converted_file = data_dir.join('recommendmst_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3get, m_requests, m_s3put):
        m_requests.return_value = _create_mock_response(500, 0)
        result = _init().execute()

    # assert
    assert result.is_api_error()
    assert result.counter['recommend.request']  == 3
    assert result.counter['recommend.response'] == 0
    assert result.counter['request']            == 3
    assert result.counter['request.200']        == 0
    assert result.counter['request.500']        == 3
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))

    assert m_requests.call_count == 3
    assert m_requests.call_args_list[0][0] == ('http://localhost:5000/recommendlist', )
    assert m_requests.call_args_list[0][1]['data'] == {
               'app_key'       : 'brainpad',
               'login_id'      : 'login_id_9999',
               'password'      : 'password_9999',
               'advertiser_id' : 'advertiser_id_9999',
               'rpl_id_1'      : '100',
               'rpl_name_1'    : 'name100',
               'rpl_note_1'    : 'note100',
           }
    assert m_requests.call_args_list[1][0] == ('http://localhost:5000/recommendlist', )
    assert m_requests.call_args_list[1][1]['data'] == {
               'app_key'       : 'brainpad',
               'login_id'      : 'login_id_9999',
               'password'      : 'password_9999',
               'advertiser_id' : 'advertiser_id_9999',
               'rpl_id_1'      : '200',
               'rpl_name_1'    : 'name200',
               'rpl_note_1'    : 'note200',
           }
    assert m_requests.call_args_list[2][0] == ('http://localhost:5000/recommendlist', )
    assert m_requests.call_args_list[2][1]['data'] == {
               'app_key'       : 'brainpad',
               'login_id'      : 'login_id_9999',
               'password'      : 'password_9999',
               'advertiser_id' : 'advertiser_id_9999',
               'rpl_id_1'      : '300',
               'rpl_name_1'    : 'name300',
               'rpl_note_1'    : 'note300',
           }

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3BUCKET, S3DIR_CONV)

def test_execute_update_count_error(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    data_file = data_dir.join('recommendmst.csv.gz')
    _create_gzip(str(data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2, CSV_DATA3])
    converted_file = data_dir.join('recommendmst_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3get, m_requests, m_s3put):
        m_requests.return_value = _create_mock_response(200, 0)
        result = _init().execute()

    # assert
    assert result.is_api_error()
    assert result.counter['recommend.request']  == 3
    assert result.counter['recommend.response'] == 0
    assert result.counter['request']            == 3
    assert result.counter['request.200']        == 3
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))

    assert m_requests.call_count == 3
    assert m_requests.call_args_list[0][0] == ('http://localhost:5000/recommendlist', )
    assert m_requests.call_args_list[0][1]['data'] == {
               'app_key'       : 'brainpad',
               'login_id'      : 'login_id_9999',
               'password'      : 'password_9999',
               'advertiser_id' : 'advertiser_id_9999',
               'rpl_id_1'      : '100',
               'rpl_name_1'    : 'name100',
               'rpl_note_1'    : 'note100',
           }
    assert m_requests.call_args_list[1][0] == ('http://localhost:5000/recommendlist', )
    assert m_requests.call_args_list[1][1]['data'] == {
               'app_key'       : 'brainpad',
               'login_id'      : 'login_id_9999',
               'password'      : 'password_9999',
               'advertiser_id' : 'advertiser_id_9999',
               'rpl_id_1'      : '200',
               'rpl_name_1'    : 'name200',
               'rpl_note_1'    : 'note200',
           }
    assert m_requests.call_args_list[2][0] == ('http://localhost:5000/recommendlist', )
    assert m_requests.call_args_list[2][1]['data'] == {
               'app_key'       : 'brainpad',
               'login_id'      : 'login_id_9999',
               'password'      : 'password_9999',
               'advertiser_id' : 'advertiser_id_9999',
               'rpl_id_1'      : '300',
               'rpl_name_1'    : 'name300',
               'rpl_note_1'    : 'note300',
           }

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3BUCKET, S3DIR_CONV)

def test_execute_unexpected_error(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    data_file = data_dir.join('recommendmst.csv.gz')
    _create_gzip(str(data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2, CSV_DATA3])
    converted_file = data_dir.join('recommendmst_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3get, m_requests, m_s3put):
        m_requests.side_effect = Exception('Failed to request')
        result = _init().execute()

    # assert
    assert result.is_ng()
    assert result.counter['recommend.request']  == 1
    assert result.counter['recommend.response'] == 0
    assert result.counter['request']            == 1
    assert result.counter['request.200']        == 0
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))

    assert m_requests.call_count == 1
    assert m_requests.call_args_list[0][0] == ('http://localhost:5000/recommendlist', )
    assert m_requests.call_args_list[0][1]['data'] == {
               'app_key'       : 'brainpad',
               'login_id'      : 'login_id_9999',
               'password'      : 'password_9999',
               'advertiser_id' : 'advertiser_id_9999',
               'rpl_id_1'      : '100',
               'rpl_name_1'    : 'name100',
               'rpl_note_1'    : 'note100',
           }

    assert m_s3put.call_count == 0
