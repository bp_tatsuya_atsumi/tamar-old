# -*- coding: utf-8 -*-

from lib.transferer.ma.segmentmst import SegmentMstTransferer

import contextlib
import gzip
import json
import os
from   mock                     import Mock
from   mock                     import patch
import pytest
from   lib.transferer.exception import NoS3Files


ENV        = 'dev'
SID        = '9999'
PID        = 'ma'
S3BUCKET   = 's3_bucket'
S3FILE     = 's3_path/segmentmst/raw/segmentmst.csv.gz'
S3DIR_CONV = 's3_path/segmentmst/converted/ma'

CSV_HEADER = 'id,name,description,ignore'
CSV_DATA1  = '100,name100,note100,"ignore_partner"'
CSV_DATA2  = '200,name200,note200,"ignore_partner"'
CSV_DATA3  = '300,name300,note300,"ignore_partner"'

@pytest.fixture
def data_dir(tmp_transfer_dir):
    return tmp_transfer_dir.mkdir(SID).mkdir(PID).mkdir('segmentmst')

@contextlib.contextmanager
def _patches(func_init_config):
    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('lib.s3util.S3Util.download_single_file'),
        patch('requests.post'),
        patch('lib.s3util.S3Util.upload_single_file'),
    ) as (m_config, m_s3get, m_requests, m_s3put):
        m_s3get.return_value    = {'key': S3FILE}
        m_requests.return_value = _create_mock_response(200, 3)

        yield (m_s3get, m_requests, m_s3put)

def _init():
    return SegmentMstTransferer(SID, PID, ENV, S3BUCKET, S3FILE)

def _create_gzip(file, lines):
    with gzip.open(file, 'wb') as f:
        f.write('\n'.join(lines))

def _create_mock_response(status_code, update_count):
    response = Mock()
    response.status_code = status_code
    response.text = json.dumps({'segment_count': update_count})
    return response


def test_download_s3file(tmp_transfer_dir, data_dir, func_init_config):
    with _patches(func_init_config) as (m_s3get, m_requests, m_s3put):
        m_s3get.return_value = {
            'key'           : 'path/to/target_file.csv.gz',
            'is_dir'        : False,
            'is_file'       : True,
            'size'          : 100,
            'last_modified' : '2013-10-01T00:00:00.000Z'
        }

        result = _init()._download_s3file()
        assert result == (
                   os.path.join(str(data_dir), 'target_file.csv.gz'),
                   'target_file.csv.gz'
               )

def test_download_s3file_s3file_not_exist(tmp_transfer_dir, data_dir, func_init_config):
    with _patches(func_init_config) as (m_s3get, m_requests, m_s3put):
        m_s3get.side_effect  = None
        m_s3get.return_value = None
        with pytest.raises(NoS3Files):
            _init()._download_s3file()

def test_create_request_datas(data_dir):
    data_file = data_dir.join('segmentmst.csv.gz')
    _create_gzip(str(data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2, CSV_DATA3])

    result = _init()._create_request_datas(str(data_file))
    expected_result = [{
                        'app_key'       : 'brainpad',
                        'login_id'      : 'login_id_9999',
                        'password'      : 'password_9999',
                        'advertiser_id' : 'advertiser_id_9999',
                        'seg_id_1'      : '100',
                        'seg_name_1'    : 'name100',
                        'seg_note_1'    : 'note100',
                        'seg_id_2'      : '200',
                        'seg_name_2'    : 'name200',
                        'seg_note_2'    : 'note200',
                        'seg_id_3'      : '300',
                        'seg_name_3'    : 'name300',
                        'seg_note_3'    : 'note300'
                      }]

    assert result == expected_result

def test_create_request_datas_over_limit(data_dir):
    data_file = data_dir.join('segmentmst.csv.gz')
    lines = [CSV_HEADER]
    lines.extend([CSV_DATA1 for i in range(250)])
    _create_gzip(str(data_file), lines)

    result = _init()._create_request_datas(str(data_file))

    assert len(result) == 3
    assert len([k for k in result[0].keys() if k.startswith('seg_id')]) == 100
    assert len([k for k in result[1].keys() if k.startswith('seg_id')]) == 100
    assert len([k for k in result[2].keys() if k.startswith('seg_id')]) == 50

def test_execute(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    data_file = data_dir.join('segmentmst.csv.gz')
    _create_gzip(str(data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2, CSV_DATA3])
    converted_file = data_dir.join('segmentmst_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3get, m_requests, m_s3put):
        result = _init().execute()

    # assert
    assert result.is_ok()
    assert result.counter['segment.request']  == 3
    assert result.counter['segment.response'] == 3
    assert result.counter['request']          == 1
    assert result.counter['request.200']      == 1
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))

    assert m_requests.call_count == 1
    assert m_requests.call_args_list[0][0] == ('http://localhost:5000/segmentlist',)
    assert m_requests.call_args_list[0][1]['data'] == {
                'app_key'       : 'brainpad',
                'login_id'      : 'login_id_9999',
                'password'      : 'password_9999',
                'advertiser_id' : 'advertiser_id_9999',
                'seg_id_1'      : '100',
                'seg_name_1'    : 'name100',
                'seg_note_1'    : 'note100',
                'seg_id_2'      : '200',
                'seg_name_2'    : 'name200',
                'seg_note_2'    : 'note200',
                'seg_id_3'      : '300',
                'seg_name_3'    : 'name300',
                'seg_note_3'    : 'note300',
            }

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3BUCKET, S3DIR_CONV)

def test_execute_s3file_notfound(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # execute
    with _patches(func_init_config) as (m_s3get, m_requests, m_s3put):
        m_s3get.return_value = None
        result = _init().execute()

    # assert
    assert result.is_ng()
    assert not result.counter

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))

    assert m_requests.call_count == 0
    assert m_s3put.call_count    == 0

def test_execute_no_csv_header(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    data_file = data_dir.join('segmentmst.csv.gz')
    _create_gzip(str(data_file), [CSV_DATA1, CSV_DATA2, CSV_DATA3])
    converted_file = data_dir.join('segmentmst_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3get, m_requests, m_s3put):
        result = _init().execute()

    # assert
    assert result.is_ng()
    assert not result.counter
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))

    assert m_requests.call_count == 0
    assert m_s3put.call_count    == 0

def test_execute_invalid_csv_header(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    data_file = data_dir.join('segmentmst.csv.gz')
    _create_gzip(str(data_file), ['foo,bar', CSV_DATA1, CSV_DATA2, CSV_DATA3])
    converted_file = data_dir.join('segmentmst_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3get, m_requests, m_s3put):
        result = _init().execute()

    # assert
    assert result.is_ng()
    assert not result.counter
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))

    assert m_requests.call_count == 0
    assert m_s3put.call_count    == 0

def test_execute_no_csv_record(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    data_file = data_dir.join('segmentmst.csv.gz')
    _create_gzip(str(data_file), [CSV_HEADER])
    converted_file = data_dir.join('segmentmst_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3get, m_requests, m_s3put):
        result = _init().execute()

    # assert
    assert result.is_ok()
    assert not result.counter
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))

    assert m_requests.call_count == 0

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3BUCKET, S3DIR_CONV)

def test_execute_invalid_csv_record(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    data_file = data_dir.join('segmentmst.csv.gz')
    _create_gzip(str(data_file), [CSV_HEADER, CSV_DATA1, '200,name200', '', '300,name300,note300,other300'])
    converted_file = data_dir.join('segmentmst_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3get, m_requests, m_s3put):
        result = _init().execute()

    # assert
    assert result.is_ok()
    assert result.counter['segment.request']  == 3
    assert result.counter['segment.response'] == 3
    assert result.counter['request']          == 1
    assert result.counter['request.200']      == 1
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))

    assert m_requests.call_count == 1
    assert m_requests.call_args_list[0][0] == ('http://localhost:5000/segmentlist',)
    assert m_requests.call_args_list[0][1]['data'] == {
                'app_key'       : 'brainpad',
                'login_id'      : 'login_id_9999',
                'password'      : 'password_9999',
                'advertiser_id' : 'advertiser_id_9999',
                'seg_id_1'      : '100',
                'seg_name_1'    : 'name100',
                'seg_note_1'    : 'note100',
                'seg_id_2'      : '200',
                'seg_name_2'    : 'name200',
                'seg_note_2'    : None,
                'seg_id_3'      : '300',
                'seg_name_3'    : 'name300',
                'seg_note_3'    : 'note300',
            }

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3BUCKET, S3DIR_CONV)

def test_execute_empty_csv_file(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    data_file = data_dir.join('segmentmst.csv.gz')
    _create_gzip(str(data_file), [])
    converted_file = data_dir.join('segmentmst_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3get, m_requests, m_s3put):
        result = _init().execute()

    # assert
    assert result.is_ok()
    assert not result.counter
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))

    assert m_requests.call_count == 0

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3BUCKET, S3DIR_CONV)

def test_execute_api_error(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    data_file = data_dir.join('segmentmst.csv.gz')
    _create_gzip(str(data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2, CSV_DATA3])
    converted_file = data_dir.join('segmentmst_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3get, m_requests, m_s3put):
        m_requests.return_value = _create_mock_response(500, 3)
        result = _init().execute()

    # assert
    assert result.is_api_error()
    assert result.counter['segment.request']  == 3
    assert result.counter['segment.response'] == 0
    assert result.counter['request']          == 1
    assert result.counter['request.200']      == 0
    assert result.counter['request.500']      == 1
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))

    assert m_requests.call_count == 1
    assert m_requests.call_args_list[0][0] == ('http://localhost:5000/segmentlist',)
    assert m_requests.call_args_list[0][1]['data'] == {
                'app_key'       : 'brainpad',
                'login_id'      : 'login_id_9999',
                'password'      : 'password_9999',
                'advertiser_id' : 'advertiser_id_9999',
                'seg_id_1'      : '100',
                'seg_name_1'    : 'name100',
                'seg_note_1'    : 'note100',
                'seg_id_2'      : '200',
                'seg_name_2'    : 'name200',
                'seg_note_2'    : 'note200',
                'seg_id_3'      : '300',
                'seg_name_3'    : 'name300',
                'seg_note_3'    : 'note300',
            }

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3BUCKET, S3DIR_CONV)

def test_execute_update_count_error(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    data_file = data_dir.join('segmentmst.csv.gz')
    _create_gzip(str(data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2, CSV_DATA3])
    converted_file = data_dir.join('segmentmst_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3get, m_requests, m_s3put):
        m_requests.return_value = _create_mock_response(200, 2)
        result = _init().execute()

    # assert
    assert result.is_api_error()
    assert result.counter['segment.request']  == 3
    assert result.counter['segment.response'] == 2
    assert result.counter['request']          == 1
    assert result.counter['request.200']      == 1
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))

    assert m_requests.call_count == 1
    assert m_requests.call_args_list[0][0] == ('http://localhost:5000/segmentlist',)
    assert m_requests.call_args_list[0][1]['data'] == {
                'app_key'       : 'brainpad',
                'login_id'      : 'login_id_9999',
                'password'      : 'password_9999',
                'advertiser_id' : 'advertiser_id_9999',
                'seg_id_1'      : '100',
                'seg_name_1'    : 'name100',
                'seg_note_1'    : 'note100',
                'seg_id_2'      : '200',
                'seg_name_2'    : 'name200',
                'seg_note_2'    : 'note200',
                'seg_id_3'      : '300',
                'seg_name_3'    : 'name300',
                'seg_note_3'    : 'note300',
            }

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3BUCKET, S3DIR_CONV)

def test_execute_unexpected_error(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    data_file = data_dir.join('segmentmst.csv.gz')
    _create_gzip(str(data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2, CSV_DATA3])
    converted_file = data_dir.join('segmentmst_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3get, m_requests, m_s3put):
        m_requests.side_effect = Exception('Failed to request')
        result = _init().execute()

    # assert
    assert result.is_ng()
    assert result.counter['segment.request']  == 3
    assert result.counter['segment.response'] == 0
    assert result.counter['request']          == 1
    assert result.counter['request.200']      == 0
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))

    assert m_requests.call_count == 1
    assert m_requests.call_args_list[0][0] == ('http://localhost:5000/segmentlist',)
    assert m_requests.call_args_list[0][1]['data'] == {
                'app_key'       : 'brainpad',
                'login_id'      : 'login_id_9999',
                'password'      : 'password_9999',
                'advertiser_id' : 'advertiser_id_9999',
                'seg_id_1'      : '100',
                'seg_name_1'    : 'name100',
                'seg_note_1'    : 'note100',
                'seg_id_2'      : '200',
                'seg_name_2'    : 'name200',
                'seg_note_2'    : 'note200',
                'seg_id_3'      : '300',
                'seg_name_3'    : 'name300',
                'seg_note_3'    : 'note300',
            }

    assert m_s3put.call_count == 0
