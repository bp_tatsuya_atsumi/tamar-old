# -*- coding: utf-8 -*-

from lib.transferer.ma.remove_segmentmst import RemoveSegmentMstTransferer

import contextlib
import json
import gzip
import os
from   mock                     import Mock
from   mock                     import patch
import pytest
from   lib.transferer.exception import NoS3Files


ENV        = 'dev'
SID        = '9999'
PID        = 'ma'
S3BUCKET   = 's3bucket'
S3FILE     = 's3path/segmentmst/raw/segmentmst_20130103.csv.gz'
S3DIR      = 's3path/segmentmst/raw'
S3DIR_CONV = 's3path/segmentmst/converted_remove/ma'

CSV_HEADER = 'id,name,description'
CSV_DATA1  = '100,name100,note100'
CSV_DATA2  = '200,name200,note200'
CSV_DATA3  = '300,name300,note300'
CSV_DATA4  = '400,name400,note400'
CSV_DATA5  = '500,name500,note500'

@pytest.fixture
def data_dir(tmp_transfer_dir):
    return tmp_transfer_dir.mkdir(SID).mkdir(PID).mkdir('remove_segmentmst')

@contextlib.contextmanager
def _patches(func_init_config):
    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('lib.s3util.S3Util.list_dir'),
        patch('lib.s3util.S3Util.download_single_file'),
        patch('requests.post'),
        patch('lib.s3util.S3Util.upload_single_file'),
    ) as (m_config, m_s3ls, m_s3get, m_requests, m_s3put):
        m_s3ls.return_value = [
            {'key': 's3path/segmentmst/raw/segmentmst_20130101.csv.gz'},
            {'key': 's3path/segmentmst/raw/segmentmst_20130102.csv.gz'},
            {'key': 's3path/segmentmst/raw/segmentmst_20130103.csv.gz'}
        ]
        m_requests.return_value = _create_mock_response(200, 2)

        yield (m_s3ls, m_s3get, m_requests, m_s3put)

def _init():
    return RemoveSegmentMstTransferer(SID, PID, ENV, S3BUCKET, S3FILE)

def _create_gzip(file, lines):
    with gzip.open(file, 'wb') as f:
        f.write('\n'.join(lines))

def _create_mock_response(status_code, update_count):
    response = Mock()
    response.status_code = status_code
    response.text = json.dumps({'segment_count': update_count})
    return response


def test_download_s3files(tmp_transfer_dir, data_dir, func_init_config):
    with _patches(func_init_config) as (m_s3ls, m_s3get, m_requests, m_s3put):
        result = _init()._download_s3files()
        assert result == (
                   os.path.join(str(data_dir), 'segmentmst_20130102.csv.gz'),
                   'segmentmst_20130102.csv.gz',
                   os.path.join(str(data_dir), 'segmentmst_20130103.csv.gz'),
                   'segmentmst_20130103.csv.gz'
               )

def test_download_s3files_s3file_not_exist(tmp_transfer_dir, data_dir, func_init_config):
    with _patches(func_init_config) as (m_s3ls, m_s3get, m_requests, m_s3put):
        m_s3ls.return_value = []
        with pytest.raises(NoS3Files):
            _init()._download_s3files()

def test_create_request_datas():
    removed_segment_ids = [1,2,3]

    result = _init()._create_request_datas(removed_segment_ids)

    assert result == [{
        'app_key'       : 'brainpad',
        'login_id'      : 'login_id_9999',
        'password'      : 'password_9999',
        'advertiser_id' : 'advertiser_id_9999',
        'seg_id_1'      : 1,
        'seg_id_2'      : 2,
        'seg_id_3'      : 3
    }]

def test_create_request_datas_over_limit():
    removed_segment_ids = [x for x in range(0, 250)]

    result = _init()._create_request_datas(removed_segment_ids)

    assert len(result) == 3
    assert len([k for k in result[0].keys() if k.startswith('seg_id')]) == 100
    assert len([k for k in result[1].keys() if k.startswith('seg_id')]) == 100
    assert len([k for k in result[2].keys() if k.startswith('seg_id')]) == 50

def test_execute(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    old_data_file = data_dir.join('segmentmst_20130102.csv.gz')
    new_data_file = data_dir.join('segmentmst_20130103.csv.gz')
    _create_gzip(str(old_data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2, CSV_DATA3])
    _create_gzip(str(new_data_file), [CSV_HEADER, CSV_DATA1])
    converted_file = data_dir.join('segmentmst_20130103_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3ls, m_s3get, m_requests, m_s3put):
        result = _init().execute()

    # assert
    assert result.is_ok()
    assert result.counter['segment.request']  == 2
    assert result.counter['segment.response'] == 2
    assert result.counter['request']          == 1
    assert result.counter['request.200']      == 1
    assert old_data_file.check(exists=0)
    assert new_data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3ls.call_count == 1
    assert m_s3ls.call_args_list[0][0] == (S3BUCKET, S3DIR)

    assert m_s3get.call_count == 2
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, 's3path/segmentmst/raw/segmentmst_20130102.csv.gz', str(data_dir))
    assert m_s3get.call_args_list[1][0] == (S3BUCKET, 's3path/segmentmst/raw/segmentmst_20130103.csv.gz', str(data_dir))

    assert m_requests.call_count == 1
    assert m_requests.call_args_list[0][0] == ('http://localhost:5000/segmentlist/remove_bulk',)
    assert m_requests.call_args_list[0][1]['data'] == {
                'app_key'       : 'brainpad',
                'login_id'      : 'login_id_9999',
                'password'      : 'password_9999',
                'advertiser_id' : 'advertiser_id_9999',
                'seg_id_1'      : '200',
                'seg_id_2'      : '300',
           }

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3BUCKET, S3DIR_CONV)

def test_execute_s3file_notfound(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # execute
    with _patches(func_init_config) as (m_s3ls, m_s3get, m_requests, m_s3put):
        m_s3ls.return_value = []
        result = _init().execute()

    # assert
    assert result.is_ng()
    assert result.counter['segment.request']  == 0
    assert result.counter['segment.response'] == 0
    assert result.counter['request']          == 0
    assert result.counter['request.200']      == 0

    assert m_s3ls.call_count == 1
    assert m_s3ls.call_args_list[0][0] == (S3BUCKET, S3DIR)

    assert m_s3get.call_count       == 0
    assert m_requests.call_count    == 0
    assert m_s3put.call_count       == 0

def test_execute_no_removed_segment(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    old_data_file = data_dir.join('segmentmst_20130102.csv.gz')
    new_data_file = data_dir.join('segmentmst_20130103.csv.gz')
    _create_gzip(str(old_data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2, CSV_DATA3])
    _create_gzip(str(new_data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2, CSV_DATA3])
    converted_file = data_dir.join('segmentmst_20130103_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3ls, m_s3get, m_requests, m_s3put):
        result = _init().execute()

    # assert
    assert result.is_ok()
    assert result.counter['segment.request']  == 0
    assert result.counter['segment.response'] == 0
    assert result.counter['request']          == 0
    assert result.counter['request.200']      == 0
    assert old_data_file.check(exists=0)
    assert new_data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3ls.call_count == 1
    assert m_s3ls.call_args_list[0][0] == (S3BUCKET, S3DIR)

    assert m_s3get.call_count == 2
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, 's3path/segmentmst/raw/segmentmst_20130102.csv.gz', str(data_dir))
    assert m_s3get.call_args_list[1][0] == (S3BUCKET, 's3path/segmentmst/raw/segmentmst_20130103.csv.gz', str(data_dir))

    assert m_requests.call_count == 0
    assert m_s3put.call_count    == 0

def test_execute_empty_old_file(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    old_data_file = data_dir.join('segmentmst_20130102.csv.gz')
    new_data_file = data_dir.join('segmentmst_20130103.csv.gz')
    _create_gzip(str(old_data_file), [])
    _create_gzip(str(new_data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2, CSV_DATA3])
    converted_file = data_dir.join('segmentmst_20130103_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3ls, m_s3get, m_requests, m_s3put):
        result = _init().execute()

    # assert
    assert result.is_ok()
    assert result.counter['segment.request']  == 0
    assert result.counter['segment.response'] == 0
    assert result.counter['request']          == 0
    assert result.counter['request.200']      == 0
    assert old_data_file.check(exists=0)
    assert new_data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3ls.call_count == 1
    assert m_s3ls.call_args_list[0][0] == (S3BUCKET, S3DIR)

    assert m_s3get.call_count == 2
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, 's3path/segmentmst/raw/segmentmst_20130102.csv.gz', str(data_dir))
    assert m_s3get.call_args_list[1][0] == (S3BUCKET, 's3path/segmentmst/raw/segmentmst_20130103.csv.gz', str(data_dir))

    assert m_requests.call_count == 0
    assert m_s3put.call_count    == 0

def test_execute_empty_new_file(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    old_data_file = data_dir.join('segmentmst_20130102.csv.gz')
    new_data_file = data_dir.join('segmentmst_20130103.csv.gz')
    _create_gzip(str(old_data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2])
    _create_gzip(str(new_data_file), [])
    converted_file = data_dir.join('segmentmst_20130103_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3ls, m_s3get, m_requests, m_s3put):
        result = _init().execute()

    # assert
    assert result.is_ok()
    assert result.counter['segment.request']  == 2
    assert result.counter['segment.response'] == 2
    assert result.counter['request']          == 1
    assert result.counter['request.200']      == 1
    assert old_data_file.check(exists=0)
    assert new_data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3ls.call_count == 1
    assert m_s3ls.call_args_list[0][0] == (S3BUCKET, S3DIR)

    assert m_s3get.call_count == 2
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, 's3path/segmentmst/raw/segmentmst_20130102.csv.gz', str(data_dir))
    assert m_s3get.call_args_list[1][0] == (S3BUCKET, 's3path/segmentmst/raw/segmentmst_20130103.csv.gz', str(data_dir))

    assert m_requests.call_count == 1
    assert m_requests.call_args_list[0][0] == ('http://localhost:5000/segmentlist/remove_bulk',)
    assert m_requests.call_args_list[0][1]['data'] == {
                'app_key'       : 'brainpad',
                'login_id'      : 'login_id_9999',
                'password'      : 'password_9999',
                'advertiser_id' : 'advertiser_id_9999',
                'seg_id_1'      : '100',
                'seg_id_2'      : '200',
           }

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3BUCKET, S3DIR_CONV)

def test_execute_api_error(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    old_data_file = data_dir.join('segmentmst_20130102.csv.gz')
    new_data_file = data_dir.join('segmentmst_20130103.csv.gz')
    _create_gzip(str(old_data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2, CSV_DATA3])
    _create_gzip(str(new_data_file), [CSV_HEADER, CSV_DATA1])
    converted_file = data_dir.join('segmentmst_20130103_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3ls, m_s3get, m_requests, m_s3put):
        m_requests.return_value = _create_mock_response(500, 2)
        result = _init().execute()

    # assert
    assert result.is_api_error()
    assert result.counter['segment.request']  == 2
    assert result.counter['segment.response'] == 0
    assert result.counter['request']          == 1
    assert result.counter['request.200']      == 0
    assert result.counter['request.500']      == 1
    assert old_data_file.check(exists=0)
    assert new_data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3ls.call_count == 1
    assert m_s3ls.call_args_list[0][0] == (S3BUCKET, S3DIR)

    assert m_s3get.call_count == 2
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, 's3path/segmentmst/raw/segmentmst_20130102.csv.gz', str(data_dir))
    assert m_s3get.call_args_list[1][0] == (S3BUCKET, 's3path/segmentmst/raw/segmentmst_20130103.csv.gz', str(data_dir))

    assert m_requests.call_count == 1
    assert m_requests.call_args_list[0][0] == ('http://localhost:5000/segmentlist/remove_bulk',)
    assert m_requests.call_args_list[0][1]['data'] == {
                'app_key'       : 'brainpad',
                'login_id'      : 'login_id_9999',
                'password'      : 'password_9999',
                'advertiser_id' : 'advertiser_id_9999',
                'seg_id_1'      : '200',
                'seg_id_2'      : '300',
           }

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3BUCKET, S3DIR_CONV)

def test_execute_update_count_error(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    old_data_file = data_dir.join('segmentmst_20130102.csv.gz')
    new_data_file = data_dir.join('segmentmst_20130103.csv.gz')
    _create_gzip(str(old_data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2, CSV_DATA3])
    _create_gzip(str(new_data_file), [CSV_HEADER, CSV_DATA1])
    converted_file = data_dir.join('segmentmst_20130103_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3ls, m_s3get, m_requests, m_s3put):
        m_requests.return_value = _create_mock_response(200, 1)
        result = _init().execute()

    # assert
    assert result.is_api_error()
    assert result.counter['segment.request']  == 2
    assert result.counter['segment.response'] == 1
    assert result.counter['request']          == 1
    assert result.counter['request.200']      == 1
    assert old_data_file.check(exists=0)
    assert new_data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3ls.call_count == 1
    assert m_s3ls.call_args_list[0][0] == (S3BUCKET, S3DIR)

    assert m_s3get.call_count == 2
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, 's3path/segmentmst/raw/segmentmst_20130102.csv.gz', str(data_dir))
    assert m_s3get.call_args_list[1][0] == (S3BUCKET, 's3path/segmentmst/raw/segmentmst_20130103.csv.gz', str(data_dir))

    assert m_requests.call_count == 1
    assert m_requests.call_args_list[0][0] == ('http://localhost:5000/segmentlist/remove_bulk',)
    assert m_requests.call_args_list[0][1]['data'] == {
                'app_key'       : 'brainpad',
                'login_id'      : 'login_id_9999',
                'password'      : 'password_9999',
                'advertiser_id' : 'advertiser_id_9999',
                'seg_id_1'      : '200',
                'seg_id_2'      : '300',
           }

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3BUCKET, S3DIR_CONV)

def test_execute_unexpected_error(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    old_data_file = data_dir.join('segmentmst_20130102.csv.gz')
    new_data_file = data_dir.join('segmentmst_20130103.csv.gz')
    _create_gzip(str(old_data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2, CSV_DATA3])
    _create_gzip(str(new_data_file), [CSV_HEADER, CSV_DATA1])
    converted_file = data_dir.join('segmentmst_20130103_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3ls, m_s3get, m_requests, m_s3put):
        m_requests.side_effect = Exception('Failed to request')
        result = _init().execute()

    # assert
    assert result.is_ng()
    assert result.counter['segment.request']  == 2
    assert result.counter['segment.response'] == 0
    assert result.counter['request']          == 1
    assert result.counter['request.200']      == 0
    assert old_data_file.check(exists=0)
    assert new_data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3ls.call_count == 1
    assert m_s3ls.call_args_list[0][0] == (S3BUCKET, S3DIR)

    assert m_s3get.call_count == 2
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, 's3path/segmentmst/raw/segmentmst_20130102.csv.gz', str(data_dir))
    assert m_s3get.call_args_list[1][0] == (S3BUCKET, 's3path/segmentmst/raw/segmentmst_20130103.csv.gz', str(data_dir))

    assert m_requests.call_count == 1
    assert m_requests.call_args_list[0][0] == ('http://localhost:5000/segmentlist/remove_bulk',)
    assert m_requests.call_args_list[0][1]['data'] == {
                'app_key'       : 'brainpad',
                'login_id'      : 'login_id_9999',
                'password'      : 'password_9999',
                'advertiser_id' : 'advertiser_id_9999',
                'seg_id_1'      : '200',
                'seg_id_2'      : '300',
           }
