# -*- coding: utf-8 -*-

from lib.transferer.ma.conversion import ConversionTransferer

import contextlib
import gzip
import os
from   mock                     import patch
import pytest
from   lib.transferer.exception import NoS3Files


ENV             = 'dev'
SID             = '9999'
PID             = 'ma'
S3BUCKET        = 's3bucket'
S3FILE          = 's3dir/conversion/raw/conversion_20131001201510_9999.json.gz'
S3DIR_CONVERTED = 's3dir/conversion/converted/ma'

JSON_DATA1  = '{"pid": "rtoaster", "sid": "9999", "uid": "rt_uid1", "code": "item_code1", "datetime": "2013-01-01 00:00:00", "unit_id": 10, "figure": 100, "price": 1000}'
JSON_DATA2  = '{"pid": "rtoaster", "sid": "9999", "uid": "rt_uid2", "code": "item_code2", "datetime": "2013-01-02 00:00:00", "unit_id": 20, "figure": 200, "price": 2000}'
JSON_DATA3  = '{"pid": "rtoaster", "sid": "9999", "uid": "rt_uid3", "code": "item_code3", "datetime": "2013-01-03 00:00:00", "unit_id": 30, "figure": 300, "price": 3000}'
JSON_DATA4  = '{"pid": "rtoaster", "sid": "9999", "uid": "rt_uid4", "code": "item_code4", "datetime": "2013-01-04 00:00:00", "unit_id": 40, "figure": 400, "price": 4000}'
JSON_DATA5  = '{"pid": "rtoaster", "sid": "9999", "uid": "rt_uid5", "code": "item_code5", "datetime": "2013-01-05 00:00:00", "unit_id": 50, "figure": 500, "price": 5000}'

@pytest.fixture
def data_dir(tmp_transfer_dir):
    return tmp_transfer_dir.mkdir(SID).mkdir(PID).mkdir('conversion')

@contextlib.contextmanager
def _patches_with_default_action(func_init_config):
    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('lib.s3util.S3Util.download_single_file'),
        patch('lib.mappingtable.MappingTable.select_latest_mapping_partner_uid_by_user_id_until_n_days'),
        patch('lib.util.code_to_long_id'),
        patch('lib.sftputil.put'),
        patch('lib.s3util.S3Util.upload_single_file'),
    ) as (m_config, m_s3get, m_select, m_itemconv, m_sftpput, m_s3put):
        m_s3get.return_value       = {'key': S3FILE}
        m_select.side_effect       = _convert_uid
        m_itemconv.side_effect     = _convert_item_code
        m_sftpput.return_value     = True

        yield (m_s3get, m_select, m_itemconv, m_sftpput, m_s3put)

def _init(sid, pid, env, s3_bucket, s3_path):
    return ConversionTransferer(sid, pid, env, s3_bucket, s3_path)

def _create_gzip(file, lines):
    with gzip.open(file, 'wb') as f:
        f.write('\n'.join(lines))

def _convert_uid(*args):
    if args[0] == 'rt_uid1':
        return 'pt_uid1'
    if args[0] == 'rt_uid2':
        return 'pt_uid2'
    if args[0] == 'rt_uid3':
        return 'pt_uid3'
    return None

def _convert_item_code(*args):
    return int(args[0][-1])

def test_download_s3file(tmp_transfer_dir, data_dir, func_init_config):
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_select, m_itemconv, m_sftpput, m_s3put = mocks
        m_s3get.return_value = {
                                    'key'           : '/path/to/target_file.json.gz',
                                    'is_dir'        : False,
                                    'is_file'       : True,
                                    'size'          : 100,
                                    'last_modified' : '2013-10-10%00:00:00.000Z'
                               }
        transferer = _init(SID, PID, ENV, S3BUCKET, S3FILE)
        result     = transferer._download_s3file()
        assert result == (
                            os.path.join(str(data_dir), 'target_file.json.gz'),
                            'target_file.json.gz'
                         )

def test_download_s3file_file_not_exists(tmp_transfer_dir, data_dir, func_init_config):
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_select, m_itemconv, m_sftpput, m_s3put = mocks
        m_s3get.return_value = None

        with pytest.raises(NoS3Files):
            _init(SID, PID, ENV, S3BUCKET, S3FILE)._download_s3file()

def test_transform_transfer_format():
    transferer = _init(SID, PID, ENV, S3BUCKET, S3FILE)

    json_obj = {
                    'sid'        : '9999',
                    'uid'        : 'xxx',
                    'mapped_uid' : 'aaa',
                    'code'       : '12345',
                    'datetime'   : '2013-10-15 20:15:30',
                    'figure'     : '2',
                    'price'      : '1000',
                    'unit_id'    : '12345',
               }
    with patch('lib.util.code_to_long_id') as mock_code_to_long_id:
        mock_code_to_long_id.return_value = 1
        result = transferer._transform_transfer_format(json_obj)
        expected_result = '9999\t12345\tadvertiser_id_9999\taaa\t1\t2\t1000\t20131015201530\n'
        assert result == expected_result
        assert mock_code_to_long_id.call_count == 1
        assert mock_code_to_long_id.call_args_list[0][0] == ('12345',)

def test_create_transfer_filename(tmp_transfer_dir, data_dir, func_init_config):
    with _patches_with_default_action(func_init_config):
        transferer = _init(SID, PID, ENV, S3BUCKET, S3FILE)

        target_file = u'conversion_20131001201510_9999.json.gz'
        result      = transferer._create_transfer_filename(target_file)
        assert result == (
                            os.path.join(str(data_dir), 'brainpad_conversion_log_advertiser_id_9999.2013100120'),
                            'brainpad_conversion_log_advertiser_id_9999.2013100120'
                         )

        with pytest.raises(Exception):
            target_file = u'conversion_2013_9999.json.gz'
            transferer._create_transfer_filename(target_file)

def test_send_transfer_file():
    with patch('lib.sftputil.put'):
        transferer = _init(SID, PID, ENV, S3BUCKET, S3FILE)
        result = transferer._send_transfer_file('conversion')
        assert result

    with patch('lib.sftputil.put', side_effect=Exception('exception')):
        result = transferer._send_transfer_file('conversion')
        assert not result

def test_execute(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    data_file      = data_dir.join('conversion_20131001201510_9999.json.gz')
    transfer_file  = data_dir.join('brainpad_conversion_log_advertiser_id_9999.2013100120')
    converted_file = data_dir.join('brainpad_conversion_log_advertiser_id_9999.2013100120.gz')
    _create_gzip(str(data_file), [JSON_DATA1, JSON_DATA2, JSON_DATA3])

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_select, m_itemconv, m_sftpput, m_s3put = mocks

        transferer = _init(SID, PID, ENV, S3BUCKET, S3FILE)
        result = transferer.execute()

    # assert
    assert result.is_ok()
    assert result.counter['line.rtoaster'] == 3
    assert result.counter['line.mapped']   == 3
    assert result.counter['line.unmapped'] == 0
    assert data_file.check(exists=0)
    assert transfer_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))

    assert m_select.call_count == 3
    assert m_select.call_args_list[0][0] == (u'rt_uid1', 500)
    assert m_select.call_args_list[1][0] == (u'rt_uid2', 500)
    assert m_select.call_args_list[2][0] == (u'rt_uid3', 500)

    assert m_itemconv.call_count == 3
    assert m_itemconv.call_args_list[0][0] == (u'item_code1',)
    assert m_itemconv.call_args_list[1][0] == (u'item_code2',)
    assert m_itemconv.call_args_list[2][0] == (u'item_code3',)

    assert m_sftpput.call_count == 1
    assert m_sftpput.call_args_list[0][1]['host']        == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[0][1]['port']        == 115
    assert m_sftpput.call_args_list[0][1]['username']    == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['password']    == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['local_file']  == str(transfer_file)
    assert m_sftpput.call_args_list[0][1]['remote_dir']  == '/receive'
    assert m_sftpput.call_args_list[0][1]['rename']      == True

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3BUCKET, S3DIR_CONVERTED)

def test_execute_s3file_notfound(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_select, m_itemconv, m_sftpput, m_s3put = mocks
        m_s3get.return_value = None

        transferer = _init(SID, PID, ENV, S3BUCKET, S3FILE)
        result = transferer.execute()

    # assert
    assert result.is_ng()
    assert result.counter['line.rtoaster'] == 0
    assert result.counter['line.mapped']   == 0
    assert result.counter['line.unmapped'] == 0

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))

    assert m_select.call_count      == 0
    assert m_itemconv.call_count    == 0
    assert m_sftpput.call_count     == 0
    assert m_s3put.call_count       == 0

def test_execute_invalid_lines(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    data_file      = data_dir.join('conversion_20131001201510_9999.json.gz')
    transfer_file  = data_dir.join('brainpad_conversion_log_advertiser_id_9999.2013100120')
    converted_file = data_dir.join('brainpad_conversion_log_advertiser_id_9999.2013100120.gz')
    _create_gzip(str(data_file), [JSON_DATA1, JSON_DATA2, '', '  ', '{"id":"""}', JSON_DATA3])

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_select, m_itemconv, m_sftpput, m_s3put = mocks

        transferer = _init(SID, PID, ENV, S3BUCKET, S3FILE)
        result = transferer.execute()

    # assert
    assert result.is_api_error()
    assert result.counter['line.rtoaster'] == 6
    assert result.counter['line.mapped']   == 3
    assert result.counter['line.unmapped'] == 0
    assert result.counter['line.invalid']  == 1
    assert data_file.check(exists=0)
    assert transfer_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))

    assert m_select.call_count == 3
    assert m_select.call_args_list[0][0] == (u'rt_uid1', 500)
    assert m_select.call_args_list[1][0] == (u'rt_uid2', 500)
    assert m_select.call_args_list[2][0] == (u'rt_uid3', 500)

    assert m_itemconv.call_count == 3
    assert m_itemconv.call_args_list[0][0] == (u'item_code1',)
    assert m_itemconv.call_args_list[1][0] == (u'item_code2',)
    assert m_itemconv.call_args_list[2][0] == (u'item_code3',)

    assert m_sftpput.call_count == 1
    assert m_sftpput.call_args_list[0][1]['host']        == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[0][1]['port']        == 115
    assert m_sftpput.call_args_list[0][1]['username']    == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['password']    == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['local_file']  == str(transfer_file)
    assert m_sftpput.call_args_list[0][1]['remote_dir']  == '/receive'
    assert m_sftpput.call_args_list[0][1]['rename']      == True

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3BUCKET, S3DIR_CONVERTED)

def test_execute_empty_json_file(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    data_file      = data_dir.join('conversion_20131001201510_9999.json.gz')
    transfer_file  = data_dir.join('brainpad_conversion_log_advertiser_id_9999.2013100120')
    converted_file = data_dir.join('brainpad_conversion_log_advertiser_id_9999.2013100120.gz')
    _create_gzip(str(data_file), [])

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_select, m_itemconv, m_sftpput, m_s3put = mocks

        transferer = _init(SID, PID, ENV, S3BUCKET, S3FILE)
        result = transferer.execute()

    # assert
    assert result.is_ok()
    assert result.counter['line.rtoaster'] == 0
    assert result.counter['line.mapped']   == 0
    assert result.counter['line.unmapped'] == 0
    assert data_file.check(exists=0)
    assert transfer_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))

    assert m_select.call_count   == 0
    assert m_itemconv.call_count == 0

    assert m_sftpput.call_count == 1
    assert m_sftpput.call_args_list[0][1]['host']        == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[0][1]['port']        == 115
    assert m_sftpput.call_args_list[0][1]['username']    == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['password']    == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['local_file']  == str(transfer_file)
    assert m_sftpput.call_args_list[0][1]['remote_dir']  == '/receive'
    assert m_sftpput.call_args_list[0][1]['rename']      == True

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3BUCKET, S3DIR_CONVERTED)

def test_execute_some_unmapped_user(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    data_file      = data_dir.join('conversion_20131001201510_9999.json.gz')
    transfer_file  = data_dir.join('brainpad_conversion_log_advertiser_id_9999.2013100120')
    converted_file = data_dir.join('brainpad_conversion_log_advertiser_id_9999.2013100120.gz')
    _create_gzip(str(data_file), [JSON_DATA1, JSON_DATA2, JSON_DATA3, JSON_DATA4, JSON_DATA5])

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_select, m_itemconv, m_sftpput, m_s3put = mocks

        transferer = _init(SID, PID, ENV, S3BUCKET, S3FILE)
        result = transferer.execute()

    # assert
    assert result.is_ok()
    assert result.counter['line.rtoaster'] == 5
    assert result.counter['line.mapped']   == 3
    assert result.counter['line.unmapped'] == 2
    assert data_file.check(exists=0)
    assert transfer_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))

    assert m_select.call_count == 5
    assert m_select.call_args_list[0][0] == (u'rt_uid1', 500)
    assert m_select.call_args_list[1][0] == (u'rt_uid2', 500)
    assert m_select.call_args_list[2][0] == (u'rt_uid3', 500)
    assert m_select.call_args_list[3][0] == (u'rt_uid4', 500)
    assert m_select.call_args_list[4][0] == (u'rt_uid5', 500)

    assert m_itemconv.call_count == 3
    assert m_itemconv.call_args_list[0][0] == (u'item_code1',)
    assert m_itemconv.call_args_list[1][0] == (u'item_code2',)
    assert m_itemconv.call_args_list[2][0] == (u'item_code3',)

    assert m_sftpput.call_count == 1
    assert m_sftpput.call_args_list[0][1]['host']        == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[0][1]['port']        == 115
    assert m_sftpput.call_args_list[0][1]['username']    == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['password']    == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['local_file']  == str(transfer_file)
    assert m_sftpput.call_args_list[0][1]['remote_dir']  == '/receive'
    assert m_sftpput.call_args_list[0][1]['rename']      == True

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3BUCKET, S3DIR_CONVERTED)

def test_execute_no_mapped_user(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    data_file      = data_dir.join('conversion_20131001201510_9999.json.gz')
    transfer_file  = data_dir.join('brainpad_conversion_log_advertiser_id_9999.2013100120')
    converted_file = data_dir.join('brainpad_conversion_log_advertiser_id_9999.2013100120.gz')
    _create_gzip(str(data_file), [JSON_DATA4, JSON_DATA5])

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_select, m_itemconv, m_sftpput, m_s3put = mocks

        transferer = _init(SID, PID, ENV, S3BUCKET, S3FILE)
        result = transferer.execute()

    # assert
    assert result.is_ok()
    assert result.counter['line.rtoaster'] == 2
    assert result.counter['line.mapped']   == 0
    assert result.counter['line.unmapped'] == 2
    assert data_file.check(exists=0)
    assert transfer_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))

    assert m_select.call_count == 2
    assert m_select.call_args_list[0][0] == (u'rt_uid4', 500)
    assert m_select.call_args_list[1][0] == (u'rt_uid5', 500)

    assert m_itemconv.call_count == 0

    assert m_sftpput.call_count == 1
    assert m_sftpput.call_args_list[0][1]['host']        == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[0][1]['port']        == 115
    assert m_sftpput.call_args_list[0][1]['username']    == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['password']    == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['local_file']  == str(transfer_file)
    assert m_sftpput.call_args_list[0][1]['remote_dir']  == '/receive'
    assert m_sftpput.call_args_list[0][1]['rename']      == True

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3BUCKET, S3DIR_CONVERTED)

def test_execute_sftp_error(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    data_file      = data_dir.join('conversion_20131001201510_9999.json.gz')
    transfer_file  = data_dir.join('brainpad_conversion_log_advertiser_id_9999.2013100120')
    converted_file = data_dir.join('brainpad_conversion_log_advertiser_id_9999.2013100120.gz')
    _create_gzip(str(data_file), [JSON_DATA1, JSON_DATA2, JSON_DATA3])

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_select, m_itemconv, m_sftpput, m_s3put = mocks
        m_sftpput.side_effect = Exception()

        transferer = _init(SID, PID, ENV, S3BUCKET, S3FILE)
        result = transferer.execute()

    # assert
    assert result.is_ng()
    assert result.counter['line.rtoaster'] == 3
    assert result.counter['line.mapped']   == 3
    assert result.counter['line.unmapped'] == 0
    assert data_file.check(exists=0)
    assert transfer_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))

    assert m_select.call_count == 3
    assert m_select.call_args_list[0][0] == (u'rt_uid1', 500)
    assert m_select.call_args_list[1][0] == (u'rt_uid2', 500)
    assert m_select.call_args_list[2][0] == (u'rt_uid3', 500)

    assert m_itemconv.call_count == 3
    assert m_itemconv.call_args_list[0][0] == (u'item_code1',)
    assert m_itemconv.call_args_list[1][0] == (u'item_code2',)
    assert m_itemconv.call_args_list[2][0] == (u'item_code3',)

    assert m_sftpput.call_count == 1
    assert m_sftpput.call_args_list[0][1]['host']        == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[0][1]['port']        == 115
    assert m_sftpput.call_args_list[0][1]['username']    == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['password']    == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['local_file']  == str(transfer_file)
    assert m_sftpput.call_args_list[0][1]['remote_dir']  == '/receive'
    assert m_sftpput.call_args_list[0][1]['rename']      == True

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3BUCKET, S3DIR_CONVERTED)

def test_execute_unexpected_error(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    data_file      = data_dir.join('conversion_20131001201510_9999.json.gz')
    transfer_file  = data_dir.join('brainpad_conversion_log_advertiser_id_9999.2013100120')
    converted_file = data_dir.join('brainpad_conversion_log_advertiser_id_9999.2013100120.gz')
    _create_gzip(str(data_file), [JSON_DATA1, JSON_DATA2, JSON_DATA3])

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_select, m_itemconv, m_sftpput, m_s3put = mocks
        m_itemconv.return_value = None
        m_itemconv.side_effect  = Exception()

        transferer = _init(SID, PID, ENV, S3BUCKET, S3FILE)
        result = transferer.execute()

    # assert
    assert result.is_ng()
    assert result.counter['line.rtoaster'] == 0
    assert result.counter['line.mapped']   == 0
    assert result.counter['line.unmapped'] == 0
    assert data_file.check(exists=0)
    assert transfer_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))

    assert m_select.call_count == 1
    assert m_select.call_args_list[0][0] == (u'rt_uid1', 500)

    assert m_itemconv.call_count == 1
    assert m_itemconv.call_args_list[0][0] == (u'item_code1',)

    assert m_sftpput.call_count == 0
    assert m_s3put.call_count   == 0
