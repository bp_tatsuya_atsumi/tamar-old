# -*- coding: utf-8 -*-

from lib.transferer.ma.recommenddata import RecommendDataTransferer

import contextlib
import datetime
import gzip
import os
from mock import Mock
from mock import patch
from lib.transferer.exception import NoS3Files
import pytest


ENV = 'dev'
SID = '9999'
PID = 'ma'
S3_BUCKET = 's3_bucket'
S3_PATH_RAW = 's3_path/recommenddata/raw/split/recommenddata.json.gz'
S3_PATH_CNV = 's3_path/recommenddata/converted/ma/split'

JSON_DATA1  = '{"pid": "rtoaster", "sid": "9999", "uid": "rt_uid1", "item_lists": {"1": ["rtoaster", "l2mixer"], "2": ["rtoaster", "l2mixer"]}}'
JSON_DATA2  = '{"pid": "rtoaster", "sid": "9999", "uid": "rt_uid2", "item_lists": {"1": ["crossoven", "semanticfinder"], "2": ["crossoven", "semanticfinder"]}}'
JSON_DATA3  = '{"pid": "rtoaster", "sid": "9999", "uid": "rt_uid3", "item_lists": {"1": ["rtoaster", "l2mixer"], "2": ["rtoaster", "l2mixer"]}}'

@pytest.fixture
def data_dir(tmp_transfer_dir):
    return tmp_transfer_dir.mkdir(SID).mkdir(PID).mkdir('recommenddata')

class CurrentDatetime(datetime.datetime):
    @classmethod
    def now(cls):
        return cls(2013, 10, 19, 23, 40, 59, 0)

@contextlib.contextmanager
def _patches_with_default_action(func_init_config):
    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('lib.s3util.S3Util.download_single_file'),
        patch('lib.mappingtable.MappingTable.select_latest_mapping_partner_uid_by_user_id_until_n_days'),
        patch('datetime.datetime', CurrentDatetime),
        patch('requests.get'),
        patch('lib.s3util.S3Util.upload_single_file'),
        patch('lib.util.code_to_long_id'),
    ) as (m_config, m_s3get, m_select, m_now, m_requests, m_s3put,  m_itemcodeconv):
        m_s3get.return_value       = {'key': 's3_path/recommenddata/raw/split/recommenddata.json.gz'}
        m_select.side_effect       = _convert_uid
        m_requests.return_value    = _create_mock_response(200)
        m_itemcodeconv.side_effect = _convert_itemcode

        yield (m_s3get, m_select, m_requests, m_s3put, m_itemcodeconv)

def _convert_uid(*args):
    tbl = {'rt_uid1': 'pt_uid1', 'rt_uid2': 'pt_uid2'}
    return tbl.get(args[0])

def _convert_itemcode(*args):
    tbl = {
        'rtoaster'       : 100,
        'l2mixer'        : 200,
        'crossoven'      : 300,
        'semanticfinder' : 400,
    }
    return tbl.get(args[0])

def _init(sid, pid, env, s3_bucket, s3_path):
    return RecommendDataTransferer(sid, pid, env, s3_bucket, s3_path)

def _create_gzip(file, lines):
    with gzip.open(file, 'wb') as f:
        f.write('\n'.join(lines))

def _create_mock_response(status_code):
    response = Mock()
    response.status_code = status_code
    response.text = ''
    return response

def test_transform_transfer_format(func_init_config):
    row = {
            'pid'        : 'rtoaster',
            'sid'        : '9999',
            'uid'        : 'rt_uid1',
            'mapped_uid' : 'pt_uid1',
            'item_lists' : {
                                '1' : ['rtoaster',  'l2mixer'],
                                '2' : ['crossoven', 'semanticfinder'],
                           }
          }

    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_select, m_requests, m_s3put, m_itemcodeconv = mocks
        transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH_RAW)
        query_string = transferer._transform_transfer_format(row)

    assert query_string == 'v=1&u=pt_uid1&rpl=advertiser_id_9999-1382452859:1-100.200,2-300.400'

def test_execute(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    data_file = data_dir.join('recommenddata.json.gz')
    _create_gzip(str(data_file), [JSON_DATA1, JSON_DATA2])
    converted_file = data_dir.join('recommenddata_converted.tsv.gz')

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_select, m_requests, m_s3put, m_itemcodeconv = mocks

        transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH_RAW)
        result = transferer.execute()

    # assert
    assert result.is_ok()
    assert result.counter['user.rtoaster'] == 2
    assert result.counter['user.mapped']   == 2
    assert result.counter['user.unmapped'] == 0
    assert result.counter['request']       == 2
    assert result.counter['request.200']   == 2
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == ('s3_bucket', S3_PATH_RAW, str(data_dir))

    assert m_select.call_count == 2
    assert m_select.call_args_list[0][0] == (u'rt_uid1', 500)
    assert m_select.call_args_list[1][0] == (u'rt_uid2', 500)

    assert m_requests.call_count == 2
    assert m_requests.call_args_list[0][0] == ('http://localhost:5000/rpl?v=1&u=pt_uid1&rpl=advertiser_id_9999-1382452859:1-100.200,2-100.200',)
    assert m_requests.call_args_list[1][0] == ('http://localhost:5000/rpl?v=1&u=pt_uid2&rpl=advertiser_id_9999-1382452859:1-300.400,2-300.400',)

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3_BUCKET, S3_PATH_CNV)

def test_execute_s3file_notfound(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_select, m_requests, m_s3put, m_itemcodeconv = mocks
        m_s3get.return_value = None

        transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH_RAW)
        result = transferer.execute()

    # assert
    assert result.is_ng()
    assert result.counter['user.rtoaster'] == 0
    assert result.counter['user.mapped']   == 0
    assert result.counter['user.unmapped'] == 0
    assert result.counter['request']       == 0
    assert result.counter['request.200']   == 0

    assert m_s3get.call_args_list[0][0] == ('s3_bucket', S3_PATH_RAW, str(data_dir))

    assert m_select.call_count    == 0
    assert m_requests.call_count  == 0
    assert m_s3put.call_count     == 0

def test_execute_invalid_lines(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    data_file = data_dir.join('recommenddata.json.gz')
    _create_gzip(str(data_file), [JSON_DATA1, JSON_DATA2, '', '  ', '{"id":"""}'])
    converted_file = data_dir.join('recommenddata_converted.tsv.gz')

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_select, m_requests, m_s3put, m_itemcodeconv = mocks

        transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH_RAW)
        result = transferer.execute()

    # assert
    assert result.is_api_error()
    assert result.counter['user.rtoaster'] == 5
    assert result.counter['user.mapped']   == 2
    assert result.counter['user.unmapped'] == 0
    assert result.counter['user.invalid']  == 1
    assert result.counter['request']       == 2
    assert result.counter['request.200']   == 2
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == ('s3_bucket', S3_PATH_RAW, str(data_dir))

    assert m_select.call_count == 2
    assert m_select.call_args_list[0][0] == (u'rt_uid1', 500)
    assert m_select.call_args_list[1][0] == (u'rt_uid2', 500)

    assert m_requests.call_count == 2
    assert m_requests.call_args_list[0][0] == ('http://localhost:5000/rpl?v=1&u=pt_uid1&rpl=advertiser_id_9999-1382452859:1-100.200,2-100.200',)
    assert m_requests.call_args_list[1][0] == ('http://localhost:5000/rpl?v=1&u=pt_uid2&rpl=advertiser_id_9999-1382452859:1-300.400,2-300.400',)

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3_BUCKET, S3_PATH_CNV)

def test_execute_empty_json_file(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    data_file = data_dir.join('recommenddata.json.gz')
    _create_gzip(str(data_file), [])
    converted_file = data_dir.join('recommenddata_converted.tsv.gz')

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_select, m_requests, m_s3put, m_itemcodeconv = mocks

        transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH_RAW)
        result = transferer.execute()

    # assert
    assert result.is_ok()
    assert result.counter['user.rtoaster'] == 0
    assert result.counter['user.mapped']   == 0
    assert result.counter['user.unmapped'] == 0
    assert result.counter['request']       == 0
    assert result.counter['request.200']   == 0
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == ('s3_bucket', S3_PATH_RAW, str(data_dir))

    assert m_select.call_count   == 0
    assert m_requests.call_count == 0

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3_BUCKET, S3_PATH_CNV)

def test_execute_some_unmapped_user(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    data_file = data_dir.join('recommenddata.json.gz')
    _create_gzip(str(data_file), [JSON_DATA1, JSON_DATA2, JSON_DATA3])
    converted_file = data_dir.join('recommenddata_converted.tsv.gz')

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_select, m_requests, m_s3put, m_itemcodeconv = mocks

        transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH_RAW)
        result = transferer.execute()

    # assert
    assert result.is_ok()
    assert result.counter['user.rtoaster'] == 3
    assert result.counter['user.mapped']   == 2
    assert result.counter['user.unmapped'] == 1
    assert result.counter['request']       == 2
    assert result.counter['request.200']   == 2
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == ('s3_bucket', S3_PATH_RAW, str(data_dir))

    assert m_select.call_count == 3
    assert m_select.call_args_list[0][0] == (u'rt_uid1', 500)
    assert m_select.call_args_list[1][0] == (u'rt_uid2', 500)
    assert m_select.call_args_list[2][0] == (u'rt_uid3', 500)

    assert m_requests.call_count == 2
    assert m_requests.call_args_list[0][0] == ('http://localhost:5000/rpl?v=1&u=pt_uid1&rpl=advertiser_id_9999-1382452859:1-100.200,2-100.200',)
    assert m_requests.call_args_list[1][0] == ('http://localhost:5000/rpl?v=1&u=pt_uid2&rpl=advertiser_id_9999-1382452859:1-300.400,2-300.400',)

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3_BUCKET, S3_PATH_CNV)

def test_execute_no_mapped_user(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    data_file = data_dir.join('recommenddata.json.gz')
    _create_gzip(str(data_file), [JSON_DATA3])
    converted_file = data_dir.join('recommenddata_converted.tsv.gz')

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_select, m_requests, m_s3put, m_itemcodeconv = mocks

        transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH_RAW)
        result = transferer.execute()

    # assert
    assert result.is_ok()
    assert result.counter['user.rtoaster'] == 1
    assert result.counter['user.mapped']   == 0
    assert result.counter['user.unmapped'] == 1
    assert result.counter['request']       == 0
    assert result.counter['request.200']   == 0
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == ('s3_bucket', S3_PATH_RAW, str(data_dir))

    assert m_select.call_count == 1
    assert m_select.call_args_list[0][0] == (u'rt_uid3', 500)

    assert m_requests.call_count == 0

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3_BUCKET, S3_PATH_CNV)

def test_execute_api_error(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    data_file = data_dir.join('recommenddata.json.gz')
    _create_gzip(str(data_file), [JSON_DATA1, JSON_DATA2])
    converted_file = data_dir.join('recommenddata_converted.tsv.gz')

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_select, m_requests, m_s3put, m_itemcodeconv = mocks
        m_requests.return_value = None
        m_requests.side_effect  = [
                                    _create_mock_response(200),
                                    _create_mock_response(400),
                                  ]

        transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH_RAW)
        result = transferer.execute()

    # assert
    assert result.is_api_error()
    assert result.counter['user.rtoaster'] == 2
    assert result.counter['user.mapped']   == 2
    assert result.counter['user.unmapped'] == 0
    assert result.counter['request']       == 2
    assert result.counter['request.200']   == 1
    assert result.counter['request.400']   == 1
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == ('s3_bucket', S3_PATH_RAW, str(data_dir))

    assert m_select.call_count == 2
    assert m_select.call_args_list[0][0] == (u'rt_uid1', 500)
    assert m_select.call_args_list[1][0] == (u'rt_uid2', 500)

    assert m_requests.call_count == 2
    assert m_requests.call_args_list[0][0] == ('http://localhost:5000/rpl?v=1&u=pt_uid1&rpl=advertiser_id_9999-1382452859:1-100.200,2-100.200',)
    assert m_requests.call_args_list[1][0] == ('http://localhost:5000/rpl?v=1&u=pt_uid2&rpl=advertiser_id_9999-1382452859:1-300.400,2-300.400',)

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3_BUCKET, S3_PATH_CNV)

def test_execute_unexpected_error(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    data_file = data_dir.join('recommenddata.json.gz')
    _create_gzip(str(data_file), [JSON_DATA1, JSON_DATA2])
    converted_file = data_dir.join('recommenddata_converted.tsv.gz')

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_select, m_requests, m_s3put, m_itemcodeconv = mocks
        m_select.side_effect = Exception()

        transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH_RAW)
        result = transferer.execute()

    # assert
    assert result.is_ng()
    assert result.counter['user.rtoaster'] == 0
    assert result.counter['user.mapped']   == 0
    assert result.counter['user.unmapped'] == 0
    assert result.counter['request']       == 0
    assert result.counter['request.200']   == 0
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == ('s3_bucket', S3_PATH_RAW, str(data_dir))

    assert m_select.call_count == 1
    assert m_select.call_args_list[0][0] == (u'rt_uid1', 500)

    assert m_requests.call_count == 0
    assert m_s3put.call_count    == 0

