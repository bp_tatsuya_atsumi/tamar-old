# -*- coding: utf-8 -*-

from lib.transferer.ma.segmentdata import SegmentDataTransferer

import datetime
import contextlib
import json
import gzip
import os
from   mock                     import Mock
from   mock                     import patch
import pytest
from   lib.transferer.exception import NoS3Files

SID        = '9999'
PID        = 'ma'
ENV        = 'dev'
S3BUCKET   = 's3bucket'
S3PATH     = 's3path/segmentdata/raw/split/segmentdata.json.gz'
S3PATH_CNV = 's3path/segmentdata/converted/ma/split'

SEG_DATA1  = '{"pid": "rtoaster", "sid": "9999", "uid": "rt_uid1", "attrs": [10, 11, 12]}'
SEG_DATA2  = '{"pid": "rtoaster", "sid": "9999", "uid": "rt_uid2", "attrs": [20, 21, 22]}'
SEG_DATA3  = '{"pid": "rtoaster", "sid": "9999", "uid": "rt_uid3", "attrs": [30, 31, 32]}'
SEG_DATA4  = '{"pid": "rtoaster", "sid": "9999", "uid": "rt_uid4", "attrs": []}'

@pytest.fixture
def data_dir(tmp_transfer_dir):
    return tmp_transfer_dir.mkdir(SID).mkdir(PID).mkdir('segmentdata')

class CurrentDatetime(datetime.datetime):
    @classmethod
    def now(cls):
        return cls(2013, 10, 19, 23, 40, 59, 0)

@contextlib.contextmanager
def _patches(func_init_config):
    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('lib.s3util.S3Util.download_single_file'),
        patch('lib.mappingtable.MappingTable.select_latest_mapping_partner_uid_by_user_id_until_n_days'),
        patch('datetime.datetime', CurrentDatetime),
        patch('requests.get'),
        patch('lib.s3util.S3Util.upload_single_file'),
    ) as (m_config, m_s3get, m_select, m_now, m_requests, m_s3put):
        m_s3get.side_effect        = [ {'key' : 's3_path/segmentdata/segmentdata.json.gz'} ]
        m_select.side_effect       = _convert_uid
        m_requests.return_value    = _create_mock_response(200)

        yield (m_s3get, m_select, m_requests, m_s3put)

def _convert_uid(*args):
    tbl = {
        'rt_uid1': 'pt_uid1',
        'rt_uid2': 'pt_uid2',
        'rt_uid4': 'pt_uid4'
    }
    return tbl.get(args[0])

def _init():
    return SegmentDataTransferer(SID, PID, ENV, S3BUCKET, S3PATH)

def _create_gzip(file, lines):
    with gzip.open(file, 'wb') as f:
        f.write('\n'.join(lines))

def _create_mock_response(status_code):
    response = Mock()
    response.status_code = status_code
    response.text = ''
    return response


def test_download_s3file(data_dir, func_init_config):
    with _patches(func_init_config) as mocks:
        m_s3get, m_select, m_requests, m_s3put = mocks
        result = _init()._download_s3file()
        assert result == (
            os.path.join(str(data_dir), 'segmentdata.json.gz'),
            'segmentdata.json.gz'
        )

def test_download_s3file_not_exists(data_dir, func_init_config):
    with _patches(func_init_config) as mocks:
        m_s3get, m_select, m_requests, m_s3put = mocks
        m_s3get.side_effect  = None
        m_s3get.return_value = None
        with pytest.raises(NoS3Files):
            _init()._download_s3file()

def test_create_request_url(func_init_config):
    with _patches(func_init_config):
        row = json.loads(SEG_DATA1)
        row['mapped_uid'] = 'pt_uid1'
        result = _init()._create_request_url(row)
        assert result == 'http://localhost:5000/sl?v=1&u=pt_uid1&sl=advertiser_id_9999-1382452859:10,11,12'

def test_execute(data_dir, func_init_config):

    # prepare
    data_file      = data_dir.join('segmentdata.json.gz')
    converted_file = data_dir.join('segmentdata_converted.tsv.gz')

    _create_gzip(str(data_file), [SEG_DATA1,  SEG_DATA2, SEG_DATA3, SEG_DATA4])

    # execute
    with _patches(func_init_config) as mocks:
        m_s3get, m_select, m_requests, m_s3put = mocks
        result = _init().execute()

    # assert
    assert result.is_ok()
    assert result.counter['user.rtoaster'] == 4
    assert result.counter['user.mapped']   == 3
    assert result.counter['user.unmapped'] == 1
    assert result.counter['request']       == 2
    assert result.counter['request.200']   == 2
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3PATH, str(data_dir))

    assert m_select.call_count == 4
    assert m_select.call_args_list[0][0] == (u'rt_uid1', 500)
    assert m_select.call_args_list[1][0] == (u'rt_uid2', 500)
    assert m_select.call_args_list[2][0] == (u'rt_uid3', 500)
    assert m_select.call_args_list[3][0] == (u'rt_uid4', 500)

    assert m_requests.call_count == 2
    assert m_requests.call_args_list[0][0] == ('http://localhost:5000/sl?v=1&u=pt_uid1&sl=advertiser_id_9999-1382452859:10,11,12',)
    assert m_requests.call_args_list[1][0] == ('http://localhost:5000/sl?v=1&u=pt_uid2&sl=advertiser_id_9999-1382452859:20,21,22',)

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3BUCKET, S3PATH_CNV)

def test_execute_data_not_exists(data_dir, func_init_config):

    # execute
    with _patches(func_init_config) as mocks:
        m_s3get, m_select, m_requests, m_s3put = mocks
        m_s3get.side_effect = [ None ]
        result = _init().execute()

    # assert
    assert result.is_ng()
    assert result.counter['user.rtoaster'] == 0
    assert result.counter['user.mapped']   == 0
    assert result.counter['user.unmapped'] == 0
    assert result.counter['request']       == 0
    assert result.counter['request.200']   == 0

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3PATH, str(data_dir))

    assert m_select.call_count      == 0
    assert m_requests.call_count    == 0
    assert m_s3put.call_count       == 0

    # execute
    with _patches(func_init_config) as mocks:
        m_s3get, m_select, m_requests, m_s3put = mocks
        m_s3get.side_effect        = [ {'key' : 's3_path/segmentdata/segmentdata.json.gz'} ]
        result = _init().execute()

    # assert
    assert result.is_ng()
    assert result.counter['user.rtoaster'] == 0
    assert result.counter['user.mapped']   == 0
    assert result.counter['user.unmapped'] == 0
    assert result.counter['request']       == 0
    assert result.counter['request.200']   == 0

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3PATH, str(data_dir))

    assert m_select.call_count      == 0
    assert m_requests.call_count    == 0
    assert m_s3put.call_count       == 0

def test_execute_empty_data(data_dir, func_init_config):

    # prepare
    data_file      = data_dir.join('segmentdata.json.gz')
    converted_file = data_dir.join('segmentdata_converted.tsv.gz')

    _create_gzip(str(data_file), [])

    # execute
    with _patches(func_init_config) as mocks:
        m_s3get, m_select, m_requests, m_s3put = mocks
        result = _init().execute()

    # assert
    assert result.is_ok()
    assert result.counter['user.rtoaster'] == 0
    assert result.counter['user.mapped']   == 0
    assert result.counter['user.unmapped'] == 0
    assert result.counter['request']       == 0
    assert result.counter['request.200']   == 0
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3PATH, str(data_dir))

    assert m_select.call_count   == 0
    assert m_requests.call_count == 0

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3BUCKET, S3PATH_CNV)

def test_execute_api_error(data_dir, func_init_config):

    # prepare
    data_file      = data_dir.join('segmentdata.json.gz')
    converted_file = data_dir.join('segmentdata_converted.tsv.gz')

    _create_gzip(str(data_file), [SEG_DATA1,  SEG_DATA2, SEG_DATA3, SEG_DATA4])

    # execute
    with _patches(func_init_config) as mocks:
        m_s3get, m_select, m_requests, m_s3put = mocks
        m_requests.side_effect  = [_create_mock_response(200),
                                   _create_mock_response(400)]
        result = _init().execute()

    # assert
    assert result.is_api_error()
    assert result.counter['user.rtoaster'] == 4
    assert result.counter['user.mapped']   == 3
    assert result.counter['user.unmapped'] == 1
    assert result.counter['request']       == 2
    assert result.counter['request.200']   == 1
    assert result.counter['request.400']   == 1
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3PATH, str(data_dir))

    assert m_select.call_count == 4
    assert m_select.call_args_list[0][0] == (u'rt_uid1', 500)
    assert m_select.call_args_list[1][0] == (u'rt_uid2', 500)
    assert m_select.call_args_list[2][0] == (u'rt_uid3', 500)
    assert m_select.call_args_list[3][0] == (u'rt_uid4', 500)

    assert m_requests.call_count == 2
    assert m_requests.call_args_list[0][0] == ('http://localhost:5000/sl?v=1&u=pt_uid1&sl=advertiser_id_9999-1382452859:10,11,12',)
    assert m_requests.call_args_list[1][0] == ('http://localhost:5000/sl?v=1&u=pt_uid2&sl=advertiser_id_9999-1382452859:20,21,22',)

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3BUCKET, S3PATH_CNV)

def test_execute_unexpected_error(data_dir, func_init_config):

    # prepare
    data_file      = data_dir.join('segmentdata.json.gz')
    converted_file = data_dir.join('segmentdata_converted.tsv.gz')

    _create_gzip(str(data_file), [SEG_DATA1,  SEG_DATA2, SEG_DATA3, SEG_DATA4])

    # execute
    with _patches(func_init_config) as mocks:
        m_s3get, m_select, m_requests, m_s3put = mocks
        m_select.side_effect = Exception()
        result = _init().execute()

    # assert
    assert result.is_ng()
    assert result.counter['user.rtoaster'] == 0
    assert result.counter['user.mapped']   == 0
    assert result.counter['user.unmapped'] == 0
    assert result.counter['request']       == 0
    assert result.counter['request.200']   == 0
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3PATH, str(data_dir))

    assert m_select.call_count == 1
    assert m_select.call_args_list[0][0] == (u'rt_uid1', 500)

    assert m_requests.call_count == 0
    assert m_s3put.call_count    == 0
