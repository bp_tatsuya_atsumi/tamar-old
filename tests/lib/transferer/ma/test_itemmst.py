# -*- coding: utf-8 -*-

from lib.transferer.ma.itemmst import ItemMstTransferer

import contextlib
import datetime
import gzip
import os
from mock import patch
import pytest


ENV         = 'dev'
SID         = '9999'
PID         = 'ma'
S3_BUCKET   = 's3_bucket'
S3_PATH     = 's3_path/itemmst/raw/itemmst_20131001201510_9999.csv.gz'
S3_DIR_CONV = 's3_path/itemmst/converted/ma'

CSV_HEADER = 'pid,sid,code,name,img_url,link_url,price,description,status,last_modified_date'
CSV_DATA1  = 'rtoaster,999,Item_1000,商品名1000,http://imageurl/1000.jpg,http://linkurl/1000,"<br>1,000<br>",説明1000,0,2013-09-06 15:20:53'
CSV_DATA2  = 'rtoaster,999,Item_2000,商品名2000,http://imageurl/2000.jpg,http://linkurl/2000,"<br>2,000<br>",説明2000,0,2013-09-06 15:20:53'
CSV_DATA3  = 'rtoaster,999,Item_3000,商品名3000,http://imageurl/3000.jpg,http://linkurl/3000,"<br>3,000<br>",説明3000,0,2013-09-06 15:20:53'
CSV_DATA4  = 'rtoaster,999,Item_4000,商品名4000,http://imageurl/4000.jpg,http://linkurl/4000,"<br>4,000<br>",説明4000,1,2013-09-06 15:20:53'
CSV_DATA5  = 'rtoaster,999,Item_5000,商品名5000,http://imageurl/5000.jpg,http://linkutl/5000,"<br>5,000<br>",説明5000,1,2013-09-06 15:20:53'

TSV_UPDATE_DATA1 = '9999	1000	商品名1000	http://imageurl/1000.jpg	http://linkurl/1000	<br>1,000<br>	説明1000	advertiser_id_9999'
TSV_UPDATE_DATA2 = '9999	2000	商品名2000	http://imageurl/2000.jpg	http://linkurl/2000	<br>2,000<br>	説明2000	advertiser_id_9999'
TSV_UPDATE_DATA3 = '9999	3000	商品名3000	http://imageurl/3000.jpg	http://linkurl/3000	<br>3,000<br>	説明3000	advertiser_id_9999'
TSV_DELETE_DATA4 = '9999	4000	advertiser_id_9999'
TSV_DELETE_DATA5 = '9999	5000	advertiser_id_9999'

class CurrentDatetime(datetime.datetime):
    @classmethod
    def now(cls):
        return cls(2013, 10, 19, 23, 40, 59, 0)

@pytest.fixture
def data_dir(tmp_transfer_dir):
    return tmp_transfer_dir.mkdir(SID).mkdir(PID).mkdir('itemmst')

@contextlib.contextmanager
def _patches_with_default_action(func_init_config):
    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('lib.s3util.S3Util.download_single_file'),
        patch('lib.util.code_to_long_id'),
        patch('lib.sftputil.SftpUtil.__enter__'),
        patch('lib.s3util.S3Util.upload_single_file'),
    ) as (m_config, m_s3get, m_itemconv, m_sftp, m_s3put):
        m_s3get.return_value       = {'key': S3_PATH}
        m_itemconv.side_effect     = _convert_item_code

        yield (m_s3get, m_itemconv, m_sftp, m_s3put)

@contextlib.contextmanager
def _patches_filename(data_dir):
    with contextlib.nested (
        patch('lib.transferer.ma.itemmst.ItemMstTransferer.create_update_filename'),
        patch('lib.transferer.ma.itemmst.ItemMstTransferer.create_delete_filename'),
    ) as (m_updfilename, m_delfilename):
        m_updfilename.side_effect = [os.path.join(str(data_dir), 'itemmst_update_1.gz'),
                                     os.path.join(str(data_dir), 'itemmst_update_2.gz'),
                                     os.path.join(str(data_dir), 'itemmst_update_3.gz'),
                                     os.path.join(str(data_dir), 'itemmst_update_4.gz'),
                                     os.path.join(str(data_dir), 'itemmst_update_5.gz'),
                                    ]
        m_delfilename.side_effect = [os.path.join(str(data_dir), 'itemmst_delete_1.gz'),
                                     os.path.join(str(data_dir), 'itemmst_delete_2.gz'),
                                     os.path.join(str(data_dir), 'itemmst_delete_3.gz'),
                                     os.path.join(str(data_dir), 'itemmst_delete_4.gz'),
                                     os.path.join(str(data_dir), 'itemmst_delete_5.gz'),
                                    ]
        yield (m_updfilename, m_delfilename)

def _target():
    return ItemMstTransferer(SID, PID, ENV, S3_BUCKET, S3_PATH)

def _create_gzip(file, lines):
    with gzip.open(file, 'wb') as f:
        f.write('\n'.join(lines))

def _convert_item_code(*args):
    return int(args[0][5:])

def _assert_py_path_file(py_path_file, lines):
    assert py_path_file.check(file=1, exists=1)
    content = '\n'.join(lines)
    if content:
        content = content + '\n'
    assert gzip.open(str(py_path_file), 'rb').read() == content

def test_create_update_filename(func_init_config, data_dir):
    with _patches_with_default_action(func_init_config):
        with patch('datetime.datetime', CurrentDatetime):
            result = _target().create_update_filename()
            assert result == os.path.join(str(data_dir), 'brainpad_product_master_advertiser_id_9999.20131019234059.gz')

def test_create_delete_filename(func_init_config, data_dir):
    with _patches_with_default_action(func_init_config):
        with patch('datetime.datetime', CurrentDatetime):
            result = _target().create_delete_filename()
            assert result == os.path.join(str(data_dir), 'brainpad_product_delete_advertiser_id_9999.20131019234059.gz')

def test_create_transfer_files(func_init_config, data_dir):

    # prepare
    data_file = data_dir.join('itemmst_20131001201510_9999.csv.gz')
    _create_gzip(str(data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2, CSV_DATA3, CSV_DATA4, CSV_DATA5])

    transfer_update_file  = data_dir.join('itemmst_update_1.gz')
    transfer_delete_file  = data_dir.join('itemmst_delete_1.gz')

    # execute
    with contextlib.nested (
        _patches_with_default_action(func_init_config),
        _patches_filename(data_dir)
    ) as (mocks, mocks_fn):
        m_s3get, m_itemconv, m_sftp, m_s3put = mocks

        result = []
        _target().create_transfer_files(str(data_file), result)

    # assert
    assert result == [str(transfer_update_file), str(transfer_delete_file)]
    _assert_py_path_file(transfer_update_file, [TSV_UPDATE_DATA1, TSV_UPDATE_DATA2, TSV_UPDATE_DATA3])
    _assert_py_path_file(transfer_delete_file, [TSV_DELETE_DATA4, TSV_DELETE_DATA5])

def test_create_transfer_files_over_limit(func_init_config, data_dir):

    # prepare
    data_file = data_dir.join('itemmst_20131001201510_9999.csv.gz')
    _create_gzip(str(data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2, CSV_DATA3, CSV_DATA4, CSV_DATA5])

    transfer_update_file1  = data_dir.join('itemmst_update_1.gz')
    transfer_update_file2  = data_dir.join('itemmst_update_2.gz')
    transfer_delete_file1  = data_dir.join('itemmst_delete_1.gz')

    # execute
    with contextlib.nested (
        _patches_with_default_action(func_init_config),
        _patches_filename(data_dir)
    ) as (mocks, mocks_fn):

        result = []
        target = _target()
        target.LINE_NUM_LIMIT = 2
        target.create_transfer_files(str(data_file), result)

    # assert
    assert result == [str(transfer_update_file1), str(transfer_delete_file1), str(transfer_update_file2)]
    _assert_py_path_file(transfer_update_file1, [TSV_UPDATE_DATA1, TSV_UPDATE_DATA2])
    _assert_py_path_file(transfer_update_file2, [TSV_UPDATE_DATA3])
    _assert_py_path_file(transfer_delete_file1, [TSV_DELETE_DATA4, TSV_DELETE_DATA5])

def test_create_transfer_files_empty_csv_file(func_init_config, data_dir):

    # prepare
    data_file = data_dir.join('itemmst_20131001201510_9999.csv.gz')
    _create_gzip(str(data_file), [])

    transfer_update_file  = data_dir.join('itemmst_update_1.gz')
    transfer_delete_file  = data_dir.join('itemmst_delete_1.gz')

    # execute
    with contextlib.nested (
        _patches_with_default_action(func_init_config),
        _patches_filename(data_dir)
    ) as (mocks, mocks_fn):
        m_s3get, m_itemconv, m_sftp, m_s3put = mocks

        result = []
        _target().create_transfer_files(str(data_file), result)

    # assert
    assert result == [str(transfer_update_file), str(transfer_delete_file)]
    _assert_py_path_file(transfer_update_file, [])
    _assert_py_path_file(transfer_delete_file, [])

def test_transform_update_format():
    row = {
            'pid'               : 'rtoaster',
            'sid'               : '9999',
            'code'              : 'Item_1628',
            'name'              : '商品名1628',
            'img_url'           : 'http://stroustrup.brainpad.co.jp/img/item/mItem_1628.jpg',
            'link_url'          : 'http://stroustrup.brainpad.co.jp/c/itemDetail/itemDetailPage.html?itemCode=Item_1628',
            'price'             : '<br>1,500<br>',
            'description'       : '<b>価値</b>：8<br>グループ名：GROUP_K</a><br> カテゴリ名：C06</a><br> カテゴリ名：かてごり1</a><br> カテゴリ名：カテ2</a><br>',
            'status'            : '0',
            'last_modified_date': '2013-09-06 15:20:53'
          }

    with patch('lib.util.code_to_long_id') as m_itemconv:
        m_itemconv.side_effect = _convert_item_code

        result = _target().transform_update_format(row)
        assert result == [
                            '9999',
                            1628,
                            '商品名1628',
                            'http://stroustrup.brainpad.co.jp/img/item/mItem_1628.jpg',
                            'http://stroustrup.brainpad.co.jp/c/itemDetail/itemDetailPage.html?itemCode=Item_1628',
                            '<br>1,500<br>',
                            '<b>価値</b>：8<br>グループ名：GROUP_K</a><br> カテゴリ名：C06</a><br> カテゴリ名：かてごり1</a><br> カテゴリ名：カテ2</a><br>',
                            'advertiser_id_9999'
                         ]

def test_transform_update_format_over_length():
    row = {
            'pid'               : 'rtoaster',
            'sid'               : '9999',
            'code'              : 'Item_1628',
            'name'              : 'a' * 100,
            'img_url'           : 'b' * 2000,
            'link_url'          : 'c' * 2000,
            'price'             : 'd' * 100,
            'description'       : 'e' * 200,
            'status'            : '0',
            'last_modified_date': '2013-09-06 15:20:53'
          }

    with patch('lib.util.code_to_long_id') as m_itemconv:
        m_itemconv.side_effect = _convert_item_code

        result = _target().transform_update_format(row)
        assert result[2] == 'a' * 49 + '…'
        assert result[3] == 'b' * 1000
        assert result[4] == 'c' * 1000
        assert result[5] == 'd' * 49 + '…'
        assert result[6] == 'e' * 99 +  '…'

def test_transform_delete_format():
    row = {
            'pid'               : 'rtoaster',
            'sid'               : '9999',
            'code'              : 'Item_1628',
            'name'              : '商品名1628',
            'img_url'           : 'http://stroustrup.brainpad.co.jp/img/item/mItem_1628.jpg',
            'link_url'          : 'http://stroustrup.brainpad.co.jp/c/itemDetail/itemDetailPage.html?itemCode=Item_1628',
            'price'             : '<br>1,500<br>',
            'description'       : '<b>価値</b>：8<br>グループ名：GROUP_K</a><br> カテゴリ名：C06</a><br> カテゴリ名：かてごり1</a><br> カテゴリ名：カテ2</a><br>',
            'status'            : '0',
            'last_modified_date': '2013-09-06 15:20:53'
          }

    with patch('lib.util.code_to_long_id') as m_itemconv:
        m_itemconv.side_effect = _convert_item_code

        result = _target().transform_delete_format(row)
        assert result == ['9999', 1628, 'advertiser_id_9999']

def test_send_transformed_files():
    with patch('lib.sftputil.SftpUtil.__enter__') as m_sftp:
        result = _target().send_transformed_files(['item1.gz', 'item2.gz'])

        assert result
        assert m_sftp.call_count == 1
        assert m_sftp.return_value.put.call_count == 2
        assert m_sftp.return_value.put.call_args_list[0][0] == ('item1.gz', '/receive')
        assert m_sftp.return_value.put.call_args_list[1][0] == ('item2.gz', '/receive')
        assert m_sftp.return_value.touch.call_count == 2
        assert m_sftp.return_value.touch.call_args_list[0][0] == ('/receive/item1.gz.completed',)
        assert m_sftp.return_value.touch.call_args_list[1][0] == ('/receive/item2.gz.completed',)

def test_send_transformed_files_failed():
    with patch('lib.sftputil.SftpUtil.__enter__') as m_sftp:
        m_sftp.return_value.put.side_effect = Exception()

        result = _target().send_transformed_files(['item1.gz', 'item2.gz'])

        assert not result
        assert m_sftp.call_count == 1
        assert m_sftp.return_value.put.call_count == 1
        assert m_sftp.return_value.put.call_args_list[0][0] == ('item1.gz', '/receive')
        assert m_sftp.return_value.touch.call_count == 0

def test_execute(func_init_config, data_dir):

    # prepare
    data_file = data_dir.join('itemmst_20131001201510_9999.csv.gz')
    _create_gzip(str(data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2, CSV_DATA3, CSV_DATA4, CSV_DATA5])

    transfer_update_file  = data_dir.join('itemmst_update_1.gz')
    transfer_delete_file  = data_dir.join('itemmst_delete_1.gz')

    # execute
    with contextlib.nested (
        _patches_with_default_action(func_init_config),
        _patches_filename(data_dir)
    ) as (mocks, mocks_fn):
        m_s3get, m_itemconv, m_sftp, m_s3put = mocks

        result = _target().execute()

    # assert
    assert result.is_ok()
    assert result.counter['item.rtoaster.update'] == 3
    assert result.counter['item.rtoaster.delete'] == 2
    assert data_file.check(exists=0)
    assert transfer_update_file.check(exists=0)
    assert transfer_delete_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3_BUCKET, S3_PATH, str(data_dir))

    assert m_itemconv.call_count == 5
    assert m_itemconv.call_args_list[0][0] == (u'Item_1000',)
    assert m_itemconv.call_args_list[1][0] == (u'Item_2000',)
    assert m_itemconv.call_args_list[2][0] == (u'Item_3000',)
    assert m_itemconv.call_args_list[3][0] == (u'Item_4000',)
    assert m_itemconv.call_args_list[4][0] == (u'Item_5000',)

    assert m_sftp.call_count == 1
    assert m_sftp.return_value.put.call_count == 2
    assert m_sftp.return_value.put.call_args_list[0][0] == (str(transfer_update_file), '/receive')
    assert m_sftp.return_value.put.call_args_list[1][0] == (str(transfer_delete_file), '/receive')
    assert m_sftp.return_value.touch.call_count == 2
    assert m_sftp.return_value.touch.call_args_list[0][0] == ('/receive/itemmst_update_1.gz.completed',)
    assert m_sftp.return_value.touch.call_args_list[1][0] == ('/receive/itemmst_delete_1.gz.completed',)

    assert m_s3put.call_count == 2
    assert m_s3put.call_args_list[0][0] == (str(transfer_update_file), S3_BUCKET, S3_DIR_CONV)
    assert m_s3put.call_args_list[1][0] == (str(transfer_delete_file), S3_BUCKET, S3_DIR_CONV)

def test_execute_s3file_notfound(func_init_config, data_dir):

    # prepare
    data_file = data_dir.join('itemmst_20131001201510_9999.csv.gz')
    _create_gzip(str(data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2, CSV_DATA3, CSV_DATA4, CSV_DATA5])

    transfer_update_file  = data_dir.join('itemmst_update_1.gz')
    transfer_delete_file  = data_dir.join('itemmst_delete_1.gz')

    # execute
    with contextlib.nested (
        _patches_with_default_action(func_init_config),
        _patches_filename(data_dir)
    ) as (mocks, mocks_fn):
        m_s3get, m_itemconv, m_sftp, m_s3put = mocks
        m_s3get.return_value = None

        result = _target().execute()

    # assert
    assert result.is_ng()
    assert result.counter['item.rtoaster.update'] == 0
    assert result.counter['item.rtoaster.delete'] == 0
    assert transfer_update_file.check(exists=0)
    assert transfer_delete_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3_BUCKET, S3_PATH, str(data_dir))

    assert m_itemconv.call_count                == 0
    assert m_sftp.call_count                    == 0
    assert m_sftp.return_value.put.call_count   == 0
    assert m_sftp.return_value.touch.call_count == 0
    assert m_s3put.call_count                   == 0

def test_execute_sftp_error(func_init_config, data_dir):

    # prepare
    data_file = data_dir.join('itemmst_20131001201510_9999.csv.gz')
    _create_gzip(str(data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2, CSV_DATA3, CSV_DATA4, CSV_DATA5])

    transfer_update_file  = data_dir.join('itemmst_update_1.gz')
    transfer_delete_file  = data_dir.join('itemmst_delete_1.gz')

    # execute
    with contextlib.nested (
        _patches_with_default_action(func_init_config),
        _patches_filename(data_dir)
    ) as (mocks, mocks_fn):
        m_s3get, m_itemconv, m_sftp, m_s3put = mocks
        m_sftp.side_effect = Exception()

        result = _target().execute()

    # assert
    assert result.is_ng()
    assert result.counter['item.rtoaster.update'] == 3
    assert result.counter['item.rtoaster.delete'] == 2
    assert data_file.check(exists=0)
    assert transfer_update_file.check(exists=0)
    assert transfer_delete_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3_BUCKET, S3_PATH, str(data_dir))

    assert m_itemconv.call_count == 5
    assert m_itemconv.call_args_list[0][0] == (u'Item_1000',)
    assert m_itemconv.call_args_list[1][0] == (u'Item_2000',)
    assert m_itemconv.call_args_list[2][0] == (u'Item_3000',)
    assert m_itemconv.call_args_list[3][0] == (u'Item_4000',)
    assert m_itemconv.call_args_list[4][0] == (u'Item_5000',)

    assert m_sftp.call_count                    == 1
    assert m_sftp.return_value.put.call_count   == 0
    assert m_sftp.return_value.touch.call_count == 0
    assert m_s3put.call_count                   == 0

def test_execute_unexpected_error(func_init_config, data_dir):

    # prepare
    data_file = data_dir.join('itemmst_20131001201510_9999.csv.gz')
    _create_gzip(str(data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2, CSV_DATA3, CSV_DATA4, CSV_DATA5])

    transfer_update_file  = data_dir.join('itemmst_update_1.gz')
    transfer_delete_file  = data_dir.join('itemmst_delete_1.gz')

    # execute
    with contextlib.nested (
        _patches_with_default_action(func_init_config),
        _patches_filename(data_dir)
    ) as (mocks, mocks_fn):
        m_s3get, m_itemconv, m_sftp, m_s3put = mocks
        m_itemconv.return_value = None
        m_itemconv.side_effect  = Exception()

        result = _target().execute()

    # assert
    assert result.is_ng()
    assert result.counter['item.rtoaster.update'] == 0
    assert result.counter['item.rtoaster.delete'] == 0
    assert data_file.check(exists=0)
    assert transfer_update_file.check(exists=0)
    assert transfer_delete_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3_BUCKET, S3_PATH, str(data_dir))

    assert m_itemconv.call_count == 1
    assert m_itemconv.call_args_list[0][0] == (u'Item_1000',)

    assert m_sftp.call_count                    == 0
    assert m_sftp.return_value.put.call_count   == 0
    assert m_sftp.return_value.touch.call_count == 0
    assert m_s3put.call_count                   == 0
