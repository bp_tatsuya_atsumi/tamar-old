# -*- coding: utf-8 -*-

from lib.transferer.transfer_result import TransferResult


ENV = 'dev'
SID = '9999'
PID = 'ma'
DATA_TYPE = 'segmentmst'

target = None

def setup_function(function):
    global target
    target = TransferResult(SID, PID, ENV, DATA_TYPE)

def test_ok():
    target.ok('ok message')
    assert target.result   == TransferResult.RESULT_OK
    assert target.messages == ['ok message']

def test_api_error():
    target.api_error('api error message')
    assert target.result   == TransferResult.RESULT_API_ERROR
    assert target.messages == ['api error message']

def test_ng():
    target.ng('ng message')
    assert target.result   == TransferResult.RESULT_NG
    assert target.messages == ['ng message']

def test_add_message():
    target.add_message('message1')
    target.add_message('message2')
    assert target.messages == ['message1', 'message2']

def test_is_ok():
    target.result = TransferResult.RESULT_OK
    assert target.is_ok()
    assert not target.is_api_error()
    assert not target.is_ng()

def test_is_api_error():
    target.result = TransferResult.RESULT_API_ERROR
    assert not target.is_ok()
    assert target.is_api_error()
    assert not target.is_ng()

def test_is_ng():
    target.result = TransferResult.RESULT_NG
    assert not target.is_ok()
    assert not target.is_api_error()
    assert target.is_ng()

def test_increment_counter():
    target.increment_counter('request')
    target.increment_counter('request', 10)
    assert target.counter['request'] == 11

def test_decrement_counter():
    target.decrement_counter('request')
    target.decrement_counter('request', 10)
    assert target.counter['request'] == -11

