# -*- coding: utf-8 -*-

import lib.transferer.rtfile as rtfile

import gzip
from mock import MagicMock


JSON_DATA1  = '{"pid": "rtoaster", "sid": "9999", "uid": "rt_uid1"}'
JSON_DATA2  = '{"pid": "rtoaster", "sid": "9999", "uid": "rt_uid2"}'
JSON_DATA3  = '{"pid": "rtoaster", "sid": "9999", "uid": "rt_uid3"'
JSON_DATA4  = '{"pid": "rtoaster", "sid": "9999"}'
JSON_DATA5  = '{"pid": "rtoaster", "sid": "9999", "uid": "rt_uid5"}'
JSON_DATA6  = ''

def _create_gzip(filename, lines):
    with gzip.open(filename, 'wb') as f:
        f.write('\n'.join(lines))

def test_mst_reader(tmpdir):
    def _assert_mst(reader):
        actual = []
        [actual.append(row) for row in reader]
        assert actual == [
                            {'id': '100', 'name': 'name100', 'description': 'note100'},
                            {'id': '200', 'name': 'name200', 'description': 'note200'},
                            {'id': '300', 'name': 'name300', 'description': None},
                            {'id': '400', 'name': 'name400', 'description': 'note400', None: ['extra400']},
                            {'id': '500', 'name': 'name500', 'description': 'note500'},
                         ]
        assert reader.line_count_all == 5

    CSV_HEADER = 'id,name,description'
    CSV_DATA1  = '100,name100,note100'
    CSV_DATA2  = '200,name200,note200'
    CSV_DATA3  = '300,name300'
    CSV_DATA4  = '400,name400,note400,extra400'
    CSV_DATA5  = '500,name500,note500'

    # prepare
    data_file = tmpdir.join('data.csv.gz')
    _create_gzip(str(data_file),
                 [CSV_HEADER, CSV_DATA1, CSV_DATA2, CSV_DATA3, CSV_DATA4, CSV_DATA5])

    # execute
    with rtfile.MstReader(str(data_file)) as reader:
        _assert_mst(reader)
    with rtfile.open_recommendmst(str(data_file)) as reader:
        _assert_mst(reader)
    with rtfile.open_itemmst(str(data_file)) as reader:
        _assert_mst(reader)

def test_mst_reader_segment(tmpdir):
    def _assert_mst(reader):
        actual = []
        [actual.append(row) for row in reader]
        assert actual == [
                            {'id': '100', 'name': 'name100', 'description': 'note100', 'ignore': ''},
                            {'id': '200', 'name': 'name200', 'description': 'note200', 'ignore': 'YTM_PLUS'},
                            {'id': '300', 'name': 'name300', 'description': '', 'ignore': ''},
                            {'id': '400', 'name': 'name400', 'description': 'note400', 'ignore': 'YTM_PLUS', None: ['extra400']},
                            {'id': '500', 'name': 'name500', 'description': 'note500', 'ignore': ''},
                            {'id': '600', 'name': 'name600', 'description': 'note600', 'ignore': 'YTM_PLUS'},
                            {'id': '700', 'name': 'name700', 'description': '', 'ignore': ''}
                         ]
        assert reader.line_count_all == 7

    CSV_HEADER = 'id,name,description,ignore'
    CSV_DATA1  = '100,name100,note100,'
    CSV_DATA2  = '200,name200,note200,YTM_PLUS'
    CSV_DATA3  = '300,name300,,'
    CSV_DATA4  = '400,name400,note400,YTM_PLUS,extra400'
    CSV_DATA5  = '500,name500,note500,'
    CSV_DATA6  = '600,name600,note600,YTM_PLUS'
    CSV_DATA7  = '700,name700,,'

    # prepare
    data_file = tmpdir.join('data.csv.gz')
    _create_gzip(str(data_file),
                 [CSV_HEADER, CSV_DATA1, CSV_DATA2, CSV_DATA3, CSV_DATA4, CSV_DATA5, CSV_DATA6, CSV_DATA7])

    # execute
    with rtfile.open_segmentmst(str(data_file)) as reader:
        _assert_mst(reader)

def _assert_data_oldest_user(reader, mock_mapping_table):
    actual = []
    [actual.append(row) for row in reader]
    assert actual == [
                        {'pid': 'rtoaster', 'sid': '9999', 'uid': 'rt_uid1', 'mapped_uid': 'pt_uid1'},
                        {'pid': 'rtoaster', 'sid': '9999', 'uid': 'rt_uid2', 'mapped_uid': 'pt_uid2'},
                     ]
    assert reader.line_count_all      == 5
    assert reader.line_count_invalid  == 2
    assert reader.line_count_unmapped == 1
    assert reader.line_count_mapped   == 2
    assert reader.invalid_line_nums   == [3, 4]
    assert reader.invalid_lines       == [JSON_DATA3.rstrip(), JSON_DATA4.rstrip()]

    mock_func = mock_mapping_table.select_latest_mapping_partner_uid_by_user_id_until_n_days
    assert mock_func.call_count == 3
    assert mock_func.call_args_list[0][0] == ('rt_uid1', 500)
    assert mock_func.call_args_list[1][0] == ('rt_uid2', 500)
    assert mock_func.call_args_list[2][0] == ('rt_uid5', 500)

def _create_mapping_table_mock_oldest_user():
    mock = MagicMock()
    mock.select_latest_mapping_partner_uid_by_user_id_until_n_days.side_effect = [
        'pt_uid1', 'pt_uid2', None
    ]
    return mock

def _assert_data_all_user(reader, mock_mapping_table):
    actual = []
    [actual.append(row) for row in reader]
    assert actual == [
                        {'pid': 'rtoaster', 'sid': '9999', 'uid': 'rt_uid1', 'mapped_uids': ['pt_uid1-1']},
                        {'pid': 'rtoaster', 'sid': '9999', 'uid': 'rt_uid2', 'mapped_uids': ['pt_uid2-1', 'pt_uid2-2']},
                     ]
    assert reader.line_count_all      == 5
    assert reader.line_count_invalid  == 2
    assert reader.line_count_unmapped == 1
    assert reader.line_count_mapped   == 3
    assert reader.invalid_line_nums   == [3, 4]
    assert reader.invalid_lines       == [JSON_DATA3.rstrip(), JSON_DATA4.rstrip()]

    mock_func = mock_mapping_table.select_partner_uids_by_user_id
    assert mock_func.call_count == 3
    assert mock_func.call_args_list[0][0] == ('rt_uid1',)
    assert mock_func.call_args_list[1][0] == ('rt_uid2',)
    assert mock_func.call_args_list[2][0] == ('rt_uid5',)

def _create_mapping_table_mock_all_user():
    mock = MagicMock()
    mock.select_partner_uids_by_user_id.side_effect = [
        ['pt_uid1-1'],
        ['pt_uid2-1', 'pt_uid2-2'],
        None
    ]
    return mock

def test_data_reader(tmpdir):

    # prepare
    data_file = tmpdir.join('data.json.gz')
    _create_gzip(str(data_file),
                 [JSON_DATA1, JSON_DATA2, JSON_DATA3, JSON_DATA4, JSON_DATA5, JSON_DATA6])

    # execute(oldest user)
    mock = _create_mapping_table_mock_oldest_user()
    with rtfile.DataReader(str(data_file), mock) as reader:
        _assert_data_oldest_user(reader, mock)
    mock = _create_mapping_table_mock_oldest_user()
    with rtfile.open_segmentdata(str(data_file), mock) as reader:
        _assert_data_oldest_user(reader, mock)
    mock = _create_mapping_table_mock_oldest_user()
    with rtfile.open_mailsegmentdata(str(data_file), mock) as reader:
        _assert_data_oldest_user(reader, mock)
    mock = _create_mapping_table_mock_oldest_user()
    with rtfile.open_conversion(str(data_file), mock) as reader:
        _assert_data_oldest_user(reader, mock)
    mock = _create_mapping_table_mock_oldest_user()
    with rtfile.open_recommenddata(str(data_file), mock) as reader:
        _assert_data_oldest_user(reader, mock)

    # execute(all user)
    mock = _create_mapping_table_mock_all_user()
    with rtfile.DataReader(str(data_file), mock, oldest_user=False) as reader:
        _assert_data_all_user(reader, mock)
    mock = _create_mapping_table_mock_all_user()
    with rtfile.open_segmentdata(str(data_file), mock, oldest_user=False) as reader:
        _assert_data_all_user(reader, mock)
    mock = _create_mapping_table_mock_all_user()
    with rtfile.open_mailsegmentdata(str(data_file), mock, oldest_user=False) as reader:
        _assert_data_all_user(reader, mock)
    mock = _create_mapping_table_mock_all_user()
    with rtfile.open_conversion(str(data_file), mock, oldest_user=False) as reader:
        _assert_data_all_user(reader, mock)
    mock = _create_mapping_table_mock_all_user()
    with rtfile.open_recommenddata(str(data_file), mock, oldest_user=False) as reader:
        _assert_data_all_user(reader, mock)

