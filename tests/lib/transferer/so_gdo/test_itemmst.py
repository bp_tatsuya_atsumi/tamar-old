# -*- coding: utf-8 -*-

from lib.transferer.so_gdo.itemmst import ItemMstTransferer

import contextlib
import gzip
import os
from   mock import patch
import pytest


ENV = 'dev'
SID = '9999'
PID = 'so'

S3_BUCKET   = 'dev-transfer-data'
S3_PATH_RAW = '9999/item_mst/raw'
S3_PATH_CNV = '9999/item_mst/converted/so'

DATA1 = '{"aid":"rtoaster","sid":"0009","code":"236102","name":"松島チサンカントリークラブ 大郷コース","price":"","grouping":"宮城県"}'
DATA2 = '{"aid":"rtoaster","sid":"0009","code":"236202","name":"宮城野ゴルフクラブ","price":"","grouping":"宮城県"}'

@pytest.fixture
def data_dir(tmp_transfer_dir):
    return tmp_transfer_dir.mkdir(SID).mkdir(PID).mkdir('itemmst')

@contextlib.contextmanager
def _patches_with_default_action(func_init_config, data_dir):
    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('lib.s3util.S3Util.download_single_file_order_by_filename'),
        patch('lib.lockfile.LastUpdateFile.is_latest'),
        patch('lib.transferer.so.soutil.create_transfer_filename'),
        patch('lib.sftputil.put'),
        patch('lib.s3util.S3Util.upload_single_file'),
    ) as (m_config, m_s3get, m_lastupdfile, m_filename, m_sftpput, m_s3put):
        m_s3get.return_value       = {'key': '9999/item_mst/raw/itemmst_20131001201510_9999.csv.gz'}
        m_lastupdfile.return_value = False
        m_filename.return_value    = os.path.join(str(data_dir), 'transfer_file.json.gz')

        yield (m_s3get, m_lastupdfile, m_filename, m_sftpput, m_s3put)

def _target():
    return ItemMstTransferer(SID, PID, ENV, S3_BUCKET, S3_PATH_RAW)

def _create_gzip(file, lines):
    with gzip.open(file, 'wb') as f:
        f.write('\n'.join(lines))

def test_create_transfer_file(func_init_config, data_dir):

    # prepare
    data_file     = data_dir.join('itemmst_20131001201510_9999.csv.gz')
    transfer_file = data_dir.join('transfer_file.json.gz')

    # execute
    _create_gzip(str(data_file), [
        '{"aid":"rtoaster","sid":"0009","code":"236102","name":"松島チサンカントリークラブ 大郷コース","price":"","grouping":"宮城県"}',
        '{"aid":"rtoaster","sid":"0009","code":"236202","name":"宮城野ゴルフクラブ","price":"","grouping":"宮城県"}'
    ])
    _target()._create_transfer_file(str(data_file), str(transfer_file))
    transfer_file.read() == '''{"aid":"brainpad","sid":"1234123456789012","code":"236102","name":"松島チサンカントリークラブ 大郷コース","price":"","grouping":"宮城県"}
{"aid":"brainpad","sid":"1234123456789012","code":"236202","name":"宮城野ゴルフクラブ","price":"","grouping":"宮城県"}'''

    _create_gzip(str(data_file), [
        '{"aid" : "rtoaster","sid"   :   "0009","code":"236102","name":"松島チサンカントリークラブ 大郷コース","price":"","grouping":"宮城県"}',
        '{"aid" : "rtoaster","sid"   :   "0009","code":"236202","name":"宮城野ゴルフクラブ","price":"","grouping":"宮城県"}'
     ])
    _target()._create_transfer_file(str(data_file), str(transfer_file))
    transfer_file.read() == '''{"aid":"brainpad","sid":"1234123456789012","code":"236102","name":"松島チサンカントリークラブ 大郷コース","price":"","grouping":"宮城県"}
{"aid":"brainpad","sid":"1234123456789012","code":"236202","name":"宮城野ゴルフクラブ","price":"","grouping":"宮城県"}'''


def test_send_transformed_file():
    with patch('lib.sftputil.put') as m_sftpput:
        result = _target()._send_transformed_file('item1.gz')

        assert result
        assert m_sftpput.call_count == 1
        assert m_sftpput.call_args_list[0][1]['host']       == 'sftp-s.c-ovn.jp'
        assert m_sftpput.call_args_list[0][1]['port']       == 115
        assert m_sftpput.call_args_list[0][1]['username']   == 'rtoaster9999'
        assert m_sftpput.call_args_list[0][1]['password']   == 'rtoaster9999'
        assert m_sftpput.call_args_list[0][1]['local_file'] == 'item1.gz'
        assert m_sftpput.call_args_list[0][1]['remote_dir'] == '/receive/so/content'
        assert m_sftpput.call_args_list[0][1]['rename']     == True

def test_send_transformed_file_failed():
    with patch('lib.sftputil.put') as m_sftpput:
        m_sftpput.side_effect = Exception()

        result = _target()._send_transformed_file('item1.gz')

        assert not result
        assert m_sftpput.call_count == 1
        assert m_sftpput.call_args_list[0][1]['host']       == 'sftp-s.c-ovn.jp'
        assert m_sftpput.call_args_list[0][1]['port']       == 115
        assert m_sftpput.call_args_list[0][1]['username']   == 'rtoaster9999'
        assert m_sftpput.call_args_list[0][1]['password']   == 'rtoaster9999'
        assert m_sftpput.call_args_list[0][1]['local_file'] == 'item1.gz'
        assert m_sftpput.call_args_list[0][1]['remote_dir'] == '/receive/so/content'
        assert m_sftpput.call_args_list[0][1]['rename']     == True

def test_execute(func_init_config, data_dir):

    # prepare
    data_file     = data_dir.join('itemmst_20131001201510_9999.csv.gz')
    transfer_file = data_dir.join('transfer_file.json.gz')

    _create_gzip(str(data_file), [DATA1, DATA2])

    # execute
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_lastupdfile, m_filename, m_sftpput, m_s3put = mocks

        result = _target().execute()

    # assert
    assert result.is_ok()
    assert result.counter['item.rtoaster'] == 0
    assert data_file.check(exists=0)
    assert transfer_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3_BUCKET, S3_PATH_RAW, str(data_dir))

    assert m_lastupdfile.call_count == 1
    assert m_lastupdfile.call_args_list[0][0] == ('itemmst_20131001201510_9999.csv.gz',)

    assert m_filename.call_count == 1
    assert m_filename.call_args_list[0][0]             == (str(data_dir), 'brainpad', '1234123456789012')
    assert m_filename.call_args_list[0][1]['filetype'] == 'item'

    assert m_sftpput.call_count == 1
    assert m_sftpput.call_args_list[0][1]['host']       == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[0][1]['port']       == 115
    assert m_sftpput.call_args_list[0][1]['username']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['password']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['local_file'] == str(transfer_file)
    assert m_sftpput.call_args_list[0][1]['remote_dir'] == '/receive/so/content'
    assert m_sftpput.call_args_list[0][1]['rename']     == True

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(transfer_file), S3_BUCKET, S3_PATH_CNV)

def test_execute_s3file_notfound(func_init_config, data_dir):

    # execute
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_lastupdfile, m_filename, m_sftpput, m_s3put = mocks
        m_s3get.return_value = None

        result = _target().execute()

    # assert
    assert result.is_ng()
    assert result.counter['item.rtoaster'] == 0

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3_BUCKET, S3_PATH_RAW, str(data_dir))

    assert m_lastupdfile.call_count == 0
    assert m_filename.call_count    == 0
    assert m_sftpput.call_count     == 0
    assert m_s3put.call_count       == 0

def test_execute_already_transfered(func_init_config, data_dir):

    # prepare
    data_file     = data_dir.join('itemmst_20131001201510_9999.csv.gz')
    transfer_file = data_dir.join('transfer_file.json.gz')

    _create_gzip(str(data_file), [DATA1, DATA2])

    # execute
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_lastupdfile, m_filename, m_sftpput, m_s3put = mocks
        m_lastupdfile.return_value = True

        result = _target().execute()

    # assert
    assert result.is_ok()
    assert result.counter['item.rtoaster'] == 0
    assert data_file.check(exists=0)
    assert transfer_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3_BUCKET, S3_PATH_RAW, str(data_dir))

    assert m_lastupdfile.call_count == 1
    assert m_lastupdfile.call_args_list[0][0] == ('itemmst_20131001201510_9999.csv.gz',)

    assert m_filename.call_count == 0
    assert m_sftpput.call_count  == 0
    assert m_s3put.call_count    == 0

def test_execute_sftp_error(func_init_config, data_dir):

    # prepare
    data_file     = data_dir.join('itemmst_20131001201510_9999.csv.gz')
    transfer_file = data_dir.join('transfer_file.json.gz')

    _create_gzip(str(data_file), [DATA1, DATA2])

    # execute
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_lastupdfile, m_filename, m_sftpput, m_s3put = mocks
        m_sftpput.side_effect = Exception()

        result = _target().execute()

    # assert
    assert result.is_ng()
    assert result.counter['item.rtoaster'] == 0
    assert data_file.check(exists=0)
    assert transfer_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3_BUCKET, S3_PATH_RAW, str(data_dir))

    assert m_lastupdfile.call_count == 1
    assert m_lastupdfile.call_args_list[0][0] == ('itemmst_20131001201510_9999.csv.gz',)

    assert m_filename.call_count == 1
    assert m_filename.call_args_list[0][0]             == (str(data_dir), 'brainpad', '1234123456789012')
    assert m_filename.call_args_list[0][1]['filetype'] == 'item'

    assert m_sftpput.call_count == 1
    assert m_sftpput.call_args_list[0][1]['host']       == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[0][1]['port']       == 115
    assert m_sftpput.call_args_list[0][1]['username']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['password']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['local_file'] == str(transfer_file)
    assert m_sftpput.call_args_list[0][1]['remote_dir'] == '/receive/so/content'
    assert m_sftpput.call_args_list[0][1]['rename']     == True

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(transfer_file), S3_BUCKET, S3_PATH_CNV)

def test_execute_unexpected_error(func_init_config, data_dir):

    # prepare
    data_file = data_dir.join('itemmst_20131001201510_9999.csv.gz')
    transfer_file = data_dir.join('transfer_file.json.gz')

    _create_gzip(str(data_file), [DATA1, DATA2])

    # execute
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_lastupdfile, m_filename, m_sftpput, m_s3put = mocks
        m_filename.side_effect = Exception()

        result = _target().execute()

    # assert
    assert result.is_ng()
    assert result.counter['item.rtoaster'] == 0
    assert data_file.check(exists=0)
    assert transfer_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3_BUCKET, S3_PATH_RAW, str(data_dir))

    assert m_lastupdfile.call_count == 1
    assert m_lastupdfile.call_args_list[0][0] == ('itemmst_20131001201510_9999.csv.gz',)

    assert m_filename.call_count == 1
    assert m_filename.call_args_list[0][0]             == (str(data_dir), 'brainpad', '1234123456789012')
    assert m_filename.call_args_list[0][1]['filetype'] == 'item'

    assert m_sftpput.call_count == 0
    assert m_s3put.call_count   == 0
