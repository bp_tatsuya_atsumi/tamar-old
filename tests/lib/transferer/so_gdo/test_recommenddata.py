# -*- coding: utf-8 -*-

from lib.transferer.so_gdo.recommenddata import RecommendDataTransferer

import contextlib
import datetime
import gzip
import json
import os
from   mock import patch
import pytest
from   lib.transferer.exception import NoS3Files


ENV = 'dev'
SID = '9999'
PID = 'so'

S3BUCKET  = 'dev-transfer-data'
S3PATH    = '9999/recommend_list_data/raw/split/recommenddata.json.gz'
S3DIR_CNV = '9999/recommend_list_data/converted/so/split'
SEQNO     = 3

DATA1 = '{"aid": "rtoaster", "sid": "9999", "uid": "rt_uid1", "replace": {"price": "field4", "discount": "dct"}, "item_lists": {"basket": ["rtoaster", "l2mixer"], "history": ["rtoaster", "l2mixer"]}}'
DATA2 = '{"aid": "rtoaster", "sid": "9999", "uid": "rt_uid2", "replace": {"price": "field4", "discount": "dct"}, "item_lists": {"basket": ["crossoven", "semanticfinder"], "history": ["crossoven", "semanticfinder"]}}'

class CurrentDatetime(datetime.datetime):
    @classmethod
    def now(cls):
        return cls(2013, 10, 19, 23, 40, 59, 0)

@pytest.fixture
def data_dir(tmp_transfer_dir):
    return tmp_transfer_dir.mkdir(SID).mkdir(PID).mkdir('recommenddata')

@contextlib.contextmanager
def _patches_with_default_action(func_init_config, data_dir):
    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('lib.s3util.S3Util.download_single_file'),
        patch('lib.transferer.so.soutil.create_transfer_filename'),
        patch('lib.mappingtable.MappingTable.select_partner_uids_by_user_id'),
        patch('lib.sftputil.put'),
        patch('lib.s3util.S3Util.upload_single_file'),
        patch('datetime.datetime', CurrentDatetime)
    ) as (m_config, m_s3get, m_filename, m_mapping, m_sftpput, m_s3put, m_now):
        m_s3get.return_value    = {'key': '9999/recommend_list_data/raw/recommenddata.json.gz'}
        m_mapping.side_effect   = _convert_uid
        m_filename.return_value = os.path.join(str(data_dir), 'transfer_file-003.json.gz')

        yield (m_s3get, m_filename, m_mapping, m_sftpput, m_s3put, m_now)

def _convert_uid(*args):
    tbl = {
        'rt_uid1': ['pt_uid1'],
        'rt_uid2': ['pt_uid2'],
    }
    return tbl.get(args[0])

def _target():
    option = {'seqno': SEQNO}
    return RecommendDataTransferer(SID, PID, ENV, S3BUCKET, S3PATH, **option)

def _create_gzip(file, lines):
    with gzip.open(file, 'wb') as f:
        f.write('\n'.join(lines))

def test_download_s3file(func_init_config, data_dir):
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_filename, m_mapping, m_sftp, m_s3put, m_now = mocks
        m_s3get.side_effect  = None
        m_s3get.return_value = {
                                    'key'           : '/path/to/target_file.json.gz',
                                    'is_dir'        : False,
                                    'is_file'       : True,
                                    'size'          : 100,
                                    'last_modified' : '2013-10-01%00:00:00.000Z'
                               }

        result = _target()._download_s3file()
        assert result == (
                            os.path.join(str(data_dir), 'target_file.json.gz'),
                            'target_file.json.gz'
                         )

def test_download_s3file_not_exits(func_init_config, data_dir):
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_filename, m_mapping, m_sftpput, m_s3put, m_now = mocks
        m_s3get.side_effect  = None
        m_s3get.return_value = None

        with pytest.raises(NoS3Files):
            _target()._download_s3file()

def test_transform_transfer_format():
    row = {
            u'aid'         : u'rtoaster',
            u'sid'         : u'9999',
            u'uid'         : u'rt_uid1',
            u'mapped_uids' : [u'pt_uid1'],
            u'replace'     : {
                                 u'price'    : u'field4',
                                 u'discount' : u'dct'
                            },
            u'item_lists'  : {
                                 u'basket'  : [u'rtoaster',  u'l2mixer'],
                                 u'history' : [u'crossoven', u'semanticfinder'],
                            }
          }
    result = _target()._transform_transfer_format(row)
    assert json.loads(result) == json.loads('{"aid":"brainpad","sid":"1234123456789012","uid":"pt_uid1","replace":{"price":"field4","discount":"dct"},"item_lists":{"basket":["rtoaster","l2mixer"],"history":["crossoven","semanticfinder"]}}')

def test_send_transfer_file():
    with patch('lib.sftputil.put') as m_sftpput:
        result = _target()._send_transfer_file('item1.gz')

        assert result
        assert m_sftpput.call_count == 1
        assert m_sftpput.call_args_list[0][1]['host']       == 'sftp-s.c-ovn.jp'
        assert m_sftpput.call_args_list[0][1]['port']       == 115
        assert m_sftpput.call_args_list[0][1]['username']   == 'rtoaster9999'
        assert m_sftpput.call_args_list[0][1]['password']   == 'rtoaster9999'
        assert m_sftpput.call_args_list[0][1]['local_file'] == 'item1.gz'
        assert m_sftpput.call_args_list[0][1]['remote_dir'] == '/receive/so/extra'
        assert m_sftpput.call_args_list[0][1]['rename']     == True

def test_send_transfer_file_failed():
    with patch('lib.sftputil.put') as m_sftpput:
        m_sftpput.side_effect = Exception()

        result = _target()._send_transfer_file('item1.gz')

        assert not result
        assert m_sftpput.call_count == 1
        assert m_sftpput.call_args_list[0][1]['host']       == 'sftp-s.c-ovn.jp'
        assert m_sftpput.call_args_list[0][1]['port']       == 115
        assert m_sftpput.call_args_list[0][1]['username']   == 'rtoaster9999'
        assert m_sftpput.call_args_list[0][1]['password']   == 'rtoaster9999'
        assert m_sftpput.call_args_list[0][1]['local_file'] == 'item1.gz'
        assert m_sftpput.call_args_list[0][1]['remote_dir'] == '/receive/so/extra'
        assert m_sftpput.call_args_list[0][1]['rename']     == True

def test_execute(func_init_config, data_dir):

    # prepare
    data_file     = data_dir.join('recommenddata.json.gz')
    transfer_file = data_dir.join('transfer_file-003.json.gz')

    _create_gzip(str(data_file),   [DATA1, DATA2])

    # execute
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_filename, m_mapping, m_sftpput, m_s3put, m_now = mocks

        result = _target().execute()

    # assert
    assert result.is_ok()
    assert result.counter['user.rtoaster'] == 2
    assert result.counter['user.mapped']   == 2
    assert result.counter['user.unmapped'] == 0
    assert result.counter['user.invalid']  == 0
    assert data_file.check(exists=0)
    assert transfer_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3PATH, str(data_dir))

    assert m_filename.call_count == 1
    assert m_filename.call_args_list[0][0]                 == (str(data_dir), 'brainpad', '1234123456789012')
    assert m_filename.call_args_list[0][1]['filedatetime'] == datetime.datetime(2013, 10, 19, 23, 40, 59, 0)
    assert m_filename.call_args_list[0][1]['filetype']     == 'recommend'
    assert m_filename.call_args_list[0][1]['seqno']        == 3

    assert m_mapping.call_count == 2
    assert m_mapping.call_args_list[0][0] == (u'rt_uid1',)
    assert m_mapping.call_args_list[1][0] == (u'rt_uid2',)

    assert m_sftpput.call_count == 1
    assert m_sftpput.call_args_list[0][1]['host']       == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[0][1]['port']       == 115
    assert m_sftpput.call_args_list[0][1]['username']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['password']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['local_file'] == str(transfer_file)
    assert m_sftpput.call_args_list[0][1]['remote_dir'] == '/receive/so/extra'
    assert m_sftpput.call_args_list[0][1]['rename']     == True

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(transfer_file), S3BUCKET, S3DIR_CNV)

def test_execute_s3data_notfound(func_init_config, data_dir):

    # execute
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_filename, m_mapping, m_sftpput, m_s3put, m_now = mocks
        m_s3get.side_effect = [None]

        result = _target().execute()

    # assert
    assert result.is_ng()
    assert result.counter['user.rtoaster'] == 0
    assert result.counter['user.mapped']   == 0
    assert result.counter['user.unmapped'] == 0
    assert result.counter['user.invalid']  == 0

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3PATH, str(data_dir))

    assert m_filename.call_count == 0
    assert m_mapping.call_count  == 0
    assert m_s3put.call_count    == 0

def test_execute_sftp_error(func_init_config, data_dir):

    # prepare
    data_file     = data_dir.join('recommenddata.json.gz')
    transfer_file = data_dir.join('transfer_file-003.json.gz')

    _create_gzip(str(data_file),   [DATA1, DATA2])

    # execute
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_filename, m_mapping, m_sftpput, m_s3put, m_now = mocks
        m_sftpput.side_effect = Exception()

        result = _target().execute()

    # assert
    assert result.is_ng()
    assert result.counter['user.rtoaster'] == 2
    assert result.counter['user.mapped']   == 2
    assert result.counter['user.unmapped'] == 0
    assert result.counter['user.invalid']  == 0
    assert data_file.check(exists=0)
    assert transfer_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3PATH, str(data_dir))

    assert m_filename.call_count == 1
    assert m_filename.call_args_list[0][0]                 == (str(data_dir), 'brainpad', '1234123456789012')
    assert m_filename.call_args_list[0][1]['filedatetime'] == datetime.datetime(2013, 10, 19, 23, 40, 59, 0)
    assert m_filename.call_args_list[0][1]['filetype']     == 'recommend'
    assert m_filename.call_args_list[0][1]['seqno']        == 3

    assert m_mapping.call_count == 2
    assert m_mapping.call_args_list[0][0] == (u'rt_uid1',)
    assert m_mapping.call_args_list[1][0] == (u'rt_uid2',)

    assert m_sftpput.call_count == 1
    assert m_sftpput.call_args_list[0][1]['host']       == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[0][1]['port']       == 115
    assert m_sftpput.call_args_list[0][1]['username']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['password']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['local_file'] == str(transfer_file)
    assert m_sftpput.call_args_list[0][1]['remote_dir'] == '/receive/so/extra'
    assert m_sftpput.call_args_list[0][1]['rename']     == True

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(transfer_file), S3BUCKET, S3DIR_CNV)

def test_execute_unexpected_error(func_init_config, data_dir):

    # prepare
    data_file     = data_dir.join('recommenddata.json.gz')
    transfer_file = data_dir.join('transfer_file-003.json.gz')

    _create_gzip(str(data_file),   [DATA1, DATA2])

    # execute
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_filename, m_mapping, m_sftpput, m_s3put, m_now = mocks
        m_mapping.side_effect = Exception()

        result = _target().execute()

    # assert
    assert result.is_ng()
    assert result.counter['user.rtoaster'] == 0
    assert result.counter['user.mapped']   == 0
    assert result.counter['user.unmapped'] == 0
    assert result.counter['user.invalid']  == 0
    assert data_file.check(exists=0)
    assert transfer_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3PATH, str(data_dir))

    assert m_filename.call_count == 1
    assert m_filename.call_args_list[0][0]                 == (str(data_dir), 'brainpad', '1234123456789012')
    assert m_filename.call_args_list[0][1]['filedatetime'] == datetime.datetime(2013, 10, 19, 23, 40, 59, 0)
    assert m_filename.call_args_list[0][1]['filetype']     == 'recommend'
    assert m_filename.call_args_list[0][1]['seqno']        == 3

    assert m_mapping.call_count == 1
    assert m_mapping.call_args_list[0][0] == (u'rt_uid1',)

    assert m_sftpput.call_count == 0
    assert m_s3put.call_count   == 0
