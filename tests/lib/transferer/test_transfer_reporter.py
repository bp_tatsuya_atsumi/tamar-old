# -*- coding: utf-8 -*-

from collections import Counter
import pytest
from lib.transferer.transfer_reporter import TransferReporter
from lib.transferer.transfer_result import TransferResult


def _init_transfer_reporter():
    result = TransferResult('9999', 'bp1', 'dev', 'segmentmst')
    result.increment_counter('test', 10)
    return TransferReporter(result)

def _is_send_target(reporter, cond):
    reporter.SEND_CONDS['dev']['segmentmst'] = cond
    return reporter.is_send_target()

def test_is_send_target():
    reporter = _init_transfer_reporter()
    reporter.result = TransferResult.RESULT_OK
    assert _is_send_target(reporter, TransferReporter.SEND_COND_ALL)     == True
    assert _is_send_target(reporter, TransferReporter.SEND_COND_NOTZERO) == True
    assert _is_send_target(reporter, TransferReporter.SEND_COND_ERROR)   == False
    assert _is_send_target(reporter, TransferReporter.SEND_COND_NONE)    == False

def test_is_send_target_zero_counter():
    reporter = _init_transfer_reporter()
    reporter.result = TransferResult.RESULT_OK
    reporter.counter['test'] = 0
    assert _is_send_target(reporter, TransferReporter.SEND_COND_ALL)     == True
    assert _is_send_target(reporter, TransferReporter.SEND_COND_NOTZERO) == False
    assert _is_send_target(reporter, TransferReporter.SEND_COND_ERROR)   == False
    assert _is_send_target(reporter, TransferReporter.SEND_COND_NONE)    == False

def test_is_send_target_none_counter():
    reporter = _init_transfer_reporter()
    reporter.result = TransferResult.RESULT_OK
    reporter.counter = None
    assert _is_send_target(reporter, TransferReporter.SEND_COND_ALL)     == True
    assert _is_send_target(reporter, TransferReporter.SEND_COND_NOTZERO) == False
    assert _is_send_target(reporter, TransferReporter.SEND_COND_ERROR)   == False
    assert _is_send_target(reporter, TransferReporter.SEND_COND_NONE)    == False

def test_is_send_target_apierror():
    reporter = _init_transfer_reporter()
    reporter.result = TransferResult.RESULT_API_ERROR
    assert _is_send_target(reporter, TransferReporter.SEND_COND_ALL)     == True
    assert _is_send_target(reporter, TransferReporter.SEND_COND_NOTZERO) == True
    assert _is_send_target(reporter, TransferReporter.SEND_COND_ERROR)   == True
    assert _is_send_target(reporter, TransferReporter.SEND_COND_NONE)    == False

def test_is_send_target_ng():
    reporter = _init_transfer_reporter()
    reporter.result = TransferResult.RESULT_NG
    reporter.counter = Counter()
    assert _is_send_target(reporter, TransferReporter.SEND_COND_ALL)     == True
    assert _is_send_target(reporter, TransferReporter.SEND_COND_NOTZERO) == True
    assert _is_send_target(reporter, TransferReporter.SEND_COND_ERROR)   == True
    assert _is_send_target(reporter, TransferReporter.SEND_COND_NONE)    == False

def test_is_send_target_invalid_data_type():
    reporter = _init_transfer_reporter()
    reporter.data_type = 'hoge'
    with pytest.raises(KeyError):
        _is_send_target(reporter, TransferReporter.SEND_COND_ALL)

def test_is_send_target_invalid_condition():
    reporter = _init_transfer_reporter()
    reporter.result = TransferResult.RESULT_OK
    reporter.counter['test'] = 10
    with pytest.raises(ValueError):
        _is_send_target(reporter, 10)
