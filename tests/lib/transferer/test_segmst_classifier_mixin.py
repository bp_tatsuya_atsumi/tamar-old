# -*- coding: utf-8 -*-

from lib.transferer.segmst_classifier_mixin import SegmentMstClassifierMixin

class TestSegmentMstClassifierMixin(SegmentMstClassifierMixin):
    def _classify_segments(self, target_segments, registered_segments, check_description=True):
        return SegmentMstClassifierMixin._classify_segments(self, target_segments, registered_segments, check_description)
    def _modify_segments(self, segments, convf):
        pass
    def _delete_segments(self, segments, convf):
        pass
    def _add_segments(self, segments, convf):
        pass

def _init():
    return TestSegmentMstClassifierMixin()

def test_classify_segments_no_diff():
    mixin = _init()

    # compare empty
    n = []
    d = []
    result = mixin._classify_segments(n, d)
    assert result == ([], [], [])

    # compare existing segments
    n = [{'id':              'segid', 'name': 'segname', 'description': 'note'}]
    d = [{'segment_id_from': 'segid', 'name': 'segname', 'description': 'note', 'status': 0, 'segment_id_to': 'pt-segid'}]
    result == mixin._classify_segments(n, d)
    assert result == ([], [], [])

    # compare existing segments with ignore column
    n = [{'id':              'segid', 'name': 'segname', 'description': 'note', 'ignore': 'ignore_partner'}]
    d = [{'segment_id_from': 'segid', 'name': 'segname', 'description': 'note', 'status': 0, 'segment_id_to': 'pt-segid'}]
    result == mixin._classify_segments(n, d)
    assert result == ([], [], [])

    # compare existing segments with a different description
    n = [{'id':              'segid', 'name': 'segname', 'description': 'note_diff'}]
    d = [{'segment_id_from': 'segid', 'name': 'segname', 'description': 'note', 'status': 0, 'segment_id_to': 'pt-segid'}]
    result == mixin._classify_segments(n, d, check_description=False)
    assert result == ([], [], [])

def test_classify_segments_add():
    mixin = _init()

    # add to empty
    n = [{'id': 'segid', 'name': 'segname', 'description': 'note'}]
    d = []
    result = mixin._classify_segments(n, d)
    assert result == ([], [{'id': 'segid', 'name': 'segname', 'description': 'note'}], [])

    # add another
    n = [{'id': 'segid1', 'name': 'segname1', 'description': 'note1'},
         {'id': 'segid2', 'name': 'segname2', 'description': 'note2'}]
    d = [{'segment_id_from': 'segid1', 'name': 'segname1', 'description': 'note1', 'status': 0, 'segment_id_to': 'pt-segid1'}]
    result = mixin._classify_segments(n, d)
    assert result == ([], [{'id': 'segid2', 'name': 'segname2', 'description': 'note2'}], [])

def test_classify_segments_delete():
    mixin = _init()
    n = []
    d = [{'segment_id_from': 'segid', 'name': 'segname', 'description': 'note', 'status': 0, 'segment_id_to': 'pt-segid'}]
    result = mixin._classify_segments(n, d)
    assert result == ([], [], [{'segment_id_from': 'segid', 'name': 'segname', 'description': 'note', 'status': 0, 'segment_id_to': 'pt-segid'}])

def test_classify_segments_modify():
    mixin = _init()

    # name diff
    n = [{'id': 'segid',              'name': 'segname_diff', 'description': 'note'}]
    d = [{'segment_id_from': 'segid', 'name': 'segname',      'description': 'note', 'status': 0, 'segment_id_to': 'pt-segid'}]
    result = mixin._classify_segments(n, d)
    assert result == ([{'segment_id_from': 'segid', 'name': 'segname', 'name_modified': 'segname_diff',
                        'description': 'note', 'status': 0, 'segment_id_to': 'pt-segid'}], [], [])

    # description diff
    n = [{'id': 'segid',              'name': 'segname', 'description': 'note_diff'}]
    d = [{'segment_id_from': 'segid', 'name': 'segname', 'description': 'note', 'status': 0, 'segment_id_to': 'pt-segid'}]
    result = mixin._classify_segments(n, d)
    assert result == ([{'segment_id_from': 'segid', 'name': 'segname', 'name': 'segname',
                        'description': 'note', 'description_modified': 'note_diff', 'status': 0, 'segment_id_to': 'pt-segid'}], [], [])

    # name and description diff
    n = [{'id': 'segid',              'name': 'segname_diff', 'description': 'note_diff'}]
    d = [{'segment_id_from': 'segid', 'name': 'segname',      'description': 'note', 'status': 0, 'segment_id_to': 'pt-segid'}]
    result = mixin._classify_segments(n, d)
    assert result == ([{'segment_id_from': 'segid', 'name': 'segname', 'name_modified': 'segname_diff',
                        'description': 'note', 'description_modified': 'note_diff', 'status': 0, 'segment_id_to': 'pt-segid'}], [], [])

def test_classify_segments_add_delete_modify():
    mixin = _init()
    n = [{'id':              'segid',  'name': 'segname_diff', 'description': 'note'},
         {'id':              'segid2', 'name': 'segname2',     'description': 'note2'}]
    d = [{'segment_id_from': 'segid',  'name': 'segname',      'description': 'note',  'status': 0, 'segment_id_to': 'pt-segid'},
         {'segment_id_from': 'segid1', 'name': 'segname1',     'description': 'note1', 'status': 0, 'segment_id_to': 'pt-segid1'}]
    result = mixin._classify_segments(n, d)
    assert result ==(
        [{'segment_id_from': 'segid', 'name': 'segname', 'name_modified': 'segname_diff',
          'description': 'note', 'status': 0, 'segment_id_to': 'pt-segid'}],
        [{'id': 'segid2', 'name': 'segname2', 'description': 'note2'}],
        [{'segment_id_from': 'segid1', 'name': 'segname1', 'description': 'note1', 'status': 0, 'segment_id_to': 'pt-segid1'}]
    )
