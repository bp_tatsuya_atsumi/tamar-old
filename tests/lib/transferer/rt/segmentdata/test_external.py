# -*- coding: utf-8 -*-

from lib.transferer.rt.segmentdata.external import SegmentDataTransferer

import contextlib
import datetime
import gzip
import json
from lib.extsegdatatable import ExtSegDataTable
from lib.mappingtable    import MappingTable
from lib.segmentmsttable import SegmentMstTable
from   mock import patch, Mock, mock_open
import pytest
from sets import Set

ENV       = 'dev'
SID       = '9999'
PID       = 'rtbp1'
S3BUCKET  = 'dev-transfer-data'
S3PATH    = '9999/segment_list_data/raw'
S3DIR_CNV = '9999/segment_list_data/converted/{pid}'

SEGMST = [ {'segment_id_from': 'pt-seg1', 'segment_id_to': '9910', 'name': '9910', 'description': '', 'status': 0},
           {'segment_id_from': 'pt-seg2', 'segment_id_to': '9911', 'name': '9910', 'description': '', 'status': 0},
           {'segment_id_from': 'pt-seg3', 'segment_id_to': '9912', 'name': '9910', 'description': '', 'status': 0},
           {'segment_id_from': 'pt-seg4', 'segment_id_to': '9920', 'name': '9910', 'description': '', 'status': 0},
           {'segment_id_from': 'pt-seg5', 'segment_id_to': '9921', 'name': '9910', 'description': '', 'status': 0},
           {'segment_id_from': 'pt-seg6', 'segment_id_to': '9922', 'name': '9910', 'description': '', 'status': 0} ]

class CurrentDatetime(datetime.datetime):
    @classmethod
    def now(cls):
        return cls(2013, 10, 19, 23, 40, 59, 0)

@pytest.fixture
def data_dir(tmp_transfer_dir):
    return tmp_transfer_dir.mkdir(SID).mkdir(PID).mkdir('segmentdata')

@pytest.fixture
def data_dir_rtim(tmp_transfer_dir):
    return tmp_transfer_dir.mkdir(SID).mkdir('rtim').mkdir('segmentdata')

@contextlib.contextmanager
def _patches_with_default_action(func_init_config, data_dir):
    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('lib.sftputil.put'),
        patch('lib.s3util.S3Util.upload_single_file'),
        patch('datetime.datetime', CurrentDatetime)
    ) as (m_config,  m_sftpput, m_s3put, m_now):
        yield (m_sftpput, m_s3put)

def _mapping_table(sid, pid):
    db_user  = 'tamaruser'
    db_pass  = '#!TawaR?u5er'
    db_host  = 'localhost'
    db_name  = 'DB_TAMAR_{sid}'.format(sid=sid)
    tbl_name = 't_user_mapping_{sid}_{pid}'.format(sid=sid, pid=pid)
    mapping_table = MappingTable(db_user, db_pass, db_host, db_name, tbl_name)
    return mapping_table

def _extsegdata_table(sid, pid):
    db_user  = 'tamaruser'
    db_pass  = '#!TawaR?u5er'
    db_host  = 'localhost'
    db_name  = 'DB_TAMAR_{sid}'.format(sid=sid)
    tbl_name = 't_user_extsegdata_{sid}_{pid}'.format(sid=sid, pid=pid)
    extsegdata_table = ExtSegDataTable(db_user, db_pass, db_host, db_name, tbl_name)
    return extsegdata_table

def _segmentmst_table(key1, key2):
    db_user  = 'tamaruser'
    db_pass  = '#!TawaR?u5er'
    db_host  = 'localhost'
    db_name  = 'DB_TAMAR_{key2}'.format(key2=key2)
    tbl_name = 't_segmentmst_from_{key1}_to_{key2}'.format(key1=key1, key2='rt'+key2)
    segmentmst_table = SegmentMstTable(db_user, db_pass, db_host, db_name, tbl_name)
    return segmentmst_table

def _init(sid=SID, pid=PID, env=ENV):
    # mapping_9999_bp1
    mapping_table_9999_bp1 = _mapping_table(sid, pid[2:])
    mapping_table_9999_bp1.truncate_table()
    sample_data = [{
                    'user_id'         : 'rt_uid1',
                    'partner_user_id' : 'pt_uid1',
                    'tamar_user_type' : 2,
                    'tamar_user_id'   : 'pt_uid1',
                    'inserted_at'     : datetime.datetime(2013, 10, 19, 21, 10, 0),
                    'updated_at'      : datetime.datetime(2013, 10, 19, 21, 10, 0),
                    'is_deleted'      : 0
                   },{
                    'user_id'         : 'rt_uid2',
                    'partner_user_id' : 'pt_uid2',
                    'tamar_user_type' : 2,
                    'tamar_user_id'   : 'pt_uid2',
                    'inserted_at'     : datetime.datetime(2013, 10, 19, 20, 10, 0),
                    'updated_at'      : datetime.datetime(2013, 10, 19, 20, 10, 0),
                    'is_deleted'      : 0
                   }]
    for data in sample_data:
        mapping_table_9999_bp1.insert_row(data['user_id'],
                                          data['partner_user_id'],
                                          data['tamar_user_type'],
                                          data['tamar_user_id'],
                                          data['inserted_at'],
                                          data['updated_at'],
                                          data['is_deleted'])

    # segmst_9999_bp1
    segmentmst_table_9999_bp1 = _segmentmst_table(pid[2:], sid)
    segmentmst_table_9999_bp1.truncate_table()
    sample_data = SEGMST
    for data in sample_data:
        segmentmst_table_9999_bp1.insert_row(data['segment_id_from'],
                                             data['segment_id_to'],
                                             data['name'],
                                             data['description'],
                                             data['status'])

    # extsegdata_9999_bp1
    extsegdata_table_9999_bp1 = _extsegdata_table(sid, pid[2:])
    extsegdata_table_9999_bp1.truncate_table()
    sample_data = [{
                    'user_id'   : 'pt_uid1',
                    'segment_id': 'pt-seg1',
                    'ttl'       :  datetime.datetime(2013, 10, 20, 0, 0, 0)
                  },{
                    'user_id'   : 'pt_uid1',
                    'segment_id': 'pt-seg2',
                    'ttl'       :  datetime.datetime(2013, 10, 20, 0, 0, 0)
                  },{
                    'user_id'   : 'pt_uid1',
                    'segment_id': 'pt-seg3',
                    'ttl'       :  datetime.datetime(2013, 10, 19, 10, 0, 0) # not notified
                  },{
                    'user_id'   : 'pt_uid1',
                    'segment_id': 'pt-seg4',
                    'ttl'       :  datetime.datetime(2013, 10, 16, 0, 0, 0)  # not notified
                  },{
                    'user_id'   : 'pt_uid2',
                    'segment_id': 'pt-seg3',
                    'ttl'       :  datetime.datetime(2013, 10, 19, 10, 0, 0) # must be notified as deleted
                  },{
                    'user_id'   : 'pt_uid_not_mapped',
                    'segment_id': 'pt-seg3',
                    'ttl'       :  datetime.datetime(2013, 10, 20, 0, 0, 0)
                  }]
    for data in sample_data:
        extsegdata_table_9999_bp1.insert_row(data['user_id'],
                                             data['segment_id'],
                                             data['ttl'])

    return SegmentDataTransferer(sid, pid, env, S3BUCKET, S3PATH)

def test_create_transfer_filename(func_init_config, data_dir):
    with _patches_with_default_action(func_init_config, data_dir):
        file, filename = _init()._create_transfer_filename()
    assert file     == data_dir.join('extsegdata_RTA-67d6-f8822c13078f_201310192340_TEST.json.gz')
    assert filename == 'extsegdata_RTA-67d6-f8822c13078f_201310192340_TEST.json.gz'

def test_transform_transfer_format():
    result = _init()._transform_transfer_format('rt_uid1',
                                                  ['pt-seg1','pt-seg2'],
                                                  { m['segment_id_from']:m['segment_id_to'] for m in SEGMST })
    assert result == json.loads('{"uid":"rt_uid1","attrs":{"TEST":["9910", "9911"]}}')

def test_transform_transfer_format_unknown_segment():
    result = _init()._transform_transfer_format('rt_uid1',
                                                  ['pt-seg1','pt-seg_UNKNOWN'],
                                                  { m['segment_id_from']:m['segment_id_to'] for m in SEGMST })
    assert result == json.loads('{"uid":"rt_uid1","attrs":{"TEST":["9910"]}}')

def test_send_transfer_file():
    with patch('lib.sftputil.put') as m_sftpput:
        result = _init()._send_transfer_file('item1.gz')

    assert result
    assert m_sftpput.call_count == 1
    assert m_sftpput.call_args_list[0][1]['host']       == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[0][1]['port']       == 115
    assert m_sftpput.call_args_list[0][1]['username']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['password']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['local_file'] == 'item1.gz'
    assert m_sftpput.call_args_list[0][1]['remote_dir'] == '/receive/segment_feed/data'
    assert m_sftpput.call_args_list[0][1]['rename']     == True

def test_send_transfer_file_failed():
    with patch('lib.sftputil.put') as m_sftpput:
        m_sftpput.side_effect = Exception()
        result = _init()._send_transfer_file('item1.gz')

    assert not result
    assert m_sftpput.call_count == 1
    assert m_sftpput.call_args_list[0][1]['host']       == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[0][1]['port']       == 115
    assert m_sftpput.call_args_list[0][1]['username']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['password']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['local_file'] == 'item1.gz'
    assert m_sftpput.call_args_list[0][1]['remote_dir'] == '/receive/segment_feed/data'
    assert m_sftpput.call_args_list[0][1]['rename']     == True

def test_create_transfer_file(func_init_config, data_dir):
    m_fp = mock_open()
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        with patch('gzip.open', m_fp, create=True):
            result = _init()._create_transfer_file('')

    handler = m_fp()
    assert handler.write.call_count == 3
    assert json.loads(handler.write.call_args_list[0][0][0])['uid'] == 'pt_uid1'
    assert json.loads(handler.write.call_args_list[1][0][0])['uid'] == 'pt_uid2'
    assert json.loads(handler.write.call_args_list[2][0][0])['uid'] == 'pt_uid_not_mapped'
    assert Set(json.loads(handler.write.call_args_list[0][0][0])['attrs']['TEST']) == Set(['9910', '9911'])
    assert Set(json.loads(handler.write.call_args_list[1][0][0])['attrs']['TEST']) == Set([])
    assert Set(json.loads(handler.write.call_args_list[2][0][0])['attrs']['TEST']) == Set(['9912'])

def test_execute(func_init_config, data_dir):
    # prepare
    transfer_file = data_dir.join('extsegdata_RTA-67d6-f8822c13078f_201310192340_TEST.json.gz')

    # execute
    m_fp = mock_open()
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        with patch('gzip.open', m_fp, create=True):
            m_sftpput, m_s3put = mocks
            result = _init().execute()

    # assert
    assert result.is_ok()
    assert transfer_file.check(exists=0)

    handler = m_fp()
    assert handler.write.call_count == 3
    assert json.loads(handler.write.call_args_list[0][0][0])['uid'] == 'pt_uid1'
    assert json.loads(handler.write.call_args_list[1][0][0])['uid'] == 'pt_uid2'
    assert json.loads(handler.write.call_args_list[2][0][0])['uid'] == 'pt_uid_not_mapped'
    assert Set(json.loads(handler.write.call_args_list[0][0][0])['attrs']['TEST']) == Set(['9910', '9911'])
    assert Set(json.loads(handler.write.call_args_list[1][0][0])['attrs']['TEST']) == Set([])
    assert Set(json.loads(handler.write.call_args_list[2][0][0])['attrs']['TEST']) == Set(['9912'])

    assert m_sftpput.call_count == 1
    assert m_sftpput.call_args_list[0][1]['host']       == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[0][1]['port']       == 115
    assert m_sftpput.call_args_list[0][1]['username']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['password']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['local_file'] == str(transfer_file)
    assert m_sftpput.call_args_list[0][1]['remote_dir'] == '/receive/segment_feed/data'
    assert m_sftpput.call_args_list[0][1]['rename']     == True

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(transfer_file), S3BUCKET, S3DIR_CNV.format(pid=PID))

def test_execute_with_userid_translation(func_init_config, data_dir_rtim):
    # prepare
    transfer_file = data_dir_rtim.join('extsegdata_RTA-67d6-f8822c13078f_201310192340_INTIMATE_MERGER.json.gz')

    # execute
    m_fp = mock_open()
    with _patches_with_default_action(func_init_config, data_dir_rtim) as mocks:
        with patch('gzip.open', m_fp, create=True):
            m_sftpput, m_s3put = mocks
            result = _init(pid='rtim').execute()

    # assert
    assert result.is_ok()
    assert transfer_file.check(exists=0)

    handler = m_fp()
    assert handler.write.call_count == 2
    assert json.loads(handler.write.call_args_list[0][0][0])['uid'] == 'rt_uid1'
    assert json.loads(handler.write.call_args_list[1][0][0])['uid'] == 'rt_uid2'
    assert Set(json.loads(handler.write.call_args_list[0][0][0])['attrs']['INTIMATE_MERGER']) == Set(['9910', '9911'])
    assert Set(json.loads(handler.write.call_args_list[1][0][0])['attrs']['INTIMATE_MERGER']) == Set([])

    assert m_sftpput.call_count == 1
    assert m_sftpput.call_args_list[0][1]['host']       == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[0][1]['port']       == 115
    assert m_sftpput.call_args_list[0][1]['username']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['password']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['local_file'] == str(transfer_file)
    assert m_sftpput.call_args_list[0][1]['remote_dir'] == '/receive/segment_feed/data'
    assert m_sftpput.call_args_list[0][1]['rename']     == True

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(transfer_file), S3BUCKET, S3DIR_CNV.format(pid='rtim'))

def test_execute_sftp_error(func_init_config, data_dir):
    # prepare
    transfer_file = data_dir.join('extsegdata_RTA-67d6-f8822c13078f_201310192340_TEST.json.gz')

    # execute
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_sftpput, m_s3put = mocks
        m_sftpput.side_effect = Exception()
        result = _init().execute()

    # assert
    assert result.is_ng()
    assert transfer_file.check(exists=0)

    assert m_sftpput.call_count == 1
    assert m_sftpput.call_args_list[0][1]['host']       == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[0][1]['port']       == 115
    assert m_sftpput.call_args_list[0][1]['username']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['password']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['local_file'] == str(transfer_file)
    assert m_sftpput.call_args_list[0][1]['remote_dir'] == '/receive/segment_feed/data'
    assert m_sftpput.call_args_list[0][1]['rename']     == True

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(transfer_file), S3BUCKET, S3DIR_CNV.format(pid=PID))
