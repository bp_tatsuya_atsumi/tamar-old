# -*- coding: utf-8 -*-

from lib.transferer.rt.segmentdata.bridge import SegmentDataTransferer

import contextlib
import datetime
import gzip
import json
from   mock import patch
import pytest

ENV       = 'dev'
SID       = '9999'
PID       = 'rt9998'
S3BUCKET  = 'dev-transfer-data'
S3PATH    = '9999/segment_list_data/raw/segment_list_data_20131019234059_9999.json.gz'
S3DIR_CNV = '9999/segment_list_data/converted/rt9998'
DATA1     = '{"pid": "rtoaster", "sid": "9999", "uid": "rt_uid1", "attrs": [9910, 9911, 9912]}'
DATA2     = '{"pid": "rtoaster", "sid": "9999", "uid": "rt_uid2", "attrs": [9920, 9921, 9922]}'

class CurrentDatetime(datetime.datetime):
    @classmethod
    def now(cls):
        return cls(2013, 10, 19, 23, 40, 59, 0)

@pytest.fixture
def data_dir(tmp_transfer_dir):
    return tmp_transfer_dir.mkdir(SID).mkdir(PID).mkdir('segmentdata')

@contextlib.contextmanager
def _patches_with_default_action(func_init_config, data_dir):
    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('lib.s3util.S3Util.download_single_file'),
        patch('lib.mappingtable.MappingTable.select_partner_uids_by_user_id'),
        patch('lib.segmentmsttable.SegmentMstTable.select_all_rows'),
        patch('lib.sftputil.put'),
        patch('lib.s3util.S3Util.upload_single_file'),
        patch('datetime.datetime', CurrentDatetime)
    ) as (m_config, m_s3get, m_mapping, m_segmst, m_sftpput, m_s3put, m_now):
        m_s3get.return_value  = {'key': '9999/segment_list_data/raw/segment_list_data_20131019234059_9999.json.gz'}
        m_segmst.return_value = [
            {'segment_id_from': '9910', 'segment_id_to': '9810'},
            {'segment_id_from': '9911', 'segment_id_to': '9811'},
            {'segment_id_from': '9912', 'segment_id_to': '9812'},
            {'segment_id_from': '9920', 'segment_id_to': '9820'},
            {'segment_id_from': '9921', 'segment_id_to': '9821'},
            {'segment_id_from': '9922', 'segment_id_to': '9822'}
        ]
        m_mapping.side_effect = _convert_uid
        yield (m_s3get, m_mapping, m_sftpput, m_s3put)

def _convert_uid(*args):
    tbl = {
        'rt_uid1': ['pt_uid1'],
        'rt_uid2': ['pt_uid2']
    }
    return tbl.get(args[0])

def _target(sid=SID):
    return SegmentDataTransferer(sid, PID, ENV, S3BUCKET, S3PATH)

def _create_gzip(file, lines):
    with gzip.open(file, 'wb') as f:
        f.write('\n'.join(lines))

def test_create_transfer_filename(func_init_config, data_dir):
    with _patches_with_default_action(func_init_config, data_dir):
        file, filename = _target()._create_transfer_filename('segment_list_data_20131019234059_9999.json.gz')
    assert file     == data_dir.join('extsegdata_RTA-67d6-f8822c13078f_201310192340_RTA-67d6-f8822c13078f.json.gz')
    assert filename == 'extsegdata_RTA-67d6-f8822c13078f_201310192340_RTA-67d6-f8822c13078f.json.gz'

def test_create_transfer_filename_invalid_filename(func_init_config, data_dir):
    with _patches_with_default_action(func_init_config, data_dir):
        with pytest.raises(ValueError):
            _target()._create_transfer_filename('INVALID_FILENAME.json.gz')

def test_transform_transfer_format():
    row = {
            'aid'         : 'rtoaster',
            'sid'         : '9999',
            'uid'         : 'rt_uid1',
            'mapped_uids' : ['pt_uid1'],
            'attrs'       : ['rt9999_seg1', 'rt9999_seg2']
          }
    result = _target()._transform_transfer_format(row, {'rt9999_seg1':'rt9998_seg1', 'rt9999_seg2':'rt9998_seg2'})
    assert json.loads(result) == json.loads('{"uid":"pt_uid1","attrs":{"RTA-67d6-f8822c13078f":["rt9998_seg1", "rt9998_seg2"]}}')

def test_transform_transfer_format_unknown_segment():
    row = {
            'aid'         : 'rtoaster',
            'sid'         : '9999',
            'uid'         : 'rt_uid1',
            'mapped_uids' : ['pt_uid1'],
            'attrs'       : ['rt9999_seg1', 'rt9999_seg2']
          }
    result = _target()._transform_transfer_format(row, {'rt9999_seg1':'rt9998_seg1'})
    assert json.loads(result) == json.loads('{"uid":"pt_uid1","attrs":{"RTA-67d6-f8822c13078f":["rt9998_seg1"]}}')

def test_send_transfer_file():
    with patch('lib.sftputil.put') as m_sftpput:
        result = _target()._send_transfer_file('item1.gz')

    assert result
    assert m_sftpput.call_count == 1
    assert m_sftpput.call_args_list[0][1]['host']       == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[0][1]['port']       == 115
    assert m_sftpput.call_args_list[0][1]['username']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['password']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['local_file'] == 'item1.gz'
    assert m_sftpput.call_args_list[0][1]['remote_dir'] == '/receive/segment_feed/data'
    assert m_sftpput.call_args_list[0][1]['rename']     == True

def test_send_transfer_file_failed():
    with patch('lib.sftputil.put') as m_sftpput:
        m_sftpput.side_effect = Exception()
        result = _target()._send_transfer_file('item1.gz')

    assert not result
    assert m_sftpput.call_count == 1
    assert m_sftpput.call_args_list[0][1]['host']       == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[0][1]['port']       == 115
    assert m_sftpput.call_args_list[0][1]['username']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['password']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['local_file'] == 'item1.gz'
    assert m_sftpput.call_args_list[0][1]['remote_dir'] == '/receive/segment_feed/data'
    assert m_sftpput.call_args_list[0][1]['rename']     == True

def test_execute(func_init_config, data_dir):
    # prepare
    data_file     = data_dir.join('segment_list_data_20131019234059_9999.json.gz')
    transfer_file = data_dir.join('extsegdata_RTA-67d6-f8822c13078f_201310192340_RTA-67d6-f8822c13078f.json.gz')
    _create_gzip(str(data_file), [DATA1, DATA2])

    # execute
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_mapping, m_sftpput, m_s3put = mocks
        result = _target().execute()

    # assert
    assert result.is_ok()
    assert result.counter['user.rtoaster'] == 2
    assert result.counter['user.mapped']   == 2
    assert result.counter['user.unmapped'] == 0
    assert result.counter['user.invalid']  == 0
    assert data_file.check(exists=0)
    assert transfer_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3PATH, str(data_dir))

    assert m_mapping.call_count == 2
    assert m_mapping.call_args_list[0][0] == ('rt_uid1',)
    assert m_mapping.call_args_list[1][0] == ('rt_uid2',)

    assert m_sftpput.call_count == 1
    assert m_sftpput.call_args_list[0][1]['host']       == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[0][1]['port']       == 115
    assert m_sftpput.call_args_list[0][1]['username']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['password']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['local_file'] == str(transfer_file)
    assert m_sftpput.call_args_list[0][1]['remote_dir'] == '/receive/segment_feed/data'
    assert m_sftpput.call_args_list[0][1]['rename']     == True

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(transfer_file), S3BUCKET, S3DIR_CNV)

def test_execute_s3data_notfound(func_init_config, data_dir):

    # execute
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_mapping, m_sftpput, m_s3put = mocks
        m_s3get.side_effect = [None]

        result = _target().execute()

    # assert
    assert result.is_ng()
    assert result.counter['user.rtoaster'] == 0
    assert result.counter['user.mapped']   == 0
    assert result.counter['user.unmapped'] == 0
    assert result.counter['user.invalid']  == 0

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3PATH, str(data_dir))
    assert m_mapping.call_count      == 0
    assert m_s3put.call_count        == 0

def test_execute_sftp_error(func_init_config, data_dir):
    # prepare
    data_file     = data_dir.join('segment_list_data_20131019234059_9999.json.gz')
    transfer_file = data_dir.join('extsegdata_RTA-67d6-f8822c13078f_201310192340_RTA-67d6-f8822c13078f.json.gz')

    _create_gzip(str(data_file), [DATA1, DATA2])

    # execute
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_mapping, m_sftpput, m_s3put = mocks
        m_sftpput.side_effect = Exception()
        result = _target().execute()

    # assert
    assert result.is_ng()
    assert result.counter['user.rtoaster'] == 2
    assert result.counter['user.mapped']   == 2
    assert result.counter['user.unmapped'] == 0
    assert result.counter['user.invalid']  == 0
    assert data_file.check(exists=0)
    assert transfer_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3PATH, str(data_dir))

    assert m_mapping.call_count == 2
    assert m_mapping.call_args_list[0][0] == ('rt_uid1',)
    assert m_mapping.call_args_list[1][0] == ('rt_uid2',)

    assert m_sftpput.call_count == 1
    assert m_sftpput.call_args_list[0][1]['host']       == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[0][1]['port']       == 115
    assert m_sftpput.call_args_list[0][1]['username']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['password']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['local_file'] == str(transfer_file)
    assert m_sftpput.call_args_list[0][1]['remote_dir'] == '/receive/segment_feed/data'
    assert m_sftpput.call_args_list[0][1]['rename']     == True

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(transfer_file), S3BUCKET, S3DIR_CNV)

def test_execute_unexpected_error(func_init_config, data_dir):
    # prepare
    data_file     = data_dir.join('segment_list_data_20131019234059_9999.json.gz')
    transfer_file = data_dir.join('extsegdata_RTA-67d6-f8822c13078f_201310192340_RTA-67d6-f8822c13078f.json.gz')

    _create_gzip(str(data_file), [DATA1, DATA2])

    # execute
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_mapping, m_sftpput, m_s3put = mocks
        m_mapping.side_effect = Exception()
        result = _target().execute()

    # assert
    assert result.is_ng()
    assert result.counter['user.rtoaster'] == 0
    assert result.counter['user.mapped']   == 0
    assert result.counter['user.unmapped'] == 0
    assert result.counter['user.invalid']  == 0
    assert data_file.check(exists=0)
    assert transfer_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3PATH, str(data_dir))

    assert m_mapping.call_count == 1
    assert m_mapping.call_args_list[0][0] == ('rt_uid1',)

    assert m_sftpput.call_count == 0
    assert m_s3put.call_count   == 0
