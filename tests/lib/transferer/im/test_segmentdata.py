# -*- coding: utf-8 -*-

import datetime
import contextlib
import gzip
import json
import os
import pytest
from lib.transferer.exception      import NoS3Files
from lib.transferer.im.segmentdata import SegmentDataTransferer
from lib.transferer.im.base        import IMBase
from mock import Mock
from mock import PropertyMock
from mock import patch

ENV = 'dev'
SID = '9999'
PID = 'im'
S3_BUCKET         = 's3_bucket'
S3_PATH           = 's3_path/segmentdata/raw/split/segmentdata.json.gz'
S3_PATH_CONVERTED = 's3_path/segmentdata/converted/im/split'
TOKEN = 'the_token'
CUSTOMER_ID = 'bp'

JSON_DATA1        = '{"pid": "rtoaster", "sid": "9999", "uid": "rt_uid1", "attrs": [10, 11]}'
JSON_DATA2        = '{"pid": "rtoaster", "sid": "9999", "uid": "rt_uid2", "attrs": [20, 11]}'
JSON_DATA3        = '{"pid": "rtoaster", "sid": "9999", "uid": "rt_uid3", "attrs": [10]}'
JSON_DATA4        = '{"pid": "rtoaster", "sid": "9999", "uid": "rt_uid4", "attrs": [30]}'
JSON_DATA_UNKNOWN = '{"pid": "rtoaster", "sid": "9999", "uid": "rt_uid6", "attrs": [10, 99]}'

@pytest.fixture
def data_dir(tmp_transfer_dir):
    return tmp_transfer_dir.mkdir(SID).mkdir(PID).mkdir('segmentdata')

class CurrentDatetime(datetime.datetime):
    @classmethod
    def now(cls):
        return cls(2013, 10, 19, 23, 40, 59, 0)

def _convert_uid(*args):
    tbl = {'rt_uid1': 'pt_uid1', 'rt_uid2': 'pt_uid2', 'rt_uid3': 'pt_uid3',
           'rt_uid4': 'pt_uid4', 'rt_uid6': 'pt_uid6'}
    return tbl.get(args[0])

def _convert_segid(*args):
    tbl = {'10': {'segment_id_to': 'pt_segid10'}, '11': {'segment_id_to': 'pt_segid11'},
           '20': {'segment_id_to': 'pt_segid20'}, '30': {'segment_id_to': 'pt_segid30'}}
    return tbl.get(args[0])

def _create_gzip(file, lines):
    with gzip.open(file, 'wb') as f:
        f.write('\n'.join(lines))

def _init(sid, pid, env, s3_bucket, s3_path):
    return SegmentDataTransferer(sid, pid, env, s3_bucket, s3_path)

def _create_mock_response(status_code):
    response = Mock()
    response.status_code = status_code
    response.text = '{}'
    return response

@contextlib.contextmanager
def _patches_with_default_action(func_init_config):
    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('lib.s3util.S3Util.download_single_file'),
        patch('lib.mappingtable.MappingTable.select_latest_mapping_partner_uid_by_user_id_until_n_days'),
        patch('lib.segmentmsttable.SegmentMstTable.select_row_by_segment_id'),
        patch('datetime.datetime', CurrentDatetime),
        patch('requests.post'),
        patch('lib.transferer.im.base.IMBase.token', new_callable=PropertyMock),
        patch('lib.s3util.S3Util.upload_single_file'),
    ) as (m_config, m_s3get, m_mapping, m_segmst, m_now, m_post, m_token, m_s3put):
        m_s3get.return_value  = {'key': 's3_path/segmentdata/segmentdata.json.gz'}
        m_mapping.side_effect = _convert_uid
        m_segmst.side_effect  = _convert_segid
        m_post.return_value   = _create_mock_response(200)
        m_token.return_value  = TOKEN

        yield (m_s3get, m_mapping, m_segmst, m_post, m_s3put)

def test_send_requests(lock_dir, tmp_transfer_dir, data_dir, func_init_config):
    data_file = data_dir.join('segmentdata.json.gz')
    _create_gzip(str(data_file), [JSON_DATA1, JSON_DATA2, JSON_DATA3, JSON_DATA4])

    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_mapping, m_segmst, m_post, m_s3put = mocks
        transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH)
        transferer._send_each_request = Mock()
        transferer._send_each_request.return_value = False
        fp_converted = Mock()
        result = transferer._send_requests(str(data_file), fp_converted)
    assert result == False
    assert transferer._send_each_request.call_count == 4
    assert transferer._send_each_request.call_args_list[0][0][0:2] == ('pt_segid10', ['pt_uid1', 'pt_uid3'])
    assert transferer._send_each_request.call_args_list[1][0][0:2] == ('pt_segid11', ['pt_uid1', 'pt_uid2'])
    assert transferer._send_each_request.call_args_list[2][0][0:2] == ('pt_segid20', ['pt_uid2'])
    assert transferer._send_each_request.call_args_list[3][0][0:2] == ('pt_segid30', ['pt_uid4'])
    assert transferer.result.counter['segment.known']   == 4
    assert transferer.result.counter['segment.unknown'] == 0

def test_send_requests_with_flush(lock_dir, tmp_transfer_dir, data_dir, func_init_config):
    data_file = data_dir.join('segmentdata.json.gz')
    _create_gzip(str(data_file), [JSON_DATA2, JSON_DATA3, JSON_DATA4, JSON_DATA1])

    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_mapping, m_segmst, m_post, m_s3put = mocks
        transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH)
        transferer.FLUSH_LIMIT = 3
        transferer._send_each_request = Mock()
        transferer._send_each_request.return_value = False
        fp_converted = Mock()
        result = transferer._send_requests(str(data_file), fp_converted)
    assert result == False
    assert transferer._send_each_request.call_count == 6
    assert transferer._send_each_request.call_args_list[0][0][0:2] == ('pt_segid10', ['pt_uid3'])
    assert transferer._send_each_request.call_args_list[1][0][0:2] == ('pt_segid11', ['pt_uid2'])
    assert transferer._send_each_request.call_args_list[2][0][0:2] == ('pt_segid20', ['pt_uid2'])
    assert transferer._send_each_request.call_args_list[3][0][0:2] == ('pt_segid30', ['pt_uid4'])
    assert transferer._send_each_request.call_args_list[4][0][0:2] == ('pt_segid10', ['pt_uid1'])
    assert transferer._send_each_request.call_args_list[5][0][0:2] == ('pt_segid11', ['pt_uid1'])
    assert transferer.result.counter['segment.known']   == 4
    assert transferer.result.counter['segment.unknown'] == 0

def test_send_requests_unknown_segment(lock_dir, tmp_transfer_dir, data_dir, func_init_config):
    data_file = data_dir.join('segmentdata.json.gz')
    _create_gzip(str(data_file), [JSON_DATA1, JSON_DATA_UNKNOWN])

    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_mapping, m_segmst, m_post, m_s3put = mocks
        transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH)
        transferer._send_each_request = Mock()
        transferer._send_each_request.return_value = False
        fp_converted = Mock()
        result = transferer._send_requests(str(data_file), fp_converted)
    assert result == False
    assert transferer._send_each_request.call_count == 2
    assert transferer._send_each_request.call_args_list[0][0][0:2] == ('pt_segid10', ['pt_uid1', 'pt_uid6'])
    assert transferer._send_each_request.call_args_list[1][0][0:2] == ('pt_segid11', ['pt_uid1'])
    assert transferer.result.counter['segment.known']   == 2
    assert transferer.result.counter['segment.unknown'] == 1

def test_send_each_request(lock_dir, tmp_transfer_dir, data_dir, func_init_config):
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_mapping, m_segmst, m_post, m_s3put = mocks
        transferer   = _init(SID, PID, ENV, S3_BUCKET, S3_PATH)
        fp_converted = Mock()
        result = transferer._send_each_request('segment_id', ['pt_uid1', 'pt_uid2', 'pt_uid3'], fp_converted)
    assert m_post.call_count == 1
    assert m_post.call_args_list[0] == [
        (u'http://localhost:5000/segment/set?customer_id={customer_id}&token={token}'.format(token='the_token', customer_id='bp'),),
        { 'data': json.dumps({'segment_id': 'segment_id', 'audience_ids': ['pt_uid1', 'pt_uid2', 'pt_uid3']}),
          'headers': { 'content-type': 'application/json' } }
    ]
    assert transferer.result.counter['request']     == 1
    assert transferer.result.counter['request.200'] == 1

def test_send_each_request_over_limit(lock_dir, tmp_transfer_dir, data_dir, func_init_config):
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_mapping, m_segmst, m_post, m_s3put = mocks
        transferer   = _init(SID, PID, ENV, S3_BUCKET, S3_PATH)
        fp_converted = Mock()
        transferer.REQUEST_USER_LIMIT = 2
        result = transferer._send_each_request('segment_id', ['pt_uid1', 'pt_uid2', 'pt_uid3'], fp_converted)
    assert result == False
    assert m_post.call_count == 2
    assert m_post.call_args_list[0] == [
        (u'http://localhost:5000/segment/set?customer_id={customer_id}&token={token}'.format(token='the_token', customer_id='bp'),),
        { 'data': json.dumps({'segment_id': 'segment_id', 'audience_ids': ['pt_uid2', 'pt_uid3']}),
          'headers': { 'content-type': 'application/json' } }
    ]
    assert m_post.call_args_list[1] == [
        (u'http://localhost:5000/segment/set?customer_id={customer_id}&token={token}'.format(token='the_token', customer_id='bp'),),
        { 'data': json.dumps({'segment_id': 'segment_id', 'audience_ids': ['pt_uid1']}),
          'headers': { 'content-type': 'application/json' } }
    ]
    assert transferer.result.counter['request']     == 2
    assert transferer.result.counter['request.200'] == 2

def test_send_each_request_api_error(lock_dir, tmp_transfer_dir, data_dir, func_init_config):
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_mapping, m_segmst, m_post, m_s3put = mocks
        m_post.return_value  = _create_mock_response(400)
        transferer   = _init(SID, PID, ENV, S3_BUCKET, S3_PATH)
        fp_converted = Mock()
        result = transferer._send_each_request('segment_id', ['pt_uid1', 'pt_uid2', 'pt_uid3'], fp_converted)
    assert m_post.call_count == 1
    assert m_post.call_args_list[0] == [
        (u'http://localhost:5000/segment/set?customer_id={customer_id}&token={token}'.format(token='the_token', customer_id='bp'),),
        { 'data': json.dumps({'segment_id': 'segment_id', 'audience_ids': ['pt_uid1', 'pt_uid2', 'pt_uid3']}),
          'headers': { 'content-type': 'application/json' } }
    ]
    assert transferer.result.counter['request']     == 1
    assert transferer.result.counter['request.200'] == 0
    assert transferer.result.counter['request.400'] == 1

def test_execute(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    data_file = data_dir.join('segmentdata.json.gz')
    _create_gzip(str(data_file), [JSON_DATA1, JSON_DATA2, JSON_DATA3])
    converted_file = data_dir.join('segmentdata_converted.tsv.gz')

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_mapping, m_segmst, m_post, m_s3put = mocks

        transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH)
        result = transferer.execute()

    # assert
    assert result.is_ok()
    assert result.counter['user.rtoaster'] == 3
    assert result.counter['user.mapped']   == 3
    assert result.counter['user.unmapped'] == 0
    assert result.counter['request']       == 3
    assert result.counter['request.200']   == 3
    assert transferer.result.counter['segment.known']   == 3
    assert transferer.result.counter['segment.unknown'] == 0
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == ('s3_bucket', S3_PATH, str(data_dir))

    assert m_mapping.call_count == 3
    assert m_mapping.call_args_list[0][0] == (u'rt_uid1', 500)
    assert m_mapping.call_args_list[1][0] == (u'rt_uid2', 500)
    assert m_mapping.call_args_list[2][0] == (u'rt_uid3', 500)

    assert m_post.call_count  == 3
    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3_BUCKET, S3_PATH_CONVERTED)

def test_execute_with_unknown_segument(lock_dir, tmp_transfer_dir, data_dir, func_init_config):

    # prepare
    data_file = data_dir.join('segmentdata.json.gz')
    _create_gzip(str(data_file), [JSON_DATA1, JSON_DATA_UNKNOWN])
    converted_file = data_dir.join('segmentdata_converted.tsv.gz')

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_mapping, m_segmst, m_post, m_s3put = mocks

        transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH)
        result = transferer.execute()

    # assert
    assert result.is_api_error()
    assert result.counter['user.rtoaster'] == 2
    assert result.counter['user.mapped']   == 2
    assert result.counter['user.unmapped'] == 0
    assert result.counter['request']       == 2
    assert result.counter['request.200']   == 2
    assert transferer.result.counter['segment.known']   == 2
    assert transferer.result.counter['segment.unknown'] == 1
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == ('s3_bucket', S3_PATH, str(data_dir))

    assert m_mapping.call_count == 2
    assert m_mapping.call_args_list[0][0] == (u'rt_uid1', 500)
    assert m_mapping.call_args_list[1][0] == (u'rt_uid6', 500)

    assert m_post.call_count  == 2
    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3_BUCKET, S3_PATH_CONVERTED)
