# -*- coding: utf-8 -*-

import datetime
import contextlib
import os
import pytest
from lib.transferer.im.base import IMBase
from mock import Mock
from mock import patch

ENV = 'dev'
SID = '9999'
PID = 'im'
DATATYPE  = 'segmentdata'
S3_BUCKET = 's3_bucket'
S3_PATH = 's3_path/segmentdata/raw/split/segmentdata.json.gz'

def _init(sid, pid, env,datatype, s3_bucket, s3_path):
    return IMBase(sid, pid, env, datatype, s3_bucket, s3_path)

class CurrentDatetime(datetime.datetime):
    @classmethod
    def now(cls):
        return cls(2013, 10, 1, 9, 10, 20, 0)

def _create_mock_response(status_code):
    response = Mock()
    response.status_code = status_code
    response.text = '{\"token\":\"the_token\"}'
    return response

@contextlib.contextmanager
def _patches_with_default_action(func_init_config):
    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('datetime.datetime', CurrentDatetime),
        patch('requests.get'),
    ) as (m_config, m_now, m_get):
        m_get.return_value = _create_mock_response(200)

        yield (m_get)

def test_is_token_expired_initial(func_init_config):
    manager = _init(SID, PID, ENV, DATATYPE, S3_BUCKET, S3_PATH)
    assert manager._is_token_expired()

def test_is_token_expired_not_expired(func_init_config):
    with _patches_with_default_action(func_init_config) as mocks:
        manager = _init(SID, PID, ENV, DATATYPE, S3_BUCKET, S3_PATH)
        manager.LAST_TIME = datetime.datetime(2013, 10, 1, 9, 10, 20 ,0) - datetime.timedelta(hours=3, minutes=-1)
        assert not manager._is_token_expired()

def test_is_token_expired_actually_expired(func_init_config):
    with _patches_with_default_action(func_init_config) as mocks:
        manager = _init(SID, PID, ENV, DATATYPE, S3_BUCKET, S3_PATH)
        manager.LAST_TIME = datetime.datetime(2013, 10, 1, 9, 10, 20 ,0) - datetime.timedelta(hours=3, minutes=1)
        assert manager._is_token_expired()

def test_refresh_token(func_init_config):
    with _patches_with_default_action(func_init_config) as mocks:
        m_get = mocks
        manager = _init(SID, PID, ENV, DATATYPE, S3_BUCKET, S3_PATH)
        manager.TOKEN = "old_token"
        manager._refresh_token()
    assert manager.TOKEN == "the_token"
    assert m_get.call_count == 1
    assert m_get.call_args_list[0][0] == ('http://localhost:5000/token/get?username=brainpad&password=password&api_key=api_key', )

def test_refresh_token_api_error(func_init_config):
    with _patches_with_default_action(func_init_config) as mocks:
        m_get = mocks
        m_get.return_value = _create_mock_response(400)
        manager = _init(SID, PID, ENV, DATATYPE, S3_BUCKET, S3_PATH)
        with pytest.raises(Exception):
            manager._refresh_token()
    assert m_get.call_count == 1
    assert m_get.call_args_list[0][0] == ('http://localhost:5000/token/get?username=brainpad&password=password&api_key=api_key', )

def test_token(func_init_config):
    with _patches_with_default_action(func_init_config) as mocks:
        m_get = mocks
        manager = _init(SID, PID, ENV, DATATYPE, S3_BUCKET, S3_PATH)
        result = manager.token
    assert result == "the_token"
    assert m_get.call_count == 1
    assert m_get.call_args_list[0][0] == ('http://localhost:5000/token/get?username=brainpad&password=password&api_key=api_key', )
