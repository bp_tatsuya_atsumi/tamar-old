# -*- coding: utf-8 -*-

import contextlib
import gzip
import json
import os
import pytest
from mock                         import Mock
from mock                         import patch
from lib.transferer.exception     import NoS3Files
from lib.transferer.im.segmentmst import SegmentMstTransferer
from lib.transferer.im.base       import IMBase
from mock import PropertyMock

ENV = 'dev'
SID = '9999'
PID = 'im'
TOKEN = 'the_token'

S3BUCKET   = 's3_bucket'
S3FILE     = 's3_path/segmentmst/raw/segmentmst.csv.gz'
S3DIR_CONV = 's3_path/segmentmst/converted/ma'

CSV_HEADER                     = 'id,name,description,ignore'
CSV_DATA1                      = '100,name100,note100,"ignore_partner"'
CSV_DATA2                      = '200,name200,note200,"ignore_partner"'
CSV_DATA3                      = '300,name300,note300,"ignore_partner"'
CSV_DATA4                      = '400,name400,note400,"ignore_partner"'
CSV_DATA1_NAME_MODIFIED        = '100,name100_modified,note100,"ignore_partner"'
CSV_DATA1_DESCRIPTION_MODIFIED = '100,name100,note100_modified,"ignore_partner"'

@pytest.fixture
def data_dir(tmp_transfer_dir):
    return tmp_transfer_dir.mkdir(SID).mkdir(PID).mkdir('segmentmst')

def _init():
    return SegmentMstTransferer(SID, PID, ENV, S3BUCKET, S3FILE)

def _create_gzip(file, lines):
    with gzip.open(file, 'wb') as f:
        f.write('\n'.join(lines))

def _create_mock_response(status_code):
    response = Mock()
    response.status_code = status_code
    response.text = '{"id": 999}'
    return response

@contextlib.contextmanager
def _patches(func_init_config):
    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('lib.s3util.S3Util.download_single_file'),
        patch('requests.post'),
        patch('requests.put'),
        patch('lib.segmentmsttable.SegmentMstTable.select_all_rows'),
        patch('lib.segmentmsttable.SegmentMstTable.insert_row'),
        patch('lib.segmentmsttable.SegmentMstTable.delete_rows_by_segment_id'),
        patch('lib.segmentmsttable.SegmentMstTable.update_row_by_segment_id'),
        patch('lib.transferer.im.base.IMBase.token', new_callable=PropertyMock),
        patch('lib.s3util.S3Util.upload_single_file'),
    ) as (m_config, m_s3get, m_post, m_put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_token, m_s3put):
        m_s3get.return_value = {'key': S3FILE}
        m_post.return_value  = _create_mock_response(200)
        m_put.return_value   = _create_mock_response(200)
        m_token.return_value = TOKEN
        m_segmst_all.return_value = [
            { 'segment_id_from':'100' , 'segment_id_to':'pt100' , 'name':'name100' , 'description':'note100' , 'status':0 },
            { 'segment_id_from':'200' , 'segment_id_to':'pt200' , 'name':'name200' , 'description':'note200' , 'status':0 },
            { 'segment_id_from':'300' , 'segment_id_to':'pt300' , 'name':'name300' , 'description':'note300' , 'status':0 }
        ]
        yield (m_s3get, m_post, m_put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_s3put)

def test_execute_no_diff(tmp_transfer_dir, data_dir, func_init_config):
    # prepare
    data_file = data_dir.join('segmentmst.csv.gz')
    _create_gzip(str(data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2, CSV_DATA3])
    converted_file = data_dir.join('segmentmst_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3get, m_post, m_put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_s3put):
        result = _init().execute()

    assert result.is_ok()
    assert result.counter['segment.modified'] == 0
    assert result.counter['segment.new']      == 0
    assert result.counter['segment.deleted']  == 0
    assert result.counter['request']          == 0
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)
    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))
    assert m_segmst_del.call_count == 0
    assert m_segmst_ins.call_count == 0
    assert m_s3put.call_count == 1

def test_execute_no_diff_description_modified(tmp_transfer_dir, data_dir, func_init_config):
    # prepare
    data_file = data_dir.join('segmentmst.csv.gz')
    _create_gzip(str(data_file), [CSV_HEADER, CSV_DATA1_DESCRIPTION_MODIFIED, CSV_DATA2, CSV_DATA3])
    converted_file = data_dir.join('segmentmst_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3get, m_post, m_put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_s3put):
        result = _init().execute()

    assert result.is_ok()
    assert result.counter['segment.modified'] == 0
    assert result.counter['segment.new']      == 0
    assert result.counter['segment.deleted']  == 0
    assert result.counter['request']          == 0
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)
    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))
    assert m_segmst_del.call_count == 0
    assert m_segmst_ins.call_count == 0
    assert m_s3put.call_count == 1

def test_execute_add_segment(tmp_transfer_dir, data_dir, func_init_config):
    # prepare
    data_file = data_dir.join('segmentmst.csv.gz')
    _create_gzip(str(data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2, CSV_DATA3, CSV_DATA4])
    converted_file = data_dir.join('segmentmst_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3get, m_post, m_put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_s3put):
        result = _init().execute()

    assert result.is_ok()
    assert result.counter['segment.modified'] == 0
    assert result.counter['segment.new']      == 1
    assert result.counter['segment.deleted']  == 0
    assert result.counter['request']          == 1
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)
    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))
    assert m_post.call_count == 1
    assert m_post.call_args_list[0] == [
        ('http://localhost:5000/segment?customer_id={customer_id}&token={token}'.format(token='the_token', customer_id='bp'),),
        { 'data': json.dumps({'name': 'name400'}),
          'headers': { 'content-type': 'application/json' } }
    ]
    assert m_put.call_count  == 0
    assert m_segmst_del.call_count == 0
    assert m_segmst_ins.call_count == 1
    assert m_s3put.call_count == 1

def test_execute_delete_segment(tmp_transfer_dir, data_dir, func_init_config):
    # prepare
    data_file = data_dir.join('segmentmst.csv.gz')
    _create_gzip(str(data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2])
    converted_file = data_dir.join('segmentmst_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3get, m_post, m_put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_s3put):
        result = _init().execute()

    assert result.is_ok()
    assert result.counter['segment.modified'] == 0
    assert result.counter['segment.new']      == 0
    assert result.counter['segment.deleted']  == 1
    assert result.counter['request']          == 1
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)
    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))
    assert m_post.call_count == 0
    assert m_put.call_count  == 1
    assert m_put.call_args_list[0] == [
        ('http://localhost:5000/segment/{seg_id}/?customer_id={customer_id}&token={token}'.format(token='the_token', customer_id='bp', seg_id='pt300'),),
        { 'data': json.dumps({'name': '[REMOVED] name300'}),
          'headers': { 'content-type': 'application/json' } }
    ]
    assert m_segmst_del.call_count == 1
    assert m_segmst_ins.call_count == 0
    assert m_s3put.call_count == 1

def test_execute_no_diff_modified_description(tmp_transfer_dir, data_dir, func_init_config):
    # prepare
    data_file = data_dir.join('segmentmst.csv.gz')
    _create_gzip(str(data_file), [CSV_HEADER, CSV_DATA1_DESCRIPTION_MODIFIED, CSV_DATA2, CSV_DATA3])
    converted_file = data_dir.join('segmentmst_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3get, m_post, m_put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_s3put):
        result = _init().execute()

    assert result.is_ok()
    assert result.counter['segment.modified'] == 0
    assert result.counter['segment.new']      == 0
    assert result.counter['segment.deleted']  == 0
    assert result.counter['request']          == 0
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)
    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))
    assert m_post.call_count == 0
    assert m_put.call_count  == 0
    assert m_segmst_del.call_count == 0
    assert m_segmst_ins.call_count == 0
    assert m_s3put.call_count == 1

def test_execute_modify_segment(tmp_transfer_dir, data_dir, func_init_config):
    # prepare
    data_file = data_dir.join('segmentmst.csv.gz')
    _create_gzip(str(data_file), [CSV_HEADER, CSV_DATA1_NAME_MODIFIED, CSV_DATA2, CSV_DATA3])
    converted_file = data_dir.join('segmentmst_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3get, m_post, m_put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_s3put):
        result = _init().execute()

    assert result.is_ok()
    assert result.counter['segment.modified'] == 1
    assert result.counter['segment.new']      == 0
    assert result.counter['segment.deleted']  == 0
    assert result.counter['request']          == 1
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)
    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))
    assert m_post.call_count == 0
    assert m_put.call_count  == 1
    assert m_put.call_args_list[0] == [
        ('http://localhost:5000/segment/{seg_id}/?customer_id={customer_id}&token={token}'.format(token='the_token', customer_id='bp', seg_id='pt100'),),
        { 'data': json.dumps({'name': 'name100_modified'}),
          'headers': { 'content-type': 'application/json' } }
    ]
    assert m_segmst_del.call_count == 0
    assert m_segmst_ins.call_count == 0
    assert m_s3put.call_count == 1

def test_execute_add_delete_modify_segment(tmp_transfer_dir, data_dir, func_init_config):
    # prepare
    data_file = data_dir.join('segmentmst.csv.gz')
    _create_gzip(str(data_file), [CSV_HEADER, CSV_DATA1_NAME_MODIFIED, CSV_DATA3, CSV_DATA4])
    converted_file = data_dir.join('segmentmst_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3get, m_post, m_put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_s3put):
        result = _init().execute()

    assert result.is_ok()
    assert result.counter['segment.modified'] == 1
    assert result.counter['segment.new']      == 1
    assert result.counter['segment.deleted']  == 1
    assert result.counter['request']          == 3
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)
    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))
    assert m_post.call_count == 1
    assert m_post.call_args_list[0] == [
        ('http://localhost:5000/segment?customer_id={customer_id}&token={token}'.format(token='the_token', customer_id='bp'),),
        { 'data': json.dumps({'name': 'name400'}),
          'headers': { 'content-type': 'application/json' } }
    ]
    assert m_put.call_count  == 2
    assert m_put.call_args_list[0] == [
        ('http://localhost:5000/segment/{seg_id}/?customer_id={customer_id}&token={token}'.format(token='the_token', customer_id='bp', seg_id='pt100'),),
        { 'data': json.dumps({'name': 'name100_modified'}),
          'headers': { 'content-type': 'application/json' } }
    ]
    assert m_put.call_args_list[1] == [
        ('http://localhost:5000/segment/{seg_id}/?customer_id={customer_id}&token={token}'.format(token='the_token', customer_id='bp', seg_id='pt200'),),
        { 'data': json.dumps({'name': '[REMOVED] name200'}),
          'headers': { 'content-type': 'application/json' } }
    ]
    assert m_segmst_del.call_count == 1
    assert m_segmst_ins.call_count == 1
    assert m_s3put.call_count == 1

def test_execute_s3file_notfound(tmp_transfer_dir, data_dir, func_init_config):
    # execute
    with _patches(func_init_config) as (m_s3get, m_post, m_put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_s3put):
        result = _init().execute()

    assert result.is_ng()
    assert not result.counter
    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))

def test_execute_no_csv_header(tmp_transfer_dir, data_dir, func_init_config):
    # prepare
    data_file = data_dir.join('segmentmst.csv.gz')
    _create_gzip(str(data_file), [CSV_DATA1, CSV_DATA2, CSV_DATA3])
    converted_file = data_dir.join('segmentmst_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3get, m_post, m_put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_s3put):
        result = _init().execute()

    assert result.is_ng()
    assert not result.counter
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)
    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))
    assert m_post.call_count == 0
    assert m_segmst_del.call_count == 0
    assert m_segmst_ins.call_count == 0
    assert m_s3put.call_count == 0

def test_execute_invalid_csv_header(tmp_transfer_dir, data_dir, func_init_config):
    # prepare
    data_file = data_dir.join('segmentmst.csv.gz')
    _create_gzip(str(data_file), ['foo.bar', CSV_DATA1])
    converted_file = data_dir.join('segmentmst_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3get, m_post, m_put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_s3put):
        result = _init().execute()

    assert result.is_ng()
    assert not result.counter
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)
    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))
    assert m_post.call_count == 0
    assert m_segmst_del.call_count == 0
    assert m_segmst_ins.call_count == 0
    assert m_s3put.call_count == 0

def test_execute_no_csv_record(tmp_transfer_dir, data_dir, func_init_config):
    # prepare
    data_file = data_dir.join('segmentmst.csv.gz')
    _create_gzip(str(data_file), [CSV_HEADER])
    converted_file = data_dir.join('segmentmst_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3get, m_post, m_put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_s3put):
        result = _init().execute()

    assert result.is_ok()
    assert result.counter['segment.modified'] == 0
    assert result.counter['segment.new']      == 0
    assert result.counter['segment.deleted']  == 3
    assert result.counter['request']          == 3
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)
    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))
    assert m_post.call_count == 0
    assert m_put.call_count  == 3
    assert m_put.call_args_list[0] == [
        ('http://localhost:5000/segment/{seg_id}/?customer_id={customer_id}&token={token}'.format(token='the_token', customer_id='bp', seg_id='pt100'),),
        { 'data': json.dumps({'name': '[REMOVED] name100'}),
          'headers': { 'content-type': 'application/json' } }
    ]
    assert m_put.call_args_list[1] == [
        ('http://localhost:5000/segment/{seg_id}/?customer_id={customer_id}&token={token}'.format(token='the_token', customer_id='bp', seg_id='pt200'),),
        { 'data': json.dumps({'name': '[REMOVED] name200'}),
          'headers': { 'content-type': 'application/json' } }
    ]
    assert m_put.call_args_list[2] == [
        ('http://localhost:5000/segment/{seg_id}/?customer_id={customer_id}&token={token}'.format(token='the_token', customer_id='bp', seg_id='pt300'),),
        { 'data': json.dumps({'name': '[REMOVED] name300'}),
          'headers': { 'content-type': 'application/json' } }
    ]
    assert m_segmst_del.call_count == 3
    assert m_segmst_ins.call_count == 0
    assert m_s3put.call_count == 1

def test_execute_invalid_csv_record(tmp_transfer_dir, data_dir, func_init_config):
    # prepare
    data_file = data_dir.join('segmentmst.csv.gz')
    _create_gzip(str(data_file), [CSV_HEADER, CSV_DATA1, '200,name200', '', '300,name300,note300,other300'])
    converted_file = data_dir.join('segmentmst_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3get, m_post, m_put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_s3put):
        result = _init().execute()

    assert result.is_ok()
    assert result.counter['segment.modified'] == 0
    assert result.counter['segment.new']      == 0
    assert result.counter['segment.deleted']  == 0
    assert result.counter['request']          == 0
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)
    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))
    assert m_post.call_count == 0
    assert m_segmst_del.call_count == 0
    assert m_segmst_ins.call_count == 0
    assert m_s3put.call_count == 1

def test_execute_empty_csv_file(tmp_transfer_dir, data_dir, func_init_config):
    # prepare
    data_file = data_dir.join('segmentmst.csv.gz')
    _create_gzip(str(data_file), [])
    converted_file = data_dir.join('segmentmst_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3get, m_post, m_put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_s3put):
        result = _init().execute()

    assert result.is_ok()
    assert result.counter['segment.modified'] == 0
    assert result.counter['segment.new']      == 0
    assert result.counter['segment.deleted']  == 3
    assert result.counter['request']          == 3
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)
    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))
    assert m_post.call_count == 0
    assert m_put.call_count  == 3
    assert m_put.call_args_list[0] == [
        ('http://localhost:5000/segment/{seg_id}/?customer_id={customer_id}&token={token}'.format(token='the_token', customer_id='bp', seg_id='pt100'),),
        { 'data': json.dumps({'name': '[REMOVED] name100'}),
          'headers': { 'content-type': 'application/json' } }
    ]
    assert m_put.call_args_list[1] == [
        ('http://localhost:5000/segment/{seg_id}/?customer_id={customer_id}&token={token}'.format(token='the_token', customer_id='bp', seg_id='pt200'),),
        { 'data': json.dumps({'name': '[REMOVED] name200'}),
          'headers': { 'content-type': 'application/json' } }
    ]
    assert m_put.call_args_list[2] == [
        ('http://localhost:5000/segment/{seg_id}/?customer_id={customer_id}&token={token}'.format(token='the_token', customer_id='bp', seg_id='pt300'),),
        { 'data': json.dumps({'name': '[REMOVED] name300'}),
          'headers': { 'content-type': 'application/json' } }
    ]
    assert m_segmst_del.call_count == 3
    assert m_segmst_ins.call_count == 0
    assert m_s3put.call_count == 1

def test_execute_api_error(tmp_transfer_dir, data_dir, func_init_config):
    # prepare
    data_file = data_dir.join('segmentmst.csv.gz')
    _create_gzip(str(data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2, CSV_DATA3, CSV_DATA4])
    converted_file = data_dir.join('segmentmst_converted.tsv.gz')

    # execute
    with _patches(func_init_config) as (m_s3get, m_post, m_put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_s3put):
        m_post.return_value = _create_mock_response(400)
        result = _init().execute()

    assert result.api_error()
    assert result.counter['segment.modified'] == 0
    assert result.counter['segment.new']      == 1
    assert result.counter['segment.deleted']  == 0
    assert result.counter['request']          == 1
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)
    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))
    assert m_post.call_count == 1
    assert m_post.call_args_list[0] == [
        ('http://localhost:5000/segment?customer_id={customer_id}&token={token}'.format(token='the_token', customer_id='bp'),),
        { 'data': json.dumps({'name': 'name400'}),
          'headers': { 'content-type': 'application/json' } }
    ]
    assert m_segmst_del.call_count == 0
    assert m_segmst_ins.call_count == 0
    assert m_s3put.call_count == 1

def test_modify_segments(tmp_transfer_dir, data_dir, func_init_config):
    with _patches(func_init_config) as (m_s3get, m_post, m_put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_s3put):
        fp_converted = Mock()
        transferer = _init()
        transferer._modify_segments(
            [{'segment_id_from': 'segid', 'name': 'segname', 'name_modified': 'segname_rename',
              'description': 'note', 'status': 0, 'segment_id_to': 'pt-segid'}],
            fp_converted
        )
    assert m_put.call_count == 1
    assert m_put.call_args_list[0] == [
        ('http://localhost:5000/segment/{seg_id}/?customer_id={customer_id}&token={token}'\
         .format(token='the_token', customer_id='bp', seg_id='pt-segid'),),
        { 'data': json.dumps({'name': 'segname_rename'}),
          'headers': { 'content-type': 'application/json' } }
    ]
    assert m_segmst_update.call_count == 1
    assert m_segmst_update.call_args_list[0][0] == ('segid', {'name': 'segname_rename'})

def test_modify_segments_db_fail(tmp_transfer_dir, data_dir, func_init_config):
    with _patches(func_init_config) as (m_s3get, m_post, m_put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_s3put):
        fp_converted = Mock()
        m_segmst_del.return_value = 0
        transferer = _init()
        transferer._modify_segments(
            [{'segment_id_from': 'segid', 'name': 'segname', 'name_modified': 'segname_rename',
              'description': 'note', 'status': 0, 'segment_id_to': 'pt-segid'}],
            fp_converted
        )
    assert m_put.call_count == 1
    assert m_put.call_args_list[0] == [
        ('http://localhost:5000/segment/{seg_id}/?customer_id={customer_id}&token={token}'\
         .format(token='the_token', customer_id='bp', seg_id='pt-segid'),),
        { 'data': json.dumps({'name': 'segname_rename'}),
          'headers': { 'content-type': 'application/json' } }
    ]
    assert m_segmst_update.call_count == 1
    assert m_segmst_update.call_args_list[0][0] == ('segid', {'name': 'segname_rename'})

def test_modify_segments_put_fail(tmp_transfer_dir, data_dir, func_init_config):
    with _patches(func_init_config) as (m_s3get, m_post, m_put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_s3put):
        fp_converted = Mock()
        m_put.return_value = _create_mock_response(400)
        transferer = _init()
        transferer._modify_segments(
            [{'segment_id_from': 'segid', 'name': 'segname', 'name_modified': 'segname_rename',
              'description': 'note', 'status': 0, 'segment_id_to': 'pt-segid'}],
            fp_converted
        )
    assert m_put.call_count == 1
    assert m_put.call_args_list[0] == [
        ('http://localhost:5000/segment/{seg_id}/?customer_id={customer_id}&token={token}'\
         .format(token='the_token', customer_id='bp', seg_id='pt-segid'),),
        { 'data': json.dumps({'name': 'segname_rename'}),
          'headers': { 'content-type': 'application/json' } }
    ]
    assert m_segmst_update.call_count == 0

def test_delete_segments(tmp_transfer_dir, data_dir, func_init_config):
    with _patches(func_init_config) as (m_s3get, m_post, m_put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_s3put):
        fp_converted = Mock()
        transferer = _init()
        transferer._delete_segments(
            [{'segment_id_from': 'segid', 'name': 'segname', 'description': 'note', 'status': 0, 'segment_id_to': 'pt-segid'}],
            fp_converted
        )
    assert m_put.call_count == 1
    assert m_put.call_args_list[0] == [
        ('http://localhost:5000/segment/{seg_id}/?customer_id={customer_id}&token={token}'.format(token='the_token', customer_id='bp', seg_id='pt-segid'),),
        { 'data': json.dumps({'name': '[REMOVED] segname'}),
          'headers': { 'content-type': 'application/json' } }
    ]
    assert m_segmst_del.call_count == 1
    assert m_segmst_del.call_args_list[0][0] == ('segid',)

def test_delete_segments_db_fail(tmp_transfer_dir, data_dir, func_init_config):
    with _patches(func_init_config) as (m_s3get, m_post, m_put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_s3put):
        m_segmst_del.return_value = 0
        fp_converted = Mock()
        transferer = _init()
        transferer._delete_segments(
            [{'segment_id_from': 'segid', 'name': 'segname', 'description': 'note', 'status': 0, 'segment_id_to': 'pt-segid'}],
            fp_converted
        )
    assert m_put.call_count == 1
    assert m_put.call_args_list[0] == [
        ('http://localhost:5000/segment/{seg_id}/?customer_id={customer_id}&token={token}'.format(token='the_token', customer_id='bp', seg_id='pt-segid'),),
        { 'data': json.dumps({'name': '[REMOVED] segname'}),
          'headers': { 'content-type': 'application/json' } }
    ]
    assert m_segmst_del.call_count == 1
    assert m_segmst_del.call_args_list[0][0] == ('segid',)

def test_delete_segments_put_fail(tmp_transfer_dir, data_dir, func_init_config):
    with _patches(func_init_config) as (m_s3get, m_post, m_put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_s3put):
        fp_converted = Mock()
        m_put.return_value = _create_mock_response(400)
        transferer = _init()
        transferer._delete_segments(
            [{'segment_id_from': 'segid', 'name': 'segname', 'description': 'note', 'status': 0, 'segment_id_to': 'pt-segid'}],
            fp_converted
        )
    assert m_put.call_count == 1
    assert m_put.call_args_list[0] == [
        ('http://localhost:5000/segment/{seg_id}/?customer_id={customer_id}&token={token}'.format(token='the_token', customer_id='bp', seg_id='pt-segid'),),
        { 'data': json.dumps({'name': '[REMOVED] segname'}),
          'headers': { 'content-type': 'application/json' } }
    ]
    assert m_segmst_del.call_count == 0

def test_add_segments(tmp_transfer_dir, data_dir, func_init_config):
    converted_file = data_dir.join('segmentmst_converted.tsv.gz')
    with _patches(func_init_config) as (m_s3get, m_post, m_put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_s3put):
        fp_converted = Mock()
        transferer = _init()
        transferer._add_segments([{'id': 'segid',  'name': 'segname', 'description': 'note'}], fp_converted)

    assert m_post.call_count == 1
    assert m_post.call_args_list[0] == [
        ('http://localhost:5000/segment?customer_id={customer_id}&token={token}'.format(token='the_token', customer_id='bp'),),
        { 'data': json.dumps({'name': 'segname'}),
          'headers': { 'content-type': 'application/json' } }
    ]
    assert m_segmst_ins.call_count == 1
    assert m_segmst_ins.call_args_list[0][0] == ('segid', '999', 'segname', 'note')

def test_add_segments_db_fail(tmp_transfer_dir, data_dir, func_init_config):
    converted_file = data_dir.join('segmentmst_converted.tsv.gz')
    with _patches(func_init_config) as (m_s3get, m_post, m_put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_s3put):
        fp_converted = Mock()
        m_segmst_ins.side_effect = Exception()
        transferer = _init()
        with pytest.raises(Exception):
            transferer._add_segments([{'id': 'segid',  'name': 'segname', 'description': 'note'}], fp_converted)

    assert m_post.call_count == 1
    assert m_post.call_args_list[0] == [
        ('http://localhost:5000/segment?customer_id={customer_id}&token={token}'.format(token='the_token', customer_id='bp'),),
        { 'data': json.dumps({'name': 'segname'}),
          'headers': { 'content-type': 'application/json' } }
    ]
    assert m_segmst_ins.call_count == 1
    assert m_segmst_ins.call_args_list[0][0] == ('segid', '999', 'segname', 'note')

def test_add_segments_post_fail(tmp_transfer_dir, data_dir, func_init_config):
    converted_file = data_dir.join('segmentmst_converted.tsv.gz')
    with _patches(func_init_config) as (m_s3get, m_post, m_put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_s3put):
        fp_converted = Mock()
        m_post.return_value = _create_mock_response(400)
        transferer = _init()
        transferer._add_segments([{'id': 'segid',  'name': 'segname', 'description': 'note'}], fp_converted)

    assert m_post.call_count == 1
    assert m_post.call_args_list[0] == [
        ('http://localhost:5000/segment?customer_id={customer_id}&token={token}'.format(token='the_token', customer_id='bp'),),
        { 'data': json.dumps({'name': 'segname'}),
          'headers': { 'content-type': 'application/json' } }
    ]
    assert m_segmst_ins.call_count == 0
