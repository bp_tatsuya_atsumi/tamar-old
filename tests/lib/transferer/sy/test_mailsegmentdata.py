# -*- coding: utf-8 -*-

from lib.transferer.sy.mailsegmentdata import MailSegmentDataTransferer

import contextlib
import datetime
import gzip
import json
from   lib.transferer.exception import NoS3Files
import lib.util as util
from   mock import patch
import os
import pytest
from   sets import Set

ENV = 'dev'
SID = '9999'
PID = 'sy'

S3BUCKET  = 'dev-transfer-data'
S3PATH    = '9999/mail_list_data/raw/mailsegmentdata.json.gz'
S3DIR_CNV = '9999/mail_list_data/converted/sy'

DATA1 = '{"pid": "rtoaster", "sid": "9999", "uid": "rt_uid1", "attrs": ["seg1", "seg2", "seg3"]}'
DATA2 = '{"pid": "rtoaster", "sid": "9999", "uid": "rt_uid2", "attrs": ["seg1", "seg4"]}'

class CurrentDatetime(datetime.datetime):
    @classmethod
    def now(cls):
        return cls(2013, 10, 19, 23, 40, 59, 0)

@pytest.fixture
def data_dir(tmp_transfer_dir):
    return tmp_transfer_dir.mkdir(SID).mkdir(PID).mkdir('mailsegmentdata')

@contextlib.contextmanager
def _patches_with_default_action(func_init_config, data_dir):
    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('lib.s3util.S3Util.download_single_file'),
        patch('lib.sftputil.SftpUtil.__enter__'),
        patch('lib.s3util.S3Util.upload_single_file'),
        patch('datetime.datetime', CurrentDatetime)
    ) as (m_config, m_s3get, m_sftp, m_s3put, m_now):
        m_s3get.return_value    = {'key': S3PATH}

        yield (m_s3get, m_sftp, m_s3put, m_now)

def _target():
    return MailSegmentDataTransferer(SID, PID, ENV, S3BUCKET, S3PATH)

def _create_gzip(file, lines):
    with gzip.open(file, 'wb') as f:
        f.write('\n'.join(lines))

def test_create_transfer_files(func_init_config, data_dir):
    # prepare
    data_file = data_dir.join('mailsegmentdata.json.gz')
    _create_gzip(str(data_file), [DATA1, DATA2])
    transfer_files = [os.path.join(str(data_dir), u'seg1_20131019_9999.csv.gz'),
                      os.path.join(str(data_dir), u'seg2_20131019_9999.csv.gz'),
                      os.path.join(str(data_dir), u'seg3_20131019_9999.csv.gz'),
                      os.path.join(str(data_dir), u'seg4_20131019_9999.csv.gz')]

    # execute
    try:
        with _patches_with_default_action(func_init_config, data_dir) as mocks:
            m_s3get, m_sftp, m_s3put, m_now = mocks
            target = _target()
            result = target._create_transfer_files(str(data_file))
            assert len(result) == 4
            assert Set(result) == Set(transfer_files)
    finally:
        transfer_files.append(str(data_file))
        util.remove_files(transfer_files)

def test_create_transfer_filename(func_init_config, data_dir):
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_sftp, m_s3put, m_now = mocks
        target = _target()
        assert target._create_transfer_filename('segname')       == os.path.join(str(data_dir), 'segname_20131019_9999.csv.gz')
        assert target._create_transfer_filename(u'セグメント名') == os.path.join(str(data_dir), u'セグメント名_20131019_9999.csv.gz')

def test_send_transfer_files():
    with patch('lib.sftputil.SftpUtil.__enter__') as m_sftp:
        result = _target()._send_transfer_files(['seg1_20131019_9999.csv.gz'])

        assert result
        assert m_sftp.call_count == 1
        assert m_sftp.return_value.put.call_count == 1
        assert m_sftp.return_value.put.call_args_list[0][1]['local_file'] == 'seg1_20131019_9999.csv.gz'
        assert m_sftp.return_value.put.call_args_list[0][1]['remote_dir'] == '/receive/sy/attrs'
        assert m_sftp.return_value.put.call_args_list[0][1]['rename']     == True

def test_send_transfer_files_failed():
    with patch('lib.sftputil.SftpUtil.__enter__') as m_sftp:
        m_sftp.return_value.put.side_effect = Exception()

        result = _target()._send_transfer_files(['seg1_20131019_9999.csv.gz'])

        assert not result
        assert m_sftp.call_count == 1
        assert m_sftp.return_value.put.call_count == 1
        assert m_sftp.return_value.put.call_args_list[0][1]['local_file'] == 'seg1_20131019_9999.csv.gz'
        assert m_sftp.return_value.put.call_args_list[0][1]['remote_dir'] == '/receive/sy/attrs'
        assert m_sftp.return_value.put.call_args_list[0][1]['rename']     == True

def test_execute(func_init_config, data_dir):
    # prepare
    data_file = data_dir.join('mailsegmentdata.json.gz')
    _create_gzip(str(data_file), [DATA1, DATA2])
    transfer_files = [os.path.join(str(data_dir), u'seg1_20131019_9999.csv.gz'),
                      os.path.join(str(data_dir), u'seg2_20131019_9999.csv.gz'),
                      os.path.join(str(data_dir), u'seg3_20131019_9999.csv.gz'),
                      os.path.join(str(data_dir), u'seg4_20131019_9999.csv.gz')]

    # execute
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_sftp, m_s3put, m_now = mocks

        transferer = _target()
        result = transferer.execute()

    # assert
    assert result.is_ok()
    assert result.counter['user.rtoaster'] == 2
    assert result.counter['segment.num'] == 4
    assert result.counter['segment.sftp.ok'] == 4
    assert data_file.check(exists=0)
    for transfer_file in transfer_files:
        assert not os.path.exists(transfer_file)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == ('dev-transfer-data', '9999/mail_list_data/raw/mailsegmentdata.json.gz', str(data_dir))

    assert m_s3put.call_count == 4
    for transfer_file in transfer_files:
        m_s3put.assert_any_call(transfer_file, S3BUCKET, S3DIR_CNV)

    assert m_sftp.call_count == 1
    assert m_sftp.return_value.put.call_count == 4
    assert m_sftp.return_value.put.call_args_list[0][1]['remote_dir'] == '/receive/sy/attrs'
    assert m_sftp.return_value.put.call_args_list[0][1]['rename']     == True
    for transfer_file in transfer_files:
        assert Set([ f[1]['local_file'] for f in m_sftp.return_value.put.call_args_list ]) == Set(transfer_files)

def test_execute_s3data_notfound(func_init_config, data_dir):
    # prepare
    data_file = data_dir.join('mailsegmentdata.json.gz')

    # execute
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_sftp, m_s3put, m_now = mocks
        m_s3get.return_value = None
        transferer = _target()
        result = transferer.execute()

    # assert
    assert result.is_ng()
    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == ('dev-transfer-data', '9999/mail_list_data/raw/mailsegmentdata.json.gz', str(data_dir))

def test_execute_s3put_error(func_init_config, data_dir):
    # prepare
    data_file = data_dir.join('mailsegmentdata.json.gz')
    _create_gzip(str(data_file), [DATA1, DATA2])
    transfer_files = [os.path.join(str(data_dir), u'seg1_20131019_9999.csv.gz'),
                      os.path.join(str(data_dir), u'seg2_20131019_9999.csv.gz'),
                      os.path.join(str(data_dir), u'seg3_20131019_9999.csv.gz'),
                      os.path.join(str(data_dir), u'seg4_20131019_9999.csv.gz')]

    # execute
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_sftp, m_s3put, m_now = mocks
        m_s3put.side_effect = Exception()

        transferer = _target()
        result = transferer.execute()

    # assert
    assert result.is_ng()
    assert result.counter['user.rtoaster'] == 2
    assert result.counter['segment.num'] == 4
    assert data_file.check(exists=0)
    for transfer_file in transfer_files:
        assert not os.path.exists(transfer_file)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == ('dev-transfer-data', '9999/mail_list_data/raw/mailsegmentdata.json.gz', str(data_dir))

    assert m_s3put.call_count == 1
    assert m_sftp.call_count == 0

def test_execute_sftp_error(func_init_config, data_dir):
    # prepare
    data_file = data_dir.join('mailsegmentdata.json.gz')
    _create_gzip(str(data_file), [DATA1, DATA2])
    transfer_files = [os.path.join(str(data_dir), u'seg1_20131019_9999.csv.gz'),
                      os.path.join(str(data_dir), u'seg2_20131019_9999.csv.gz'),
                      os.path.join(str(data_dir), u'seg3_20131019_9999.csv.gz'),
                      os.path.join(str(data_dir), u'seg4_20131019_9999.csv.gz')]

    # execute
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_sftp, m_s3put, m_now = mocks
        m_sftp.return_value.put.side_effect = Exception()

        transferer = _target()
        result = transferer.execute()

    # assert
    assert result.is_ng()
    assert result.counter['user.rtoaster'] == 2
    assert result.counter['segment.num'] == 4
    assert result.counter['segment.sftp.ok'] == 0
    assert data_file.check(exists=0)
    for transfer_file in transfer_files:
        assert not os.path.exists(transfer_file)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == ('dev-transfer-data', '9999/mail_list_data/raw/mailsegmentdata.json.gz', str(data_dir))

    assert m_s3put.call_count == 4
    for transfer_file in transfer_files:
        m_s3put.assert_any_call(transfer_file, S3BUCKET, S3DIR_CNV)

    assert m_sftp.call_count == 1
    assert m_sftp.return_value.put.call_count == 1

