# -*- coding: utf-8 -*-

from lib.transferer.ma_gdo.recommenddata import RecommendDataTransferer

import datetime
import contextlib
import json
import gzip
import os
from   mock                     import Mock
from   mock                     import patch
import pytest
from   lib.transferer.exception import NoS3Files


SID        = '9999'
PID        = 'ma'
ENV        = 'dev'
S3BUCKET   = 's3bucket'
S3PATH     = 's3path/recommenddata/raw/split/segmentdata.json.gz'
S3DIR_MST  = '9999/recommend_list_mst/raw'
S3PATH_CNV = 's3path/recommenddata/converted/ma/split'

REC_DATA1  = '{"aid":"rt_aid","sid":"rt_sid","uid":"rt_uid1","replace":{"price":"field4","discount":"dct"},"item_lists":{"レコメンド１":["code1","code2"]}}'
REC_DATA2  = '{"aid":"rt_aid","sid":"rt_sid","uid":"rt_uid2","replace":{"price":"field4","discount":"dct"},"item_lists":{"レコメンド１":["code1","code2"],"レコメンド２":["code3","code4"]}}'
REC_DATA3  = '{"aid":"rt_aid","sid":"rt_sid","uid":"rt_uid3","replace":{"price":"field4","discount":"dct"},"item_lists":{"レコメンド３":["code3","code4"]}}'
REC_DATA4  = '{"aid":"rt_aid","sid":"rt_sid","uid":"rt_uid4","replace":{"price":"field4","discount":"dct"}}'
REC_DATA5  = '{"aid":"rt_aid","sid":"rt_sid","uid":"rt_uid5","replace":{"price":"field4","discount":"dct"},"item_lists":{"レコメンド１":["code1","code2"]}}'

MST_HEADER = '"pid","sid","id","name","description"'
MST_DATA1  = '"rtoaster","9999","100","レコメンド１","概要１"'
MST_DATA2  = '"rtoaster","9999","200","レコメンド２","概要２"'

@pytest.fixture
def data_dir(tmp_transfer_dir):
    return tmp_transfer_dir.mkdir(SID).mkdir(PID).mkdir('recommenddata')

class CurrentDatetime(datetime.datetime):
    @classmethod
    def now(cls):
        return cls(2013, 10, 19, 23, 40, 59, 0)

@contextlib.contextmanager
def _patches(func_init_config):
    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('lib.s3util.S3Util.download_single_file'),
        patch('lib.s3util.S3Util.download_single_file_order_by_filename'),
        patch('lib.mappingtable.MappingTable.select_latest_mapping_partner_uid_by_user_id_until_n_days'),
        patch('datetime.datetime', CurrentDatetime),
        patch('requests.get'),
        patch('lib.s3util.S3Util.upload_single_file'),
        patch('lib.util.code_to_long_id'),
    ) as (m_config, m_s3get, m_s3get_latest, m_select, m_now, m_requests, m_s3put, m_itemcodeconv):
        m_s3get.side_effect        = [ {'key' : 's3path/recommenddata/recommenddata.json.gz'} ]
        m_s3get_latest.side_effect = [ {'key' : 's3path/recommendmst/recommendmst.csv.gz'} ]
        m_select.side_effect       = _convert_uid
        m_requests.return_value    = _create_mock_response(200)
        m_itemcodeconv.side_effect = _convert_itemcode

        yield (m_s3get, m_s3get_latest, m_select, m_requests, m_s3put, m_itemcodeconv)

def _convert_uid(*args):
    tbl = {
        'rt_uid1': 'pt_uid1',
        'rt_uid2': 'pt_uid2',
        'rt_uid3': 'pt_uid3',
        'rt_uid4': 'pt_uid4'
    }
    return tbl.get(args[0])

def _convert_itemcode(*args):
    tbl = {
        'code1' : 100,
        'code2' : 200,
        'code3' : 300,
        'code4' : 400,
    }
    return tbl.get(args[0])

def _init():
    return RecommendDataTransferer(SID, PID, ENV, S3BUCKET, S3PATH)

def _create_gzip(file, lines):
    with gzip.open(file, 'wb') as f:
        f.write('\n'.join(lines))

def _create_mock_response(status_code):
    response = Mock()
    response.status_code = status_code
    response.text = ''
    return response


def test_download_s3file(data_dir, func_init_config):
    with _patches(func_init_config) as mocks:
        m_s3get, m_s3get_latest, m_select, m_requests, m_s3put, m_itemcodeconv = mocks
        result = _init()._download_s3file()
        assert result == (
            os.path.join(str(data_dir), 'recommenddata.json.gz'),
            'recommenddata.json.gz'
        )

def test_download_s3file_not_exists(data_dir, func_init_config):
    with _patches(func_init_config) as mocks:
        m_s3get, m_s3get_latest, m_select, m_requests, m_s3put, m_itemcodeconv = mocks
        m_s3get.side_effect  = None
        m_s3get.return_value = None
        with pytest.raises(NoS3Files):
            _init()._download_s3file()

def test_load_recommend_master(data_dir, func_init_config):
    master_file = data_dir.join('recommendmst.csv.gz')
    _create_gzip(str(master_file), [MST_HEADER, MST_DATA1, MST_DATA2])

    result = _init()._load_recommend_master(str(master_file))
    assert result == {
        'レコメンド１': '100',
        'レコメンド２': '200'
    }

def test_create_request_url(func_init_config):
    recname_to_recid = {
        'レコメンド１': '100',
        'レコメンド２': '200'
    }
    with _patches(func_init_config):
        row = json.loads(REC_DATA1)
        row['mapped_uid'] = 'pt_uid1'
        result = _init()._create_request_url(row, recname_to_recid)
        assert result == 'http://localhost:5000/rpl?v=1&u=pt_uid1&rpl=advertiser_id_9999-1382452859:100-100.200'

def test_execute(data_dir, func_init_config):

    # prepare
    data_file      = data_dir.join('recommenddata.json.gz')
    mst_file       = data_dir.join('recommendmst.csv.gz')
    converted_file = data_dir.join('recommenddata_converted.tsv.gz')

    _create_gzip(str(data_file), [REC_DATA1,  REC_DATA2, REC_DATA3, REC_DATA4, REC_DATA5])
    _create_gzip(str(mst_file),  [MST_HEADER, MST_DATA1, MST_DATA2])

    # execute
    with _patches(func_init_config) as mocks:
        m_s3get, m_s3get_latest, m_select, m_requests, m_s3put, m_itemcodeconv = mocks
        result = _init().execute()

    # assert
    assert result.is_ok()
    assert result.counter['user.rtoaster'] == 5
    assert result.counter['user.mapped']   == 4
    assert result.counter['user.unmapped'] == 1
    assert result.counter['request']       == 2
    assert result.counter['request.200']   == 2
    assert data_file.check(exists=0)
    assert mst_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3PATH, str(data_dir))
    assert m_s3get_latest.call_count == 1
    assert m_s3get_latest.call_args_list[0][0] == (S3BUCKET, S3DIR_MST, str(data_dir))

    assert m_select.call_count == 5
    assert m_select.call_args_list[0][0] == (u'rt_uid1', 500)
    assert m_select.call_args_list[1][0] == (u'rt_uid2', 500)
    assert m_select.call_args_list[2][0] == (u'rt_uid3', 500)
    assert m_select.call_args_list[3][0] == (u'rt_uid4', 500)
    assert m_select.call_args_list[4][0] == (u'rt_uid5', 500)

    assert m_requests.call_count == 2
    assert m_requests.call_args_list[0][0] == ('http://localhost:5000/rpl?v=1&u=pt_uid1&rpl=advertiser_id_9999-1382452859:100-100.200',)
    assert m_requests.call_args_list[1][0] == ('http://localhost:5000/rpl?v=1&u=pt_uid2&rpl=advertiser_id_9999-1382452859:200-300.400,100-100.200',)

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3BUCKET, S3PATH_CNV)

def test_execute_data_not_exists(data_dir, func_init_config):

    # execute
    with _patches(func_init_config) as mocks:
        m_s3get, m_s3get_latest, m_select, m_requests, m_s3put, m_itemcodeconv = mocks
        m_s3get.side_effect = [ None ]
        result = _init().execute()

    # assert
    assert result.is_ng()
    assert result.counter['user.rtoaster'] == 0
    assert result.counter['user.mapped']   == 0
    assert result.counter['user.unmapped'] == 0
    assert result.counter['request']       == 0
    assert result.counter['request.200']   == 0

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3PATH, str(data_dir))

    assert m_select.call_count      == 0
    assert m_requests.call_count    == 0
    assert m_s3put.call_count       == 0

def test_execute_mst_not_exists(data_dir, func_init_config):

    # execute
    with _patches(func_init_config) as mocks:
        m_s3get, m_s3get_latest, m_select, m_requests, m_s3put, m_itemcodeconv = mocks
        m_s3get.side_effect        = [ {'key' : 's3path/recommenddata/recommenddata.json.gz'} ]
        m_s3get_latest.side_effect = [ None ]
        result = _init().execute()

    # assert
    assert result.is_ng()
    assert result.counter['user.rtoaster'] == 0
    assert result.counter['user.mapped']   == 0
    assert result.counter['user.unmapped'] == 0
    assert result.counter['request']       == 0
    assert result.counter['request.200']   == 0

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3PATH, str(data_dir))
    assert m_s3get_latest.call_count == 1
    assert m_s3get_latest.call_args_list[0][0] == (S3BUCKET, S3DIR_MST, str(data_dir))

    assert m_select.call_count      == 0
    assert m_requests.call_count    == 0
    assert m_s3put.call_count       == 0

def test_execute_empty_data(data_dir, func_init_config):

    # prepare
    data_file      = data_dir.join('recommenddata.json.gz')
    mst_file       = data_dir.join('recommendmst.csv.gz')
    converted_file = data_dir.join('recommenddata_converted.tsv.gz')

    _create_gzip(str(data_file), [])
    _create_gzip(str(mst_file),  [MST_HEADER, MST_DATA1, MST_DATA2])

    # execute
    with _patches(func_init_config) as mocks:
        m_s3get, m_s3get_latest, m_select, m_requests, m_s3put, m_itemcodeconv = mocks
        result = _init().execute()

    # assert
    assert result.is_ok()
    assert result.counter['user.rtoaster'] == 0
    assert result.counter['user.mapped']   == 0
    assert result.counter['user.unmapped'] == 0
    assert result.counter['request']       == 0
    assert result.counter['request.200']   == 0
    assert data_file.check(exists=0)
    assert mst_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3PATH, str(data_dir))
    assert m_s3get_latest.call_count == 1
    assert m_s3get_latest.call_args_list[0][0] == (S3BUCKET, S3DIR_MST, str(data_dir))

    assert m_select.call_count   == 0
    assert m_requests.call_count == 0

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3BUCKET, S3PATH_CNV)

def test_execute_api_error(data_dir, func_init_config):

    # prepare
    data_file      = data_dir.join('recommenddata.json.gz')
    mst_file       = data_dir.join('recommendmst.csv.gz')
    converted_file = data_dir.join('recommenddata_converted.tsv.gz')

    _create_gzip(str(data_file), [REC_DATA1,  REC_DATA2, REC_DATA3, REC_DATA4, REC_DATA5])
    _create_gzip(str(mst_file),  [MST_HEADER, MST_DATA1, MST_DATA2])

    # execute
    with _patches(func_init_config) as mocks:
        m_s3get, m_s3get_latest, m_select, m_requests, m_s3put, m_itemcodeconv = mocks
        m_requests.side_effect  = [_create_mock_response(200),
                                   _create_mock_response(400)]
        result = _init().execute()

    # assert
    assert result.is_api_error()
    assert result.counter['user.rtoaster'] == 5
    assert result.counter['user.mapped']   == 4
    assert result.counter['user.unmapped'] == 1
    assert result.counter['request']       == 2
    assert result.counter['request.200']   == 1
    assert result.counter['request.400']   == 1
    assert data_file.check(exists=0)
    assert mst_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3PATH, str(data_dir))
    assert m_s3get_latest.call_count == 1
    assert m_s3get_latest.call_args_list[0][0] == (S3BUCKET, S3DIR_MST, str(data_dir))

    assert m_select.call_count == 5
    assert m_select.call_args_list[0][0] == (u'rt_uid1', 500)
    assert m_select.call_args_list[1][0] == (u'rt_uid2', 500)
    assert m_select.call_args_list[2][0] == (u'rt_uid3', 500)
    assert m_select.call_args_list[3][0] == (u'rt_uid4', 500)
    assert m_select.call_args_list[4][0] == (u'rt_uid5', 500)

    assert m_requests.call_count == 2
    assert m_requests.call_args_list[0][0] == ('http://localhost:5000/rpl?v=1&u=pt_uid1&rpl=advertiser_id_9999-1382452859:100-100.200',)
    assert m_requests.call_args_list[1][0] == ('http://localhost:5000/rpl?v=1&u=pt_uid2&rpl=advertiser_id_9999-1382452859:200-300.400,100-100.200',)

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3BUCKET, S3PATH_CNV)

def test_execute_unexpected_error(data_dir, func_init_config):

    # prepare
    data_file      = data_dir.join('recommenddata.json.gz')
    mst_file       = data_dir.join('recommendmst.csv.gz')
    converted_file = data_dir.join('recommenddata_converted.tsv.gz')

    _create_gzip(str(data_file), [REC_DATA1,  REC_DATA2, REC_DATA3, REC_DATA4, REC_DATA5])
    _create_gzip(str(mst_file),  [MST_HEADER, MST_DATA1, MST_DATA2])

    # execute
    with _patches(func_init_config) as mocks:
        m_s3get, m_s3get_latest, m_select, m_requests, m_s3put, m_itemcodeconv = mocks
        m_select.side_effect = Exception()
        result = _init().execute()

    # assert
    assert result.is_ng()
    assert result.counter['user.rtoaster'] == 0
    assert result.counter['user.mapped']   == 0
    assert result.counter['user.unmapped'] == 0
    assert result.counter['request']       == 0
    assert result.counter['request.200']   == 0
    assert data_file.check(exists=0)
    assert mst_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3PATH, str(data_dir))
    assert m_s3get_latest.call_count == 1
    assert m_s3get_latest.call_args_list[0][0] == (S3BUCKET, S3DIR_MST, str(data_dir))

    assert m_select.call_count == 1
    assert m_select.call_args_list[0][0] == (u'rt_uid1', 500)

    assert m_requests.call_count == 0
    assert m_s3put.call_count    == 0
