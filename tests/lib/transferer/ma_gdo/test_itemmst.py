# -*- coding: utf-8 -*-

from lib.transferer.ma_gdo.itemmst import ItemMstTransferer

import contextlib
import datetime
import json
import gzip
import os
from   mock                     import patch
import pytest
from   lib.transferer.exception import NoS3Files


SID        = '9999'
PID        = 'ma'
ENV        = 'dev'
S3BUCKET   = 's3bucket'
S3PATH     = 's3path/itemmst/raw/itemmst_20130103.json.gz'
S3PATH_CNV = 's3path/itemmst/converted/ma'

MST_DATA1  = '{"aid":"rt_aid","sid":"rt_sid","code":"Item_1000","name":"アイテム１","imageUrl":"http://imageurl/1000.jpg","url":"http://linkurl/1000","field0":"道順１","field1":"概要１","field4":"￥1,000〜"}'
MST_DATA2  = '{"aid":"rt_aid","sid":"rt_sid","code":"Item_2000","name":"アイテム２","imageUrl":"http://imageurl/2000.jpg","url":"http://linkurl/2000","field0":"道順２","field1":"概要２","field4":"￥2,000〜"}'
MST_DATA3  = '{"aid":"rt_aid","sid":"rt_sid","code":"Item_3000","name":"アイテム３","imageUrl":"http://imageurl/3000.jpg","url":"http://linkurl/3000","field0":"道順３","field1":"概要３","field4":"￥3,000〜"}'
MST_DATA4  = '{"aid":"rt_aid","sid":"rt_sid","code":"Item_4000","name":"アイテム４","imageUrl":"http://imageurl/4000.jpg","url":"http://linkurl/4000","field0":"道順４","field1":"概要４","field4":"￥4,000〜"}'
MST_DATA5  = '{"aid":"rt_aid","sid":"rt_sid","code":"Item_5000","name":"アイテム５","imageUrl":"http://imageurl/5000.jpg","url":"http://linkurl/5000","field0":"道順５","field1":"概要５","field4":"￥5,000〜"}'

TSV_UPDATE_DATA1 = '9999	1000	アイテム１	http://imageurl/1000.jpg	http://linkurl/1000	￥1,000〜	概要１	advertiser_id_9999'
TSV_UPDATE_DATA2 = '9999	2000	アイテム２	http://imageurl/2000.jpg	http://linkurl/2000	￥2,000〜	概要２	advertiser_id_9999'
TSV_UPDATE_DATA3 = '9999	3000	アイテム３	http://imageurl/3000.jpg	http://linkurl/3000	￥3,000〜	概要３	advertiser_id_9999'
TSV_UPDATE_DATA4 = '9999	4000	アイテム４	http://imageurl/4000.jpg	http://linkurl/4000	￥4,000〜	概要４	advertiser_id_9999'
TSV_UPDATE_DATA5 = '9999	5000	アイテム５	http://imageurl/5000.jpg	http://linkurl/5000	￥5,000〜	概要５	advertiser_id_9999'

TSV_DELETE_DATA1 = '9999	1000	advertiser_id_9999'
TSV_DELETE_DATA2 = '9999	2000	advertiser_id_9999'

class CurrentDatetime(datetime.datetime):
    @classmethod
    def now(cls):
        return cls(2013, 10, 19, 23, 40, 59, 0)

@pytest.fixture
def data_dir(tmp_transfer_dir):
    return tmp_transfer_dir.mkdir(SID).mkdir(PID).mkdir('itemmst')

@contextlib.contextmanager
def _patches(func_init_config):
    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('lib.s3util.S3Util.list_dir'),
        patch('lib.s3util.S3Util.download_single_file'),
        patch('lib.util.code_to_long_id'),
        patch('lib.sftputil.SftpUtil.__enter__'),
        patch('lib.s3util.S3Util.upload_single_file'),
    ) as (m_config, m_s3ls, m_s3get, m_itemconv, m_sftp, m_s3put):
        m_s3ls.return_value = [
            {'key': 's3path/itemmst/raw/itemmst_20130101.json.gz'},
            {'key': 's3path/itemmst/raw/itemmst_20130102.json.gz'},
            {'key': 's3path/itemmst/raw/itemmst_20130103.json.gz'},
            {'key': 's3path/itemmst/raw/itemmst_20130104.json.gz'},
        ]
        m_s3get.side_effect = [
            {'key': 's3path/itemmst/raw/itemmst_20130102.json.gz'},
            {'key': 's3path/itemmst/raw/itemmst_20130103.json.gz'}
        ]
        m_itemconv.side_effect     = _convert_item_code

        yield (m_s3ls, m_s3get, m_itemconv, m_sftp, m_s3put)

@contextlib.contextmanager
def _patches_filename(data_dir):
    with contextlib.nested (
        patch('lib.transferer.ma_gdo.itemmst.ItemMstTransferer._create_update_filename'),
        patch('lib.transferer.ma_gdo.itemmst.ItemMstTransferer._create_delete_filename'),
    ) as (m_updfilename, m_delfilename):
        m_updfilename.side_effect = [
            os.path.join(str(data_dir), 'itemmst_update_1.gz'),
            os.path.join(str(data_dir), 'itemmst_update_2.gz'),
            os.path.join(str(data_dir), 'itemmst_update_3.gz'),
            os.path.join(str(data_dir), 'itemmst_update_4.gz'),
            os.path.join(str(data_dir), 'itemmst_update_5.gz'),
        ]
        m_delfilename.side_effect = [
            os.path.join(str(data_dir), 'itemmst_delete_1.gz'),
            os.path.join(str(data_dir), 'itemmst_delete_2.gz'),
            os.path.join(str(data_dir), 'itemmst_delete_3.gz'),
            os.path.join(str(data_dir), 'itemmst_delete_4.gz'),
            os.path.join(str(data_dir), 'itemmst_delete_5.gz'),
        ]
        yield (m_updfilename, m_delfilename)

def _target():
    return ItemMstTransferer(SID, PID, ENV, S3BUCKET, S3PATH)

def _create_gzip(file, lines):
    with gzip.open(file, 'wb') as f:
        f.write('\n'.join(lines))

def _convert_item_code(*args):
    return int(args[0][5:])

def _assert_py_path_file(py_path_file, lines):
    assert py_path_file.check(file=1, exists=1)
    content = '\n'.join(lines)
    if content:
        content = content + '\n'
    assert gzip.open(str(py_path_file), 'rb').read() == content


def test_download_s3files(data_dir, func_init_config):
    with _patches(func_init_config) as mocks:
        m_s3ls, m_s3get, m_itemconv, m_sftp, m_s3put = mocks
        result = _target()._download_s3files()
        assert result == (
            os.path.join(str(data_dir), 'itemmst_20130102.json.gz'),
            os.path.join(str(data_dir), 'itemmst_20130103.json.gz'),
        )

def test_download_s3files_not_exists(data_dir, func_init_config):
    with _patches(func_init_config) as mocks:
        m_s3ls, m_s3get, m_itemconv, m_sftp, m_s3put = mocks
        m_s3ls.return_value = [{'key': 's3path/itemmst/itemmst_20130101.json.gz'}]
        with pytest.raises(NoS3Files):
            _target()._download_s3files()

def test_load_target_file(data_dir):
    mst_file = data_dir.join('itemmst.json.gz')
    _create_gzip(str(mst_file), [MST_DATA1, MST_DATA2])

    result = _target()._load_target_file(str(mst_file))
    assert result == {
        u'Item_1000' : {
            u'aid'      : u'rt_aid',
            u'sid'      : u'rt_sid',
            u'code'     : u'Item_1000',
            u'name'     : u'アイテム１',
            u'imageUrl' : u'http://imageurl/1000.jpg',
            u'url'      : u'http://linkurl/1000',
            u'field0'   : u'道順１',
            u'field1'   : u'概要１',
            u'field4'   : u'￥1,000〜'
        },
        u'Item_2000' : {
            u'aid'      : u'rt_aid',
            u'sid'      : u'rt_sid',
            u'code'     : u'Item_2000',
            u'name'     : u'アイテム２',
            u'imageUrl' : u'http://imageurl/2000.jpg',
            u'url'      : u'http://linkurl/2000',
            u'field0'   : u'道順２',
            u'field1'   : u'概要２',
            u'field4'   : u'￥2,000〜'
        }
    }

def test_create_transfer_files(data_dir, func_init_config):

    # prepare
    old_code_to_row = {
        'Item_1000' : json.loads(MST_DATA1),
        'Item_2000' : json.loads(MST_DATA2),
        'Item_3000' : json.loads(MST_DATA3)
    }
    new_code_to_row = {
        'Item_2000' : json.loads(MST_DATA2),
        'Item_3000' : json.loads(MST_DATA3),
        'Item_4000' : json.loads(MST_DATA4)
    }

    transfer_update_file  = data_dir.join('itemmst_update_1.gz')
    transfer_delete_file  = data_dir.join('itemmst_delete_1.gz')

    # execute
    with contextlib.nested (
        _patches(func_init_config),
        _patches_filename(data_dir)
    ) as (mocks, mocks_fn):
        m_s3ls, m_s3get, m_itemconv, m_sftp, m_s3put = mocks

        result = _target()._create_transfer_files(old_code_to_row, new_code_to_row)

    # assert
    assert result == [str(transfer_update_file), str(transfer_delete_file)]
    _assert_py_path_file(transfer_update_file, [TSV_UPDATE_DATA4])
    _assert_py_path_file(transfer_delete_file, [TSV_DELETE_DATA1])

def test_create_update_filename(data_dir, func_init_config):
    with _patches(func_init_config):
        with patch('datetime.datetime', CurrentDatetime):
            result = _target()._create_update_filename()
            assert result == os.path.join(str(data_dir), 'brainpad_product_master_advertiser_id_9999.20131019234059.gz')

def test_create_delete_filename(data_dir, func_init_config):
    with _patches(func_init_config):
        with patch('datetime.datetime', CurrentDatetime):
            result = _target()._create_delete_filename()
            assert result == os.path.join(str(data_dir), 'brainpad_product_delete_advertiser_id_9999.20131019234059.gz')

def test_get_update_rows():
    old_code_to_row = {
        u'Item_1000' : {u'name' : u'アイテム１'},
        u'Item_2000' : {u'name' : u'アイテム２'}
    }
    new_code_to_row = {
        u'Item_4000' : {u'name' : u'アイテム４'},
        u'Item_2000' : {u'name' : u'アイテム弐'}
    }
    result = _target()._get_update_rows(old_code_to_row, new_code_to_row)
    assert result == [
        {u'name' : u'アイテム４'},
        {u'name' : u'アイテム弐'}
    ]

def test_get_delete_codes():
    old_code_to_row = {
        u'Item_1000' : {u'name' : u'アイテム１'},
        u'Item_2000' : {u'name' : u'アイテム２'}
    }
    new_code_to_row = {
        u'Item_2000' : {u'name' : u'アイテム弐'}
    }
    result = _target()._get_delete_codes(old_code_to_row, new_code_to_row)
    assert result == [u'Item_1000']

def test_transform_update_format(func_init_config):
    row = {
        u'aid'      : u'rt_aid',
        u'sid'      : u'rt_sid',
        u'code'     : u'Item_1000',
        u'name'     : u'アイテム１',
        u'imageUrl' : u'http://imageurl/1000.jpg',
        u'url'      : u'http://linkurl/1000',
        u'field0'   : u'道順１',
        u'field1'   : u'概要１',
        u'field4'   : u'￥1,000〜'
    }
    with _patches(func_init_config):
        result = _target()._transform_update_format(row)
        assert result == [
            '9999',
            1000,
            'アイテム１',
            'http://imageurl/1000.jpg',
            'http://linkurl/1000',
            '￥1,000〜',
            '概要１',
            'advertiser_id_9999'
        ]

def test_transform_delete_format(func_init_config):
    with _patches(func_init_config):
        result = _target()._transform_delete_format('Item_1000')
        result == [
            '9999',
            1000,
            'advertiser_id_9999'
        ]

def test_send_transfer_files():
    with patch('lib.sftputil.SftpUtil.__enter__') as m_sftp:
        result = _target()._send_transfer_files(['item1.gz', 'item2.gz'])

        assert result
        assert m_sftp.call_count == 1
        assert m_sftp.return_value.put.call_count == 2
        assert m_sftp.return_value.put.call_args_list[0][0] == ('item1.gz', '/receive/ma')
        assert m_sftp.return_value.put.call_args_list[1][0] == ('item2.gz', '/receive/ma')
        assert m_sftp.return_value.touch.call_count == 2
        assert m_sftp.return_value.touch.call_args_list[0][0] == ('/receive/ma/item1.gz.completed',)
        assert m_sftp.return_value.touch.call_args_list[1][0] == ('/receive/ma/item2.gz.completed',)

def test_send_transfer_files_failed():
    with patch('lib.sftputil.SftpUtil.__enter__') as m_sftp:
        m_sftp.return_value.put.side_effect = Exception('Failed to put file to sftp server')
        result = _target()._send_transfer_files(['item1.gz', 'item2.gz'])

        assert not result
        assert m_sftp.call_count == 1
        assert m_sftp.return_value.put.call_count == 1
        assert m_sftp.return_value.put.call_args_list[0][0] == ('item1.gz', '/receive/ma')
        assert m_sftp.return_value.touch.call_count == 0

def test_execute(data_dir, func_init_config):

    # prepare
    old_mst_file = data_dir.join('itemmst_20130102.json.gz')
    new_mst_file = data_dir.join('itemmst_20130103.json.gz')

    _create_gzip(str(old_mst_file), [MST_DATA1, MST_DATA2, MST_DATA3])
    _create_gzip(str(new_mst_file), [MST_DATA2, MST_DATA3, MST_DATA4])

    transfer_update_file  = data_dir.join('itemmst_update_1.gz')
    transfer_delete_file  = data_dir.join('itemmst_delete_1.gz')

    # execute
    with contextlib.nested (
        _patches(func_init_config),
        _patches_filename(data_dir)
    ) as (mocks, mocks_fn):
        m_s3ls, m_s3get, m_itemconv, m_sftp, m_s3put = mocks

        result = _target().execute()

    # assert
    assert result.is_ok()
    assert result.counter['item.rtoaster.update'] == 1
    assert result.counter['item.rtoaster.delete'] == 1
    assert old_mst_file.check(exists=0)
    assert new_mst_file.check(exists=0)
    assert transfer_update_file.check(exists=0)
    assert transfer_delete_file.check(exists=0)

    assert m_s3ls.call_count == 1
    assert m_s3ls.call_args_list[0][0] == (S3BUCKET, os.path.dirname(S3PATH))

    assert m_s3get.call_count == 2
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, 's3path/itemmst/raw/itemmst_20130102.json.gz', str(data_dir))
    assert m_s3get.call_args_list[1][0] == (S3BUCKET, 's3path/itemmst/raw/itemmst_20130103.json.gz', str(data_dir))

    assert m_itemconv.call_count == 2
    assert m_itemconv.call_args_list[0][0] == (u'Item_4000',)
    assert m_itemconv.call_args_list[1][0] == (u'Item_1000',)

    assert m_sftp.call_count == 1
    assert m_sftp.return_value.put.call_count == 2
    assert m_sftp.return_value.put.call_args_list[0][0] == (str(transfer_update_file), '/receive/ma')
    assert m_sftp.return_value.put.call_args_list[1][0] == (str(transfer_delete_file), '/receive/ma')
    assert m_sftp.return_value.touch.call_count == 2
    assert m_sftp.return_value.touch.call_args_list[0][0] == ('/receive/ma/itemmst_update_1.gz.completed',)
    assert m_sftp.return_value.touch.call_args_list[1][0] == ('/receive/ma/itemmst_delete_1.gz.completed',)

    assert m_s3put.call_count == 2
    assert m_s3put.call_args_list[0][0] == (str(transfer_update_file), S3BUCKET, S3PATH_CNV)
    assert m_s3put.call_args_list[1][0] == (str(transfer_delete_file), S3BUCKET, S3PATH_CNV)


def test_execute_unexpected_error(data_dir, func_init_config):

    # execute
    with contextlib.nested (
        _patches(func_init_config),
        _patches_filename(data_dir)
    ) as (mocks, mocks_fn):
        m_s3ls, m_s3get, m_itemconv, m_sftp, m_s3put = mocks
        m_s3ls.return_value = None
        m_s3ls.side_effect  = Exception('Failed to list s3dir')
        result = _target().execute()

    # assert
    assert result.is_ng()
    assert result.counter['item.rtoaster.update'] == 0
    assert result.counter['item.rtoaster.delete'] == 0

    assert m_s3ls.call_count == 1
    assert m_s3ls.call_args_list[0][0] == (S3BUCKET, os.path.dirname(S3PATH))

    assert m_s3get.call_count    == 0
    assert m_itemconv.call_count == 0
    assert m_sftp.call_count     == 0
    assert m_s3put.call_count    == 0
