# -*- coding: utf-8 -*-


import contextlib
import datetime
import gzip
import json
import os
import pytest
from collections                   import OrderedDict
from requests                      import ConnectionError
from mock                          import patch
from mock                          import PropertyMock
from lib.transferer.ac.segmentdata import SegmentDataTransferer
from lib.transferer.exception      import NoS3Files


ENV = 'dev'
SID = '9999'
PID = 'ac'

S3_BUCKET   = 'dev-transfer-data'
S3_PATH_RAW = '9999/segment_list_data/raw/segmentdata.json.gz'
S3_PATH_CNV = '9999/segment_list_data/converted/ac'

DATA1  = '{"pid": "rtoaster", "sid": "9999", "uid": "rt_uid1", "attrs": [10, 11, 12]}'
DATA2  = '{"pid": "rtoaster", "sid": "9999", "uid": "rt_uid2", "attrs": [20, 21, 22]}'
DATA3  = '{"pid": "rtoaster", "sid": "9999", "uid": "rt_uid3", "attrs": [20, 21, 22]}'

@pytest.fixture
def data_dir(tmp_transfer_dir):
    return tmp_transfer_dir.mkdir(SID).mkdir(PID).mkdir('segmentdata')

@pytest.fixture
def get_init_query_dict():
    with patch('datetime.datetime', CurrentDatetime):
        query_dict = OrderedDict()
        query_dict['document']  = 'TDI'
        query_dict['version']   = '1.0'
        query_dict['source']    = 'jpbpad'
        query_dict['generationEpoch'] = int((datetime.datetime.utcnow() - datetime.datetime(1970, 1, 1)).total_seconds())
        query_dict['users']     = []

    return query_dict

class CurrentDatetime(datetime.datetime):
    @classmethod
    def utcnow(cls):
        return cls(2013, 10, 1, 9, 10, 20, 0)

@contextlib.contextmanager
def _patches_with_default_action(func_init_config, data_dir):
    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('lib.s3util.S3Util.download_single_file'),
        patch('lib.mappingtable.MappingTable.select_latest_mapping_partner_uid_by_user_id_until_n_days'),
        patch('lib.s3util.S3Util.upload_single_file'),
        patch('requests.post'),
        patch('datetime.datetime', CurrentDatetime),
    ) as (m_config, m_s3get, m_mapping, m_s3put, patched_post, m_now):
        m_s3get.side_effect        = [
                                        {'key': '9999/segment_list_data/raw/segmentdata.json.gz'},
                                     ]
        m_mapping.side_effect      = _convert_uid
        type(patched_post.return_value).status_code = PropertyMock(return_value=204)

        yield (m_s3get, m_mapping, m_s3put, patched_post)

def _convert_uid(*args):
    tbl = {'rt_uid1': 'pt_uid1', 'rt_uid2': 'pt_uid2', 'rt_uid3': 'pt_uid3'}
    return tbl.get(args[0])

def _target():
    return SegmentDataTransferer(SID, PID, ENV, S3_BUCKET, S3_PATH_RAW)

def _create_gzip(file, lines):
    with gzip.open(file, 'wb') as f:
        f.write('\n'.join(lines))

def test_transform_transfer_format(func_init_config, data_dir):
    with _patches_with_default_action(func_init_config, data_dir):
        target = _target()
        row = {
                'mapped_uid' : 'testuid',
                'attrs'      : [1, 2, 3],
              }
        result = target._transform_transfer_format(row)

        assert json.loads(result) == {"document":"TDI","version":"1.0","source":"jpbpad","generationEpoch":1380618620,"users":[{"segments":[{"srcSegmentId":"1","qualificationEpoch":1380618620},{"srcSegmentId":"2","qualificationEpoch":1380618620},{"srcSegmentId":"3","qualificationEpoch":1380618620}],"destUserId":"testuid"}]}

def test_send_request_connection_error():
    with patch('requests.post') as patched_post:
        patched_post.side_effect = ConnectionError()

        with pytest.raises(ConnectionError):
            query_str = json.dumps(get_init_query_dict(), ensure_ascii=True, separators=(',', ':'))
            _target()._send_request('http://endpoint', query_str)

def test_send_request_ok(func_init_config, data_dir):
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_mapping, m_s3put, patched_post = mocks

        query_str = json.dumps(get_init_query_dict(), ensure_ascii=True, separators=(',', ':'))
        result = _target()._send_request('http://endpoint', query_str)

        assert result.status_code == 204

def test_init_query_dict():
    with patch('datetime.datetime', CurrentDatetime):
        result = _target()._init_query_dict()

    assert result['document']       == 'TDI'
    assert result['version']        == '1.0'
    assert result['source']         == 'jpbpad'
    assert result['generationEpoch']== 1380618620
    assert result['users']          == []
    assert result == get_init_query_dict()

def test_is_all_sent_with_remaining():
    data = '{"document":"TDI"}'
    result = _target()._is_all_sent(data)

    assert result == False

def test_is_all_sent_without_remaining():
    data = ''
    result = _target()._is_all_sent(data)

    assert result == True

def test_execute_response_status_code_400():
    with patch('requests.post') as patched_post:
        type(patched_post.return_value).status_code = PropertyMock(return_value=400)
        result = _target()._send_request('http://endpoint', '')
        assert result.status_code == 400

def test_execute_response_status_code_503():
    with patch('requests.post') as patched_post:
        type(patched_post.return_value).status_code = PropertyMock(return_value=503)
        result = _target()._send_request('http://endpoint', '')
        assert result.status_code == 503

def test_execute(func_init_config, data_dir):

    # prepare
    data_file      = data_dir.join('segmentdata.json.gz')
    converted_file = data_dir.join('segmentdata_converted.tsv.gz')

    _create_gzip(str(data_file),   [DATA1, DATA2])

    # execute
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_mapping, m_s3put, patched_post = mocks

        result = _target().execute()

    # assert
    assert result.is_ok()
    assert result.counter['user.rtoaster'] == 2
    assert result.counter['user.mapped']   == 2
    assert result.counter['user.unmapped'] == 0
    assert result.counter['user.invalid']  == 0
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3_BUCKET, S3_PATH_RAW, str(data_dir))

    assert m_mapping.call_count == 2
    assert m_mapping.call_args_list[0][0] == (u'rt_uid1', 500)
    assert m_mapping.call_args_list[1][0] == (u'rt_uid2', 500)

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3_BUCKET, S3_PATH_CNV)

def test_execute_s3data_notfound(func_init_config, data_dir):

    # execute
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_mapping, m_s3put, patched_post = mocks
        m_s3get.side_effect = [None]

        result = _target().execute()

    # assert
    assert result.is_ng()
    assert result.counter['user.rtoaster'] == 0
    assert result.counter['user.mapped']   == 0
    assert result.counter['user.unmapped'] == 0
    assert result.counter['user.invalid']  == 0

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3_BUCKET, S3_PATH_RAW, str(data_dir))

    assert m_mapping.call_count == 0
    assert m_s3put.call_count   == 0

def test_execute_unexpected_error(func_init_config, data_dir):

    # prepare
    data_file      = data_dir.join('segmentdata.json.gz')
    transfer_file  = data_dir.join('transfer_file.json.gz')
    converted_file = data_dir.join('segmentdata_converted.tsv.gz')

    _create_gzip(str(data_file),   [DATA1, DATA2])

    # execute
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_mapping, m_s3put, patched_post = mocks
        m_mapping.side_effect = Exception()

        result = _target().execute()

    # assert
    assert result.is_ng()
    assert result.counter['user.rtoaster'] == 0
    assert result.counter['user.mapped']   == 0
    assert result.counter['user.unmapped'] == 0
    assert result.counter['user.invalid']  == 0
    assert data_file.check(exists=0)
    assert transfer_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3_BUCKET, S3_PATH_RAW, str(data_dir))

    assert m_mapping.call_count == 1
    assert m_mapping.call_args_list[0][0] == (u'rt_uid1', 500)

    assert m_s3put.call_count   == 0

def test_execute_post_error(func_init_config, data_dir):

    # prepare
    data_file      = data_dir.join('segmentdata.json.gz')
    transfer_file  = data_dir.join('transfer_file.json.gz')
    converted_file = data_dir.join('segmentdata_converted.tsv.gz')

    _create_gzip(str(data_file),   [DATA1, DATA2, DATA3])

    # execute
    with _patches_with_default_action(func_init_config, data_dir) as mocks:
        m_s3get, m_mapping, m_s3put, patched_post = mocks
        target = _target()
        target.API_SEND_UNIT_PER_REQUEST = 2

        type(patched_post.return_value).status_code = PropertyMock(return_value=500)

        result = target.execute()

    # assert
    assert result.is_ok()
    assert result.counter['user.rtoaster'] == 3
    assert result.counter['user.mapped']   == 3
    assert result.counter['user.unmapped'] == 0
    assert result.counter['user.invalid']  == 0
    assert result.counter['request']       == 2
    assert result.counter['request.204']   == 0
    assert result.counter['request.500']   == 2
    assert data_file.check(exists=0)
    assert transfer_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3_BUCKET, S3_PATH_RAW, str(data_dir))

    assert m_mapping.call_count == 3
    assert m_mapping.call_args_list[0][0] == (u'rt_uid1', 500)
    assert m_mapping.call_args_list[1][0] == (u'rt_uid2', 500)
    assert m_mapping.call_args_list[2][0] == (u'rt_uid3', 500)

    assert m_s3put.call_count == 1

