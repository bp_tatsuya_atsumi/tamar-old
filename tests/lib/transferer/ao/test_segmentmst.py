# -*- coding: utf-8 -*-

from lib.transferer.ao.segmentmst import SegmentMstTransferer

import datetime
import contextlib
import gzip
import os
from mock import patch
import pytest

ENV = 'dev'
SID = '9999'
PID = 'ao'
S3_BUCKET = 's3_bucket'
S3_PATH   = 's3_path/segmentmst/raw'

CSV_HEADER = 'id,name,description,ignore'
CSV_DATA1  = '100,name100,note100,"ignore_partner"'
CSV_DATA2  = '200,name200,note200,"ignore_partner"'
CSV_DATA3  = '300,name300,note300,"ignore_partner"'

@pytest.fixture
def data_dir(tmp_transfer_dir):
    return tmp_transfer_dir.mkdir(SID).mkdir(PID).mkdir('segmentmst')

class CurrentDatetime(datetime.datetime):
    @classmethod
    def now(cls):
        return cls(2013, 10, 1, 9, 10, 20, 0)

@contextlib.contextmanager
def _patches_with_default_action(func_init_config):
    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('lib.s3util.S3Util.download_single_file_order_by_filename'),
        patch('lib.sftputil.put'),
        patch('lib.s3util.S3Util.upload_single_file'),
        patch('datetime.datetime', CurrentDatetime),
    ) as (m_config, m_s3get, m_sftpput, m_s3upload, m_now):
        m_s3get.return_value          = {'key': 's3_path/segmentmst/raw/segment_list_mst_20131001080000_9999.csv.gz'}
        m_sftpput.return_value        = True

        yield (m_s3get, m_sftpput, m_s3upload)

def _init(sid, pid, env, s3_bucket, s3_path):
    return SegmentMstTransferer(sid, pid, env, s3_bucket, s3_path)

def _create_gzip(file, lines):
    with gzip.open(file, 'wb') as f:
        f.write('\n'.join(lines))

def test_get_target_fp(data_dir):
    data_file = data_dir.join('data.csv.gz')
    data_file.write('')

    transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH)
    transferer._get_target_fp(str(data_file))

def test_get_target_fp_localfile_not_exist(data_dir):
    transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH)
    with pytest.raises(IOError):
        transferer._get_target_fp('notexists.csv.gz')

def test_transform_transfer_format():
    test_recs = [{'pid': 'rtoaster', 'sid': '9999', 'id': '1', 'name': 'セグメント名_1', 'description': 'セグメント画用_1'},
                 {'pid': 'rtoaster', 'sid': '9999', 'id': '2', 'name': 'セグメント名_2', 'description': 'セグメント画用_2'},
                 {'pid': 'rtoaster', 'sid': '9999', 'id': '3', 'name': 'セグメント名_3', 'description': 'セグメント画用_3'}]

    transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH)
    result = []
    for rec in test_recs:
        result.append(transferer._transform_transfer_format(rec))

    assert result == ['1\t1\tセグメント名_1\n', '1\t2\tセグメント名_2\n', '1\t3\tセグメント名_3\n']

def test_get_current_time_format():
    transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH)
    transferer.start_time = datetime.date(year=2013,month=10,day=1)
    result = transferer._get_current_time_format()
    assert result == '20131001000000'

def test_create_transfer_filename(func_init_config):
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_sftpput, m_s3upload = mocks
        transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH)
        target_file = 'path/to/segment_list_mst_20131001080000_9999.csv.gz'
        result      = transferer._create_transfer_filename(target_file)

    assert result == 'brainpad_bp_9999_master_20131001091020.tsv'

def test_create_transfer_filename_with_previous_day(func_init_config):
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_sftpput, m_s3upload = mocks
        transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH)
        target_file = 'path/to/segment_list_mst_20130901080000_9999.csv.gz'
        result      = transferer._create_transfer_filename(target_file)

    transferer.remote_dir = '/receive/{YYYYMMDD}'.format(YYYYMMDD='2013-10-01')
    assert result == 'brainpad_bp_9999_master_20131001091020.tsv'

def test_send_transformed_file(func_init_config):
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_sftpput, m_s3upload = mocks
 
        transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH)
        result     = transferer._send_transformed_file('segmentmst')
        assert result

        m_sftpput.side_effect = Exception('exception')
        result = transferer._send_transformed_file('segmentmst')
        assert not result

        m_sftpput.return_value = True

def test_execute(lock_dir, tmp_transfer_dir, data_dir, func_init_config):
    # prepare
    data_file     = data_dir.join('segment_list_mst_20131001080000_9999.csv.gz')
    transfer_file = data_dir.join('brainpad_bp_9999_master_20131001091020.tsv')
    _create_gzip(str(data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2, CSV_DATA3])

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_sftpput, m_s3upload = mocks

        transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH)
        result = transferer.execute()

    # assert
    assert result.is_ok()
    assert result.counter['line.rtoaster'] == 3
    assert data_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == ('s3_bucket', 's3_path/segmentmst/raw', str(data_dir))

    assert m_s3upload.call_count == 1
    assert m_s3upload.call_args_list[0][0][1] == ('s3_bucket')
    assert m_s3upload.call_args_list[0][0][2] == ('s3_path/segmentmst/converted/ao')

    assert m_sftpput.call_count == 1
    assert m_sftpput.call_args_list[0][1]['host']       == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[0][1]['port']       == 115
    assert m_sftpput.call_args_list[0][1]['username']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['password']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['local_file'] == str(transfer_file)
    assert m_sftpput.call_args_list[0][1]['remote_dir'] == '/receive/{YYYYMMDD}'.format(YYYYMMDD='2013-10-01')
    assert m_sftpput.call_args_list[0][1]['rename']     == True

def test_execute_s3file_notfound(lock_dir, tmp_transfer_dir, data_dir, func_init_config):
    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_sftpput, m_s3upload = mocks
        m_s3get.return_value = None

        transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH)
        result = transferer.execute()

    # assert
    assert result.is_ng()
    assert not result.counter

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == ('s3_bucket', 's3_path/segmentmst/raw', str(data_dir))

    assert m_s3upload.call_count    == 0
    assert m_sftpput.call_count == 0

def test_execute_no_csv_header(lock_dir, tmp_transfer_dir, data_dir, func_init_config):
    # prepare
    data_file = data_dir.join('segment_list_mst_20131001080000_9999.csv.gz')
    _create_gzip(str(data_file), [CSV_DATA1, CSV_DATA2, CSV_DATA3])

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_sftpput, m_s3upload = mocks

        transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH)
        result = transferer.execute()

    # assert
    assert result.is_ng()
    assert not result.counter
    assert data_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == ('s3_bucket', 's3_path/segmentmst/raw', str(data_dir))

    assert m_s3upload.call_count == 0
    assert m_sftpput.call_count == 0

def test_execute_invalid_csv_header(lock_dir, tmp_transfer_dir, data_dir, func_init_config):
    # prepare
    data_file = data_dir.join('segment_list_mst_20131001080000_9999.csv.gz')
    _create_gzip(str(data_file), ['foo,bar', CSV_DATA1, CSV_DATA2, CSV_DATA3])

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_sftpput, m_s3upload = mocks

        transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH)
        result = transferer.execute()

    # assert
    assert result.is_ng()
    assert not result.counter
    assert data_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == ('s3_bucket', 's3_path/segmentmst/raw', str(data_dir))

    assert m_s3upload.call_count == 0
    assert m_sftpput.call_count == 0

def test_execute_not_csv_record(lock_dir, tmp_transfer_dir, data_dir, func_init_config):
     # prepare
    data_file     = data_dir.join('segment_list_mst_20131001080000_9999.csv.gz')
    transfer_file = data_dir.join('brainpad_bp_9999_master_20131001091020.tsv')
    _create_gzip(str(data_file), [CSV_HEADER])

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_sftpput, m_s3upload = mocks

        transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH)
        result = transferer.execute()

    # assert
    assert result.is_ok()
    assert not result.counter
    assert data_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == ('s3_bucket', 's3_path/segmentmst/raw', str(data_dir))

    assert m_s3upload.call_count == 1
    assert m_s3upload.call_args_list[0][0][1] == ('s3_bucket')
    assert m_s3upload.call_args_list[0][0][2] == ('s3_path/segmentmst/converted/ao')

    assert m_sftpput.call_count == 1
    assert m_sftpput.call_args_list[0][1]['host']       == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[0][1]['port']       == 115
    assert m_sftpput.call_args_list[0][1]['username']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['password']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['local_file'] == str(transfer_file)
    assert m_sftpput.call_args_list[0][1]['remote_dir'] == '/receive/{YYYYMMDD}'.format(YYYYMMDD='2013-10-01')
    assert m_sftpput.call_args_list[0][1]['rename']     == True

def test_execute_invalid_csv_record(lock_dir, tmp_transfer_dir, data_dir, func_init_config):
     # prepare
    data_file     = data_dir.join('segment_list_mst_20131001080000_9999.csv.gz')
    transfer_file = data_dir.join('brainpad_bp_9999_master_20131001091020.tsv')
    _create_gzip(str(data_file), [CSV_HEADER, CSV_DATA1, '200,name200', '', '300,name300,note300,other300'])

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_sftpput, m_s3upload = mocks

        transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH)
        result = transferer.execute()

    # assert
    assert result.is_ok()
    assert result.counter['line.rtoaster'] == 3
    assert data_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == ('s3_bucket', 's3_path/segmentmst/raw', str(data_dir))

    assert m_s3upload.call_count == 1
    assert m_s3upload.call_args_list[0][0][1] == ('s3_bucket')
    assert m_s3upload.call_args_list[0][0][2] == ('s3_path/segmentmst/converted/ao')

    assert m_sftpput.call_count == 1
    assert m_sftpput.call_args_list[0][1]['host']       == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[0][1]['port']       == 115
    assert m_sftpput.call_args_list[0][1]['username']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['password']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['local_file'] == str(transfer_file)
    assert m_sftpput.call_args_list[0][1]['remote_dir'] == '/receive/{YYYYMMDD}'.format(YYYYMMDD='2013-10-01')
    assert m_sftpput.call_args_list[0][1]['rename']     == True

def test_execute_empty_csv_file(lock_dir, tmp_transfer_dir, data_dir, func_init_config):
     # prepare
    data_file = data_dir.join('segment_list_mst_20131001080000_9999.csv.gz')
    transfer_file = data_dir.join('brainpad_bp_9999_master_20131001091020.tsv')
    _create_gzip(str(data_file), [])

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_sftpput, m_s3upload = mocks

        transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH)
        result = transferer.execute()

    # assert
    assert result.is_ok()
    assert not result.counter
    assert data_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == ('s3_bucket', 's3_path/segmentmst/raw', str(data_dir))

    assert m_s3upload.call_count == 1
    assert m_s3upload.call_args_list[0][0][1] == ('s3_bucket')
    assert m_s3upload.call_args_list[0][0][2] == ('s3_path/segmentmst/converted/ao')

    assert m_sftpput.call_count == 1
    assert m_sftpput.call_args_list[0][1]['host']       == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[0][1]['port']       == 115
    assert m_sftpput.call_args_list[0][1]['username']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['password']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['local_file'] == str(transfer_file)
    assert m_sftpput.call_args_list[0][1]['remote_dir'] == '/receive/{YYYYMMDD}'.format(YYYYMMDD='2013-10-01')
    assert m_sftpput.call_args_list[0][1]['rename']     == True

def test_execute_sftp_error(lock_dir, tmp_transfer_dir, data_dir, func_init_config):
    # prepare
    data_file     = data_dir.join('segment_list_mst_20131001080000_9999.csv.gz')
    transfer_file = data_dir.join('brainpad_bp_9999_master_20131001091020.tsv')
    _create_gzip(str(data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2, CSV_DATA3])

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_sftpput, m_s3upload = mocks
        m_sftpput.side_effect = Exception()

        transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH)
        result = transferer.execute()

    # assert
    assert result.is_ng()
    assert result.counter['line.rtoaster'] == 3
    assert data_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == ('s3_bucket', 's3_path/segmentmst/raw', str(data_dir))

    assert m_s3upload.call_count == 1
    assert m_s3upload.call_args_list[0][0][1] == ('s3_bucket')
    assert m_s3upload.call_args_list[0][0][2] == ('s3_path/segmentmst/converted/ao')

    assert m_sftpput.call_count == 1
    assert m_sftpput.call_args_list[0][1]['host']       == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[0][1]['port']       == 115
    assert m_sftpput.call_args_list[0][1]['username']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['password']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['local_file'] == str(transfer_file)
    assert m_sftpput.call_args_list[0][1]['remote_dir'] == '/receive/{YYYYMMDD}'.format(YYYYMMDD='2013-10-01')
    assert m_sftpput.call_args_list[0][1]['rename']     == True

def test_execute_unexpected_error(lock_dir, tmp_transfer_dir, data_dir, func_init_config):
    pass

