# -*- coding: utf-8 -*-

from lib.transferer.ao.segmentdata import SegmentDataTransferer

import datetime
import contextlib
import gzip
import os
from mock import patch
import pytest

ENV = 'dev'
SID = '9999'
PID = 'ao'
S3_BUCKET = 's3_bucket'
S3_PATH   = 's3_path/segmentdata/raw/segment_list_data_20131001080000_9999.json.gz'

JSON_DATA1 = '{"pid":"brainpad","sid":"9999","uid":"rt_uid1","attrs":["1234567890","0000000000"]}'
JSON_DATA2 = '{"pid":"brainpad","sid":"9999","uid":"rt_uid2","attrs":["1234567890"]}'
JSON_DATA3 = '{"pid":"brainpad","sid":"9099","uid":"rt_uid3","attrs":["0000000000"]}'
JSON_DATA4 = '{"pid":"brainpad","sid":"9099","uid":"rt_uid4_unmapped","attrs":["0000000000"]}'

@pytest.fixture
def data_dir(tmp_transfer_dir):
    return tmp_transfer_dir.mkdir(SID).mkdir(PID).mkdir('segmentdata')

class CurrentDatetime(datetime.datetime):
    @classmethod
    def now(cls):
        return cls(2013, 10, 1, 9, 10, 20, 0)

class CurrentDatetime_before_4(datetime.datetime):
    @classmethod
    def now(cls):
        return cls(2013, 10, 1, 2, 10, 20, 0)

@contextlib.contextmanager
def _patches_with_default_action(func_init_config):
    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('lib.s3util.S3Util.download_single_file'),
        patch('lib.lockfile.LastUpdateFile.is_latest'),
        patch('lib.lockfile.LastUpdateFile.get_last_update_filename'),
        patch('lib.sftputil.put'),
        patch('lib.s3util.S3Util.upload_single_file'),
        patch('lib.mappingtable.MappingTable.select_latest_mapping_partner_uid_by_user_id_until_n_days'),
        patch('datetime.datetime', CurrentDatetime),
    ) as (m_config, m_s3get, m_lastupdfile, m_lastupfilename, m_sftpput, m_s3upload, m_select, m_now):
        m_s3get.return_value          = {'key': 's3_path/segmentdata/raw/segment_list_data_20131001080000_9999.json.gz'}
        m_lastupdfile.return_value    = False
        m_lastupfilename.return_value = None
        m_sftpput.return_value        = True
        m_select.side_effect          = _convert_uid

        yield (m_s3get, m_lastupdfile, m_lastupfilename, m_sftpput, m_s3upload, m_select)

def _init(sid, pid, env, s3_bucket, s3_path):
    return SegmentDataTransferer(sid, pid, env, s3_bucket, s3_path)

def _create_gzip(file, lines):
    with gzip.open(file, 'wb') as f:
        f.write('\n'.join(lines))

def _convert_uid(*args):
     if args[0] == 'rt_uid1':
         return 'pt_uid1'
     if args[0] == 'rt_uid2':
         return 'pt_uid2'
     if args[0] == 'rt_uid3':
         return 'pt_uid3'
     return None

def test_calculate_remote_dir(func_init_config):
    with patch('datetime.datetime', CurrentDatetime):
        transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH)
        result = transferer._calculate_remote_dir()
    assert result == '/receive/2013-10-01'

def test_calculate_remote_dir_before_4(func_init_config):
    with patch('datetime.datetime', CurrentDatetime_before_4):
        transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH)
        result = transferer._calculate_remote_dir()
    assert result == '/receive/2013-09-30'

def test_transform_transfer_format():
    test_recs = [{"pid":"brainpad","sid":"9999","uid":"rt_uid1","mapped_uid":"ao-uid1","attrs":["1234567890","0000000000"]},
                 {"pid":"brainpad","sid":"9999","uid":"rt_uid2","mapped_uid":"ao-uid2","attrs":["1234567890"]},
                 {"pid":"brainpad","sid":"9099","uid":"rt_uid3","mapped_uid":"ao-uid3","attrs":["0000000000"]}]

    transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH)
    result = []
    for rec in test_recs:
        result.append(transferer._transform_transfer_format(rec))

    assert result == ['ao-uid1\t1234567890\nao-uid1\t0000000000\n',
                      'ao-uid2\t1234567890\n',
                      'ao-uid3\t0000000000\n']

def test_get_current_time_format():
    transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH)
    transferer.start_time = datetime.date(year=2013,month=10,day=1)
    result = transferer._get_current_time_format()
    assert result == '20131001000000'

def test_create_transfer_filename(func_init_config):
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_lastupdfile, m_lastupfilename, m_sftpput, m_s3upload, m_select = mocks

        transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH)
        target_file = 'path/to/segment_list_data_20131001080000_9999.json.gz'
        result      = transferer._create_transfer_filename(target_file)
    assert result == 'brainpad_bp_9999_member_20131001091020.tsv.bz2'

def test_send_transformed_file(func_init_config):
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_lastupdfile, m_lastupfilename, m_sftpput, m_s3upload, m_select = mocks

        transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH)
        result     = transferer._send_transformed_file('segmentdata')
        assert result

        m_sftpput.side_effect = Exception('exception')
        result = transferer._send_transformed_file('segmentdata')
        assert not result

        m_sftpput.return_value = True

def test_execute(lock_dir, tmp_transfer_dir, data_dir, func_init_config):
    # prepare
    data_file     = data_dir.join('segment_list_data_20131001080000_9999.json.gz')
    transfer_file = data_dir.join('brainpad_bp_9999_member_20131001091020.tsv.bz2')
    end_file      = data_dir.join('brainpad_bp_9999_endfile')
    _create_gzip(str(data_file), [JSON_DATA1, JSON_DATA2, JSON_DATA3, JSON_DATA4])

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_lastupdfile, m_lastupfilename, m_sftpput, m_s3upload, m_select = mocks

        transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH)

        result = transferer.execute()

    # assert
    assert result.is_ok()
    assert m_select.call_count == 4
    assert result.counter['user.rtoaster'] == 4
    assert result.counter['user.mapped']   == 3
    assert result.counter['user.unmapped'] == 1
    assert result.counter['user.invalid']  == 0
    assert data_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3_BUCKET, S3_PATH, str(data_dir))

    assert m_lastupdfile.call_count == 1
    assert m_lastupdfile.call_args_list[0][0] == ('segment_list_data_20131001080000_9999.json.gz', )

    assert m_s3upload.call_count == 1
    assert m_s3upload.call_args_list[0][0][1] == (S3_BUCKET)
    assert m_s3upload.call_args_list[0][0][2] == ('s3_path/segmentdata/converted/ao')

    assert m_sftpput.call_count == 2
    assert m_sftpput.call_args_list[0][1]['host']       == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[0][1]['port']       == 115
    assert m_sftpput.call_args_list[0][1]['username']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['password']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['local_file'] == str(transfer_file)
    assert m_sftpput.call_args_list[0][1]['remote_dir'] == '/receive/{YYYYMMDD}'.format(YYYYMMDD='2013-10-01')
    assert m_sftpput.call_args_list[0][1]['rename']     == True
    assert m_sftpput.call_args_list[1][1]['host']       == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[1][1]['port']       == 115
    assert m_sftpput.call_args_list[1][1]['username']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[1][1]['password']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[1][1]['local_file'] == str(end_file)
    assert m_sftpput.call_args_list[1][1]['remote_dir'] == '/receive/{YYYYMMDD}'.format(YYYYMMDD='2013-10-01')
    assert m_sftpput.call_args_list[1][1]['rename']     == True

def test_execute_s3file_notfound(lock_dir, tmp_transfer_dir, data_dir, func_init_config):
    end_file = data_dir.join('brainpad_bp_9999_endfile')

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_lastupdfile, m_lastupfilename, m_sftpput, m_s3upload, m_select = mocks
        m_s3get.return_value = None

        transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH)
        result = transferer.execute()

    # assert
    assert result.is_ng()
    assert m_select.call_count == 0

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3_BUCKET, S3_PATH, str(data_dir))

    assert m_lastupdfile.call_count == 0
    assert m_s3upload.call_count    == 0

    assert m_sftpput.call_count == 1
    assert m_sftpput.call_args_list[0][1]['host']       == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[0][1]['port']       == 115
    assert m_sftpput.call_args_list[0][1]['username']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['password']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['local_file'] == str(end_file)
    assert m_sftpput.call_args_list[0][1]['remote_dir'] == '/receive/{YYYYMMDD}'.format(YYYYMMDD='2013-10-01')
    assert m_sftpput.call_args_list[0][1]['rename']     == True

def test_execute_already_transfered(lock_dir, tmp_transfer_dir, data_dir, func_init_config):
    # prepare
    end_file  = data_dir.join('brainpad_bp_9999_endfile')
    data_file = data_dir.join('segment_list_data_20131001080000_9999.json.gz')
    _create_gzip(str(data_file), [JSON_DATA1, JSON_DATA2, JSON_DATA3])

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_lastupdfile, m_lastupfilename, m_sftpput, m_s3upload, m_select = mocks
        m_lastupdfile.return_value = True

        transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH)
        result = transferer.execute()

    # assert
    assert result.is_ok()
    assert m_select.call_count == 0
    assert data_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3_BUCKET, S3_PATH, str(data_dir))

    assert m_lastupdfile.call_count == 1
    assert m_lastupdfile.call_args_list[0][0] == ('segment_list_data_20131001080000_9999.json.gz',)

    assert m_s3upload.call_count == 0

    assert m_sftpput.call_count == 1
    assert m_sftpput.call_args_list[0][1]['host']       == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[0][1]['port']       == 115
    assert m_sftpput.call_args_list[0][1]['username']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['password']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['local_file'] == str(end_file)
    assert m_sftpput.call_args_list[0][1]['remote_dir'] == '/receive/{YYYYMMDD}'.format(YYYYMMDD='2013-10-01')
    assert m_sftpput.call_args_list[0][1]['rename']     == True

def test_has_transfered_today_within_a_day(lock_dir, tmp_transfer_dir, data_dir, func_init_config):
    # prepare
    data_filename = 'segment_list_data_20131001080000_9999.json.gz' # 10/01 8時

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_lastupdfile, m_lastupfilename, m_sftpput, m_s3upload, m_select = mocks
        m_lastupfilename.return_value = 'segment_list_data_20131001073000_9999.json.gz' # 10/01 7時30分
        transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH)
        result = transferer._has_transfered_today(data_filename)

    # assert
    assert result
    assert m_lastupfilename.call_count == 1

def test_has_transfered_today_over_a_day(lock_dir, tmp_transfer_dir, data_dir, func_init_config):
    # prepare
    data_filename = 'segment_list_data_20131001080000_9999.json.gz' # 10/01 8時

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_lastupdfile, m_lastupfilename, m_sftpput, m_s3upload, m_select = mocks
        m_lastupfilename.return_value = 'segment_list_data_20130930090000_9999.json.gz' # 09/30 22時
        transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH)
        result = transferer._has_transfered_today(data_filename)

    # assert
    assert not result
    assert m_lastupfilename.call_count == 1

def test_has_transfered_today_without_updatefile(lock_dir, tmp_transfer_dir, data_dir, func_init_config):
    # prepare
    data_filename = 'segment_list_data_20131001080000_9999.json.gz'

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_lastupdfile, m_lastupfilename, m_sftpput, m_s3upload, m_select = mocks
        m_lastupfilename.return_value = None
        transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH)
        result = transferer._has_transfered_today(data_filename)

    # assert
    assert not result
    assert m_lastupfilename.call_count == 1

def test_has_transfered_today_with_invalid_format(lock_dir, tmp_transfer_dir, data_dir, func_init_config):
    # prepare
    data_filename = 'segment_list_data_2013100120_9999.json.gz'

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_lastupdfile, m_lastupfilename, m_sftpput, m_s3upload, m_select = mocks
        m_lastupfilename.return_value = 'segment_list_data_2013093022_9999.json.gz'
        transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH)
        # assert
        with pytest.raises(ValueError):
            result = transferer._has_transfered_today(data_filename)
    assert m_lastupfilename.call_count == 1

def test_execute_twice_in_a_day(lock_dir, tmp_transfer_dir, data_dir, func_init_config):
    # prepare
    end_file      = data_dir.join('brainpad_bp_9999_endfile')
    data_file     = data_dir.join('segment_list_data_20131001080000_9999.json.gz')
    transfer_file = data_dir.join('brainpad_bp_9999_member_20131001091020.tsv.bz2')

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_lastupdfile, m_lastupfilename, m_sftpput, m_s3upload, m_select = mocks
        m_lastupfilename.return_value = 'segment_list_data_20131001083000_9999.json.gz'
        transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH)
        result = transferer.execute()

    # assert
    assert result.is_ng()
    assert result.counter['user.rtoaster'] == 0
    assert data_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3_BUCKET, S3_PATH, str(data_dir))

def test_execute_invalid_json_record(lock_dir, tmp_transfer_dir, data_dir, func_init_config):
    # prepare
    end_file      = data_dir.join('brainpad_bp_9999_endfile')
    data_file     = data_dir.join('segment_list_data_20131001080000_9999.json.gz')
    transfer_file = data_dir.join('brainpad_bp_9999_member_20131001091020.tsv.bz2')
    _create_gzip(str(data_file), [JSON_DATA1,
                                  '{"pid":"brainpad","sid":"9999","uid":"rt_uid1","attrs":["1234567890"'])

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_lastupdfile, m_lastupfilename, m_sftpput, m_s3upload, m_select = mocks
        transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH)
        result = transferer.execute()

    # assert
    assert result.is_ok()
    assert m_select.call_count == 1
    assert result.counter['user.rtoaster'] == 2
    assert result.counter['user.mapped']   == 1
    assert result.counter['user.unmapped'] == 0
    assert result.counter['user.invalid']  == 1
    assert data_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3_BUCKET, S3_PATH, str(data_dir))

    assert m_lastupdfile.call_count == 1
    assert m_lastupdfile.call_args_list[0][0] == ('segment_list_data_20131001080000_9999.json.gz',)

    assert m_s3upload.call_count == 1
    assert m_s3upload.call_args_list[0][0][1] == (S3_BUCKET)
    assert m_s3upload.call_args_list[0][0][2] == ('s3_path/segmentdata/converted/ao')

    assert m_sftpput.call_count == 2
    assert m_sftpput.call_args_list[0][1]['host']       == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[0][1]['port']       == 115
    assert m_sftpput.call_args_list[0][1]['username']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['password']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['local_file'] == str(transfer_file)
    assert m_sftpput.call_args_list[0][1]['remote_dir'] == '/receive/{YYYYMMDD}'.format(YYYYMMDD='2013-10-01')
    assert m_sftpput.call_args_list[0][1]['rename']     == True
    assert m_sftpput.call_args_list[1][1]['host']       == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[1][1]['port']       == 115
    assert m_sftpput.call_args_list[1][1]['username']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[1][1]['password']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[1][1]['local_file'] == str(end_file)
    assert m_sftpput.call_args_list[1][1]['remote_dir'] == '/receive/{YYYYMMDD}'.format(YYYYMMDD='2013-10-01')
    assert m_sftpput.call_args_list[1][1]['rename']     == True

def test_execute_empty_json_file(lock_dir, tmp_transfer_dir, data_dir, func_init_config):
    # prepare
    end_file  = data_dir.join('brainpad_bp_9999_endfile')
    data_file = data_dir.join('segment_list_data_20131001080000_9999.json.gz')
    transfer_file = data_dir.join('brainpad_bp_9999_member_20131001091020.tsv.bz2')
    _create_gzip(str(data_file), [])

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_lastupdfile, m_lastupfilename, m_sftpput, m_s3upload, m_select = mocks

        transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH)
        result = transferer.execute()

    # assert
    assert result.is_ok()
    assert m_select.call_count == 0
    assert data_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3_BUCKET, S3_PATH, str(data_dir))

    assert m_lastupdfile.call_count == 1
    assert m_lastupdfile.call_args_list[0][0] == ('segment_list_data_20131001080000_9999.json.gz',)

    assert m_s3upload.call_count == 1
    assert m_s3upload.call_args_list[0][0][1] == (S3_BUCKET)
    assert m_s3upload.call_args_list[0][0][2] == ('s3_path/segmentdata/converted/ao')

    assert m_sftpput.call_count == 2
    assert m_sftpput.call_args_list[0][1]['host']       == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[0][1]['port']       == 115
    assert m_sftpput.call_args_list[0][1]['username']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['password']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['local_file'] == str(transfer_file)
    assert m_sftpput.call_args_list[0][1]['remote_dir'] == '/receive/{YYYYMMDD}'.format(YYYYMMDD='2013-10-01')
    assert m_sftpput.call_args_list[0][1]['rename']     == True
    assert m_sftpput.call_args_list[1][1]['host']       == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[1][1]['port']       == 115
    assert m_sftpput.call_args_list[1][1]['username']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[1][1]['password']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[1][1]['local_file'] == str(end_file)
    assert m_sftpput.call_args_list[1][1]['remote_dir'] == '/receive/{YYYYMMDD}'.format(YYYYMMDD='2013-10-01')
    assert m_sftpput.call_args_list[1][1]['rename']     == True

def test_execute_sftp_error(lock_dir, tmp_transfer_dir, data_dir, func_init_config):
    # prepare
    end_file      = data_dir.join('brainpad_bp_9999_endfile')
    data_file     = data_dir.join('segment_list_data_20131001080000_9999.json.gz')
    transfer_file = data_dir.join('brainpad_bp_9999_member_20131001091020.tsv.bz2')
    _create_gzip(str(data_file), [JSON_DATA1, JSON_DATA2, JSON_DATA3])

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3get, m_lastupdfile, m_lastupfilename, m_sftpput, m_s3upload, m_select = mocks
        m_sftpput.side_effect = Exception()

        transferer = _init(SID, PID, ENV, S3_BUCKET, S3_PATH)
        result = transferer.execute()

    # assert
    assert result.is_ng()
    assert m_select.call_count == 3
    assert result.counter['user.rtoaster'] == 3
    assert result.counter['user.mapped']   == 3
    assert result.counter['user.unmapped'] == 0
    assert result.counter['user.invalid']  == 0
    assert data_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3_BUCKET, S3_PATH, str(data_dir))

    assert m_lastupdfile.call_count == 1
    assert m_lastupdfile.call_args_list[0][0] == ('segment_list_data_20131001080000_9999.json.gz',)

    assert m_s3upload.call_count == 1
    assert m_s3upload.call_args_list[0][0][1] == (S3_BUCKET)
    assert m_s3upload.call_args_list[0][0][2] == ('s3_path/segmentdata/converted/ao')

    assert m_sftpput.call_count == 2
    # datafile
    assert m_sftpput.call_args_list[0][1]['host']       == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[0][1]['port']       == 115
    assert m_sftpput.call_args_list[0][1]['username']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['password']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['local_file'] == str(transfer_file)
    assert m_sftpput.call_args_list[0][1]['remote_dir'] == '/receive/{YYYYMMDD}'.format(YYYYMMDD='2013-10-01')
    assert m_sftpput.call_args_list[0][1]['rename']     == True
    # endfile
    assert m_sftpput.call_args_list[1][1]['host']       == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[1][1]['port']       == 115
    assert m_sftpput.call_args_list[1][1]['username']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[1][1]['password']   == 'rtoaster9999'
    assert m_sftpput.call_args_list[1][1]['local_file'] == str(end_file)
    assert m_sftpput.call_args_list[1][1]['remote_dir'] == '/receive/{YYYYMMDD}'.format(YYYYMMDD='2013-10-01')
    assert m_sftpput.call_args_list[1][1]['rename']     == True

def test_execute_unexpected_error(lock_dir, tmp_transfer_dir, data_dir, func_init_config):
    pass

