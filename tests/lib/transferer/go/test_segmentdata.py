# -*- coding: utf-8 -*-

import datetime
import contextlib
import gzip
import json
import os
import pytest
from lib.transferer.exception import NoS3Files
from lib.transferer.go.segmentdata import SegmentDataTransferer
from mock import Mock
from mock import patch

ENV = 'dev'
SID = '9999'
PID = 'go'
S3_BUCKET         = 's3_bucket'
S3_PATH           = 's3_path/segmentdata/raw/split/segmentdata.json.gz'
S3_PATH_CONVERTED = 's3_path/segmentdata/converted/go/split'

JSON_DATA1        = '{"pid": "rtoaster", "sid": "9999", "uid": "rt_uid1", "attrs": ["rt_segid1", "rt_segid2"]}'
JSON_DATA2        = '{"pid": "rtoaster", "sid": "9999", "uid": "rt_uid2", "attrs": ["rt_segid2", "rt_segid3"]}'
JSON_DATA3        = '{"pid": "rtoaster", "sid": "9999", "uid": "rt_uid3", "attrs": ["rt_segid1"]}'
JSON_DATA4        = '{"pid": "rtoaster", "sid": "9999", "uid": "rt_uid4", "attrs": ["rt_segid4"]}'
JSON_DATA_UNKNOWN = '{"pid": "rtoaster", "sid": "9999", "uid": "rt_uid6", "attrs": ["rt_segid1", "rt_segid99"]}'

@pytest.fixture
def data_dir(tmp_transfer_dir):
    return tmp_transfer_dir.mkdir(SID).mkdir(PID).mkdir('segmentdata')

# ここ問題、複数のケースとか
def _convert_uid(*args):
    tbl = {'rt_uid1': ['pt_uid1'], 'rt_uid2': ['pt_uid2', 'pt_uid7'], 'rt_uid3': ['pt_uid3'],
           'rt_uid4': ['pt_uid4'], 'rt_uid6': ['pt_uid6']}
    return tbl.get(args[0])

def _convert_segid(*args):
    tbl = {'10': {'segment_id_to': 'pt_segid10'}, '11': {'segment_id_to': 'pt_segid11'},
           '20': {'segment_id_to': 'pt_segid20'}, '30': {'segment_id_to': 'pt_segid30'}}
    return tbl.get(args[0])

def _select_all_rows():
    return [
            {
                'segment_id_from' : 'rt_segid1',
                'segment_id_to'   : '1',
                'name'            : u'セグメント1',
                'description'     : u'備考1',
                'status'          : 0
            },
            {
                'segment_id_from' : 'rt_segid2',
                'segment_id_to'   : '2',
                'name'            : u'セグメント2',
                'description'     : u'備考2',
                'status'          : 0
            },
            {
                'segment_id_from' : 'rt_segid3',
                'segment_id_to'   : '3',
                'name'            : u'セグメント3',
                'description'     : u'備考3',
                'status'          : 0
            },
            {
                'segment_id_from' : 'rt_segid4',
                'segment_id_to'   : '4',
                'name'            : u'セグメント4',
                'description'     : u'備考4',
                'status'          : 0
            }
           ]

def _create_gzip(file, lines):
    with gzip.open(file, 'wb') as f:
        f.write('\n'.join(lines))

def _create_mock_response(status_code):
    response = Mock()
    response.status_code = status_code
    response.text = '{}'
    response.content = ''
    return response

@contextlib.contextmanager
def _patches_with_default_action(func_init_config):
    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('lib.s3util.S3Util.download_single_file'),
        patch('lib.mappingtable.MappingTable.select_partner_uids_by_user_id'),
        patch('lib.segmentmsttable.SegmentMstTable.select_all_rows'),
        patch('lib.segmentmsttable.SegmentMstTable.select_row_by_segment_id'),
        patch('requests.post'),
        patch('lib.s3util.S3Util.upload_single_file'),
    ) as (m_config, m_s3get, m_mapping, m_segmst_all, m_segmst, m_post, m_s3put):
        m_s3get.return_value = {'key': 's3_path/segmentdata/segmentdata.json.gz'}
        m_mapping.side_effect    = _convert_uid
        m_segmst_all.side_effect = _select_all_rows
        m_segmst.side_effect     = _convert_segid
        m_post.return_value      = _create_mock_response(200)

        yield (m_s3get, m_mapping, m_segmst, m_post, m_s3put)
    

def test_send_requests(tmp_transfer_dir, data_dir, func_init_config):
    data_file = data_dir.join('segmentdata.json.gz')
    _create_gzip(str(data_file), [JSON_DATA1, JSON_DATA2, JSON_DATA3, JSON_DATA4])

    with _patches_with_default_action(func_init_config) as (m_s3get, m_mapping, m_segmst, m_post, m_s3put):
        transferer = SegmentDataTransferer(SID, PID, ENV, S3_BUCKET, S3_PATH)
        transferer._execute_request = Mock()
        transferer._execute_request.return_value = False
        fp_converted = Mock()
        api_error_occurred = transferer._send_requests(str(data_file), fp_converted)

    assert api_error_occurred == False
    assert transferer._execute_request.call_count == 1
    assert transferer.result.counter['segment.known'] == 6
    assert transferer.result.counter['segment.unknown'] == 0

def test_send_requests_with_flush(tmp_transfer_dir, data_dir, func_init_config):
    data_file = data_dir.join('segmentdata.json.gz')
    _create_gzip(str(data_file), [JSON_DATA1, JSON_DATA2, JSON_DATA3, JSON_DATA4])

    with _patches_with_default_action(func_init_config) as (m_s3get, m_mapping, m_segmst, m_post, m_s3put):
        transferer = SegmentDataTransferer(SID, PID, ENV, S3_BUCKET, S3_PATH)
        transferer.FLUSH_LIMIT = 3
        transferer._execute_request = Mock()
        transferer._execute_request.return_value = False
        fp_converted = Mock()
        api_error_occurred = transferer._send_requests(str(data_file), fp_converted)

    assert api_error_occurred == False
    assert transferer._execute_request.call_count == 2
    assert transferer.result.counter['segment.known'] == 6
    assert transferer.result.counter['segment.unknown'] == 0

def test_send_requests_unknown_segment(tmp_transfer_dir, data_dir, func_init_config):
    data_file = data_dir.join('segmentdata.json.gz')
    _create_gzip(str(data_file), [JSON_DATA1, JSON_DATA_UNKNOWN])
    with _patches_with_default_action(func_init_config) as (m_s3get, m_mapping, m_segmst, m_post, m_s3put):
        transferer = SegmentDataTransferer(SID, PID, ENV, S3_BUCKET, S3_PATH)
        transferer._execute_request = Mock()
        transferer._execute_request.return_value = False
        fp_converted = Mock()
        api_error_occurred = transferer._send_requests(str(data_file), fp_converted)

    assert api_error_occurred == False
    assert transferer._execute_request.call_count == 1
    assert transferer.result.counter['segment.known'] == 3
    assert transferer.result.counter['segment.unknown'] == 1
    

def test_send_requests_requests_api_error(tmp_transfer_dir, data_dir, func_init_config):
    data_file = data_dir.join('segmentdata.json.gz')
    _create_gzip(str(data_file), [JSON_DATA1, JSON_DATA2, JSON_DATA3])
    with _patches_with_default_action(func_init_config) as (m_s3get, m_mapping, m_segmst, m_post, m_s3put):
        m_post.return_value = _create_mock_response(400)
        transferer = SegmentDataTransferer(SID, PID, ENV, S3_BUCKET, S3_PATH)
        fp_converted = Mock()
        api_error_occurred = transferer._send_requests(str(data_file), fp_converted)

    assert api_error_occurred == True
    assert m_post.call_count == 3
    # m_post.call_args_listのチェックもしたい(protocol buffersでバイナリになっているためやや面倒)
    assert transferer.result.counter['segment.known'] == 5
    assert transferer.result.counter['segment.unknown'] == 0
    assert transferer.uids_by_segid == {
                 '1': ['pt_uid1', 'pt_uid2', 'pt_uid7', 'pt_uid3'],
                 '2': ['pt_uid1', 'pt_uid2', 'pt_uid7', 'pt_uid3'],
                 '3': ['pt_uid2', 'pt_uid7']
            }
            

def test_execute(tmp_transfer_dir, data_dir, func_init_config):
    data_file = data_dir.join('segmentdata.json.gz')
    _create_gzip(str(data_file), [JSON_DATA1, JSON_DATA2, JSON_DATA3])
    converted_file = data_dir.join('segmentdata_converted.tsv.gz')
    with _patches_with_default_action(func_init_config) as (m_s3get, m_mapping, m_segmst, m_post, m_s3put):
        transferer = SegmentDataTransferer(SID, PID, ENV, S3_BUCKET, S3_PATH)
        result = transferer.execute()

    assert result.is_ok()
    assert result.counter['user.rtoaster'] == 3
    assert result.counter['user.mapped']   == 4
    assert result.counter['user.unmapped'] == 0
    assert result.counter['request.200']   == 3
    assert transferer.result.counter['segment.known']   == 5
    assert transferer.result.counter['segment.unknown'] == 0
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == ('s3_bucket', S3_PATH, str(data_dir))

    assert m_mapping.call_count == 3
    assert m_mapping.call_args_list[0][0] == (u'rt_uid1',)
    assert m_mapping.call_args_list[1][0] == (u'rt_uid2',)
    assert m_mapping.call_args_list[2][0] == (u'rt_uid3',)

    assert m_post.call_count == 3
    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3_BUCKET, S3_PATH_CONVERTED)


def test_execute_with_unknown_segment(tmp_transfer_dir, data_dir, func_init_config):
    data_file = data_dir.join('segmentdata.json.gz')
    _create_gzip(str(data_file), [JSON_DATA1, JSON_DATA_UNKNOWN])
    converted_file = data_dir.join('segmentdata_converted.tsv.gz')

    with _patches_with_default_action(func_init_config) as (m_s3get, m_mapping, m_segmst, m_post, m_s3put):
        transferer = SegmentDataTransferer(SID, PID, ENV, S3_BUCKET, S3_PATH)
        result = transferer.execute()

    assert result.is_ok()
    assert result.counter['user.rtoaster'] == 2
    assert result.counter['user.mapped']   == 2
    assert result.counter['user.unmapped'] == 0
    assert result.counter['request.200']   == 2
    assert transferer.result.counter['segment.known']   == 3
    assert transferer.result.counter['segment.unknown'] == 1
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)
    
    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == ('s3_bucket', S3_PATH, str(data_dir))

    assert m_mapping.call_count == 2
    assert m_mapping.call_args_list[0][0] == (u'rt_uid1',)
    assert m_mapping.call_args_list[1][0] == (u'rt_uid6',)

    assert m_post.call_count == 2
    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(converted_file), S3_BUCKET, S3_PATH_CONVERTED)
