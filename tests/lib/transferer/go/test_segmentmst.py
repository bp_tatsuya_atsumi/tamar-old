# -*- coding: utf-8 -*-

import contextlib
import gzip
from mock import Mock
from mock import patch
import pytest

from lib.transferer.exception import NoS3Files
from lib.transferer.go.segmentmst import SegmentMstTransferer

ENV = 'dev'
SID = '9999'
PID = 'go'

S3BUCKET   = 's3_bucket'
S3FILE     = 's3_path/segmentmst/raw/segmentmst.csv.gz'
S3DIR_CONV = 's3_path/segmentmst/converted/go'

CSV_HEADER = 'id,name,description,ignore'
CSV_DATA1  = '100,name100,note100,"ignore_partner"'
CSV_DATA2  = '200,name200,note200,"ignore_partner"'
CSV_DATA3  = '300,name300,note300,"ignore_partner"'
CSV_DATA4  = '400,name400,note400,"ignore_partner"'

CSV_DATA1_NAME_MODIFIED = '100,name100_mofified,note100'
CSV_DATA1_DESCRIPTION_MODIFIED = '100,name100,note100_modified'

@pytest.fixture
def data_dir(tmp_transfer_dir):
    return tmp_transfer_dir.mkdir(SID).mkdir(PID).mkdir('segmentmst')

def _create_gzip(file, lines):
    with gzip.open(file, 'wb') as f:
        f.write('\n'.join(lines))

@contextlib.contextmanager
def _patches(func_init_config):
    with contextlib.nested(
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('lib.s3util.S3Util.download_single_file'),
        patch('lib.s3util.S3Util.upload_single_file'),
        patch('lib.segmentmsttable.SegmentMstTable.select_all_rows'),
        patch('lib.segmentmsttable.SegmentMstTable.insert_row'),
        patch('lib.segmentmsttable.SegmentMstTable.delete_rows_by_segment_id'),
        patch('lib.segmentmsttable.SegmentMstTable.update_row_by_segment_id'),
        patch('lib.partner_api.go.googleads.ddp.DdpClient'),
    ) as (m_config, m_s3get, m_s3put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_ddp):
        m_s3get.return_value = {'key': S3FILE}
        m_segmst_all.return_value = [
            { 'segment_id_from': '100', 'segment_id_to': 'pt100', 'name': 'name100', 'description': 'note100', 'status': 0 },
            { 'segment_id_from': '200', 'segment_id_to': 'pt200', 'name': 'name200', 'description': 'note200', 'status': 0 },
            { 'segment_id_from': '300', 'segment_id_to': 'pt300', 'name': 'name300', 'description': 'note300', 'status': 0 },
        ]

        yield (m_s3get, m_s3put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_ddp)

def test_execute_no_diff(tmp_transfer_dir, data_dir, func_init_config):
    data_file = data_dir.join('segmentmst.csv.gz')
    _create_gzip(str(data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2, CSV_DATA3])
    converted_file = data_dir.join('segment_converted.tsv.gz')

    with _patches(func_init_config) as (m_s3get, m_s3put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_ddp):
        result = SegmentMstTransferer(SID, PID, ENV, S3BUCKET, S3FILE).execute()

    assert result.is_ok()
    assert result.counter['segment_modified'] == 0
    assert result.counter['segment.new']      == 0
    assert result.counter['segment.deleted']  == 0
    assert result.counter['request']          == 0
    assert result.counter['result.success']   == 0
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)
    assert m_s3get.call_count                 == 1
    assert m_s3get.call_args_list[0][0]       == (S3BUCKET, S3FILE, str(data_dir))
    assert m_segmst_del.call_count            == 0
    assert m_segmst_ins.call_count            == 0
    assert m_segmst_update.call_count         == 0
    assert m_s3put.call_count                 == 1

def test_execute_no_diff_description_modified(tmp_transfer_dir, data_dir, func_init_config):
    data_file = data_dir.join('segmentmst.csv.gz')
    _create_gzip(str(data_file), [CSV_HEADER, CSV_DATA1_DESCRIPTION_MODIFIED, CSV_DATA2, CSV_DATA3])
    converted_file = data_dir.join('segment_converted.tsv.gz')

    with _patches(func_init_config) as (m_s3get, m_s3put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_ddp):
        result = SegmentMstTransferer(SID, PID, ENV, S3BUCKET, S3FILE).execute()

    assert result.is_ok()
    assert result.counter['segment.modified'] == 0
    assert result.counter['segment.new']      == 0
    assert result.counter['segment.deleted']  == 0
    assert result.counter['request']          == 0
    assert result.counter['result.success']   == 0
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)
    assert m_s3get.call_count                 == 1
    assert m_s3get.call_args_list[0][0]       == (S3BUCKET, S3FILE, str(data_dir))
    assert m_segmst_all.call_count            == 1
    assert m_segmst_del.call_count            == 0
    assert m_segmst_ins.call_count            == 0
    assert m_segmst_update.call_count         == 0
    assert m_s3put.call_count                 == 1

def test_execute_add_segment(tmp_transfer_dir, data_dir, func_init_config):
    data_file = data_dir.join('segmentmst.csv.gz')
    _create_gzip(str(data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2, CSV_DATA3, CSV_DATA4])
    converted_file = data_dir.join('segment_converted.tsv.gz')

    with _patches(func_init_config) as (m_s3get, m_s3put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_ddp):
        m_ddp.LoadFromStorage().GetService().mutate.return_value = {'value': [{'id': '12345'}]}
        result = SegmentMstTransferer(SID, PID, ENV, S3BUCKET, S3FILE).execute()

    assert result.is_ok()
    assert result.counter['segment.new']      == 1
    assert result.counter['segment.modified'] == 0
    assert result.counter['segment.deleted']  == 0
    assert result.counter['request']          == 1
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)
    assert m_s3get.call_count                 == 1
    assert m_s3get.call_args_list[0][0]       == (S3BUCKET, S3FILE, str(data_dir))
    assert m_segmst_all.call_count            == 1
    assert m_segmst_del.call_count            == 0
    assert m_segmst_ins.call_count            == 1
    assert m_segmst_update.call_count         == 0
    assert m_s3put.call_count                 == 1

    assert m_ddp.LoadFromStorage().GetService().mutate.call_count == 1


def test_execute_delete_segment(tmp_transfer_dir, data_dir, func_init_config):
    data_file = data_dir.join('segmentmst.csv.gz')
    _create_gzip(str(data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2])
    converted_file = data_dir.join('segment_converted.tsv.gz')

    with _patches(func_init_config) as (m_s3get, m_s3put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_ddp):
        m_ddp.LoadFromStorage().GetService().mutate.return_value = {'value': [{'id': '12345'}]}
        result = SegmentMstTransferer(SID, PID, ENV, S3BUCKET, S3FILE).execute()

    assert result.is_ok()
    assert result.counter['segment.new']      == 0
    assert result.counter['segment.modified'] == 0
    assert result.counter['segment.deleted']  == 1
    assert result.counter['request']          == 1
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)
    assert m_s3get.call_count                 == 1
    assert m_s3get.call_args_list[0][0]       == (S3BUCKET, S3FILE, str(data_dir))
    assert m_segmst_all.call_count            == 1
    assert m_segmst_del.call_count            == 1 
    assert m_segmst_ins.call_count            == 0
    assert m_segmst_update.call_count         == 0
    assert m_s3put.call_count                 == 1

    assert m_ddp.LoadFromStorage().GetService().mutate.call_count == 1


def test_execute_no_dif_modified_description(tmp_transfer_dir, data_dir, func_init_config):
    data_file = data_dir.join('segmentmst.csv.gz')
    _create_gzip(str(data_file), [CSV_HEADER, CSV_DATA1_DESCRIPTION_MODIFIED, CSV_DATA2, CSV_DATA3])
    converted_file = data_dir.join('segment_converted.tsv.gz')

    with _patches(func_init_config) as (m_s3get, m_s3put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_ddp):
        result = SegmentMstTransferer(SID, PID, ENV, S3BUCKET, S3FILE).execute()

        assert result.is_ok()
        assert result.counter['segment.modified'] == 0
        assert result.counter['segment.new']      == 0
        assert result.counter['segment.deleted']  == 0
        assert result.counter['request']          == 0
        assert data_file.check(exists=0)
        assert converted_file.check(exists=0)
        assert m_s3get.call_count                 == 1
        assert m_s3get.call_args_list[0][0]       == (S3BUCKET, S3FILE, str(data_dir))
        assert m_segmst_all.call_count            == 1
        assert m_segmst_del.call_count            == 0
        assert m_segmst_ins.call_count            == 0
        assert m_segmst_update.call_count         == 0
        assert m_s3put.call_count                 == 1

def test_execute_modify_segment(tmp_transfer_dir, data_dir, func_init_config):
    data_file = data_dir.join('segmentmst.csv.gz')
    _create_gzip(str(data_file), [CSV_HEADER, CSV_DATA1_NAME_MODIFIED, CSV_DATA2, CSV_DATA3])
    converted_file = data_dir.join('segment_converted.tsv.gz')

    with _patches(func_init_config) as (m_s3get, m_s3put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_ddp):
        m_ddp.LoadFromStorage().GetService().mutate.return_value = {'value': [{'id': '12345'}]}
        result = SegmentMstTransferer(SID, PID, ENV, S3BUCKET, S3FILE).execute()

    assert result.is_ok()
    assert result.counter['segment.modified'] == 1
    assert result.counter['segment.new']      == 0
    assert result.counter['segment.deleted']  == 0
    assert result.counter['request']          == 1
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)
    assert m_s3get.call_count                 == 1
    assert m_s3get.call_args_list[0][0]       == (S3BUCKET, S3FILE, str(data_dir))
    assert m_segmst_all.call_count            == 1
    assert m_segmst_del.call_count            == 0
    assert m_segmst_ins.call_count            == 0
    assert m_segmst_update.call_count         == 1
    assert m_s3put.call_count                 == 1

    assert m_ddp.LoadFromStorage().GetService().mutate.call_count == 1


def test_execute_add_delete_modify_segment(tmp_transfer_dir, data_dir, func_init_config):
    data_file = data_dir.join('segmentmst.csv.gz')
    _create_gzip(str(data_file), [CSV_HEADER, CSV_DATA1_NAME_MODIFIED, CSV_DATA3, CSV_DATA4])
    converted_file = data_dir.join('segment_converted.tsv.gz')

    with _patches(func_init_config) as (m_s3get, m_s3put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_ddp):
        m_ddp.LoadFromStorage().GetService().mutate.return_value = {'value': [{'id': '12345'}]}
        result = SegmentMstTransferer(SID, PID, ENV, S3BUCKET, S3FILE).execute()

    assert result.is_ok()
    assert result.counter['segment.modified'] == 1
    assert result.counter['segment.new']      == 1
    assert result.counter['segment.deleted']  == 1
    assert result.counter['request']          == 3
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)
    assert m_s3get.call_count                 == 1
    assert m_s3get.call_args_list[0][0]       == (S3BUCKET, S3FILE, str(data_dir))
    assert m_segmst_all.call_count            == 1
    assert m_segmst_del.call_count            == 1
    assert m_segmst_ins.call_count            == 1
    assert m_segmst_update.call_count         == 1
    assert m_s3put.call_count                 == 1

    assert m_ddp.LoadFromStorage().GetService().mutate.call_count == 3


def test_execute_s3file_notfound(tmp_transfer_dir, data_dir, func_init_config):
    with _patches(func_init_config) as (m_s3get, m_s3put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_ddp):
        result = SegmentMstTransferer(SID, PID, ENV, S3BUCKET, S3FILE).execute()

    assert result.is_ng()
    assert not result.counter
    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))


def test_execute_no_csv_header(tmp_transfer_dir, data_dir, func_init_config):
    data_file = data_dir.join('segmentmst.csv.gz')
    _create_gzip(str(data_file), [CSV_DATA1, CSV_DATA3, CSV_DATA4])
    converted_file = data_dir.join('segment_converted.tsv.gz')

    with _patches(func_init_config) as (m_s3get, m_s3put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_ddp):
        result = SegmentMstTransferer(SID, PID, ENV, S3BUCKET, S3FILE).execute()

    assert result.is_ng()
    assert not result.counter
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)
    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))
    assert m_segmst_all.call_count      == 1
    assert m_segmst_del.call_count      == 0
    assert m_segmst_ins.call_count      == 0
    assert m_segmst_update.call_count   == 0
    assert m_s3put.call_count           == 0

def test_execute_invalid_csv_header(tmp_transfer_dir, data_dir, func_init_config):
    data_file = data_dir.join('segmentmst.csv.gz')
    _create_gzip(str(data_file), [CSV_DATA1, CSV_DATA3, CSV_DATA4])
    converted_file = data_dir.join('segment_converted.tsv.gz')

    with _patches(func_init_config) as (m_s3get, m_s3put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_ddp):
        result = SegmentMstTransferer(SID, PID, ENV, S3BUCKET, S3FILE).execute()

    assert result.is_ng()
    assert not result.counter
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)
    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))
    assert m_segmst_all.call_count      == 1
    assert m_segmst_del.call_count      == 0
    assert m_segmst_ins.call_count      == 0
    assert m_segmst_update.call_count   == 0
    assert m_s3put.call_count           == 0


def test_execute_no_csv_record(tmp_transfer_dir, data_dir, func_init_config):
    data_file = data_dir.join('segmentmst.csv.gz')
    _create_gzip(str(data_file), [CSV_HEADER])
    converted_file = data_dir.join('segment_converted.tsv.gz')

    with _patches(func_init_config) as (m_s3get, m_s3put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_ddp):
        m_ddp.LoadFromStorage().GetService().mutate.return_value = {'value': [{'id': '12345'}]}
        result = SegmentMstTransferer(SID, PID, ENV, S3BUCKET, S3FILE).execute()

    assert result.is_ok()
    assert result.counter['segment.modified'] == 0
    assert result.counter['segment.new']      == 0
    assert result.counter['segment.deleted']  == 3
    assert result.counter['request']          == 3
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)
    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))
    assert m_segmst_all.call_count            == 1
    assert m_segmst_del.call_count            == 3
    assert m_segmst_ins.call_count            == 0
    assert m_segmst_update.call_count         == 0
    assert m_s3put.call_count                 == 1

    assert m_ddp.LoadFromStorage().GetService().mutate.call_count == 3

def test_execute_invalid_csv_record(tmp_transfer_dir, data_dir, func_init_config):
    data_file = data_dir.join('segmentmst.csv.gz')
    _create_gzip(str(data_file), [CSV_HEADER, CSV_DATA1, '200,name200', '', '300,name300,note300,other300'])
    converted_file = data_dir.join('segment_converted.tsv.gz')

    with _patches(func_init_config) as (m_s3get, m_s3put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_ddp):
        result = SegmentMstTransferer(SID, PID, ENV, S3BUCKET, S3FILE).execute()

    assert result.is_ok()
    assert result.counter['segment.modified'] == 0
    assert result.counter['segment.new']      == 0
    assert result.counter['segment.deleted']  == 0
    assert result.counter['request']          == 0
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)
    assert m_s3get.call_count                 == 1
    assert m_s3get.call_args_list[0][0]       == (S3BUCKET, S3FILE, str(data_dir))
    assert m_segmst_all.call_count            == 1
    assert m_segmst_del.call_count            == 0
    assert m_segmst_ins.call_count            == 0
    assert m_segmst_update.call_count         == 0
    assert m_s3put.call_count                 == 1


def test_execute_empty_csv_file(tmp_transfer_dir, data_dir, func_init_config):
    data_file = data_dir.join('segmentmst.csv.gz')
    _create_gzip(str(data_file), [])
    converted_file = data_dir.join('segment_converted.tsv.gz')

    with _patches(func_init_config) as (m_s3get, m_s3put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_ddp):
        m_ddp.LoadFromStorage().GetService().mutate.return_value = {'value': [{'id': '12345'}]}
        result = SegmentMstTransferer(SID, PID, ENV, S3BUCKET, S3FILE).execute()

    assert result.is_ok()
    assert result.counter['segment.modified'] == 0
    assert result.counter['segment.new']      == 0
    assert result.counter['segment.deleted']  == 3
    assert result.counter['request']          == 3
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)
    assert m_s3get.call_count                 == 1
    assert m_s3get.call_args_list[0][0]       == (S3BUCKET, S3FILE, str(data_dir))
    assert m_segmst_all.call_count            == 1
    assert m_segmst_del.call_count            == 3
    assert m_segmst_ins.call_count            == 0
    assert m_segmst_update.call_count         == 0
    assert m_s3put.call_count                 == 1


def test_execute_api_error(tmp_transfer_dir, data_dir, func_init_config):
    data_file = data_dir.join('segmentmst.csv.gz')
    _create_gzip(str(data_file), [CSV_HEADER, CSV_DATA1, CSV_DATA2, CSV_DATA3, CSV_DATA4])
    converted_file = data_dir.join('segment_converted.tsv.gz')

    with _patches(func_init_config) as (m_s3get, m_s3put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_ddp):
        m_ddp.LoadFromStorage().GetService().mutate.side_effect = Exception
        result = SegmentMstTransferer(SID, PID, ENV, S3BUCKET, S3FILE).execute()

    assert result.api_error()
    assert result.counter['segment.modified'] == 0
    assert result.counter['segment.new']      == 1
    assert result.counter['segment.deleted']  == 0
    assert result.counter['request']          == 1
    assert data_file.check(exists=0)
    assert converted_file.check(exists=0)
    assert m_s3get.call_count                 == 1
    assert m_s3get.call_args_list[0][0]       == (S3BUCKET, S3FILE, str(data_dir))
    assert m_segmst_all.call_count            == 1
    assert m_segmst_del.call_count            == 0
    assert m_segmst_ins.call_count            == 0
    assert m_segmst_update.call_count         == 0
    assert m_s3put.call_count                 == 1


def test_add_segments(tmp_transfer_dir, data_dir, func_init_config):
    converted_file = data_dir.join('segment_converted.tsv.gz')
    with _patches(func_init_config) as (m_s3get, m_s3put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_ddp):
        m_ddp.LoadFromStorage().GetService().mutate.return_value = {'value': [{'id': '12345'}]}
        fp_converted = Mock()
        transferer = SegmentMstTransferer(SID, PID, ENV, S3BUCKET, S3FILE)
        transferer._add_segments([{'id': 'segid', 'name': 'segname', 'description': 'note'}], fp_converted)
    
    assert m_ddp.LoadFromStorage().GetService().mutate.call_count == 1
    assert m_segmst_ins.call_count == 1
    assert m_segmst_ins.call_args_list[0][0] == ('segid', '12345', 'segname', 'note')


def test_add_segments_db_fail(tmp_transfer_dir, data_dir, func_init_config):
    converted_file = data_dir.join('segment_converted.tsv.gz')
    with _patches(func_init_config) as (m_s3get, m_s3put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_ddp):
        fp_converted = Mock()
        m_segmst_ins.side_effect = Exception
        m_ddp.LoadFromStorage().GetService().mutate.return_value = {'value': [{'id': '12345'}]}
        transferer = SegmentMstTransferer(SID, PID, ENV, S3BUCKET, S3FILE)
        transferer._add_segments([{'id': 'segid', 'name': 'segname', 'description': 'note'}], fp_converted)
    assert m_ddp.LoadFromStorage().GetService().mutate.call_count == 1
    assert m_segmst_ins.call_count == 1
    assert m_segmst_ins.call_args_list[0][0] == ('segid', '12345', 'segname', 'note')

def test_add_segments_soap_fail(tmp_transfer_dir, data_dir, func_init_config):
    converted_file = data_dir.join('segment_converted.tsv.gz')
    with _patches(func_init_config) as (m_s3get, m_s3put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_ddp):
        fp_converted = Mock()
        m_ddp.LoadFromStorage().GetService().mutate.side_effect = Exception()
        transferer = SegmentMstTransferer(SID, PID, ENV, S3BUCKET, S3FILE)
        transferer._add_segments([{'id': 'segid', 'name': 'segname', 'description': 'note'}], fp_converted)
    assert m_ddp.LoadFromStorage().GetService().mutate.call_count == 1
    assert m_segmst_ins.call_count == 0


def test_modify_segments(tmp_transfer_dir, data_dir, func_init_config):
    with _patches(func_init_config) as (m_s3get, m_s3put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_ddp):
        fp_converted = Mock()
        m_ddp.LoadFromStorage().GetService().mutate.return_value = {'value': [{'id': '12345'}]}
        transferer = SegmentMstTransferer(SID, PID, ENV, S3BUCKET, S3FILE)
        transferer._modify_segments(
            [{'segment_id_from': 'segid', 'name': 'segname', 'name_modified': 'segname_rename',
              'description': 'note', 'status': 0, 'segment_id_to': 'pt-segid'}],
            fp_converted
        )
    assert m_ddp.LoadFromStorage().GetService().mutate.call_count == 1
    assert m_segmst_update.call_count == 1
    assert m_segmst_update.call_args_list[0][0] == ('segid', {'name': 'segname_rename'})

def test_modify_segments_db_fail(tmp_transfer_dir, data_dir, func_init_config):
    with _patches(func_init_config) as (m_s3get, m_s3put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_ddp):
        fp_converted = Mock()
        m_ddp.LoadFromStorage().GetService().mutate.return_value = {'value': [{'id': '12345'}]}
        transferer = SegmentMstTransferer(SID, PID, ENV, S3BUCKET, S3FILE)
        m_segmst_update.side_effect = Exception()
        transferer._modify_segments(
            [{'segment_id_from': 'segid', 'name': 'segname', 'name_modified': 'segname_rename',
              'description': 'note', 'status': 0, 'segment_id_to': 'pt-segid'}],
            fp_converted
        )
    assert m_ddp.LoadFromStorage().GetService().mutate.call_count == 1
    assert m_segmst_update.call_count == 1
    assert m_segmst_update.call_args_list[0][0] == ('segid', {'name': 'segname_rename'})
        

def test_modify_segments_soap_fail(tmp_transfer_dir, data_dir, func_init_config):
    with _patches(func_init_config) as (m_s3get, m_s3put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_ddp):
        fp_converted = Mock()
        m_ddp.LoadFromStorage().GetService().mutate.side_effect = Exception()
        transferer = SegmentMstTransferer(SID, PID, ENV, S3BUCKET, S3FILE)
        transferer._modify_segments(
            [{'segment_id_from': 'segid', 'name': 'segname', 'name_modified': 'segname_rename',
              'description': 'note', 'status': 0, 'segment_id_to': 'pt-segid'}],
            fp_converted
        )
    assert m_ddp.LoadFromStorage().GetService().mutate.call_count == 1
    assert m_segmst_update.call_count == 0
    

def test_delete_segments(tmp_transfer_dir, data_dir, func_init_config):
    with _patches(func_init_config) as (m_s3get, m_s3put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_ddp):
        fp_converted = Mock()
        m_ddp.LoadFromStorage().GetService().mutate.return_value = {'value': [{'id': '12345'}]}
        transferer = SegmentMstTransferer(SID, PID, ENV, S3BUCKET, S3FILE)
        transferer._delete_segments(
            [{'segment_id_from': 'segid', 'name': 'segname', 'description': 'note', 'status': 0, 'segment_id_to': 'pt-segid'}],
            fp_converted
        )
    assert m_ddp.LoadFromStorage().GetService().mutate.call_count == 1
    assert m_segmst_del.call_count == 1
    assert m_segmst_del.call_args_list[0][0] == ('segid',)


def test_delete_segments_db_fail(tmp_transfer_dir, data_dir, func_init_config):
    with _patches(func_init_config) as (m_s3get, m_s3put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_ddp):
        fp_converted = Mock()
        m_ddp.LoadFromStorage().GetService().mutate.return_value = {'value': [{'id': '12345'}]}
        transferer = SegmentMstTransferer(SID, PID, ENV, S3BUCKET, S3FILE)
        m_segmst_del.side_effect = Exception()
        transferer._delete_segments(
            [{'segment_id_from': 'segid', 'name': 'segname', 'description': 'note', 'status': 0, 'segment_id_to': 'pt-segid'}],
            fp_converted
        )
    assert m_ddp.LoadFromStorage().GetService().mutate.call_count == 1
    assert m_segmst_del.call_count == 1
    assert m_segmst_del.call_args_list[0][0] == ('segid',)

def test_delete_segments_soap_fail(tmp_transfer_dir, data_dir, func_init_config):
    with _patches(func_init_config) as (m_s3get, m_s3put, m_segmst_all, m_segmst_ins, m_segmst_del, m_segmst_update, m_ddp):
        fp_converted = Mock()
        m_ddp.LoadFromStorage().GetService().mutate.side_effect == Exception()
        transferer = SegmentMstTransferer(SID, PID, ENV, S3BUCKET, S3FILE)
        transferer._delete_segments(
            [{'segment_id_from': 'segid', 'name': 'segname', 'description': 'note', 'status': 0, 'segment_id_to': 'pt-segid'}],
            fp_converted
        )
    assert m_ddp.LoadFromStorage().GetService().mutate.call_count == 1
    assert m_segmst_del.call_count == 0

