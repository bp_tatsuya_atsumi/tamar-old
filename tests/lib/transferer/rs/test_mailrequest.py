# -*- coding: utf-8 -*-

from lib.transferer.rs.mailrequest import MailRequestTransferer

import contextlib
import datetime
import gzip
import os
from   mock       import patch
import pytest


SID           = '9999'
PID           = 'rs'
ENV           = 'dev'
S3BUCKET      = 's3bucket'
S3PATH        = 's3path/mail_request_data/raw/20140101_pattern.csv'
S3DIR_CNV     = 's3path/mail_request_data/converted/rs'

CSV_HEADER_RS = '"MEMBER_ID"'
CSV_DATA1_RS  = '"pt_uid1"'
CSV_DATA2_RS  = '"pt_uid2"'
CSV_DATA3_RS  = '"pt_uid3"'

CSV_HEADER_RT = '"MEMBER_ID"'
CSV_DATA1_RT  = '"rt_uid1"'
CSV_DATA2_RT  = '"rt_uid2"'

class CurrentDatetime(datetime.datetime):
    @classmethod
    def now(cls):
        return cls(2014, 2, 25, 14, 40, 59, 0)

@pytest.fixture
def data_dir(tmp_transfer_dir):
    return tmp_transfer_dir.mkdir(SID).mkdir(PID).mkdir('mailrequest')

@contextlib.contextmanager
def _patches(func_init_config):
    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('lib.s3util.S3Util.download_single_file'),
        patch('lib.mappingtable.MappingTable.select_rows_by_partner_user_id'),
        patch('lib.s3util.S3Util.upload_single_file'),
        patch('lib.sftputil.put'),
        patch('lib.util.remove_files'),
        patch('datetime.datetime', CurrentDatetime)
    ) as (m_config, m_s3get, m_mapping, m_s3put, m_sftpput, m_rm, m_now):
        m_s3get.return_value  = {'key':S3PATH}
        m_mapping.side_effect = _convert_uid

        yield (m_s3get, m_mapping, m_s3put, m_sftpput, m_rm)

def _target():
    return MailRequestTransferer(SID, PID, ENV, S3BUCKET, S3PATH)

def _convert_uid(*args):
    tbl = {
        'pt_uid1': [['rt_uid1']],
        'pt_uid2': [['rt_uid1'], ['rt_uid2']],
    }
    return tbl.get(args[0])

def _assert_py_path_file(py_path_file, lines):
    assert py_path_file.check(file=1, exists=1)
    content = '\r\n'.join(lines)
    if content:
        content = content + '\r\n'
    assert gzip.open(str(py_path_file), 'rb').read() == content

def _create_file(file, lines):
    with open(file, 'w') as f:
        f.write('\r\n'.join(lines))


def test_create_transfer_filename(func_init_config, data_dir):
    with _patches(func_init_config):
        result = _target()._create_transfer_filename('20140101_pattern.csv')
        assert result == (os.path.join(str(data_dir),
                                       'pattern_20140101_20140225144059.csv.gz'),
                          'pattern_20140101_20140225144059.csv.gz')

        result = _target()._create_transfer_filename('20140101_recommend_pattern.csv')
        assert result == (os.path.join(str(data_dir),
                                       'recommend_pattern_20140101_20140225144059.csv.gz'),
                          'recommend_pattern_20140101_20140225144059.csv.gz')

        with pytest.raises(Exception):
            _target()._create_transfer_filename('201401XX_pattern.csv')

        with pytest.raises(Exception):
            _target()._create_transfer_filename('_pattern.csv')

        with pytest.raises(Exception):
            _target()._create_transfer_filename('20140101_.csv')

        with pytest.raises(Exception):
            _target()._create_transfer_filename('20140101_pattern.tsv')

def test_execute(func_init_config, data_dir):

    # prepare
    target_file   = data_dir.join('20140101_pattern.csv')
    transfer_file = data_dir.join('pattern_20140101_20140225144059.csv.gz')

    _create_file(str(target_file),
                 [CSV_HEADER_RS, CSV_DATA1_RS, CSV_DATA2_RS, CSV_DATA3_RS])

    # execute
    with _patches(func_init_config) as (m_s3get, m_mapping, m_s3put, m_sftpput, m_rm):
        result = _target().execute()

    # assert
    assert result.is_ok()
    assert result.counter['user.responsys']          == 3
    assert result.counter['user.responsys.mapped']   == 2
    assert result.counter['user.responsys.unmapped'] == 1
    assert result.counter['user.rtoaster']           == 3
    assert result.counter['user.rtoaster.unique']    == 2

    _assert_py_path_file(transfer_file,
                         [CSV_HEADER_RT, CSV_DATA1_RT, CSV_DATA2_RT])

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3PATH, str(data_dir))

    assert m_mapping.call_count == 3
    assert m_mapping.call_args_list[0][0] == (u'pt_uid1',)
    assert m_mapping.call_args_list[1][0] == (u'pt_uid2',)
    assert m_mapping.call_args_list[2][0] == (u'pt_uid3',)

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(transfer_file), S3BUCKET, S3DIR_CNV)

    assert m_sftpput.call_count == 1
    assert m_sftpput.call_args_list[0][1]['host']         == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[0][1]['port']         == 115
    assert m_sftpput.call_args_list[0][1]['username']     == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['password']     == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['local_file']   == str(transfer_file)
    assert m_sftpput.call_args_list[0][1]['remote_dir']   == '/receive/rs/rt_request'
    assert m_sftpput.call_args_list[0][1]['rename']       == True
    assert m_sftpput.call_args_list[0][1]['tmp_filename'] == 'pattern_20140101_20140225144059.csv.gz.tmp'

    assert m_rm.call_count == 1
    assert m_rm.call_args_list[0][0] == ([str(target_file), str(transfer_file)],)

def test_execute_empty_ptuid(func_init_config, data_dir):

    # prepare
    target_file   = data_dir.join('20140101_pattern.csv')
    transfer_file = data_dir.join('pattern_20140101_20140225144059.csv.gz')

    _create_file(str(target_file), [CSV_HEADER_RS])

    # execute
    with _patches(func_init_config) as (m_s3get, m_mapping, m_s3put, m_sftpput, m_rm):
        result = _target().execute()

    # assert
    assert result.is_ok()
    assert result.counter['user.responsys']          == 0
    assert result.counter['user.responsys.mapped']   == 0
    assert result.counter['user.responsys.unmapped'] == 0
    assert result.counter['user.rtoaster']           == 0
    assert result.counter['user.rtoaster.unique']    == 0

    _assert_py_path_file(transfer_file, [CSV_HEADER_RT])

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3PATH, str(data_dir))

    assert m_mapping.call_count == 0

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(transfer_file), S3BUCKET, S3DIR_CNV)

    assert m_sftpput.call_count == 1
    assert m_sftpput.call_args_list[0][1]['host']         == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[0][1]['port']         == 115
    assert m_sftpput.call_args_list[0][1]['username']     == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['password']     == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['local_file']   == str(transfer_file)
    assert m_sftpput.call_args_list[0][1]['remote_dir']   == '/receive/rs/rt_request'
    assert m_sftpput.call_args_list[0][1]['rename']       == True
    assert m_sftpput.call_args_list[0][1]['tmp_filename'] == 'pattern_20140101_20140225144059.csv.gz.tmp'

    assert m_rm.call_count == 1
    assert m_rm.call_args_list[0][0] == ([str(target_file), str(transfer_file)],)

def test_execute_no_mapping(func_init_config, data_dir):

    # prepare
    target_file   = data_dir.join('20140101_pattern.csv')
    transfer_file = data_dir.join('pattern_20140101_20140225144059.csv.gz')

    _create_file(str(target_file), [CSV_HEADER_RS, CSV_DATA3_RS])

    # execute
    with _patches(func_init_config) as (m_s3get, m_mapping, m_s3put, m_sftpput, m_rm):
        result = _target().execute()

    # assert
    assert result.is_ok()
    assert result.counter['user.responsys']          == 1
    assert result.counter['user.responsys.mapped']   == 0
    assert result.counter['user.responsys.unmapped'] == 1
    assert result.counter['user.rtoaster']           == 0
    assert result.counter['user.rtoaster.unique']    == 0

    _assert_py_path_file(transfer_file, [CSV_HEADER_RT])

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3PATH, str(data_dir))

    assert m_mapping.call_count == 1
    assert m_mapping.call_args_list[0][0] == (u'pt_uid3',)

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(transfer_file), S3BUCKET, S3DIR_CNV)

    assert m_sftpput.call_count == 1
    assert m_sftpput.call_args_list[0][1]['host']         == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[0][1]['port']         == 115
    assert m_sftpput.call_args_list[0][1]['username']     == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['password']     == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['local_file']   == str(transfer_file)
    assert m_sftpput.call_args_list[0][1]['remote_dir']   == '/receive/rs/rt_request'
    assert m_sftpput.call_args_list[0][1]['rename']       == True
    assert m_sftpput.call_args_list[0][1]['tmp_filename'] == 'pattern_20140101_20140225144059.csv.gz.tmp'

    assert m_rm.call_count == 1
    assert m_rm.call_args_list[0][0] == ([str(target_file), str(transfer_file)],)

def test_execute_empty_file(func_init_config, data_dir):

    # prepare
    target_file   = data_dir.join('20140101_pattern.csv')
    transfer_file = data_dir.join('pattern_20140101_20140225144059.csv.gz')

    _create_file(str(target_file), [])

    # execute
    with _patches(func_init_config) as (m_s3get, m_mapping, m_s3put, m_sftpput, m_rm):
        result = _target().execute()

    # assert
    assert result.is_ng()
    assert result.counter['user.responsys']          == 0
    assert result.counter['user.responsys.mapped']   == 0
    assert result.counter['user.responsys.unmapped'] == 0
    assert result.counter['user.rtoaster']           == 0
    assert result.counter['user.rtoaster.unique']    == 0

    _assert_py_path_file(transfer_file, [])

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3PATH, str(data_dir))

    assert m_mapping.call_count == 0
    assert m_s3put.call_count   == 0
    assert m_sftpput.call_count == 0

    assert m_rm.call_count == 1
    assert m_rm.call_args_list[0][0] == ([str(target_file), str(transfer_file)],)

def test_execute_no_header(func_init_config, data_dir):

    # prepare
    target_file   = data_dir.join('20140101_pattern.csv')
    transfer_file = data_dir.join('pattern_20140101_20140225144059.csv.gz')

    _create_file(str(target_file), [CSV_DATA1_RS, CSV_DATA2_RS, CSV_DATA3_RS])

    # execute
    with _patches(func_init_config) as (m_s3get, m_mapping, m_s3put, m_sftpput, m_rm):
        result = _target().execute()

    # assert
    assert result.is_ng()
    assert result.counter['user.responsys']          == 0
    assert result.counter['user.responsys.mapped']   == 0
    assert result.counter['user.responsys.unmapped'] == 0
    assert result.counter['user.rtoaster']           == 0
    assert result.counter['user.rtoaster.unique']    == 0

    _assert_py_path_file(transfer_file, [])

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3PATH, str(data_dir))

    assert m_mapping.call_count == 0
    assert m_s3put.call_count   == 0
    assert m_sftpput.call_count == 0

    assert m_rm.call_count == 1
    assert m_rm.call_args_list[0][0] == ([str(target_file), str(transfer_file)],)

def test_execute_s3file_notfound(func_init_config, data_dir):

    # prepare
    # execute
    with _patches(func_init_config) as (m_s3get, m_mapping, m_s3put, m_sftpput, m_rm):
        m_s3get.return_value = None
        result = _target().execute()

    # assert
    assert result.is_ng()
    assert result.counter['user.responsys']          == 0
    assert result.counter['user.responsys.mapped']   == 0
    assert result.counter['user.responsys.unmapped'] == 0
    assert result.counter['user.rtoaster']           == 0
    assert result.counter['user.rtoaster.unique']    == 0

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3PATH, str(data_dir))

    assert m_mapping.call_count == 0
    assert m_s3put.call_count   == 0
    assert m_sftpput.call_count == 0

    assert m_rm.call_count == 1
    assert m_rm.call_args_list[0][0] == ([None, None],)

def test_execute_sftp_error(func_init_config, data_dir):

    # prepare
    target_file   = data_dir.join('20140101_pattern.csv')
    transfer_file = data_dir.join('pattern_20140101_20140225144059.csv.gz')

    _create_file(str(target_file),
                 [CSV_HEADER_RS, CSV_DATA1_RS, CSV_DATA2_RS, CSV_DATA3_RS])

    # execute
    with _patches(func_init_config) as (m_s3get, m_mapping, m_s3put, m_sftpput, m_rm):
        m_sftpput.side_effect = Exception()
        result = _target().execute()

    # assert
    assert result.is_ng()
    assert result.counter['user.responsys']          == 3
    assert result.counter['user.responsys.mapped']   == 2
    assert result.counter['user.responsys.unmapped'] == 1
    assert result.counter['user.rtoaster']           == 3
    assert result.counter['user.rtoaster.unique']    == 2

    _assert_py_path_file(transfer_file,
                         [CSV_HEADER_RT, CSV_DATA1_RT, CSV_DATA2_RT])

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3PATH, str(data_dir))

    assert m_mapping.call_count == 3
    assert m_mapping.call_args_list[0][0] == (u'pt_uid1',)
    assert m_mapping.call_args_list[1][0] == (u'pt_uid2',)
    assert m_mapping.call_args_list[2][0] == (u'pt_uid3',)

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(transfer_file), S3BUCKET, S3DIR_CNV)

    assert m_sftpput.call_count == 1
    assert m_sftpput.call_args_list[0][1]['host']         == 'sftp-s.c-ovn.jp'
    assert m_sftpput.call_args_list[0][1]['port']         == 115
    assert m_sftpput.call_args_list[0][1]['username']     == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['password']     == 'rtoaster9999'
    assert m_sftpput.call_args_list[0][1]['local_file']   == str(transfer_file)
    assert m_sftpput.call_args_list[0][1]['remote_dir']   == '/receive/rs/rt_request'
    assert m_sftpput.call_args_list[0][1]['rename']       == True
    assert m_sftpput.call_args_list[0][1]['tmp_filename'] == 'pattern_20140101_20140225144059.csv.gz.tmp'

    assert m_rm.call_count == 1
    assert m_rm.call_args_list[0][0] == ([str(target_file), str(transfer_file)],)

def test_execute_unexpected_error(func_init_config, data_dir):

    # prepare
    target_file   = data_dir.join('20140101_pattern.csv')
    transfer_file = data_dir.join('pattern_20140101_20140225144059.csv.gz')

    _create_file(str(target_file),
                 [CSV_HEADER_RS, CSV_DATA1_RS, CSV_DATA2_RS, CSV_DATA3_RS])

    # execute
    with _patches(func_init_config) as (m_s3get, m_mapping, m_sftpput, m_s3put, m_rm):
        m_mapping.side_effect = Exception()
        result = _target().execute()

    # assert
    assert result.is_ng()
    assert result.counter['user.responsys']          == 1
    assert result.counter['user.responsys.mapped']   == 0
    assert result.counter['user.responsys.unmapped'] == 0
    assert result.counter['user.rtoaster']           == 0
    assert result.counter['user.rtoaster.unique']    == 0

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3PATH, str(data_dir))

    assert m_mapping.call_count == 1
    assert m_mapping.call_args_list[0][0] == (u'pt_uid1',)

    assert m_s3put.call_count   == 0
    assert m_sftpput.call_count == 0

    assert m_rm.call_count == 1
    assert m_rm.call_args_list[0][0] == ([str(target_file), str(transfer_file)],)
