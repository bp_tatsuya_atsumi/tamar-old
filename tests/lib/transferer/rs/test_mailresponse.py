# -*- coding: utf-8 -*-

from lib.transferer.rs.mailresponse import MailResponseTransferer

import contextlib
import datetime
import gzip
import os
from   mock       import patch
import pytest


SID           = '9999'
PID           = 'rs'
ENV           = 'dev'
S3BUCKET      = 's3bucket'
S3PATH        = 's3path/mail_response_data/raw/pattern_20140101_20140225144059.csv.gz'
S3DIR_CNV     = 's3path/mail_response_data/converted/rs'

CSV_HEADER_RT = '"MEMBER_ID","col01","col02","col03","col04","col05","col06","col07","col08","col09","col10","col11","col12","col13","col14","col15","col16","col17","col18","col19","col20","col21","col22","col23","col24","col25","col26","col27","col28","col29","col30","col31","col32","col33","col34","col35","col36","col37","col38","col39","col40"'
CSV_DATA1_RT  = '"rt_uid1","data01_1","data02_1","data03_1","data04_1","data05_1","data06_1","data07_1","data08_1","data09_1","data10_1","data11_1","data12_1","data13_1","data14_1","data15_1","data16_1","data17_1","data18_1","data19_1","data20_1","data21_1","data22_1","data23_1","data24_1","data25_1","data26_1","data27_1","data28_1","data29_1","data30_1","data31_1","data32_1","data33_1","data34_1","data35_1","data36_1","data37_1","data38_1","data39_1","data40_1"'
CSV_DATA2_RT  = '"rt_uid2","data01_2","data02_2","data03_2","data04_2","data05_2","data06_2","data07_2","data08_2","data09_2","data10_2","data11_2","data12_2","data13_2","data14_2","data15_2","data16_2","data17_2","data18_2","data19_2","data20_2","data21_2","data22_2","data23_2","data24_2","data25_2","data26_2","data27_2","data28_2","data29_2","data30_2","data31_2","data32_2","data33_2","data34_2","data35_2","data36_2","data37_2","data38_2","data39_2","data40_2"'
CSV_DATA3_RT  = '"rt_uid3","data01_3","data02_3","data03_3","data04_3","data05_3","data06_3","data07_3","data08_3","data09_3","data10_3","data11_3","data12_3","data13_3","data14_3","data15_3","data16_3","data17_3","data18_3","data19_3","data20_3","data21_3","data22_3","data23_3","data24_3","data25_3","data26_3","data27_3","data28_3","data29_3","data30_3","data31_3","data32_3","data33_3","data34_3","data35_3","data36_3","data37_3","data38_3","data39_3","data40_3"'

CSV_HEADER_RS_1 = '"MEMBER_ID","col01","col02","col03","col04","col05","col06","col07","col08","col09","col10","col11","col12","col13","col14","col15","col16","col17","col18","col19","col20","col21","col22","col23","col24","col25","col26","col27","col28","col29","col30","col31","col32","col33","col34"'
CSV_DATA1_RS_1  = '"pt_uid1","data01_1","data02_1","data03_1","data04_1","data05_1","data06_1","data07_1","data08_1","data09_1","data10_1","data11_1","data12_1","data13_1","data14_1","data15_1","data16_1","data17_1","data18_1","data19_1","data20_1","data21_1","data22_1","data23_1","data24_1","data25_1","data26_1","data27_1","data28_1","data29_1","data30_1","data31_1","data32_1","data33_1","data34_1"'
CSV_DATA2_RS_1  = '"pt_uid2","data01_2","data02_2","data03_2","data04_2","data05_2","data06_2","data07_2","data08_2","data09_2","data10_2","data11_2","data12_2","data13_2","data14_2","data15_2","data16_2","data17_2","data18_2","data19_2","data20_2","data21_2","data22_2","data23_2","data24_2","data25_2","data26_2","data27_2","data28_2","data29_2","data30_2","data31_2","data32_2","data33_2","data34_2"'

CSV_HEADER_RS_2 = '"MEMBER_ID","col35","col36","col37","col38","col39","col40"'
CSV_DATA1_RS_2  = '"pt_uid1","data35_1","data36_1","data37_1","data38_1","data39_1","data40_1"'
CSV_DATA2_RS_2  = '"pt_uid2","data35_2","data36_2","data37_2","data38_2","data39_2","data40_2"'

@pytest.fixture
def data_dir(tmp_transfer_dir):
    return tmp_transfer_dir.mkdir(SID).mkdir(PID).mkdir('mailresponse')

@contextlib.contextmanager
def _patches(func_init_config):
    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('lib.s3util.S3Util.download_single_file'),
        patch('lib.mappingtable.MappingTable.select_rows_by_user_id'),
        patch('lib.s3util.S3Util.upload_single_file'),
        patch('lib.sftputil.SftpUtil.__enter__'),
        patch('lib.util.remove_files'),
    ) as (m_config, m_s3get, m_mapping, m_s3put, m_sftp, m_rm):
        m_s3get.return_value  = {'key': S3PATH}
        m_mapping.side_effect = _convert_uid

        yield (m_s3get, m_mapping, m_s3put, m_sftp, m_rm)

def _target():
    return MailResponseTransferer(SID, PID, ENV, S3BUCKET, S3PATH)

def _convert_uid(*args):
    tbl = {
        'rt_uid1': [
            [None, 'pt_uid1', None, None, None, datetime.datetime(2014, 2, 25, 14, 40, 59, 0)]
        ],
        'rt_uid2': [
            [None, 'pt_uid1', None, None, None, datetime.datetime(2014, 2, 20, 14, 40, 59, 0)],
            [None, 'pt_uid2', None, None, None, datetime.datetime(2014, 2, 25, 14, 40, 59, 0)]
        ],
    }
    return tbl.get(args[0])

def _assert_py_path_file(py_path_file, lines):
    assert py_path_file.check(file=1, exists=1)
    content = '\r\n'.join(lines)
    if content:
        content = content + '\r\n'
    assert open(str(py_path_file), 'r').read() == content

def _create_gzip(file, lines):
    with gzip.open(file, 'wb') as f:
        f.write('\r\n'.join(lines))


def test_create_transfer_filename(func_init_config, data_dir):
    with _patches(func_init_config):
        result = _target()._create_transfer_filename('pattern_20140101_20140225144059.csv.gz', 1)
        assert result == os.path.join(str(data_dir), '20140101_pattern_1.csv')

        result = _target()._create_transfer_filename('recommend_pattern_20140101_20140225144059.csv.gz', 1)
        assert result == os.path.join(str(data_dir), '20140101_recommend_pattern_1.csv')

        with pytest.raises(Exception):
            _target()._create_transfer_filename('_20140101_20140225144059.csv.gz')

        with pytest.raises(Exception):
            _target()._create_transfer_filename('pattern_201401XX_20140225144059.csv.gz')

        with pytest.raises(Exception):
            _target()._create_transfer_filename('pattern_20140101_201402251440XX.csv.gz')

        with pytest.raises(Exception):
            _target()._create_transfer_filename('pattern_20140101_20140225144059.csv')

def test_execute(func_init_config, data_dir):

    # prepare
    target_file    = data_dir.join('pattern_20140101_20140225144059.csv.gz')
    transfer_file1 = data_dir.join('20140101_pattern_1.csv')
    transfer_file2 = data_dir.join('20140101_pattern_2.csv')

    _create_gzip(str(target_file),
                 [CSV_HEADER_RT, CSV_DATA1_RT, CSV_DATA2_RT, CSV_DATA3_RT])

    # execute
    with _patches(func_init_config) as (m_s3get, m_mapping, m_s3put, m_sftp, m_rm):
        result = _target().execute()

    # assert
    assert result.is_ok()
    assert result.counter['user.rtoaster']           == 3
    assert result.counter['user.rtoaster.mapped']    == 2
    assert result.counter['user.rtoaster.unmapped']  == 1
    assert result.counter['user.responsys']          == 3
    assert result.counter['user.responsys.unique']   == 2

    _assert_py_path_file(transfer_file1,
                         [CSV_HEADER_RS_1, CSV_DATA1_RS_1, CSV_DATA2_RS_1])
    _assert_py_path_file(transfer_file2,
                         [CSV_HEADER_RS_2, CSV_DATA1_RS_2, CSV_DATA2_RS_2])

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3PATH, str(data_dir))

    assert m_mapping.call_count == 3
    assert m_mapping.call_args_list[0][0] == (u'rt_uid1',)
    assert m_mapping.call_args_list[1][0] == (u'rt_uid2',)
    assert m_mapping.call_args_list[2][0] == (u'rt_uid3',)

    assert m_s3put.call_count == 2
    assert m_s3put.call_args_list[0][0] == (str(transfer_file1), S3BUCKET, S3DIR_CNV)
    assert m_s3put.call_args_list[1][0] == (str(transfer_file2), S3BUCKET, S3DIR_CNV)

    assert m_sftp.call_count == 1
    assert m_sftp.return_value.put.call_count == 2
    assert m_sftp.return_value.put.call_args_list[0][1]['local_file'] == str(transfer_file1)
    assert m_sftp.return_value.put.call_args_list[0][1]['remote_dir'] == '/receive/rs/rs_response'
    assert m_sftp.return_value.put.call_args_list[0][1]['rename']     == True
    assert m_sftp.return_value.put.call_args_list[1][1]['local_file'] == str(transfer_file2)
    assert m_sftp.return_value.put.call_args_list[1][1]['remote_dir'] == '/receive/rs/rs_response'
    assert m_sftp.return_value.put.call_args_list[1][1]['rename']     == True

    assert m_rm.call_count == 2
    assert m_rm.call_args_list[0][0] == ([str(target_file)],)
    assert m_rm.call_args_list[1][0] == ([str(transfer_file1), str(transfer_file2)],)

def test_execute_empty_file(func_init_config, data_dir):

    # prepare
    target_file = data_dir.join('pattern_20140101_20140225144059.csv.gz')
    _create_gzip(str(target_file), [])

    # execute
    with _patches(func_init_config) as (m_s3get, m_mapping, m_s3put, m_sftp, m_rm):
        result = _target().execute()

    # assert
    assert result.is_ng()
    assert result.counter['user.rtoaster']           == 0
    assert result.counter['user.rtoaster.mapped']    == 0
    assert result.counter['user.rtoaster.unmapped']  == 0
    assert result.counter['user.responsys']          == 0
    assert result.counter['user.responsys.unique']   == 0

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3PATH, str(data_dir))

    assert m_mapping.call_count == 0
    assert m_s3put.call_count   == 0
    assert m_sftp.call_count    == 0

    assert m_rm.call_count == 2
    assert m_rm.call_args_list[0][0] == ([str(target_file)],)
    assert m_rm.call_args_list[1][0] == ([],)

def test_execute_no_header(func_init_config, data_dir):

    # prepare
    target_file = data_dir.join('pattern_20140101_20140225144059.csv.gz')
    _create_gzip(str(target_file), [CSV_DATA1_RT, CSV_DATA2_RT, CSV_DATA3_RT])

    # execute
    with _patches(func_init_config) as (m_s3get, m_mapping, m_s3put, m_sftp, m_rm):
        result = _target().execute()

    # assert
    assert result.is_ng()
    assert result.counter['user.rtoaster']           == 0
    assert result.counter['user.rtoaster.mapped']    == 0
    assert result.counter['user.rtoaster.unmapped']  == 0
    assert result.counter['user.responsys']          == 0
    assert result.counter['user.responsys.unique']   == 0

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3PATH, str(data_dir))

    assert m_mapping.call_count == 0
    assert m_s3put.call_count   == 0
    assert m_sftp.call_count    == 0

    assert m_rm.call_count == 2
    assert m_rm.call_args_list[0][0] == ([str(target_file)],)
    assert m_rm.call_args_list[1][0] == ([],)

def test_execute_s3file_notfound(func_init_config, data_dir):

    # execute
    with _patches(func_init_config) as (m_s3get, m_mapping, m_s3put, m_sftp, m_rm):
        m_s3get.return_value = None
        result = _target().execute()

    # assert
    assert result.is_ng()
    assert result.counter['user.rtoaster']           == 0
    assert result.counter['user.rtoaster.mapped']    == 0
    assert result.counter['user.rtoaster.unmapped']  == 0
    assert result.counter['user.responsys']          == 0
    assert result.counter['user.responsys.unique']   == 0

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3PATH, str(data_dir))

    assert m_mapping.call_count == 0
    assert m_s3put.call_count   == 0
    assert m_sftp.call_count    == 0

    assert m_rm.call_count == 2
    assert m_rm.call_args_list[0][0] == ([None],)
    assert m_rm.call_args_list[1][0] == ([],)

def test_execute_sftp_error(func_init_config, data_dir):

    # prepare
    target_file    = data_dir.join('pattern_20140101_20140225144059.csv.gz')
    transfer_file1 = data_dir.join('20140101_pattern_1.csv')
    transfer_file2 = data_dir.join('20140101_pattern_2.csv')

    _create_gzip(str(target_file),
                 [CSV_HEADER_RT, CSV_DATA1_RT, CSV_DATA2_RT, CSV_DATA3_RT])

    # execute
    with _patches(func_init_config) as (m_s3get, m_mapping, m_s3put, m_sftp, m_rm):
        m_sftp.return_value.put.side_effect = Exception()
        result = _target().execute()

    # assert
    assert result.is_ng()
    assert result.counter['user.rtoaster']           == 3
    assert result.counter['user.rtoaster.mapped']    == 2
    assert result.counter['user.rtoaster.unmapped']  == 1
    assert result.counter['user.responsys']          == 3
    assert result.counter['user.responsys.unique']   == 2

    _assert_py_path_file(transfer_file1,
                         [CSV_HEADER_RS_1, CSV_DATA1_RS_1, CSV_DATA2_RS_1])
    _assert_py_path_file(transfer_file2,
                         [CSV_HEADER_RS_2, CSV_DATA1_RS_2, CSV_DATA2_RS_2])

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3PATH, str(data_dir))

    assert m_mapping.call_count == 3
    assert m_mapping.call_args_list[0][0] == (u'rt_uid1',)
    assert m_mapping.call_args_list[1][0] == (u'rt_uid2',)
    assert m_mapping.call_args_list[2][0] == (u'rt_uid3',)

    assert m_s3put.call_count == 2
    assert m_s3put.call_args_list[0][0] == (str(transfer_file1), S3BUCKET, S3DIR_CNV)
    assert m_s3put.call_args_list[1][0] == (str(transfer_file2), S3BUCKET, S3DIR_CNV)

    assert m_sftp.call_count == 1
    assert m_sftp.return_value.put.call_count == 1
    assert m_sftp.return_value.put.call_args_list[0][1]['local_file'] == str(transfer_file1)
    assert m_sftp.return_value.put.call_args_list[0][1]['remote_dir'] == '/receive/rs/rs_response'
    assert m_sftp.return_value.put.call_args_list[0][1]['rename']     == True

    assert m_rm.call_count == 2
    assert m_rm.call_args_list[0][0] == ([str(target_file)],)
    assert m_rm.call_args_list[1][0] == ([str(transfer_file1), str(transfer_file2)],)

def test_execute_unexpected_error(func_init_config, data_dir):

    # prepare
    target_file    = data_dir.join('pattern_20140101_20140225144059.csv.gz')

    _create_gzip(str(target_file),
                 [CSV_HEADER_RT, CSV_DATA1_RT, CSV_DATA2_RT, CSV_DATA3_RT])

    # execute
    with _patches(func_init_config) as (m_s3get, m_mapping, m_s3put, m_sftp, m_rm):
        m_mapping.side_effect = Exception()
        result = _target().execute()

    # assert
    assert result.is_ng()
    assert result.counter['user.rtoaster']           == 1
    assert result.counter['user.rtoaster.mapped']    == 0
    assert result.counter['user.rtoaster.unmapped']  == 0
    assert result.counter['user.responsys']          == 0
    assert result.counter['user.responsys.unique']   == 0

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3PATH, str(data_dir))

    assert m_mapping.call_count == 1
    assert m_mapping.call_args_list[0][0] == (u'rt_uid1',)

    assert m_s3put.call_count == 0
    assert m_sftp.call_count  == 0

    assert m_rm.call_count == 2
    assert m_rm.call_args_list[0][0] == ([str(target_file)],)
    assert m_rm.call_args_list[1][0] == ([],)

def test_execute_recommend_error(func_init_config, data_dir):

    # prepare
    target_file   = data_dir.join('pattern_20140101_20140225144059.csv.gz.err')
    transfer_file = data_dir.join('20140101_pattern_1.csv.err')

    open(str(target_file), 'w').write('9')

    # execute
    with _patches(func_init_config) as (m_s3get, m_mapping, m_s3put, m_sftp, m_rm):
        m_s3get.return_value  = {'key': S3PATH + '.err'}
        result = _target().execute()

    # assert
    assert result.is_ng()
    assert result.counter['user.rtoaster']           == 0
    assert result.counter['user.rtoaster.mapped']    == 0
    assert result.counter['user.rtoaster.unmapped']  == 0
    assert result.counter['user.responsys']          == 0
    assert result.counter['user.responsys.unique']   == 0

    assert transfer_file.check(file=1, exists=1)
    assert open(str(transfer_file), 'r').read() == '9'

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3PATH, str(data_dir))

    assert m_mapping.call_count == 0

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(transfer_file), S3BUCKET, S3DIR_CNV)

    assert m_sftp.call_count == 1
    assert m_sftp.return_value.put.call_count == 1
    assert m_sftp.return_value.put.call_args_list[0][1]['local_file'] == str(transfer_file)
    assert m_sftp.return_value.put.call_args_list[0][1]['remote_dir'] == '/receive/rs/rs_response'
    assert m_sftp.return_value.put.call_args_list[0][1]['rename']     == True

    assert m_rm.call_count == 2
    assert m_rm.call_args_list[0][0] == ([str(target_file)],)
    assert m_rm.call_args_list[1][0] == ([str(transfer_file)],)
