# -*- coding: utf-8 -*-

from lib.extapi.parameters import *
from lib.extapi.errors import IllegalParameter

import pytest

def test_Parameter():
    param = Parameter('hoge')
    assert param.value == 'hoge'

def test_valid_PID():
    expected_pid = u'bp1'
    pid = PID(expected_pid)
    assert pid.value == expected_pid

def test_invalid_PID():
    for invalid_name in [None, '', u'', 'This is not unicode object', 12345]:
        with pytest.raises(IllegalParameter):
            pid = PID('')

def test_valid_SID():
    expected_sid = u'0001'
    sid = SID(expected_sid)
    assert sid.value == expected_sid

def test_invalid_SID():
    for invalid_name in [None, '', u'', 'This is not unicode object', 12345]:
        with pytest.raises(IllegalParameter):
            sid = SID(invalid_name)

def test_valid_SegID():
    for expected_segid in [u'abcde12345', u'a'*40]:
        segid = SegID(expected_segid)
        assert segid.value == expected_segid

def test_invalid_SegID():
    for invalid_name in [None, '', u'', u'あいうえお', u'a'*100]:
        with pytest.raises(IllegalParameter):
            segid = SegID(invalid_name)

def test_valid_Name():
    for expected_name in [u'segmentname', u'セグメント名', u'segメント名1', u'あ'*30]:
        name = Name(expected_name)
        assert name.value == expected_name

def test_invalid_Name():
    for invalid_name in ['', u'', 'This is not unicode object', 12345]:
        with pytest.raises(IllegalParameter):
            name = Name(invalid_name)

def test_valid_Description():
    for expected_description in [None, u'', u'あいうえお', u'aiueo', u'あ'*400]:
        description = Description(expected_description)
        assert description.value == expected_description

def test_invalid_Description():
    for invalid_description in ['', u'あ'*1100]:
        with pytest.raises(IllegalParameter):
            description = Description(invalid_description)

def test_valid_UIDs():
    for expected_uids in [None, [], ['aaa', 'bbb']]:
        uids = UIDs(expected_uids)
        assert uids.value == expected_uids

def test_invalid_UIDs():
    for invalid_uids in ['aaa', 12345, range(600)]:
        with pytest.raises(IllegalParameter):
            uids = UIDs(invalid_uids)

def test_valid_TTL():
    for expected_ttl in [123]:
        ttl = TTL(expected_ttl)
        assert ttl.value == expected_ttl

def test_invalid_TTL():
    for invalid_ttl in [None, 'abc', '123']:
        with pytest.raises(IllegalParameter):
            ttl = TTL(invalid_ttl)
