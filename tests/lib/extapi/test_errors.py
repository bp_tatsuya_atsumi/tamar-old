# -*- coding: utf-8 -*-

from lib.extapi.errors  import *

import pytest

def test_iLLegalParameter_Exception():
    with pytest.raises(HTTPException):
        raise IllegalParameter()
    with pytest.raises(IllegalParameter):
        raise IllegalParameter() 

def test_OverLimit_Exception():
    with pytest.raises(HTTPException):
        raise OverLimit()
    with pytest.raises(OverLimit):
        raise OverLimit()

def test_SegmentDuplicated_Exception():
    with pytest.raises(HTTPException):
        raise IllegalParameter()
    with pytest.raises(SegmentDuplicated):
        raise SegmentDuplicated()

def test_SegmentNotFound_Exception():
    with pytest.raises(HTTPException):
        raise IllegalParameter()
    with pytest.raises(SegmentNotFound):
        raise SegmentNotFound()

def test_PostFailed_Exception():
    with pytest.raises(HTTPException):
        raise IllegalParameter()
    with pytest.raises(PostFailed):
        raise PostFailed()

def test_UpdateFailed_Exception():
    with pytest.raises(HTTPException):
        raise IllegalParameter()
    with pytest.raises(UpdateFailed):
        raise UpdateFailed()

def test_DeleteFailed_Exception():
    with pytest.raises(HTTPException):
        raise IllegalParameter()
    with pytest.raises(DeleteFailed):
        raise DeleteFailed()

def test_StatusConflicted_Exception():
    with pytest.raises(HTTPException):
        raise IllegalParameter()
    with pytest.raises(StatusConflicted):
        raise StatusConflicted()

def test_InternalError():
    with pytest.raises(HTTPException):
        raise IllegalParameter()
    with pytest.raises(InternalError):
        raise InternalError()
