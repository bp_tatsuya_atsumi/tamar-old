# -*- coding: utf-8 -*-

from __future__ import absolute_import
from lib.sesutil import SESUtil

from mock import patch


def test_send_mail_prd():
    with patch('boto.ses.SESConnection.send_email') as mock_send_mail:
        SESUtil('prd').send_mail('subject', 'body')

    assert mock_send_mail.call_count == 1
    args, kwargs = mock_send_mail.call_args
    assert kwargs['source']             == 'coven-alert@brainpad.co.jp'
    assert kwargs['subject']            == 'subject'
    assert kwargs['body']               == 'body'
    assert kwargs['to_addresses']       == 'coven-alert@brainpad.co.jp'
    assert kwargs['format']             == 'text'
    assert kwargs['reply_addresses']    == 'coven-alert@brainpad.co.jp'
    assert kwargs['return_path']        == 'coven-alert@brainpad.co.jp'

def test_send_mail_stg():
    with patch('boto.ses.SESConnection.send_email') as mock_send_mail:
        SESUtil('stg').send_mail('subject', 'body')

    assert mock_send_mail.call_count == 0

def test_send_mail_dev():
    with patch('boto.ses.SESConnection.send_email') as mock_send_mail:
        SESUtil('dev').send_mail('subject', 'body')

    assert mock_send_mail.call_count == 0
