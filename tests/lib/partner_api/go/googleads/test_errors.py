# -*- coding: utf-8 -*-

__author__ = 'CrossOven開発チーム<coven-dev.brainpad.co.jp'

import pytest

from lib.partner_api.go.googleads import errors

def test_GoogleAdsError():
    errors.GoogleAdsError('error message')

def test_GoogleAdsValueError():
    errors.GoogleAdsValueError('error message2')
