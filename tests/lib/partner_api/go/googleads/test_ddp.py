# -*- coding: utf-8 -*-

__author__ = 'CrossOven開発チーム<coven-dev.c-ovn.jp>'

import io
import sys
import pytest
import urllib
import urllib2

import mock

import lib.partner_api.go.googleads.ddp
import lib.partner_api.go.googleads.common
import lib.partner_api.go.googleads.errors

PYTHON2 = sys.version_info[0] == 2
URL_REQUEST_PATH = ('urlib2' if PYTHON2 else 'urllib.request')

def _set_DdpHeader_handler():
    ddp_client = mock.Mock()
    header_handler = lib.partner_api.go.googleads.ddp._DdpHeaderHandler(
            ddp_client, 'v12345')

    return ddp_client, header_handler

# Tests for the lib.partner_api.go.googleads.ddp._DdpHeaderHandler class
def test_DdpHeaderHandler_SetHeaders():
    ddp_client, header_handler = _set_DdpHeader_handler()

    suds_client = mock.Mock()

    ccid            = 'client customer id'
    user_agent      = 'developer token'
    validate_only   = True
    partial_failure = False
    oauth_header    = {'oauth': 'header'}

    ddp_client.client_customer_id = ccid
    ddp_client.user_agent         = user_agent
    ddp_client.validate_only      = validate_only
    ddp_client.partial_failure    = partial_failure
    ddp_client.oauth_header.oauth2_client.CreateHttpHeader.return_value = (
            oauth_header)

    header_handler.SetHeaders(suds_client)

    assert suds_client.factory.create.call_count == 1
    assert suds_client.factory.create.call_args_list[0][0] == (
            '{https://ddp.googleapis.com/api/ddp/dmp/v12345}SoapHeader', )
    soap_header = suds_client.factory.create.return_value
    assert soap_header.clientCustomerId == ccid
    assert soap_header.userAgent == ''.join([user_agent, lib.partner_api.go.googleads.ddp._DdpHeaderHandler._LIB_SIG])
    assert soap_header.validateOnly == validate_only
    assert soap_header.partialFailure == partial_failure

    assert suds_client.set_options.call_count == 1
    assert suds_client.set_options.assert_anly_call(
            soapheaders=soap_header, headers=oauth_header)

def _set_DdpClient():
    user_agent         = 'users users users'
    oauth2_client      = 'unused'
    client_customer_id = 'client customer id'
    https_proxy        = 'my.proxy:443'
    ddp_client = lib.partner_api.go.googleads.ddp.DdpClient(
            oauth2_client, user_agent, client_customer_id, https_proxy=https_proxy)

    return user_agent, oauth2_client, https_proxy, ddp_client

# Tests for the lib.partner_api.go.googleads.ddp.DdpClient class
"""TODO
def test_LoadFromStorage():
    with mock.patch('lib.partner_api.go.googleads.common.LoadFromStorage') as mock_load:
        mock_load.return_value = {
            'oauth2_client': True,
            'user_agent': 'unit testing'
        }
        assert isinstance(lib.partner_api.go.googleads.ddp.DdpClient.LoadFromStorage(),
                          lib.partner_api.go.googleads.ddp.DdpClient)
"""

def test_GetService_success_and_use_of_customserver():
    user_agent, oauth2_client, https_proxy, ddp_client = _set_DdpClient()
    version   = lib.partner_api.go.googleads.ddp._SERVICE_MAP.keys()[0]
    service   = lib.partner_api.go.googleads.ddp._SERVICE_MAP[version].keys()[0]
    namespace = lib.partner_api.go.googleads.ddp._SERVICE_MAP[version][service]

    server = 'https://testing.test.com/'
    https_proxy = {'https': https_proxy}
    with mock.patch('suds.client.Client') as mock_client:
        suds_service = ddp_client.GetService(service, version, server)

        assert mock_client.call_count == 1
        assert mock_client.call_args_list[0][0] == (
                'https://testing.test.com/api/ddp/%s/%s/%s?wsdl' % (namespace, version, service), )
        assert isinstance(suds_service, lib.partner_api.go.googleads.common.SudsServiceProxy)

def test_GetService_success_and_use_of_defaultserver():
    user_agent, oauth2_client, https_proxy, ddp_client = _set_DdpClient()
    version   = lib.partner_api.go.googleads.ddp._SERVICE_MAP.keys()[0]
    service   = lib.partner_api.go.googleads.ddp._SERVICE_MAP[version].keys()[0]
    namespace = lib.partner_api.go.googleads.ddp._SERVICE_MAP[version][service]

    ddp_client.https_proxy = None
    with mock.patch('suds.client.Client') as mock_client:
        suds_service = ddp_client.GetService(service, version)
        
        assert mock_client.call_count == 1
        assert mock_client.call_args_list[0][0] == (
                'https://ddp.googleapis.com/api/ddp/%s/%s/%s?wsdl' % (namespace, version, service), )
        assert mock_client.return_value.set_option.call_count == 0
        assert isinstance(suds_service, lib.partner_api.go.googleads.common.SudsServiceProxy)

def test_GetService_badService():
    user_agent, oauth2_client, https_proxy, ddp_client = _set_DdpClient()
    version   = lib.partner_api.go.googleads.ddp._SERVICE_MAP.keys()[0]
    with pytest.raises(lib.partner_api.go.googleads.errors.GoogleAdsValueError):
        ddp_client.GetService('bad_service', version)

def test_GetService_badVersion():
    user_agent, oauth2_client, https_proxy, ddp_client = _set_DdpClient()
    with pytest.raises(lib.partner_api.go.googleads.errors.GoogleAdsValueError):
        ddp_client.GetService('DmpUserListService', '11111')
