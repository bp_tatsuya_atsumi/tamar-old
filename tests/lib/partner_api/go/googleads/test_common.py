# -*- coding: utf-8 -*-

__author__ = 'CrossOven開発チーム<coven-dev.brainpad.co.jp>'

import fake_filesystem
import fake_tempfile
import mock
import pytest
import suds
import warnings
import yaml

import lib.partner_api.go.googleads.common
import lib.partner_api.go.googleads.errors

_OAUTH_DICT = {'client_id': 'a', 'client_secret': 'b', 'refresh_token': 'c'}

def _patch():
    filesystem = fake_filesystem.FakeFilesystem()
    tempfile   = fake_tempfile.FakeTempfileModule(filesystem)
    fake_open  = fake_filesystem.FakeFileOpen(filesystem)

    return filesystem, tempfile, fake_open

def _CreateYamlFile(tempfile, fake_open, key, values, add_oauth=True):
    yaml_file = tempfile.NamedTemporaryFile(delete=False)
    if add_oauth: values.update(_OAUTH_DICT)
    with fake_open(yaml_file.name, 'w') as yaml_handler:
        yaml_handler.write(yaml.dump({key: values}))
    return yaml_file.name

# Tests for lib.partner_api.go.googleads.oauth2.Common class
def test_LoadFromStorage_missingFile():
    filesystem, tempfile, fake_open = _patch()
    with mock.patch('lib.partner_api.go.googleads.common.open', fake_open, create=True):
        with pytest.raises(lib.partner_api.go.googleads.errors.GoogleAdsValueError):
            lib.partner_api.go.googleads.common.LoadFromStorage('/path/to/yaml_filename', 'woo', [], [])

def test_LoadFromStorage_missingOAuthKey():
    filesystem, tempfile, fake_open = _patch()
    yaml_fname = _CreateYamlFile(tempfile, fake_open, 'woo', {}, False)
    with mock.patch('lib.partner_api.go.googleads.common.open', fake_open, create=True):
        with pytest.raises(lib.partner_api.go.googleads.errors.GoogleAdsValueError):
            lib.partner_api.go.googleads.common.LoadFromStorage(yaml_fname, 'woo', [], [])

def test_LoadFromStorage_passesWithNoRequiredKeys():
    fileystem, tempfile, fake_open = _patch()
    yaml_fname = _CreateYamlFile(tempfile, fake_open, 'woo', {})
    with mock.patch('lib.partner_api.go.googleads.oauth2.GoogleRefreshTokenClient') as mock_client:
        with mock.patch('lib.partner_api.go.googleads.common.open', fake_open, create=True):
            rval = lib.partner_api.go.googleads.common.LoadFromStorage(yaml_fname, 'woo', [], [])
            assert mock_client.call_count == 1
            assert mock_client.call_args_list[0][0] == ('a', 'b', 'c', None)
            assert rval == {'oauth2_client': mock_client.return_value}

def test_LoadFromStorage_missingRequiredKey():
    fileystem, tempfile, fake_open = _patch()
    with mock.patch('lib.partner_api.go.googleads.common.open', fake_open, create=True):

        yaml_fname = _CreateYamlFile(tempfile, fake_open, 'two', {})
        with pytest.raises(lib.partner_api.go.googleads.errors.GoogleAdsValueError):
            lib.partner_api.go.googleads.common.LoadFromStorage(yaml_fname, 'two', ['needed', 'keys'], [])

        yaml_fname = _CreateYamlFile(tempfile, fake_open, 'three', {'needed': 'd'})
        with pytest.raises(lib.partner_api.go.googleads.errors.GoogleAdsValueError):
            lib.partner_api.go.googleads.common.LoadFromStorage(yaml_fname, 'three', ['needed', 'keys'], [])

def test_LoadFromStorage():
    filesystem, tempfile, fake_open = _patch()
    yaml_fname = _CreateYamlFile(tempfile, fake_open,
                                 'one', {'needed': 'd', 'keys': 'e',
                                         'https_proxy': 'www.moo.cow'})
    with mock.patch('lib.partner_api.go.googleads.oauth2.GoogleRefreshTokenClient') as mock_client:
        with mock.patch('lib.partner_api.go.googleads.common.open', fake_open, create=True):
            rval = lib.partner_api.go.googleads.common.LoadFromStorage(
                    yaml_fname, 'one', ['needed', 'keys'], ['other'])
            assert mock_client.call_count == 1
            assert mock_client.call_args_list[0][0] == ('a', 'b', 'c', 'www.moo.cow')
            assert rval == {'oauth2_client': mock_client.return_value, 'needed': 'd', 'keys': 'e'}

def test_LoadFromStorage_relativePath():
    filesystem, tempfile, fake_open = _patch()
    fake_os = fake_filesystem.FakeOsModule(filesystem)
    yaml_contents = {'one': {'needed': 'd', 'keys': 'e'}}
    yaml_contents['one'].update(_OAUTH_DICT)
    filesystem.CreateFile('/home/test/yaml/googleads.yaml',
                          contents=yaml.dump(yaml_contents))
    fake_os.chdir('/home/test')
    
    with mock.patch('lib.partner_api.go.googleads.oauth2.GoogleRefreshTokenClient') as mock_client:
        with mock.patch('lib.partner_api.go.googleads.common.os', fake_os):
            with mock.patch('lib.partner_api.go.googleads.common.open', fake_open, create=True):
                rval = lib.partner_api.go.googleads.common.LoadFromStorage(
                        'yaml/googleads.yaml', 'one', ['needed', 'keys'], ['other'])
                assert mock_client.call_count == 1
                assert mock_client.call_args_list[0][0] == ('a', 'b', 'c', None)
                assert rval == {'oauth2_client': mock_client.return_value, 'needed': 'd', 'keys': 'e'}

def test_LoadFromStorage_warningWithUnrecognizedKey():
    filesystem, tempfile, fake_open = _patch()
    yaml_fname = _CreateYamlFile(tempfile, fake_open,
                   'kval', {'Im': 'here', 'whats': 'this?'})
    with mock.patch('lib.partner_api.go.googleads.oauth2.GoogleRefreshTokenClient') as mock_client:
        with warnings.catch_warnings(record=True) as captured_warnings:
            with mock.patch('lib.partner_api.go.googleads.common.open', fake_open, create=True):
                rval = lib.partner_api.go.googleads.common.LoadFromStorage(
                        yaml_fname, 'kval', ['Im'], ['other'])
                assert mock_client.call_count == 1
                assert mock_client.call_args_list[0][0] == ('a', 'b', 'c', None)
                assert rval == {'oauth2_client': mock_client.return_value, 'Im': 'here'}
                assert len(captured_warnings) == 1

def test_GenerateLiSib():
    my_name = 'shimoda'
    assert lib.partner_api.go.googleads.common.GenerateLibSig(my_name) == ' (%s, %s, %s)' % (my_name, 
                                                 lib.partner_api.go.googleads.common._COMMON_LIB_SIG,
                                                 lib.partner_api.go.googleads.common._PYTHON_VERSION)

def test_PackForSuds():
    factory = mock.Mock()

    assert lib.partner_api.go.googleads.common._PackForSuds('input', factory) == 'input'
    assert lib.partner_api.go.googleads.common._PackForSuds(set([1]), factory) == set([1])

    input_list = ['1', set([3]), {'moo': 'cow'}]
    assert lib.partner_api.go.googleads.common._PackForSuds(input_list, factory) == ['1', set([3]), {'moo': 'cow'}]
    assert input_list == ['1', set([3]), {'moo': 'cow'}]

    input_dict = {'1': 'moo', frozenset([2]): ['val']}
    assert lib.partner_api.go.googleads.common._PackForSuds(input_dict, factory) == {'1': 'moo', frozenset([2]): ['val']}
    assert input_dict == {'1': 'moo', frozenset([2]): ['val']}

    input_dict = {'xsi_type': 'EliteCampaign', 'name': 'Sales', 'id': 123456,
                  'metadata': {'a': 'b'}}
    factory.create.return_value = mock.MagicMock()
    factory.create.return_value.__iter__.return_value = iter(
            [('id', 0), ('name', 1), ('metadata', 2), ('Campaign.Type', 3),
             ('status', 4)])

    rval = lib.partner_api.go.googleads.common._PackForSuds(input_dict, factory)
    assert factory.create.call_count == 1
    assert factory.create.call_args_list[0][0] == ('EliteCampaign',)
    assert rval.name == 'Sales'
    assert rval.id == 123456
    assert rval.metadata == {'a':'b'}
    assert getattr(rval, 'Campaign.Type') == 'EliteCampaign'
    assert rval.status is None
    assert input_dict == {'xsi_type': 'EliteCampaign', 'name': 'Sales',
                          'id': 123456, 'metadata': {'a': 'b'}}

def test_PackSuds_nested():
    factory = mock.Mock()
    factory.create.side_effect = [mock.MagicMock(), mock.MagicMock()]
    input_list = [{'xsi_type': 'EliteCampaign', 'name': 'Sales', 'id': None,
                   'metadata': {'xsi_type': 'metadata', 'a': {'b': 'c'}}},
                   {'i do not have': 'a type'}]
    rval = lib.partner_api.go.googleads.common._PackForSuds(input_list, factory)
    factory.create.assert_any_call('EliteCampaign')
    factory.create.assert_any_call('metadata')
    assert type(rval) == list
    assert rval[0].name == 'Sales'
    assert isinstance(rval[0].id, suds.null)
    assert rval[0].metadata.a == {'b': 'c'}
    assert rval[1] == {'i do not have': 'a type'}
    assert input_list == [{'xsi_type': 'EliteCampaign', 'name': 'Sales', 'id': None,
                           'metadata': {'xsi_type': 'metadata', 'a': {'b': 'c'}}},
                          {'i do not have': 'a type'}]

def test_PackForSuds_secondNamespace():
    factory = mock.Mock()
    factory.create.side_effect = [suds.TypeNotFound(''), mock.MagicMock()]
    input_list = {'xsi_type': 'EliteCampaign', 'name': 'Sales'}
    rval = lib.partner_api.go.googleads.common._PackForSuds(input_list, factory)
    factory.create.assert_any_call('EliteCampaign')
    factory.create.assert_any_call('ns0:EliteCampaign')
    assert rval.name == 'Sales'

# Tests for lib.partner_api.go.googleads.oauth2.SudsServiceProxy class
def test_SudsServiceProxy():
    header_handler = mock.Mock()

    port = mock.Mock()
    port.methods = ('SoapMethod',)
    services = mock.Mock()
    services.ports = [port]
    client = mock.Mock()
    client.wsdl.services = [services]
    suds_service_wrapper = lib.partner_api.go.googleads.common.SudsServiceProxy(
                    client, header_handler)

    assert suds_service_wrapper.SoapMethod == suds_service_wrapper._method_proxies['SoapMethod'] 
    assert client.service.NotSoapMethod == suds_service_wrapper.NotSoapMethod

    with mock.patch('lib.partner_api.go.googleads.common._PackForSuds') as mock_pack_for_suds:
        mock_pack_for_suds.return_value = 'modified_test'
        suds_service_wrapper.SoapMethod('test')
        assert mock_pack_for_suds.call_count == 1
        assert mock_pack_for_suds.call_args_list[0][0] == ('test', client.factory)

    assert client.service.SoapMethod.call_count == 1
    assert client.service.SoapMethod.call_args_list[0][0] == ('modified_test',)
    assert header_handler.SetHeaders.call_count == 1
    assert header_handler.SetHeaders.call_args_list[0][0] == (client,)


# Test for other
def test_header_hander():
    with pytest.raises(NotImplementedError):
        lib.partner_api.go.googleads.common.HeaderHandler().SetHeaders(mock.Mock())
