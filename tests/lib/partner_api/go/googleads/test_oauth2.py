# -*- coding: utf-8 -*-

import io
import sys
import urllib2

import contextlib
import mock
from oauthlib import oauth2
import pytest

import lib.partner_api.go.googleads.oauth2

PYTHON2 = sys.version_info[0] == 2
URL_REQUEST_PATH = ('urllib2' if PYTHON2 else 'urllib.request')

CLIENT_ID     = 'client_id'
CLIENT_SECRET = 'itsasecret'
REFRESH_TOKEN = 'refreshing'
HTTPS_PROXY   = 'my.proxy.com:443'

@contextlib.contextmanager
def _patch():
    with mock.patch('oauthlib.oauth2.BackendApplicationClient') as mock_oauthlib_client:
        oauthlib_client = mock_oauthlib_client.return_value
        googleads_client = lib.partner_api.go.googleads.oauth2.GoogleRefreshTokenClient(
                            CLIENT_ID, CLIENT_SECRET, REFRESH_TOKEN, HTTPS_PROXY)

        yield(oauthlib_client, googleads_client)


# Test for lib.partner_api.go.googleads.oauth2.GoogleOAuth2Client class
def test_create_http_header():
    with pytest.raises(NotImplementedError):
        lib.partner_api.go.googleads.oauth2.GoogleOAuth2Client().CreateHttpHeader()


# Tests for lib.partner_api.go.googleads.oauth2.GoogleRefreshTokenClient class
def test_create_http_header_no_refresh():
    header = {'Authorization': 'b'}

    with _patch() as (oauthlib_client, googleads_client):
        oauthlib_client.add_token.return_value = ('unused', header, 'unused')
        assert header == googleads_client.CreateHttpHeader() 

def test_create_http_header_refresh():
    header    = {'Authorization': 'b'}
    post_body = 'post_body'
    content   = u'content'

    fake_request = io.StringIO() if PYTHON2 else io.BytesIO()
    fake_request.write(content if PYTHON2 else bytes(content, 'utf-8'))
    fake_request.seek(0)

    with _patch() as (oauthlib_client, googleads_client):
        oauthlib_client.add_token.side_effect = [oauth2.TokenExpiredError(),
                                                 ('unused', header, 'unused')]
        oauthlib_client.prepare_refresh_body.return_value = post_body

        with mock.patch(URL_REQUEST_PATH + '.urlopen') as mock_urlopen:
            with mock.patch(URL_REQUEST_PATH + '.Request') as mock_request:
                mock_urlopen.return_value = fake_request
                returned_header = googleads_client.CreateHttpHeader()

                assert mock_request.call_count == 1
                assert mock_request.call_args_list[0][0][1] == post_body
                assert mock_urlopen.call_count == 1

        assert header == returned_header
        assert oauthlib_client.parse_request_body_response.call_count == 1
        assert len(oauthlib_client.add_token.call_args_list) == 2
        assert type(returned_header.keys()[0]) == str

def test_create_http_header_refresh_fails():
    post_body = 'post_body'
    error     = urllib2.HTTPError('', 400, 'Bad Request', {}, None)

    with _patch() as (oauthlib_client, googleads_client):
        oauthlib_client.add_token.side_effect = oauth2.TokenExpiredError()
        oauthlib_client.prepare_refresh_body.return_value = post_body

        with mock.patch(URL_REQUEST_PATH + '.urlopen') as mock_urlopen:
            with mock.patch(URL_REQUEST_PATH + '.Request') as mock_request:
                mock_urlopen.side_effect = error
                assert googleads_client.CreateHttpHeader() == {}

                assert mock_request.call_count == 1
                assert mock_request.call_args_list[0][0][1] == post_body
                assert mock_urlopen.call_count == 1
        assert oauthlib_client.parse_request_body_response.called == False
        assert oauthlib_client.add_token.call_count == 1
