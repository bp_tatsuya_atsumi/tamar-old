# -*- coding: utf-8 -*-

import contextlib
from   lib.partner_api.so.api_token import ApiToken
from   mock                         import Mock
from   mock                         import patch
import pytest
from   requests.exceptions          import HTTPError

ENV = 'dev'

def _init(env):
    return ApiToken(env)

def _create_mock_response(status_code):
    response             = Mock()
    response.status_code = status_code
    response.text        = '{\"api_token\":\"the_token\"}'
    return response

@contextlib.contextmanager
def _patches_with_default_action(func_init_config):
    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('requests.post'),
    ) as (m_config, m_post):
        m_post.return_value = _create_mock_response(200)

        yield (m_post)

def test_token_and_headers(func_init_config):
    with _patches_with_default_action(func_init_config) as mocks:
        m_post  = mocks
        manager = _init(ENV)

    assert manager.TOKEN       == "the_token"
    assert manager.API_HEADERS == {'Authorization': 'login=bpapitest@scaleout.jp;api_token=the_token',
                                   'content-type' : 'application/json'}
    assert m_post.call_count == 1
    assert m_post.call_args_list[0][0] == ('http://api-test.scaleout.jp/api/token.json', )

def test_token_and_headers_api_error(func_init_config):
    with _patches_with_default_action(func_init_config) as mocks:
        m_post = mocks
        m_post.return_value = _create_mock_response(401)
        with pytest.raises(HTTPError):
            manager = _init(ENV)

    assert m_post.call_count == 1
    assert m_post.call_args_list[0][0] == ('http://api-test.scaleout.jp/api/token.json', )
