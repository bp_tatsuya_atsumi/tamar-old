# -*- coding: utf-8 -*-

import contextlib
from   lib.partner_api.so.api_connector import ApiConnector
from   mock                             import Mock
from   mock                             import patch
import pytest
from   requests.exceptions              import HTTPError

ENV       = 'dev'

URL_ORDERS            = 'http://api-test.scaleout.jp/api/advertisers/1.json?includes=%5B%22orders%22%5D'
URL_CONVERSION_PIXELS = 'http://api-test.scaleout.jp/api/advertisers/1.json?includes=%5B%22conversion_pixels%22%5D'
URL_SCHEDULES1        = 'http://api-test.scaleout.jp/api/advertisers/1.json?includes=%5B%22schedules%22%5D'

RESPONSES = {URL_ORDERS \
                 : '{\"orders\":            [{\"id\":\"1\"},{\"id\":\"2\"}]}',
             URL_CONVERSION_PIXELS \
                 : '{\"conversion_pixels\": [{\"id\":\"1\"},{\"id\":\"2\"}]}',
             URL_SCHEDULES1 \
                 : '{\"schedules\":         [{\"id\":\"1\",\"order_id\":\"2\"},{\"id\":\"2\",\"order_id\":\"1\"},{\"id\":\"3\", \"order_id\":\"3\"}]}',
             }

def _init(env):
    return ApiConnector(env)

def _create_mock_sleep(*args, **kwargs):
    pass

def _create_mock_post_response(status_code):
    response             = Mock()
    response.status_code = status_code
    response.text        = '{\"api_token\":\"the_token\"}'
    return response

def _create_mock_get_response(*args, **kwargs):
    url                  = args[0]
    response             = Mock()
    response.status_code = 200
    response.text        = RESPONSES[url]
    return response

def _create_mock_get_response_401(*args, **kwargs):
    response             = Mock()
    response.status_code = 401
    response.text        = None
    return response    

@contextlib.contextmanager
def _patches_with_default_action(func_init_config):
    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('time.sleep'),
        patch('requests.get'),
        patch('requests.post'),
    ) as (m_config, m_sleep, m_get, m_post):
        m_sleep.side_effect = _create_mock_sleep
        m_get.side_effect   = _create_mock_get_response
        m_post.return_value = _create_mock_post_response(200)

        yield (m_get, m_post)

def test_get_order_ids_from_advertiser_ids(func_init_config):
    with _patches_with_default_action(func_init_config) as mocks:
        m_get   = mocks[0]
        manager = _init(ENV)
        result  = manager.get_order_ids_from_advertiser_ids(['1'])

    assert result == ['1', '2']

    assert m_get.call_count == 1
    assert m_get.call_args_list[0][0] == (URL_ORDERS,)

def test_get_cv_pixel_ids_from_advertiser_ids(func_init_config):
    with _patches_with_default_action(func_init_config) as mocks:
        m_get   = mocks[0]
        manager = _init(ENV)
        result  = manager.get_cv_pixel_ids_from_advertiser_ids(['1'])

    assert result == ['1', '2']

    assert m_get.call_count == 1
    assert m_get.call_args_list[0][0] == (URL_CONVERSION_PIXELS,)

def test_get_schedule_ids_from_advertiser_ids(func_init_config):
    with _patches_with_default_action(func_init_config) as mocks:
        m_get   = mocks[0]
        manager = _init(ENV)
        result  = manager.get_schedule_ids_from_advertiser_ids(['1'],['1','2'])

    assert result == ['1', '2']

    assert m_get.call_count == 1
    assert m_get.call_args_list[0][0] == (URL_SCHEDULES1,)

def test_get_order_ids_from_advertiser_ids_error(func_init_config):
    with _patches_with_default_action(func_init_config) as mocks:
        m_get             = mocks[0]
        m_get.side_effect = _create_mock_get_response_401
        manager           = _init(ENV)
        with pytest.raises(HTTPError):
            result = manager.get_order_ids_from_advertiser_ids(['1'])

    assert m_get.call_count == 1
    assert m_get.call_args_list[0][0] == (URL_ORDERS,)

def test_get_cv_pixel_ids_from_advertiser_ids_error(func_init_config):
    with _patches_with_default_action(func_init_config) as mocks:
        m_get             = mocks[0]
        m_get.side_effect = _create_mock_get_response_401
        manager           = _init(ENV)
        with pytest.raises(HTTPError):
            result = manager.get_cv_pixel_ids_from_advertiser_ids(['1'])

    assert m_get.call_count == 1
    assert m_get.call_args_list[0][0] == (URL_CONVERSION_PIXELS,)

def test_get_schedule_ids_from_advertiser_ids_error(func_init_config):
    with _patches_with_default_action(func_init_config) as mocks:
        m_get             = mocks[0]
        m_get.side_effect = _create_mock_get_response_401
        manager           = _init(ENV)
        with pytest.raises(HTTPError):
            result = manager.get_schedule_ids_from_advertiser_ids(['1'], ['1','2'])

    assert m_get.call_count == 1
    assert m_get.call_args_list[0][0] == (URL_SCHEDULES1,)
