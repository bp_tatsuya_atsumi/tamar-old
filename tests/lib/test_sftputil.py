# -*- coding: utf-8 -*-

from __future__ import absolute_import
import lib.sftputil as sftputil

import os
import pytest
from conf.config import BatchConfig


HOST          = 'sftp-s.c-ovn.jp'
PORT          = 115
USERNAME9997  = 'rtoaster9997'      # for rsa private key authentification
USERNAME9998  = 'rtoaster9998'      # for dsa private key authentification
USERNAME9999  = 'rtoaster9999'      # for password authentification
PASSWORD      = 'rtoaster9999'
PKEY_FILE_RSA = 'tests/lib/ssh_key/id_rsa'
PKEY_FILE_DSA = 'tests/lib/ssh_key/id_dsa'
REMOTE_DIR    = 'receive'
REMOTE_FILE   = 'receive/test.txt'

config = BatchConfig('dev')

@pytest.fixture
def local_receive_dir(tmpdir):
    return tmpdir.mkdir('receive')

@pytest.fixture
def local_receive_file(local_receive_dir):
    return local_receive_dir.join('test.txt')

@pytest.fixture
def local_send_dir(tmpdir):
    return tmpdir.mkdir('send')

@pytest.fixture
def local_send_file(local_send_dir):
    local_send_file = local_send_dir.join('test.txt')
    local_send_file.write('test')
    return local_send_file

@pytest.fixture()
def create_remote_file(local_send_file):
    try:
        with _init_sftp() as sftp:
            sftp.put(str(local_send_file), REMOTE_DIR)
    except:
        pass

@pytest.fixture()
def delete_remote_file():
    try:
        with _init_sftp() as sftp:
            sftp.delete(REMOTE_FILE)
    except:
        pass

def _init_sftp(host=HOST, port=PORT, username=USERNAME9999, password=PASSWORD):
    return sftputil.SftpUtil(host=host, port=port, username=username, password=password)

# connect, close
@pytest.mark.sftptest
def test_connect_password():
    with _init_sftp() as sftp:
        assert sftp.transport.is_active()
    assert not sftp.transport.is_active()

@pytest.mark.sftptest
def test_connect_rsa_key():
    with sftputil.SftpUtil(host=HOST, port=PORT, username=USERNAME9997, password=None, pkey_file=PKEY_FILE_RSA) as sftp:
        assert sftp.transport.is_active()
    assert not sftp.transport.is_active()

@pytest.mark.sftptest
def test_connect_dsa_key():
    with sftputil.SftpUtil(host=HOST, port=PORT, username=USERNAME9998, password=None, pkey_file=PKEY_FILE_DSA) as sftp:
        assert sftp.transport.is_active()
    assert not sftp.transport.is_active()

@pytest.mark.sftptest
def test_connect_invalid_host():
    with pytest.raises(Exception):
        _init_sftp(host='notexists').connect()

@pytest.mark.sftptest
def test_connect_invalid_port():
    with pytest.raises(Exception):
        _init_sftp(port=153443).connect()

@pytest.mark.sftptest
def test_connect_invalid_username():
    with pytest.raises(Exception):
        _init_sftp(username='rtuser').connect()

@pytest.mark.sftptest
def test_connect_invalid_password():
    with pytest.raises(Exception):
        _init_sftp(password='wrong').connect()

@pytest.mark.sftptest
def test_connect_invalid_pkey_file(tmpdir):
    pkey_file = tmpdir.join('invalid_pkey')
    pkey_file.write('test')
    with pytest.raises(Exception):
        sftputil.SftpUtil(host=HOST, port=PORT, username=USERNAME9997, password=None, pkey_file=str(pkey_file)).connect()

# listdir
@pytest.mark.sftptest
@pytest.mark.usefixtures('delete_remote_file')
def test_listdir(local_send_file):
    with _init_sftp() as sftp:

        # remote dir exists
        assert not 'test.txt' in sftp.listdir(REMOTE_DIR)
        sftp.put(str(local_send_file), REMOTE_DIR)
        assert 'test.txt' in sftp.listdir(REMOTE_DIR)
        assert 'test.txt' in sftp.listdir(REMOTE_DIR, only_file=True)
        assert not 'test.txt' in sftp.listdir(REMOTE_DIR, only_dir=True)
        with pytest.raises(Exception):
            assert 'test.txt' in sftp.listdir(REMOTE_DIR, only_file=True, only_dir=True)

        # remote dir not exists
        with pytest.raises(Exception):
            sftp.listdir('notexists')

# exists
@pytest.mark.sftptest
@pytest.mark.usefixtures('create_remote_file')
def test_exists_found():
    with _init_sftp() as sftp:

        # remote file exists
        assert sftp.exists(REMOTE_FILE)

        # remote file not exists
        assert not sftp.exists('notexists')

# get
@pytest.mark.sftptest
@pytest.mark.usefixtures('create_remote_file')
def test_get(local_receive_dir, local_receive_file, local_send_file):
    with _init_sftp() as sftp:

        # remote file exists
        assert local_receive_file.check(exists=0)
        sftp.get(REMOTE_FILE, str(local_receive_dir))
        assert local_receive_file.check(exists=1)

        # local file already exists
        with pytest.raises(Exception):
            sftp.get(REMOTE_FILE, str(local_receive_dir))

        # remote file not exists
        with pytest.raises(Exception):
            sftp.get('notexists', str(local_receive_dir))

# put
@pytest.mark.sftptest
@pytest.mark.usefixtures('delete_remote_file')
def test_put(local_receive_file, local_send_file):
    with _init_sftp() as sftp:

        # remote file not exists
        assert not 'test.txt' in sftp.listdir(REMOTE_DIR)
        sftp.put(str(local_send_file), REMOTE_DIR)
        assert 'test.txt' in sftp.listdir(REMOTE_DIR)

        # remote file not exists
        sftp.delete(REMOTE_FILE)
        assert not 'test.txt' in sftp.listdir(REMOTE_DIR)
        sftp.put(str(local_send_file), REMOTE_DIR, tmp_filename='test.txt.tmp')
        assert 'test.txt' in sftp.listdir(REMOTE_DIR)

        # remote file already exists
        with pytest.raises(Exception):
            sftp.put(str(local_send_file), REMOTE_DIR)

        # remote tmpfile already exists
        with pytest.raises(Exception):
            sftp.put(str(local_send_file), REMOTE_DIR, tmp_filename='test.txt')

        # local file not exists
        with pytest.raises(Exception):
            sftp.put('notexists', REMOTE_DIR)

# touch
@pytest.mark.sftptest
@pytest.mark.usefixtures('delete_remote_file')
def test_touch(local_receive_dir, local_receive_file):
    with _init_sftp() as sftp:

        # remote file exists
        sftp.touch(REMOTE_FILE)
        assert sftp.exists(REMOTE_FILE)
        sftp.get(REMOTE_FILE, str(local_receive_dir))
        local_receive_file.read() == ''

        # remote file exists
        sftp.delete(REMOTE_FILE)
        local_receive_file.remove()

        sftp.touch(REMOTE_FILE, 'tamar')
        assert sftp.exists(REMOTE_FILE)
        sftp.get(REMOTE_FILE, str(local_receive_dir))
        local_receive_file.read() == 'tamar'

        # remote file already exists
        with pytest.raises(Exception):
            sftp.touch(REMOTE_FILE)

# delete
@pytest.mark.sftptest
@pytest.mark.usefixtures('create_remote_file')
def test_delete():
    with _init_sftp() as sftp:

        # remote file exists
        assert 'test.txt' in sftp.listdir(REMOTE_DIR)
        sftp.delete(REMOTE_FILE)
        assert not 'test.txt' in sftp.listdir(REMOTE_DIR)

        # remote file not exists
        with pytest.raises(Exception):
            sftp.delete(REMOTE_FILE)
