# -*- coding: utf-8 -*-

import pytest
import MySQLdb
from lib.mapping import csvload
from lib.mappingtable import MappingTable

ENV = 'dev'
CSV_LINES = '''
rt_user1,partner_user1,1,rt_user1
rt_user1,partner_user2,1,rt_user1
rt_user2,partner_user3,1,rt_user2
'''.lstrip()

@pytest.fixture
def csv_dir(tmpdir):
    return tmpdir.mkdir('csv')

@pytest.fixture
def tbl():
    DB_USER  = 'tamaruser'
    DB_PASS  = '#!TawaR?u5er'
    DB_HOST  = 'localhost'
    DB_NAME  = 'DB_TAMAR_9999'
    TBL_NAME = 't_user_mapping_9999_bp1'
    return MappingTable(DB_USER, DB_PASS, DB_HOST, DB_NAME, TBL_NAME)

@pytest.fixture(autouse=True)
def truncate_table(tbl):
    tbl.truncate_table()

def _assert_row(row, user_id, partner_user_id, tamar_user_type, tamar_user_id, deleted):
    assert row[0] == user_id
    assert row[1] == partner_user_id
    assert row[2] == tamar_user_type
    assert row[3] == tamar_user_id
    assert row[6] == deleted

def _prepare_csv_file(csv_dir, csv_filename, content):
    csv_file = csv_dir.join(csv_filename)
    csv_file.write(content)

def test_load_insert_use_temporary_table(csv_dir, tbl):

    # prepare
    _prepare_csv_file(csv_dir, 'access_log.csv', CSV_LINES)

    # execute
    csvload.load('9999', 'bp1', ENV, str(csv_dir), use_temporary_table=True)

    # assert
    rows = tbl.select_all_rows()
    assert len(rows) == 3
    _assert_row(rows[0], 'rt_user1', 'partner_user1', 1, 'rt_user1', False)
    _assert_row(rows[1], 'rt_user1', 'partner_user2', 1, 'rt_user1', False)
    _assert_row(rows[2], 'rt_user2', 'partner_user3', 1, 'rt_user2', False)

def test_load_insert_no_use_temporary_table(csv_dir, tbl):

    # prepare
    _prepare_csv_file(csv_dir, 'access_log.csv', CSV_LINES)

    # execute
    csvload.load('9999', 'bp1', ENV, str(csv_dir), use_temporary_table=False)

    # assert
    rows = tbl.select_all_rows()
    assert len(rows) == 3
    _assert_row(rows[0], 'rt_user1', 'partner_user1', 1, 'rt_user1', False)
    _assert_row(rows[1], 'rt_user1', 'partner_user2', 1, 'rt_user1', False)
    _assert_row(rows[2], 'rt_user2', 'partner_user3', 1, 'rt_user2', False)

def test_load_update(csv_dir, tbl):

    # prepare table
    tbl.insert_row('rt_user1', 'partner_user1', 2, 'tamar_user1', None, None, False)
    tbl.insert_row('rt_user1', 'partner_user2', 2, 'tamar_user2', None, None, False)

    # prepare csv
    _prepare_csv_file(csv_dir, 'access_log.csv', CSV_LINES)

    # execute
    csvload.load('9999', 'bp1', ENV, str(csv_dir))

    # assert
    rows = tbl.select_all_rows()
    assert len(rows) == 3
    _assert_row(rows[0], 'rt_user1', 'partner_user1', 1, 'rt_user1', False)
    _assert_row(rows[1], 'rt_user1', 'partner_user2', 1, 'rt_user1', False)
    _assert_row(rows[2], 'rt_user2', 'partner_user3', 1, 'rt_user2', False)

def test_load_empty_file(csv_dir, tbl):

    # prepare
    _prepare_csv_file(csv_dir, 'access_log.csv', '')

    # execute
    csvload.load('9999', 'bp1', ENV, str(csv_dir))

    # assert
    rows = tbl.select_all_rows()
    assert len(rows) == 0

def test_load_no_file(csv_dir, tbl):

    # prepare

    # execute
    csvload.load('9999', 'bp1', ENV, str(csv_dir))

def test_load_no_table(csv_dir, tbl):

    # prepare
    _prepare_csv_file(csv_dir, 'access_log.csv', CSV_LINES)

    # execute
    with pytest.raises(MySQLdb.Error):
        csvload.load('9999', 'xxx', ENV, str(csv_dir))

def test_load_no_database_config(csv_dir, tbl):

    # execute
    with pytest.raises(ValueError):
        csvload.load('xxx', 'bp1', ENV, csv_dir)

def test_load_csv_directory_not_exists(csv_dir, tbl):

    # execute
    with pytest.raises(ValueError):
        csvload.load('xxx', 'bp1', ENV, '/data/csv')
