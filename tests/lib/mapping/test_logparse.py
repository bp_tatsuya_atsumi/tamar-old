# -*- coding: utf-8 -*-

from lib.mapping import logparse

import gzip
from   mock import patch
import pytest


ENV          = 'dev'

LOG_LINE1    = '2013-09-26T20:00:59+09:00	9999.bp1	{"remote":"127.0.0.1","host":"-","user":"-","method":"GET","path":"/p-sync?sid=9999&uid=encrypted_uid&pid=bp1&pt_uid=partner_uid1","code":"200","size":"43","referer":"http://example.com","agent":"Test User Agent","forwarder":"127.0.0.1"}'
LOG_LINE2    = '2013-09-26T20:00:59+09:00	9999.bp1	{"remote":"127.0.0.1","host":"-","user":"-","method":"GET","path":"/p-sync?sid=9999&uid=encrypted_uid&pid=bp1&pt_uid=partner_uid2","code":"200","size":"43","referer":"http://example.com","agent":"Test User Agent","forwarder":"127.0.0.1"}'
LOG_LINE3    = '2013-09-26T20:00:59+09:00	9999.bp1	{"remote":"127.0.0.1","host":"-","user":"-","method":"GET","path":"/p-sync?sid=9999&uid=encrypted_uid&pid=bp1&pt_uid=partner_uid3","code":"200","size":"43","referer":"http://example.com","agent":"Test User Agent","forwarder":"127.0.0.1"}'
LOG_LINE4    = '2013-09-26T20:00:59+09:00	9999.bp1	{"remote":"127.0.0.1","host":"-","user":"-","method":"GET","path":"/p-sync?sid=9999&uid=encrypted_uid&pid=bp1&pt_uid=partner_uid4","code":"200","size":"43","referer":"http://example.com","agent":"Test User Agent","forwarder":"127.0.0.1"}'
LOG_LINE5    = '2013-09-26T20:00:59+09:00	9999.bp1	{"remote":"127.0.0.1","host":"-","user":"-","method":"GET","path":"/p-sync?sid=9999&uid=encrypted_uid&pid=bp1&pt_uid=partner_uid5","code":"200","size":"43","referer":"http://example.com","agent":"Test User Agent","forwarder":"127.0.0.1"}'
LOG_LINE6    = '2013-09-26T20:00:59+09:00	9999.bp1	{"remote":"127.0.0.1","host":"-","user":"-","method":"GET","path":"/p-sync?sid=9999&uid=encrypted_uid&pid=bp1&pt_uid=partner_uid6","code":"200","size":"43","referer":"http://example.com","agent":"Test User Agent","forwarder":"127.0.0.1"}'

CSV_LINE1    = 'decrypted_uid,partner_uid1,1,decrypted_uid'
CSV_LINE2    = 'decrypted_uid,partner_uid2,1,decrypted_uid'
CSV_LINE3    = 'decrypted_uid,partner_uid3,1,decrypted_uid'
CSV_LINE4    = 'decrypted_uid,partner_uid4,1,decrypted_uid'
CSV_LINE5    = 'decrypted_uid,partner_uid5,1,decrypted_uid'
CSV_LINE6    = 'decrypted_uid,partner_uid6,1,decrypted_uid'

LOG_LINE_CO1 = '2013-09-26T20:00:59+09:00	9999.co	{"remote":"127.0.0.1","host":"-","user":"-","method":"GET","path":"/c-sync?sid=9999&uid=encrypted_uid","code":"200","size":"43","referer":"http://example.com","agent":"Test User Agent","forwarder":"127.0.0.1","setuid":"-","getuid":"co.uid=tamar_id1"}'
LOG_LINE_CO2 = '2013-09-26T20:00:59+09:00	9999.co	{"remote":"127.0.0.1","host":"-","user":"-","method":"GET","path":"/c-sync?sid=9999&uid=encrypted_uid","code":"200","size":"43","referer":"http://example.com","agent":"Test User Agent","forwarder":"127.0.0.1","setuid":"co.uid=tamar_id2","getuid":"-"}'
LOG_LINE_CO3 = '2013-09-26T20:00:59+09:00	9999.co	{"remote":"127.0.0.1","host":"-","user":"-","method":"GET","path":"/c-sync?sid=9999&uid=encrypted_uid","code":"200","size":"43","referer":"http://example.com","agent":"Test User Agent","forwarder":"127.0.0.1","setuid":"-","getuid":"-"}'

CSV_LINE_CO1 = 'decrypted_uid,tamar_id1,2,tamar_id1'

LOG_LINE_RS1 = '2013-09-26T20:00:59+09:00	9999.rs	{"remote":"127.0.0.1","host":"-","user":"-","method":"GET","path":"/p-sync?sid=9999&uid=uid1&pid=rs&pt_uid=partner_uid1","code":"200","size":"43","referer":"http://example.com","agent":"Test User Agent","forwarder":"127.0.0.1"}'
LOG_LINE_RS2 = '2013-09-26T20:00:59+09:00	9999.rs	{"remote":"127.0.0.1","host":"-","user":"-","method":"GET","path":"/p-sync?sid=9999&uid=uid2&pid=rs&pt_uid=partner_uid2","code":"200","size":"43","referer":"http://example.com","agent":"Test User Agent","forwarder":"127.0.0.1"}'
LOG_LINE_RS3 = '2013-09-26T20:00:59+09:00	9999.rs	{"remote":"127.0.0.1","host":"-","user":"-","method":"GET","path":"/p-sync?sid=9999&uid=uid3&pid=rs&pt_uid=partner_uid3","code":"200","size":"43","referer":"http://example.com","agent":"Test User Agent","forwarder":"127.0.0.1"}'

CSV_LINE_RS1 = 'uid1,partner_uid1,1,uid1'
CSV_LINE_RS2 = 'uid2,partner_uid2,1,uid2'
CSV_LINE_RS3 = 'uid3,partner_uid3,1,uid3'

LOG_LINE_GO1 = '2013-09-26T20:00:59+09:00	9999.go	{"remote":"127.0.0.1","host":"-","user":"-","method":"GET","path":"/p-sync?pid=go&sid=9999&uid=encrypted_uid1&google_gid=google_id1&google_cver=1","code":"200","size":"43","referer":"http://example.com","agent":"Test User Agent","forwarder":"127.0.0.1"}'
LOG_LINE_GO2 = '2013-09-26T20:00:59+09:00	9999.go	{"remote":"127.0.0.1","host":"-","user":"-","method":"GET","path":"/p-sync?pid=go&sid=9999&uid=encrypted_uid2&google_gid=google_id2&google_cver=1","code":"200","size":"43","referer":"http://example.com","agent":"Test User Agent","forwarder":"127.0.0.1"}'
LOG_LINE_GO3 = '2013-09-26T20:00:59+09:00	9999.go	{"remote":"127.0.0.1","host":"-","user":"-","method":"GET","path":"/p-sync?pid=go&google_error=0","code":"200","size":"43","referer":"http://example.com","agent":"Test User Agent","forwarder":"127.0.0.1"}'


CSV_LINE_GO1 = 'decrypted_uid1,google_uid1,decrypted_uid1'
CSV_LINE_GO2 = 'decrypted_uid2,google_uid2,decrypted_uid2'

@pytest.fixture
def csv_dir(tmp_mapping_dir):
    return tmp_mapping_dir.mkdir('csv')

@pytest.fixture
def log_dir(tmp_mapping_dir):
    return tmp_mapping_dir.mkdir('log')

def _create_gzip(file, lines):
    with gzip.open(file, 'wb') as f:
        f.write('\n'.join(lines))

def _assert_py_path_file(py_path_file, lines):
    assert py_path_file.check(file=1, exists=1)
    content = '\n'.join(lines)
    if content:
        content = content + '\n'
    assert py_path_file.read() == content


# normal
def test_parse_one_file(log_dir, csv_dir):
    with patch('lib.crypto.AesCrypto.decrypt') as m:
        m.return_value = 'decrypted_uid'

        log_file = log_dir.join('access_log.gz')
        _create_gzip(str(log_file), [LOG_LINE1, LOG_LINE2])

        logparse.parse('9999', 'bp1', ENV, str(log_dir), str(csv_dir))

        csv_file = csv_dir.join('access_log.csv')
        _assert_py_path_file(csv_file, [CSV_LINE1, CSV_LINE2])

def test_parse_many_file(log_dir, csv_dir):
    with patch('lib.crypto.AesCrypto.decrypt') as m:
        m.return_value = 'decrypted_uid'

        log_file1 = log_dir.join('access_log1.gz')
        log_file2 = log_dir.join('access_log2.gz')
        log_file3 = log_dir.join('access_log3.gz')

        _create_gzip(str(log_file1), [LOG_LINE1, LOG_LINE2])
        _create_gzip(str(log_file2), [LOG_LINE3, LOG_LINE4])
        _create_gzip(str(log_file3), [LOG_LINE5, LOG_LINE6])

        logparse.parse('9999', 'bp1', ENV, str(log_dir), str(csv_dir))

        csv_file1 = csv_dir.join('access_log1.csv')
        csv_file2 = csv_dir.join('access_log2.csv')
        csv_file3 = csv_dir.join('access_log3.csv')
        _assert_py_path_file(csv_file1, [CSV_LINE1, CSV_LINE2])
        _assert_py_path_file(csv_file2, [CSV_LINE3, CSV_LINE4])
        _assert_py_path_file(csv_file3, [CSV_LINE5, CSV_LINE6])

def test_parse_empty_file(log_dir, csv_dir):
    with patch('lib.crypto.AesCrypto.decrypt') as m:
        m.return_value = 'decrypted_uid'

        log_file = log_dir.join('access_log.gz')
        _create_gzip(str(log_file), [])

        logparse.parse('9999', 'bp1', ENV, str(log_dir), str(csv_dir))

        csv_file = csv_dir.join('access_log.csv')
        _assert_py_path_file(csv_file, [])

def test_parse_no_file(log_dir, csv_dir):
    with patch('lib.crypto.AesCrypto.decrypt') as m:
        m.return_value = 'decrypted_uid'

        logparse.parse('9999', 'bp1', ENV, str(log_dir), str(csv_dir))

        assert not csv_dir.listdir()

def test_parse_no_uid(log_dir, csv_dir):
    with patch('lib.crypto.AesCrypto.decrypt') as m:
        m.return_value = 'decrypted_uid'

        log_file = log_dir.join('access_log.gz')

        log_line_no_uid = '2013-09-26T20:00:59+09:00	9999.bp1	{"remote":"127.0.0.1","host":"-","user":"-","method":"GET","path":"/p-sync?sid=9999&pid=bp1&pt_uid=partner_uid1","code":"200","size":"43","referer":"http://example.com","agent":"Test User Agent","forwarder":"127.0.0.1"}'
        _create_gzip(str(log_file), [log_line_no_uid, LOG_LINE2])

        logparse.parse('9999', 'bp1', ENV, str(log_dir), str(csv_dir))

        csv_file = csv_dir.join('access_log.csv')
        _assert_py_path_file(csv_file, [CSV_LINE2])

def test_parse_no_ptuid(log_dir, csv_dir):
    with patch('lib.crypto.AesCrypto.decrypt') as m:
        m.return_value = 'decrypted_uid'

        log_file = log_dir.join('access_log.gz')
        log_line_no_ptuid = '2013-09-26T20:00:59+09:00	9999.bp1	{"remote":"127.0.0.1","host":"-","user":"-","method":"GET","path":"/p-sync?sid=9999&uid=encrypted_uid&pid=bp1","code":"200","size":"43","referer":"http://example.com","agent":"Test User Agent","forwarder":"127.0.0.1"}'
        _create_gzip(str(log_file), [log_line_no_ptuid, LOG_LINE2])

        logparse.parse('9999', 'bp1', ENV, str(log_dir), str(csv_dir))

        csv_file = csv_dir.join('access_log.csv')
        _assert_py_path_file(csv_file, [CSV_LINE2])

def test_parse_less_column(log_dir, csv_dir):
    with patch('lib.crypto.AesCrypto.decrypt') as m:
        m.return_value = 'decrypted_uid'

        log_file = log_dir.join('access_log.gz')

        log_line_less_column = '9999.bp1	{"remote":"127.0.0.1","host":"-","user":"-","method":"GET","path":"/p-sync?sid=9999&uid=encrypted_uid&pid=bp1&pt_uid=partner_uid1","code":"200","size":"43","referer":"http://example.com","agent":"Test User Agent","forwarder":"127.0.0.1"}'
        _create_gzip(str(log_file), [log_line_less_column, LOG_LINE2])

        logparse.parse('9999', 'bp1', ENV, str(log_dir), str(csv_dir))

        csv_file = csv_dir.join('access_log.csv')
        _assert_py_path_file(csv_file, [CSV_LINE2])

def test_parse_empty_ptuid(log_dir, csv_dir):
    with patch('lib.crypto.AesCrypto.decrypt') as m:
        m.return_value = 'decrypted_uid'

        log_file = log_dir.join('access_log.gz')

        log_line_empty_ptuid = '2013-09-26T20:00:59+09:00	9999.bp1	{"remote":"127.0.0.1","host":"-","user":"-","method":"GET","path":"/p-sync?sid=9999&uid=encrypted_uid&pid=bp1&pt_uid=&ma_error=1","code":"200","size":"43","referer":"http://example.com","agent":"Test User Agent","forwarder":"127.0.0.1"}'
        _create_gzip(str(log_file), [log_line_empty_ptuid, LOG_LINE2])

        logparse.parse('9999', 'bp1 ', ENV, str(log_dir), str(csv_dir))

        csv_file = csv_dir.join('access_log.csv')
        _assert_py_path_file(csv_file, [CSV_LINE2])

def test_parse_empty_invalid_decrypted_uid(log_dir, csv_dir):
    with patch('lib.crypto.AesCrypto.decrypt') as m:
        m.return_value = None
        m.side_effect  = [
            'decrypted_uid',
            'decrypted_uid',
            '"×à`LÙ5È250Ï¦ŸÉáBamü´ÓTEÂ–EãÛA8wXÁ¯ëÒ6djª',
            '',
            'decrypted_uid'
        ]

        log_file = log_dir.join('access_log.gz')
        _create_gzip(str(log_file), [LOG_LINE1, LOG_LINE2, LOG_LINE3, LOG_LINE4, LOG_LINE5])

        logparse.parse('9999', 'bp1', ENV, str(log_dir), str(csv_dir))

        csv_file = csv_dir.join('access_log.csv')
        _assert_py_path_file(csv_file, [CSV_LINE1, CSV_LINE2, CSV_LINE5])

def test_parse_escapechar(log_dir, csv_dir):
    with patch('lib.crypto.AesCrypto.decrypt') as m:
        m.return_value = 'de\ncryp,ted_\\uid'

        log_file = log_dir.join('access_log.gz')
        _create_gzip(str(log_file), [LOG_LINE1])

        logparse.parse('9999', 'bp1', ENV, str(log_dir), str(csv_dir))

        csv_file = csv_dir.join('access_log.csv')
        _assert_py_path_file(csv_file, ['de\\\ncryp\\,ted_\\\\uid,partner_uid1,1,de\\\ncryp\\,ted_\\\\uid'])


# co
def test_parse_one_file_co(log_dir, csv_dir):
    with patch('lib.crypto.AesCrypto.decrypt') as m:
        m.return_value = 'decrypted_uid'

        log_file = log_dir.join('access_log.gz')
        _create_gzip(str(log_file), [LOG_LINE_CO1, LOG_LINE_CO2])

        logparse.parse('9999', 'co', ENV, str(log_dir), str(csv_dir))

        csv_file = csv_dir.join('access_log.csv')
        _assert_py_path_file(csv_file, [CSV_LINE_CO1])

def test_parse_many_file_co(log_dir, csv_dir):
    with patch('lib.crypto.AesCrypto.decrypt') as m:
        m.return_value = 'decrypted_uid'

        log_file1 = log_dir.join('access_log1.gz')
        log_file2 = log_dir.join('access_log2.gz')
        _create_gzip(str(log_file1), [LOG_LINE_CO1])
        _create_gzip(str(log_file2), [LOG_LINE_CO2])

        logparse.parse('9999', 'co', ENV, str(log_dir), str(csv_dir))

        csv_file1 = csv_dir.join('access_log1.csv')
        csv_file2 = csv_dir.join('access_log2.csv')
        _assert_py_path_file(csv_file1, [CSV_LINE_CO1])
        _assert_py_path_file(csv_file2, [])

def test_parse_no_uid_co(log_dir, csv_dir):
    with patch('lib.crypto.AesCrypto.decrypt') as m:
        m.return_value = 'decrypted_uid'

        log_file = log_dir.join('access_log.gz')

        log_line_no_uid = '2013-09-26T20:00:59+09:00	9999.co	{"remote":"127.0.0.1","host":"-","user":"-","method":"GET","path":"/c-sync?sid=9999","code":"200","size":"43","referer":"http://example.com","agent":"Test User Agent","forwarder":"127.0.0.1","setuid":"-","getuid":"co.uid=tamar_id1"}'
        _create_gzip(str(log_file), [log_line_no_uid, LOG_LINE_CO2])

        logparse.parse('9999', 'co', ENV, str(log_dir), str(csv_dir))

        csv_file = csv_dir.join('access_log.csv')
        _assert_py_path_file(csv_file, [])

def test_parse_no_couid(log_dir, csv_dir):
    with patch('lib.crypto.AesCrypto.decrypt') as m:
        m.return_value = 'decrypted_uid'

        log_file = log_dir.join('access_log.gz')
        _log_line_no_couid = '2013-09-26T20:00:59+09:00	9999.co	{"remote":"127.0.0.1","host":"-","user":"-","method":"GET","path":"/c-sync?sid=9999&uid=encrypted_uid","code":"200","size":"43","referer":"http://example.com","agent":"Test User Agent","forwarder":"127.0.0.1"'
        log_line_no_couid1 = _log_line_no_couid + '}'
        log_line_no_couid2 = _log_line_no_couid + ',"setuid":"-","getuid":"-"}'
        _create_gzip(str(log_file), [log_line_no_couid1, log_line_no_couid2, LOG_LINE_CO2])

        logparse.parse('9999', 'co', ENV, str(log_dir), str(csv_dir))

        csv_file = csv_dir.join('access_log.csv')
        _assert_py_path_file(csv_file, [])

def test_parse_less_column_co(log_dir, csv_dir):
    with patch('lib.crypto.AesCrypto.decrypt') as m:
        m.return_value = 'decrypted_uid'

        log_file = log_dir.join('access_log.gz')

        log_line_less_column = '9999.co	{"remote":"127.0.0.1","host":"-","user":"-","method":"GET","path":"/c-sync?sid=9999&uid=encrypted_uid","code":"200","size":"43","referer":"http://example.com","agent":"Test User Agent","forwarder":"127.0.0.1","setuid":"-","getuid":"co.uid=tamar_id1"}'
        _create_gzip(str(log_file), [log_line_less_column, LOG_LINE_CO2])

        logparse.parse('9999', 'co', ENV, str(log_dir), str(csv_dir))

        csv_file = csv_dir.join('access_log.csv')
        _assert_py_path_file(csv_file, [])

def test_parse_invalid_decrypted_uid_co(log_dir, csv_dir):
    with patch('lib.crypto.AesCrypto.decrypt') as m:
        m.return_value = None
        m.side_effect  = [
            '"×à`LÙ5È250Ï¦ŸÉáBamü´ÓTEÂ–EãÛA8wXÁ¯ëÒ6djª',
            ''
        ]

        log_file = log_dir.join('access_log.gz')
        _create_gzip(str(log_file), [LOG_LINE_CO1, LOG_LINE_CO2])

        logparse.parse('9999', 'bp1', ENV, str(log_dir), str(csv_dir))

        csv_file = csv_dir.join('access_log.csv')
        _assert_py_path_file(csv_file, [])

def test_parse_escapechar_co(log_dir, csv_dir):
    with patch('lib.crypto.AesCrypto.decrypt') as m:
        m.return_value = 'de\ncryp,ted_\\uid'

        log_file = log_dir.join('access_log.gz')
        _create_gzip(str(log_file), [LOG_LINE_CO1])

        logparse.parse('9999', 'co', ENV, str(log_dir), str(csv_dir))

        csv_file = csv_dir.join('access_log.csv')
        _assert_py_path_file(csv_file, ['de\\\ncryp\\,ted_\\\\uid,tamar_id1,2,tamar_id1'])


# rs
def test_parse_one_file_rs(log_dir, csv_dir):
    log_file = log_dir.join('access_log.gz')
    _create_gzip(str(log_file), [LOG_LINE_RS1, LOG_LINE_RS2])

    logparse.parse('9999', 'rs', ENV, str(log_dir), str(csv_dir))

    csv_file = csv_dir.join('access_log.csv')
    _assert_py_path_file(csv_file, [CSV_LINE_RS1, CSV_LINE_RS2])

def test_parse_many_file_rs(log_dir, csv_dir):
    with patch('lib.crypto.AesCrypto.decrypt') as m:
        m.return_value = 'decrypted_uid'

        log_file1 = log_dir.join('access_log1.gz')
        log_file2 = log_dir.join('access_log2.gz')

        _create_gzip(str(log_file1), [LOG_LINE_RS1, LOG_LINE_RS2])
        _create_gzip(str(log_file2), [LOG_LINE_RS2, LOG_LINE_RS3])

        logparse.parse('9999', 'rs', ENV, str(log_dir), str(csv_dir))

        csv_file1 = csv_dir.join('access_log1.csv')
        csv_file2 = csv_dir.join('access_log2.csv')
        _assert_py_path_file(csv_file1, [CSV_LINE_RS1, CSV_LINE_RS2])
        _assert_py_path_file(csv_file2, [CSV_LINE_RS2, CSV_LINE_RS3])

def test_parse_empty_file_rs(log_dir, csv_dir):
    log_file = log_dir.join('access_log.gz')
    _create_gzip(str(log_file), [])

    logparse.parse('9999', 'rs', ENV, str(log_dir), str(csv_dir))

    csv_file = csv_dir.join('access_log.csv')
    _assert_py_path_file(csv_file, [])

def test_parse_no_file_rs(log_dir, csv_dir):
    logparse.parse('9999', 'rs', ENV, str(log_dir), str(csv_dir))

    assert not csv_dir.listdir()

def test_parse_no_uid_rs(log_dir, csv_dir):
    log_file = log_dir.join('access_log.gz')

    log_line_no_uid = '2013-09-26T20:00:59+09:00	9999.rs	{"remote":"127.0.0.1","host":"-","user":"-","method":"GET","path":"/p-sync?sid=9999&pid=rs&pt_uid=partner_uid1","code":"200","size":"43","referer":"http://example.com","agent":"Test User Agent","forwarder":"127.0.0.1"}'
    _create_gzip(str(log_file), [log_line_no_uid, LOG_LINE_RS2])

    logparse.parse('9999', 'rs', ENV, str(log_dir), str(csv_dir))

    csv_file = csv_dir.join('access_log.csv')
    _assert_py_path_file(csv_file, [CSV_LINE_RS2])

def test_parse_no_ptuid_rs(log_dir, csv_dir):
    log_file = log_dir.join('access_log.gz')
    log_line_no_ptuid = '2013-09-26T20:00:59+09:00	9999.rs	{"remote":"127.0.0.1","host":"-","user":"-","method":"GET","path":"/p-sync?sid=9999&uid=uid&pid=rs","code":"200","size":"43","referer":"http://example.com","agent":"Test User Agent","forwarder":"127.0.0.1"}'
    _create_gzip(str(log_file), [log_line_no_ptuid, LOG_LINE_RS2])

    logparse.parse('9999', 'rs', ENV, str(log_dir), str(csv_dir))

    csv_file = csv_dir.join('access_log.csv')
    _assert_py_path_file(csv_file, [CSV_LINE_RS2])

def test_parse_less_column_rs(log_dir, csv_dir):
    log_file = log_dir.join('access_log.gz')

    log_line_less_column = '9999.rs	{"remote":"127.0.0.1","host":"-","user":"-","method":"GET","path":"/p-sync?sid=9999&uid=uid&pid=rs&pt_uid=partner_uid1","code":"200","size":"43","referer":"http://example.com","agent":"Test User Agent","forwarder":"127.0.0.1"}'
    _create_gzip(str(log_file), [log_line_less_column, LOG_LINE_RS2])

    logparse.parse('9999', 'rs', ENV, str(log_dir), str(csv_dir))

    csv_file = csv_dir.join('access_log.csv')
    _assert_py_path_file(csv_file, [CSV_LINE_RS2])

def test_parse_empty_ptuid_rs(log_dir, csv_dir):
    log_file = log_dir.join('access_log.gz')

    log_line_empty_ptuid = '2013-09-26T20:00:59+09:00	9999.rs	{"remote":"127.0.0.1","host":"-","user":"-","method":"GET","path":"/p-sync?sid=9999&uid=encrypted_uid&pid=rs&pt_uid=","code":"200","size":"43","referer":"http://example.com","agent":"Test User Agent","forwarder":"127.0.0.1"}'
    _create_gzip(str(log_file), [log_line_empty_ptuid, LOG_LINE_RS2])

    logparse.parse('9999', 'rs', ENV, str(log_dir), str(csv_dir))

    csv_file = csv_dir.join('access_log.csv')
    _assert_py_path_file(csv_file, [CSV_LINE_RS2])

def test_parse_escapechar_rs(log_dir, csv_dir):
    log_file = log_dir.join('access_log.gz')

    log_line = '2013-09-26T20:00:59+09:00	9999.rs	{"remote":"127.0.0.1","host":"-","user":"-","method":"GET","path":"/p-sync?sid=9999&uid=ui\\nd1&pid=rs&pt_uid=par,tner_\\\\uid1","code":"200","size":"43","referer":"http://example.com","agent":"Test User Agent","forwarder":"127.0.0.1"}'
    _create_gzip(str(log_file), [log_line])

    logparse.parse('9999', 'rs', ENV, str(log_dir), str(csv_dir))

    csv_file = csv_dir.join('access_log.csv')
    _assert_py_path_file(csv_file, ['ui\\\nd1,par\\,tner_\\\\uid1,1,ui\\\nd1'])

def test_yield_from_fluentd(log_dir, csv_dir):
    log_file = log_dir.join('access_log.gz')
    _create_gzip(str(log_file), [LOG_LINE1, LOG_LINE2])

    expected = [['encrypted_uid', 'partner_uid1'],
                ['encrypted_uid', 'partner_uid2']]
    result = []
    for uid, pt_uid in logparse.yield_from_fluentd(str(log_file), 'uid', 'pt_uid'):
        result.append([uid, pt_uid])

    assert expected == result

def test_yield_from_fluentd_including_invalid_log(log_dir, csv_dir):
    log_file = log_dir.join('access_log.gz')
    _create_gzip(str(log_file), [LOG_LINE1,
                                 '2013-09-26T20:00:59+09:00	9999.bp1	{"remote":"127.0.0.1","host":"-","user":"-","method":"GET","path":"/p-sync?hoge=fuga","code":"200","size":"43","referer":"http://example.com","agent":"Test User Agent","forwarder":"127.0.0.1"}'])

    expected = [['encrypted_uid', 'partner_uid1']]
    result = []
    for uid, pt_uid in logparse.yield_from_fluentd(str(log_file), 'uid', 'pt_uid'):
        result.append([uid, pt_uid])

    assert expected == result

def test_yield_from_fluentd_for_co(log_dir, csv_dir):
    log_file = log_dir.join('access_log.gz')
    _create_gzip(str(log_file), [LOG_LINE_CO1, LOG_LINE_CO2])

    expected = [['tamar_id1', 'encrypted_uid']]
    result = []
    for tamar_id, uid in logparse.yield_from_fluentd_for_co(str(log_file), 'uid'):
        result.append([tamar_id, uid])

    assert expected == result

def test_yield_from_fluentd_for_co_with_invalid_log(log_dir, csv_dir):
    log_file = log_dir.join('access_log.gz')
    _create_gzip(str(log_file), [LOG_LINE_CO1, LOG_LINE_CO3])

    expected = [['tamar_id1', 'encrypted_uid']]

    result = []
    for tamar_id, uid in logparse.yield_from_fluentd_for_co(str(log_file), 'uid'):
        result.append([tamar_id, uid])

    assert expected == result

def test_yield_from_fluentd_for_google_sync(log_dir, csv_dir):
    log_file = log_dir.join('access_log.gz')
    _create_gzip(str(log_file), [LOG_LINE_GO1, LOG_LINE_GO2])

    expected = [['encrypted_uid1', 'google_id1'],
                ['encrypted_uid2', 'google_id2']]

    result = []
    for uid, pt_uid in logparse.yield_from_fluentd(str(log_file), 'uid', 'google_gid'):
        result.append([uid, pt_uid])

    assert expected == result

def test_yield_from_fluentd_for_google_sync_with_google_error_user(log_dir, csv_dir):
    log_file = log_dir.join('access_log.gz')
    _create_gzip(str(log_file), [LOG_LINE_GO1, LOG_LINE_GO3])

    expected = [['encrypted_uid1', 'google_id1']]

    result = []
    for uid, pt_uid in logparse.yield_from_fluentd(str(log_file), 'uid', 'google_gid'):
        result.append([uid, pt_uid])

    assert expected == result
