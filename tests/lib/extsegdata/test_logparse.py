# -*- coding: utf-8 -*-

from lib.extsegdata import logparse

import datetime
import gzip
from   mock import patch
import pytest


ENV = 'dev'

LOG_LINE1      = '2013-09-26T20:00:59+09:00	9999.bp1	{"ttl":1408957978,"seg_id":"seg_id1","pid":"im","uids":["p-user1","p-user2"],"sid":"9999"}'
LOG_LINE2      = '2013-09-26T20:00:59+09:00	9999.bp1	{"ttl":null,"seg_id":"seg_id2","pid":"im","uids":["p-user1","p-user3"],"sid":"9999"}'
LOG_LINE3      = '2013-09-26T20:00:59+09:00	9999.bp1	{"ttl":null,"seg_id":"seg_id3","pid":"im","uids":[],"sid":"9999"}'
LOG_LINE_NOSEG = '2013-09-26T20:00:59+09:00	9999.bp1	{"ttl":null,"pid":"im","uids":["p-user1"],"sid":"9999"}'
LOG_LINE_NOUID = '2013-09-26T20:00:59+09:00	9999.bp1	{"ttl":null,"seg_id":"seg_id1","pid":"im","sid":"9999"}'
LOG_LINE_UTF8  = u'2013-09-26T20:00:59+09:00	9999.bp1	{"ttl":null,"seg_id":"seg_id1","pid":"im","uids":["\ufffd"],"sid":"9999"}'.encode('utf-8')

CSV_LINE1 = 'p-user1,seg_id1,2014-08-25 09:12:58'
CSV_LINE2 = 'p-user2,seg_id1,2014-08-25 09:12:58'
CSV_LINE3 = 'p-user1,seg_id2,2014-07-22 23:40:59'
CSV_LINE4 = 'p-user3,seg_id2,2014-07-22 23:40:59'

@pytest.fixture
def csv_dir(tmp_mapping_dir):
    return tmp_mapping_dir.mkdir('csv')

@pytest.fixture
def log_dir(tmp_mapping_dir):
    return tmp_mapping_dir.mkdir('log')

class CurrentDatetime(datetime.datetime):
    @classmethod
    def utcnow(cls):
        return cls(2014, 07, 19, 23, 40, 59, 0)

def _create_gzip(file, lines):
    with gzip.open(file, 'wb') as f:
        f.write('\n'.join(lines))

def _assert_py_path_file(py_path_file, lines):
    assert py_path_file.check(file=1, exists=1)
    content = '\n'.join(lines)
    if content:
        content = content + '\n'
    assert py_path_file.read() == content


# normal
def test_parse_file(log_dir, csv_dir):
    with patch('datetime.datetime', CurrentDatetime) as m:

        log_file1 = log_dir.join('access_log1.gz')
        log_file2 = log_dir.join('access_log2.gz')
        log_file3 = log_dir.join('access_log3.gz')

        _create_gzip(str(log_file1), [LOG_LINE1])
        _create_gzip(str(log_file2), [LOG_LINE2])
        _create_gzip(str(log_file3), [LOG_LINE3])

        logparse.parse('9999', 'bp1', ENV, str(log_dir), str(csv_dir))

        csv_file1 = csv_dir.join('access_log1.csv')
        csv_file2 = csv_dir.join('access_log2.csv')
        csv_file3 = csv_dir.join('access_log3.csv')
        _assert_py_path_file(csv_file1, [CSV_LINE1, CSV_LINE2])
        _assert_py_path_file(csv_file2, [CSV_LINE3, CSV_LINE4])
        _assert_py_path_file(csv_file3, [])

def test_parse_empty_file(log_dir, csv_dir):
    with patch('datetime.datetime', CurrentDatetime) as m:

        log_file = log_dir.join('access_log.gz')
        _create_gzip(str(log_file), [])

        logparse.parse('9999', 'bp1', ENV, str(log_dir), str(csv_dir))

        csv_file = csv_dir.join('access_log.csv')
        _assert_py_path_file(csv_file, [])

def test_parse_no_file(log_dir, csv_dir):
    with patch('datetime.datetime', CurrentDatetime) as m:

        logparse.parse('9999', 'bp1', ENV, str(log_dir), str(csv_dir))

        assert not csv_dir.listdir()

def test_parse_no_seg(log_dir, csv_dir):
    with patch('datetime.datetime', CurrentDatetime) as m:

        log_file = log_dir.join('access_log.gz')

        _create_gzip(str(log_file), [LOG_LINE_NOSEG])

        logparse.parse('9999', 'bp1', ENV, str(log_dir), str(csv_dir))

        csv_file = csv_dir.join('access_log.csv')
        _assert_py_path_file(csv_file, [])

def test_parse_no_uid(log_dir, csv_dir):
    with patch('datetime.datetime', CurrentDatetime) as m:

        log_file = log_dir.join('access_log.gz')

        _create_gzip(str(log_file), [LOG_LINE_NOUID])

        logparse.parse('9999', 'bp1', ENV, str(log_dir), str(csv_dir))

        csv_file = csv_dir.join('access_log.csv')
        _assert_py_path_file(csv_file, [])

def test_yield_from_fluentd(log_dir, csv_dir):
    with patch('datetime.datetime', CurrentDatetime) as m:
        log_file = log_dir.join('access_log.gz')
        _create_gzip(str(log_file), [LOG_LINE1, LOG_LINE2])

        expected = [[ 'seg_id1', ["p-user1","p-user2"], 1408957978],
                    [ 'seg_id2', ["p-user1","p-user3"], None ]]
        result = []
        for seg_id, uids, ttl in logparse.yield_from_fluentd(str(log_file)):
            result.append([seg_id, uids, ttl])

        assert expected == result

def test_parse_unicode_encode_error(log_dir, csv_dir):
    with patch('datetime.datetime', CurrentDatetime) as m:

        log_file1 = log_dir.join('access_log1.gz')

        _create_gzip(str(log_file1), [LOG_LINE_UTF8])

        logparse.parse('9999', 'bp1', ENV, str(log_dir), str(csv_dir))

        csv_file1 = csv_dir.join('access_log1.csv')
        _assert_py_path_file(csv_file1, [])
