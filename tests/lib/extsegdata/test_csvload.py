# -*- coding: utf-8 -*-

import datetime
import pytest
import MySQLdb
from lib.extsegdata import csvload
from lib.extsegdatatable import ExtSegDataTable

ENV = 'dev'
CSV_LINES = '''
pt_user1,segment_1,2014-08-01 12:00:00
pt_user1,segment_2,2014-08-01 12:00:00
pt_user2,segment_3,
'''.lstrip()

@pytest.fixture
def csv_dir(tmpdir):
    return tmpdir.mkdir('csv')

@pytest.fixture
def tbl():
    DB_USER  = 'tamaruser'
    DB_PASS  = '#!TawaR?u5er'
    DB_HOST  = 'localhost'
    DB_NAME  = 'DB_TAMAR_9999'
    TBL_NAME = 't_user_extsegdata_9999_bp1'
    return ExtSegDataTable(DB_USER, DB_PASS, DB_HOST, DB_NAME, TBL_NAME)

@pytest.fixture(autouse=True)
def truncate_table(tbl):
    tbl.truncate_table()

def _assert_row(row, user_id, segment_id, ttl):
    assert row[0] == user_id
    assert row[1] == segment_id
    assert row[2] == ttl

def _prepare_csv_file(csv_dir, csv_filename, content):
    csv_file = csv_dir.join(csv_filename)
    csv_file.write(content)

def test_load_insert_use_temporary_table(csv_dir, tbl):

    # prepare
    _prepare_csv_file(csv_dir, 'extsegdata_log.csv', CSV_LINES)

    # execute
    csvload.load('9999', 'bp1', ENV, str(csv_dir), use_temporary_table=True)

    # assert
    rows = tbl.select_all_rows()
    assert len(rows) == 3
    _assert_row(rows[0], 'pt_user1', 'segment_1', datetime.datetime(2014, 8, 1, 12, 0))
    _assert_row(rows[1], 'pt_user1', 'segment_2', datetime.datetime(2014, 8, 1, 12, 0))
    _assert_row(rows[2], 'pt_user2', 'segment_3', None) 

def test_load_insert_no_use_temporary_table(csv_dir, tbl):

    # prepare
    _prepare_csv_file(csv_dir, 'extsegdata_log.csv', CSV_LINES)

    # execute
    csvload.load('9999', 'bp1', ENV, str(csv_dir), use_temporary_table=False)

    # assert
    rows = tbl.select_all_rows()
    assert len(rows) == 3
    _assert_row(rows[0], 'pt_user1', 'segment_1', datetime.datetime(2014, 8, 1, 12, 0))
    _assert_row(rows[1], 'pt_user1', 'segment_2', datetime.datetime(2014, 8, 1, 12, 0))
    _assert_row(rows[2], 'pt_user2', 'segment_3', None)

def test_load_update(csv_dir, tbl):

    # prepare table
    tbl.insert_row('pt_user1', 'segment_1', '2014-08-01 11:00:00')
    tbl.insert_row('pt_user1', 'segment_2', '2014-08-01 11:00:00')

    # prepare csv
    _prepare_csv_file(csv_dir, 'extsegdata_log.csv', CSV_LINES)

    # execute
    csvload.load('9999', 'bp1', ENV, str(csv_dir))

    # assert
    rows = tbl.select_all_rows()
    assert len(rows) == 3
    _assert_row(rows[0], 'pt_user1', 'segment_1', datetime.datetime(2014, 8, 1, 12, 0))
    _assert_row(rows[1], 'pt_user1', 'segment_2', datetime.datetime(2014, 8, 1, 12, 0))
    _assert_row(rows[2], 'pt_user2', 'segment_3', None)

def test_load_empty_file(csv_dir, tbl):

    # prepare
    _prepare_csv_file(csv_dir, 'extsegdata_log.csv', '')

    # execute
    csvload.load('9999', 'bp1', ENV, str(csv_dir))

    # assert
    rows = tbl.select_all_rows()
    assert len(rows) == 0

def test_load_no_file(csv_dir, tbl):

    # execute
    csvload.load('9999', 'bp1', ENV, str(csv_dir))

def test_load_no_table(csv_dir, tbl):

    # prepare
    _prepare_csv_file(csv_dir, 'extsegdata_log.csv', CSV_LINES)

    # execute
    with pytest.raises(MySQLdb.Error):
        csvload.load('9999', 'xxx', ENV, str(csv_dir))

def test_load_no_database_config(csv_dir, tbl):

    # execute
    with pytest.raises(ValueError):
        csvload.load('xxx', 'bp1', ENV, csv_dir)

def test_load_csv_directory_not_exists(csv_dir, tbl):

    # execute
    with pytest.raises(ValueError):
        csvload.load('xxx', 'bp1', ENV, '/data/csv')
