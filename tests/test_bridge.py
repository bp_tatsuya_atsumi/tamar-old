# -*- coding: utf-8 -*-

from __future__       import absolute_import
from lib.mappingtable import MappingTable
from mock             import patch
from sqlalchemy.sql   import text
import bridge
import contextlib
import datetime
import pytest

ENV = 'dev'
SID = '9999'
PID = 'rt9998'

class CurrentDatetime(datetime.datetime):
    @classmethod
    def now(cls):
        return cls(2013, 10, 19, 23, 50, 0, 0)

def _open_table(sid, pid):
    db_user  = 'tamaruser'
    db_pass  = '#!TawaR?u5er'
    db_host  = 'localhost'
    db_name  = 'DB_TAMAR_{sid}'.format(sid=sid)
    tbl_name = 't_user_mapping_{sid}_{pid}'.format(sid=sid, pid=pid)
    mapping_table = MappingTable(db_user, db_pass, db_host, db_name, tbl_name)
    return mapping_table

def _init(sid=SID, pid=PID, env=ENV, target_hour=None):
    # 9999_rt9998
    mapping_table_9999_rt9998 = _open_table(sid, pid)
    mapping_table_9999_rt9998.truncate_table()

    # 9999_co
    mapping_table_9999 = _open_table(sid, 'co')
    mapping_table_9999.truncate_table()
    sample_data_9999 = [{
                        'user_id'         : 'rt9999-1',
                        'partner_user_id' : 'co1',
                        'tamar_user_type' : 2,
                        'tamar_user_id'   : 'co1',
                        'inserted_at'     : datetime.datetime(2013, 10, 19, 21, 10, 0),
                        'updated_at'      : datetime.datetime(2013, 10, 19, 21, 10, 0),
                        'is_deleted'      : 0
                       },{
                        'user_id'         : 'rt9999-2',
                        'partner_user_id' : 'co2',
                        'tamar_user_type' : 2,
                        'tamar_user_id'   : 'co2',
                        'inserted_at'     : datetime.datetime(2013, 10, 19, 20, 10, 0),
                        'updated_at'      : datetime.datetime(2013, 10, 19, 20, 10, 0),
                        'is_deleted'      : 0
                       }]
    for data in sample_data_9999:
        mapping_table_9999.insert_row(data['user_id'],
                                      data['partner_user_id'],
                                      data['tamar_user_type'],
                                      data['tamar_user_id'],
                                      data['inserted_at'],
                                      data['updated_at'],
                                      data['is_deleted'])

    # 9998_co
    mapping_table_9998 = _open_table(pid[2:], 'co')
    mapping_table_9998.truncate_table()
    sample_data_9998 = [{
                        'user_id'         : 'rt9998-1',
                        'partner_user_id' : 'co1',
                        'tamar_user_type' : 2,
                        'tamar_user_id'   : 'co1',
                        'inserted_at'     : datetime.datetime(2013, 10, 12, 23, 10, 0),
                        'updated_at'      : datetime.datetime(2013, 10, 12, 23, 10, 0),
                        'is_deleted'      : 0
                       },{
                        'user_id'         : 'rt9998-2',
                        'partner_user_id' : 'co2',
                        'tamar_user_type' : 2,
                        'tamar_user_id'   : 'co2',
                        'inserted_at'     : datetime.datetime(2013, 10, 12, 23, 10, 0),
                        'updated_at'      : datetime.datetime(2013, 10, 12, 23, 10, 0),
                        'is_deleted'      : 0
                       },{
                        'user_id'         : 'rt9998-3',
                        'partner_user_id' : 'co2',
                        'tamar_user_type' : 2,
                        'tamar_user_id'   : 'co2',
                        'inserted_at'     : datetime.datetime(2013, 10, 12, 23, 10, 0),
                        'updated_at'      : datetime.datetime(2013, 10, 12, 23, 10, 0),
                        'is_deleted'      : 0
                       }]
    for data in sample_data_9998:
        mapping_table_9998.insert_row(data['user_id'],
                                      data['partner_user_id'],
                                      data['tamar_user_type'],
                                      data['tamar_user_id'],
                                      data['inserted_at'],
                                      data['updated_at'],
                                      data['is_deleted'])

    return bridge.Bridge(sid, pid, env, target_hour)

@patch('datetime.datetime', CurrentDatetime)
def test_execute_bridging():
    target=_init()
    results = target._execute_bridging()
    assert results.rowcount > 0

@patch('datetime.datetime', CurrentDatetime)
def test_execute_bridging_set_target_hour():
    target_datetime = datetime.datetime.now() - datetime.timedelta(hours=3)
    target=_init(target_hour=target_datetime.strftime('%Y%m%d%H'))
    results = target._execute_bridging()
    assert results.rowcount > 0

@patch('datetime.datetime', CurrentDatetime)
def test_execute():
    target=_init()
    results = target.execute()
    with _open_table(SID, PID) as con:
        rows = con.select_all_rows()
    assert len(rows) == 1
    assert rows[0]['user_id']         == 'rt9999-1'
    assert rows[0]['partner_user_id'] == 'rt9998-1'
    assert rows[0]['tamar_user_type'] == 2
    assert rows[0]['tamar_user_id']   == 'co1'

@patch('datetime.datetime', CurrentDatetime)
def test_execute_set_target_hour():
    target_datetime = datetime.datetime.now() - datetime.timedelta(hours=3)
    target=_init(target_hour=target_datetime.strftime('%Y%m%d%H'))
    results = target.execute()
    with _open_table(SID, PID) as con:
        rows = con.select_all_rows()
    assert len(rows) == 2
    assert rows[0]['user_id']         == 'rt9999-2'
    assert rows[0]['partner_user_id'] == 'rt9998-2'
    assert rows[0]['tamar_user_type'] == 2
    assert rows[0]['tamar_user_id']   == 'co2'
    assert rows[1]['user_id']         == 'rt9999-2'
    assert rows[1]['partner_user_id'] == 'rt9998-3'
    assert rows[1]['tamar_user_type'] == 2
    assert rows[1]['tamar_user_id']   == 'co2'
