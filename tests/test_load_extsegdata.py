# -*- coding: utf-8 -*-

from __future__ import absolute_import
from mock       import patch
import load_extsegdata
import contextlib
import datetime
import pytest


ENV = 'dev'
SID = '9999'
PID = 'bp1'

@pytest.fixture
def csv_dir(tmp_extsegdata_dir):
    return tmp_extsegdata_dir.join(SID).join(PID).join('csv')

@pytest.fixture
def log_dir(tmp_extsegdata_dir):
    return tmp_extsegdata_dir.join(SID).join(PID).join('log')

class CurrentDatetime(datetime.datetime):
    @classmethod
    def now(cls):
        return cls(2013, 10, 19, 23, 59, 59, 0)

@contextlib.contextmanager
def _patches_with_default_action(func_init_config):
    with contextlib.nested (
        patch('datetime.datetime', CurrentDatetime),
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('lib.s3util.S3Util.download_files'),
        patch('lib.extsegdata.logparse.parse'),
        patch('lib.extsegdata.csvload.load'),
        patch('lib.s3util.S3Util.upload_files')
    ) as (m_today, m_config, m_s3download, m_parselog, m_loadcsv, m_s3upload):
        m_s3download.return_value = [{}, {}]
        yield (m_s3download, m_parselog, m_loadcsv, m_s3upload)

def test_execute(log_dir, csv_dir, func_init_config):

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3download, m_parselog, m_loadcsv, m_s3upload = mocks
        load_extsegdata.ExtSegDataLoader(SID, PID, ENV).execute()

    # assert
    assert m_s3download.call_count == 1
    assert m_s3download.call_args_list[0][0] == (u'dev-api-log', u'api-data-log/9999/bp1/2013/10/19/22/', str(log_dir))

    assert m_parselog.call_count == 1
    assert m_parselog.call_args_list[0][0] == (SID, PID, ENV, log_dir, csv_dir)

    assert m_loadcsv.call_count == 1
    assert m_loadcsv.call_args_list[0][0] == (SID, PID, ENV, csv_dir)

    assert m_s3upload.call_count == 1
    assert m_s3upload.call_args_list[0][0] == (csv_dir, u'dev-api-log', u'extseg-data/9999/bp1/2013/10/19/22/')

def test_execute_specify_hour(log_dir, csv_dir, func_init_config):

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3download, m_parselog, m_loadcsv, m_s3upload = mocks
        load_extsegdata.ExtSegDataLoader(SID, PID, ENV, '2099123123').execute()

    # assert
    assert m_s3download.call_count == 1
    assert m_s3download.call_args_list[0][0] == (u'dev-api-log', u'api-data-log/9999/bp1/2099/12/31/23/', str(log_dir))

    assert m_parselog.call_count == 1
    assert m_parselog.call_args_list[0][0] == (SID, PID, ENV, log_dir, csv_dir)

    assert m_loadcsv.call_count == 1
    assert m_loadcsv.call_args_list[0][0] == (SID, PID, ENV, csv_dir)

    assert m_s3upload.call_count == 1
    assert m_s3upload.call_args_list[0][0] == (csv_dir, u'dev-api-log', u'extseg-data/9999/bp1/2099/12/31/23/')

def test_execute_no_logs(log_dir, csv_dir, func_init_config):

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3download, m_parselog, m_loadcsv, m_s3upload = mocks
        m_s3download.return_value = []
        load_extsegdata.ExtSegDataLoader(SID, PID, ENV).execute()

    # assert
    assert m_s3download.call_count == 1
    assert m_s3download.call_args_list[0][0] == (u'dev-api-log', u'api-data-log/9999/bp1/2013/10/19/22/', str(log_dir))

    assert m_parselog.call_count == 0
    assert m_loadcsv.call_count  == 0
    assert m_s3upload.call_count == 0
