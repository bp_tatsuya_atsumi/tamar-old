# -*- coding: utf-8 -*-

from __future__ import absolute_import
import api_data
import base64
import contextlib
import json
import lib.extapi.errors as errors
from   mock              import Mock
from   mock              import patch
import requests

ENV = 'dev'
PID = 'bp1'
SID = '9999'
FORBIDDEN_SID = '9998'

CONTENT_TYPE = 'application/json;charset=UTF-8'

POST_HEADERS       = { 'Authorization': 'Basic ' + base64.b64encode('bp_user' + ":" + 'eqd0IavUAtP9qb8n'),
                       'Accept'       : 'applicatin/json;charset=UTF-8' }
FALSE_POST_HEADERS = { 'Authorization': 'Basic ' + base64.b64encode('bp_user' + ":" + 'false_password'),
                       'Accept'       : 'applicatin/json;charset=UTF-8' }


def _init():
     api_data.api.app.config['TESTING'] = True
     return api_data.api.app.test_client()

@contextlib.contextmanager
def _patches():
    with patch('requests.post') as m_post:
        m_post.return_value = Mock()
        m_post.return_value.status_code = 200
        yield m_post

def test_unauthorized():
    with _patches() as m_post:
        result = _init().post('/v1/segdata',
                              data=json.dumps({'pid'   : PID,
                                               'sid'   : SID,
                                               'seg_id': 'f100',
                                               'uids'  : ['user1', 'user2']}),
                              content_type=CONTENT_TYPE,
                              headers=FALSE_POST_HEADERS)
    assert result.status_code == 401

def test_forbidden_sid():
    with _patches() as m_post:
        result = _init().post('/v1/segdata',
                              data=json.dumps({'pid'   : PID,
                                               'sid'   : FORBIDDEN_SID,
                                               'seg_id': 'f100',
                                               'uids'  : ['user1', 'user2']}),
                              content_type=CONTENT_TYPE,
                              headers=POST_HEADERS)
    assert result.status_code == 401

def test_post():
    with _patches() as m_post:
        result = _init().post('/v1/segdata',
                              data=json.dumps({'pid'   : PID,
                                               'sid'   : SID,
                                               'seg_id': 'f100',
                                               'uids'  : ['user1', 'user2']}),
                              content_type=CONTENT_TYPE,
                              headers=POST_HEADERS)
    assert result.status_code == 200
    assert json.loads(result.get_data()) == { 'status' : 'ok' }

    # with ttl
    with _patches() as m_post:
        result = _init().post('/v1/segdata',
                              data=json.dumps({'pid'   : PID,
                                               'sid'   : SID,
                                               'seg_id': 'f100',
                                               'uids'  : ['user1', 'user2'],
                                               'ttl'   : 1408435971}),
                              content_type=CONTENT_TYPE,
                              headers=POST_HEADERS)
    assert result.status_code == 200
    assert json.loads(result.get_data()) == { 'status' : 'ok' }

    # uids length is maximum
    uids_maximum = ['user'] * 500
    with _patches() as m_post:
        result = _init().post('/v1/segdata',
                              data=json.dumps({'sid'   : SID,
                                               'pid'   : PID,
                                               'seg_id': 'f100',
                                               'uids'  : uids_maximum}),
                              content_type=CONTENT_TYPE,
                              headers=POST_HEADERS)
    assert result.status_code == 200
    assert json.loads(result.get_data()) == { 'status' : 'ok' }

def test_post_parameter_missing():
    # SID
    with _patches() as m_post:
        result = _init().post('/v1/segdata',
                              data=json.dumps({'pid'   : PID,
                                               'seg_id': 'f100',
                                               'uids'  : ['user1', 'user2']}),
                              content_type=CONTENT_TYPE,
                              headers=POST_HEADERS)
    assert result.status_code == 401

    # PID
    with _patches() as m_post:
        result = _init().post('/v1/segdata',
                              data=json.dumps({'sid'   : SID,
                                               'seg_id': 'f100',
                                               'uids'  : ['user1', 'user2']}),
                              content_type=CONTENT_TYPE,
                              headers=POST_HEADERS)
    assert result.status_code == 401

    # SEG_ID
    with _patches() as m_post:
        result = _init().post('/v1/segdata',
                              data=json.dumps({'sid'   : SID,
                                               'pid'   : PID,
                                               'uids'  : ['user1', 'user2']}),
                              content_type=CONTENT_TYPE,
                              headers=POST_HEADERS)
    expected_error = errors.IllegalParameter()
    assert result.status_code == expected_error.code == 400
    assert json.loads(result.get_data()) == { 'message': expected_error.description, 'status_code': expected_error.code }

def test_post_illegal_parameter():
    # uid is not list
    with _patches() as m_post:
        result = _init().post('/v1/segdata',
                              data=json.dumps({'sid'   : SID,
                                               'pid'   : PID,
                                               'seg_id': 'f100',
                                               'uids'  : 'user1'}),
                              content_type=CONTENT_TYPE,
                              headers=POST_HEADERS)
    expected_error = errors.IllegalParameter()
    assert result.status_code == expected_error.code == 400
    assert json.loads(result.get_data()) == { 'message': expected_error.description, 'status_code': expected_error.code }

    # uid is too long
    uids_too_long = ['user'] * 501
    with _patches() as m_post:
        result = _init().post('/v1/segdata',
                              data=json.dumps({'sid'   : SID,
                                               'pid'   : PID,
                                               'seg_id': 'f100',
                                               'uids'  : uids_too_long}),
                              content_type=CONTENT_TYPE,
                              headers=POST_HEADERS)
    expected_error = errors.IllegalParameter()
    assert result.status_code == expected_error.code == 400
    assert json.loads(result.get_data()) == { 'message': expected_error.description, 'status_code': expected_error.code }

    # ttl is not timestamp
    with _patches() as m_post:
        result = _init().post('/v1/segdata',
                              data=json.dumps({'sid'   : SID,
                                               'pid'   : PID,
                                               'seg_id': 'f100',
                                               'uids'  : ['user1', 'user2'],
                                               'ttl'   : 'string'}),
                              content_type=CONTENT_TYPE,
                              headers=POST_HEADERS)
    expected_error = errors.IllegalParameter()
    assert result.status_code == expected_error.code == 400
    assert json.loads(result.get_data()) == { 'message': expected_error.description, 'status_code': expected_error.code }

def test_post_fluentd_is_not_ready():
    with _patches() as m_post:
        m_post.side_effect = requests.exceptions.ConnectionError()
        result = _init().post('/v1/segdata',
                              data=json.dumps({'sid'   : SID,
                                               'pid'   : PID,
                                               'seg_id': 'f100',
                                               'uids'  : ['user1', 'user2']}),
                              content_type=CONTENT_TYPE,
                              headers=POST_HEADERS)
    expected_error = errors.InternalError()
    assert result.status_code == expected_error.code == 500
    assert json.loads(result.get_data()) == { 'message': expected_error.description, 'status_code': expected_error.code }

def test_post_fluentd_returns_not_ok():
    with _patches() as m_post:
        m_post.return_value.status_code = 400
        result = _init().post('/v1/segdata',
                              data=json.dumps({'sid'   : SID,
                                               'pid'   : PID,
                                               'seg_id': 'f100',
                                               'uids'  : ['user1', 'user2']}),
                              content_type=CONTENT_TYPE,
                              headers=POST_HEADERS)
    expected_error = errors.InternalError()
    assert result.status_code == expected_error.code == 500
    assert json.loads(result.get_data()) == { 'message': expected_error.description, 'status_code': expected_error.code }
