# -*- coding : utf-8 -*-

import json
import virtual_partner.microad

def _init_app():
    virtual_partner.microad.app.config['TESTING'] = True
    return virtual_partner.microad.app.test_client()

def test_sync_valid_params():
    microad = _init_app()
    rv = microad.get('/pixel?ma_mid=12345&pid=ma&uid=hoge&sid=001')
    assert rv.status_code == 302

def test_sync_invalid_params():
    microad = _init_app()
    rv = microad.get('/pixel?ma_mid=12345&sid=001')
    assert rv.status_code == 400


def test_regist_segment_mst_valid_params():
    microad = _init_app()

    # Case of one segment
    params = {
                'app_key'       : 'app_key_id',
                'login_id'      : 'user1_id',
                'password'      : 'password1',
                'advertiser_id' : 'ad_id_1',
                'seg_id_1'      : 1,
                'seg_name_1'    : 'name1',
                'seg_note_1'    : 'note1'
             }
    rv = microad.post('/segmentlist', data=params)
    response = json.loads(rv.data)
    expected_response = {
                            'login_id'      : 'user1_id',
                            'advertiser_id' : 'ad_id_1',
                            'segment_count' : 1
                        }
    assert response == expected_response

    # Case of multi segments
    params = {
                'app_key'       : 'app_key_id',
                'login_id'      : 'user1_id',
                'password'      : 'password1',
                'advertiser_id' : 'ad_id_1',
                'seg_id_1'      : 1,
                'seg_name_1'    : 'name1',
                'seg_note_1'    : 'note1',
                'seg_id_2'      : 2,
                'seg_name_2'    : 'name2',
                'seg_note_2'    : 'note2'
             }
    rv = microad.post('/segmentlist', data=params)
    response = json.loads(rv.data)
    expected_response = {
                            'login_id'      : 'user1_id',
                            'advertiser_id' : 'ad_id_1',
                            'segment_count' : 2
                        }
    assert response == expected_response


def test_regist_segment_mst_invalid_params():
    microad = _init_app()

    # case : lack of params
    params = {
                'app_key'       : 'app_key_id',
                'login_id'      : 'user1_id',
                'advertiser_id' : 'ad_id_1',
                'seg_name_1'    : 'name1',
                'seg_note_1'    : 'note1'
             }
    rv = microad.post('/segmentlist', data=params)
    assert rv.status_code == 400

    # case : lack of params2
    params = {
                'app_key'       : 'app_key_id',
                'login_id'      : 'user1_id',
                'password'      : 'password1',
                'advertiser_id' : 'ad_id_1',
                'seg_name_1'    : 'name1',
                'seg_note_1'    : 'note1'
             }
    rv = microad.post('/segmentlist', data=params)
    assert rv.status_code == 400


    # case : invalid param
    params = {
                'app_key'       : 'app_key_id',
                'login_id'      : 'user1_id',
                'password'      : 'password1',
                'advertiser_id' : 'ad_id_1',
                'hoge'          : 'fuga'
             }
    rv = microad.post('/segmentlist', data=params)
    assert rv.status_code == 400

def test_regist_segment_mst_error():
    microad = _init_app()

    params = {
                'app_key'       : '401',
                'login_id'      : 'user1_id',
                'password'      : 'password1',
                'advertiser_id' : 'ad_id_1',
                'seg_id_1'      : 1,
                'seg_name_1'    : 'name1',
                'seg_note_1'    : 'note1'
             }
    rv = microad.post('/segmentlist', data=params)
    assert rv.status_code == 401

    params['app_key'] = '500'
    rv = microad.post('/segmentlist', data=params)
    assert rv.status_code == 500

    params['app_key'] = '503'
    rv = microad.post('/segmentlist', data=params)
    assert rv.status_code == 503

def test_remove_segment_mst_valid_params():
    microad = _init_app()

    # Case of one segment
    params = {
                'app_key'       : 'app_key_id',
                'login_id'      : 'user1_id',
                'password'      : 'password1',
                'advertiser_id' : 'ad_id_1',
                'seg_id_1'      : 1
             }
    rv = microad.post('/segmentlist/remove_bulk', data=params)
    response = json.loads(rv.data)
    expected_response = {
                            'login_id'      : 'user1_id',
                            'advertiser_id' : 'ad_id_1',
                            'segment_count' : 1
                        }
    assert response == expected_response

    # Case of multi segments
    params = {
                'app_key'       : 'app_key_id',
                'login_id'      : 'user1_id',
                'password'      : 'password1',
                'advertiser_id' : 'ad_id_1',
                'seg_id_1'      : 1,
                'seg_id_2'      : 2,
             }
    rv = microad.post('/segmentlist/remove_bulk', data=params)
    response = json.loads(rv.data)
    expected_response = {
                            'login_id'      : 'user1_id',
                            'advertiser_id' : 'ad_id_1',
                            'segment_count' : 2
                        }
    assert response == expected_response


def test_remove_segment_mst_invalid_params():
    microad = _init_app()

     # case : lack of params
    params = {
                'app_key'       : 'app_key_id',
                'login_id'      : 'user1_id',
                'advertiser_id' : 'ad_id_1',
                'seg_id_1'      : 1
             }
    rv = microad.post('/segmentlist/remove_bulk', data=params)
    assert rv.status_code == 400

    # case : invalid param
    params = {
                'app_key'       : 'app_key_id',
                'login_id'      : 'user1_id',
                'password'      : 'password1',
                'advertiser_id' : 'ad_id_1',
                'hoge'          : 'fuga'
             }
    rv = microad.post('/segmentlist/remove_bulk', data=params)
    assert rv.status_code == 400


def test_remove_segment_mst_error():
    microad = _init_app()

    params = {
                'app_key'       : '401',
                'login_id'      : 'user1_id',
                'password'      : 'password1',
                'advertiser_id' : 'ad_id_1',
                'seg_id_1'      : 1,

             }
    rv = microad.post('/segmentlist/remove_bulk', data=params)
    assert rv.status_code == 401

    params['app_key'] = '500'
    rv = microad.post('/segmentlist/remove_bulk', data=params)
    assert rv.status_code == 500

    params['app_key'] = '503'
    rv = microad.post('/segmentlist/remove_bulk', data=params)
    assert rv.status_code == 503


def test_segment_list_valid_params():
    microad = _init_app()

    rv = microad.get('/sl?v=1&u=hoge&sl=a1-t1:s1,s2,s3;a2-t2:s4,s5,s6')
    assert rv.status_code == 200

    rv = microad.get('/sl?v=1&u=fuga&sl=a1-t1:s1,s3s3')
    assert rv.status_code == 200

def test_segment_list_invalid_params():
    microad = _init_app()

    rv = microad.get('/sl?v=1&u=fuga')
    assert rv.status_code == 400

    rv = microad.get('/sl?v=1&u=fuga&sl=a1:s1')
    assert rv.status_code == 400

    rv = microad.get('/sl?v=1&u=fuga&sl=-t1:s1')
    assert rv.status_code == 400

    rv = microad.get('/sl?v=1&u=fuga&sl=-t1:')
    assert rv.status_code == 400

def test_regist_recommend_mst_valid_params():
    microad = _init_app()

    # Case of one recommend
    params = {
                'app_key'       : 'app_key_id',
                'login_id'      : 'user1_id',
                'password'      : 'password1',
                'advertiser_id' : 'ad_id_1',
                'rpl_id_1'      : 1,
                'rpl_name_1'    : 'name1',
                'rpl_note_1'    : 'note1'
             }
    rv = microad.post('/recommendlist', data=params)
    response = json.loads(rv.data)
    expected_response = {
                            'login_id'      : 'user1_id',
                            'advertiser_id' : 'ad_id_1',
                            'rpl_count'     : 1
                        }
    assert response == expected_response

    # Case of multi recommend lists
    params = {
                'app_key'       : 'app_key_id',
                'login_id'      : 'user1_id',
                'password'      : 'password1',
                'advertiser_id' : 'ad_id_1',
                'rpl_id_1'      : 1,
                'rpl_name_1'    : 'name1',
                'rpl_note_1'    : 'note1',
                'rpl_id_2'      : 2,
                'rpl_name_2'    : 'name2',
                'rpl_note_2'    : 'note2'
             }
    rv = microad.post('/recommendlist', data=params)
    response = json.loads(rv.data)
    expected_response = {
                            'login_id'      : 'user1_id',
                            'advertiser_id' : 'ad_id_1',
                            'rpl_count'     : 2
                        }
    assert response == expected_response

def test_regist_recommend_mst_invalid_params():
    microad = _init_app()

    # case : lack of params
    params = {
                'app_key'       : 'app_key_id',
                'login_id'      : 'user1_id',
                'advertiser_id' : 'ad_id_1',
                'rpl_name_1'    : 'name1',
                'rpl_note_1'    : 'note1'
             }
    rv = microad.post('/recommendlist', data=params)
    assert rv.status_code == 400

    # case : lack of params2
    params = {
                'app_key'       : 'app_key_id',
                'login_id'      : 'user1_id',
                'password'      : 'password1',
                'advertiser_id' : 'ad_id_1',
                'rpl_name_1'    : 'name1',
                'rpl_note_1'    : 'note1'
             }
    rv = microad.post('/recommendlist', data=params)
    assert rv.status_code == 400


    # case : invalid param
    params = {
                'app_key'       : 'app_key_id',
                'login_id'      : 'user1_id',
                'password'      : 'password1',
                'advertiser_id' : 'ad_id_1',
                'hoge'          : 'fuga'
             }
    rv = microad.post('/recommendlist', data=params)
    assert rv.status_code == 400

def test_regist_recommend_mst_error():
    microad = _init_app()

    params = {
                'app_key'       : '401',
                'login_id'      : 'user1_id',
                'password'      : 'password1',
                'advertiser_id' : 'ad_id_1',
                'rpl_id_1'      : 1,
                'rpl_name_1'    : 'name1',
                'rpl_note_1'    : 'note1'
             }
    rv = microad.post('/recommendlist', data=params)
    assert rv.status_code == 401

    params['app_key'] = '500'
    rv = microad.post('/recommendlist', data=params)
    assert rv.status_code == 500

    params['app_key'] = '503'
    rv = microad.post('/recommendlist', data=params)
    assert rv.status_code == 503

def test_remove_recommend_mst_valid_params():
    microad = _init_app()

    # Case of one recommend
    params = {
                'app_key'       : 'app_key_id',
                'login_id'      : 'user1_id',
                'password'      : 'password1',
                'advertiser_id' : 'ad_id_1',
                'rpl_id_1'      : 1
             }
    rv = microad.post('/recommendlist/remove_bulk', data=params)
    response = json.loads(rv.data)
    expected_response = {
                            'login_id'      : 'user1_id',
                            'advertiser_id' : 'ad_id_1',
                            'rpl_count' : 1
                        }
    assert response == expected_response

    # Case of multi recommend lists
    params = {
                'app_key'       : 'app_key_id',
                'login_id'      : 'user1_id',
                'password'      : 'password1',
                'advertiser_id' : 'ad_id_1',
                'rpl_id_1'      : 1,
                'rpl_id_2'      : 2,
             }
    rv = microad.post('/recommendlist/remove_bulk', data=params)
    response = json.loads(rv.data)
    expected_response = {
                            'login_id'      : 'user1_id',
                            'advertiser_id' : 'ad_id_1',
                            'rpl_count' : 2
                        }
    assert response == expected_response

def test_remove_recommend_mst_invalid_params():
    microad = _init_app()

     # case : lack of params
    params = {
                'app_key'       : 'app_key_id',
                'login_id'      : 'user1_id',
                'advertiser_id' : 'ad_id_1',
                'rpl_id_1'      : 1
             }
    rv = microad.post('/recommendlist/remove_bulk', data=params)
    assert rv.status_code == 400

    # case : invalid param
    params = {
                'app_key'       : 'app_key_id',
                'login_id'      : 'user1_id',
                'password'      : 'password1',
                'advertiser_id' : 'ad_id_1',
                'hoge'          : 'fuga'
             }
    rv = microad.post('/recommendlist/remove_bulk', data=params)
    assert rv.status_code == 400


def test_remove_recommend_mst_error():
    microad = _init_app()

    params = {
                'app_key'       : '401',
                'login_id'      : 'user1_id',
                'password'      : 'password1',
                'advertiser_id' : 'ad_id_1',
                'rpl_id_1'      : 1,
             }
    rv = microad.post('/recommendlist/remove_bulk', data=params)
    assert rv.status_code == 401

    params['app_key'] = '500'
    rv = microad.post('/recommendlist/remove_bulk', data=params)
    assert rv.status_code == 500

    params['app_key'] = '503'
    rv = microad.post('/recommendlist/remove_bulk', data=params)
    assert rv.status_code == 503


def test_recommend_list_valid_params():
    microad = _init_app()

    rv = microad.get('/rpl?v=1&u=hoge&rpl=a1-ttl:l1-p1.p2.p3,l2-p4.p5;a2-ttl:l1-p6.p7,l2-p8.p9')
    assert rv.status_code == 200

    rv = microad.get('/rpl?v=1&u=fuga&rpl=a1-ttl:l1-p1.p2.p3.p4')
    assert rv.status_code == 200

def test_recommend_list_invalid_params():
    microad = _init_app()

    rv = microad.get('/rpl?v=1&u=fuga')
    assert rv.status_code == 400

    rv = microad.get('/rpl?v=1&u=fuga&rpl=a1-ttl:l1')
    assert rv.status_code == 400

    rv = microad.get('/rpl?v=1&u=fuga&sl=a1-ttl:l1-')
    assert rv.status_code == 400

    rv = microad.get('/rpl?v=1&u=fuga&sl=a1-ttl:l1-p1.')
    assert rv.status_code == 400

    rv = microad.get('/rpl?v=1&u=fuga&rpl=a1:l1-p1.p2.p3.p4')
    assert rv.status_code == 400
