# -*- coding: utf-8 -*-

from __future__ import absolute_import
import filemonitor

import argparse
import contextlib
import datetime
import gzip
import lib.const   as     const
import os
from   mock        import patch
import pytest
from   conf.config import BatchConfig


ENV = 'dev'


#---------------------------------------------------------------------------
#   Private functions
#---------------------------------------------------------------------------
def _create_gzip_file(dir, filename):
    file = dir.join(filename)
    with gzip.open(str(file), 'wb') as f:
        f.write('test')
    return file

def _create_text_file(dir, filename):
    file = dir.join(filename)
    with open(str(file), 'w') as f:
        f.write('test')
    return file

class MockDate(datetime.date):
    @classmethod
    def today(cls):
        return cls(2013, 10, 19)

class MockDatetime(datetime.datetime):
    @classmethod
    def now(cls):
        return cls(2013, 10, 19, 23, 40, 59, 123456)

def _mock_init_config_rtoaster(tmpdir, save_file_days=3):
    config = BatchConfig(ENV)
    config.lock_dir = str(tmpdir)
    config.tmp_transfer_dir = os.path.join(str(tmpdir), '{sid}/{pid}/{data_type}')
    config.filemonitor['rtoaster']['receive_dir']    = os.path.join(str(tmpdir), 'rtoaster{sid}/receive')
    config.filemonitor['rtoaster']['save_dir']       = os.path.join(str(tmpdir), 'rtoaster{sid}/receive_save')
    config.filemonitor['rtoaster']['save_file_days'] = save_file_days
    def _init_mock_config(self, env):
        for k, v in vars(config).items():
            setattr(self, k, v)
    return _init_mock_config

def _mock_init_config_responsys_next(tmpdir, save_file_days=3):
    config = BatchConfig(ENV)
    config.lock_dir = str(tmpdir)
    config.tmp_transfer_dir = os.path.join(str(tmpdir), '{sid}/{pid}/{data_type}')
    config.filemonitor['responsys_next']['receive_dir']    = os.path.join(str(tmpdir), 'responsys_next/request')
    config.filemonitor['responsys_next']['save_dir']       = os.path.join(str(tmpdir), 'responsys_next/request_save')
    config.filemonitor['responsys_next']['save_file_days'] = save_file_days
    def _init_mock_config(self, env):
        for k, v in vars(config).items():
            setattr(self, k, v)
    return _init_mock_config

def _mock_init_config_rtoaster_next(tmpdir, save_file_days=3):
    config = BatchConfig(ENV)
    config.lock_dir = str(tmpdir)
    config.tmp_transfer_dir = os.path.join(str(tmpdir), '{sid}/{pid}/{data_type}')
    config.filemonitor['rtoaster_next']['receive_dir']    = os.path.join(str(tmpdir), 'rtoaster0019/response')
    config.filemonitor['rtoaster_next']['save_dir']       = os.path.join(str(tmpdir), 'rtoaster0019/response_save')
    config.filemonitor['rtoaster_next']['save_file_days'] = save_file_days
    def _init_mock_config(self, env):
        for k, v in vars(config).items():
            setattr(self, k, v)
    return _init_mock_config

def _assert_exists(*paths):
    for path in paths:
        assert path.check(exists=1)

def _assert_not_exists(*paths):
    for path in paths:
        assert path.check(exists=0)


#---------------------------------------------------------------------------
#   Test for process_local_receive_files
#---------------------------------------------------------------------------
@contextlib.contextmanager
def _patches_for_process_local_receive_files(tmpdir, save_file_days=3, func_init_config=_mock_init_config_rtoaster):
    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=func_init_config(tmpdir, save_file_days)),
        patch('lib.s3util.S3Util.upload_single_file'),
        patch('lib.workflow.starter.start_transfer'),
        patch('datetime.datetime', MockDatetime)
    ) as (mock_config, mock_s3put, mock_startwf, mock_now):
        mock_startwf.return_value = 'DummyRunId'
        yield (mock_s3put, mock_startwf)

def test_process_local_one_file(tmpdir):

    # prepare
    root_dir = tmpdir.mkdir('rtoaster9999')
    rcv_dir  = root_dir.mkdir('receive')
    rcv_file = _create_gzip_file(rcv_dir, 'item_mst_20131231000000_9999.csv.gz')

    # execute
    with _patches_for_process_local_receive_files(tmpdir) as (mock_s3put, mock_startwf):
        filemonitor.process_local_receive_files('rtoaster', ENV)

    # assert
    save_dir  = root_dir.join('receive_save')
    save_file = save_dir.join('item_mst_20131231000000_9999.csv.gz')

    _assert_exists(save_dir, save_file)
    _assert_not_exists(rcv_file)

    assert mock_s3put.call_count == 1
    assert mock_s3put.call_args_list[0][0] == (str(rcv_file), 'dev-transfer-data', u'9999/item_mst/raw/')

    assert mock_startwf.call_count == 1
    assert mock_startwf.call_args_list[0][0] == ('9999', 'dev', 'itemmst', 'dev-transfer-data', '9999/item_mst/raw/item_mst_20131231000000_9999.csv.gz')

def test_process_local_many_files(tmpdir):

    # prepare
    root_dir  = tmpdir.mkdir('rtoaster9999')
    rcv_dir   = root_dir.mkdir('receive')
    rcv_file1 = _create_gzip_file(rcv_dir, 'item_mst_20130101000000_9999.csv.gz')
    rcv_file2 = _create_gzip_file(rcv_dir, 'item_mst_20131231000000_9999.csv.gz')

    # execute
    with _patches_for_process_local_receive_files(tmpdir) as (mock_s3put, mock_startwf):
        filemonitor.process_local_receive_files('rtoaster', ENV)

    # assert
    save_dir   = root_dir.join('receive_save')
    save_file1 = save_dir.join('item_mst_20130101000000_9999.csv.gz')
    save_file2 = save_dir.join('item_mst_20131231000000_9999.csv.gz')

    _assert_exists(save_dir, save_file1, save_file2)
    _assert_not_exists(rcv_file1, rcv_file2)

    assert mock_s3put.call_count == 2
    assert mock_s3put.call_args_list[0][0] == (str(rcv_file1), 'dev-transfer-data', u'9999/item_mst/raw/')
    assert mock_s3put.call_args_list[1][0] == (str(rcv_file2), 'dev-transfer-data', u'9999/item_mst/raw/')

    assert mock_startwf.call_count == 2
    assert mock_startwf.call_args_list[0][0] == ('9999', 'dev', 'itemmst', 'dev-transfer-data', '9999/item_mst/raw/item_mst_20130101000000_9999.csv.gz')
    assert mock_startwf.call_args_list[1][0] == ('9999', 'dev', 'itemmst', 'dev-transfer-data', '9999/item_mst/raw/item_mst_20131231000000_9999.csv.gz')

def test_process_local_many_users(tmpdir):

    # prepare
    root_dir1 = tmpdir.mkdir('rtoaster9998')
    rcv_dir1  = root_dir1.mkdir('receive')
    rcv_file1 = _create_gzip_file(rcv_dir1, 'item_mst_20130101000000_9998.csv.gz')

    root_dir2 = tmpdir.mkdir('rtoaster9999')
    rcv_dir2  = root_dir2.mkdir('receive')
    rcv_file2 = _create_gzip_file(rcv_dir2, 'item_mst_20131231000000_9999.csv.gz')

    # execute
    with _patches_for_process_local_receive_files(tmpdir) as (mock_s3put, mock_startwf):
        filemonitor.process_local_receive_files('rtoaster', ENV)

    # assert
    save_dir1  = root_dir1.join('receive_save')
    save_file1 = save_dir1.join('item_mst_20130101000000_9998.csv.gz')
    save_dir2  = root_dir2.join('receive_save')
    save_file2 = save_dir2.join('item_mst_20131231000000_9999.csv.gz')

    _assert_exists(save_dir1, save_file1, save_dir2, save_file2)
    _assert_not_exists(rcv_file1, rcv_file2)

    assert mock_s3put.call_count == 2
    assert mock_s3put.call_args_list[0][0] == (str(rcv_file1), 'dev-transfer-data', u'9998/item_mst/raw/')
    assert mock_s3put.call_args_list[1][0] == (str(rcv_file2), 'dev-transfer-data', u'9999/item_mst/raw/')

    assert mock_startwf.call_count == 2
    assert mock_startwf.call_args_list[0][0] == ('9998', 'dev', 'itemmst', 'dev-transfer-data', '9998/item_mst/raw/item_mst_20130101000000_9998.csv.gz')
    assert mock_startwf.call_args_list[1][0] == ('9999', 'dev', 'itemmst', 'dev-transfer-data', '9999/item_mst/raw/item_mst_20131231000000_9999.csv.gz')

def test_process_local_no_file(tmpdir):

    # execute
    with _patches_for_process_local_receive_files(tmpdir) as (mock_s3put, mock_startwf):
        filemonitor.process_local_receive_files('rtoaster', ENV)

    # asssert
    assert mock_s3put.call_count   == 0
    assert mock_startwf.call_count == 0

def test_process_local_no_save(tmpdir):

    # prepare
    root_dir = tmpdir.mkdir('rtoaster9999')
    rcv_dir  = root_dir.mkdir('receive')
    rcv_file = _create_gzip_file(rcv_dir, 'item_mst_20131231000000_9999.csv.gz')

    # execute
    with _patches_for_process_local_receive_files(tmpdir, -1) as (mock_s3put, mock_startwf):
        filemonitor.process_local_receive_files('rtoaster', ENV)

    # assert
    save_dir  = root_dir.join('receive_save')
    save_file = save_dir.join('item_mst_20131231000000_9999.csv.gz')

    _assert_not_exists(rcv_file, save_dir, save_file)

    assert mock_s3put.call_count == 1
    assert mock_s3put.call_args_list[0][0] == (str(rcv_file), 'dev-transfer-data', u'9999/item_mst/raw/')

    assert mock_startwf.call_count == 1
    assert mock_startwf.call_args_list[0][0] == ('9999', 'dev', 'itemmst', 'dev-transfer-data', '9999/item_mst/raw/item_mst_20131231000000_9999.csv.gz')

def test_process_local_s3put_failed(tmpdir):

    # prepare
    root_dir = tmpdir.mkdir('rtoaster9999')
    rcv_dir  = root_dir.mkdir('receive')
    rcv_file = _create_gzip_file(rcv_dir, 'item_mst_20131231000000_9999.csv.gz')

    # execute
    with _patches_for_process_local_receive_files(tmpdir, -1) as (mock_s3put, mock_startwf):
        mock_s3put.side_effect = Exception('Failed to put file to s3')
        with pytest.raises(Exception):
            filemonitor.process_local_receive_files('rtoaster', ENV)

    # assert
    save_dir  = root_dir.join('receive_save')
    save_file = save_dir.join('item_mst_20131231000000_9999.csv.gz')

    _assert_exists(rcv_file)
    _assert_not_exists(save_dir, save_file)

    assert mock_s3put.call_count == 1
    assert mock_s3put.call_args_list[0][0] == (str(rcv_file), 'dev-transfer-data', u'9999/item_mst/raw/')

    assert mock_startwf.call_count == 0

def test_process_local_startwf_failed(tmpdir):

    # prepare
    root_dir  = tmpdir.mkdir('rtoaster9999')
    rcv_dir   = root_dir.mkdir('receive')
    rcv_file1 = _create_gzip_file(rcv_dir, 'item_mst_20130101000000_9999.csv.gz')
    rcv_file2 = _create_gzip_file(rcv_dir, 'item_mst_20131231000000_9999.csv.gz')

    # execute
    with _patches_for_process_local_receive_files(tmpdir) as (mock_s3put, mock_startwf):
        mock_startwf.side_effect = ['DummyRunId', Exception('Failed to start workflow')]
        filemonitor.process_local_receive_files('rtoaster', ENV)

    # assert
    save_dir   = root_dir.join('receive_save')
    save_file1 = save_dir.join('item_mst_20130101000000_9999.csv.gz')
    save_file2 = save_dir.join('item_mst_20131231000000_9999.csv.gz')

    _assert_exists(save_dir, save_file1, save_file2)
    _assert_not_exists(rcv_file1, rcv_file2)

    assert mock_s3put.call_count == 2
    assert mock_s3put.call_args_list[0][0] == (str(rcv_file1), 'dev-transfer-data', u'9999/item_mst/raw/')
    assert mock_s3put.call_args_list[1][0] == (str(rcv_file2), 'dev-transfer-data', u'9999/item_mst/raw/')

    assert mock_startwf.call_count == 2
    assert mock_startwf.call_args_list[0][0] == ('9999', 'dev', 'itemmst', 'dev-transfer-data', '9999/item_mst/raw/item_mst_20130101000000_9999.csv.gz')
    assert mock_startwf.call_args_list[1][0] == ('9999', 'dev', 'itemmst', 'dev-transfer-data', '9999/item_mst/raw/item_mst_20131231000000_9999.csv.gz')

def test_process_local_all_types(tmpdir):

    # prepare
    root_dir   = tmpdir.mkdir('rtoaster9999')
    rcv_dir    = root_dir.mkdir('receive')
    rcv_file1  = _create_gzip_file(rcv_dir, 'conversion_20130101000000_9999.json.gz')
    rcv_file2  = _create_gzip_file(rcv_dir, 'item_mst_20130101000000_9999.csv.gz')
    rcv_file3  = _create_gzip_file(rcv_dir, 'recommend_list_data_20130101000000_9999.json.gz')
    rcv_file4  = _create_gzip_file(rcv_dir, 'recommend_list_mst_20130101000000_9999.csv.gz')
    rcv_file5  = _create_gzip_file(rcv_dir, 'segment_list_data_20130101000000_9999.json.gz')
    rcv_file6  = _create_gzip_file(rcv_dir, 'segment_list_mst_20130101000000_9999.csv.gz')
    rcv_file7  = _create_gzip_file(rcv_dir, 'zzzz-20130101-000000-9999-item.json.gz')
    rcv_file8  = _create_gzip_file(rcv_dir, 'zzzz-20130102-000000-9999-recommend.json.gz')
    rcv_file9 = _create_gzip_file(rcv_dir, 'mail_list_data_20130101000000_9999.json.gz')

    # execute
    with _patches_for_process_local_receive_files(tmpdir) as (mock_s3put, mock_startwf):
        filemonitor.process_local_receive_files('rtoaster', ENV)

    # assert
    save_dir    = root_dir.join('receive_save')
    save_file1  = save_dir.join('conversion_20130101000000_9999.json.gz')
    save_file2  = save_dir.join('item_mst_20130101000000_9999.csv.gz')
    save_file3  = save_dir.join('recommend_list_data_20130101000000_9999.json.gz')
    save_file4  = save_dir.join('recommend_list_mst_20130101000000_9999.csv.gz')
    save_file5  = save_dir.join('segment_list_data_20130101000000_9999.json.gz')
    save_file6  = save_dir.join('segment_list_mst_20130101000000_9999.csv.gz')
    save_file7  = save_dir.join('zzzz-20130101-000000-9999-item.json.gz')
    save_file8  = save_dir.join('zzzz-20130102-000000-9999-recommend.json.gz')
    save_file9 = save_dir.join('mail_list_data_20130101000000_9999.json.gz')

    _assert_exists(save_dir,   save_file1, save_file2, save_file3, save_file4,
                   save_file5, save_file6, save_file7, save_file8, save_file9)
    _assert_not_exists(rcv_file1, rcv_file2, rcv_file3, rcv_file4, rcv_file5,
                       rcv_file6, rcv_file7, rcv_file8, rcv_file9)

    assert mock_s3put.call_count == 9
    mock_s3put.assert_any_call(str(rcv_file1),  'dev-transfer-data', u'9999/conversion_log/raw/')
    mock_s3put.assert_any_call(str(rcv_file2),  'dev-transfer-data', u'9999/item_mst/raw/')
    mock_s3put.assert_any_call(str(rcv_file3),  'dev-transfer-data', u'9999/recommend_list_data/raw/')
    mock_s3put.assert_any_call(str(rcv_file4),  'dev-transfer-data', u'9999/recommend_list_mst/raw/')
    mock_s3put.assert_any_call(str(rcv_file5),  'dev-transfer-data', u'9999/segment_list_data/raw/')
    mock_s3put.assert_any_call(str(rcv_file6),  'dev-transfer-data', u'9999/segment_list_mst/raw/')
    mock_s3put.assert_any_call(str(rcv_file7),  'dev-transfer-data', u'9999/item_mst/raw/')
    mock_s3put.assert_any_call(str(rcv_file8),  'dev-transfer-data', u'9999/recommend_list_data/raw/')
    mock_s3put.assert_any_call(str(rcv_file9), 'dev-transfer-data', u'9999/mail_list_data/raw/')

    assert mock_startwf.call_count == 9
    mock_startwf.assert_any_call('9999', 'dev', 'conversion',      'dev-transfer-data', '9999/conversion_log/raw/conversion_20130101000000_9999.json.gz')
    mock_startwf.assert_any_call('9999', 'dev', 'itemmst',         'dev-transfer-data', '9999/item_mst/raw/item_mst_20130101000000_9999.csv.gz')
    mock_startwf.assert_any_call('9999', 'dev', 'recommenddata',   'dev-transfer-data', '9999/recommend_list_data/raw/recommend_list_data_20130101000000_9999.json.gz')
    mock_startwf.assert_any_call('9999', 'dev', 'recommendmst',    'dev-transfer-data', '9999/recommend_list_mst/raw/recommend_list_mst_20130101000000_9999.csv.gz')
    mock_startwf.assert_any_call('9999', 'dev', 'segmentdata',     'dev-transfer-data', '9999/segment_list_data/raw/segment_list_data_20130101000000_9999.json.gz')
    mock_startwf.assert_any_call('9999', 'dev', 'segmentmst',      'dev-transfer-data', '9999/segment_list_mst/raw/segment_list_mst_20130101000000_9999.csv.gz')
    mock_startwf.assert_any_call('9999', 'dev', 'itemmst',         'dev-transfer-data', '9999/item_mst/raw/zzzz-20130101-000000-9999-item.json.gz')
    mock_startwf.assert_any_call('9999', 'dev', 'recommenddata',   'dev-transfer-data', '9999/recommend_list_data/raw/zzzz-20130102-000000-9999-recommend.json.gz')
    mock_startwf.assert_any_call('9999', 'dev', 'mailsegmentdata', 'dev-transfer-data', '9999/mail_list_data/raw/mail_list_data_20130101000000_9999.json.gz')

def test_process_local_responsys_next(tmpdir):

    # prepare
    root_dir  = tmpdir.mkdir('responsys_next')
    req_dir   = root_dir.mkdir('request')
    req_file  = _create_text_file(req_dir, '20140304_RecommendPattern.csv')

    # execute
    with _patches_for_process_local_receive_files(tmpdir, func_init_config=_mock_init_config_responsys_next) as (mock_s3put, mock_startwf):
        filemonitor.process_local_receive_files('responsys_next', ENV)

    # assert
    save_dir   = root_dir.join('request_save')
    save_file  = save_dir.join('20140304_RecommendPattern.csv')

    _assert_exists(save_dir, save_file)
    _assert_not_exists(req_file)

    assert mock_s3put.call_count == 1
    assert mock_s3put.call_args_list[0][0] == (str(req_file), 'dev-transfer-data', u'0019/mail_request_data/raw/')

    assert mock_startwf.call_count == 1
    assert mock_startwf.call_args_list[0][0] == ('0019', 'dev', 'mailrequest', 'dev-transfer-data', '0019/mail_request_data/raw/20140304_RecommendPattern.csv')

def test_process_local_rtoaster_next(tmpdir):

    # prepare
    root_dir  = tmpdir.mkdir('rtoaster0019')
    res_dir   = root_dir.mkdir('response')
    res_file1 = _create_text_file(res_dir, 'RecommendPattern_20140304_20140305150023.csv.gz')
    res_file2 = _create_text_file(res_dir, 'RecommendPattern_20140304_20140305150024.csv.gz.err')

    # execute
    with _patches_for_process_local_receive_files(tmpdir, func_init_config=_mock_init_config_rtoaster_next) as (mock_s3put, mock_startwf):
        filemonitor.process_local_receive_files('rtoaster_next', ENV)

    # assert
    save_dir   = root_dir.join('response_save')
    save_file1 = save_dir.join('RecommendPattern_20140304_20140305150023.csv.gz')
    save_file2 = save_dir.join('RecommendPattern_20140304_20140305150024.csv.gz.err')

    _assert_exists(save_dir, save_file1, save_file2)
    _assert_not_exists(res_file1, res_file2)

    assert mock_s3put.call_count == 2
    assert mock_s3put.call_args_list[0][0] == (str(res_file1), 'dev-transfer-data', u'0019/mail_response_data/raw/')
    assert mock_s3put.call_args_list[1][0] == (str(res_file2), 'dev-transfer-data', u'0019/mail_response_data/raw/')

    assert mock_startwf.call_count == 2
    assert mock_startwf.call_args_list[0][0] == ('0019', 'dev', 'mailresponse', 'dev-transfer-data', '0019/mail_response_data/raw/RecommendPattern_20140304_20140305150023.csv.gz')
    assert mock_startwf.call_args_list[1][0] == ('0019', 'dev', 'mailresponse', 'dev-transfer-data', '0019/mail_response_data/raw/RecommendPattern_20140304_20140305150024.csv.gz.err')

def test_process_local_receive_same_file_again(tmpdir):

    # prepare
    root_dir   = tmpdir.mkdir('rtoaster9999')
    rcv_dir    = root_dir.mkdir('receive')
    rcv_file   = _create_gzip_file(rcv_dir, 'item_mst_20131231000000_9999.csv.gz')

    save_dir   = root_dir.mkdir('receive_save')
    save_file1 = _create_gzip_file(save_dir, 'item_mst_20131231000000_9999.csv.gz')
    save_file2 = save_dir.join('item_mst_20131231000000_9999.csv.gz.20131019234059')

    # execute
    with _patches_for_process_local_receive_files(tmpdir) as (mock_s3put, mock_startwf):
        filemonitor.process_local_receive_files('rtoaster', ENV)

    # assert
    _assert_exists(save_dir, save_file1, save_file2)
    _assert_not_exists(rcv_file)

    assert mock_s3put.call_count == 1
    assert mock_s3put.call_args_list[0][0] == (str(rcv_file), 'dev-transfer-data', u'9999/item_mst/raw/')

    assert mock_startwf.call_count == 1
    assert mock_startwf.call_args_list[0][0] == ('9999', 'dev', 'itemmst', 'dev-transfer-data', '9999/item_mst/raw/item_mst_20131231000000_9999.csv.gz')


#---------------------------------------------------------------------------
#   Test for remove_local_save_files
#---------------------------------------------------------------------------
@contextlib.contextmanager
def _patches_for_remove_local_save_files(tmpdir, save_file_days=3):
    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=_mock_init_config_rtoaster(tmpdir, save_file_days)),
        patch('datetime.date', MockDate)
    ) as (mock_config, mock_today):
        yield (mock_today,)

def test_remove_local_files(tmpdir):

    # prepare
    root_dir   = tmpdir.mkdir('rtoaster9999')
    save_dir   = root_dir.mkdir('receive_save')
    save_file1 = _create_gzip_file(save_dir, 'item_mst_20131018000000_9999.csv.gz')
    save_file2 = _create_gzip_file(save_dir, 'item_mst_20131017000000_9999.csv.gz')
    save_file3 = _create_gzip_file(save_dir, 'item_mst_20131016000000_9999.csv.gz')
    save_file4 = _create_gzip_file(save_dir, 'item_mst_20131015000000_9999.csv.gz')

    # execute
    with _patches_for_remove_local_save_files(tmpdir):
        filemonitor.remove_local_save_files('rtoaster', ENV)

    #assert
    _assert_exists(save_file1, save_file2, save_file3)
    _assert_not_exists(save_file4)

def test_remove_local_many_users(tmpdir):

    # prepare
    root_dir1   = tmpdir.mkdir('rtoaster998')
    save_dir1   = root_dir1.mkdir('receive_save')
    save_file1 = _create_gzip_file(save_dir1, 'item_mst_20131010000000_9998.csv.gz')

    root_dir2   = tmpdir.mkdir('rtoaster9999')
    save_dir2   = root_dir2.mkdir('receive_save')
    save_file2 = _create_gzip_file(save_dir2, 'item_mst_20131010000000_9999.csv.gz')

    # execute
    with _patches_for_remove_local_save_files(tmpdir):
        filemonitor.remove_local_save_files('rtoaster', ENV)

    #assert
    _assert_not_exists(save_file1, save_file2)

def test_remove_local_no_file(tmpdir):

    # execute
    with _patches_for_remove_local_save_files(tmpdir):
        filemonitor.remove_local_save_files('rtoaster', ENV)

def test_remove_local_files_all_types(tmpdir):

    # prepare
    root_dir    = tmpdir.mkdir('rtoaster0009')
    save_dir    = root_dir.mkdir('receive_save')
    save_file1  = _create_gzip_file(save_dir, 'conversion_20131015000000_9999.json.gz')
    save_file2  = _create_gzip_file(save_dir, 'item_mst_20131015000000_9999.csv.gz')
    save_file3  = _create_gzip_file(save_dir, 'recommend_list_data_20131015000000_9999.json.gz')
    save_file4  = _create_gzip_file(save_dir, 'recommend_list_mst_20131015000000_9999.csv.gz')
    save_file5  = _create_gzip_file(save_dir, 'segment_list_data_20131015000000_9999.json.gz')
    save_file6  = _create_gzip_file(save_dir, 'segment_list_mst_20131015000000_9999.csv.gz')
    save_file7  = _create_gzip_file(save_dir, 'zzzz-20131015-000000-9999-item.json.gz')
    save_file8  = _create_gzip_file(save_dir, 'zzzz-20131015-000000-9999-recommend.json.gz')
    save_file9 = _create_gzip_file(save_dir, 'mail_list_data_20131015000000_9999.json.gz')

    # execute
    with _patches_for_remove_local_save_files(tmpdir):
        filemonitor.remove_local_save_files('rtoaster', ENV)

    #assert
    _assert_not_exists(save_file1, save_file2, save_file3, save_file4,
                       save_file5, save_file6, save_file7, save_file8,
                       save_file9)


#---------------------------------------------------------------------------
#   Test for process_remote_receive_files
#---------------------------------------------------------------------------
@contextlib.contextmanager
def _patches_for_process_scaleads_rawdata_files(tmpdir, save_file_days=3):
    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=_mock_init_config_rtoaster(tmpdir, save_file_days)),
        patch('lib.sftputil.SftpUtil.__enter__'),
        patch('lib.s3util.S3Util.upload_single_file'),
        patch('lib.workflow.starter.start_rawdata_process'),
        patch('datetime.datetime')
    ) as (mock_config, mock_sftp, mock_s3put, mock_startwf, mock_datetime):
        mock_startwf.return_value = 'DummyRunId'
        yield (mock_sftp, mock_s3put, mock_datetime, mock_startwf)

def test_process_rawdata_normal(tmpdir):

    # prepare
    data_dir   = tmpdir.mkdir('dummy').mkdir('scaleads').mkdir('rawdata')
    data_file  = _create_text_file(data_dir, 'brainpad-20140516.tgz')

    expected_remote_dir = '/receive/scaleads/brainpad-20140516.tgz'
    expected_local_dir  = str(data_dir)
    expected_s3_bucket  = 'dev-transfer-data'
    expected_s3dir      = 'rawdata/raw/'

    # execute
    datetime_obj = datetime.datetime.strptime('2014-05-21 00:00:00', '%Y-%m-%d %H:%M:%S')
    with _patches_for_process_scaleads_rawdata_files(tmpdir) as (mock_sftp, mock_s3put, mock_datetime, mock_startwf):
        mock_sftp.return_value.exists.return_value = True
        mock_datetime.now.return_value = datetime_obj
        filemonitor.process_scaleads_rawdata_file('dummy', ENV)

    # assert
    _assert_not_exists(data_file)

    assert mock_sftp.return_value.exists.call_count == 1
    assert mock_sftp.return_value.exists.call_args_list[0][0] == (expected_remote_dir,)

    assert mock_sftp.return_value.get.call_count == 1
    assert mock_sftp.return_value.get.call_args_list[0][0] == (expected_remote_dir, expected_local_dir)

    assert mock_s3put.call_count == 1
    assert mock_s3put.call_args_list[0][0] == (str(data_file), expected_s3_bucket, expected_s3dir)

    assert mock_startwf.call_count == 1
    assert mock_startwf.call_args_list[0][0] == ('dev', 'rawdata', 'dev-transfer-data', 'rawdata/raw/brainpad-20140516.tgz')

def test_process_rawdata_file_does_not_exist(tmpdir):

    # prepare
    data_dir  = tmpdir.mkdir('dummy').mkdir('scaleads').mkdir('rawdata')
    data_file = _create_text_file(data_dir, 'brainpad-20140516.tgz')

    expected_remote_dir = '/receive/scaleads/brainpad-20140516.tgz'
    expected_local_dir  = str(data_dir)
    expected_s3_bucket  = 'dev-transfer-data'
    expected_s3dir      = 'rawdata/raw/'

    # execute
    datetime_obj = datetime.datetime.strptime('2014-05-21 00:00:00', '%Y-%m-%d %H:%M:%S')
    with _patches_for_process_scaleads_rawdata_files(tmpdir) as (mock_sftp, mock_s3put, mock_datetime, mock_startwf):
        mock_sftp.return_value.exists.return_value = False
        mock_datetime.now.return_value = datetime_obj

        with pytest.raises(Exception):
            filemonitor.process_scaleads_rawdata_file('dummy', ENV)

    # assert
    _assert_not_exists(data_file)

    assert mock_sftp.return_value.exists.call_count == 1
    assert mock_sftp.return_value.exists.call_args_list[0][0] == (expected_remote_dir,)

    assert mock_sftp.return_value.get.call_count == 0
    assert mock_s3put.call_count == 0

    assert mock_startwf.call_count == 0

def test_process_rawdata_s3put_error(tmpdir):

    # prepare
    data_dir   = tmpdir.mkdir('dummy').mkdir('scaleads').mkdir('rawdata')
    data_file  = _create_text_file(data_dir, 'brainpad-20140516.tgz')

    expected_remote_dir = '/receive/scaleads/brainpad-20140516.tgz'
    expected_local_dir  = str(data_dir)
    expected_s3_bucket  = 'dev-transfer-data'
    expected_s3dir      = 'rawdata/raw/'

    # execute
    datetime_obj = datetime.datetime.strptime('2014-05-21 00:00:00', '%Y-%m-%d %H:%M:%S')
    with _patches_for_process_scaleads_rawdata_files(tmpdir) as (mock_sftp, mock_s3put, mock_datetime, mock_startwf):
        mock_sftp.return_value.exists.return_value = True
        mock_s3put.side_effect = Exception('Failed to put file to s3')
        mock_datetime.now.return_value = datetime_obj

        with pytest.raises(Exception):
            filemonitor.process_scaleads_rawdata_file('dummy', ENV)

    # assert
    _assert_not_exists(data_file)

    assert mock_sftp.return_value.exists.call_count == 1
    assert mock_sftp.return_value.exists.call_args_list[0][0] == (expected_remote_dir,)

    assert mock_sftp.return_value.get.call_count == 1
    assert mock_sftp.return_value.get.call_args_list[0][0] == (expected_remote_dir, expected_local_dir)

    assert mock_startwf.call_count == 0


#---------------------------------------------------------------------------
#   Test for command line functions
#---------------------------------------------------------------------------
def test_get_function_args_from_parse_args(func_init_config):
    # Create command
    parser = argparse.ArgumentParser()

    # Create subcommands
    subparsers = parser.add_subparsers(help='subcommands')

    # Create "process_local_receive_files" command
    parser_process = subparsers.add_parser('process', help='process local receive files')
    parser_process.add_argument('agency_id', type=str, help='agency_id')
    parser_process.add_argument('env', type=str, choices=const.ENVS, help='environment')
    parser_process.set_defaults(func=filemonitor.process_local_receive_files)

    # Create "remove_local_save_files" command
    parser_remove = subparsers.add_parser('remove', help='remove save files')
    parser_remove.add_argument('agency_id', type=str, help='agency_id')
    parser_remove.add_argument('env', type=str, choices=const.ENVS, help='environment')
    parser_remove.set_defaults(func=filemonitor.remove_local_save_files)

    # Create "process_scaleads_receive_rawdata_file" command
    parser_scaleads = subparsers.add_parser('scaleads_rawdata', help='process scaleads receive rawdata file')
    parser_scaleads.add_argument('agency_id', default='dummy', help='agency_id')
    parser_scaleads.add_argument('env', type=str, choices=const.ENVS, help='environment')
    parser_scaleads.add_argument('-t', '--target_datetime_str', type=str, help='target datetime(format: yyyymmdd)')
    parser_scaleads.set_defaults(func=filemonitor.process_scaleads_rawdata_file)

    args = parser.parse_args(['process', 'rtoaster', 'dev'])
    assert filemonitor._get_function_args_from_parse_args(args) == {'agency_id':'rtoaster', 'env':'dev'}

    args = parser.parse_args(['remove', 'rtoaster', 'dev'])
    assert filemonitor._get_function_args_from_parse_args(args) == {'agency_id':'rtoaster', 'env':'dev'}

    args = parser.parse_args(['scaleads_rawdata', 'dummy', 'dev'])
    assert filemonitor._get_function_args_from_parse_args(args) == {'agency_id':'dummy', 'env':'dev', 'target_datetime_str':None}

    args = parser.parse_args(['scaleads_rawdata', 'dummy', 'dev', '--target_datetime_str', '20140515'])
    assert filemonitor._get_function_args_from_parse_args(args) == {'agency_id':'dummy', 'env':'dev', 'target_datetime_str':'20140515'}
