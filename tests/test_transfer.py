# -*- coding: utf-8 -*-

from __future__ import absolute_import
import transfer

import contextlib
from   mock       import patch
import pytest


ENV       = 'dev'
SID       = '9999'
PID       = 'ma'
DATA_TYPE = 'segmentmst'
S3BUCKET  = 's3bucket'
S3PATH    = 's3path'

def test_start_transfer(lock_dir, func_init_config):
    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('lib.transferer.ma.segmentmst.SegmentMstTransferer.execute'),
        patch('lib.transferer.transfer_reporter.TransferReporter.send_report'),
    ) as (m_config, m_transfer, m_report):
        transfer.start_transfer(SID, PID, ENV, DATA_TYPE)

    assert m_transfer.call_count == 1
    assert m_report.call_count   == 1

def test_start_transfer_with_s3file_args(lock_dir, func_init_config):
    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('lib.transferer.ma.segmentmst.SegmentMstTransferer.execute'),
        patch('lib.transferer.transfer_reporter.TransferReporter.send_report'),
    ) as (m_config, m_transfer, m_report):
        transfer.start_transfer(SID, PID, ENV, DATA_TYPE, S3BUCKET, S3PATH)

    assert m_transfer.call_count == 1
    assert m_report.call_count   == 1

def test_start_transfer_module_notexists(lock_dir, func_init_config):
    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('lib.transferer.ma.segmentmst.SegmentMstTransferer.execute'),
        patch('lib.transferer.transfer_reporter.TransferReporter.send_report'),
        pytest.raises(ImportError)
    ) as (m_config, m_transfer, m_report, error):
        transfer.start_transfer(SID, 'pid_notexists', ENV, DATA_TYPE)

    assert m_transfer.call_count == 0
    assert m_report.call_count   == 0

def test_start_transfer_class_notexists(lock_dir, func_init_config):
    settings = {
        'segmentmst': {
            'class_name'  : 'NotExistsTransferer',
            's3_path'     : '{sid}/segment_list_mst/raw',
            'send_report' : True
        }
    }
    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch.dict('transfer.SETTINGS', settings),
        patch('lib.transferer.ma.segmentmst.SegmentMstTransferer.execute'),
        patch('lib.transferer.transfer_reporter.TransferReporter.send_report'),
        pytest.raises(AttributeError)
    ) as (m_config, m_settings, m_transfer, m_report, error):
        transfer.start_transfer(SID, PID, ENV, DATA_TYPE)

    assert m_transfer.call_count == 0
    assert m_report.call_count   == 0

def test_start_transfer_gdo_segment(lock_dir, func_init_config):
    def _func_init_transfer(self, sid, pid, env, s3bucket, s3path, **option):
        return None

    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('lib.transferer.so.segmentdata.SegmentDataTransferer.__init__', new=_func_init_transfer),
        patch('lib.transferer.so.segmentdata.SegmentDataTransferer.execute'),
        patch('lib.transferer.transfer_reporter.TransferReporter.send_report'),
    ) as (m_config, m_transfer_init, m_transfer_execute, m_report):
        transfer.start_transfer('0009', 'so', ENV, 'segmentdata')

    assert m_transfer_execute.call_count == 1
    assert m_report.call_count           == 1

def test_start_transfer_gdo_recommend(lock_dir, func_init_config):
    def _func_init_transfer(self, sid, pid, env, s3bucket, s3path, **option):
        return None

    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('lib.transferer.so_gdo.recommenddata.RecommendDataTransferer.__init__', new=_func_init_transfer),
        patch('lib.transferer.so_gdo.recommenddata.RecommendDataTransferer.execute'),
        patch('lib.transferer.transfer_reporter.TransferReporter.send_report'),
    ) as (m_config, m_transfer_init, m_transfer_execute, m_report):
        transfer.start_transfer('0009', 'so', ENV, 'recommenddata')

    assert m_transfer_execute.call_count == 1
    assert m_report.call_count           == 1

def test_start_transfer_rt_bridge_segment(lock_dir, func_init_config):
    def _func_init_transfer(self, sid, pid, env, s3bucket, s3path, **option):
        return None

    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('lib.transferer.rt.segmentdata.bridge.SegmentDataTransferer.__init__', new=_func_init_transfer),
        patch('lib.transferer.rt.segmentdata.bridge.SegmentDataTransferer.execute'),
        patch('lib.transferer.transfer_reporter.TransferReporter.send_report'),
    ) as (m_config, m_transfer_init, m_transfer_execute, m_report):
        transfer.start_transfer(SID, 'rt9998', ENV, 'segmentdata')

    assert m_transfer_execute.call_count == 1
    assert m_report.call_count           == 1
