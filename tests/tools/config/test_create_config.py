# -*- coding: utf-8 -*-

from __future__ import absolute_import

__author__  = 'CrossOven開発チーム <coven-dev@brainpad.co.jp>'
__status__  = 'development'
__version__ = ''
__date__    = ''

import os
from   tools.config.create_config import CreateConfig

ENV = 'dev'
SID = '9999'

def test_get_query_9999():
    create_config_obj = CreateConfig(ENV, SID)

    with open('tests/tools/config/sql_files/DB_TAMAR_9999.sql') as f:
        expected_query = f.read()
    query = create_config_obj._get_query()

    assert query == expected_query

def test_get_query_with_9998():
    create_config_obj = CreateConfig(ENV, '9998')

    with open('tests/tools/config/sql_files/DB_TAMAR_9998.sql') as f:
        expected_query = f.read()
    query = create_config_obj._get_query()

    assert query == expected_query

def test_get_pids():
    create_config_obj = CreateConfig(ENV, SID)

    assert set(create_config_obj._get_pids()) == set(['bp1', 'ac', 'ao', 'co', 'go', 'im', 'ma', 'rs', 'rt', 'so', 'sy', 'ydmp'])

def test_get_unmapping_pids():
    create_config_obj = CreateConfig(ENV, SID)

    assert create_config_obj._get_unmapping_pids() == ['deltacube', 'rt', 'sy', 'ydmp']

def test_get_bridge_sids():
    create_config_obj = CreateConfig(ENV, SID)

    assert create_config_obj._get_bridge_sids() == ['rt9998']

def test_get_data_sources(func_init_config):
    create_config_obj = CreateConfig(ENV, SID)

    assert create_config_obj._get_data_sources() == ['bp1', 'deltacube', 'im']

def test_get_str_sql_mapping():
    create_config_obj = CreateConfig(ENV, SID)

    mapping_sql_file_path = 'tests/tools/config/sql_files/mapping_so.sql'
    with open(mapping_sql_file_path) as f:
        expected_query = f.read()

    assert create_config_obj._get_str_sql_mapping('so') == expected_query

def test_get_str_sql_mapping_co():
    create_config_obj = CreateConfig(ENV, SID)

    mapping_sql_file_path = 'tests/tools/config/sql_files/mapping_co.sql'
    with open(mapping_sql_file_path) as f:
        expected_query = f.read()

    assert create_config_obj._get_str_sql_mapping_co() == expected_query

def test_get_str_sql_mapping_with_segmentmst():
    create_config_obj = CreateConfig(ENV, SID)

    mapping_im_sql_file_path = 'tests/tools/config/sql_files/mapping_im.sql'
    with open(mapping_im_sql_file_path) as f:
        expected_query = f.read()

    assert create_config_obj._get_str_sql_mapping_with_segmentmst('im') == expected_query

def test_get_str_sql_bridge():
    create_config_obj = CreateConfig(ENV, SID)

    bridge_sql_file_path = 'tests/tools/config/sql_files/bridge_to_rt9998.sql'
    with open(bridge_sql_file_path) as f:
        expected_query = f.read()

    assert create_config_obj._get_str_sql_bridge('rt9998') == expected_query

def test_get_str_sql_data_source():
    create_config_obj = CreateConfig(ENV, SID)

    data_source_sql_file_path = 'tests/tools/config/sql_files/deltacube.sql'
    with open(data_source_sql_file_path) as f:
        expected_query = f.read()

    assert create_config_obj._get_str_sql_data_source('rt9999', 'deltacube') == expected_query

def test_get_str_sql_start():
    create_config_obj = CreateConfig(ENV, SID)

    start_sql_file_path = 'tests/tools/config/sql_files/start.sql'
    with open(start_sql_file_path) as f:
        expected_query = f.read()

    assert create_config_obj._get_str_sql_start() == expected_query

def test_get_str_sql_stop():
    create_config_obj = CreateConfig(ENV, SID)

    stop_sql_file_path = 'tests/tools/config/sql_files/stop.sql'
    with open(stop_sql_file_path) as f:
        expected_query = f.read()

    assert create_config_obj._get_str_sql_stop() == expected_query

def test_get_sql_file_path():
    create_config_obj = CreateConfig(ENV, SID)

    assert create_config_obj._get_sql_file_path() == 'settings/database/DB_TAMAR_9999.sql'

def test_create_new_sql_file(tmpdir):
    create_config_obj = CreateConfig(ENV, SID)

    sql_dir = tmpdir.mkdir('sql_file')
    sql_file_path = str(sql_dir.join('DB_TAMAR_9999.sql'))

    query = ''
    create_config_obj._create_new_sql_file(query, sql_file_path)

    assert os.path.isfile(sql_file_path)
