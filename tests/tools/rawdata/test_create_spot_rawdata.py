# -*- coding: utf-8 -*-

from __future__ import absolute_import

from   conf.config                 import BatchConfig
import contextlib
from tools.rawdata.create_spot_rawdata import CreateSpotRawdata
import datetime
from   lib.rawdata.rawdata_creator import RawdataCreator
from   mock                        import Mock
from   mock                        import patch
import os
import pytest
import tarfile

SID       = '9999'
ENV       = 'dev'
DATA_TYPE = 'rawdata'
S3BUCKET  = 's3bucket'
STR_START_DATE= '20141230'
STR_END_DATE  = '20150101'

S3FILE    = 's3_path/rawdata/raw/brainpad-20140515.tgz'

ORDER_HEADER = u'"種別","アクセス時刻","IP","UID","ページID","ロボット判定","オーダーID","内部ID","スケジュールID","クリエイティブID","デバイス","キャリア","OS","ブラウザ","リファラドメイン","初回アクセスフラグ"'.encode('cp932')
ORDER_LOG    = '"bc","2014-05-15 09:00:00","153.164.1.7","my_user_id","8085","0","737","7378","21486","24250","Unknown","Unknown","Windows Vista","Internet Explorer 9","","0"'

CLICK_HEADER = u'"種別","アクセス時刻","IP","UID","ページID","ロボット判定","オーダーID","内部ID","スケジュールID","クリエイティブID","デバイス","キャリア","OS","ブラウザ","リファラドメイン","初回アクセスフラグ"'.encode('cp932')
CLICK_LOG    = '"cl","2014-05-15 09:18:28","1.115.197.192","my_user_id","8392","0","737","7346","21047","23118","Unknown","Unknown","Android 4.0","Android","","0"'

CV_HEADER    = u'"ピクセルID","CV発生時刻","UID","CV対象","ビュー・クリック種別","対象ID","スケジュールID","CV対象最終アクセス時刻"'.encode('cp932')
CV_LOG       = '1\t1400112014\ttest_user3\tcreative\tview\t24746\t1\t1382597028548\t{(12345),(6789)}'

def _create_tgz_file(tgz_dir, tgz_file_name, tmp_dir, my_date):
    str_date = my_date.strftime('%Y%m%d')
    order_file_path      = _create_csv_file(tmp_dir, 'order_9999_{str_date}.csv'.format(str_date=str_date),      [ORDER_HEADER, ORDER_LOG, ORDER_LOG])
    click_file_path      = _create_csv_file(tmp_dir, 'click_9999_{str_date}.csv'.format(str_date=str_date),      [CLICK_HEADER, CLICK_LOG, CLICK_LOG])
    conversion_file_path = _create_csv_file(tmp_dir, 'conversion_9999_{str_date}.csv'.format(str_date=str_date), [CV_HEADER, CV_LOG, CV_LOG])
    __create_tgz_file(tgz_dir, tgz_file_name, [order_file_path, click_file_path, conversion_file_path])

def _create_tgz_file_with_no_log(tgz_dir, tgz_file_name, tmp_dir, my_date):
    str_date = my_date.strftime('%Y%m%d')
    order_file_path      = _create_csv_file(tmp_dir, 'order_9999_{str_date}.csv'.format(str_date=str_date),      [ORDER_HEADER])
    click_file_path      = _create_csv_file(tmp_dir, 'click_9999_{str_date}.csv'.format(str_date=str_date),      [CLICK_HEADER])
    conversion_file_path = _create_csv_file(tmp_dir, 'conversion_9999_{str_date}.csv'.format(str_date=str_date), [CV_HEADER])
    __create_tgz_file(tgz_dir, tgz_file_name, [order_file_path, click_file_path, conversion_file_path])

def __create_tgz_file(dirname, filename, archive_file_list):
    path = str(dirname.join(filename))
    with tarfile.open(path, 'w:gz') as tar:
        for archive_file in archive_file_list:
            my_basename = os.path.basename(archive_file)
            tar.add(str(archive_file), arcname=my_basename)

def _create_csv_file(dirname, filename, lines):
    path = str(dirname.join(filename))
    with open(path, 'w') as f:
        for line in lines:
            f.write(line+'\n')
    return path

@contextlib.contextmanager
def _patches(func_init_config):
    with contextlib.nested (
            patch('conf.config.BatchConfig.__init__', new=func_init_config),
            patch('lib.s3util.S3Util.download_single_file'),
            patch('lib.s3util.S3Util.upload_single_file'),
            patch('lib.s3util.S3Util.exists'),
            ) as (m_config, m_s3get, m_s3put, m_s3exists):
        m_s3get.return_value    = {'key': S3FILE}
        m_s3exists.return_value = True

        yield (m_s3get, m_s3put, m_s3exists)

def test_get_converted_rawdata_file_from_s3(tmpdir, func_init_config):
    base_dir        = tmpdir.mkdir('spot_rawdata')
    rawdata_dir     = base_dir.mkdir('rawdata_dir')
    rawdata_csv_dir = base_dir.mkdir('csv')
    s3_file_name = '9999_20140515.tgz'
    s3_file_path = os.path.join('rawdata/converted/9999', s3_file_name)

    expected_decompressed_rawdata_path_list = [ os.path.join(str(rawdata_dir), '{rawdata_type}_9999_20140515.csv'.format(rawdata_type=rawdata_type)) for rawdata_type in ('order', 'click', 'conversion')]

    order_file_path      = _create_csv_file(rawdata_csv_dir, 'order_9999_20140515.csv', [ORDER_HEADER, ORDER_LOG, ORDER_LOG])
    click_file_path      = _create_csv_file(rawdata_csv_dir, 'click_9999_20140515.csv', [CLICK_HEADER, CLICK_LOG, CLICK_LOG])
    conversion_file_path = _create_csv_file(rawdata_csv_dir, 'conversion_9999_20140515.csv', [CV_HEADER, CV_LOG, CV_LOG])
    __create_tgz_file(rawdata_dir, s3_file_name, [order_file_path, click_file_path, conversion_file_path])

    with _patches(func_init_config) as (m_s3get, m_s3put, m_s3exists):
        m_s3get.return_value = {
            'key'           : s3_file_name,
            'is_dir'        : False,
            'is_file'       : True,
            'size'          : 100,
            'last_modified' : '2014-05-15T00:00:00.000Z'
            }

        create_spot_rawdata_obj = CreateSpotRawdata(SID, ENV, DATA_TYPE, S3BUCKET, STR_START_DATE, STR_END_DATE)
        create_spot_rawdata_obj.local_base_dir = str(rawdata_dir)
        create_spot_rawdata_obj._get_converted_rawdata_file_from_s3(s3_file_path)

        assert sorted(create_spot_rawdata_obj.decompressed_rawdata_path_list) == sorted(expected_decompressed_rawdata_path_list)
        for csv_path in create_spot_rawdata_obj.decompressed_rawdata_path_list:
            assert os.path.exists(csv_path)

def test_get_converted_rawdata_file_from_s3_error(tmpdir, func_init_config):
    base_dir        = tmpdir.mkdir('spot_rawdata')
    rawdata_dir     = base_dir.mkdir('rawdata_dir')
    s3_file_name    = '9999_20140515.tgz'
    s3_file_path    = os.path.join('rawdata/converted/9999', s3_file_name)

    with _patches(func_init_config) as (m_s3get, m_s3put, m_s3exists):
        m_s3get.return_value = {
            'key'           : s3_file_name,
            'is_dir'        : False,
            'is_file'       : True,
            'size'          : 100,
            'last_modified' : '2014-05-15T00:00:00.000Z'
            }

        create_spot_rawdata_obj = CreateSpotRawdata(SID, ENV, DATA_TYPE, S3BUCKET, STR_START_DATE, STR_END_DATE)
        create_spot_rawdata_obj.local_base_dir = str(rawdata_dir)

        with pytest.raises(IOError):
            create_spot_rawdata_obj._get_converted_rawdata_file_from_s3(s3_file_path)

def test_get_converted_rawdata_files_from_s3(tmpdir, func_init_config):
    base_dir        = tmpdir.mkdir('spot_rawdata')
    rawdata_dir     = base_dir.mkdir('rawdata_dir')
    rawdata_csv_dir = base_dir.mkdir('csv')

    my_date  = datetime.datetime.strptime(STR_START_DATE, '%Y%m%d').date()
    end_date = datetime.datetime.strptime(STR_END_DATE,   '%Y%m%d').date()
    while my_date <= end_date:
        str_date = my_date.strftime('%Y%m%d')
        s3_file_name = '9999_{str_date}.tgz'.format(str_date=str_date)
        _create_tgz_file(rawdata_dir, s3_file_name, rawdata_csv_dir, my_date)
        my_date += datetime.timedelta(days=1)

    with _patches(func_init_config) as (m_s3get, m_s3put, m_s3exists):
        m_s3get.return_value = {
            'key'           : s3_file_name,
            'is_dir'        : False,
            'is_file'       : True,
            'size'          : 100,
            'last_modified' : '2014-05-15T00:00:00.000Z'
            }

        create_spot_rawdata_obj = CreateSpotRawdata(SID, ENV, DATA_TYPE, S3BUCKET, STR_START_DATE, STR_END_DATE)
        create_spot_rawdata_obj.local_base_dir = str(rawdata_dir)
        create_spot_rawdata_obj._get_converted_rawdata_files_from_s3()

    for rawdata_type in ('order', 'click', 'conversion'):
        for str_date in ('20141230', '20141231', '20150101'):
            file_name = '{rawdata_type}_9999_{str_date}.csv'.format(rawdata_type=rawdata_type,
                                                                    str_date=str_date)
            assert os.path.exists(os.path.join(str(rawdata_dir), file_name))

def test_merge_rawdata_files(tmpdir, func_init_config):
    base_dir        = tmpdir.mkdir('spot_rawdata')
    rawdata_dir     = base_dir.mkdir('rawdata_dir')
    rawdata_csv_dir = base_dir.mkdir('csv')

    my_date  = datetime.datetime.strptime(STR_START_DATE, '%Y%m%d').date()
    end_date = datetime.datetime.strptime(STR_END_DATE,   '%Y%m%d').date()
    while my_date <= end_date:
        str_date = my_date.strftime('%Y%m%d')
        s3_file_name = '9999_{str_date}.tgz'.format(str_date=str_date)
        _create_tgz_file(rawdata_dir, s3_file_name, rawdata_csv_dir, my_date)
        my_date += datetime.timedelta(days=1)

    with _patches(func_init_config) as (m_s3get, m_s3put, m_s3exists):
        m_s3get.return_value = {
            'key'           : s3_file_name,
            'is_dir'        : False,
            'is_file'       : True,
            'size'          : 100,
            'last_modified' : '2014-05-15T00:00:00.000Z'
            }

        create_spot_rawdata_obj = CreateSpotRawdata(SID, ENV, DATA_TYPE, S3BUCKET, STR_START_DATE, STR_END_DATE)
        create_spot_rawdata_obj.local_base_dir = str(rawdata_dir)
        create_spot_rawdata_obj._get_converted_rawdata_files_from_s3()

    create_spot_rawdata_obj._merge_rawdata_files()

    for rawdata_type in ('order', 'click', 'conversion'):
        rawdata_file_name = '{rawdata_type}_9999_20141230_20150101.csv'.format(rawdata_type=rawdata_type)
        rawdata_file_path = os.path.join(str(rawdata_dir), rawdata_file_name)

        assert os.path.exists(rawdata_file_path)

        with open(rawdata_file_path, 'r') as fp:
            lines = fp.readlines()
            assert len(lines) == 7
            if rawdata_type == 'order':
                assert lines[0] == ORDER_HEADER + '\n'
                for line in lines[1:]:
                    assert line == ORDER_LOG + '\n'
            elif rawdata_type == 'click':
                assert lines[0] == CLICK_HEADER + '\n'
                for line in lines[1:]:
                    assert line == CLICK_LOG + '\n'
            elif rawdata_type == 'conversion':
                assert lines[0] == CV_HEADER + '\n'
                for line in lines[1:]:
                    assert line == CV_LOG + '\n'

def test_merge_rawdata_files_with_no_log(tmpdir, func_init_config):
    base_dir        = tmpdir.mkdir('spot_rawdata')
    rawdata_dir     = base_dir.mkdir('rawdata_dir')
    rawdata_csv_dir = base_dir.mkdir('csv')

    _create_tgz_file_with_no_log(rawdata_dir, '9999_20141230.tgz', rawdata_csv_dir, datetime.date(2014,12,30))
    _create_tgz_file_with_no_log(rawdata_dir, '9999_20141231.tgz', rawdata_csv_dir, datetime.date(2014,12,31))
    _create_tgz_file_with_no_log(rawdata_dir, '9999_20150101.tgz', rawdata_csv_dir, datetime.date(2015,1,1))

    with _patches(func_init_config) as (m_s3get, m_s3put, m_s3exists):
        m_s3get.return_value = {
            'key'           : "",
            'is_dir'        : False,
            'is_file'       : True,
            'size'          : 100,
            'last_modified' : '2014-05-15T00:00:00.000Z'
            }

        create_spot_rawdata_obj = CreateSpotRawdata(SID, ENV, DATA_TYPE, S3BUCKET, STR_START_DATE, STR_END_DATE)
        create_spot_rawdata_obj.local_base_dir = str(rawdata_dir)
        create_spot_rawdata_obj._get_converted_rawdata_files_from_s3()

    create_spot_rawdata_obj._merge_rawdata_files()

    for rawdata_type in ('order', 'click', 'conversion'):
        rawdata_file_name = '{rawdata_type}_9999_20141230_20150101.csv'.format(rawdata_type=rawdata_type)
        rawdata_file_path = os.path.join(str(rawdata_dir), rawdata_file_name)

        assert os.path.exists(rawdata_file_path)

        with open(rawdata_file_path, 'r') as fp:
            lines = fp.readlines()
            assert len(lines) == 1
            if rawdata_type == 'order':
                assert lines[0] == ORDER_HEADER + '\n'
            elif rawdata_type == 'click':
                assert lines[0] == CLICK_HEADER + '\n'
            elif rawdata_type == 'conversion':
                assert lines[0] == CV_HEADER + '\n'

def test_compress_spot_rawdata(tmpdir, func_init_config):
    base_dir        = tmpdir.mkdir('spot_rawdata')
    rawdata_dir     = base_dir.mkdir('rawdata_dir')
    rawdata_csv_dir = base_dir.mkdir('csv')

    my_date  = datetime.datetime.strptime(STR_START_DATE, '%Y%m%d').date()
    end_date = datetime.datetime.strptime(STR_END_DATE,   '%Y%m%d').date()
    while my_date <= end_date:
        str_date = my_date.strftime('%Y%m%d')
        s3_file_name = '9999_{str_date}.tgz'.format(str_date=str_date)
        _create_tgz_file(rawdata_dir, s3_file_name, rawdata_csv_dir, my_date)
        my_date += datetime.timedelta(days=1)

    with _patches(func_init_config) as (m_s3get, m_s3put, m_s3exists):
        m_s3get.return_value = {
            'key'           : s3_file_name,
            'is_dir'        : False,
            'is_file'       : True,
            'size'          : 100,
            'last_modified' : '2014-05-15T00:00:00.000Z'
            }

        create_spot_rawdata_obj = CreateSpotRawdata(SID, ENV, DATA_TYPE, S3BUCKET, STR_START_DATE, STR_END_DATE)
        create_spot_rawdata_obj.local_base_dir = str(rawdata_dir)
        create_spot_rawdata_obj._get_converted_rawdata_files_from_s3()

    create_spot_rawdata_obj._merge_rawdata_files()
    create_spot_rawdata_obj._compress_spot_rawdata()

    assert os.path.exists(os.path.join(str(rawdata_dir), '9999_20141230_20150101.tgz'))

def test_detect_rawdata_type(func_init_config):
    create_spot_rawdata_obj = CreateSpotRawdata(SID, ENV, DATA_TYPE, S3BUCKET, STR_START_DATE, STR_END_DATE)

    assert 'order'      == create_spot_rawdata_obj._CreateSpotRawdata__detect_rawdata_type('order_9999_20141230.csv')
    assert 'click'      == create_spot_rawdata_obj._CreateSpotRawdata__detect_rawdata_type('click_9999_20141230.csv')
    assert 'conversion' == create_spot_rawdata_obj._CreateSpotRawdata__detect_rawdata_type('conversion_9999_20141230.csv')
