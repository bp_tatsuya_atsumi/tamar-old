# -*- coding: utf-8 -*-

from __future__ import absolute_import

import tools.mapping.reporter as reporter

import contextlib
from   mock        import patch
import pytest
from   conf.config import BatchConfig


ENV = 'dev'


def _mock_init_config():
    config = BatchConfig(ENV)
    config.site_mst = {
        'bp1' : {'name_jp': u'ブレインパッドテスト1'},
        'bp2' : {'name_jp': u'ブレインパッドテスト2'},
        'bp3' : {'name_jp': u'ブレインパッドテスト3'},
        'bp4' : {'name_jp': u'ブレインパッドテスト4'},
    }
    config.partner_mst = {
        'pt1' : {'name_jp': u'パートナー1'},
        'pt2' : {'name_jp': u'パートナー2'},
        'rt'  : {'name_jp': u'パートナーRT'},
    }
    config.mapping = {
        'bp1' : {'pt1' : {}, 'pt2' : {}},
        'bp2' : {'pt1' : {}, 'pt2' : {}},
        'bp3' : {'pt1' : {}, 'pt3' : {}},
        'bp4' : {'rt' :  {'bridges':['rtbp1', 'rtbp2']}},
    }
    config.database = {
        'bp1' : {
            'host'     : 'host',
            'port'     : '3306',
            'user'     : 'tamaruser',
            'password' : 'pasword',
            'db_name'  : 'db_name'
        },
        'bp3' : {
            'host'     : 'host',
            'port'     : '3306',
            'user'     : 'tamaruser',
            'password' : 'pasword',
            'db_name'  : 'db_name'
        },
        'bp4' : {
            'host'     : 'host',
            'port'     : '3306',
            'user'     : 'tamaruser',
            'password' : 'pasword',
            'db_name'  : 'db_name'
        },
    }
    def __mock_init_config(self, env):
        for k, v in vars(config).items():
            setattr(self, k, v)
    return __mock_init_config

@contextlib.contextmanager
def _patches():
    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=_mock_init_config()),
        patch('MySQLdb.connect'),
        patch('lib.sesutil.SESUtil.send_mail')
    ) as (mock_config, mock_mysql, mock_sesutil):
        yield (mock_sesutil)


def test_send_report():
    with _patches() as mock_sesutil:
        reporter.send_report(ENV)

    assert mock_sesutil.call_count == 1
    assert mock_sesutil.call_args_list[0][0][0] == u'マッピング結果レポート[dev]'
    assert u'ブレインパッドテスト1 <=> パートナー1' in mock_sesutil.call_args_list[0][0][1]
    assert u'ブレインパッドテスト1 <=> パートナー2' in mock_sesutil.call_args_list[0][0][1]
    assert u'ブレインパッドテスト3 <=> パートナー1' in mock_sesutil.call_args_list[0][0][1]
    assert u'ブレインパッドテスト4 <=> パートナーRT[rtbp1]' in mock_sesutil.call_args_list[0][0][1]

def test_send_report_ses_error():
    with _patches() as mock_sesutil:
        mock_sesutil.side_effect = Exception('Failed to send mail')
        with pytest.raises(Exception):
            reporter.send_report(ENV)

    assert mock_sesutil.call_count == 1
    assert mock_sesutil.call_args_list[0][0][0] == u'マッピング結果レポート[dev]'
    assert u'ブレインパッドテスト1 <=> パートナー1' in mock_sesutil.call_args_list[0][0][1]
    assert u'ブレインパッドテスト1 <=> パートナー2' in mock_sesutil.call_args_list[0][0][1]
    assert u'ブレインパッドテスト2 <=> パートナー1' not in mock_sesutil.call_args_list[0][0][1]
    assert u'ブレインパッドテスト2 <=> パートナー2' not in mock_sesutil.call_args_list[0][0][1]
    assert u'ブレインパッドテスト3 <=> パートナー1' in mock_sesutil.call_args_list[0][0][1]
    assert u'ブレインパッドテスト3 <=> パートナー2' not in mock_sesutil.call_args_list[0][0][1]
    assert u'ブレインパッドテスト4 <=> パートナー1' not in mock_sesutil.call_args_list[0][0][1]
    assert u'ブレインパッドテスト4 <=> パートナー2' not in mock_sesutil.call_args_list[0][0][1]
