# -*- coding: utf-8 -*-

from tools.migration_so import migrate_mapping_table

import contextlib
import datetime
import gzip
from   mock             import patch
import pytest
from   lib.mappingtable import MappingTable


SID = '9999'
ENV = 'dev'

DB_USER  = 'tamaruser'
DB_PASS  = '#!TawaR?u5er'
DB_HOST  = 'localhost'
DB_NAME  = 'DB_TAMAR_9999'
TBL_NAME = 't_user_mapping_9999_so'

TSV_LINES = '''
1384004914\tpartner_user1\tpartner_sid\tunknown_text\trt_user1
1384004915\tpartner_user2\tpartner_sid\tunknown_text\trt_user2
1384004916\tpartner_user3\tpartner_sid\tunknown_text\trt_user3
'''.lstrip()

@pytest.fixture
def tbl():
    return MappingTable(DB_USER, DB_PASS, DB_HOST, DB_NAME, TBL_NAME)

@pytest.fixture(autouse=True)
def truncate_table(tbl):
    tbl.truncate_table()

@pytest.fixture
def data_dir(tmp_tools_dir):
    return tmp_tools_dir.mkdir('migration_so').mkdir(SID)

def _prepare_tsvgz_file(data_dir, tsvgz_filename, content):
    tsvgz_file = data_dir.join(tsvgz_filename)
    with gzip.open(str(tsvgz_file), 'wb') as f:
        f.write(content)
    return tsvgz_file

@contextlib.contextmanager
def _patches(func_init_config):
    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('shutil.rmtree'),
        patch('os.makedirs'),
        patch('lib.s3util.S3Util.download_single_file'),
    ) as (m_config, m_rmtree, m_makedirs, m_s3get):
        m_s3get.return_value = {'key': 's3dir/test.tsv.gz'}
        yield m_s3get

def test_execute(func_init_config, data_dir, tbl):

    # prepare
    _prepare_tsvgz_file(data_dir, 'test.tsv.gz', TSV_LINES)

    # execute
    with _patches(func_init_config) as m_s3get:
        migrate_mapping_table.execute(SID, ENV, 's3dir/test.tsv.gz')

    # assert file
    data_dir.join('test.tsv_sorted_splitted.0000').check(exists=1, dir=0, file=1)
    data_dir.join('test.tsv_sorted_splitted.0001').check(exists=0)

    # assert mock
    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == ('dev-partner-log', 's3dir/test.tsv.gz', str(data_dir))

    # assert table
    rows = tbl.select_all_rows()
    assert len(rows) == 3
    assert rows[0] == ('rt_user1', 'partner_user1', 1, 'rt_user1', datetime.datetime(2013, 11, 9, 22, 48, 34), datetime.datetime(2013, 11, 9, 22, 48, 34), False)
    assert rows[1] == ('rt_user2', 'partner_user2', 1, 'rt_user2', datetime.datetime(2013, 11, 9, 22, 48, 35), datetime.datetime(2013, 11, 9, 22, 48, 35), False)
    assert rows[2] == ('rt_user3', 'partner_user3', 1, 'rt_user3', datetime.datetime(2013, 11, 9, 22, 48, 36), datetime.datetime(2013, 11, 9, 22, 48, 36), False)

def test_execute_split(func_init_config, data_dir, tbl):

    # prepare
    _prepare_tsvgz_file(data_dir, 'test.tsv.gz', TSV_LINES)

    # execute
    with _patches(func_init_config) as m_s3get:
        migrate_mapping_table.execute(SID, ENV, 's3dir/test.tsv.gz', 2)

    # assert file
    data_dir.join('test.tsv_sorted_splitted.0000').check(exists=1, dir=0, file=1)
    data_dir.join('test.tsv_sorted_splitted.0001').check(exists=1, dir=0, file=1)

    # assert mock
    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == ('dev-partner-log', 's3dir/test.tsv.gz', str(data_dir))

    # assert table
    rows = tbl.select_all_rows()
    assert len(rows) == 3
    assert rows[0] == ('rt_user1', 'partner_user1', 1, 'rt_user1', datetime.datetime(2013, 11, 9, 22, 48, 34), datetime.datetime(2013, 11, 9, 22, 48, 34), False)
    assert rows[1] == ('rt_user2', 'partner_user2', 1, 'rt_user2', datetime.datetime(2013, 11, 9, 22, 48, 35), datetime.datetime(2013, 11, 9, 22, 48, 35), False)
    assert rows[2] == ('rt_user3', 'partner_user3', 1, 'rt_user3', datetime.datetime(2013, 11, 9, 22, 48, 36), datetime.datetime(2013, 11, 9, 22, 48, 36), False)

def test_execute_empty_file(func_init_config, data_dir, tbl):

    # prepare
    _prepare_tsvgz_file(data_dir, 'test.tsv.gz', '')

    # execute
    with _patches(func_init_config) as m_s3get:
        migrate_mapping_table.execute(SID, ENV, 's3dir/test.tsv.gz')

    # assert file
    data_dir.join('test.tsv_sorted_splitted.0000').check(exists=1, dir=0, file=1)
    data_dir.join('test.tsv_sorted_splitted.0001').check(exists=0)

    # assert mock
    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == ('dev-partner-log', 's3dir/test.tsv.gz', str(data_dir))

    # assert table
    rows = tbl.select_all_rows()
    assert len(rows) == 0

def test_execute_s3file_not_exists(func_init_config, data_dir, tbl):

    # execute
    with _patches(func_init_config) as m_s3get:
        m_s3get.return_value = None
        with pytest.raises(Exception):
            migrate_mapping_table.execute(SID, ENV, 's3dir/test.tsv.gz')

def test_execute_database_config_not_exists(func_init_config, data_dir, tbl):

    # prepare
    _prepare_tsvgz_file(data_dir, 'test.tsv.gz', TSV_LINES)

    # execute
    with _patches(func_init_config) as m_s3get:
        with pytest.raises(Exception):
            migrate_mapping_table.execute('XXXX', ENV, 's3dir/test.tsv.gz')

    # assert file
    data_dir.join('test.tsv_sorted_splitted.0000').check(exists=1, dir=0, file=1)
    data_dir.join('test.tsv_sorted_splitted.0001').check(exists=0)

    # assert mock
    assert m_s3get.call_count == 0

    # assert table
    rows = tbl.select_all_rows()
    assert len(rows) == 0
