# -*- coding: utf-8 -*-

from __future__ import absolute_import

import tools.transferer.splitter as splitter

import contextlib
import gzip
import pytest
from   mock import patch


SID         = '9999'
ENV         = 'dev'
DATA_TYPE   = 'segmentdata'
S3BUCKET    = 's3bucket'
S3FILE      = 's3dir/segment_list_data/raw/segment_list_data_20131001201510_9999.json.gz'
S3DIR_SPLIT = 's3dir/segment_list_data/raw/split/'

@pytest.fixture
def data_dir(tmp_tools_dir):
    return tmp_tools_dir.mkdir('splitter')

@contextlib.contextmanager
def _patches_with_default_action(func_init_config):
    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('lib.s3util.S3Util.download_single_file'),
        patch('lib.s3util.S3Util.upload_single_file')
    ) as (m_config, m_s3get, m_s3put):
        m_s3get.return_value = {'key': S3FILE}
        yield (m_s3get, m_s3put)

def _create_gzip(file, lines):
    gzip.open(file, 'wb').write('\n'.join(lines))


def test_split(func_init_config, data_dir):

    # prepare
    gzfile     = data_dir.join('segment_list_data_20131001201510_9999.json.gz')
    splitfile0 = data_dir.join('segment_list_data_20131001201510_9999_0001.json.gz')
    splitfile1 = data_dir.join('segment_list_data_20131001201510_9999_0002.json.gz')
    splitfile2 = data_dir.join('segment_list_data_20131001201510_9999_0003.json.gz')
    splitfile3 = data_dir.join('segment_list_data_20131001201510_9999_0004.json.gz')

    _create_gzip(str(gzfile), ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'])

    # execute
    with _patches_with_default_action(func_init_config) as (m_s3get, m_s3put):
        result = splitter.split(SID, ENV, DATA_TYPE, S3BUCKET, S3FILE, 3)

    # assert
    assert len(result) == 4
    assert result      == ['s3dir/segment_list_data/raw/split/segment_list_data_20131001201510_9999_0001.json.gz',
                           's3dir/segment_list_data/raw/split/segment_list_data_20131001201510_9999_0002.json.gz',
                           's3dir/segment_list_data/raw/split/segment_list_data_20131001201510_9999_0003.json.gz',
                           's3dir/segment_list_data/raw/split/segment_list_data_20131001201510_9999_0004.json.gz']
    assert gzfile.check(exists=0)
    assert splitfile0.check(exists=0)
    assert splitfile1.check(exists=0)
    assert splitfile2.check(exists=0)
    assert splitfile3.check(exists=0)
    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))
    assert m_s3put.call_count == 4
    assert m_s3put.call_args_list[0][0] == (str(splitfile0), S3BUCKET, S3DIR_SPLIT)
    assert m_s3put.call_args_list[1][0] == (str(splitfile1), S3BUCKET, S3DIR_SPLIT)
    assert m_s3put.call_args_list[2][0] == (str(splitfile2), S3BUCKET, S3DIR_SPLIT)
    assert m_s3put.call_args_list[3][0] == (str(splitfile3), S3BUCKET, S3DIR_SPLIT)

def test_split_no_split(func_init_config, data_dir):

    # prepare
    gzfile     = data_dir.join('segment_list_data_20131001201510_9999.json.gz')
    splitfile0 = data_dir.join('segment_list_data_20131001201510_9999_0001.json.gz')

    _create_gzip(str(gzfile), ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'])

    # execute
    with _patches_with_default_action(func_init_config) as (m_s3get, m_s3put):
        result = splitter.split(SID, ENV, DATA_TYPE, S3BUCKET, S3FILE, 10)

    # assert
    assert len(result) == 1
    assert result      == ['s3dir/segment_list_data/raw/split/segment_list_data_20131001201510_9999_0001.json.gz']
    assert gzfile.check(exists=0)
    assert splitfile0.check(exists=0)
    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))
    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(splitfile0), S3BUCKET, S3DIR_SPLIT)

def test_split_no_s3file(func_init_config, data_dir):

    # execute
    with _patches_with_default_action(func_init_config) as (m_s3get, m_s3put):
        m_s3get.return_value = None
        with pytest.raises(Exception):
            splitter.split(SID, ENV, DATA_TYPE, S3BUCKET, S3FILE, 3)

def test_split_empty_file(func_init_config, data_dir):

    # prepare
    gzfile     = data_dir.join('segment_list_data_20131001201510_9999.json.gz')
    splitfile0 = data_dir.join('segment_list_data_20131001201510_9999_0001.json.gz')

    _create_gzip(str(gzfile), [])

    # execute
    with _patches_with_default_action(func_init_config) as (m_s3get, m_s3put):
        result = splitter.split(SID, ENV, DATA_TYPE, S3BUCKET, S3FILE, 10)

    # assert
    assert len(result) == 1
    assert result      == ['s3dir/segment_list_data/raw/split/segment_list_data_20131001201510_9999_0001.json.gz']
    assert gzfile.check(exists=0)
    assert splitfile0.check(exists=0)
    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE, str(data_dir))
    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(splitfile0), S3BUCKET, S3DIR_SPLIT)

def test_split_not_gzip_file(func_init_config, data_dir):

    # prepare
    gzfile     = data_dir.join('segment_list_data_20131001201510_9999.json.gz')
    splitfile0 = data_dir.join('segment_list_data_20131001201510_9999_0001.json.gz')

    open(str(gzfile), 'w').write('test')

    # execute
    with _patches_with_default_action(func_init_config) as (m_s3get, m_s3put):
        with pytest.raises(Exception):
            splitter.split(SID, ENV, DATA_TYPE, S3BUCKET, S3FILE, 3)

    # assert
    assert gzfile.check(exists=0)
    assert splitfile0.check(exists=0)
