# -*- coding: utf-8 -*-

from __future__ import absolute_import

import tools.transferer.tombsender as tombsender

import contextlib
import datetime
import os
from   mock       import patch
import pytest


SID           = '9999'
ENV           = 'dev'
S3BUCKET      = 'dev-transfer-data'
S3DIR_SEGMST  = '9999/segment_list_mst'
S3DIR_SEGDATA = '9999/segment_list_data'
S3DIR_RECMST  = '9999/recommend_list_mst'
S3DIR_RECDATA = '9999/recommend_list_data'
S3DIR_CONV    = '9999/conversion_log'
S3DIR_MAILREQ = '9999/mail_request_data'
S3DIR_MAILRES = '9999/mail_response_data'
S3DIR_MAILSEG = '9999/mail_list_data'


class CurrentDatetime(datetime.datetime):
    @classmethod
    def utcnow(cls):
        return cls(2013, 10, 1, 9, 10, 20, 0)

@contextlib.contextmanager
def _patches():
    with contextlib.nested (
        patch('lib.s3util.S3Util.list_dir'),
        patch('lib.s3util.S3Util.move_single_file'),
        patch('datetime.datetime', CurrentDatetime)
    ) as (m_s3ls, m_s3mv, m_now):
        m_s3ls.return_value = None
        m_s3mv.return_value = True

        yield (m_s3ls, m_s3mv)

def _execute():
    return tombsender.send_tomb(SID, ENV)


def test_send_tomb_expired():
    with _patches() as (m_s3ls, m_s3mv):
        m_s3ls.side_effect = [
            [
                {
                    'key'           : os.path.join(S3DIR_SEGMST,
                                                   'raw/segment_list_mst_20140311174700.csv.gz'),
                    'is_file'       : True,
                    'last_modified' : '2013-09-01T00:00:00.000Z'
                }
            ],
            [],
            [],
            [],
            [],
            [],
            [],
            []
        ]
        _execute()

    assert m_s3ls.call_count == 8
    assert m_s3ls.call_args_list[0][0] == (S3BUCKET, S3DIR_SEGMST)
    assert m_s3ls.call_args_list[1][0] == (S3BUCKET, S3DIR_SEGDATA)
    assert m_s3ls.call_args_list[2][0] == (S3BUCKET, S3DIR_RECMST)
    assert m_s3ls.call_args_list[3][0] == (S3BUCKET, S3DIR_RECDATA)
    assert m_s3ls.call_args_list[4][0] == (S3BUCKET, S3DIR_CONV)
    assert m_s3ls.call_args_list[5][0] == (S3BUCKET, S3DIR_MAILREQ)
    assert m_s3ls.call_args_list[6][0] == (S3BUCKET, S3DIR_MAILRES)
    assert m_s3ls.call_args_list[7][0] == (S3BUCKET, S3DIR_MAILSEG)

    assert m_s3mv.call_count == 1
    assert m_s3mv.call_args_list[0][0] == (
               S3BUCKET,
               os.path.join(S3DIR_SEGMST, 'raw/segment_list_mst_20140311174700.csv.gz'),
               S3BUCKET,
               os.path.join('tomb', S3DIR_SEGMST, 'raw')
           )

def test_send_tomb_not_expired():
    with _patches() as (m_s3ls, m_s3mv):
        m_s3ls.side_effect = [
            [
                {
                    'key'           : os.path.join(S3DIR_SEGMST,
                                                   'raw/segment_list_mst_20140311174700.csv.gz'),
                    'is_file'       : True,
                    'last_modified' : '2013-10-01T00:00:00.000Z'
                }
            ],
            [],
            [],
            [],
            [],
            [],
            [],
            []
        ]
        _execute()

    assert m_s3ls.call_count == 8
    assert m_s3ls.call_args_list[0][0] == (S3BUCKET, S3DIR_SEGMST)
    assert m_s3ls.call_args_list[1][0] == (S3BUCKET, S3DIR_SEGDATA)
    assert m_s3ls.call_args_list[2][0] == (S3BUCKET, S3DIR_RECMST)
    assert m_s3ls.call_args_list[3][0] == (S3BUCKET, S3DIR_RECDATA)
    assert m_s3ls.call_args_list[4][0] == (S3BUCKET, S3DIR_CONV)
    assert m_s3ls.call_args_list[5][0] == (S3BUCKET, S3DIR_MAILREQ)
    assert m_s3ls.call_args_list[6][0] == (S3BUCKET, S3DIR_MAILRES)
    assert m_s3ls.call_args_list[7][0] == (S3BUCKET, S3DIR_MAILSEG)

    assert m_s3mv.call_count == 0

def test_send_tomb_dir(func_init_config):
    with _patches() as (m_s3ls, m_s3mv):
        m_s3ls.side_effect = [
            [
                {
                    'key'           : os.path.join(S3DIR_SEGMST,
                                                   'raw/segment_list_mst_20140311174700.csv.gz'),
                    'is_file'       : False,
                    'last_modified' : '2013-09-01T00:00:00.000Z'
                }
            ],
            [],
            [],
            [],
            [],
            [],
            [],
            []
        ]
        _execute()

    assert m_s3ls.call_count == 8
    assert m_s3ls.call_args_list[0][0] == (S3BUCKET, S3DIR_SEGMST)
    assert m_s3ls.call_args_list[1][0] == (S3BUCKET, S3DIR_SEGDATA)
    assert m_s3ls.call_args_list[2][0] == (S3BUCKET, S3DIR_RECMST)
    assert m_s3ls.call_args_list[3][0] == (S3BUCKET, S3DIR_RECDATA)
    assert m_s3ls.call_args_list[4][0] == (S3BUCKET, S3DIR_CONV)
    assert m_s3ls.call_args_list[5][0] == (S3BUCKET, S3DIR_MAILREQ)
    assert m_s3ls.call_args_list[6][0] == (S3BUCKET, S3DIR_MAILRES)
    assert m_s3ls.call_args_list[7][0] == (S3BUCKET, S3DIR_MAILSEG)

    assert m_s3mv.call_count == 0

def test_send_tomb_expired_multiple_files(func_init_config):
    with _patches() as (m_s3ls, m_s3mv):
        m_s3ls.side_effect = [
            [
                {
                    'key'           : os.path.join(S3DIR_SEGMST,
                                                   'raw/segment_list_mst_20140311174700.csv.gz'),
                    'is_file'       : True,
                    'last_modified' : '2013-09-01T00:00:00.000Z'
                },
                {
                    'key'           : os.path.join(S3DIR_SEGMST,
                                                   'raw/segment_list_mst_20140311174701.csv.gz'),
                    'is_file'       : True,
                    'last_modified' : '2013-09-01T00:00:00.000Z'
                }
            ],
            [],
            [],
            [],
            [],
            [],
            [],
            []
        ]
        _execute()

    assert m_s3ls.call_count == 8
    assert m_s3ls.call_args_list[0][0] == (S3BUCKET, S3DIR_SEGMST)
    assert m_s3ls.call_args_list[1][0] == (S3BUCKET, S3DIR_SEGDATA)
    assert m_s3ls.call_args_list[2][0] == (S3BUCKET, S3DIR_RECMST)
    assert m_s3ls.call_args_list[3][0] == (S3BUCKET, S3DIR_RECDATA)
    assert m_s3ls.call_args_list[4][0] == (S3BUCKET, S3DIR_CONV)
    assert m_s3ls.call_args_list[5][0] == (S3BUCKET, S3DIR_MAILREQ)
    assert m_s3ls.call_args_list[6][0] == (S3BUCKET, S3DIR_MAILRES)
    assert m_s3ls.call_args_list[7][0] == (S3BUCKET, S3DIR_MAILSEG)

    assert m_s3mv.call_count == 2
    assert m_s3mv.call_args_list[0][0] == (
               S3BUCKET,
               os.path.join(S3DIR_SEGMST, 'raw/segment_list_mst_20140311174700.csv.gz'),
               S3BUCKET,
               os.path.join('tomb', S3DIR_SEGMST, 'raw')
           )
    assert m_s3mv.call_args_list[1][0] == (
               S3BUCKET,
               os.path.join(S3DIR_SEGMST, 'raw/segment_list_mst_20140311174701.csv.gz'),
               S3BUCKET,
               os.path.join('tomb', S3DIR_SEGMST, 'raw')
           )

def test_send_tomb_expired_multiple_dirs(func_init_config):
    with _patches() as (m_s3ls, m_s3mv):
        m_s3ls.side_effect = [
            [
                {
                    'key'           : os.path.join(S3DIR_SEGMST,
                                                   'raw/segment_list_mst_20140311174700.csv.gz'),
                    'is_file'       : True,
                    'last_modified' : '2013-09-01T00:00:00.000Z'
                }
            ],
            [
                {
                    'key'           : os.path.join(S3DIR_SEGDATA,
                                                   'raw/segment_list_data_20140311174700.csv.gz'),
                    'is_file'       : True,
                    'last_modified' : '2013-09-01T00:00:00.000Z'
                }
            ],
            [],
            [],
            [],
            [],
            [],
            []
        ]
        _execute()

    assert m_s3ls.call_count == 8
    assert m_s3ls.call_args_list[0][0] == (S3BUCKET, S3DIR_SEGMST)
    assert m_s3ls.call_args_list[1][0] == (S3BUCKET, S3DIR_SEGDATA)
    assert m_s3ls.call_args_list[2][0] == (S3BUCKET, S3DIR_RECMST)
    assert m_s3ls.call_args_list[3][0] == (S3BUCKET, S3DIR_RECDATA)
    assert m_s3ls.call_args_list[4][0] == (S3BUCKET, S3DIR_CONV)
    assert m_s3ls.call_args_list[5][0] == (S3BUCKET, S3DIR_MAILREQ)
    assert m_s3ls.call_args_list[6][0] == (S3BUCKET, S3DIR_MAILRES)
    assert m_s3ls.call_args_list[7][0] == (S3BUCKET, S3DIR_MAILSEG)

    assert m_s3mv.call_count == 2
    assert m_s3mv.call_args_list[0][0] == (
               S3BUCKET,
               os.path.join(S3DIR_SEGMST, 'raw/segment_list_mst_20140311174700.csv.gz'),
               S3BUCKET,
               os.path.join('tomb', S3DIR_SEGMST, 'raw')
           )
    assert m_s3mv.call_args_list[1][0] == (
               S3BUCKET,
               os.path.join(S3DIR_SEGDATA, 'raw/segment_list_data_20140311174700.csv.gz'),
               S3BUCKET,
               os.path.join('tomb', S3DIR_SEGDATA, 'raw')
           )

def test_send_tomb_expired_and_not_expired(func_init_config):
    with _patches() as (m_s3ls, m_s3mv):
        m_s3ls.side_effect = [
            [
                {
                    'key'           : os.path.join(S3DIR_SEGMST,
                                                   'raw/segment_list_mst_20140311174700.csv.gz'),
                    'is_file'       : True,
                    'last_modified' : '2013-09-01T00:00:00.000Z'
                },
                {
                    'key'           : os.path.join(S3DIR_SEGMST,
                                                   'raw/segment_list_mst_20140311174701.csv.gz'),
                    'is_file'       : True,
                    'last_modified' : '2013-10-01T00:00:00.000Z'
                }
            ],
            [],
            [],
            [],
            [],
            [],
            [],
            []
        ]
        _execute()

    assert m_s3ls.call_count == 8
    assert m_s3ls.call_args_list[0][0] == (S3BUCKET, S3DIR_SEGMST)
    assert m_s3ls.call_args_list[1][0] == (S3BUCKET, S3DIR_SEGDATA)
    assert m_s3ls.call_args_list[2][0] == (S3BUCKET, S3DIR_RECMST)
    assert m_s3ls.call_args_list[3][0] == (S3BUCKET, S3DIR_RECDATA)
    assert m_s3ls.call_args_list[4][0] == (S3BUCKET, S3DIR_CONV)
    assert m_s3ls.call_args_list[5][0] == (S3BUCKET, S3DIR_MAILREQ)
    assert m_s3ls.call_args_list[6][0] == (S3BUCKET, S3DIR_MAILRES)
    assert m_s3ls.call_args_list[7][0] == (S3BUCKET, S3DIR_MAILSEG)

    assert m_s3mv.call_count == 1
    assert m_s3mv.call_args_list[0][0] == (
               S3BUCKET,
               os.path.join(S3DIR_SEGMST, 'raw/segment_list_mst_20140311174700.csv.gz'),
               S3BUCKET,
               os.path.join('tomb', S3DIR_SEGMST, 'raw')
           )

def test_send_tomb_file_not_exists(func_init_config):
    with _patches() as (m_s3ls, m_s3mv):
        m_s3ls.return_value = None
        _execute()

    assert m_s3ls.call_count == 8
    assert m_s3ls.call_args_list[0][0] == (S3BUCKET, S3DIR_SEGMST)
    assert m_s3ls.call_args_list[1][0] == (S3BUCKET, S3DIR_SEGDATA)
    assert m_s3ls.call_args_list[2][0] == (S3BUCKET, S3DIR_RECMST)
    assert m_s3ls.call_args_list[3][0] == (S3BUCKET, S3DIR_RECDATA)
    assert m_s3ls.call_args_list[4][0] == (S3BUCKET, S3DIR_CONV)
    assert m_s3ls.call_args_list[5][0] == (S3BUCKET, S3DIR_MAILREQ)
    assert m_s3ls.call_args_list[6][0] == (S3BUCKET, S3DIR_MAILRES)
    assert m_s3ls.call_args_list[7][0] == (S3BUCKET, S3DIR_MAILSEG)

    assert m_s3mv.call_count == 0

def test_send_tomb_with_s3mv_error(func_init_config):
    with _patches() as (m_s3ls, m_s3mv):
        m_s3ls.side_effect = [
            [
                {
                    'key'           : os.path.join(S3DIR_SEGMST,
                                                   'raw/segment_list_mst_20140311174700.csv.gz'),
                    'is_file'       : True,
                    'last_modified' : '2013-09-01T00:00:00.000Z'
                }
            ],
            [],
            [],
            [],
            [],
            [],
            [],
            []
        ]
        m_s3mv.side_effect = Exception('Failed to move')
        with pytest.raises(Exception):
            _execute()

    assert m_s3ls.call_count == 1
    assert m_s3ls.call_args_list[0][0] == (S3BUCKET, S3DIR_SEGMST)

    assert m_s3mv.call_count == 1
    assert m_s3mv.call_args_list[0][0] == (
               S3BUCKET,
               os.path.join(S3DIR_SEGMST, 'raw/segment_list_mst_20140311174700.csv.gz'),
               S3BUCKET,
               os.path.join('tomb', S3DIR_SEGMST, 'raw')
           )
