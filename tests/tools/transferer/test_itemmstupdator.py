# -*- coding: utf-8 -*-

from __future__ import absolute_import

import tools.transferer.itemmstupdator as updator

import contextlib
import gzip
import pytest
from   mock                     import patch
from   lib.transferer.exception import NoS3Files


SID               = '9999'
ENV               = 'dev'
S3BUCKET          = 'dev-transfer_data'
S3FILE_RAW        = '9999/item_mst/raw/item_mst_20131105140000_9999.csv.gz'
S3FILE_LATEST_OLD = '9999/item_mst/latest/item_mst_latest_20131105130000_9999.csv.gz'
S3FILE_LATEST_NEW = '9999/item_mst/latest/item_mst_latest_20131105140000_9999.csv.gz'

CSV_HEADER_RAW    = 'pid,sid,code,name,img_url,link_url,price,description,status,last_modified_date'
CSV_DATA_RAW1     = 'rtoaster,9999,itemcode1,商品名１,http://img_url/10,http://link_url10,1000円,商品説明１,1,2013-11-11 00:00:00'
CSV_DATA_RAW2     = 'rtoaster,9999,itemcode2,商品名２,http://img_url/20,http://link_url20,2000円,商品説明２,2,2013-11-12 00:00:00'
CSV_DATA_RAW3     = 'rtoaster,9999,itemcode3,商品名３,http://img_url/30,http://link_url30,3000円,商品説明３,3,2013-11-13 00:00:00'
CSV_DATA_RAW4     = 'rtoaster,9999,itemcode4,商品名４,http://img_url/40,http://link_url40,4000円,商品説明４,4,2013-11-14 00:00:00'
CSV_DATA_RAW5     = 'rtoaster,9999,itemcode5,商品名５,http://img_url/50,http://link_url50,5000円,商品説明５,5,2013-11-15 00:00:00'

CSV_HEADER_LATEST = 'pid,sid,code,id,name,img_url,link_url,price,description,status,last_modified_date'
CSV_DATA_LATEST1  = 'rtoaster,9999,itemcode1,1234567890,商品名1,http://img_url/1,http://link_url1,100円,商品説明1,0,2013-11-01 00:00:00'
CSV_DATA_LATEST2  = 'rtoaster,9999,itemcode2,1234567890,商品名2,http://img_url/2,http://link_url2,200円,商品説明2,0,2013-11-02 00:00:00'
CSV_DATA_LATEST3  = 'rtoaster,9999,itemcode3,1234567890,商品名3,http://img_url/3,http://link_url3,300円,商品説明3,0,2013-11-03 00:00:00'

@pytest.fixture
def data_dir(tmp_tools_dir):
    return tmp_tools_dir.mkdir('itemmstupdator').mkdir('9999')

@contextlib.contextmanager
def _patches_with_default_action(func_init_config):
    with contextlib.nested (
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('shutil.rmtree'),
        patch('lib.s3util.S3Util.download_single_file'),
        patch('lib.s3util.S3Util.download_single_file_order_by_filename'),
        patch('lib.s3util.S3Util.upload_single_file'),
        patch('lib.util.code_to_long_id'),
    ) as (m_config, m_rmtree, m_s3get, m_s3getlatest, m_s3put, m_itemcodeconv):
        m_s3get.return_value        = {'key': S3FILE_RAW}
        m_s3getlatest.return_value  = {'key': S3FILE_LATEST_OLD}
        m_itemcodeconv.return_value = 1234567890

        yield (m_s3get, m_s3getlatest, m_s3put, m_itemcodeconv)

def _create_gzip(file, lines):
    with gzip.open(file, 'wb') as f:
        f.write('\n'.join(lines))

def _assert_py_path_file(py_path_file, lines):
    assert py_path_file.check(file=1, exists=1)
    content = '\n'.join(lines)
    if content:
        content = content + '\n'
    assert gzip.open(str(py_path_file), 'rb').read() == content

def test_update(func_init_config, data_dir):

    # prepare
    latest_file_old = data_dir.join('item_mst_latest_20131105130000_9999.csv.gz')
    raw_file        = data_dir.join('item_mst_20131105140000_9999.csv.gz')
    latest_file_new = data_dir.join('item_mst_latest_20131105140000_9999.csv.gz')

    # latest: item1, item2, item3
    # update:        item2, item3, item4, item5
    _create_gzip(str(latest_file_old), [CSV_HEADER_LATEST, CSV_DATA_LATEST1, CSV_DATA_LATEST2, CSV_DATA_LATEST3])
    _create_gzip(str(raw_file),        [CSV_HEADER_RAW, CSV_DATA_RAW2, CSV_DATA_RAW3, CSV_DATA_RAW4, CSV_DATA_RAW5])

    # execute
    with _patches_with_default_action(func_init_config) as (m_s3get, m_s3getlatest, m_s3put, m_itemcodeconv):
        updator.update(SID, ENV, S3BUCKET, S3FILE_RAW)

    # assert
    _assert_py_path_file(
        latest_file_new,
        [
            CSV_HEADER_LATEST,
            'rtoaster,9999,itemcode2,1234567890,商品名２,http://img_url/20,http://link_url20,2000円,商品説明２,2,2013-11-12 00:00:00',
            'rtoaster,9999,itemcode3,1234567890,商品名３,http://img_url/30,http://link_url30,3000円,商品説明３,3,2013-11-13 00:00:00',
            'rtoaster,9999,itemcode4,1234567890,商品名４,http://img_url/40,http://link_url40,4000円,商品説明４,4,2013-11-14 00:00:00',
            'rtoaster,9999,itemcode5,1234567890,商品名５,http://img_url/50,http://link_url50,5000円,商品説明５,5,2013-11-15 00:00:00',
            CSV_DATA_LATEST1
        ]
    )

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE_RAW, str(data_dir))

    assert m_s3getlatest.call_count == 1
    assert m_s3getlatest.call_args_list[0][0] == (S3BUCKET, '9999/item_mst/latest/', str(data_dir))

    assert m_itemcodeconv.call_count == 4
    assert m_itemcodeconv.call_args_list[0][0] == ('itemcode2',)
    assert m_itemcodeconv.call_args_list[1][0] == ('itemcode3',)
    assert m_itemcodeconv.call_args_list[2][0] == ('itemcode4',)
    assert m_itemcodeconv.call_args_list[3][0] == ('itemcode5',)

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(latest_file_new), S3BUCKET, '9999/item_mst/latest/')

def test_update_raw_file_not_exists(func_init_config, data_dir):

    # execute
    with _patches_with_default_action(func_init_config) as (m_s3get, m_s3getlatest, m_s3put, m_itemcodeconv):
        m_s3get.return_value = None
        with pytest.raises(NoS3Files):
            updator.update(SID, ENV, S3BUCKET, S3FILE_RAW)

    # assert
    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE_RAW, str(data_dir))

    assert m_s3getlatest.call_count  == 0
    assert m_itemcodeconv.call_count == 0
    assert m_s3put.call_count        == 0

def test_update_latest_file_not_exists(func_init_config, data_dir):

    # prepare
    raw_file        = data_dir.join('item_mst_20131105140000_9999.csv.gz')
    latest_file_new = data_dir.join('item_mst_latest_20131105140000_9999.csv.gz')

    # update:        item2, item3, item4, item5
    _create_gzip(str(raw_file),        [CSV_HEADER_RAW, CSV_DATA_RAW2, CSV_DATA_RAW3, CSV_DATA_RAW4, CSV_DATA_RAW5])

    # execute
    with _patches_with_default_action(func_init_config) as (m_s3get, m_s3getlatest, m_s3put, m_itemcodeconv):
        m_s3getlatest.return_value = None
        updator.update(SID, ENV, S3BUCKET, S3FILE_RAW)

    # assert
    _assert_py_path_file(
        latest_file_new,
        [
            CSV_HEADER_LATEST,
            'rtoaster,9999,itemcode2,1234567890,商品名２,http://img_url/20,http://link_url20,2000円,商品説明２,2,2013-11-12 00:00:00',
            'rtoaster,9999,itemcode3,1234567890,商品名３,http://img_url/30,http://link_url30,3000円,商品説明３,3,2013-11-13 00:00:00',
            'rtoaster,9999,itemcode4,1234567890,商品名４,http://img_url/40,http://link_url40,4000円,商品説明４,4,2013-11-14 00:00:00',
            'rtoaster,9999,itemcode5,1234567890,商品名５,http://img_url/50,http://link_url50,5000円,商品説明５,5,2013-11-15 00:00:00',
        ]
    )

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE_RAW, str(data_dir))

    assert m_s3getlatest.call_count == 1
    assert m_s3getlatest.call_args_list[0][0] == (S3BUCKET, '9999/item_mst/latest/', str(data_dir))

    assert m_itemcodeconv.call_count == 4
    assert m_itemcodeconv.call_args_list[0][0] == ('itemcode2',)
    assert m_itemcodeconv.call_args_list[1][0] == ('itemcode3',)
    assert m_itemcodeconv.call_args_list[2][0] == ('itemcode4',)
    assert m_itemcodeconv.call_args_list[3][0] == ('itemcode5',)

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(latest_file_new), S3BUCKET, '9999/item_mst/latest/')

def test_update_empty_raw_file(func_init_config, data_dir):

    # prepare
    latest_file_old = data_dir.join('item_mst_latest_20131105130000_9999.csv.gz')
    raw_file        = data_dir.join('item_mst_20131105140000_9999.csv.gz')
    latest_file_new = data_dir.join('item_mst_latest_20131105140000_9999.csv.gz')

    # latest: item1, item2, item3
    # update:        item2, item3, item4, item5
    _create_gzip(str(latest_file_old), [CSV_HEADER_LATEST, CSV_DATA_LATEST1, CSV_DATA_LATEST2, CSV_DATA_LATEST3])
    _create_gzip(str(raw_file),        [])

    # execute
    with _patches_with_default_action(func_init_config) as (m_s3get, m_s3getlatest, m_s3put, m_itemcodeconv):
        updator.update(SID, ENV, S3BUCKET, S3FILE_RAW)

    # assert
    _assert_py_path_file(
        latest_file_new,
        [CSV_HEADER_LATEST, CSV_DATA_LATEST1, CSV_DATA_LATEST2, CSV_DATA_LATEST3]
    )

    assert m_s3get.call_count == 1
    assert m_s3get.call_args_list[0][0] == (S3BUCKET, S3FILE_RAW, str(data_dir))

    assert m_s3getlatest.call_count == 1
    assert m_s3getlatest.call_args_list[0][0] == (S3BUCKET, '9999/item_mst/latest/', str(data_dir))

    assert m_itemcodeconv.call_count == 0

    assert m_s3put.call_count == 1
    assert m_s3put.call_args_list[0][0] == (str(latest_file_new), S3BUCKET, '9999/item_mst/latest/')

def test_update_ignore_gdo(func_init_config, data_dir):

    # execute
    with _patches_with_default_action(func_init_config) as (m_s3get, m_s3getlatest, m_s3put, m_itemcodeconv):
        updator.update('0009', ENV, S3BUCKET, S3FILE_RAW)

    # assert
    assert m_s3get.call_count        == 0
    assert m_s3getlatest.call_count  == 0
    assert m_itemcodeconv.call_count == 0
    assert m_s3put.call_count        == 0
