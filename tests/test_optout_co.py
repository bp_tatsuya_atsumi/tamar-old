# -*- coding: utf-8 -*-

from __future__ import absolute_import

import optout_co
import contextlib
import datetime
import pytest
import gzip
from mock import patch
from sets import Set

ENV = 'dev'

LOG_DATA1 = '2013-10-18T00:00:00+09:00\toptout.co\t{"remote":"-","host":"-","user":"-","method":"GET","path":"/co_optout?uid=test1","code":"200","size":"43"}'
LOG_DATA2 = '2013-10-18T00:10:00+09:00\toptout.co\t{"remote":"-","host":"-","user":"-","method":"GET","path":"/co_optout?uid=test2","code":"200","size":"43"}'

@pytest.fixture
def log_dir(tmp_optout_dir):
    return tmp_optout_dir.mkdir('co').mkdir('log')

class CurrentDatetime(datetime.datetime):
    @classmethod
    def now(cls):
        return cls(2013, 10, 19, 23, 59, 59, 0)

@contextlib.contextmanager
def _patches_with_default_action(func_init_config):
    with contextlib.nested (
        patch('datetime.datetime', CurrentDatetime),
        patch('conf.config.BatchConfig.__init__', new=func_init_config),
        patch('lib.s3util.S3Util.download_files'),
        patch('shutil.rmtree'),
        patch('os.makedirs'),
        patch('lib.mappingtable.MappingTable.delete_rows_by_partner_user_ids'),
        patch('lib.mappingtable.MappingTable.select_user_ids_by_partner_user_id'),
        patch('lib.mappingtable.MappingTable.delete_rows_by_user_ids')
    ) as (m_today, m_config, m_s3download, m_rmtree, m_mkdirs, m_delete_co, m_select_uids, m_delete_uid):
        m_s3download.return_value = []
        m_delete_co.return_value  = 0
        m_delete_uid.return_value = 0
        m_select_uids.side_effect = _select_uids_from_ptuid
        yield (m_s3download, m_delete_co, m_select_uids, m_delete_uid)

def _select_uids_from_ptuid(*args):
    tbl = {
        'test1': ['rt_uid1'],
        'test2': ['rt_uid1', 'rt_uid2'],
    }
    return tbl.get(args[0])

def _create_gzip(file, lines):
    with gzip.open(file, 'wb') as f:
        f.write('\n'.join(lines))

def test_execute(log_dir, func_init_config):

    # prepare
    data_file = log_dir.join('test_file.gz')
    _create_gzip(str(data_file), [LOG_DATA1])

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3download, m_delete_co, m_select_uids, m_delete_uid = mocks
        m_s3download.return_value = [{ 'is_dir'  : False,
                                       'is_file' : True,
                                       'key'     : 'test_file.gz' }]
        optout_co_obj = optout_co.CoOptout(ENV)
        optout_co_obj.execute()

    # assert
    assert m_s3download.call_count == 1
    assert m_s3download.call_args_list[0][0] == (u'dev-partner-log', u'optout/co/2013/10/18/', str(log_dir))
    assert m_delete_co.call_count == 1
    assert m_delete_co.call_args_list[0][0][0] == Set(['test1'])
    assert m_select_uids.call_count == 1
    assert len([arg_list for arg_list in m_select_uids.call_args_list if arg_list[0][0] == 'test1']) == 1
    assert m_delete_uid.call_count == 9

def test_execute_with_several_logs(log_dir, func_init_config):

    # prepare
    data_file1 = log_dir.join('test_file1.gz')
    _create_gzip(str(data_file1), [LOG_DATA1])
    data_file2 = log_dir.join('test_file2.gz')
    _create_gzip(str(data_file2), [LOG_DATA2])

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3download, m_delete_co, m_select_uids, m_delete_uid = mocks
        m_s3download.return_value = [{ 'is_dir'  : False,
                                       'is_file' : True,
                                       'key'     : 'test_file1.gz' },
                                     { 'is_dir'  : False,
                                       'is_file' : True,
                                       'key'     : 'test_file2.gz' } ]
        optout_co_obj = optout_co.CoOptout(ENV)
        optout_co_obj.execute()

    # assert
    assert m_s3download.call_count == 1
    assert m_s3download.call_args_list[0][0] == (u'dev-partner-log', u'optout/co/2013/10/18/', str(log_dir))
    assert m_delete_co.call_count == 1
    assert m_delete_co.call_args_list[0][0][0] == Set(['test1', 'test2'])
    assert m_select_uids.call_count == 2
    assert len([arg_list for arg_list in m_select_uids.call_args_list if arg_list[0][0] == 'test1']) == 1
    assert len([arg_list for arg_list in m_select_uids.call_args_list if arg_list[0][0] == 'test2']) == 1
    assert m_delete_uid.call_count == 9

def test_execute_specified_day(log_dir, func_init_config):

    # prepare
    data_file = log_dir.join('test_file.gz')
    _create_gzip(str(data_file), [LOG_DATA1])

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3download, m_delete_co, m_select_uids, m_delete_uid = mocks
        m_s3download.return_value = [{ 'is_dir'  : False,
                                       'is_file' : True,
                                       'key'     : 'test_file.gz' }]
        optout_co.CoOptout(ENV, '20991231').execute()

    # assert
    assert m_s3download.call_count == 1
    assert m_s3download.call_args_list[0][0] == (u'dev-partner-log', u'optout/co/2099/12/31/', str(log_dir))
    assert m_delete_co.call_count == 1
    assert m_delete_co.call_args_list[0][0][0] == Set(['test1'])
    assert m_select_uids.call_count == 1
    assert len([arg_list for arg_list in m_select_uids.call_args_list if arg_list[0][0] == 'test1']) == 1

def test_execute_no_logs(log_dir, func_init_config):

    # execute
    with _patches_with_default_action(func_init_config) as mocks:
        m_s3download, m_delete_co, m_select_uids, m_delete_uid = mocks
        m_s3download.return_value = []
        optout_co.CoOptout(ENV).execute()

    # assert
    assert m_s3download.call_count == 1
    assert m_s3download.call_args_list[0][0] == (u'dev-partner-log', u'optout/co/2013/10/18/', str(log_dir))
    assert m_delete_co.call_count == 0
    assert m_select_uids.call_count == 0
    assert m_delete_uid.call_count == 0

