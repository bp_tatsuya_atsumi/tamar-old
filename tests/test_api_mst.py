# -*- coding: utf-8 -*-

from __future__       import absolute_import
import api_mst
import base64
import contextlib
import json
import pytest
from mock                import Mock
from mock                import patch
from lib.segmentmsttable import SegmentMstTable
import lib.extapi.errors as errors

ENV = 'dev'
PID = 'bp1'
SID = '9999'
FORBIDDEN_SID = '9998'

CONTENT_TYPE = 'application/json;charset=UTF-8'
REGISTERED_SEGMENTS = [
    { 'segment_id_from':'f100' , 'segment_id_to':'t100' , 'name':'name100'       , 'description':'note100' , 'status':0 },
    { 'segment_id_from':'f200' , 'segment_id_to':'t200' , 'name':'name200'       , 'description':'note200' , 'status':0 },
    { 'segment_id_from':'f300' , 'segment_id_to':'t300' , 'name':'セグメント300' , 'description':'備考300' , 'status':0 }
]

GET_HEADERS        = { 'Authorization': 'Basic ' + base64.b64encode('bp_user' + ":" + 'eqd0IavUAtP9qb8n') }
POST_HEADERS       = { 'Authorization': 'Basic ' + base64.b64encode('bp_user' + ":" + 'eqd0IavUAtP9qb8n'),
                       'Accept'       : 'applicatin/json;charset=UTF-8' }
FALSE_GET_HEADERS  = { 'Authorization': 'Basic ' + base64.b64encode('bp_user' + ":" + 'false_password') }
FALSE_POST_HEADERS = { 'Authorization': 'Basic ' + base64.b64encode('bp_user' + ":" + 'false_password'),
                       'Accept'       : 'applicatin/json;charset=UTF-8' }


def _init():
     api_mst.api.app.config['TESTING'] = True
     return api_mst.api.app.test_client()

def _select_row_by_segment_id(*args):
    result = [ s for s in REGISTERED_SEGMENTS if s['segment_id_from'] == args[0] ]
    if len(result) > 0:
        return result[0]
    else:
        return None

@contextlib.contextmanager
def _patches(func_init_config):
    with contextlib.nested (
        patch('requests.get'),
        patch('requests.post'),
        patch('requests.put'),
        patch('lib.segmentmsttable.SegmentMstTable.select_all_rows'),
        patch('lib.segmentmsttable.SegmentMstTable.select_row_by_segment_id'),
        patch('lib.segmentmsttable.SegmentMstTable.insert_row'),
        patch('lib.segmentmsttable.SegmentMstTable.delete_rows_by_segment_id'),
        patch('lib.segmentmsttable.SegmentMstTable.update_row_by_segment_id'),
    ) as (m_get, m_post, m_put, m_segmst_all, m_segmst_sel, m_segmst_ins, m_segmst_del, m_segmst_update):
        m_get.return_value = Mock()
        m_get.return_value.status_code = 200
        m_get.return_value.text = '{"limit": 10, "count": 3}'
        m_post.return_value = Mock()
        m_post.return_value.status_code = 200
        m_post.return_value.text = '{"id": "t400"}'
        m_put.return_value = Mock()
        m_put.return_value.status_code = 200
        m_segmst_all.return_value = REGISTERED_SEGMENTS
        m_segmst_sel.side_effect = _select_row_by_segment_id
        yield (m_get, m_post, m_put, m_segmst_all, m_segmst_sel, m_segmst_ins, m_segmst_del, m_segmst_update)


def test_unauthorized(tmp_transfer_dir, func_init_config):
    with _patches(func_init_config) as (m_get, m_post, m_put, m_segmst_all, m_segmst_sel, m_segmst_ins, m_segmst_del, m_segmst_update):
        # GET
        result = _init().get('/v1/segmst?pid={pid}&sid={sid}'.format(pid=PID, sid=SID),
                             headers=FALSE_GET_HEADERS)
        assert result.status_code == 401

        # POST
        result = _init().post('/v1/segmst',
                              data=json.dumps({'pid'        : PID,
                                               'sid'        : SID,
                                               'seg_id'     : 'f400',
                                               'name'       : 'name',
                                               'description': 'desc'}),
                              content_type=CONTENT_TYPE,
                              headers=FALSE_POST_HEADERS)
        assert result.status_code == 401

        # PUT
        result = _init().put('/v1/segmst',
                             data=json.dumps({'pid'        : PID,
                                              'sid'        : SID,
                                              'seg_id'     : 'f100',
                                              'name'       : 'name',
                                              'description': 'desc'}),
                             content_type=CONTENT_TYPE,
                             headers=FALSE_POST_HEADERS)
        assert result.status_code == 401

        # DELETE
        result = _init().delete('/v1/segmst',
                                data=json.dumps({'pid'        : PID,
                                                 'sid'        : SID,
                                                 'seg_id'     : 'f100'}),
                                content_type=CONTENT_TYPE,
                                headers=FALSE_POST_HEADERS)
        assert result.status_code == 401


def test_forbidden_sid(tmp_transfer_dir, func_init_config):
    with _patches(func_init_config) as (m_get, m_post, m_put, m_segmst_all, m_segmst_sel, m_segmst_ins, m_segmst_del, m_segmst_update):
        # GET
        result = _init().get('/v1/segmst?pid={pid}&sid={sid}'.format(pid=PID, sid=FORBIDDEN_SID),
                             headers=GET_HEADERS)
        assert result.status_code == 401
        # POST
        result = _init().post('/v1/segmst',
                              data=json.dumps({'pid'        : PID,
                                               'sid'        : FORBIDDEN_SID,
                                               'seg_id'     : 'f400',
                                               'name'       : 'name',
                                               'description': 'desc'}),
                              content_type=CONTENT_TYPE,
                              headers=POST_HEADERS)
        assert result.status_code == 401
        # PUT
        result = _init().put('/v1/segmst',
                             data=json.dumps({'pid'        : PID,
                                              'sid'        : FORBIDDEN_SID,
                                              'seg_id'     : 'f100',
                                              'name'       : 'name',
                                              'description': 'desc'}),
                             content_type=CONTENT_TYPE,
                             headers=POST_HEADERS)
        assert result.status_code == 401
        # DELETE
        result = _init().delete('/v1/segmst',
                                data=json.dumps({'pid'        : PID,
                                                 'sid'        : FORBIDDEN_SID,
                                                 'seg_id'     : 'f100'}),
                                content_type=CONTENT_TYPE,
                                headers=POST_HEADERS)
        assert result.status_code == 401



def test_get(tmp_transfer_dir, func_init_config):
    with _patches(func_init_config) as (m_get, m_post, m_put, m_segmst_all, m_segmst_sel, m_segmst_ins, m_segmst_del, m_segmst_update):
        result = _init().get('/v1/segmst?pid={pid}&sid={sid}'.format(pid=PID, sid=SID),
                             headers=GET_HEADERS)
    assert result.charset      == 'utf-8'
    assert result.content_type == 'application/json; charset=UTF-8'
    assert result.status_code  == 200
    assert json.loads(result.get_data()) == {
               u'segments' : [{u'id':s['segment_id_from'], u'name':s['name'].decode('utf-8'), u'description':s['description'].decode('utf-8')} for s in REGISTERED_SEGMENTS]
           }
    assert m_get.call_count           == 0
    assert m_post.call_count          == 0
    assert m_put.call_count           == 0
    assert m_segmst_all.call_count    == 1
    assert m_segmst_sel.call_count    == 0
    assert m_segmst_ins.call_count    == 0
    assert m_segmst_del.call_count    == 0
    assert m_segmst_update.call_count == 0

def test_get_failed(tmp_transfer_dir, func_init_config):
    with _patches(func_init_config) as (m_get, m_post, m_put, m_segmst_all, m_segmst_sel, m_segmst_ins, m_segmst_del, m_segmst_update):
        m_segmst_all.side_effect = Exception()
        result = _init().get('/v1/segmst?pid={pid}&sid={sid}'.format(pid=PID, sid=SID),
                             headers=GET_HEADERS)
    expected_error = errors.InternalError()
    assert result.status_code == expected_error.code == 500
    assert json.loads(result.get_data()) == { 'message': expected_error.description, 'status_code': expected_error.code }
    assert m_get.call_count           == 0
    assert m_post.call_count          == 0
    assert m_put.call_count           == 0
    assert m_segmst_all.call_count    == 1
    assert m_segmst_sel.call_count    == 0
    assert m_segmst_ins.call_count    == 0
    assert m_segmst_del.call_count    == 0
    assert m_segmst_update.call_count == 0



def test_post(tmp_transfer_dir, func_init_config):
    with _patches(func_init_config) as (m_get, m_post, m_put, m_segmst_all, m_segmst_sel, m_segmst_ins, m_segmst_del, m_segmst_update):
        result = _init().post('/v1/segmst',
                              data=json.dumps({'pid'        : PID,
                                               'sid'        : SID,
                                               'seg_id'     : 'f400',
                                               'name'       : 'name',
                                               'description': 'desc'}),
                              content_type=CONTENT_TYPE,
                              headers=POST_HEADERS)
    assert result.status_code == 200
    assert json.loads(result.get_data()) == { 'status' : 'ok' }
    assert m_get.call_count           == 1
    assert m_post.call_count          == 1
    assert m_put.call_count           == 0
    assert m_segmst_all.call_count    == 1
    assert m_segmst_sel.call_count    == 0
    assert m_segmst_ins.call_count    == 1
    assert m_segmst_del.call_count    == 0
    assert m_segmst_update.call_count == 0

    # without description
    with _patches(func_init_config) as (m_get, m_post, m_put, m_segmst_all, m_segmst_sel, m_segmst_ins, m_segmst_del, m_segmst_update):
        result = _init().post('/v1/segmst',
                              data=json.dumps({'pid'        : PID,
                                               'sid'        : SID,
                                               'seg_id'     : 'f400',
                                               'name'       : 'name'}),
                              content_type=CONTENT_TYPE,
                              headers=POST_HEADERS)
    assert result.status_code == 200
    assert json.loads(result.get_data()) == { 'status' : 'ok' }
    assert m_get.call_count           == 1
    assert m_post.call_count          == 1
    assert m_put.call_count           == 0
    assert m_segmst_all.call_count    == 1
    assert m_segmst_sel.call_count    == 0
    assert m_segmst_ins.call_count    == 1
    assert m_segmst_del.call_count    == 0
    assert m_segmst_update.call_count == 0

def test_post_over_limit(tmp_transfer_dir, func_init_config):
    with _patches(func_init_config) as (m_get, m_post, m_put, m_segmst_all, m_segmst_sel, m_segmst_ins, m_segmst_del, m_segmst_update):
        m_get.return_value.text = '{"limit": 10, "count": 10}'
        result = _init().post('/v1/segmst',
                              data=json.dumps({'pid'        : PID,
                                               'sid'        : SID,
                                               'seg_id'     : 'f400',
                                               'name'       : 'name',
                                               'description': 'desc'}),
                              content_type=CONTENT_TYPE,
                              headers=POST_HEADERS)
    assert result.status_code == 400
    assert m_get.call_count           == 1
    assert m_post.call_count          == 0
    assert m_put.call_count           == 0
    assert m_segmst_all.call_count    == 1
    assert m_segmst_sel.call_count    == 0
    assert m_segmst_ins.call_count    == 0
    assert m_segmst_del.call_count    == 0
    assert m_segmst_update.call_count == 0

def test_post_api_failed(tmp_transfer_dir, func_init_config):
    with _patches(func_init_config) as (m_get, m_post, m_put, m_segmst_all, m_segmst_sel, m_segmst_ins, m_segmst_del, m_segmst_update):
        m_post.return_value.status_code = 500
        result = _init().post('/v1/segmst',
                              data=json.dumps({'pid'        : PID,
                                               'sid'        : SID,
                                               'seg_id'     : 'f400',
                                               'name'       : 'name',
                                               'description': 'desc'}),
                              content_type=CONTENT_TYPE,
                              headers=POST_HEADERS)
    expected_error = errors.PostFailed()
    assert result.status_code == expected_error.code == 500
    assert json.loads(result.get_data()) == { 'message': expected_error.description, 'status_code': expected_error.code }
    assert m_get.call_count           == 1
    assert m_post.call_count          == 1
    assert m_put.call_count           == 0
    assert m_segmst_all.call_count    == 1
    assert m_segmst_sel.call_count    == 0
    assert m_segmst_ins.call_count    == 0
    assert m_segmst_del.call_count    == 0
    assert m_segmst_update.call_count == 0

def test_post_sql_failed(tmp_transfer_dir, func_init_config):
    with _patches(func_init_config) as (m_get, m_post, m_put, m_segmst_all, m_segmst_sel, m_segmst_ins, m_segmst_del, m_segmst_update):
        m_segmst_ins.side_effect = Exception()
        result = _init().post('/v1/segmst',
                              data=json.dumps({'pid'        : PID,
                                               'sid'        : SID,
                                               'seg_id'     : 'f400',
                                               'name'       : 'name',
                                               'description': 'desc'}),
                              content_type=CONTENT_TYPE,
                              headers=POST_HEADERS)
    expected_error = errors.StatusConflicted()
    assert result.status_code == expected_error.code == 500
    assert json.loads(result.get_data()) == { 'message': expected_error.description, 'status_code': expected_error.code }
    assert m_get.call_count           == 1
    assert m_post.call_count          == 1
    assert m_put.call_count           == 0
    assert m_segmst_all.call_count    == 1
    assert m_segmst_sel.call_count    == 0
    assert m_segmst_ins.call_count    == 1
    assert m_segmst_del.call_count    == 0
    assert m_segmst_update.call_count == 0

def test_post_duplicate(tmp_transfer_dir, func_init_config):
    with _patches(func_init_config) as (m_get, m_post, m_put, m_segmst_all, m_segmst_sel, m_segmst_ins, m_segmst_del, m_segmst_update):
        # id is duplicated
        result = _init().post('/v1/segmst',
                              data=json.dumps({'pid'        : PID,
                                               'sid'        : SID,
                                               'seg_id'     : 'f100',
                                               'name'       : 'name',
                                               'description': 'desc'}),
                              content_type=CONTENT_TYPE,
                              headers=POST_HEADERS)
        expected_error = errors.SegmentDuplicated()
        assert result.status_code == expected_error.code == 400

        # name is duplicated
        result = _init().post('/v1/segmst',
                              data=json.dumps({'pid'        : PID,
                                               'sid'        : SID,
                                               'seg_id'     : 'f400',
                                               'name'       : 'name100',
                                               'description': 'desc'}),
                              content_type=CONTENT_TYPE,
                              headers=POST_HEADERS)
        expected_error = errors.SegmentDuplicated()
        assert result.status_code == expected_error.code == 400

def test_post_params_too_long(tmp_transfer_dir, func_init_config):
    with _patches(func_init_config) as (m_get, m_post, m_put, m_segmst_all, m_segmst_sel, m_segmst_ins, m_segmst_del, m_segmst_update):
        # id too long
        seg_id_51 = 'a' * 51
        result = _init().post('/v1/segmst',
                              data=json.dumps({'pid'        : PID,
                                               'sid'        : SID,
                                               'seg_id'     : seg_id_51,
                                               'name'       : 'name',
                                               'description': 'desc'}),
                              content_type=CONTENT_TYPE,
                              headers=POST_HEADERS)
        expected_error = errors.IllegalParameter()
        assert result.status_code == expected_error.code == 400

        # name too long
        name_51 = 'a' * 51
        result = _init().post('/v1/segmst',
                              data=json.dumps({'pid'        : PID,
                                               'sid'        : SID,
                                               'seg_id'     : 'f400',
                                               'name'       : name_51,
                                               'description': 'desc'}),
                              content_type=CONTENT_TYPE,
                              headers=POST_HEADERS)
        expected_error = errors.IllegalParameter()
        assert result.status_code == expected_error.code == 400

        # description too long
        description_1001 = 'a' * 1001
        result = _init().post('/v1/segmst',
                              data=json.dumps({'pid'        : PID,
                                               'sid'        : SID,
                                               'seg_id'     : 'f400',
                                               'name'       : 'name',
                                               'description': description_1001}),
                              content_type=CONTENT_TYPE,
                              headers=POST_HEADERS)
        expected_error = errors.IllegalParameter()
        assert result.status_code == expected_error.code == 400

def test_post_params_multibyte(tmp_transfer_dir, func_init_config):
    with _patches(func_init_config) as (m_get, m_post, m_put, m_segmst_all, m_segmst_sel, m_segmst_ins, m_segmst_del, m_segmst_update):
        # id must not contain multi-byte characters
        result = _init().post('/v1/segmst',
                              data=json.dumps({'pid'        : PID,
                                               'sid'        : SID,
                                               'seg_id'     : 'セグメントID',
                                               'name'       : 'name',
                                               'description': 'desc'}),
                              content_type=CONTENT_TYPE,
                              headers=POST_HEADERS)
        expected_error = errors.IllegalParameter()
        assert result.status_code == expected_error.code == 400

        # name can contain multi-byte characters
        result = _init().post('/v1/segmst',
                              data=json.dumps({'pid'        : PID,
                                               'sid'        : SID,
                                               'seg_id'     : 'f400',
                                               'name'       : 'セグメント名',
                                               'description': 'desc'}),
                              content_type=CONTENT_TYPE,
                              headers=POST_HEADERS)
        assert result.status_code == 200
        assert json.loads(result.get_data()) == { 'status' : 'ok' }

        # a multi-byte character should be counted as 1
        name_50_multibyte = 'あ' * 50
        result = _init().post('/v1/segmst',
                              data=json.dumps({'pid'        : PID,
                                               'sid'        : SID,
                                               'seg_id'     : 'f400',
                                               'name'       : name_50_multibyte,
                                               'description': 'desc'}),
                              content_type=CONTENT_TYPE,
                              headers=POST_HEADERS)
        assert result.status_code == 200
        assert json.loads(result.get_data()) == { 'status' : 'ok' }

        # description can contain multi-byte characters
        result = _init().post('/v1/segmst',
                              data=json.dumps({'pid'        : PID,
                                               'sid'        : SID,
                                               'seg_id'     : 'f400',
                                               'name'       : 'name',
                                               'description': '備考'}),
                              content_type=CONTENT_TYPE,
                              headers=POST_HEADERS)
        assert result.status_code == 200
        assert json.loads(result.get_data()) == { 'status' : 'ok' }

        # a multi-byte character should be counted as 1
        description_1000_multibyte = 'あ' * 1000
        result = _init().post('/v1/segmst',
                              data=json.dumps({'pid'        : PID,
                                               'sid'        : SID,
                                               'seg_id'     : 'f400',
                                               'name'       : 'name',
                                               'description': description_1000_multibyte}),
                              content_type=CONTENT_TYPE,
                              headers=POST_HEADERS)
        assert result.status_code == 200
        assert json.loads(result.get_data()) == { 'status' : 'ok' }



def test_put(tmp_transfer_dir, func_init_config):
    with _patches(func_init_config) as (m_get, m_post, m_put, m_segmst_all, m_segmst_sel, m_segmst_ins, m_segmst_del, m_segmst_update):
        result = _init().put('/v1/segmst',
                             data=json.dumps({'pid'        : PID,
                                              'sid'        : SID,
                                              'seg_id'     : 'f100',
                                              'name'       : 'name',
                                              'description': 'desc'}),
                             content_type=CONTENT_TYPE,
                             headers=POST_HEADERS)
    assert result.status_code == 200
    assert json.loads(result.get_data()) == { 'status' : 'ok' }
    assert m_get.call_count           == 0
    assert m_post.call_count          == 0
    assert m_put.call_count           == 1
    assert m_segmst_all.call_count    == 0
    assert m_segmst_sel.call_count    == 1
    assert m_segmst_ins.call_count    == 0
    assert m_segmst_del.call_count    == 0
    assert m_segmst_update.call_count == 1

    # without name
    with _patches(func_init_config) as (m_get, m_post, m_put, m_segmst_all, m_segmst_sel, m_segmst_ins, m_segmst_del, m_segmst_update):
        result = _init().put('/v1/segmst',
                             data=json.dumps({'pid'        : PID,
                                              'sid'        : SID,
                                              'seg_id'     : 'f100',
                                              'description': 'desc'}),
                             content_type=CONTENT_TYPE,
                             headers=POST_HEADERS)
    assert result.status_code == 200
    assert json.loads(result.get_data()) == { 'status' : 'ok' }
    assert m_get.call_count           == 0
    assert m_post.call_count          == 0
    assert m_put.call_count           == 1
    assert m_segmst_all.call_count    == 0
    assert m_segmst_sel.call_count    == 1
    assert m_segmst_ins.call_count    == 0
    assert m_segmst_del.call_count    == 0
    assert m_segmst_update.call_count == 1

    # without description
    with _patches(func_init_config) as (m_get, m_post, m_put, m_segmst_all, m_segmst_sel, m_segmst_ins, m_segmst_del, m_segmst_update):
        result = _init().put('/v1/segmst',
                             data=json.dumps({'pid'        : PID,
                                              'sid'        : SID,
                                              'seg_id'     : 'f100',
                                              'name'       : 'name'}),
                             content_type=CONTENT_TYPE,
                             headers=POST_HEADERS)
    assert result.status_code == 200
    assert json.loads(result.get_data()) == { 'status' : 'ok' }
    assert m_get.call_count           == 0
    assert m_post.call_count          == 0
    assert m_put.call_count           == 1
    assert m_segmst_all.call_count    == 0
    assert m_segmst_sel.call_count    == 1
    assert m_segmst_ins.call_count    == 0
    assert m_segmst_del.call_count    == 0
    assert m_segmst_update.call_count == 1

def test_put_notfound(tmp_transfer_dir, func_init_config):
    with _patches(func_init_config) as (m_get, m_post, m_put, m_segmst_all, m_segmst_sel, m_segmst_ins, m_segmst_del, m_segmst_update):
        result = _init().put('/v1/segmst',
                             data=json.dumps({'pid'        : PID,
                                              'sid'        : SID,
                                              'seg_id'     : 'f400',
                                              'name'       : 'name',
                                              'description': 'desc'}),
                             content_type=CONTENT_TYPE,
                             headers=POST_HEADERS)
    expected_error = errors.SegmentNotFound()
    assert result.status_code == expected_error.code == 404
    assert json.loads(result.get_data()) == { 'message': expected_error.description, 'status_code': expected_error.code }
    assert m_get.call_count           == 0
    assert m_post.call_count          == 0
    assert m_put.call_count           == 0
    assert m_segmst_all.call_count    == 0
    assert m_segmst_sel.call_count    == 1
    assert m_segmst_ins.call_count    == 0
    assert m_segmst_del.call_count    == 0
    assert m_segmst_update.call_count == 0

def test_put_api_failed(tmp_transfer_dir, func_init_config):
    with _patches(func_init_config) as (m_get, m_post, m_put, m_segmst_all, m_segmst_sel, m_segmst_ins, m_segmst_del, m_segmst_update):
        m_put.return_value.status_code = 500
        result = _init().put('/v1/segmst',
                             data=json.dumps({'pid'        : PID,
                                              'sid'        : SID,
                                              'seg_id'     : 'f100',
                                              'name'       : 'name',
                                              'description': 'desc'}),
                             content_type=CONTENT_TYPE,
                             headers=POST_HEADERS)
    expected_error = errors.UpdateFailed()
    assert result.status_code == expected_error.code == 500
    assert json.loads(result.get_data()) == { 'message': expected_error.description, 'status_code': expected_error.code }
    assert m_get.call_count           == 0
    assert m_post.call_count          == 0
    assert m_put.call_count           == 1
    assert m_segmst_all.call_count    == 0
    assert m_segmst_sel.call_count    == 1
    assert m_segmst_ins.call_count    == 0
    assert m_segmst_del.call_count    == 0
    assert m_segmst_update.call_count == 0

def test_put_sql_failed(tmp_transfer_dir, func_init_config):
    with _patches(func_init_config) as (m_get, m_post, m_put, m_segmst_all, m_segmst_sel, m_segmst_ins, m_segmst_del, m_segmst_update):
        m_segmst_update.side_effect = Exception()
        result = _init().put('/v1/segmst',
                             data=json.dumps({'pid'        : PID,
                                              'sid'        : SID,
                                              'seg_id'     : 'f100',
                                              'name'       : 'name',
                                              'description': 'desc'}),
                             content_type=CONTENT_TYPE,
                             headers=POST_HEADERS)
    expected_error = errors.StatusConflicted()
    assert result.status_code == expected_error.code == 500
    assert json.loads(result.get_data()) == { 'message': expected_error.description, 'status_code': expected_error.code }
    assert m_get.call_count           == 0
    assert m_post.call_count          == 0
    assert m_put.call_count           == 1
    assert m_segmst_all.call_count    == 0
    assert m_segmst_sel.call_count    == 1
    assert m_segmst_ins.call_count    == 0
    assert m_segmst_del.call_count    == 0
    assert m_segmst_update.call_count == 1

def test_put_params_too_long(tmp_transfer_dir, func_init_config):
    with _patches(func_init_config) as (m_get, m_post, m_put, m_segmst_all, m_segmst_sel, m_segmst_ins, m_segmst_del, m_segmst_update):
        # name too long
        name_51 = 'a' * 51
        result = _init().put('/v1/segmst',
                             data=json.dumps({'pid'        : PID,
                                              'sid'        : SID,
                                              'seg_id'     : 'f400',
                                              'name'       : name_51,
                                              'description': 'desc'}),
                             content_type=CONTENT_TYPE,
                             headers=POST_HEADERS)
        expected_error = errors.IllegalParameter()
        assert result.status_code == expected_error.code == 400

        # description too long
        description_1001 = 'a' * 1001
        result = _init().put('/v1/segmst',
                             data=json.dumps({'pid'        : PID,
                                              'sid'        : SID,
                                              'seg_id'     : 'f400',
                                              'name'       : 'name',
                                              'description': description_1001}),
                             content_type=CONTENT_TYPE,
                             headers=POST_HEADERS)
        expected_error = errors.IllegalParameter()
        assert result.status_code == expected_error.code == 400



def test_delete(tmp_transfer_dir, func_init_config):
    with _patches(func_init_config) as (m_get, m_post, m_put, m_segmst_all, m_segmst_sel, m_segmst_ins, m_segmst_del, m_segmst_update):
        result = _init().delete('/v1/segmst',
                                data=json.dumps({'pid'        : PID,
                                                 'sid'        : SID,
                                                 'seg_id'     : 'f100'}),
                                content_type=CONTENT_TYPE,
                                headers=POST_HEADERS)
    assert result.status_code == 200
    assert json.loads(result.get_data()) == { 'status' : 'ok' }
    assert m_get.call_count           == 0
    assert m_post.call_count          == 0
    assert m_put.call_count           == 1
    assert m_segmst_all.call_count    == 0
    assert m_segmst_sel.call_count    == 1
    assert m_segmst_ins.call_count    == 0
    assert m_segmst_del.call_count    == 1
    assert m_segmst_update.call_count == 0

def test_delete_notfound(tmp_transfer_dir, func_init_config):
    with _patches(func_init_config) as (m_get, m_post, m_put, m_segmst_all, m_segmst_sel, m_segmst_ins, m_segmst_del, m_segmst_update):
        result = _init().delete('/v1/segmst',
                                data=json.dumps({'pid'        : PID,
                                                 'sid'        : SID,
                                                 'seg_id'     : 'f400'}),
                                content_type=CONTENT_TYPE,
                                headers=POST_HEADERS)
    expected_error = errors.SegmentNotFound()
    assert result.status_code == expected_error.code == 404
    assert json.loads(result.get_data()) == { 'message': expected_error.description, 'status_code': expected_error.code }
    assert m_get.call_count           == 0
    assert m_post.call_count          == 0
    assert m_put.call_count           == 0
    assert m_segmst_all.call_count    == 0
    assert m_segmst_sel.call_count    == 1
    assert m_segmst_ins.call_count    == 0
    assert m_segmst_del.call_count    == 0
    assert m_segmst_update.call_count == 0

def test_delete_api_failed(tmp_transfer_dir, func_init_config):
    with _patches(func_init_config) as (m_get, m_post, m_put, m_segmst_all, m_segmst_sel, m_segmst_ins, m_segmst_del, m_segmst_update):
        m_put.return_value.status_code = 500
        result = _init().delete('/v1/segmst',
                                data=json.dumps({'pid'        : PID,
                                                 'sid'        : SID,
                                                 'seg_id'     : 'f100'}),
                                content_type=CONTENT_TYPE,
                                headers=POST_HEADERS)
    expected_error = errors.DeleteFailed()
    assert result.status_code == expected_error.code == 500
    assert json.loads(result.get_data()) == { 'message': expected_error.description, 'status_code': expected_error.code }
    assert m_get.call_count           == 0
    assert m_post.call_count          == 0
    assert m_put.call_count           == 1
    assert m_segmst_all.call_count    == 0
    assert m_segmst_sel.call_count    == 1
    assert m_segmst_ins.call_count    == 0
    assert m_segmst_del.call_count    == 0
    assert m_segmst_update.call_count == 0

def test_delete_sql_failed(tmp_transfer_dir, func_init_config):
    with _patches(func_init_config) as (m_get, m_post, m_put, m_segmst_all, m_segmst_sel, m_segmst_ins, m_segmst_del, m_segmst_update):
        m_segmst_del.side_effect = Exception()
        result = _init().delete('/v1/segmst',
                                data=json.dumps({'pid'        : PID,
                                                 'sid'        : SID,
                                                 'seg_id'     : 'f100'}),
                                content_type=CONTENT_TYPE,
                                headers=POST_HEADERS)
    expected_error = errors.StatusConflicted()
    assert result.status_code == expected_error.code == 500
    assert json.loads(result.get_data()) == { 'message': expected_error.description, 'status_code': expected_error.code }
    assert m_get.call_count           == 0
    assert m_post.call_count          == 0
    assert m_put.call_count           == 1
    assert m_segmst_all.call_count    == 0
    assert m_segmst_sel.call_count    == 1
    assert m_segmst_ins.call_count    == 0
    assert m_segmst_del.call_count    == 1
    assert m_segmst_update.call_count == 0
