# -*- coding: utf-8 -*-

import json
import os

CONF_DIR_COMMON = 'conf/common/{env}'

FILE_NUM_COMMON = 5
FILE_NUM_SYNC   = 1

SETTING_FILES = {
                    'SITE'        : 'site_mst.json',
                    'PARTNER'     : 'partner_mst.json',
                    'MAPPING'     : 'mapping.json',
                    'DATABASE'    : 'database.json',
                    'CRYPTO'      : 'cryptokey.txt',
                }


def _check_file_base(env):

    # common
    common_dir = CONF_DIR_COMMON.format(env=env)
    common_files = os.listdir(common_dir.format(env=env))
    assert len(common_files) == FILE_NUM_COMMON
    assert set(common_files).issubset(set(SETTING_FILES.values()))
    assert os.path.getsize(os.path.join(common_dir, SETTING_FILES['SITE']))     > 0
    assert os.path.getsize(os.path.join(common_dir, SETTING_FILES['PARTNER']))  > 0
    assert os.path.getsize(os.path.join(common_dir, SETTING_FILES['MAPPING']))  > 0
    assert os.path.getsize(os.path.join(common_dir, SETTING_FILES['DATABASE'])) > 0
    assert os.path.getsize(os.path.join(common_dir, SETTING_FILES['CRYPTO']))   > 0

def _check_file_detail(env):
    common_dir = CONF_DIR_COMMON.format(env=env)

    site_mst    = json.load(open(os.path.join(common_dir, SETTING_FILES['SITE'])))
    partner_mst = json.load(open(os.path.join(common_dir, SETTING_FILES['PARTNER'])))
    mapping     = json.load(open(os.path.join(common_dir, SETTING_FILES['MAPPING'])))
    database    = json.load(open(os.path.join(common_dir, SETTING_FILES['DATABASE'])))

    # check valid data
    assert site_mst and site_mst.keys() > 0
    assert partner_mst and partner_mst.keys() > 0
    assert mapping and mapping.keys() > 0
    assert database and database.keys() > 0

    # check site_mst.json
    for site_id in site_mst:
        assert site_id in mapping
        no_mapping = True
        for partner_id in mapping[site_id]:
            assert partner_id  in partner_mst
            if partner_id != 'sy':
                no_mapping = False
        if not no_mapping:
            assert site_id in database

    # check crypto.txt
    with open(os.path.join(common_dir, SETTING_FILES['CRYPTO'])) as f:
        for line in f:
            assert line != ''

def test_dev_checker():
    env = 'dev'
    _check_file_base(env)
    _check_file_detail(env)

def test_stg_checker():
    env = 'stg'
    _check_file_base(env)
    _check_file_detail(env)

def test_prd_checker():
    env = 'prd'
    _check_file_base(env)
    _check_file_detail(env)

