# -*- coding: utf-8 -*-

from conf.config import ApplicationConfig
from conf.config import BatchConfig
from lib.crypto import AesCrypto


def test_base_prd_env():
    config_prd = ApplicationConfig('prd')

    assert config_prd.home_dir         == '/usr/local/tamar'
    assert config_prd.tmp_dir          == '/usr/local/tamar/tmp'
    assert config_prd.tmp_mapping_dir  == '/usr/local/tamar/tmp/mapping/{sid}/{pid}'
    assert config_prd.tmp_transfer_dir == '/usr/local/tamar/tmp/transfer/{sid}/{pid}/{data_type}'
    assert config_prd.tmp_tools_dir    == '/usr/local/tamar/tmp/tools/{tool_name}'
    assert config_prd.log_dir          == '/usr/local/tamar/log'
    assert config_prd.lock_dir         == '/usr/local/tamar/lock'
    assert config_prd.repos_dir        == '/usr/local/tamar/repos'

    assert config_prd.s3bucket_partner_log   == 'prd-partner-log'
    assert config_prd.s3bucket_site_log      == 'prd-site-log'
    assert config_prd.s3bucket_transfer_data == 'prd-transfer-data'

    assert config_prd.partner_mst['ma'] == {
                                                'name_jp'             : u'マイクロアド',
                                                'name_en'             : u'microad',
                                                'description'         : u'',
                                                'http_sync_endpoint'  : 'http://dex.send.microad.jp/pixel?ma_mid=brpd&uid={uid}&sid={sid}',
                                                'https_sync_endpoint' : 'https://dex.send.microad.jp/pixel?ma_mid=brpd&uid={uid}&sid={sid}',
                                                'option'              : {
                                                                            'transfer_data_types' : [
                                                                                'segmentmst',    'remove_segmentmst',
                                                                                'segmentdata',   'conversion',
                                                                                'recommendmst',  'remove_recommendmst',
                                                                                'recommenddata', 'itemmst'
                                                                            ],
                                                                        }
                                           }
    assert config_prd.partner_mst['ao'] == {
                                                'name_jp'             : u'モデューロ',
                                                'name_en'             : u'modulo',
                                                'description'         : u'',
                                                'http_sync_endpoint'  : 'http://aw.dw.impact-ad.jp/c/v/sync?sid={sid}&uid={uid}',
                                                'https_sync_endpoint' : 'https://aw.dw.impact-ad.jp/c/v/sync?sid={sid}&uid={uid}',
                                                'option'              : {
                                                                            'transfer_data_types'        : ['segmentmst', 'segmentdata'],
                                                                            'transfer_latest_data_types' : ['segmentmst']
                                                                        }
                                           }
    assert config_prd.partner_mst['so'] == {
                                                'name_jp'             : u'スケールアウト',
                                                'name_en'             : u'scaleout',
                                                'description'         : u'',
                                                'http_sync_endpoint'  : 'http://tg.socdm.com/aux/idsync?proto=brainpad&sid={sid}&uid={uid}',
                                                'https_sync_endpoint' : 'https://ssl.socdm.com/aux/idsync?proto=brainpad&sid={sid}&uid={uid}',
                                                'option'              : {
                                                                            'api_config': {
                                                                                 'url'      : 'https://api.scaleout.jp/api/',
                                                                                 'username' : 'coven-dev@brainpad.co.jp',
                                                                                 'password' : 'MOc95asYmI9MPv93'
                                                                            },
                                                                            'transfer_data_types'        : ['segmentdata', 'recommenddata', 'itemmst'],
                                                                            'transfer_latest_data_types' : ['itemmst']
                                                                        }
                                           }
    assert config_prd.partner_mst['ac'] == {
                                                'name_jp'             : u'アドバタイジングドットコム',
                                                'name_en'             : u'Advertising.com',
                                                'description'         : u'',
                                                'http_sync_endpoint'  : 'http://tacoda.at.atwola.com/atx/sync/jpbpad/jpbpa/default?sid={sid}&uid={uid}',
                                                'https_sync_endpoint' : 'https://tacoda.at.atwola.com/atx/sync/jpbpad/jpbpa/default?sid={sid}&uid={uid}',
                                                'option'              : {
                                                                            'transfer_data_types' : ['segmentdata']
                                                                        }
                                           }
    assert config_prd.crypto == AesCrypto('-6dKj*rY7U!qWX3z')

def test_base_stg_env():
    config_stg = ApplicationConfig('stg')

    assert config_stg.home_dir         == '/usr/local/tamar'
    assert config_stg.tmp_dir          == '/usr/local/tamar/tmp'
    assert config_stg.tmp_mapping_dir  == '/usr/local/tamar/tmp/mapping/{sid}/{pid}'
    assert config_stg.tmp_transfer_dir == '/usr/local/tamar/tmp/transfer/{sid}/{pid}/{data_type}'
    assert config_stg.tmp_tools_dir    == '/usr/local/tamar/tmp/tools/{tool_name}'
    assert config_stg.log_dir          == '/usr/local/tamar/log'
    assert config_stg.lock_dir         == '/usr/local/tamar/lock'
    assert config_stg.repos_dir        == '/usr/local/tamar/repos'

    assert config_stg.s3bucket_partner_log   == 'stg-partner-log'
    assert config_stg.s3bucket_site_log      == 'stg-site-log'
    assert config_stg.s3bucket_transfer_data == 'stg-transfer-data'

    assert config_stg.partner_mst['ma'] == {
                                                'name_jp'             : u'マイクロアド',
                                                'name_en'             : u'microad',
                                                'description'         : u'',
                                                'http_sync_endpoint'  : 'http://dex.send.microad.jp/pixel?ma_mid=brpd&uid={uid}&sid={sid}',
                                                'https_sync_endpoint' : 'https://dex.send.microad.jp/pixel?ma_mid=brpd&uid={uid}&sid={sid}',
                                                'option'              : {
                                                                            'transfer_data_types' : [
                                                                                'segmentmst',    'remove_segmentmst',
                                                                                'segmentdata',   'conversion',
                                                                                'recommendmst',  'remove_recommendmst',
                                                                                'recommenddata', 'itemmst'
                                                                            ],
                                                                        }
                                           }
    assert config_stg.partner_mst['ao'] == {
                                                'name_jp'             : u'モデューロ',
                                                'name_en'             : u'modulo',
                                                'description'         : u'',
                                                'http_sync_endpoint'  : 'http://aw.dw.impact-ad.jp/c/v/sync?sid={sid}&uid={uid}',
                                                'https_sync_endpoint' : 'https://aw.dw.impact-ad.jp/c/v/sync?sid={sid}&uid={uid}',
                                                'option'              : {
                                                                            'transfer_data_types'        : ['segmentmst', 'segmentdata'],
                                                                            'transfer_latest_data_types' : ['segmentmst']
                                                                        }
                                           }
    assert config_stg.partner_mst['so'] == {
                                                'name_jp'             : u'スケールアウト',
                                                'name_en'             : u'scaleout',
                                                'description'         : u'',
                                                'http_sync_endpoint'  : 'http://tg.socdm.com/aux/idsync?proto=brainpad&sid={sid}&uid={uid}',
                                                'https_sync_endpoint' : 'https://ssl.socdm.com/aux/idsync?proto=brainpad&sid={sid}&uid={uid}',
                                                'option'              : {
                                                                            'api_config': {
                                                                                'url'      : 'http://api-test.scaleout.jp/api/',
                                                                                'username' : 'bpapitest@scaleout.jp',
                                                                                'password' : 'zrR3Te4M'
                                                                            },
                                                                            'transfer_data_types'        : ['segmentdata', 'recommenddata', 'itemmst'],
                                                                            'transfer_latest_data_types' : ['itemmst']
                                                                        }
                                           }
    assert config_stg.partner_mst['ac'] == {
                                                'name_jp'             : u'アドバタイジングドットコム',
                                                'name_en'             : u'Advertising.com',
                                                'description'         : u'',
                                                'http_sync_endpoint'  : 'http://tacoda.at.atwola.com/atx/sync/jpbpad/jpbpa/default?sid={sid}&uid={uid}',
                                                'https_sync_endpoint' : 'https://tacoda.at.atwola.com/atx/sync/jpbpad/jpbpa/default?sid={sid}&uid={uid}',
                                                'option'              : {
                                                                            'transfer_data_types' : ['segmentdata']
                                                                        }
                                           }
    assert config_stg.crypto == AesCrypto('-6dKj*rY7U!qWX3z')

def test_base_dev_env():
    config_dev = ApplicationConfig('dev')

    assert config_dev.home_dir         == '/usr/local/tamar'
    assert config_dev.tmp_dir          == '/usr/local/tamar/tmp'
    assert config_dev.tmp_mapping_dir  == '/usr/local/tamar/tmp/mapping/{sid}/{pid}'
    assert config_dev.tmp_transfer_dir == '/usr/local/tamar/tmp/transfer/{sid}/{pid}/{data_type}'
    assert config_dev.tmp_tools_dir    == '/usr/local/tamar/tmp/tools/{tool_name}'
    assert config_dev.tmp_rawdata_dir  == '/usr/local/tamar/tmp/rawdata'
    assert config_dev.log_dir          == '/usr/local/tamar/log'
    assert config_dev.lock_dir         == '/usr/local/tamar/lock'
    assert config_dev.repos_dir        == '/usr/local/tamar/repos'

    assert config_dev.s3bucket_partner_log   == 'dev-partner-log'
    assert config_dev.s3bucket_site_log      == 'dev-site-log'
    assert config_dev.s3bucket_transfer_data == 'dev-transfer-data'

    assert config_dev.site_mst['9999']   == {
                                                'name_jp'             : u'ブレインパッドテスト',
                                                'name_en'             : u'BrainPad, Inc',
                                                'description'         : u'テスト用アカウント',
                                            }

    assert config_dev.partner_mst['bp1'] == {
                                                'name_jp'             : u'ブレインパッドテスト1',
                                                'name_en'             : u'brainpad-test1',
                                                'description'         : u'キャッシュを必要とする連携サイト',
                                                'http_sync_endpoint'  : 'http://bp1-test.c-ovn.jp/sync.php?sid={sid}&pid=bp1&uid={uid}',
                                                'https_sync_endpoint' : 'https://bp1-test.c-ovn.jp/sync.php?sid={sid}&pid=bp1&uid={uid}',
                                                'option'              : {
                                                                            'transfer_data_types' : [
                                                                                'segmentmst',    'remove_segmentmst',
                                                                                'segmentdata',   'conversion',
                                                                                'recommendmst',  'remove_recommendmst',
                                                                                'recommenddata', 'itemmst'
                                                                            ],
                                                                            'external' : {
                                                                                'user' : 'bp_user',
                                                                                'pass' : '$6$rounds=109505$DqHrG5iWcWh5PSAJ$iEBQruGs11KxMF6Cq0jmfF2i1bDaoIRobTsM.s6uO6746EImpQiof8ITkgwzarUO2oww0eWa9XSe/H9nysHYj0',
                                                                                'raw_pass'    : 'eqd0IavUAtP9qb8n'
                                                                            }
                                                                        }
                                            }
    assert config_dev.partner_mst['ma']  == {
                                                'name_jp'             : u'マイクロアドテスト',
                                                'name_en'             : u'microad-test',
                                                'description'         : u'',
                                                'http_sync_endpoint'  : 'http://ma-test.c-ovn.jp/sync.php?sid={sid}&pid=ma&uid={uid}',
                                                'https_sync_endpoint' : 'https://ma-test.c-ovn.jp/sync.php?sid={sid}&pid=ma&uid={uid}',
                                                'option'              : {
                                                                            'transfer_data_types' : [
                                                                                'segmentmst',    'remove_segmentmst',
                                                                                'segmentdata',   'conversion',
                                                                                'recommendmst',  'remove_recommendmst',
                                                                                'recommenddata', 'itemmst'
                                                                            ],
                                                                        }
                                            }
    assert config_dev.partner_mst['ao']  == {
                                                'name_jp'             : u'モデューロテスト',
                                                'name_en'             : u'modulo-test',
                                                'description'         : u'',
                                                'http_sync_endpoint'  : 'http://ao-test.c-ovn.jp/sync.php?sid={sid}&pid=ao&uid={uid}',
                                                'https_sync_endpoint' : 'https://ao-test.c-ovn.jp/sync.php?sid={sid}&pid=ao&uid={uid}',
                                                'option'              : {
                                                                            'transfer_data_types'        : ['segmentmst', 'segmentdata'],
                                                                            'transfer_latest_data_types' : ['segmentmst']
                                                                        }
                                            }
    assert config_dev.partner_mst['so']  == {
                                                'name_jp'             : u'スケールアウトテスト',
                                                'name_en'             : u'scaleout-test',
                                                'description'         : u'',
                                                'http_sync_endpoint'  : 'http://so-test.c-ovn.jp/sync.php?sid={sid}&pid=so&uid={uid}',
                                                'https_sync_endpoint' : 'https://so-test.c-ovn.jp/sync.php?sid={sid}&pid=so&uid={uid}',
                                                'option'              : {
                                                                            'api_config': {
                                                                                'url'      : 'http://api-test.scaleout.jp/api/',
                                                                                'username' : 'bpapitest@scaleout.jp',
                                                                                'password' : 'zrR3Te4M'
                                                                            },
                                                                            'transfer_data_types'        : ['segmentdata', 'recommenddata', 'itemmst'],
                                                                            'transfer_latest_data_types' : ['itemmst']
                                                                        }
                                            }
    assert config_dev.partner_mst['ac']  == {
                                                'name_jp'             : u'アドバタイジングドットコム・ジャパンテスト',
                                                'name_en'             : u'advertising.com-test',
                                                'description'         : u'',
                                                'http_sync_endpoint'  : '',
                                                'https_sync_endpoint' : '',
                                                'option'              : {
                                                                            'transfer_data_types' : ['segmentdata']
                                                                        }
                                            }

    assert config_dev.mapping['9999']['bp1']['auth']   == {
                                                              'id'            : 'id_9999',
                                                              'password'      : 'password_9999'
                                                          }
    assert config_dev.mapping['9999']['bp1']['ttl']    == 3
    assert config_dev.mapping['9999']['ma']['auth']    == {
                                                              'login_id'      : 'login_id_9999',
                                                              'password'      : 'password_9999',
                                                              'advertiser_id' : 'advertiser_id_9999'
                                                          }
    assert config_dev.mapping['9999']['ma']['ttl']     == 3
    assert config_dev.mapping['9999']['so']['auth']    == {
                                                              'aid'           : 'brainpad',
                                                              'foreign_id'    : '1234123456789012'
                                                          }
    assert config_dev.mapping['9999']['so']['rawdata'] == True
    assert config_dev.mapping['9999']['ao']['auth']    == {
                                                              'client_id'     : 'bp',
                                                              'client_name'   : u'株式会社ブレインパッド',
                                                          }
    assert config_dev.mapping['9999']['ao']['ttl']     == 3

    assert config_dev.database['9999']   == {
                                                'host'     : 'localhost',
                                                'port'     : '3306',
                                                'user'     : 'tamaruser',
                                                'password' : '#!TawaR?u5er',
                                                'db_name'  : 'DB_TAMAR_9999'
                                            }

    assert config_dev.crypto == AesCrypto('-6dKj*rY7U!qWX3z')


def test_app_prd_env():
    config_prd = ApplicationConfig('prd')
    assert config_prd.environment   == 'prd'

def test_app_stg_env():
    config_stg = ApplicationConfig('stg')
    assert config_stg.environment   == 'stg'

def test_app_dev_env():
    config_dev = ApplicationConfig('dev')
    assert config_dev.environment   == 'dev'

def test_batch_prd_env():
    config_prd = BatchConfig('prd')
    assert config_prd.environment     == 'prd'

def test_batch_stg_env():
    config_stg = BatchConfig('stg')
    assert config_stg.environment     == 'stg'

def test_batch_dev_env():
    config_dev = BatchConfig('dev')
    assert config_dev.environment     == 'dev'

    mapping_log_funcs = config_dev.get_mapping_log_funcs('test', '[prefix]')
    assert len(mapping_log_funcs) == 4
    assert mapping_log_funcs[0].__name__ == 'info'
    assert mapping_log_funcs[1].__name__ == 'warning'
    assert mapping_log_funcs[2].__name__ == 'error'
    assert mapping_log_funcs[3].__name__ == 'exception'

    transfer_log_funcs = config_dev.get_transfer_log_funcs('test', '[prefix]')
    assert len(transfer_log_funcs) == 4
    assert transfer_log_funcs[0].__name__ == 'info'
    assert transfer_log_funcs[1].__name__ == 'warning'
    assert transfer_log_funcs[2].__name__ == 'error'
    assert transfer_log_funcs[3].__name__ == 'exception'
