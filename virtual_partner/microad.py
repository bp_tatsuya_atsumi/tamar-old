# -*- coding: utf-8 -*-

import pprint
import datetime
from flask import Flask
from flask import Response
from flask import abort
from flask import jsonify
from flask import redirect
from flask import request

app = Flask(__name__)
pp = pprint.PrettyPrinter(indent=4)

# ID sync
@app.route('/pixel', methods=['GET'])
def sync():
    CHECK_PARAMS = ['ma_mid', 'uid', 'sid', 'pid']

    keys = request.args.keys()
    if set(keys) != set(CHECK_PARAMS):
        return abort(400)

    import random
    pt_uid = random.randint(1,1000000000)

    redirect_url = 'http://p-ma-test.c-ovn.jp/p-sync?pid=ma-test&uid={uid}&pt_uid={pt_uid}&sid={sid}'\
                       .format(sid=request.args.get('sid'),
                               uid=request.args.get('uid'),
                               pt_uid=pt_uid)

    return redirect(redirect_url)

# register segment list master
@app.route('/segmentlist', methods=['POST'])
def register_segmst():
    CHECK_PARAMS = ['app_key', 'login_id', 'password', 'advertiser_id']

    pretty_print_form('segmentmst', request.form)
    keys = request.form.keys()
    if (len(keys) - len(CHECK_PARAMS)) % 3 != 0:
        return abort(400)
    check_counter = 0
    for key in keys:
        if key in CHECK_PARAMS:
            check_counter += 1
        else:
            if not 'seg_' in key:
                return abort(400)

    if request.form.get('app_key') == '401':
        return Response('authentification error', 401)
    elif request.form.get('app_key') == '500':
        return Response('api error', 500)
    elif request.form.get('app_key') == '503':
        return Response('maintenance', 503)

    if check_counter != 4:
        return abort(400)

    update_segment_num = (len(keys) - len(CHECK_PARAMS))/3

    return jsonify(login_id=request.form.get('login_id'),
                   advertiser_id=request.form.get('advertiser_id'),
                   segment_count=update_segment_num)

# remove segment list master
@app.route('/segmentlist/remove_bulk', methods=['POST'])
def remove_segmst():
    CHECK_PARAMS = ['app_key', 'login_id', 'password', 'advertiser_id']

    pretty_print_form('remove_segmst', request.form)
    keys = request.form.keys()
    check_counter = 0
    remove_segment_num = 0
    for key in keys:
        if key in CHECK_PARAMS:
            check_counter += 1
        else:
            if not 'seg_' in key:
                return abort(400)
            else:
                remove_segment_num += 1

    if request.form.get('app_key') == '401':
        return Response('authentification error', 401)
    elif request.form.get('app_key') == '500':
        return Response('api error', 500)
    elif request.form.get('app_key') == '503':
        return Response('maintenance', 503)

    if check_counter != 4:
        return abort(400)

    return jsonify(login_id=request.form.get('login_id'),
                   advertiser_id=request.form.get('advertiser_id'),
                   segment_count=remove_segment_num)


# segment list data
@app.route('/sl', methods=['GET'])
def seglist():
    keys = request.args.keys()

    if set(keys) != set(['v', 'u', 'sl']):
        return abort(400)

    v  = request.args.get('v')
    u  = request.args.get('u')
    sl = request.args.get('sl')

    if not v or not u or not sl:
        return abort(400)

    for account in sl.split(';'):
        splited_account = account.split(':')
        if len(splited_account) != 2 or not splited_account[1]:
            return abort(400)

        co_account_and_time = splited_account[0].split('-')
        segment_list_str    = splited_account[1].split(',')

        if len(co_account_and_time) != 2 or len(segment_list_str) == 0:
            return abort(400)

        for element in co_account_and_time:
            if not element:
                return abort(400)

        for element in segment_list_str:
            if not element:
                return abort(400)

    return Response('valid argument')


# register recommend list master
@app.route('/recommendlist', methods=['POST'])
def register_recmst():
    CHECK_PARAMS = ['app_key', 'login_id', 'password', 'advertiser_id']

    pretty_print_form('recommendmst', request.form)
    keys = request.form.keys()
    if (len(keys) - len(CHECK_PARAMS)) % 3 != 0:
        return abort(400)
    check_counter = 0
    for key in keys:
        if key in CHECK_PARAMS:
            check_counter += 1
        else:
            if not 'rpl_' in key:
                return abort(400)

    if request.form.get('app_key') == '401':
        return Response('authentification error', 401)
    elif request.form.get('app_key') == '500':
        return Response('api error', 500)
    elif request.form.get('app_key') == '503':
        return Response('maintenance', 503)

    if check_counter != 4:
        return abort(400)

    update_recommend_num = (len(keys) - len(CHECK_PARAMS))/3

    return jsonify(login_id=request.form.get('login_id'),
                   advertiser_id=request.form.get('advertiser_id'),
                   rpl_count=update_recommend_num)

# remove recommend list master
@app.route('/recommendlist/remove_bulk', methods=['POST'])
def remove_recmst():
    CHECK_PARAMS = ['app_key', 'login_id', 'password', 'advertiser_id']

    pretty_print_form('remove_recommendmst', request.form)
    keys = request.form.keys()
    check_counter = 0
    remove_recommend_num = 0
    for key in keys:
        if key in CHECK_PARAMS:
            check_counter += 1
        else:
            if not 'rpl_' in key:
                return abort(400)
            else:
                remove_recommend_num += 1

    if request.form.get('app_key') == '401':
        return Response('authentification error', 401)
    elif request.form.get('app_key') == '500':
        return Response('api error', 500)
    elif request.form.get('app_key') == '503':
        return Response('maintenance', 503)

    if check_counter != 4:
        return abort(400)

    return jsonify(login_id=request.form.get('login_id'),
                   advertiser_id=request.form.get('advertiser_id'),
                   rpl_count=remove_recommend_num)

# recommend list data
@app.route('/rpl', methods=['GET'])
def recdata():
    keys = request.args.keys()

    if set(keys) != set(['v', 'u', 'rpl']):
        return abort(400)

    v   = request.args.get('v')
    u   = request.args.get('u')
    rpl = request.args.get('rpl')

    if not v or not u or not rpl:
        return abort(400)

    for account in rpl.split(';'):
        splited_account = account.split(':')
        if len(splited_account) != 2 or not splited_account[1]:
            return abort(400)

        advertiser_id_and_ttl = splited_account[0]
        if not advertiser_id_and_ttl:
            return abort(400)
        splitted_data = advertiser_id_and_ttl.split('-')
        if len(splitted_data) != 2:
            return abort(400)
        if not splitted_data[0] or not splitted_data[1]:
            abort(400)

        rpl_id_and_p_id_list  = splited_account[1].split(',')
        if len(rpl_id_and_p_id_list) == 0:
            return abort(400)
        for rpl_id_and_p_id_str in rpl_id_and_p_id_list:
            splitted_data = rpl_id_and_p_id_str.split('-')
            if len(splitted_data) == 1:
                return abort(400)
            for splitted_data in rpl_id_and_p_id_str.split('-'):
                rpl_id    = splitted_data[0]
                rpl_p_ids = splitted_data[1].split('.')
                if not rpl_id or len(rpl_p_ids) == 0:
                    return abort(400)

    return Response('valid argument')

def pretty_print_form(data_type, form):
    print
    print '[%s] %s' % (datetime.datetime.now(), data_type)
    print'{'
    line = lambda k, v: "    '{k}': '{v}'".format(k=k, v=v.encode('utf-8'))
    lines = [line(k, v) for k, v in sorted(form.to_dict().items())]
    print '\n'.join(lines)
    print'}'

if __name__ == '__main__':
    app.run(debug=True)
