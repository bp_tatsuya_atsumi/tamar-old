# -*- coding: utf-8 -*-

import pprint
import datetime
from flask import Flask
from flask import abort
from flask import json
from flask import make_response
from flask import request

app = Flask(__name__)
pp = pprint.PrettyPrinter(indent=4)

@app.route('/api/segmentdata', methods=['POST'])
def segmentdata():
    for line in request.data.splitlines():
        json_obj = json.loads(line)
        pretty_print_data('segmentdata', json_obj)

    if _server_down(json_obj):
        return abort(503)

    response = make_response('', 204)

    return response


def on_json_loading_failed(e):
    return abort(400)


def _server_down(data):
    if data['users'][0]['destUserId'] == "serverdown":
        return True
    else:
        return False


def pretty_print_data(data_type, data):
    print
    print '[%s] %s' % (datetime.datetime.now(), data_type)
    print'{'
    line = lambda k, v: "    '{k}': '{v}'".format(k=k, v=v)
    lines = [line(k, v) for k, v in data.items()]
    print '\n'.join(lines)
    print'}'


if __name__ == '__main__':
    app.run(debug=True)
