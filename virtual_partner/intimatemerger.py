# -*- coding: utf-8 -*-

import pprint
import datetime
import random
import json
from flask import Flask
from flask import Response
from flask import abort
from flask import jsonify
from flask import redirect
from flask import request

app = Flask(__name__)
pp = pprint.PrettyPrinter(indent=4)

@app.route('/token/get', methods=['GET'])
def get_token():
    username = request.args.get('username')
    password = request.args.get('password')
    api_key  = request.args.get('api_key')
    print "username:{}".format(username)
    print "password:{}".format(password)
    print "api_key:{}".format(api_key)
    if not (username and password and api_key):
         abort(401)

    return jsonify(token="the_token")

@app.route('/segment/set', methods=['POST'])
def register_audience():
    customer_id = request.args.get('customer_id')
    token       = request.args.get('token')
    print "customer_id:{}".format(customer_id)
    print "token:{}".format(token)
    if not (customer_id and token):
        abort(401)

    # FIXME データ内容に応じてレスポンスを返す
    return jsonify()

@app.route('/segment/<segid>/', methods=['PUT'])
def update_segmst(segid):
    customer_id = request.args.get('customer_id')
    token       = request.args.get('token')
    name        = json.loads(request.data).get('name')
    print "customer_id:{}".format(customer_id)
    print "token:{}".format(token)
    print "name:{}".format(name.encode('utf-8'))
    if not (customer_id and token and name):
        abort(401)

    return jsonify(id=segid,name=name,created="2014-01-01T00:00:00Z",is_enabled=True)

@app.route('/segment', methods=['POST'])
def register_segmst():
    customer_id = request.args.get('customer_id')
    token       = request.args.get('token')
    name        = json.loads(request.data).get('name')
    print "customer_id:{}".format(customer_id)
    print "token:{}".format(token)
    print "name:{}".format(name.encode('utf-8'))
    if not (customer_id and token and name):
        abort(401)

    return jsonify(id=str(random.randint(1000,9999)),name=name,created="2014-01-01T00:00:00Z",is_enabled=True)

if __name__ == '__main__':
    app.run(debug=True)
