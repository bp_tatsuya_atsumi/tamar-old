# -*- coding: utf-8 -*-

import pprint
import datetime
from flask import Flask
from flask import Response
from flask import abort
from flask import jsonify
from flask import redirect
from flask import request
from flask import make_response
from functools import wraps

app = Flask(__name__)
pp = pprint.PrettyPrinter(indent=4)

def check_auth(username, password):
    """This function is called to check if a username /
    password combination is valid.
    """
    return username == 'bpadmin' and password == 'ARiO3Ukmh5lduqvK'

def authenticate():
    """Sends a 401 response that enables basic auth"""
    return Response(
    'Could not verify your access level for that URL.\n'
    'You have to login with proper credentials', 401,
    {'WWW-Authenticate': 'Basic realm="Login Required"'})

def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth or not check_auth(auth.username, auth.password):
            return authenticate()
        return f(*args, **kwargs)
    return decorated

def _server_down(data):
    if data['users'][0]['destUserId'] == "serverdown":
        return True
    else:
        return False

@app.route('/api/notice', methods=['POST'])
@requires_auth
def segmentdata():
    """
    $ curl http://localhost:5000/api/notice \
    > -u bpadmin:ARiO3Ukmh5lduqvK \
    > -H "Content-type":"application/json" \
    > --data "{\"s3Key\":\"s3KeyValue\", \"contentType\":\"recommend_list_data\", \"sid\":\"1234\"}"
    """
    ng_response_body = """\
{{
    "status": "NG",
    "message": "{ng_message}"
}}
"""
    ok_response_body = """\
{
    "status": "OK",
    "message": ""
}
"""

    data = request.get_json()
    if not data:
        return abort(400)

    try:
        #if _server_down(data):
        #    return abort(503)
        if set(data) != set(['s3Key', 'contentType', 'sid']):
            message = 'post data invalid!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
            response = make_response(ng_response_body.format(ng_message=message), 400)
        elif data['contentType'] not in ['recommend_list_master', 'recommend_list_data']:
            message = 'contentType invalid!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
            response = make_response(ng_response_body.format(ng_message=message), 400)
        else:
            response = make_response(ok_response_body, 200)

        return response
    finally:
        pretty_print_data('recommenddata', data)

def pretty_print_data(data_type, data):
    print
    print '[%s] %s' % (datetime.datetime.now(), data_type)
    print'{'
    line = lambda k, v: "    '{k}': '{v}'".format(k=k, v=v)
    lines = [line(k, v) for k, v in data.items()]
    print '\n'.join(lines)
    print'}'

if __name__ == '__main__':
    app.run(debug=True)
